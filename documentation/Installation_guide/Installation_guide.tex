\documentclass[12pt]{article}
\usepackage[top=0.51in,bottom=0.51in,left=1.25in,right=1.25in]{geometry}

\usepackage{color}
\usepackage{indentfirst}
\usepackage{latexsym,bm} 
\usepackage{amsmath,amssymb} 
\usepackage{graphicx}
\usepackage{multirow}
\usepackage[justification=centering]{caption} 
\usepackage{textcomp} 
\usepackage{stmaryrd} 
\usepackage{hyperref}
\hypersetup{
	colorlinks=true
	linktoc=all
	}
\usepackage[dvipsnames]{xcolor}
\begin{document}

\title{MCTDH-X software: Installation guide}
\date{\today}
\maketitle

\tableofcontents


\newpage
\section{Generalities}
This document describes the procedure for the installation of the MCTDH-X program. As a general information which could help you during installation, MCTDH-X is mainly written in Fortran and built on Ubuntu, and its installation is currently managed through CMake.


The MCTDH-X software can be downloaded from the following gitlab link:
\\ \url{https://gitlab.com/the-mctdh-x-repository/mctdh-x-releases}
\\ To download the program, the easiest way is by clicking the ``Download source code" button on the upper-right corner of the web page. After download and decompression, a folder called \texttt{/mctdh-x-releases-release} is available. 
Please move this folder to a suitable place of your preference. 
In the following, this folder is also referred to as ``the main MCTDH-X folder''. 


If you have the distributed version control management system \texttt{git} and are familiar with it, you can also download the repository by writing in the terminal/command prompt: \\

\noindent
{ \color{brown} \texttt{git clone git@gitlab.com:the-mctdh-x-repository/mctdh-x-releases.git}} \\

\noindent
The advantage of git is that it can be used to automatically check for repository updates.
This is done by stashing (ignoring the local changes) and pulling any updates: \\

\noindent
{ \color{brown} \texttt{git stash}} \\
\noindent
{ \color{brown} \texttt{git pull}} \\

\noindent
Note that any changes that you have made locally (e.g. in the source such as new potentials, interactions etc.) will be overwritten!
The release branch is for end users only.
If you think your changes are important and deserve to be kept in the repository, contact one of the MCTDH-X developers and we will be happy to include them in the next release.





\newpage
\section{Linux}


\subsection{Prerequisite and Pre-installation setup}
\label{sec:linux-install}



\subsection{Preinstalled packages}

The installation and execution of MCTDH-X on Linux systems (personal computers or clusters) require the following packages
\begin{itemize}
	\item \texttt{python3}, \texttt{mkl}, \texttt{CMake}, \texttt{gcc}, \texttt{OpenMPI}, and optionally \texttt{gnuplot} (for plotting), \texttt{ffmpeg} and \texttt{mplayer} (needed by the scripts that generate videos).
\end{itemize}

On PC like Ubuntu, these packages can be installed using the command \\

\noindent
{\color{brown}\texttt{sudo apt-get install <package>}}\\ 

\noindent
where \texttt{<package>} includes: \texttt{python3}, \texttt{cmake}, \texttt{gfortran},  \texttt{libopenmpi-dev}, \texttt{gnuplot}, \texttt{ffmpeg}, \texttt{mplayer}.

On a cluster, the corresponding modules are typically loaded by the command \\

\noindent
{\color{brown}\texttt{module load <package>}}\\ 

\noindent
where \texttt{<package>} includes: \texttt{mkl}, \texttt{cmake}, \texttt{gcc}, \texttt{python3}, \texttt{open\_mpi}.
However, usually you do not have the latest versions of \texttt{gcc} and \texttt{OpenMPI} on your cluster, and different versions of them are usually not compatible. As an example, a working combination is \texttt{gcc/4.8.2} and \texttt{open\_mpi/1.6.5}.



\subsubsection{OpenBLAS and FFTW}

At the moment two different ways are available for the loading of the packages \texttt{OpenBLAS} and \texttt{FFTW}. This can be controlled by the user in the file \\ \texttt{mctdh-x/External\_Software/External\_Software.cmake}. More specifically,

\quad 

\textbf{By pre-installed packages (recommended)}

\begin{enumerate}
	\item Pre-install these two packages by {\color{brown}{\texttt{sudo apt-get install libopenblas-dev}}}, {\color{brown}{\texttt{sudo apt-get install fftw-dev}}}. If you intend to use MPI functionalities, use the command {\color{brown}{\texttt{sudo apt-get install fftw3-mpi-dev}}} instead.
	\item In the file \texttt{mctdh-x/External\_Software/External\_Software.cmake}, set {\color{brown}{\texttt{FFTWEXTERN FALSE}}} and {\color{brown}{\texttt{OpenBLASEXTERN FALSE}}}.
	\item Find the path where the two packages are installed. This requires a bit manual search. Typical installation paths are given as the default option in the file, e.g. \texttt{/usr/lib/x86\_64-linux-gnu/} for the library of fftw. Confirm manually that they are indeed correct. Otherwise, put in the path you have found. Note that lines beginning with a hashtag are comments, so you might need to uncomment the line with the correct path.
\end{enumerate}

\textbf{By packages shipped together with MCTDH-X}

\begin{enumerate}
	\item In the file \texttt{mctdh-x/External\_Software/External\_Software.cmake}, set {\color{brown}{\texttt{FFTWEXTERN TRUE}}} and {\color{brown}{\texttt{OpenBLASEXTERN TRUE}}}.
	\item Make sure you have chosen a version of \texttt{gcc}, which is compatible with the \texttt{OpenBLAS} in \texttt{External\_Software}. Currently it is \texttt{0.3.23}.
\end{enumerate}

Note, you may load \texttt{OpenBLAS} and \texttt{fftw} in different ways. They are independent from each other.



\subsubsection{Troubleshooting}

We provide a (non-exhaustive) list for trouble shooting tips. 
These can be useful if you are working on an old cluster and do not have the administrator rights, and therefore your system and packages cannot be upgraded to recent versions.


\begin{itemize}
    \item If something goes wrong during the CMake compilation and the installation is interrupted with errors, after your fixes (e.g. putting in the correct path in \texttt{ExternalSoftware.cmake}) you might need to clear the CMake cache files when you try to compile again. You can do so by typing \texttt{rm -rf CMakeCache.txt CMakeFiles}.
	\item \textbf{FFTW on Ubuntu:} Some users may encounter difficulties when installing the pre-requisites as described above.
	Specifically, the installation might stop early, with an error message indicating that some FFTW static library (.a file ending)
	cannot be found, e.g. "make[2]:  No rule to make target '/usr/lib/x86$\_$64-linux-gnu//libfftw3$\_$omp.a', needed by 'bin/MCTDHX$\_$gcc'.  Stop."
	If this occurs, that is because Ubuntu's package manager will only install versions of fftw3 that have .so files in them. To salvage this, do the following:
	\begin{enumerate}
		\item Instead of using the package manager, download FFTW directly from \url{http://www.fftw.org/download.html}. On this website there should be a download link to a tarball.
		
		\item untar and unzip via: \\
	
		\noindent
		{\color{brown} \texttt{tar xzf fftw-<version number>.tar.gz}} \\
		
		\item Configure FFTW and specify that you want OpenMP support, otherwise it won't generate the file, via: \\
	
		\noindent
		{\color{brown} \texttt{./configure --enable-openmp }} \\

		\item Install via: \\
	
		\noindent
		{\color{brown} \texttt{make}} \\
		{\color{brown} \texttt{make install}} \\
		{\color{brown} \texttt{make check}} \\

		\item The file should exist now. To figure out where it is, navigate to the root of your file system and search via, e.g.:\\
	
		\noindent
		{\color{brown} \texttt{cd /}} \\
		{\color{brown} \texttt{find -iname libfftw3\_omp.a 2>/dev/null}} \\

		\noindent The last part of the command there just directs all the error messages that you will get, as this command tries to go through all folder some of which will deny permission to access
		into, effectively, the trash. 

		\item The file was probably placed in \texttt{/usr/local/lib/}; MCTDH-X won't find it there with the current specifications. To change this go into the MCTDH-X folder and change the paths in External$\_$Software/External$\_$Software.cmake. Specifically, change the path after \\
				
		\noindent
		{\color{brown} \texttt{SET(FFTW\_LIB	}} \\
		as well as after\\
		{\color{brown} \texttt{set(FFTW\_INCLUDE\_DIR		}} \\
		\noindent to where you found the file, e.g. \texttt{/usr/local/lib/} and \texttt{/usr/local/include}.  
	\end{enumerate}
	The installation script should run through now.

	
	
	\item \textbf{cmake:} The MCTDH-X installation requires \texttt{cmake 3.6} or older. 
	If you are running an older version of Ubuntu, e.g. Ubuntu Xenial Xerus (16.04), the default version of \texttt{cmake} is 3.5.1 and you will not be able to download older versions via {\color{brown} \texttt{sudo apt-get install cmake}} or {\color{brown} \texttt{sudo apt install cmake}}. 
	Instead, you need to download the source code, build it, and install it from scratch.
	This is explained in part B of the answer provided here: \\
	\url{https://askubuntu.com/questions/355565/how-do-i-install-the-latest-version-of-cmake-from-the-command-line}. \\
	We summarize the installation steps below.
	
	\begin{enumerate}
	\item Install the GCC tools via: \\
	
	\noindent
	{\color{brown} \texttt{sudo apt update}} \\
	{\color{brown} \texttt{sudo apt install build-essential libtool autoconf unzip wget}} \\
	
	\item Uninstall the default version provided by Ubuntu's package manager with \\
	
	\noindent
	{\color{brown} \texttt{sudo apt remove --purge --auto-remove cmake}} \\
	
	
	\item Go to the official CMake webpage (\url{https://cmake.org/download/}), then download and extract the latest version. 
	Update the version and build variables in the following command to get the desired version, e.g. for \texttt{cmake 3.25.1}: \\
	
	\noindent
	{\color{brown} \texttt{version=3.25 \#\#  modify this number to the version you want}} \\
	{\color{brown} \texttt{build=1 \#\#  modify this number to the build you want}} \\
	{\color{brown} \texttt{\#\# don't modify from here}} \\
	{\color{brown} \texttt{mkdir ~/temp}} \\
	{\color{brown} \texttt{cd ~/temp}} \\
	{\color{brown} \texttt{wget https://cmake.org/files/v\$version/cmake-\$version.\$build.tar.gz}} \\
	{\color{brown} \texttt{tar -xzvf cmake-\$version.\$build.tar.gz}} \\
	{\color{brown} \texttt{cd cmake-\$version.\$build/}} \\
	
	\item If you don't have \texttt{OpenSSL} installed, bootstrapping might run into errors. You can install \texttt{OpenSSL} with the command: \\
	
	\noindent
	{\color{brown}\texttt{sudo apt-get install libssl-dev}}. \\
	
	\item Install the extracted source by running: \\
	
	\noindent
	{\color{brown} \texttt{./bootstrap}} \\
	{\color{brown} \texttt{make -j\$(nproc)}} \\
	{\color{brown} \texttt{sudo make install}} \\
	
	\item Test your new \texttt{cmake} version: \\
	
	\noindent
	{\color{brown} \texttt{cmake --version}} \\
	
	\noindent
	The output should correspond to the version you chose.
	
	\end{enumerate}
\end{itemize}



\subsection{Installation}

Go to the main MCTDH-X folder \texttt{/mctdh-x}, and run the following command: \\
{\color{brown}\texttt{./Install\_MCTDHX.sh}} 
to initiate the installation. See Fig.~\ref{fig1}. 

In the prompt, you should then choose the option
\texttt{1) FFTW+OpenBlas+GCC+mpif90} by typing ``{\color{brown}1}''. See Fig.~\ref{fig2}.
The program now starts to compile.

Other options are available after choosing ``{\color{brown}3}''. For example if you need MPI for parallelization, you should choose sequentially \texttt{3) Custom}, \texttt{2) GNU}, \texttt{3) FFTW with MPI support}, \texttt{1) openblas}.

Estimated compilation time: 1 minute if you use pre-installed \texttt{OpenBLAS} and \texttt{fftw} packages; 10 minutes if you use packages shipped together with MCTDH-X. 

\textbf{Alternative approach}

In the main MCTDH-X folder \texttt{/mctdh-x}, you can also use the standard \texttt{cmake} installation by the following command: 
{\color{brown}\texttt{cmake .}} and then {\color{brown}\texttt{make -j}}.
The default combination of FFTW+OpenBlas+GCC+mpif90 is automatically chosen in this case.

\begin{figure}[th]
	\centering
	\includegraphics[width=0.7\columnwidth]{screenshot1.png}
	\caption{
	The installation can be initiated by the command {\color{brown}\texttt{./Install\_MCTDHX.sh}}.
	}
	\label{fig1}
\end{figure}
\begin{figure}[th]
	\centering
	\includegraphics[width=0.7\columnwidth]{screenshot2.png}
	\caption{
		When asked, choose the option 1.
	}
	\label{fig2}
\end{figure}


%\subsection{External packages: FFTW and OpenBLAS}
%
%During the installation, there are potential problems regarding to the packages \texttt{FFTW} and \texttt{OpenBLAS}. Specifically, the paths of these two packages need to be specified manually in the file \texttt{External\_Software/External\_Software.cmake}.
%
%In this file, when the variables \texttt{FFTWEXTERN} and \texttt{BLASEXTERN} are set to be \texttt{FALSE}, the headers and library files of these two softwares are read from the manually specified paths via \texttt{FFTW\_INCLUDE\_DIR}, \texttt{FFTW\_LIB}, \texttt{BLAS\_INCLUDE\_DIR}, \texttt{BLAS\_LIB}. Typical possible paths for different systems are already available in the file, please confirm if they are valid on your own computer / system. On Linux, these two packages can be installed by \texttt{apt-get} with keywords \texttt{fftw-dev} and \texttt{libopenblas-dev}.
%
%As a bypass (default method), you can also set the variables  \texttt{FFTWEXTERN} and \texttt{BLASEXTERN} to be \texttt{TRUE}. In this case, a version of the softwares shipped together with \texttt{MCTDH-X} will be installed as well.




\subsection{Is my compilation successful?}

If the compilation is successful, you should see ``\texttt{[100\%]}'' shown near the end of the prompted messages. See Fig.~\ref{fig3}. You should also find two new executable files \texttt{MCTDHX\_gcc} and \texttt{MCTDHX\_analysis\_gcc} in the folder \texttt{/mctdh-x/bin}. The suffix \texttt{gcc} of the files might change to e.g. \texttt{intel} according to the choice of your compiler. 

{\bf IMPORTANT:} The file \texttt{mctdhxlib.so} is now obsolete, though you might see it mentioned in some old tutorials.

{\bf Nota bene:} Whether the compilation is successful or not, it should also make changes in your home folder \texttt{\textasciitilde/}, which you can go to by the command {\color{brown}\texttt{cd}}. These files include a new file called \texttt{.mctdhxrc}, and several new lines in the end of the file \texttt{.bashrc}. These changes enable the following new aliases:
\begin{itemize}
	\item \texttt{MCTDHX} and \texttt{MCTDHX\_analysis}: Run the main program and the analysis program, respectively.
	\item \texttt{bincp} and \texttt{inpcp}: Copy the executable files and the  example input files from the folders \texttt{mctdh-x-releases-release/bin} and \\ \texttt{mctdh-x-releases-release/Input\_Examples}, respectively.
	\item \texttt{cdm}: Go to the folder  \texttt{/mctdh-x-releases-release}.
	\item \texttt{x-make}: Recompilation of the program. Can be useful after changes are made in the source files.
	\item \texttt{x-clean} and \texttt{x-purge}: Remove the compiled files.
\end{itemize}

If your compilation is unsuccessful, please try to run {\color{brown}\texttt{x-make}} first.

\begin{figure}[t]
	\centering
	\includegraphics[width=0.7\columnwidth]{screenshot3.png}
	\caption{
		If the compilation is successful, you should see ``[100\%]'' near the end of the prompted messages.
	}
	\label{fig3}
\end{figure}







\newpage
\section{MacOS}

Note, unless specified, the folders referred to below are subfolders of the main MCTDH-X folder \texttt{/mctdh-x}.

\subsection{Prerequisite and Pre-installation setup}


\subsubsection{General prerequisite}
The installation and the execution of MCTDH-X require Xcode Command Line Tools and HomeBrew. 

\begin{itemize}
	\item \textbf{Xcode}: You can check your installation of Xcode Command Line Tools by \\ {\color{brown}{\texttt{xcode-select -p}}}. \\
	If a path is prompted, for example  \texttt{/Applications/Xcode.app/Contents/Developer}, it means Xcode Command Line Tools is already installed on your Mac. Otherwise, you can install it via the command \\
	{\color{brown}{\texttt{xcode-select --install}}}. \\
	Note, typically after each major update of your MacOS, you might need to reinstall Xcode Command Line Tools. This can be done properly by first uninstalling it  \\
	{\color{brown}\texttt{rm -rf /Library/Developer/CommandLineTools}},\\
	 and then re-install it. Refer to, for example, \url{https://mac.install.guide/commandlinetools/6.html}.
	\item \textbf{HomeBrew}: Check \url{https://brew.sh} for the installation guide for HomeBrew. You need to install the following packages with HomeBrew: \texttt{gcc}, \texttt{cmake}, \texttt{open-mpi}, \texttt{openblas}, \texttt{fftw}, and optionally \texttt{gnuplot} (for plotting), \texttt{ffmpeg}, \texttt{mplayer} (for video generation). Use \\
	{\color{brown}{\texttt{brew install <package>}}} \\
	Sometimes the installed packages cannot be loaded and symlinked properly. This might result in, for example, a complaint that different \texttt{gcc} compilers exist and are in conflict with each other.
	In this case, use \\
		{\color{brown}{\texttt{brew link --overwrite <package>}}} \\
	%There is an option of an automatic check for the packages at the very beginning of the MCTDH-X installation program.
\end{itemize}

Please also be informed that
\begin{itemize}
	%\item To properly activate \texttt{gcc} in MacOS, \texttt{clang} is used. This is already taken care of by the installation program.
	\item There can also be potential conflicts with Anaconda, so make sure to \\
	{\color{brown}{\texttt{conda deactivate}}}.
	\item There is a strict compatibility relation between \texttt{gcc} and \texttt{OpenBLAS}. For example, with \texttt{OpenBLAS} version \texttt{0.3.23}, \texttt{gcc@12} is recommended. Fortunately, this compatibility issue is usually taken care of by HomeBrew. %If you decide to use the OpenBLAS package shipped together with MCTDH-X, you should be careful of this.
\end{itemize}

\subsubsection{OpenBLAS and FFTW}

With MacOS, you should always use the OpenBLAS and FFTW pre-installed by HomeBrew. More specifically, you should

\begin{enumerate}
	\item Pre-install these two packages by HomeBrew, \\
	{\color{brown}{\texttt{brew install openblas}}}, \\
	{\color{brown}{\texttt{brew install fftw}}}.
	\item Note down their path by \\
	{\color{brown}{\texttt{brew list openblas}}}, \\
	{\color{brown}{\texttt{brew list fftw}}}. \\
	The paths should share a common prefix like \texttt{/usr/local/Cellar/}.
	\item In the file \texttt{mctdh-x/External\_Software/External\_Software.cmake}, set {\color{brown}{\texttt{FFTWEXTERN FALSE}}} and {\color{brown}{\texttt{OpenBLASEXTERN FALSE}}}. 
	\item In the same file, modify the paths according to the results you obtain in step 2. In most cases, you should only need to modify the prefix and the version number. Note that lines beginning with a hashtag are comments, so make sure to remove the hashtag for the correct line.
\end{enumerate}

\subsection{Installation}

Go to the main MCTDH-X folder \texttt{/mctdh-x}, and run the following command: \\
{\color{brown}\texttt{./Install\_MCTDHX.sh}} 
to initiate the installation. See Fig.~\ref{fig1}. 

In the prompt, you should then choose the option
\texttt{1) FFTW+OpenBlas+GCC+mpif90} by typing ``{\color{brown}1}''. See Fig.~\ref{fig2}.
The program now starts to compile.

Other options are available after choosing ``{\color{brown}3}''. For example if you need MPI for parallelization, you should choose sequentially \texttt{3) Custom}, \texttt{2) GNU}, \texttt{3) FFTW with MPI support}, \texttt{1) openblas}.

Estimated compilation time: 1 minute if you use pre-installed \texttt{OpenBLAS} and \texttt{fftw} packages; 10 minutes if you use packages shipped together with MCTDH-X. 

\textbf{Alternative approach}

In the main MCTDH-X folder \texttt{/mctdh-x}, you can also use the standard \texttt{cmake} installation by the following command: 
{\color{brown}\texttt{cmake .}} and then {\color{brown}\texttt{make -j}}.
The default combination of FFTW+OpenBlas+GCC+mpif90 is automatically chosen in this case.



%\textbf{Note:} If you get a complaint about \texttt{mpi} or \texttt{gfortran}, it is likely that you have two very different versions of \texttt{gfortran} in your computer, which confuses \texttt{cmake}. To fix this, you should specify the path to your \texttt{gfortran} installed by HomeBrew, for example, \\
%{\color{brown}\texttt{cmake -DCMAKE\_Fortran\_COMPILER=/usr/local/Cellar/gcc/13.1.0/bin/gfortran .}} \\
%and then \\
%{\color{brown}\texttt{OMPI\_FC=/usr/local/Cellar/gcc/13.1.0/bin/gfortran make -j}} 



\subsection{Is my compilation successful?}

If the compilation is successful, you should see ``\texttt{[100\%]}'' shown near the end of the prompted messages. See Fig.~\ref{fig3}. You should also find two new executable files \texttt{MCTDHX\_gcc} and \texttt{MCTDHX\_analysis\_gcc} in the folder \texttt{/mctdh-x/bin}. The suffix \texttt{gcc} of the files might change to e.g. \texttt{intel} according to the choice of your compiler.

{\bf IMPORTANT:} The file \texttt{mctdhxlib.so} is now obsolete, though you might see it mentioned in some old tutorials.



{\bf Nota bene:} Whether the compilation is successful or not, it should also make changes in your home folder \texttt{\textasciitilde/}, which you can go to by the command {\color{brown}\texttt{cd}}. These files include a new file called \texttt{.mctdhxrc}, and several new lines in the end of the file \texttt{.bash\_profile}. These changes enable the following new aliases:
\begin{itemize}
	\item \texttt{MCTDHX} and \texttt{MCTDHX\_analysis}: Run the main program and the analysis program, respectively.
	\item \texttt{bincp} and \texttt{inpcp}: Copy the executable files and the  example input files from the folders \texttt{mctdh-x-releases-release/bin} and  \texttt{mctdh-x-releases-release/Input\_Examples}, respectively.
	\item \texttt{cdm}: Go to the folder  \texttt{/mctdh-x-releases-release}.
	\item \texttt{x-make}: Recompilation of the program. Can be useful after changes are made in the source files.
	\item \texttt{x-clean} and \texttt{x-purge}: Remove the compiled files.
\end{itemize}

If your compilation is unsuccessful, please try to run {\color{brown}\texttt{x-make}} first.
Also note that if something goes wrong during the CMake compilation and the installation is interrupted with errors, after your fixes (e.g. putting in the correct path in \texttt{ExternalSoftware.cmake}) you might need to clear the CMake cache files when you try to compile again. 
You can do so by typing \texttt{rm -rf CMakeCache.txt CMakeFiles}.





\newpage
\section{Windows}

\subsection{Prerequisite and Pre-installation setup}

MCTDH-X is currently not supported on Windows, i.e. it cannot run \emph{directly} on a Windows architecture.
However, you can still run MCTDH-X on a Windows machine if you create a virtual environment, e.g. hosting Linux.
The easiest way to do this is by using VirtualBox (\url{https://www.virtualbox.org}). 
VirtualBox is a general-purpose full virtualizer for x86 hardware, which allows you to run Linux, Mac OS and Windows on a different operating system (in this case we want Linux on Windows).
Before proceeding, make sure that you have \texttt{python3} and \texttt{win32api} installed.
Python can be installed on Windows directly by downloading the installer from \url{https://www.python.org/downloads/windows/}.
When you run the installer, make sure to select ``add python.exe to PATH'', otherwise you will need to provide the path to python manually.
After you have installed \texttt{python3}, run: \\

\noindent
{\color{black} \texttt{pip install pywin32}} \\

\noindent
\texttt{pip} is a python package installation manager that should have been installed automatically together with \texttt{python3}.

Now you can install VirtualBox and then install Ubuntu on it.
You can follow these steps:

\begin{enumerate}
\item Download an Ubuntu image, for instance from here: \url{https://ubuntu.com/download/desktop}. This installation guide has been tested with version 22.04.2.
\item Download and install VirtualBox from here: \url{https://www.virtualbox.org/wiki/Downloads}. This page includes instructions on how to install VirtualBox so we won't repeat them here. 
\item Once you have completed the installation, go ahead and run VirtualBox.
\item Click on the green plus icon to add a new virtual machine. You will be prompted to a window like the one in Fig.~\ref{fig5}. You need to give a name to the system, specify in which folder you want to install it, and provide the Ubuntu image (.iso file). Make sure also to tick the option ``Skip Unattended Installation". If you do not do this, VirtualBox will install Ubuntu with a user without sudo privileges which will cause problems when you want to run things in the terminal.


\begin{figure}[t]
	\centering
	\includegraphics[width=0.7\columnwidth]{screenshot5.png}
	\caption{
		The first configuration window when installing Ubuntu on VirtualBox.
	}
	\label{fig5}
\end{figure}


\item Don't worry if the resolution in the VirtualBox window is low. By default it will be 800x600, but you will be able to change it once the installation is completed.
\item If you move forward with the installation, you will be asked to provide specifications for the amount of RAM, the number of CPUs and the storage space you want to allocate for your Ubuntu system. The following are the recommended minimal requirements:
\begin{itemize}
\item \textbf{RAM}: 8GB
\item \textbf{CPUs}: 4.
\item \textbf{Storage}: virtual hard disk with 25GB.
\end{itemize}
\item Once your virtual machine is fully configured you can launch it by clicking on the green arrow pointing right (Start). The .iso image is mounted and the Ubuntu installation begins. When prompted, select ``Install Ubuntu", ``normal installation" and ``erase disk and install Ubuntu".
\item Follow the installation steps. You will need to provide a username and password, and information about language, keyboard, time zone etc.
\item The installer will now work on its own. Once it's done, you will be asked to restart the newly installed Ubuntu system.
\item At the end you should see a window like in Fig.~\ref{fig6}. Simply press enter and the Ubuntu desktop will appear. 


\begin{figure}[t]
	\centering
	\includegraphics[width=0.7\columnwidth]{screenshot6.png}
	\caption{
		At the end of the installation you should see the Ubuntu logo.
	}
	\label{fig6}
\end{figure}



\item Open the terminal and make sure to run  \\

\noindent
{\color{brown} \texttt{sudo apt update \&\& sudo apt upgrade -y}} \\
{\color{brown} \texttt{sudo snap refresh}} \\

\noindent
to get everything updated to the latest versions and \\

\noindent 
{\color{brown} \texttt{sudo apt-get install build-essential}} \\

\noindent 
to install the correct compilers.
\end{enumerate}


\subsection{Installation}

Now that you have a working Ubuntu system installed on your VirtualBox, you can proceed with the MCTDH-X installation on Linux as outlined in section \ref{sec:linux-install}.
First install all the required packages, download the MCTDH-X source from the repository, and install with \texttt{cmake}.





\section{For developers: Avoiding issues with GitLab}

As a developer, we encourage you to regularly pull from the release branch, to avoid the accumulation of conflicts between
your current branch and the branch on GitLab. A pull might however lead to you no longer being able to recompile mctdh-x because
the pull might override the changes you made during the first installation in \texttt{mctdh-x/External\_Software/External\_Software.cmake}.
This means, after you pull, the paths in this file will be reset to standard Linux paths.
You know that you have a problem, if you run x-make in the the folder that contains your
your mctdh-x installation and it stops before reaching 100\% installation. The error could for example be 
"make[2]:  No rule to make target '/usr/lib/x86$\_$64-linux-gnu//libfftw3$\_$omp.a', needed by 'bin/MCTDHX$\_$gcc'.  Stop."
If this happens, open \texttt{mctdh-x/External\_Software/External\_Software.cmake}. 
First try to comment out the Linux files and comment in on of the other options we have provided, based on your operating system and architecture in the 4 following instances:
\begin{itemize}
	\item under \texttt{set(FFTW$\_$INCLUDE$\_$DIR} comment out '/usr/include/' and comment in whichever line corresponds to your operating system (MacOS etc)
	\item under \texttt{SET(FFTW$\_$LIB} comment out '/usr/lib/x86$\_$64-linux-gnu/' and comment in whichever line corresponds to your operating system (MacOS etc)
	\item under \texttt{set(OpenBLAS$\_$INCLUDE$\_$DIR} comment out '/usr/include/x86$\_$64-linux-gnu/' and comment in whichever line corresponds to your operating system (MacOS etc)
	\item under \texttt{SET(OpenBLAS$\_$LIB} comment out '/usr/lib/x86$\_$64-linux-gnu/' and comment in whichever line corresponds to your operating system (MacOS etc)
\end{itemize}
If this doesn't work, you will need to locate where FFTW and OpenBLAS are located on your machine, copy
the paths and insert them into the locations mentioned above.




\section{First simulation}

Now you can make your very first simulation, to confirm that the compilation is indeed correct. The following procedures are the same on any operating system.

\begin{enumerate}
	\item Re-open all your terminals.
	\item Create and go to a new directory.
	\item Use {\color{brown}\texttt{bincp}} and {\color{brown}\texttt{inpcp}} to copy the executable files and input files. Now your folder should contain the following files: \texttt{MCTDHX.inp}, \texttt{MCTDHX\_analysis\_gcc}, \texttt{MCTDHX\_gcc}, \texttt{ParameterScan.inp}, \texttt{ParameterScan\_Relaxation.inp}, \texttt{analysis.inp}.
	\item Run the program by the command {\color{brown}\texttt{MCTDHX\_gcc}}. The simulation time should take less than 1 minute. A series of files \texttt{????0000000coef.dat}, \texttt{????0000000orbs.dat}, \texttt{NO\_PR.out}, etc. are created, where ???? indicates the simulation time, and goes from 0.0 to 20.0.
	\item If you now check the \texttt{NO\_PR.out} file by the command {\color{brown}\texttt{tail -n1 NO\_PR.out}}, you will get something similar to the following output:\\ \texttt{20.00000000000031           0.3792313893129608E-03       0.2115264439230076E-02   \\    0.1138609371624768E-01       0.9861194104552091            1.323956364795208} \\
	Depending on your operating system, machine precision etc. you might get slightly different results, but if your results agree with the ones above up to 8 decimal digits everything should have run correctly.
	\item If you now use gnuplot to plot {\color{brown}\texttt{plot "20.0000000orbs.dat" u 1:6 w l}}, you will get the result as shown in Fig.~\ref{fig4}.
\end{enumerate}


\begin{figure}[t]
	\centering
	\includegraphics[width=0.7\columnwidth]{screenshot4.png}
	\caption{
		Test simulation result.
	}
	\label{fig4}
\end{figure}


\end{document}


