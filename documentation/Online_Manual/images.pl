# LaTeX2HTML 2008 (1.71)
# Associate images original text with physical files.


$key = q/phi^{(NO)}_1(x,y,z;t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="126" HEIGHT="47" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img149.png"
 ALT="$ \phi ^{(NO)}_1(x,y,z;t)$">|; 

$key = q/6+N_l+N_{CI};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="113" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img163.png"
 ALT="$ 6+N_l+N_{CI}$">|; 

$key = q/E_{tot}(t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="57" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img125.png"
 ALT="$ E_{tot}(t)$">|; 

$key = q/N_{shots};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="52" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img365.png"
 ALT="$ N_{shots}$">|; 

$key = q/p_{1-5}equiv;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="59" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img237.png"
 ALT="$ p_{1-5}\equiv$">|; 

$key = q/xi_M(x,y,z;t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="105" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img376.png"
 ALT="$ \xi _M(x,y,z;t)$">|; 

$key = q/phi_1^{(NO)}(x);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="75" HEIGHT="47" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img2.png"
 ALT="$ \phi_1^{(NO)}(x)$">|; 

$key = q/N=2;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img8.png"
 ALT="$ N=2$">|; 

$key = q/W(vec{r},vec{r}')=I_1left(frac{sigma}{I_2^12}-frac{sigma}{I_2^6}right);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="202" HEIGHT="52" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img271.png"
 ALT="$ W(\vec{r},\vec{r}') = I_1 \left( \frac{\sigma}{I_2^12} - \frac{\sigma}{I_2^6} \right)$">|; 

$key = q/2cdot;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="19" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img150.png"
 ALT="$ 2 \cdot$">|; 

$key = q/(9+3M);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="81" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img373.png"
 ALT="$ (9+3M)$">|; 

$key = q/V(x)={cases}p_1sin(p_2x)^2&p_3<x<p_41000&text{else}{cases};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="294" HEIGHT="75" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img223.png"
 ALT="$ V(x)= \begin{cases}
p_1\sin(p_2 x)^2 &amp; p_3 &lt; x &lt; p_4 \\\\
1000 &amp; \text{else}
\end{cases}$">|; 

$key = q/T_{Orb};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="39" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img132.png"
 ALT="$ T_{Orb}$">|; 

$key = q/k_x,k_y,k_z;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="75" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img278.png"
 ALT="$ k_x,k_y,k_z$">|; 

$key = q/phi^{(NO),i}_M(x,y,z;t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="135" HEIGHT="47" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img193.png"
 ALT="$ \phi^{(NO),i}_M(x,y,z;t)$">|; 

$key = q/rho^{(2)}(vec{r}_1,-vec{r}_1;t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="117" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img362.png"
 ALT="$ \rho^{(2)}(\vec{r}_1, -\vec{r}_1;t)$">|; 

$key = q/g^{(1)}_i;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="32" HEIGHT="47" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img310.png"
 ALT="$ g^{(1)}_i$">|; 

$key = q/p_{1slash2slash3}equiv;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="69" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img203.png"
 ALT="$ p_{1/2/3}\equiv$">|; 

$key = q/P_{not};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="37" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img82.png"
 ALT="$ P_{not}$">|; 

$key = q/10;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="23" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img85.png"
 ALT="$ 10$">|; 

$key = q/rho^{(2)}(vec{r}_1=vec{r}'_1=vec{R},vec{r}_2=vec{r}'_2=vec{r});MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="243" HEIGHT="45" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img346.png"
 ALT="$ \rho^{(2)}(\vec{r}_1=\vec{r}'_1=\vec{R},\vec{r}_2=\vec{r}'_2=\vec{r})$">|; 

$key = q/E_{V_t}(t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="54" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img126.png"
 ALT="$ E_{V_t}(t)$">|; 

$key = q/3+N_l;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="57" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img277.png"
 ALT="$ 3+N_l$">|; 

$key = q/V_{jk};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="30" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img66.png"
 ALT="$ V_{jk}$">|; 

$key = q/epsilon;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img78.png"
 ALT="$ \epsilon$">|; 

$key = q/N_{samples};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="69" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img366.png"
 ALT="$ N_{samples}$">|; 

$key = q/E_{orb,rel}(t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="80" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img122.png"
 ALT="$ E_{orb,rel}(t)$">|; 

$key = q/nabla_xxi_1(x,y,z;t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="121" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img379.png"
 ALT="$ \nabla _x \xi _1(x,y,z;t)$">|; 

$key = q/rho^{order}(lbracek_{x,ref},k_{y,ref},k_{z,ref}rbrace,x,y,z;t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="295" HEIGHT="39" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img336.png"
 ALT="$ \rho^{order}(\lbrace k_{x,ref},k_{y,ref},k_{z,ref}\rbrace,x,y,z;t)$">|; 

$key = q/V(x)=frac12p_1^2((x-p_2)^2+y^2);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="226" HEIGHT="39" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img214.png"
 ALT="$ V(x)= \frac12 p_1^2 ( (x-p_2)^2 + y^2)$">|; 

$key = q/p_{1slash2}equiv;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="55" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img199.png"
 ALT="$ p_{1/2}\equiv$">|; 

$key = q/(8+3M);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="81" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img372.png"
 ALT="$ (8+3M)$">|; 

$key = q/nabla_xxi_{avg}(x,y,z;t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="136" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img377.png"
 ALT="$ \nabla _x \xi _{avg}(x,y,z;t)$">|; 

$key = q/1.d-12;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="71" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img42.png"
 ALT="$ 1.d-12$">|; 

$key = q/4+5N_l+N_{CI}+4MN_l;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="195" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img181.png"
 ALT="$ 4+5N_l+N_{CI}+ 4 M N_l$">|; 

$key = q/S^N_C(t)=sum_{vec{n},vec{n}'}-vertC_{vec{n}}(t)vert^2ln[vertC_{vec{n}'}(t)vert^2];MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="296" HEIGHT="39" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img289.png"
 ALT="$ S^N_C(t)= \sum_{\vec{n},\vec{n}'} - \vert C_{\vec{n}}(t) \vert^2 ln [\vert C_{\vec{n}'}(t) \vert^2 ]$">|; 

$key = q/g^{(2)}(vec{r}_1,-vec{r}_1;t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="117" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img93.png"
 ALT="$ g^{(2)}(\vec{r}_1, - \vec{r}_1;t)$">|; 

$key = q/p_aequiv;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="42" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img251.png"
 ALT="$ p_a\equiv$">|; 

$key = q/g^{(2)};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="32" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img324.png"
 ALT="$ g^{(2)}$">|; 

$key = q/(5+M);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="72" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img367.png"
 ALT="$ (5+M)$">|; 

$key = q/nabla_yxi_1(x,y,z;t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="121" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img380.png"
 ALT="$ \nabla _y \xi _1(x,y,z;t)$">|; 

$key = q/rho(k_{x,ref},k_{y,ref},k_{z,ref};t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="187" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img335.png"
 ALT="$ \rho(k_{x,ref},k_{y,ref},k_{z,ref};t)$">|; 

$key = q/rho_i^{(1)}(k_x,k_y,k_z|k'_x,k'_y,k'_z;t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="207" HEIGHT="47" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img304.png"
 ALT="$ \rho_i^{(1)}(k_x,k_y,k_z\vert k'_x,k'_y,k'_z;t)$">|; 

$key = q/6;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img118.png"
 ALT="$ 6$">|; 

$key = q/N_l;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img160.png"
 ALT="$ N_l$">|; 

$key = q/V(x,y,z)=;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="101" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img201.png"
 ALT="$ V(x,y,z)=$">|; 

$key = q/M=4;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="59" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img9.png"
 ALT="$ M=4$">|; 

$key = q/rho^{(1)}(vec{r}_1|-vec{r}_1;t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="123" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img360.png"
 ALT="$ \rho^{(1)}(\vec{r}_1 \vert -\vec{r}_1;t) $">|; 

$key = q/rho(k,t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="54" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img108.png"
 ALT="$ \rho(k,t)$">|; 

$key = q/ln[rho^{(2)}(vec{k}_1,vec{k}_2;t)];MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="133" HEIGHT="45" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img293.png"
 ALT="$ ln [\rho^{(2)}(\vec{k}_1,\vec{k}_2;t)]$">|; 

$key = q/hat{W}_{sl};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="34" HEIGHT="44" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img48.png"
 ALT="$ \hat{W}_{sl}$">|; 

$key = q/D=1;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="55" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img80.png"
 ALT="$ D=1$">|; 

$key = q/^o;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img120.png"
 ALT="$ ^o$">|; 

$key = q/x,x';MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="39" HEIGHT="36" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img326.png"
 ALT="$ x,x'$">|; 

$key = q/6+5N_l+N_{CI}+2MN_l;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="195" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img179.png"
 ALT="$ 6+5N_l+N_{CI}+ 2 M N_l$">|; 

$key = q/k_{x,ref};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="47" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img341.png"
 ALT="$ k_{x,ref}$">|; 

$key = q/0.01;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="38" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img40.png"
 ALT="$ 0.01$">|; 

$key = q/3+5N_l+N_{CI}+4MN_l;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="195" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img180.png"
 ALT="$ 3+5N_l+N_{CI}+4 M N_l$">|; 

$key = q/W(vec{r},vec{r}')=lambda_0frac{1}{(sqrt{2pi(sigma^2+I_1sin(I_2t))})^D};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="265" HEIGHT="40" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img266.png"
 ALT="$ W(\vec{r},\vec{r}')= \lambda_0 \frac{1}{(\sqrt{2\pi (\sigma^2+ I_1 \sin(I_2 t))})^D}$">|; 

$key = q/hat{H}vertPsirangle=ipartial_tvertPsirangle;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="123" HEIGHT="44" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img17.png"
 ALT="$ \hat{H}\vert \Psi \rangle = i \partial_t \vert \Psi \rangle$">|; 

$key = q/z_{ref};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="35" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img340.png"
 ALT="$ z_{ref}$">|; 

$key = q/G=1+Nmathcal{F};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="110" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img348.png"
 ALT="$ G= 1 + N \mathcal{F}$">|; 

$key = q/rho^{(2)}(vec{r}_1|vec{r}_2;t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="100" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img358.png"
 ALT="$ \rho^{(2)}(\vec{r}_1 \vert \vec{r}_2;t)$">|; 

$key = q/W(vec{r},vec{r}')=I_1left(frac{sigma}{vertvec{r}-vec{r}vert^12}-frac{sigma}{vertvec{r}-vec{r}vert^6}right);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="250" HEIGHT="52" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img269.png"
 ALT="$ W(\vec{r},\vec{r}') = I_1 \left( \frac{\sigma}{\vert \vec{r}-\vec{r} \vert^12} - \frac{\sigma}{\vert \vec{r}-\vec{r} \vert^6} \right)$">|; 

$key = q/(8+M);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="72" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img370.png"
 ALT="$ (8+M)$">|; 

$key = q/N;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="22" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img84.png"
 ALT="$ N$">|; 

$key = q/1.d99;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="18" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img37.png"
 ALT="$ 1.d99$">|; 

$key = q/xi=x;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="50" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img101.png"
 ALT="$ \xi=x$">|; 

$key = q/9+5(i-1);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="100" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img297.png"
 ALT="$ 9+5(i-1)$">|; 

$key = q/xslashyslashz;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="53" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img204.png"
 ALT="$ x/y/z$">|; 

$key = q/rho(k';t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="59" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img333.png"
 ALT="$ \rho(k';t)$">|; 

$key = q/hat{H};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="22" HEIGHT="22" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img18.png"
 ALT="$ \hat{H}$">|; 

$key = q/rho(-vec{r}_1;t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="75" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img361.png"
 ALT="$ \rho(-\vec{r}_1;t)$">|; 

$key = q/k=M,..,1;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="96" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img186.png"
 ALT="$ k=M,..,1$">|; 

$key = q/^*;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img7.png"
 ALT="$ ^*$">|; 

$key = q/k=1,..,M;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="97" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img184.png"
 ALT="$ k=1,..,M$">|; 

$key = q/E_{orb}(t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="59" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img121.png"
 ALT="$ E_{orb}(t)$">|; 

$key = q/rho(x_{ref},y_{ref},z_{ref};t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="154" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img334.png"
 ALT="$ \rho(x_{ref},y_{ref},z_{ref};t)$">|; 

$key = q/7;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img45.png"
 ALT="$ 7$">|; 

$key = q/p_{6}equiv;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="42" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img219.png"
 ALT="$ p_{6}\equiv$">|; 

$key = q/(1+M);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="72" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img111.png"
 ALT="$ (1+M)$">|; 

$key = q/3+3N_l+N_{CI};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="123" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img164.png"
 ALT="$ 3+3N_l+N_{CI}$">|; 

$key = q/vec{r}_{order};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="47" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img88.png"
 ALT="$ \vec{r}_{order}$">|; 

$key = q/5+N_l+N_{CI};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="113" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img162.png"
 ALT="$ 5+N_l+N_{CI}$">|; 

$key = q/1-sum_{i=2}^Mrho_i^{(NO)}(t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="148" HEIGHT="47" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img16.png"
 ALT="$ 1-\sum_{i=2}^M \rho_i^{(NO)}(t)$">|; 

$key = q/I_X;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="26" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img274.png"
 ALT="$ I_X$">|; 

$key = q/W(vec{r},vec{r}')=lambda_0cos(I_1t)delta(vec{r}-vec{r}');MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="248" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img261.png"
 ALT="$ W(\vec{r},\vec{r}')= \lambda_0 \cos (I_1 t) \delta(\vec{r}-\vec{r}')$">|; 

$key = q/vertPsirangle;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="32" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img19.png"
 ALT="$ \vert \Psi \rangle$">|; 

$key = q/(11+2M+1);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="123" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img144.png"
 ALT="$ (11+2M+1)$">|; 

$key = q/V_{m_F}(x)=frac{1}{2}p_1^2x^2+m_Fp_2vertxvert;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="223" HEIGHT="39" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img74.png"
 ALT="$ V_{m_F}(x) = \frac{1}{2} p_1^2 x^2 + m_F p_2 \vert x \vert$">|; 

$key = q/p_3equiv;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="42" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img208.png"
 ALT="$ p_3\equiv$">|; 

$key = q/k_{z,ref};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="46" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img343.png"
 ALT="$ k_{z,ref}$">|; 

$key = q/p_{4slash5}equiv;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="55" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img212.png"
 ALT="$ p_{4/5}\equiv$">|; 

$key = q/rho^{(NO)}_M(t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="70" HEIGHT="47" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img113.png"
 ALT="$ \rho^{(NO)}_M(t)$">|; 

$key = q/t=0;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="46" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img265.png"
 ALT="$ t=0$">|; 

$key = q/omega_p;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="24" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img79.png"
 ALT="$ \omega_p$">|; 

$key = q/10000;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="51" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img86.png"
 ALT="$ 10000$">|; 

$key = q/5+5N_l+N_{CI};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="123" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img174.png"
 ALT="$ 5+5N_l+N_{CI}$">|; 

$key = q/t;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img11.png"
 ALT="$ t$">|; 

$key = q/(9+M);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="72" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img371.png"
 ALT="$ (9+M)$">|; 

$key = q/20.0;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="38" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img36.png"
 ALT="$ 20.0$">|; 

$key = q/(10+2M);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="91" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img136.png"
 ALT="$ (10+2M)$">|; 

$key = q/y;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="15" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img102.png"
 ALT="$ y$">|; 

$key = q/mathaccentV{hat}05E{W};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="23" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img5.png"
 ALT="$ \mathaccentV {hat}05E{W}$">|; 

$key = q/V_{eff}=intdxisum_{ij}rho_{ij}phi_i^*(x,y,t)phi_j(x,y,t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="311" HEIGHT="39" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img100.png"
 ALT="$ V_{eff}= \int d\xi \sum_{ij} \rho_{ij} \phi_i^*(x,y,t) \phi_j(x,y,t)$">|; 

$key = q/3;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img116.png"
 ALT="$ 3$">|; 

$key = q/4+5N_l+N_{CI}+2MN_l;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="195" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img177.png"
 ALT="$ 4+5N_l+N_{CI}+ 2 M N_l$">|; 

$key = q/vec{r}_{order-1};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="64" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img89.png"
 ALT="$ \vec{r}_{order-1}$">|; 

$key = q/V(x)=frac{1}{2}(1+sin(t)cos(2t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="224" HEIGHT="39" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img225.png"
 ALT="$ V(x)=\frac{1}{2}(1+\sin(t) \cos(2t)$">|; 

$key = q/x_s;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="22" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img282.png"
 ALT="$ x_s$">|; 

$key = q/(6+M);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="72" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img368.png"
 ALT="$ (6+M)$">|; 

$key = q/rho^{(NO)}_1(t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="70" HEIGHT="47" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img114.png"
 ALT="$ \rho^{(NO)}_1(t)$">|; 

$key = q/A=1-frac{1}{4}p_1;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="104" HEIGHT="39" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img229.png"
 ALT="$ A=1-\frac{1}{4} p_1$">|; 

$key = q/rho(x,y,z;t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="91" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img315.png"
 ALT="$ \rho (x,y,z;t)$">|; 

$key = q/rho_i(k'_x,k'_y,k'_z;t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="119" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img306.png"
 ALT="$ \rho_i(k'_x,k'_y,k'_z;t)$">|; 

$key = q/y_{ref};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="35" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img339.png"
 ALT="$ y_{ref}$">|; 

$key = q/rho^i(x,y,z;t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="96" HEIGHT="39" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img279.png"
 ALT="$ \rho^i(x,y,z;t)$">|; 

$key = q/=sigma;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="36" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img256.png"
 ALT="$ =\sigma$">|; 

$key = q/3+5N_l+N_{CI};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="123" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img168.png"
 ALT="$ 3+5N_l+N_{CI}$">|; 

$key = q/S_{rho^{(2)}-r}(t)=-intdvec{r}_1dvec{r}_2rho^{(2)}(vec{r}_1,vec{r}_2;t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="286" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img290.png"
 ALT="$ S_{\rho^{(2)}-r}(t)=- \int d\vec{r}_1 d\vec{r}_2\rho^{(2)}(\vec{r}_1,\vec{r}_2;t)$">|; 

$key = q/V(x)=0qquadforall[x>20];MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="192" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img245.png"
 ALT="$ V(x)=0 \qquad \forall [x&gt;20]$">|; 

$key = q/V(x,y)=;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="83" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img197.png"
 ALT="$ V(x,y)=$">|; 

$key = q/p_4;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="22" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img73.png"
 ALT="$ p_4$">|; 

$key = q/epsilonsin(omega_pt);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="79" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img77.png"
 ALT="$ \epsilon \sin(\omega_p t )$">|; 

$key = q/E_{tot}(t)=E_{orb}(t)+E_{CI}(t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="213" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img127.png"
 ALT="$ E_{tot}(t)=E_{orb}(t) + E_{CI}(t)$">|; 

$key = q/6+3N_l+N_{CI};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="123" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img167.png"
 ALT="$ 6+3N_l+N_{CI}$">|; 

$key = q/rho_i(k_x,k_y,k_z;t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="119" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img302.png"
 ALT="$ \rho_i(k_x,k_y,k_z;t)$">|; 

$key = q/2pi;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img32.png"
 ALT="$ 2\pi$">|; 

$key = q/I=frac{1}{sum_{vec{n}}vertC_{vec{n}}(t)vert^4};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="115" HEIGHT="39" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img288.png"
 ALT="$ I=\frac{1}{\sum_{\vec{n}} \vert C_{\vec{n}}(t) \vert^4}$">|; 

$key = q/hat{W};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="23" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img22.png"
 ALT="$ \hat{W}$">|; 

$key = q/11+5(i-1);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="110" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img307.png"
 ALT="$ 11+5(i-1)$">|; 

$key = q/rho_{(NO)}(x,y,z;t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="125" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img141.png"
 ALT="$ \rho _{(NO)}(x,y,z;t)$">|; 

$key = q/rho(x';t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="59" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img332.png"
 ALT="$ \rho(x';t)$">|; 

$key = q/p_2;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="22" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img76.png"
 ALT="$ p_2$">|; 

$key = q/vec{r}_2=vec{r}'_2=vec{r}-1);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="136" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img350.png"
 ALT="$ \vec{r}_2=\vec{r}'_2=\vec{r}-1)$">|; 

$key = q/p_5equiv;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="42" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img217.png"
 ALT="$ p_5\equiv$">|; 

$key = q/rho(x',y',z';t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="104" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img319.png"
 ALT="$ \rho(x',y',z';t)$">|; 

$key = q/20;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="23" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img44.png"
 ALT="$ 20$">|; 

$key = q/(11+4M+1);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="123" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img146.png"
 ALT="$ (11+4M+1)$">|; 

$key = q/100;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="33" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img109.png"
 ALT="$ 100$">|; 

$key = q/phi^{(NO),i}_k(x,y,z;t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="135" HEIGHT="47" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img185.png"
 ALT="$ \phi^{(NO),i}_k(x,y,z;t)$">|; 

$key = q/rho^{(1slash2)}(k|k';t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="106" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img329.png"
 ALT="$ \rho^{(1/2)}(k\vert k';t)$">|; 

$key = q/phi^{(NO)}_M(x,y,z;t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="126" HEIGHT="47" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img148.png"
 ALT="$ \phi ^{(NO)}_M(x,y,z;t)$">|; 

$key = q/12;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="23" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img345.png"
 ALT="$ 12$">|; 

$key = q/neq;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="19" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img31.png"
 ALT="$ \neq$">|; 

$key = q/g^{(2)}_i;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="32" HEIGHT="47" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img311.png"
 ALT="$ g^{(2)}_i$">|; 

$key = q/V(x,y,z,t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="96" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img139.png"
 ALT="$ V(x,y,z,t)$">|; 

$key = q/delta;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="18" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img49.png"
 ALT="$ \delta$">|; 

$key = q/V_{jk}(vec{r});MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="54" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img65.png"
 ALT="$ V_{jk}(\vec{r})$">|; 

$key = q/W(vec{r},vec{r}')=lambda_0(vec{r}-vec{r}')^2;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="182" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img273.png"
 ALT="$ W(\vec{r},\vec{r}')= \lambda_0 (\vec{r}-\vec{r}')^2$">|; 

$key = q/V(x)=frac12(x-p_1)^2;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="154" HEIGHT="39" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img205.png"
 ALT="$ V(x)= \frac12 (x-p_1)^2$">|; 

$key = q/P_2^0(t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img353.png"
 ALT="$ P_2^0(t)$">|; 

$key = q/V_{text{las}}(x,t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="73" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img235.png"
 ALT="$ V_{\text{las}}(x,t)$">|; 

$key = q/(9+2M);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="81" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img153.png"
 ALT="$ (9+2M)$">|; 

$key = q/rho_w(x,y,z;t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="102" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img140.png"
 ALT="$ \rho _w(x,y,z;t)$">|; 

$key = q/S_C(t)=sum_{vec{n}}-vertC_{vec{n}}(t)vert^2ln[vertC_{vec{n}}(t)vert^2];MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="272" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img286.png"
 ALT="$ S_C(t)= \sum_{\vec{n}} - \vert C_{\vec{n}}(t) \vert^2 ln [\vert C_{\vec{n}}(t) \vert^2 ]$">|; 

$key = q/I_2;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="21" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img268.png"
 ALT="$ I_2$">|; 

$key = q/p_{1slash3}equiv;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="55" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img248.png"
 ALT="$ p_{1/3}\equiv$">|; 

$key = q/4+3N_l+N_{CI};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="123" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img165.png"
 ALT="$ 4+3N_l+N_{CI}$">|; 

$key = q/|g^{(2)};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="38" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img107.png"
 ALT="$ \vert g^{(2)}$">|; 

$key = q/displaystylehat{h}_{SO}=gammaleft[alphaleft(hat{p}_xmathbf{S}^y-hat{p}_ymathbf{Sbetaleft(hat{p}_xmathbf{S}^y+hat{p}_ymathbf{S}^xright)right].;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="373" HEIGHT="45" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img63.png"
 ALT="$\displaystyle \hat{h}_{SO}= \gamma \left[ \alpha \left( \hat{p}_x \mathbf{S}^y ...
...+ \beta \left( \hat{p}_x \mathbf{S}^y + \hat{p}_y \mathbf{S}^x \right) \right].$">|; 

$key = q/P_1^1(t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img352.png"
 ALT="$ P_1^1(t)$">|; 

$key = q/p_{1}equiv;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="42" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img216.png"
 ALT="$ p_{1}\equiv$">|; 

$key = q/V(x)=p_1cos(p_2x)+p_3cos(p_4x);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="268" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img247.png"
 ALT="$ V(x)=p_1 \cos(p_2 x) + p_3 \cos(p_4 x)$">|; 

$key = q/displaystylevertPsi(t)rangle=sum_{lbracevec{n}rbrace}C_{vec{n}}(t)vertvec{n};trangle,;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="197" HEIGHT="65" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img26.png"
 ALT="$\displaystyle \vert \Psi (t) \rangle = \sum_{\lbrace \vec{n} \rbrace} C_{\vec{n}}(t) \vert \vec{n};t\rangle,$">|; 

$key = q/tau=frac{langlex_1x_2rangle-langlexrangle^2}{langlex^2rangle-langlexrangle^2};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="118" HEIGHT="48" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img344.png"
 ALT="$ \tau=\frac{\langle x_1 x_2 \rangle-\langle x\rangle^2}{\langle x^2 \rangle-\langle x\rangle^2}$">|; 

$key = q/mathaccentV{hat}05E{V};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="20" HEIGHT="23" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img4.png"
 ALT="$ \mathaccentV {hat}05E{V}$">|; 

$key = q/times;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="20" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img314.png"
 ALT="$ \times $">|; 

$key = q/4+5N_l+N_{CI};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="123" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img169.png"
 ALT="$ 4+5N_l+N_{CI}$">|; 

$key = q/rho^i(k_x,k_y,k_z;t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="119" HEIGHT="39" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img280.png"
 ALT="$ \rho^i(k_x,k_y,k_z;t)$">|; 

$key = q/N_{CI};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="38" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img187.png"
 ALT="$ N_{CI}$">|; 

$key = q/hat{T};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="18" HEIGHT="22" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img20.png"
 ALT="$ \hat{T}$">|; 

$key = q/x_e;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="22" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img283.png"
 ALT="$ x_e$">|; 

$key = q/hat{W}_{mathrm{spin}};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="47" HEIGHT="44" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img51.png"
 ALT="$ \hat{W}_{\mathrm{spin}}$">|; 

$key = q/P_0^2(t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img351.png"
 ALT="$ P_0^2(t)$">|; 

$key = q/xi=y;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="49" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img103.png"
 ALT="$ \xi=y$">|; 

$key = q/xi_1(x,y,z;t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="97" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img375.png"
 ALT="$ \xi _1(x,y,z;t)$">|; 

$key = q/rho^{(1)}(k_x,k_y,k_z|k'_x,k'_y,k'_z;t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="207" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img318.png"
 ALT="$ \rho^{(1)}(k_x,k_y,k_z\vert k'_x,k'_y,k'_z;t)$">|; 

$key = q/nu=x,y,z;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="87" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img61.png"
 ALT="$ \nu=x,y,z$">|; 

$key = q/T_{rho};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="23" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img130.png"
 ALT="$ T_{\rho }$">|; 

$key = q/T_{CI}=T_{rho}+T_{CI,Func};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="166" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img133.png"
 ALT="$ T_{CI}=T_{\rho }+T_{CI,Func}$">|; 

$key = q/rho^i_{(NO)}(x,y,z;t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="125" HEIGHT="39" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img173.png"
 ALT="$ \rho^i_{(NO)}(x,y,z;t)$">|; 

$key = q/8.0;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="29" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img34.png"
 ALT="$ 8.0$">|; 

$key = q/N=50;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="66" HEIGHT="18" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img10.png"
 ALT="$ N=50$">|; 

$key = q/6cdots9+2M;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="105" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img155.png"
 ALT="$ 6\cdots9+2M$">|; 

$key = q/p_1equiv;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="42" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img196.png"
 ALT="$ p_1\equiv$">|; 

$key = q/0.1;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="29" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img38.png"
 ALT="$ 0.1$">|; 

$key = q/D=5T-24;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="111" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img232.png"
 ALT="$ D=5T-24$">|; 

$key = q/4;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img43.png"
 ALT="$ 4$">|; 

$key = q/k,k';MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="38" HEIGHT="36" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img327.png"
 ALT="$ k,k'$">|; 

$key = q/T_{step};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="41" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img128.png"
 ALT="$ T_{step}$">|; 

$key = q/2;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img30.png"
 ALT="$ 2$">|; 

$key = q/i=1,..,N_l;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="93" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img183.png"
 ALT="$ i=1,..,N_l$">|; 

$key = q/^2;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="20" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img313.png"
 ALT="$ ^2$">|; 

$key = q/6+5N_l+N_{CI};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="123" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img175.png"
 ALT="$ 6+5N_l+N_{CI}$">|; 

$key = q/3+M+N_l;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="100" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img159.png"
 ALT="$ 3+M+N_l$">|; 

$key = q/B=2.25p_1-9.5;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="142" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img230.png"
 ALT="$ B=2.25 p_1 - 9.5$">|; 

$key = q/S_{rho-k}(t)=-intdvec{k}_1dvec{k}_2rho^{(2)}(vec{k}_1,vec{k}_2;t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="277" HEIGHT="45" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img292.png"
 ALT="$ S_{\rho-k}(t)=- \int d\vec{k}_1 d\vec{k}_2\rho^{(2)}(\vec{k}_1,\vec{k}_2;t)$">|; 

$key = q/nabla_yxi_M(x,y,z;t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="128" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img382.png"
 ALT="$ \nabla _y \xi _M(x,y,z;t)$">|; 

$key = q/|g^{(1)}|^2;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="51" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img104.png"
 ALT="$ \vert g^{(1)}\vert^2$">|; 

$key = q/lambda(t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="37" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img260.png"
 ALT="$ \lambda(t)$">|; 

$key = q/V(x)=V(x)=-p_1x+p_2sin(2x)^4+[(x-frac{p_3pi}{2})slash0.7]^{20};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="441" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img222.png"
 ALT="$ V(x) = V(x)=-p_1 x + p_2 \sin(2 x)^4 + [(x-\frac{p_3 \pi}{2})/0.7]^{20}$">|; 

$key = q/1.d-9;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="62" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img41.png"
 ALT="$ 1.d-9$">|; 

$key = q/uparrow,downarrow;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="32" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img71.png"
 ALT="$ \uparrow,\downarrow$">|; 

$key = q/xi_{avg}(x,y,z;t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="111" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img374.png"
 ALT="$ \xi _{avg}(x,y,z;t)$">|; 

$key = q/E(t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="41" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img13.png"
 ALT="$ E(t)$">|; 

$key = q/hat{pi}_{jk};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="30" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img57.png"
 ALT="$ \hat{\pi}_{jk}$">|; 

$key = q/rho^{order}(lbracek_{x,ref},k_{y,ref},k_{z,ref}rbrace,k_{x},k_{y},k_{z};t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="318" HEIGHT="39" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img337.png"
 ALT="$ \rho^{order}(\lbrace k_{x,ref},k_{y,ref},k_{z,ref}\rbrace,k_{x},k_{y},k_{z};t)$">|; 

$key = q/V_{uparrow}(x)=frac{1}{2}p_1^2x^2;;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="124" HEIGHT="39" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img68.png"
 ALT="$ V_{\uparrow}(x)= \frac{1}{2} p_1^2 x^2;$">|; 

$key = q/V(x)=frac12p_1^2x^2;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="116" HEIGHT="39" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img195.png"
 ALT="$ V(x)=\frac12 p_1^2 x^2$">|; 

$key = q/V_{text{las}}(x,t)=p_7cos(p_8t)sin(pifrac{t}{p_6})x;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="261" HEIGHT="39" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img238.png"
 ALT="$ V_{\text{las}}(x,t)= p_7 \cos(p_8 t) \sin(\pi \frac{t}{p_6}) x $">|; 

$key = q/N_{CI}=0;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="73" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img188.png"
 ALT="$ N_{CI}=0$">|; 

$key = q/p_1,p_2;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="47" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img70.png"
 ALT="$ p_1,p_2$">|; 

$key = q/3+N_{shots};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="85" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img364.png"
 ALT="$ 3+N_{shots}$">|; 

$key = q/(11+2M+2);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="123" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img145.png"
 ALT="$ (11+2M+2)$">|; 

$key = q/V(x)=-p_1exp(-p_2(x+frac{1}{2}p_3)^2)-p_4exp(-p_5(x-frac{1}{2}p_3))+V_{text{las}}(x,t)exp(-p_9(x-p_{10})^2);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="695" HEIGHT="40" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img236.png"
 ALT="$ V(x)=-p_1 exp(-p_2(x+\frac{1}{2}p_3)^2) - p_4 exp(-p_5(x-\frac{1}{2}p_3))+V_{\text{las}}(x,t) exp(-p_9 (x-p_{10})^2)$">|; 

$key = q/C=28-6p_1;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="113" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img231.png"
 ALT="$ C=28-6p_1$">|; 

$key = q/alpha,beta,gamma;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img64.png"
 ALT="$ \alpha,\beta,\gamma$">|; 

$key = q/displaystylehat{H}=sum_{i=1}^N(hat{T}_i+hat{V}_i)+sum_{i<j=1}^Nhat{W}_{ij}.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="244" HEIGHT="77" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img23.png"
 ALT="$\displaystyle \hat{H}=\sum_{i=1}^N (\hat{T}_i+\hat{V}_i) + \sum_{i&lt;j=1}^N \hat{W}_{ij}.$">|; 

$key = q/vec{r}'_1;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="21" HEIGHT="36" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img98.png"
 ALT="$ \vec{r}'_1$">|; 

$key = q/rho^{(2)}(x,y,z|x',y',z';t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="174" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img321.png"
 ALT="$ \rho^{(2)}(x,y,z\vert x',y',z';t)$">|; 

$key = q/2+M;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="57" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img112.png"
 ALT="$ 2+M$">|; 

$key = q/V_{text{las}}(x,t)=0;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="107" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img240.png"
 ALT="$ V_{\text{las}}(x,t)=0$">|; 

$key = q/k_{y,ref};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="47" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img342.png"
 ALT="$ k_{y,ref}$">|; 

$key = q/i;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="11" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img189.png"
 ALT="$ i$">|; 

$key = q/rho_M^{(NO)}(t),rho_{M-1}^{(NO)}(t),...,rho_1^{(NO)}(t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="240" HEIGHT="47" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img12.png"
 ALT="$ \rho_M^{(NO)}(t), \rho_{M-1}^{(NO)}(t),...,\rho_1^{(NO)}(t)$">|; 

$key = q/vertvec{r}-vec{r}vertleqI_2;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="98" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img272.png"
 ALT="$ \vert \vec{r}-\vec{r} \vert\leq I_2$">|; 

$key = q/+1;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="29" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img151.png"
 ALT="$ +1$">|; 

$key = q/V^i(x,y,z,t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="102" HEIGHT="39" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img170.png"
 ALT="$ V^i(x,y,z,t)$">|; 

$key = q/nabla_yxi_{avg}(x,y,z;t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="135" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img378.png"
 ALT="$ \nabla _y \xi _{avg}(x,y,z;t)$">|; 

$key = q/M;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img115.png"
 ALT="$ M$">|; 

$key = q/sigma;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img275.png"
 ALT="$ \sigma $">|; 

$key = q/W(vec{r},vec{r}')=lambda_0(1+0.4sin^2(t))(vec{r}-vec{r}')^2;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="307" HEIGHT="40" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img263.png"
 ALT="$ W(\vec{r},\vec{r}')= \lambda_0 (1+0.4 \sin^2(t)) (\vec{r}-\vec{r}')^2$">|; 

$key = q/+A(t)exp(-frac{(x-p_5)^2}{2p_6^2});MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="165" HEIGHT="48" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img215.png"
 ALT="$ + A(t) exp(- \frac{(x-p_5)^2}{2 p_6^2})$">|; 

$key = q/x;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="15" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img211.png"
 ALT="$ x$">|; 

$key = q/V(x)={cases}1000&sqrt{(frac{x}{p_1})^2+(frac{y}{p_2})^2}>p_30&text{else}{cases};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="307" HEIGHT="80" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img254.png"
 ALT="$ V(x)=\begin{cases}
1000 &amp; \sqrt{(\frac{x}{p_1})^2+(\frac{y}{p_2})^2} &gt; p_3 \\\\
0 &amp; \text{else}
\end{cases}$">|; 

$key = q/p_1;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="22" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img75.png"
 ALT="$ p_1$">|; 

$key = q/W(vec{r},vec{r}')=lambda_0delta(vec{r}-vec{r}');MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="183" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img259.png"
 ALT="$ W(\vec{r},\vec{r}')= \lambda_0 \delta(\vec{r}-\vec{r}')$">|; 

$key = q/V(x)=inftyqquadforall[xnotin(0,20-p_1)];MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="273" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img242.png"
 ALT="$ V(x)=\infty \qquad \forall [x\notin (0,20-p_1)]$">|; 

$key = q/7+5(i-1);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="100" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img295.png"
 ALT="$ 7+5(i-1)$">|; 

$key = q/g^{(1)}(vec{k}vert-vec{k};t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="112" HEIGHT="45" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img94.png"
 ALT="$ g^{(1)}(\vec{k} \vert - \vec{k};t)$">|; 

$key = q/5;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img117.png"
 ALT="$ 5$">|; 

$key = q/vec{k}_1);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="29" HEIGHT="45" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img97.png"
 ALT="$ \vec{k}_1)$">|; 

$key = q/+p_3exp(-(frac{(x)^2}{2p_4^2}+frac{(y)^2}{2p_5^2}));MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="189" HEIGHT="48" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img210.png"
 ALT="$ + p_3 exp(- (\frac{(x)^2}{2 p_4^2}+\frac{(y)^2}{2 p_5^2}))$">|; 

$key = q/phi^i_k(x,y,z;t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="100" HEIGHT="39" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img182.png"
 ALT="$ \phi^i_k(x,y,z;t)$">|; 

$key = q/phi^M_1(x,y,z;t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="108" HEIGHT="40" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img192.png"
 ALT="$ \phi^M_1(x,y,z;t)$">|; 

$key = q/V(x,t)=linebreakfrac{1}{2}(x^2+y^2)+linebreakp_3exp[linebreak-frac{1}{p_4}(x-p_2cos(p_1t))^2+(y-p_2sin(p_1t))^2];MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="576" HEIGHT="40" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img253.png"
 ALT="$ V(x,t) = \linebreak\frac{1}{2} (x^2 + y^2)+ \linebreak p_3 exp[ \linebreak -\frac{1}{p_4}(x-p_2\cos(p_1 t))^2 + (y - p_2\sin(p_1 t))^2]$">|; 

$key = q/V_{downarrow}(x)=frac{1}{2}p_2^2(x-p_3)^2+p_4;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="214" HEIGHT="39" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img69.png"
 ALT="$ V_{\downarrow}(x)= \frac{1}{2} p_2^2 (x-p_3)^2+p_4$">|; 

$key = q/sin(frac{1}{2}t)sin(0.4t))x^2;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="156" HEIGHT="39" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img226.png"
 ALT="$ \sin(\frac{1}{2}t) \sin(0.4t)) x^2$">|; 

$key = q/10+5(i-1);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="110" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img298.png"
 ALT="$ 10+5(i-1)$">|; 

$key = q/k;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="15" HEIGHT="18" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img59.png"
 ALT="$ k$">|; 

$key = q/A(t)={cases}frac{tp_3}{p_4}&tleqp_4p_3&t>p_4{cases};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="173" HEIGHT="75" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img220.png"
 ALT="$ A(t)= \begin{cases}
\frac{t p_3}{p_4} &amp; t\leq p_4 \\\\
p_3 &amp; t &gt; p_4
\end{cases}$">|; 

$key = q/W(vec{r},vec{r}')=(lambda_0+I_1sin(I_2t))frac{1}{(sqrt{2pisigma^2})^D}expleft(-frac{vertvec{r}-vec{r}vert^2}{2sigma^2}right);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="408" HEIGHT="52" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img264.png"
 ALT="$ W(\vec{r},\vec{r}')= (\lambda_0 + I_1 \sin(I_2 t) ) \frac{1}{(\sqrt{2\pi \sigma^2})^D} exp\left(- \frac{\vert \vec{r}-\vec{r} \vert^2}{2 \sigma^2} \right)$">|; 

$key = q/0.0001;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="57" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img81.png"
 ALT="$ 0.0001$">|; 

$key = q/cdots;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="30" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img154.png"
 ALT="$ \cdots$">|; 

$key = q/C_{vec{n}}(t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img24.png"
 ALT="$ C_{\vec{n}}(t)$">|; 

$key = q/mathbf{S}^nu;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="18" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img60.png"
 ALT="$ \mathbf{S}^\nu$">|; 

$key = q/frac12(p_1^2x^2+p_2^2y^2);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="122" HEIGHT="39" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img198.png"
 ALT="$ \frac12 (p_1^2 x^2 + p_2^2 y^2)$">|; 

$key = q/hat{mathcal{P}};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="20" HEIGHT="23" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img29.png"
 ALT="$ \hat{\mathcal{P}}$">|; 

$key = q/x_{ref};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="37" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img338.png"
 ALT="$ x_{ref}$">|; 

$key = q/phi_M(x,y,z;t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="107" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img143.png"
 ALT="$ \phi _M(x,y,z;t)$">|; 

$key = q/rho^{(1)}(vec{r}_1=vec{R},vec{r}'_1=vec{r});MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="161" HEIGHT="45" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img347.png"
 ALT="$ \rho^{(1)}(\vec{r}_1=\vec{R},\vec{r}'_1=\vec{r})$">|; 

$key = q/nabla_xxi_M(x,y,z;t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="129" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img381.png"
 ALT="$ \nabla _x \xi _M(x,y,z;t)$">|; 

$key = q/8;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img119.png"
 ALT="$ 8$">|; 

$key = q/3+5N_l+N_{CI}+2MN_l;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="195" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img176.png"
 ALT="$ 3+5N_l+N_{CI}+2 M N_l$">|; 

$key = q/hat{V};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="20" HEIGHT="23" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img21.png"
 ALT="$ \hat{V}$">|; 

$key = q/vertvec{r}-vec{r}vert>I_2;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="98" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img270.png"
 ALT="$ \vert \vec{r}-\vec{r} \vert&gt;I_2$">|; 

$key = q/hat{h}_i^{(CI)};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="44" HEIGHT="47" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img50.png"
 ALT="$ \hat{h}_i^{(CI)}$">|; 

$key = q/r_{1x},r_{1y};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="60" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img359.png"
 ALT="$ r_{1x},r_{1y}$">|; 

$key = q/E_{CI,rel}(t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="79" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img124.png"
 ALT="$ E_{CI,rel}(t)$">|; 

$key = q/g^{(1)};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="32" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img323.png"
 ALT="$ g^{(1)}$">|; 

$key = q/V(x)=frac12(p_1^2x^2+p_2^2y^2);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="188" HEIGHT="39" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img209.png"
 ALT="$ V(x)= \frac12 (p_1^2 x^2+p_2^2 y^2)$">|; 

$key = q/g^{(2)}(vec{r}'_1,vec{r}_1);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="87" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img91.png"
 ALT="$ g^{(2)}(\vec{r}'_1, \vec{r}_1)$">|; 

$key = q/(8+2M);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="81" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img152.png"
 ALT="$ (8+2M)$">|; 

$key = q/xslashy;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="35" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img200.png"
 ALT="$ x/y$">|; 

$key = q/phi^{(NO),i}_1(x,y,z;t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="135" HEIGHT="47" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img194.png"
 ALT="$ \phi^{(NO),i}_1(x,y,z;t)$">|; 

$key = q/V(x)=0.05forall[xin(20-p_1,20)];MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="259" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img246.png"
 ALT="$ V(x)=0.05 \forall [x\in (20-p_1,20)]$">|; 

$key = q/rho^i_w(x,y,z;t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="102" HEIGHT="39" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img172.png"
 ALT="$ \rho^i_w(x,y,z;t)$">|; 

$key = q/rho^{(1slash2)}(x|x';t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="107" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img328.png"
 ALT="$ \rho^{(1/2)}(x\vert x';t)$">|; 

$key = q/vec{r}_1;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="21" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img96.png"
 ALT="$ \vec{r}_1$">|; 

$key = q/p_3,p_4;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="47" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img224.png"
 ALT="$ p_3,p_4$">|; 

$key = q/lambda_0;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="23" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img258.png"
 ALT="$ \lambda_0$">|; 

$key = q/rho(k;t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="54" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img331.png"
 ALT="$ \rho(k;t)$">|; 

$key = q/tleqp_6;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="54" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img239.png"
 ALT="$ t\leq p_6$">|; 

$key = q/V(x)=-p_1x+p_2sin(2x)^4+(xslash2.2)^{20};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="317" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img221.png"
 ALT="$ V(x)=-p_1 x + p_2 \sin(2 x)^4 + (x/2.2)^{20}$">|; 

$key = q/0.15;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="38" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img47.png"
 ALT="$ 0.15$">|; 

$key = q/T_{CI};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="34" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img129.png"
 ALT="$ T_{CI}$">|; 

$key = q/S_{rho-r}(t)=-intdvec{r}rho(vec{r};t)ln[rho(vec{r};t)];MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="264" HEIGHT="39" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img284.png"
 ALT="$ S_{\rho-r}(t)=- \int d\vec{r}\rho(\vec{r};t) ln [\rho(\vec{r};t)]$">|; 

$key = q/j;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img58.png"
 ALT="$ j$">|; 

$key = q/P_{not}(t,x_s,x_e);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="111" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img281.png"
 ALT="$ P_{not}(t,x_s,x_e)$">|; 

$key = q/(2+M);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="72" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img157.png"
 ALT="$ (2+M)$">|; 

$key = q/g^{(1)}(vec{r}vert-vec{r};t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="109" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img92.png"
 ALT="$ g^{(1)}(\vec{r} \vert - \vec{r};t)$">|; 

$key = q/displaystyle=;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="19" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img53.png"
 ALT="$\displaystyle =$">|; 

$key = q/vertPsirangle=vertN,0,0,...rangle;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="145" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img28.png"
 ALT="$ \vert \Psi \rangle = \vert N,0,0,... \rangle$">|; 

$key = q/frac12(p_1^2x^2+p_2^2y^2+p_3^2x^2);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="180" HEIGHT="39" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img202.png"
 ALT="$ \frac12 (p_1^2 x^2 + p_2^2 y^2 + p_3^2 x^2)$">|; 

$key = q/S_{rho_k^{(GO)}}(t)=sum_i-frac{rho^{(GO)}_i(t)}{N}ln[frac{rho^{(GO)}_i(t)}{N}];MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="279" HEIGHT="56" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img294.png"
 ALT="$ S_{\rho_k^{(GO)}}(t)=\sum_i - \frac{\rho^{(GO)}_i(t)}{N} ln [\frac{\rho^{(GO)}_i(t)}{N}] $">|; 

$key = q/p_{2slash4}equiv;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="55" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img249.png"
 ALT="$ p_{2/4}\equiv$">|; 

$key = q/V(x)={cases}frac{1}{2}x^2&xleq2Ax^3+Bx^2+Cx+D&2<xleq4p_1&x>4{cases};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="276" HEIGHT="128" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img228.png"
 ALT="$ V(x)=\begin{cases}
\frac{1}{2}x^2 &amp; x \leq 2 \\\\
Ax^3+Bx^2 +Cx+D &amp; 2 &lt; x \leq 4 \\\\
p_1 &amp; x &gt; 4
\end{cases}$">|; 

$key = q/phi_2^{(NO)}(x);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="75" HEIGHT="47" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img3.png"
 ALT="$ \phi_2^{(NO)}(x)$">|; 

$key = q/V(x)=inftyqquadforall[x<0];MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="193" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img244.png"
 ALT="$ V(x)=\infty \qquad \forall [x &lt; 0]$">|; 

$key = q/rho_i(x',y',z';t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="110" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img305.png"
 ALT="$ \rho_i(x',y',z';t)$">|; 

$key = q/phi_1(x,y,z;t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="100" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img142.png"
 ALT="$ \phi _1(x,y,z;t)$">|; 

$key = q/p_a=p_2;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="64" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img252.png"
 ALT="$ p_a=p_2$">|; 

$key = q/1.0;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="29" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img46.png"
 ALT="$ 1.0$">|; 

$key = q/displaystylehat{W}_{mathrm{spin}};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="47" HEIGHT="44" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img55.png"
 ALT="$\displaystyle \hat{W}_{\mathrm{spin}}$">|; 

$key = q/rho(vec{r}_1;t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="60" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img355.png"
 ALT="$ \rho(\vec{r}_1;t)$">|; 

$key = q/p_4equiv;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="42" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img234.png"
 ALT="$ p_4\equiv$">|; 

$key = q/1,2,3;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="50" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img363.png"
 ALT="$ 1,2,3$">|; 

$key = q/A(t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="40" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img213.png"
 ALT="$ A(t)$">|; 

$key = q/N=101;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="75" HEIGHT="18" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img110.png"
 ALT="$ N=101$">|; 

$key = q/phi^i_1(x,y,z;t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="100" HEIGHT="39" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img191.png"
 ALT="$ \phi^i_1(x,y,z;t)$">|; 

$key = q/(3+M);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="72" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img158.png"
 ALT="$ (3+M)$">|; 

$key = q/rho_i^{(2)}(x,y,z|x',y',z';t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="174" HEIGHT="47" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img308.png"
 ALT="$ \rho_i^{(2)}(x,y,z\vert x',y',z';t)$">|; 

$key = q/rho(x);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="40" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img1.png"
 ALT="$ \rho(x)$">|; 

$key = q/r_{1x},r_{1y},r_{2x},r_{2y};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="123" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img354.png"
 ALT="$ r_{1x},r_{1y},r_{2x},r_{2y}$">|; 

$key = q/p_3;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="22" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img72.png"
 ALT="$ p_3$">|; 

$key = q/8+5(i-1);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="100" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img296.png"
 ALT="$ 8+5(i-1)$">|; 

$key = q/p_2equiv;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="42" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img207.png"
 ALT="$ p_2\equiv$">|; 

$key = q/E_{CI}(t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="58" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img123.png"
 ALT="$ E_{CI}(t)$">|; 

$key = q/|g^{(2)}|;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="43" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img105.png"
 ALT="$ \vert g^{(2)}\vert$">|; 

$key = q/x,y,z;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="51" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img138.png"
 ALT="$ x,y,z$">|; 

$key = q/4+N_l+N_{CI};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="113" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img161.png"
 ALT="$ 4+N_l+N_{CI}$">|; 

$key = q/i-;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="26" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img190.png"
 ALT="$ i-$">|; 

$key = q/expleft(-frac{vertvec{r}-vec{r}vert^2}{2(sigma^2}+I_1sin(I_2t))right);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="225" HEIGHT="52" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img267.png"
 ALT="$ exp\left(- \frac{\vert \vec{r}-\vec{r} \vert^2}{2 (\sigma^2}+ I_1 \sin(I_2 t)) \right)$">|; 

$key = q/rho(k_x,k_y,k_z;t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="114" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img316.png"
 ALT="$ \rho (k_x,k_y,k_z;t)$">|; 

$key = q/V(x)=-p_1exp(-p_2(x+frac{1}{2}p_3)^2)-p_4exp(-p_5(x-frac{1}{2}p_3));MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="452" HEIGHT="39" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img233.png"
 ALT="$ V(x) = -p_1 exp(-p_2(x+\frac{1}{2}p_3)^2) - p_4 exp(-p_5(x-\frac{1}{2}p_3))$">|; 

$key = q/ln[rho^{(2)}(vec{r}_1,vec{r}_2;t)];MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="130" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img291.png"
 ALT="$ ln [\rho^{(2)}(\vec{r}_1,\vec{r}_2;t)]$">|; 

$key = q/hat{h}_{SO};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="36" HEIGHT="45" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img62.png"
 ALT="$ \hat{h}_{SO}$">|; 

$key = q/vec{k}'_1);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="29" HEIGHT="45" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img99.png"
 ALT="$ \vec{k}'_1)$">|; 

$key = q/0.0;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="29" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img35.png"
 ALT="$ 0.0$">|; 

$key = q/|g^{(1)}_1|^2=leftvertfrac{rho^{(1)}_1(x_1,x'_1;t)}{sqrt{rho_1(x_1;t)rho_1(x'_1;t)}}rightvert^2;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="210" HEIGHT="70" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img312.png"
 ALT="$ \vert g^{(1)}_1\vert^2=\left\vert \frac{\rho^{(1)}_1(x_1,x'_1;t)}{\sqrt{\rho_1(x_1;t)\rho_1(x'_1;t)}} \right\vert^2$">|; 

$key = q/V^i_{CI}(x,y,z,t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="110" HEIGHT="39" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img171.png"
 ALT="$ V^i_{CI}(x,y,z,t)$">|; 

$key = q/(11+4M+2);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="123" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img147.png"
 ALT="$ (11+4M+2)$">|; 

$key = q/11;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="23" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img135.png"
 ALT="$ 11$">|; 

$key = q/displaystylehat{h}_i^{(CI),kk};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="63" HEIGHT="47" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img52.png"
 ALT="$\displaystyle \hat{h}_i^{(CI),kk}$">|; 

$key = q/1,2,3,4;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="67" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img255.png"
 ALT="$ 1,2,3,4$">|; 

$key = q/D;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="21" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img276.png"
 ALT="$ D$">|; 

$key = q/rho_i(x,y,z;t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="96" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img301.png"
 ALT="$ \rho_i(x,y,z;t)$">|; 

$key = q/V(x)=0qquadforall[xin)0,20-p_1(];MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="258" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img243.png"
 ALT="$ V(x)=0 \qquad \forall [x\in )0,20-p_1(]$">|; 

$key = q/displaystyleleft(hat{T}_i+V_{kk}(hat{vec{r}_i})right)otimesmathbf{1}_{vec{r}};qquadhat{h}_i^{(CI),kj}=V_{kj}(vec{r}_i)hat{pi}_{jk};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="370" HEIGHT="52" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img54.png"
 ALT="$\displaystyle \left( \hat{T}_i + V_{kk}(\hat{\vec{r}_i}) \right) \otimes \mathbf{1}_{\vec{r}};\qquad \hat{h}_i^{(CI),kj}= V_{kj}(\vec{r}_i)\hat{\pi}_{jk}$">|; 

$key = q/+p_2exp(-frac{(x-p_1)^2}{2p_3^2});MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="147" HEIGHT="48" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img206.png"
 ALT="$ + p_2 exp(- \frac{(x-p_1)^2}{2 p_3^2})$">|; 

$key = q/displaystylesum_{nu=x,y,z}left(mathbf{S}^nuotimesmathbf{1}_{vec{r}}right)hat{W}(},vec{r}';t)left(mathbf{S}^nuotimesmathbf{1}_{vec{r}'}right).;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="307" HEIGHT="61" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img56.png"
 ALT="$\displaystyle \sum_{\nu=x,y,z} \left( \mathbf{S}^\nu \otimes \mathbf{1}_{\vec{r...
...\vec{r},\vec{r}';t) \left( \mathbf{S}^\nu \otimes \mathbf{1}_{\vec{r}'}\right).$">|; 

$key = q/W(vec{r},vec{r}')=lambda_0frac{1}{(sqrt{2pisigma^2})^D}expleft(-frac{vertvec{r}-vec{r}vert^2}{2sigma^2}right);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="291" HEIGHT="52" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img257.png"
 ALT="$ W(\vec{r},\vec{r}')= \lambda_0 \frac{1}{(\sqrt{2\pi \sigma^2})^D} exp\left(- \frac{\vert \vec{r}-\vec{r} \vert^2}{2 \sigma^2} \right)$">|; 

$key = q/rho(x,t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="55" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img106.png"
 ALT="$ \rho(x,t)$">|; 

$key = q/5+5N_l+N_{CI}+2MN_l;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="195" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img178.png"
 ALT="$ 5+5N_l+N_{CI}+2 M N_l$">|; 

$key = q/x',y',z';MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="65" HEIGHT="36" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img299.png"
 ALT="$ x',y',z'$">|; 

$key = q/V=0;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="54" HEIGHT="18" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img241.png"
 ALT="$ V=0$">|; 

$key = q/lambda_0=1.0;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="72" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img14.png"
 ALT="$ \lambda_0=1.0$">|; 

$key = q/rho_i^{(1)}(x,y,z|x',y',z';t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="174" HEIGHT="47" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img303.png"
 ALT="$ \rho_i^{(1)}(x,y,z\vert x',y',z';t)$">|; 

$key = q/V(x)={cases}frac{1}{2}x^2&xleq22.2662969exp(-2(x-2.25)^2)&x>2{cases};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="310" HEIGHT="101" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img227.png"
 ALT="$ V(x) = \begin{cases}
\frac{1}{2} x^2 &amp; x\leq2 \\\\
2.2662969  exp(-2 (x-2.25)^2) &amp; x &gt; 2
\end{cases}$">|; 

$key = q/|g^{(1)}|^2=leftvertfrac{rho^{(1)}(x_1,x'_1;t)}{sqrt{rho(x_1;t)rho(x'_1;t)}}rightvert^2;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="197" HEIGHT="70" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img325.png"
 ALT="$ \vert g^{(1)}\vert^2=\left\vert \frac{\rho^{(1)}(x_1,x'_1;t)}{\sqrt{\rho(x_1;t)\rho(x'_1;t)}} \right\vert^2$">|; 

$key = q/rho(vec{r}_2;t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="60" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img356.png"
 ALT="$ \rho(\vec{r}_2;t)$">|; 

$key = q/x=0;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="50" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img218.png"
 ALT="$ x=0$">|; 

$key = q/rho_i^{(2)}(k_x,k_y,k_z|k'_x,k'_y,k'_z;t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="207" HEIGHT="47" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img309.png"
 ALT="$ \rho_i^{(2)}(k_x,k_y,k_z\vert k'_x,k'_y,k'_z;t)$">|; 

$key = q/tau=frac{langlevec{r}_1vec{r}_2rangle-langlevec{r}rangle^2}{langlevec{r}^2rangle-langlevec{r}rangle^2};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="114" HEIGHT="48" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img83.png"
 ALT="$ \tau=\frac{\langle \vec{r}_1 \vec{r}_2 \rangle - \langle \vec{r} \rangle^2}{ \langle \vec{r}^2 \rangle - \langle \vec{r} \rangle^2}$">|; 

$key = q/vertvec{n};trangle;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="44" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img25.png"
 ALT="$ \vert \vec{n};t\rangle$">|; 

$key = q/9;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img134.png"
 ALT="$ 9$">|; 

$key = q/delimiter"026A30CPsidelimiter"526930B;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="32" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img6.png"
 ALT="$ \delimiter ''026A30C \Psi \delimiter ''526930B $">|; 

$key = q/5+3N_l+N_{CI};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="123" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img166.png"
 ALT="$ 5+3N_l+N_{CI}$">|; 

$key = q/-8.0;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="44" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img33.png"
 ALT="$ -8.0$">|; 

$key = q/rho^{(1)}(vec{r}_1|vec{r}_2;t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="100" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img357.png"
 ALT="$ \rho^{(1)}(\vec{r}_1 \vert \vec{r}_2;t) $">|; 

$key = q/rho(k'_x,k'_y,k'_z;t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="114" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img320.png"
 ALT="$ \rho(k'_x,k'_y,k'_z;t)$">|; 

$key = q/V(x,t)=frac12((1+p_a(t))(xcos(p_1t)+ysin(p_1t))^2+(1-p_a(t))(ycos(p_1t)-xsin(p_1t)));MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="688" HEIGHT="40" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img250.png"
 ALT="$ V(x,t)=\frac12 ((1+p_a(t))(x \cos(p_1 t) + y\sin(p_1 t))^2 + (1-p_a(t))(y \cos(p_1 t) - x \sin(p_1 t)))$">|; 

$key = q/T_{CI,Func};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="70" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img131.png"
 ALT="$ T_{CI,Func}$">|; 

$key = q/frac{1}{2};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="15" HEIGHT="39" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img67.png"
 ALT="$ \frac{1}{2}$">|; 

$key = q/(rho^{(2)}(vec{r}_1=vec{r}'_1=vec{R},;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="149" HEIGHT="45" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img349.png"
 ALT="$ (\rho^{(2)}(\vec{r}_1=\vec{r}'_1=\vec{R},$">|; 

$key = q/96%;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="39" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img15.png"
 ALT="$ 96\%$">|; 

$key = q/rho(x;t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="55" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img330.png"
 ALT="$ \rho(x;t)$">|; 

$key = q/9+2M;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="67" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img156.png"
 ALT="$ 9+2M$">|; 

$key = q/1;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img39.png"
 ALT="$ 1$">|; 

$key = q/(7+M);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="72" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img369.png"
 ALT="$ (7+M)$">|; 

$key = q/rho^{(1)}(x,y,z|x',y',z';t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="174" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img317.png"
 ALT="$ \rho^{(1)}(x,y,z\vert x',y',z';t)$">|; 

$key = q/C_{vec{n}};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="27" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img27.png"
 ALT="$ C_{\vec{n}}$">|; 

$key = q/k'_x,k'_y,k'_z;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="75" HEIGHT="36" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img300.png"
 ALT="$ k'_x,k'_y,k'_z$">|; 

$key = q/(11+2M);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="91" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img137.png"
 ALT="$ (11+2M)$">|; 

$key = q/rho^{(p)}(vec{r}_{ref},...,vec{r}_{ref},vec{r}_{order-1},vec{r}_{order-2});MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="274" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img87.png"
 ALT="$ \rho^{(p)}(\vec{r}_{ref},...,\vec{r}_{ref},\vec{r}_{order-1},\vec{r}_{order-2})$">|; 

$key = q/W(vec{r},vec{r}')=lambda_0sin(I_1t)delta(vec{r}-vec{r}');MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="246" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img262.png"
 ALT="$ W(\vec{r},\vec{r}')= \lambda_0 \sin (I_1 t) \delta(\vec{r}-\vec{r}')$">|; 

$key = q/g^{(2)}(vec{k}_1,-vec{k}_1;t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="120" HEIGHT="45" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img95.png"
 ALT="$ g^{(2)}(\vec{k}_1, - \vec{k}_1;t)$">|; 

$key = q/rho^{(2)}(k_x,k_y,k_z|k'_x,k'_y,k'_z;t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="207" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img322.png"
 ALT="$ \rho^{(2)}(k_x,k_y,k_z\vert k'_x,k'_y,k'_z;t)$">|; 

$key = q/S_n(t)=sum_i-frac{n_i(t)}{N}ln[frac{n_i(t)}{N}];MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="208" HEIGHT="45" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img287.png"
 ALT="$ S_n(t)= \sum_i - \frac{n_i(t)}{N} ln [\frac{n_i(t)}{N}] $">|; 

$key = q/g^{(1)}(vec{r}'_1vertvec{r}_1);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="84" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img90.png"
 ALT="$ g^{(1)}(\vec{r}'_1\vert \vec{r}_1)$">|; 

$key = q/S_{rho-k}(t)=-intdvec{k}rho(vec{k};t)ln[rho(vec{k};t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="264" HEIGHT="45" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img285.png"
 ALT="$ S_{\rho-k}(t)=- \int d\vec{k}\rho(\vec{k};t) ln [\rho(\vec{k};t)$">|; 

1;

