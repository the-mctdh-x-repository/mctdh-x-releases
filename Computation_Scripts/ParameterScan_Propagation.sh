#!/bin/bash
shopt -s expand_aliases
source ~/.bashrc > /dev/null 2>&1
function Propagation()
{
export LANG="en_US"
if [ "$#" -ge 3  ]; then
 echo "Script for Parameter Prop_Scan for Propagations"
 echo "All input variables must be specified in ParameterScan.inp in the same directory "
 echo "The library libmctdhx.so, an input template, and a binary file as specified in the input file must also reside in the working directory"
 echo "usage: Propagation \$RELAXDIR ["save"]"
 exit 1
fi
RELAXDIR=$1
PARDIR=$PWD
Prop_Array_File=Prop_Array_$(echo "$RELAXDIR" | sed 's|'$PARDIR'||' | sed 's|/|_|g')
Completed_Array_File=Completed_Array_$(echo "$RELAXDIR" | sed 's|'$PARDIR'||' | sed 's|/|_|g')

#echo $Prop_Array_File
#return 0  
if [ "$2" != "save" ]; then
 echo "Cleaning RunFlags and run*.sh"
# rm $PARDIR/run*.sh* 2>/dev/null
 rm $RELAXDIR/*/RunFlag 2>/dev/null
 rm $RELAXDIR/*/Continue*/RunFlag 2>/dev/null
 rm $RELAXDIR/$Prop_Array_File 2>/dev/null
 rm $RELAXDIR/$Completed_Array_File 2>/dev/null
fi
#. $MCTDHXDIR/Scripts/assemble_runscript.sh
. $MCTDHXDIR/Scripts/IterateParameters.sh
. $MCTDHXDIR/Scripts/check_integer.sh
. $MCTDHXDIR/Scripts/readarray.sh
. $MCTDHXDIR/../Computation_Scripts/do_Analysis.sh
. ./ParameterScan.inp
#. $MCTDHXDIR/Scripts/copy_bash_array.sh
#. $MCTDHXDIR/Scripts/get_JobidFromJobdir.sh
#. $MCTDHXDIR/Scripts/get_NumberOfJobs.sh
#### Added by storm
#. $MCTDHXDIR/Scripts/ContinueProp.sh
#. $MCTDHXDIR/Scripts/ExclusiveAdd.sh
#. $MCTDHXDIR/Scripts/FindContinuation.sh
#. $MCTDHXDIR/Scripts/Adjust_Inp_Prop.sh
#. $MCTDHXDIR/Scripts/RemoveArrayElement.sh
#. $MCTDHXDIR/Scripts/rbincp.sh
if [ ! -f "$binary" ];
  then
    echo "The specified executable $binary does not exist"
fi
if [ ! -f "$Propagation_Template" ];
  then
    echo "Specified input template $Propagation_Template does not exist"
    exit 1
fi
echo "################################################################################"
echo "################################################################################"
echo "################################################################################"
echo "################################################################################"
echo "################################################################################"
echo "#########################Input Variables########################################"
echo "################################################################################"
echo "################################################################################"
echo "################################################################################"
echo "################################################################################"
echo "################################################################################"
 ### Output input file to screen
echo "runhost=" $runhost
echo "MaxNodes=" $MaxNodes
echo "MPMDnodes=" $MPMDnodes
echo "maxjobs=" $maxjobs
echo "NParameters=" $Prop_NParameters

echo "Prop_Parameter1=" $Prop_Parameter1
echo "Prop_List1=" $Prop_List1         
echo "Prop_Parameter1List=" $Prop_Parameter1List  
echo "Prop_Scan1_Start=" $Prop_Scan1_Start
echo "Prop_Scan1_Stop=" $Prop_Scan1_Stop
echo "Prop_Scan1_Step=" $Prop_Scan1_Step

echo "Prop_Parameter2=" $Prop_Parameter2
echo "Prop_List2=" $Prop_List2         
echo "Prop_Parameter2List=" $Prop_Parameter2List  
echo "Prop_Scan2_Start=" $Prop_Scan2_Start
echo "Prop_Scan2_Stop=" $Prop_Scan2_Stop
echo "Prop_Scan2_Step=" $Prop_Scan2_Step

echo "Prop_Parameter3=" $Prop_Parameter3
echo "Prop_List3=" $Prop_List3         
echo "Prop_Parameter3List=" $Prop_Parameter3List  
echo "Prop_Scan3_Start=" $Prop_Scan3_Start
echo "Prop_Scan3_Stop=" $Prop_Scan3_Stop
echo "Prop_Scan3_Step=" $Prop_Scan3_Step

echo "Prop_Parameter4=" $Prop_Parameter4
echo "Prop_List4=" $Prop_List4         
echo "Prop_Parameter4List=" $Prop_Parameter4List  
echo "Prop_Scan4_Start=" $Prop_Scan4_Start
echo "Prop_Scan4_Stop=" $Prop_Scan4_Stop
echo "Prop_Scan4_Step=" $Prop_Scan4_Step

echo "Prop_Parameter5=" $Prop_Parameter5
echo "Prop_List5=" $Prop_List5         
echo "Prop_Parameter5List=" $Prop_Parameter5List  
echo "Prop_Scan5_Start=" $Prop_Scan5_Start
echo "Prop_Scan5_Stop=" $Prop_Scan5_Stop
echo "Prop_Scan5_Step=" $Prop_Scan5_Step
 ### Output input file to screen
#### Processing the input

if [ "$Prop_NParameters" -lt 1 ];
   then
   echo "You must specify at least one parameter to Prop_Scan in ParamterScan_Relaxation.inp"
   exit 1
fi
if [ "$Prop_NParameters" -ge 1 ];
   then
 if [ "$Prop_List1" != "T" ];  
   then
   a=`check_integer $Prop_Scan1_Start`
   b=`check_integer $Prop_Scan1_Stop`
   if [ "$a" -ne "0" ] || [ "$b" -ne "0" ] ; 
      then
       SC=10
      else
       SC=0
   fi  
   
   Prop_Scan1_List=`seq $Prop_Scan1_Start $Prop_Scan1_Step $Prop_Scan1_Stop`
   Prop_Scan1_Nsteps=`echo "scale=$SC;($Prop_Scan1_Stop-$Prop_Scan1_Start+1)/$Prop_Scan1_Step" | bc `
 else 
   Prop_Scan1_List=`echo $Prop_Parameter1List`
 fi
   Prop_Scan2_List="EMPTY"
   Prop_Scan3_List="EMPTY"
   Prop_Scan4_List="EMPTY"
   Prop_Scan5_List="EMPTY"
fi
if [ "$Prop_NParameters" -ge 2 ];
   then
 if [ "$Prop_List2" != "T" ];
   then
   a=`check_integer $Prop_Scan2_Start`
   b=`check_integer $Prop_Scan2_Stop`
   if [ "$a" -ne 0 ] || [ "$b" -ne 0 ] ;
      then
       SC=10
      else
       SC=0
   fi

   Prop_Scan2_List=`seq $Prop_Scan2_Start $Prop_Scan2_Step $Prop_Scan2_Stop`
   Prop_Scan2_Nsteps=`echo "scale=$SC;($Prop_Scan2_Stop-$Prop_Scan2_Start+1)/$Prop_Scan2_Step" | bc `
 else
   Prop_Scan2_List=`echo $Prop_Parameter2List`
 fi
   Prop_Scan3_List="EMPTY"
   Prop_Scan4_List="EMPTY"
   Prop_Scan5_List="EMPTY"
fi
if [ "$Prop_NParameters" -ge 3 ];
   then
 if [ "$Prop_List3" != "T" ];
   then
   a=`check_integer $Prop_Scan3_Start`
   b=`check_integer $Prop_Scan3_Stop`
   if [ "$a" -ne 0 ] || [ "$b" -ne 0 ] ;
      then
       SC=10
      else
       SC=0
   fi


   Prop_Scan3_Nsteps=`echo "scale=$SC;($Prop_Scan3_Stop-$Prop_Scan3_Start+1)/$Prop_Scan3_Step" | bc `
   Prop_Scan3_List=`seq $Prop_Scan3_Start $Prop_Scan3_Step $Prop_Scan3_Stop`
 else
   Prop_Scan3_List=`echo $Prop_Parameter3List`
 fi

   Prop_Scan4_List="EMPTY"
   Prop_Scan5_List="EMPTY"

fi
if [ "$Prop_NParameters" -ge 4 ];
   then
 if [ "$Prop_List4" != "T" ];
   then
   a=`check_integer $Prop_Scan4_Start`
   b=`check_integer $Prop_Scan4_Stop`
   if [ "$a" -ne 0 ] || [ "$b" -ne 0 ] ;
      then
       SC=10
      else
       SC=0
   fi
   Prop_Scan4_Nsteps=`echo "scale=$SC;($Prop_Scan4_Stop-$Prop_Scan4_Start+1)/$Prop_Scan4_Step" | bc `
   Prop_Scan4_List=`seq $Prop_Scan4_Start $Prop_Scan4_Step $Prop_Scan4_Stop`
 else
   Prop_Scan4_List=`echo $Prop_Parameter4List`
 fi
   Prop_Scan5_List="EMPTY"
fi
if [ "$Prop_NParameters" -ge 5 ];
   then
 if [ "$Prop_List5" != "T" ];
   then
   a=`check_integer $Prop_Scan5_Start`
   b=`check_integer $Prop_Scan5_Stop`
   if [ "$a" -ne 0 ] || [ "$b" -ne 0 ] ;
      then
       SC=10
      else
       SC=0
   fi

   Prop_Scan5_Nsteps=`echo "scale=$SC;($Prop_Scan5_Stop-$Prop_Scan5_Start+1)/$Prop_Scan5_Step" | bc `
   Prop_Scan5_List=`seq $Prop_Scan5_Start $Prop_Scan5_Step $Prop_Scan5_Stop`
 else
   Prop_Scan5_List=`echo $Prop_Parameter5List`
 fi
fi
touch $RELAXDIR/$Prop_Array_File
touch $RELAXDIR/$Completed_Array_File
echo "RELAXDIR=$RELAXDIR" 
Morb=$(cat $RELAXDIR/MCTDHX.inp | grep -o "Morb *= *[0-9]*" | grep -o "[0-9]*" )
NUMNODES=$(echo "scale=0;($Morb+1)/2.0" | bc )
echo "################################################################################"
echo "################################################################################"
echo "################################################################################"
echo "################################################################################"
echo "################################################################################"
echo "###########################Beginning Scan#######################################"
echo "################################################################################"
echo "################################################################################"
echo "################################################################################"
echo "################################################################################"
echo "################################################################################"
if [ "$NUMNODES" -gt "$MaxNodes" ] ; then  NUMNODES=$MaxNodes  ; fi
#echo $Morb
#echo $NUMNODES
## Count the scan points
ScanPoints=0
 for i in ${Prop_Scan5_List} ; do
    for j in ${Prop_Scan4_List} ; do
      for k in ${Prop_Scan3_List} ; do
        for l in ${Prop_Scan2_List} ; do
          for m in ${Prop_Scan1_List} ; do
            let "ScanPoints++"
          done
        done
      done
    done
  done
echo "Scanpoints: $ScanPoints"
while true # Infinite loop in case of cluster
do
 loopcount=0
# Njobs=`get_NumberOfJobs $runhost`
 for i in ${Prop_Scan5_List} ; do
    for j in ${Prop_Scan4_List} ; do
      for k in ${Prop_Scan3_List} ; do
        for l in ${Prop_Scan2_List} ; do
          for m in ${Prop_Scan1_List} ; do
            #### Bulk of code goes here
            echo "################################################################################"
            echo "################################################################################"
            echo "Current parameter set: $m $l $k $j $i"
            let "loopcount++"
            echo "loopcount=$loopcount"
            if [ "$loopcount" -eq "$ScanPoints" ] ; then
              ForceRun="T"
            else
              ForceRun="F"
            fi
           (
            IterateParameters_prop $PARDIR $m $l $k $j $i
           )
           wait
          done
        done
      done
    done
 done
  Prop_Array=($(readarray $RELAXDIR/$Prop_Array_File))
  Completed_Array=($(readarray $RELAXDIR/$Completed_Array_File))
  echo "Looped through all Propagation scan points"
 if [ "$Do_Analysis" == "T" ]; then
  echo "Submitting to Analysis script!"
  do_Analysis $PARDIR prop ${Prop_Array[@]/#/$RELAXDIR/} 
echo "###########################Checking Analysis of \$Completed_Array#######################################"
echo "###########################Checking Analysis of \$Completed_Array#######################################"
echo "###########################Checking Analysis of \$Completed_Array#######################################"
echo "###########################Checking Analysis of \$Completed_Array#######################################"
  do_Analysis $PARDIR prop ${Completed_Array[@]/#/$RELAXDIR/} 
echo "###########################Analysis Script Over#######################################"
echo "###########################Analysis Script Over#######################################"
echo "###########################Analysis Script Over#######################################"
echo "###########################Analysis Script Over#######################################"
 else
  echo "skipping analysis"
 fi
#      echo "Analysis script running in bg with PID=$!"
   if [ "${#Completed_Array}" -eq "$ScanPoints"  ] ; then
     ### All Propagations are done
     echo "All Propagations are done, program over"
     return
   fi          
    ### sleep for 20 minutes before looping again
    
    if [ "$runhost" != "inScript" ]; then
      if [ "$runhost" != "nemo" ]; then
        echo "sleeping for 1 hour before looping again"
        sleep 3600
       else
         echo "Sleeping 5 mins until next loop"
         sleep 300 ### wait 1 hour before starting over.
       fi
    fi
done
}
