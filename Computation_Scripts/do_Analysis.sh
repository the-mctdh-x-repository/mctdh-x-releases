#!/bin/bash

## I Take in a directory and do the analysis
## In Relax mode, I only analyze the last time point
## In Prop Mode, I analyze all eligible subdirectories as well
## If the full analysis is complete, I make movies

## I decided to not reload Anal_Array at each step in the while loop because that could lead
## To multiple instances of do_Analysis detecting a total completion of the analysis
## This could cause problems when making movies

function do_Analysis()
{

if [ $# -lt 3 ] && [ "${Mode:0:4}" == "prop" ] || [ "${Mode:0:4}" == "rela" ] ; then
 echo "Usage: do_Analysis \$PARDIR \$Mode \$AnalDir1 \$AnalDir2 ... "
 echo "\$Mode is case insensitive and must match either \"prop\" or \"rela\" or \"any\" "
 return 1
fi
PARDIR=$1
Mode=$(echo $2 | tr '[:upper:]' '[:lower:]')
Mode=${Mode:0:4}
. $PARDIR/ParameterScan.inp
. $MCTDHXDIR/Scripts/GetAnalArray.sh
. $MCTDHXDIR/Scripts/getTimes.sh
. $MCTDHXDIR/Scripts/AdjustAnalInput.sh
. $MCTDHXDIR/Scripts/FileCat.sh 
. $MCTDHXDIR/Scripts/assemble_runscript.sh 
. $MCTDHXDIR/Scripts/RemoveArrayElement.sh 
. $MCTDHXDIR/Scripts/ExclusiveAdd.sh 
. $MCTDHXDIR/Scripts/IsRelaxation.sh 
. $MCTDHXDIR/Scripts/findComputations.sh 
shift
shift
AnalDirArray=("${@}")
#echo ${Mode:0:4}
#if [ "${Mode:0:4}" == "prop" ]; then
#  echo "propagation mode"
  
#  PROPMAIN=$3
  ## Build the Array of Eligible Analysis directories
  unset Anal_Array
  declare -a Anal_Array
echo "#################################### do_Analysis ####################################"
echo "#################################### do_Analysis ####################################"
echo "#################################### do_Analysis ####################################"
echo "#################################### do_Analysis ####################################"
echo "#################################### do_Analysis ####################################"
echo "PARDIR=$PARDIR"
echo "AnalDirArray has ${#AnalDirArray[@]} elements"
echo "AnalDirArray=${AnalDirArray[@]}"

  if [ "${Mode}" == "prop" ]; then
   echo "propagation mode"
   Anal_Array=(`GetAnalArray ${AnalDirArray[@]}`)
  elif [ "${Mode}" == "rela" ]; then
   echo "relaxation mode"
   Anal_Array=(${AnalDirArray[@]})
   ForceTime=
  elif [ "${Mode:0:3}" == "any" ]; then
     # all directories that contain a computation 
     Anal_Array=$(FindComputations $PARDIR)

     for dir in ${Anal_Array[@]} ; do
       if [ -f $dir/CompleteFlag ] ; then
         Anal_Array=$(RmEle $dir ${Anal_Array[@]})
       fi
     done

  fi

# count number of directories to analyze
  analscanpoints=0
  for dir in ${Anal_Array[@]} ; do
     let "analscanpoints++"
  done


  echo "${#Anal_Array[@]} Eligible analysis directories to process " 
  ## Initialize the array of completed analysis directories
  unset Complete_Array
  declare -a Complete_Array
  ## Loop until all eligible analysis directories are done
#  while [ "${#Anal_Array[@]}" != "0" ] || [ "${#Complete_Array[@]}" != "0" ] ; do
   loopcount_anal=0
   for dir in ${Anal_Array[@]} ; do
     echo "#####################################################################################"
     echo "#####################################################################################"
     PROPMAIN=$(echo $dir | sed 's|/Continue_[0-9]*\.[0-9]*||')
     echo "In analysis loop: $dir"

     InitialAnalTime=$(getInitialPropTime $dir)
     InitialPropTime=$(getInitialPropTime $dir)
     FinalPropTime=$(getFinalPropTime $dir)
     FinalAnalTime=$(getFinalPropTime $dir)
     Output_TimeStep=$(cat $dir/MCTDHX.inp  | grep -o "Output_TimeStep *= *[0-9]*\.[0-9]*" | sed 's|Output\_TimeStep *=||' | sed 's|d0||')
     scaledOutput=$(echo "scale=0;$AnalFactor*$Output_TimeStep*1000000/1.0" | bc)

     if [ "$Mode" == "prop" ] && [ "$(echo "scale=0;1000000*($InitialAnalTime-$InitialPropTime)/1.0" | bc)" -le "$scaledOutput" ] && [ "$(echo "scale=0;1000000*($FinalAnalTime-$FinalPropTime)/1.0" | bc)" -lt "$scaledOutput" ] ; then
       ## Initial and Final Time of L_z_expectation.dat match NO_PR.out, analysis must be complete
       echo "Analysis Completed!"
       Anal_Array=(`RmEle $dir ${Anal_Array[@]}`)
       echo "Removed from \$Anal_Array: ${#Anal_Array[@]} Elements"
       mkdir $PROPMAIN/Data 2>/dev/null
       mv $dir/*[0-9]*.dat $PROPMAIN/Data 2>/dev/null
#       FileCat $PROPMAIN/Data  $dir/L_z_expectation.dat $dir/NO_PR.out #total_energy.dat
     elif [ "$Mode" == "rela" ] && [ -f $dir/CompleteFlag ]; then
       echo "Analysis Completed!" 
       Anal_Array=(`RmEle $dir ${Anal_Array[@]}`)
       echo "Removed from \$Anal_Array: ${#Anal_Array[@]} Elements"
     elif [ -e $dir/AnalFlag ]; then
       echo "Analysis currently running, lets give it time"
     else
       ## Analysis is not complete, so we must run it
        let "loopcount_anal++"
        echo "loopcount_anal=$loopcount_anal"

        echo "Analysis incomplete, lets begin/continue it!"
        echo "Copying input and excecutable"
        cp $PARDIR/$AnalTemplate $dir/analysis.inp
        cp $PARDIR/$AnalBin $dir/
        echo "Adjusting Input File"
        (
          AdjustAnalInp $PARDIR $dir $Mode
        )
        echo "Assembling Analysis Run Script"
        if [ "$loopcount_anal" -eq "$analscanpoints" ]; then
         ForceRun="T"
        else
         ForceRun="F"
        fi
        (
          echo "dir=$dir" #
          echo "runhost=$runhost" #
          assemble_runscript $dir $runhost $AnalBin $MPMDnodes $MPMD $PARDIR $ForceRun "anal" $Forcetime
        )
     fi 
#     echo "\$Anal_Array: ${Anal_Array[@]}"
#     echo "\$PROPMAIN= $PROPMAIN"
     if [ -z "$(echo ${Anal_Array[@]} | grep -o "$PROPMAIN")"  ]; then
      echo "All eligible analyses finished for $PROPMAIN"
      Complete_Array=(`xadd $PROPMAIN ${Complete_Array}`)
      echo "Added to \$Complete_Array: ${#Complete_Array[@]} Elements"
     fi
   done

   for dir in ${Complete_Array[@]}; do

    echo "In \$Complete_Array loop: $dir"
    Sum=$(cat $dir/Data/L_z_expectation.dat | awk '{print $1}' | paste -sd+ | bc  )
    dt=$(echo "$AnalFactor*$Output_TimeStep" | bc)
    CheckSum=$(seq 0 $dt $Prop_Time_Final | paste -sd+ | bc  ) 

    if [ $(echo "($Sum-$CheckSum)/$Output_TimeStep" | bc) -ge 0  ]; then
     echo "Full Analysis Complete for $dir" 
     # ADd movie scripts here
     echo "This is where movie scripts will be used when they are ready"
     Complete_Array=(`RmEle $dir ${Complete_Array[@]}`)
     echo "Removed from \$Complete Array: ${#Complete_Array[@]} Elements"
    else
     echo "All eligible analyses complete, but full analysis is incomplete"
     echo "Propagation must not be complete, so we are done for now"
       Complete_Array=(`RmEle $dir ${Complete_Array[@]}`)
     echo "Removed from \$Complete Array: ${#Complete_Array[@]} Elements"
    fi

   done

  echo "Looped through all analyses and checked for completion"
  echo "done until next function call"
#   echo "Sleeping for 20 minutes before redoing the loop"
#   sleep 1200 
#  done 
#elif [ "${Mode:0:4}" == "rela" ]; then
#   echo "Relaxation mode"
#fi

}
