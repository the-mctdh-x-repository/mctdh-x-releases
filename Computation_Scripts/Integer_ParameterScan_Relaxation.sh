#!/bin/bash
shopt -s expand_aliases
source ~/.bashrc


if [ "$#" -ne 4 ]; then
    echo "Script for Parameter scan of integer parameters"
    echo "Syntax: ./Integer_ParameterScan_Relaxation.sh <Parameter to modify> <Start of Scan> <Stop of Scan> <Steps of Scan>"
    echo "Example for scanning the particle number from 10 to 20 in steps of 2:"
    echo "./Integer_ParameterScan_Relaxation.sh Npar 10 20 2"
    echo "The file Integer_ParameterScan.inp has to be present in the working directory."
    echo "The library libmctdhx.so and a binary file as specified in the input file must also reside in the working directory."
    return 1
fi

. $MCTDHXDIR/Scripts/check_convergence.sh
. $MCTDHXDIR/Scripts/relaxation_restart.sh
. $MCTDHXDIR/Scripts/relaxation_newstart.sh

. ./Integer_ParameterScan_Relaxation.inp

if [ ! -f "$binary" ];
  then
    echo "The specified executable $binary does not exist"
    exit 1
fi

if [ ! -f "$Relaxation_Template" ];
  then
    echo "Specified input template $Relaxation_Template does not exist"
    exit 1
fi


PARDIR=$PWD
Parameter=$1

Scan_Start=$2
Scan_Stop=$3
Scan_Step=$4

Scan_Nsteps=`echo "scale=0;($Scan_Stop-$Scan_Start+1)/$Scan_Step" | bc `

ConvCount=0

for i in $(seq $Scan_Start $Scan_Step $Scan_Stop)
  do
    while  [ "$ConvCount" -lt 10 ]; 
      do
        echo "$i"
        compdir=$PARDIR/"$Parameter"_"$i"
        echo "Computation dir: $compdir"
       
        if [ -s "$compdir/PSI_bin" ] && [ -s "$compdir/CIc_bin" ];
           then #RESTART
       
           Count=$(get_convergence $compdir $ConvDig)
       
           if [ "$Count" -lt 10 ] ; # Not Converged, Restart
             then
       
                restart_relaxation $Parameter $i $compdir $Relaxation_Template $Relaxtime $binary
       
                cd $compdir
                ./$binary
       
             else
       
                echo "$compdir is converged" >> $PARDIR/CONVERGED."$Parameter"_"$i"
       
             fi
       
        else # Start New Relaxation
       
           echo "Starting new Relaxation in $compdir"
           newstart_relaxation $Parameter $i $compdir $Relaxation_Template $Relaxtime $binary
       
           cd $compdir
           ./$binary
        fi
        
        cd $PARDIR
       
        ConvCount=$(get_convergence $compdir $ConvDig)

        if [ "$ConvCount" -eq 10 ];
          then
             echo "$compdir is converged" >> $PARDIR/CONVERGED."$Parameter"_"$i"
        fi

      done
      ConvCount=0
  done
