#!/bin/bash
## Oct 3, 2014: the input file was renamed to ParameterScan.inp
## Oct 8, 2014: Script redone by storm
## When relaxations are converged, they are passed into Propagation()
shopt -s expand_aliases
source ~/.bashrc > /dev/null 2>&1

export LANG="en_US"
function Relax()
{
if [ "$#" -ge 2 ]; then
    echo "Script for Parameter scan for relaxations"
    echo "All input variables must be specified in ParameterScan_Relaxation.inp in the same directory"
    echo "The library libmctdhx.so, an input template, and a binary file as specified in the input file must also reside in the working directory."
    exit 1
fi
if [ -n "$1" ]; then
 PARDIR=$1
else
 PARDIR=$PWD
fi
Relax_Array_File=Relax_Array
if [ "$2" != "save" ]; then
#  rm $PARDIR/run*.sh
  rm $PARDIR/*/RunFlag
  rm $PARDIR/$Relax_Array_File
fi
touch $PARDIR/$Relax_Array_File
touch $PARDIR/Sent_To_Prop
. $PARDIR/ParameterScan.inp
. $MCTDHXDIR/Scripts/check_convergence.sh
. $MCTDHXDIR/Scripts/check_integer.sh
. $MCTDHXDIR/Scripts/assemble_runscript.sh
## Added by Storm
. $MCTDHXDIR/../Computation_Scripts/ParameterScan_Propagation.sh
. $MCTDHXDIR/Scripts/Adjust_Inp_Relax.sh
. $MCTDHXDIR/Scripts/readarray.sh
. $MCTDHXDIR/Scripts/ExclusiveAdd.sh
. $MCTDHXDIR/Scripts/RemoveArrayElement.sh
. $MCTDHXDIR/Scripts/IterateParameters_relax.sh


bin=$(echo "$binary" | tr ' ' '\n' | grep MCTDH | sed 's/\.\///')
if [ ! -f "$bin" ] ; then
    echo "The specified executable $binary does not exist"
    exit 1
fi

if [ ! -f "$Relaxation_Template" ] ; then
    echo "Specified input template $Relaxation_Template does not exist"
    exit 1
fi
### Output input file to screen
echo "runhost=" $runhost
echo "MaxNodes=" $MaxNodes
echo "MPMDnodes=" $MPMDnodes
echo "maxjobs=" $maxjobs

echo "NParameters=" $NParameters
echo "Parameter1=" $Parameter1
echo "List1=" $List1         
echo "Parameter1List=" $Parameter1List  
echo "Scan1_Start=" $Scan1_Start
echo "Scan1_Stop=" $Scan1_Stop
echo "Scan1_Step=" $Scan1_Step

echo "Parameter2=" $Parameter2
echo "List2=" $List2         
echo "Parameter2List=" $Parameter2List  
echo "Scan2_Start=" $Scan2_Start
echo "Scan2_Stop=" $Scan2_Stop
echo "Scan2_Step=" $Scan2_Step

echo "Parameter3=" $Parameter3
echo "List3=" $List3         
echo "Parameter3List=" $Parameter3List  
echo "Scan3_Start=" $Scan3_Start
echo "Scan3_Stop=" $Scan3_Stop
echo "Scan3_Step=" $Scan3_Step

echo "Parameter4=" $Parameter4
echo "List4=" $List4         
echo "Parameter4List=" $Parameter4List  
echo "Scan4_Start=" $Scan4_Start
echo "Scan4_Stop=" $Scan4_Stop
echo "Scan4_Step=" $Scan4_Step

echo "Parameter5=" $Parameter5
echo "List5=" $List5         
echo "Parameter5List=" $Parameter5List  
echo "Scan5_Start=" $Scan5_Start
echo "Scan5_Stop=" $Scan5_Stop
echo "Scan5_Step=" $Scan5_Step
### Output input file to screen



#### Processing the input
if [ "$runhost" == "PC" ] && [ "$MPMD" == "T" ]; then
   if [ -f "$PARDIR/1Relax_run_$MonsterName.sh" ]; then
      rm $PARDIR/1Relax_run_$MonsterName.sh
   fi
fi 

if [ "$NParameters" -lt 1 ]; then
   echo "You must specify at least one parameter to scan in ParamterScan_Relaxation.inp"
   exit 1
fi

if [ "$NParameters" -ge 1 ]; then
 if [ "$List1" != "T" ]; then
   a=`check_integer $Scan1_Start`
   b=`check_integer $Scan1_Stop`
   if [ "$a" -ne "0" ] || [ "$b" -ne "0" ] ; then
       SC=10
      else
       SC=0
   fi
   
   Scan1_List=`seq $Scan1_Start $Scan1_Step $Scan1_Stop`
   Scan1_Nsteps=`echo "scale=$SC;($Scan1_Stop-$Scan1_Start+1)/$Scan1_Step" | bc `
 else 
   Scan1_List=`echo $Parameter1List`
 fi
   Scan2_List="EMPTY"
   Scan3_List="EMPTY"
   Scan4_List="EMPTY"
   Scan5_List="EMPTY"
fi

if [ "$NParameters" -ge 2 ]; then
 if [ "$List2" != "T" ]; then
   a=`check_integer $Scan2_Start`
   b=`check_integer $Scan2_Stop`
   if [ "$a" -ne 0 ] || [ "$b" -ne 0 ] ; then
       SC=10
      else
       SC=0
   fi
   Scan2_List=`seq $Scan2_Start $Scan2_Step $Scan2_Stop`
   Scan2_Nsteps=`echo "scale=$SC;($Scan2_Stop-$Scan2_Start+1)/$Scan2_Step" | bc `
 else 
   Scan2_List=`echo $Parameter2List`
 fi
   Scan3_List="EMPTY"
   Scan4_List="EMPTY"
   Scan5_List="EMPTY"
fi
if [ "$NParameters" -ge 3 ]; then
 if [ "$List3" != "T" ]; then
   a=`check_integer $Scan3_Start`
   b=`check_integer $Scan3_Stop`
   if [ "$a" -ne 0 ] || [ "$b" -ne 0 ] ; then
       SC=10
      else
       SC=0
   fi


   Scan3_Nsteps=`echo "scale=$SC;($Scan3_Stop-$Scan3_Start+1)/$Scan3_Step" | bc `
   Scan3_List=`seq $Scan3_Start $Scan3_Step $Scan3_Stop`
 else 
   Scan3_List=`echo $Parameter3List`
 fi

   Scan4_List="EMPTY"
   Scan5_List="EMPTY"

fi


if [ "$NParameters" -ge 4 ]; then
 if [ "$List4" != "T" ]; then 
   a=`check_integer $Scan4_Start`
   b=`check_integer $Scan4_Stop`
   if [ "$a" -ne 0 ] || [ "$b" -ne 0 ] ; then
       SC=10
      else
       SC=0
   fi

   Scan4_Nsteps=`echo "scale=$SC;($Scan4_Stop-$Scan4_Start+1)/$Scan4_Step" | bc `
   Scan4_List=`seq $Scan4_Start $Scan4_Step $Scan4_Stop`
 else 
   Scan4_List=`echo $Parameter4List`
 fi
   Scan5_List="EMPTY"
fi


if [ "$NParameters" -ge 5 ]; then
 if [ "$List5" != "T" ]; then 
   a=`check_integer $Scan5_Start`
   b=`check_integer $Scan5_Stop`
   if [ "$a" -ne 0 ] || [ "$b" -ne 0 ] ; then
       SC=10
      else
       SC=0
   fi

   Scan5_Nsteps=`echo "scale=$SC;($Scan5_Stop-$Scan5_Start+1)/$Scan5_Step" | bc `
   Scan5_List=`seq $Scan5_Start $Scan5_Step $Scan5_Stop`
 else 
   Scan5_List=`echo $Parameter5List`
 fi
fi

ConvCount=0

declare -a Relax_Array      
declare -a Sent_To_Prop
echo "################################################################################"
echo "################################################################################"
echo "################################################################################"
echo "################################################################################"
echo "################################################################################"
echo "###########################Beginning Scan#######################################"
echo "################################################################################"
echo "################################################################################"
echo "################################################################################"
echo "################################################################################"
echo "################################################################################"
## Count the scan points
ScanPoints=0
 for i in ${Scan5_List} ; do
    for j in ${Scan4_List} ; do
      for k in ${Scan3_List} ; do
        for l in ${Scan2_List} ; do
          for m in ${Scan1_List} ; do
            let "ScanPoints++"
          done
        done
      done
    done
  done
echo "Scanpoints: $ScanPoints"

while true  # infinite loop needed in case of cluster
do
 loopcount=0
 for i in ${Scan5_List}; do
  for j in ${Scan4_List}; do
   for k in ${Scan3_List}; do
    for l in ${Scan2_List}; do
     for m in ${Scan1_List}; do
        echo "################################################################################"
        echo "################################################################################"
        echo "Current parameter set: $m $l $k $j $i"

         let "loopcount++"
         if [ "$loopcount" -eq "$ScanPoints" ] ; then
            ForceRun="T"
         else
            ForceRun="F"
         fi
         (
           IterateParameters $m $l $k $j $i
         )
      done
     done
    done
   done
  done

  if [ "$runhost" == "PC" ] ; then # on a PC, we're done now
    Relax_Array=($(readarray $PARDIR/$Relax_Array_File)) 
    if [ "${#Relax_Array[@]}" == "0"  ]; then # all relax finished
     break;
    fi
  else    ## on a cluster, we need to check the contents of Relaxarray
    Relax_Array=($(readarray $PARDIR/$Relax_Array_File)) 
    if [ "${#Relax_Array[@]}" == "0"  ]; then # all relax finished
      break
    fi
    if [ "$runhost" != "inScript" ]; then
       if [ "$runhost" != "nemo" ]; then
         echo "Sleeping 1 hr until next loop"
         sleep 3600 ### wait 1 hour before starting over.
       else
         echo "Sleeping 5 mins until next loop"
         sleep 300 ### wait 1 hour before starting over.
       fi
    fi
  fi
done ## end of infinite loop
}

