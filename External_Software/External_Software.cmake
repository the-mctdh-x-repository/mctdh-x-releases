set(FFTWEXTERN TRUE)
#TRUE: Compile the FFTW software shipped together with MCTDH-X. This will generate the corresponding files in /External_Software/FFTW
#FALSE: Use existing FFTW software installed before MCTDH-X. This will require specification of the path in the following.

set(FFTW_INCLUDE_DIR
#      /usr/include/ # Ubuntu when installed via apt
#      /usr/local/include/ # Ubuntu when downloaded from fftw website
#      /usr/local/Cellar/fftw/3.3.10_1/include/ # MacOS when installed via HomeBrew
#      /opt/homebrew/Cellar/fftw/3.3.10_1/include/ # MacOS when installed via HomeBrew
#      /cluster/apps/fftw/3.3.3/x86_64/gcc_4.8.2/openmpi_1.8.0/include/ # Clusters
#      /cluster/software/stacks/2024-04/spack/opt/spack/linux-ubuntu22.04-x86_64_v3/gcc-8.5.0/fftw-3.3.10-uxr3orjdikto2pysosdb4edpdmacewoj/include     #Euler Ubuntu clusters
)
#To specify the directory of the pre-installed FFTW header files to be included in CMake.
#This directory should contain files like fftw3.f, fftw3.h, fftw3l.f03, etc.


SET(FFTW_LIB
#      /usr/lib/x86_64-linux-gnu/ # Ubuntu when installed via apt
#      /usr/local/lib/ # Ubuntu when downloaded from fftw website
#      /usr/local/Cellar/fftw/3.3.10_1/lib/ # MacOS when installed via HomeBrew
#      /opt/homebrew/Cellar/fftw/3.3.10_1/lib/ # MacOS when installed via HomeBrew
#      /cluster/apps/fftw/3.3.3/x86_64/gcc_4.8.2/openmpi_1.8.0/lib64/ # Clusters
#      /cluster/software/stacks/2024-04/spack/opt/spack/linux-ubuntu22.04-x86_64_v3/gcc-8.5.0/fftw-3.3.10-uxr3orjdikto2pysosdb4edpdmacewoj/lib         #Euler Ubuntu clusters
)
#To specify the directory of the pre-installed FFTW library files to be used in CMake.
#This directory should contain files like libfftw3.a, libfftw3_mpi.a, libfftw3f.a, etc.



set(OpenBLASEXTERN TRUE)
#TRUE: Compile the FFTW software shipped together with MCTDH-X. This will generate the corresponding files in /External_Software/FFTW
#FALSE: Use existing FFTW software installed before MCTDH-X. This will require specification of the path in the following.

set(OpenBLAS_INCLUDE_DIR
#      /usr/include/x86_64-linux-gnu/ # Ubuntu when installed via apt
#      /usr/local/Cellar/openblas/0.3.27/include/ # MacOS when installed via HomeBrew
#      /opt/homebrew/Cellar/openblas/0.3.27/include/ # MacOS when installed via HomeBrew
#      /cluster/apps/openblas/0.2.8_par/x86_64/gcc_4.8.2/include/ # Clusters
#      /cluster/software/stacks/2024-06/spack/opt/spack/linux-ubuntu22.04-x86_64_v3/gcc-12.2.0/openblas-0.3.24-eiewhivju2io7doutnqmhj7rmisarv4b/include/   #Euler Ubuntu clusters (2024-06 stack)    
#      /cluster/software/stacks/2024-04/spack/opt/spack/linux-ubuntu22.04-x86_64_v3/gcc-8.5.0/openblas-0.3.24-f7dk62g2cuxpyy5jfytprtpma6ftr6cu/include/   #Euler Ubuntu clusters (2024-04 stack)
)
#To specify the directory of the pre-installed FFTW header files to be included in CMake.
#This directory should contain files like cblas.h, etc.


SET(OpenBLAS_LIB
#      /usr/lib/x86_64-linux-gnu/ # Ubuntu when installed via apt
#      /usr/local/Cellar/openblas/0.3.27/lib/ # MacOS when installed via HomeBrew
#      /opt/homebrew/Cellar/openblas/0.3.27/lib/ # MacOS when installed via HomeBrew
#      /cluster/apps/openblas/0.2.8_par/x86_64/gcc_4.8.2/lib/ # Clusters
#      /cluster/software/stacks/2024-06/spack/opt/spack/linux-ubuntu22.04-x86_64_v3/gcc-12.2.0/openblas-0.3.24-eiewhivju2io7doutnqmhj7rmisarv4b/lib/    #Euler Ubuntu clusters (2024-06 stack)
#       /cluster/software/stacks/2024-04/spack/opt/spack/linux-ubuntu22.04-x86_64_v3/gcc-8.5.0/openblas-0.3.24-f7dk62g2cuxpyy5jfytprtpma6ftr6cu/lib/   #Euler Ubuntu clusters (2024-04 stack)
)
#To specify the directory of the pre-installed FFTW library files to be used in CMake.
#This directory should contain files like libopenblas.a, etc.



