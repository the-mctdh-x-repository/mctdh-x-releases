#!/bin/bash

shopt -s expand_aliases
source ~/.bashrc

MCTDHXDIR=$PWD/bin

echo "################################################################################"
cat documentation/BANNER.txt 
echo "################################################################################"
cat documentation/GPL_APPL.txt
echo "################################################################################"


#### Check operating system
MacOSFlag=0
UNAME=$(uname)
if [ "${UNAME}" == "Darwin" ]; then
  MacOSFlag=1
  echo "MacOS (Darwin) detected."
  echo -e 'Please first verify x-code is installed properly: \033[36mxcode-select --install\033[m'
  #### Check whether Anaconda is installed
  isthereconda=$(which conda)
  if [[ "$isthereconda" != *"not found"* ]]; then
     echo -e 'Conda installation found! Please deactivate it: \033[36mconda deactivate\033[m'
  #    conda deactivate
  fi 

  #### Check installation with HomeBrew (Deactivated)
  # echo "Check following package installation with HomeBrew: cmake, gcc, open-mpi, gnuplot, ffmpeg?"
  # PS3='Answer:'
  # optionsMAC=("Yes" "No" "Stop")
  # select optMAC in "${optionsMAC[@]}"
  # do
  #   case $optMAC in 
  #   "Yes")
  #     echo "Check package installation in HomeBrew."
  #     brew list cmake &>/dev/null || brew install cmake
  #     brew list gcc &>/dev/null || brew install gcc
  #     brew list open-mpi &>/dev/null || brew install open-mpi
  #     brew list gnuplot &>/dev/null || brew install gnuplot
  #     brew list ffmpeg &>/dev/null || brew install ffmpeg
  #     break;;
  #   "No")
  #     break;;
  #   "Stop")
  #    exit 1
  #    ;;
  #   esac
  # done
elif [ "${UNAME}" == "Linux" ]; then
  echo "Linux detected."
  MacOSFlag=0
else
  echo "MCTDH-X is not compatible with the current operating system: ${UNAME}"
  exit 1
fi




#### Query user for libraries to use for the compilation.
get_cflags()
{
PS3='Please select compiler:'
options=("intel" "GNU" )
select opt in "${options[@]}"
do
    case $opt in
    "intel") 
        echo "Setting CMP_ID=Intel"
        cmp="CMP_ID=Intel"
        binary_suffix="intel"
        export F77="ifort"
        break
        ;;
    "GNU")
        echo "Setting CMP_ID=GNU"
        cmp="CMP_ID=GNU"
        export F77="gfortran"
        binary_suffix="gcc"
        break
    ;; 
    *) echo invalid option;;
    esac
done

PS3='Please select FFT library:'
options=("FFTW" "MKLFFT" "FFTW with MPI support")
select opt in "${options[@]}"
do
    case $opt in
    "FFTW") 
        echo "Setting FFTLIB=FFTW"
        FFT="FFTLIB=FFTW"
        break
        ;;
    "MKLFFT")
        echo "Setting FFTLIB=MKLFFT"
        FFT="FFTLIB=MKLFFT"
        break
        ;; 
    "FFTW with MPI support") 
        echo "Setting FFT=MPIFFTW"
        FFT="FFTLIB=MPIFFTW"
        break
        ;;

        *) echo invalid option;;
    esac
done
PS3='Please select which LAPACK; Attention: For PGI compilers, only MKL and acml is supported.'
options=("openblas" "MKL")
select opt in "${options[@]}"
do
    case $opt in
    "openblas") 

        echo "Setting lapack=OPENBLAS"
        lapack="lapack=OPENBLAS"
        break
        ;;
    "MKL")
        echo "Setting lapack=MKL"
        lapack="lapack=MKL"
        break
        ;; 
        *) echo invalid option;;
    esac
done
}

#### Query for platform
PS3='This is the MCTDH-X installation script. Please choose your build from the above:'

options=("FFTW+OpenBlas+GCC+mpif90 (without MPI)" "FFTW(MPI)+OpenBlas+GCC+mpif90 (with MPI)" "MKLFFT+MKL+Intel+mpif90" "Custom")

select opt in "${options[@]}"
do
    case $opt in
       "FFTW+OpenBlas+GCC+mpif90 (without MPI)")
            echo "FFTW+OpenBlas+GCC+mpif90 (without MPI)"
            echo "Installing to directory:" 
            echo $MCTDHXDIR

            binary_suffix="gcc"
            compile="cmake -DCMP_ID=GNU -DFFTLIB=FFTW lapack=OPENBLAS . && OMPI_FC=gfortran make -j"
            echo "Build command:"
            echo $compile
            eval $compile
            break
            ;;
       "FFTW(MPI)+OpenBlas+GCC+mpif90 (with MPI)")
            echo "FFTW(MPI)+OpenBlas+GCC+mpif90 (with MPI)"
            echo "Installing to directory:" 
            echo $MCTDHXDIR

            binary_suffix="gcc"
            compile="cmake -DCMP_ID=GNU -DFFTLIB=MPIFFTW lapack=OPENBLAS . && OMPI_FC=gfortran make -j"
            echo "Build command:"
            echo $compile
            eval $compile
            break
            ;;
       "MKLFFT+MKL+Intel+mpif90")
            echo "you chose MKLFFT+MKL+Intel+mpif90"
            echo "Installing to directory:" 
            echo $MCTDHXDIR

            binary_suffix="intel"

            compile="cmake -DCMP_ID=Intel -DFFTLIB=MKLFFT -DHOSTNAME=$HOSTNAME -Dlapack=MKL . && I_MPI_F90=ifort OMPI_FC=ifort make -j"
            
            if [ -z "$MKLROOT" ] && [ -z "$MKL_ROOT" ]; 
              then
                echo "You selected to use the MKL FFTs but your \$MKLROOT environment variable is empty.\n
                      If you indeed have MKL installed, please make \$MKLROOT point to its directory (there is a script \"mklvars.sh\" supplied with intel MKL to define useful environment variables. \n
                      If you do not have MKL, select to use openblas as LAPACK."
                exit 1
            fi

            echo "Build command:"
            echo $compile
            eval $compile

            break
            ;;
        "Custom")
            echo "you chose Custom, querying for libraries to use/link"
            echo "Installing to directory:" 
            echo $MCTDHXDIR

            get_cflags

            compile="cmake -D$cmp -D$FFT -D$lapack -DHOSTNAME=$HOSTNAME . && I_MPI_F90=$F77 OMPI_FC=$F77 make -j"
            echo "Build command:"
            echo $compile
            eval $compile

            break
            ;;
        "Quit")
            break
            ;;
        *) echo invalid option;;
    esac
done


type -P gnuplot  && echo "Found gnuplot in your installation, using it for visualization scripts." && GNUPLOT='X' || { echo "WARNING: It seems that your system does not have gnuplot; visualization scripts won't work..." 1>&2; GNUPLOT="T"; }




if [ "$GNUPLOT" == "T" ];
then

echo "Gnuplot is missing in your environment; visualization scripts won't work..."
echo "Gnuplot is missing in your environment; visualization scripts won't work..."
echo "Gnuplot is missing in your environment; visualization scripts won't work..."
echo "Gnuplot is missing in your environment; visualization scripts won't work..."

fi

#type -P hg >/dev/null && echo "Found a Mercurial installation." && MERCURIAL='X' || { echo "WARNING: It seems that your system does not have mercurial; version management will not be available..." 1>&2; MERCURIAL="T"; }

#if [ "$MERCURIAL" == "T" ];
#then

#echo "Mercurial missing; version management will not be available..."
#echo "Mercurial missing; version management will not be available..."
#echo "Mercurial missing; version management will not be available..."
#echo "Mercurial missing; version management will not be available..."

#fi

type -P mencoder >/dev/null && echo "Found an mencoder installation." && MENCODER='X' || { echo "WARNING: It seems that your system does not have mencoder; video visualizations will not work." 1>&2; MENCODER="T"; }

if [ "$MENCODER" == "T" ];
then

echo "Mencoder missing; video visualizations will not work..."
echo "Mencoder missing; video visualizations will not work..."
echo "Mencoder missing; video visualizations will not work..."
echo "Mencoder missing; video visualizations will not work..."

fi

echo "Writing aliases and paths to your .mctdhxrc"
echo "Writing aliases and paths to your .mctdhxrc"
echo "Writing aliases and paths to your .mctdhxrc"

BASHRC=~/.mctdhxrc

echo "############## MCTDH-X path and aliases ###########" > $BASHRC
echo "############## MCTDH-X path and aliases ###########" >> $BASHRC
echo "############## MCTDH-X path and aliases ###########" >> $BASHRC
echo "export MCTDHXDIR=$MCTDHXDIR" >> $BASHRC
echo "PATH=\$PATH:$MCTDHXDIR:$MCTDHXDIR/Scripts/:$MCTDHXDIR/../Visualization_Scripts:$MCTDHXDIR/../Mercurial_Scripts" >> $BASHRC
echo "alias MCTDHX='MCTDHX_$binary_suffix'"  >> $BASHRC
echo "alias MCTDHX_analysis='MCTDHX_analysis_$binary_suffix'"  >> $BASHRC
echo "alias cdmx='cd $PWD'" >> $BASHRC
echo "alias bincp='cp $PWD/bin/MCTDHX_* . '" >> $BASHRC
echo "alias inpcp='cp $PWD/Input_Examples/MCTDHX.inp $PWD/Input_Examples/analysis.inp .'" >> $BASHRC
if [[ $MacOSFlag -eq 1 && $binary_suffix=="gcc" ]]; then
  echo "alias x-make='cd $MCTDHXDIR/../ && $gccActivate && $compile && cd -'" >> $BASHRC
else
  echo "alias x-make='cd $MCTDHXDIR/../ && $compile && cd -'" >> $BASHRC
fi
echo "alias x-clean='cd $MCTDHXDIR/../ && make clean && find . -iwholename \"*cmake*\" -not -name CMakeLists.txt -delete && cd -'" >> $BASHRC
echo "alias x-purge='cd $MCTDHXDIR/../ && find . -iwholename \"*cmake*\" -not -name CMakeLists.txt -delete && rm -rf External_Software/BLAS* External_Software/FFTW* && rm -rf Makefile project_openblas* cd -'" >> $BASHRC

echo "gnuplot=gnuplot" > ./bin/Scripts/dependencies
echo "mencoder=mencoder" >> ./bin/Scripts/dependencies
echo "mplayer=mplayer" >> ./bin/Scripts/dependencies
#echo "hg=hg" >> ./bin/Scripts/dependencies

if [ "$cmp" == "CMP_ID=GNU" ];
  then
    echo "export OMPI_FC=gfortran" >> $BASHRC
fi

if [ "$cmp" == "CMP_ID=Intel" ];
 then
    echo "export OMPI_FC=ifort" >> $BASHRC
    echo "export I_MPI_FC=ifort" >> $BASHRC
fi


echo "export OMP_STACKSIZE=1000M" >> $BASHRC
echo "export OMP_NUM_THREADS=1" >> $BASHRC
if [ $MacOSFlag -eq 0 ]; then
echo "ulimit -s unlimited" >> $BASHRC
fi
echo "############## MCTDH-X path and aliases ###########" >> $BASHRC
echo "############## MCTDH-X path and aliases ###########" >> $BASHRC
echo "############## MCTDH-X path and aliases ###########" >> $BASHRC


if [ $MacOSFlag -eq 1 ]; then
  if [ -f ~/.bash_profile ]; then
    cp ~/.bash_profile ~/.bash_profile.OLD
    sed -e "/#>MCTDH-X/,/#<MCTDH-X/d" ~/.bash_profile.OLD > ~/.bash_profile
    rm ~/.bash_profile.OLD
  fi
echo "#>MCTDH-X #########################################" >> ~/.bash_profile
echo "source $BASHRC"                                       >> ~/.bash_profile
echo "#<MCTDH-X #########################################" >> ~/.bash_profile

else
  if [ -f ~/.bashrc ]; then 
     cp ~/.bashrc ~/.bashrc.OLD
     sed -e "/#>MCTDH-X/,/#<MCTDH-X/d" ~/.bashrc.OLD > ~/.bashrc 
     rm ~/.bashrc.OLD
  fi
echo "#>MCTDH-X #########################################" >> ~/.bashrc
echo "source $BASHRC"                                       >> ~/.bashrc
echo "#<MCTDH-X #########################################" >> ~/.bashrc
fi

source $BASHRC
source $BASHRC

echo "To complete the Installation, restart the shell to make all the links and aliases work and compile the program with x-make!"
echo "To complete the Installation, restart the shell to make all the links and aliases work and compile the program with x-make!"
echo "If you want to use OpenMP/shared-memory parallelization, please make sure that the environment variable OMP_NUM_THREADS is set to the number of cores in your system."
