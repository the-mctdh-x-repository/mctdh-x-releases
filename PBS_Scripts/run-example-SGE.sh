#!/bin/bash
#MSUB -l nodes=1:ppn=20
#MSUB -l walltime=0:05:00
#MSUB -l pmem=6000mb
#MSUB -v MPI_MODULE=mpi/ompi
#MSUB -N test_ompi_omp
 
source ~/.bashrc

export I_MPI_COMPATIBILITY=4
export OMP_NUM_THREADS=5
TASK_COUNT=$((${MOAB_PROCCOUNT}/${OMP_NUM_THREADS}))
echo "${MOAB_SUBMITDIR}/MCTDHX_intel running on ${MOAB_PROCCOUNT} cores with ${TASK_COUNT} MPI-tasks and ${OMP_NUM_THREADS} threads"

cd $MOAB_SUBMITDIR
mpiexec.hydra -envall -n ${TASK_COUNT} ./MCTDHX_intel


