#!/bin/bash
shopt -s expand_aliases
source ~/.bashrc > /dev/null 2>&1
. $MCTDHXDIR/Scripts/copy_bash_array.sh

#### function del_job
#### arguments: 1) "todelete" is the string which is eliminated from the job array
####            2) "runhost" is the hostname of the system.
####            3) "rlxarray" is the array of jobs (strings containing job directories)

function del_job()
{
    todelete=$1
    runhost=$2
    name=$3[@]
    rlxarray=("${!name}")

    if [ "$runhost" == "maia" ];
       then
          joblist=`qstat | sed -n '/[0-9][0-9].*/{p;}' | awk '{print $1}'`   # determine list of running jobs        
          for p in ${joblist} ;
            do
              if [ "$forhost" == "maia" ];
                 then
                 workdir=`qstat -j $p | grep sge_o_workdir | sed 's/sge_o_workdir://' | sed -e 's/^[ \t]*//'`
              elif [ "$forhost" == "bwgrid" ];
                 then
                 workdir=`qstat -f $p | grep init_work_dir | sed 's|init_work_dir\ =\ ||' |  sed -e 's/^[ \t]*//'`
              elif [ "$forhost" == "hermit" ];
                 then
                 workdir=`qstat -f $p | grep -A1 Output_Path | sed ':a;N;$!ba;s/\n//g' | sed 's|Output_Path\ =\ ||' | sed 's/^[ \t]*//' | sed 's/^[^\/]*\//\//' | awk '{$1=$1}{print}' | sed 's/ //g' | sed 's/\.o.*$//g'`
              fi

              if [ "$workdir" == "$todelete" ];
                 then
                   if [ "$forhost" == "maia" ];
                     then

                     qstat -j $p &> delTMP
                     wait $!
                     unset notrunning
                     notrunning=`cat delTMP | grep "Following jobs do not exist"`

                   elif [ "$forhost" == "bwgrid" ] || [ "$forhost" == "hermit" ];
                     then

                     notrunning=`qstat -f $p | grep "Unknown Job Id"`

                   fi
    
                   if [[ -z $notrunning ]] ;                                                                
                      then
                      qdel $p
                      wait $!
                   fi  

                   rm delTMP
                   unset notrunning                                                                                      

              fi
          done
    fi

    unset Temp_Array                                                                          
    declare -a Temp_Array                                                                     

    for q in  ${rlxarray[@]} ;
    do
     if [ "$q" != "$todelete" ] ;                                                              
     then                                                                                     
      Temp_Array[temp_index]=$q
     fi
    done
    #Recreate remaining Array
    rlxarray=()                                                                            
    cp_hash "Temp_Array" "Relax_Array"                                                        
    unset Temp_Array                    
    return 0
}


