#!/bin/bash

function restart_relaxation()
{
if [ "$#" -ne 6 ]; then
    echo "relaxation_restart: Illegal number of parameters"
    echo "Syntax: restart_relaxation <Parameter to modify> <Value of parameter> <Computation directory> <Input file template> <final relaxation time> <executable to use>"
    return 1
fi

PARDIR=$PWD
# Parameter to modify
Par=$1
# Value of the parameter to set
Value=$2
# Computation directory
compdir=$3
# Input Template
template=$4
#final Time of relaxation
finaltime=$5
#binary to use
binary=$6

sed 's/.*'"$Par"'.*/'"$Par"' = '"$Value"'/g' $PARDIR/$template > $PARDIR/MCTDHX.tmp1
sed 's/.*GUESS='\''HAND'\''.*/GUESS='\''BINR'\''/g' $PARDIR/MCTDHX.tmp1 > $PARDIR/MCTDHX.tmp2

STOP_TIME=`cat $compdir/NO_PR.out | tail -n 1 | awk '{print $1}' | cut -c 1-6`
## Rounds to the last Output timestep
Output_TimeStep=`cat $compdir/MCTDHX.inp | grep 'Output_TimeStep=' | sed 's/Output_TimeStep=//' | sed 's/!.*//' | sed 's/d0.*//'`
TimeSteps=`echo "scale=0;$STOP_TIME/$Output_TimeStep" | bc `
STOP_TIME=`echo "scale=8;$TimeSteps*$Output_TimeStep" | bc `

echo "Restarting at $STOP_TIME"
sed 's/.*Binary_Start_Time.*/Binary_Start_Time='$STOP_TIME'/g' $PARDIR/MCTDHX.tmp2 > $PARDIR/MCTDHX.tmp3      
sed 's/.*Time_Final.*/Time_Final='"$finaltime"'/g' $PARDIR/MCTDHX.tmp3 > MCTDHX.inp
echo "Found non-converged Relaxation in $compdir -- Restarting from $STOP_TIME"

cp $PARDIR/MCTDHX.inp $compdir
cp $PARDIR/$binary $PARDIR/libmctdhx.so $compdir

}
