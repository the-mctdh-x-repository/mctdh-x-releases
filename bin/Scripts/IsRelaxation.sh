#!/bin/bash

# I take a directory as argument and output 1 if it contains a propagation and 0 if it contains a relaxation


function IsRelaxation()
{
if [ $# -lt 1 ] ; then
  echo "Usage: IsRelaxation=(\`IsRelaxation \$Dir \`)"
  return 2
fi

if  [ ! -f $1/MCTDHX.inp ]; then
  echo "No MCTDHX.inp file found in directory!!!"
  return 2
fi

result=$(echo "`cat $1/MCTDHX.inp | grep -i "job_prefactor" | sed 's/[^0-9\-\.\,d]*//g' | cut -c 1-3` == 0" | bc)

echo $result

}
