#!/bin/bash

shopt -s expand_aliases
source ~/.bashrc

# this function takes 6 input arguments and generates a file list named TMP1 needed 
# for the generation of a movie
# $1 from time
# $2 to time
# $3 time step
# $4 number of orbitals
# $5 number of particles
# $6 file suffix

function getlist {

if [ "$6" == "orbs.dat" ];
then
  FILE_LIST="`ls *$6 | sort -g | sed s/orbs.dat//`"
elif [ "$6" == "coef.dat" ]; 
then
  FILE_LIST="`ls *$6 | sort -g | sed s/coef.dat//`"
  echo "It's coefficients!!!"
else
  FILE_LIST="`ls *$6 | sort -g | sed s/N\$5M\$4\$6//`"
fi

IFS=$'\n'
((j=0))
for i in ${FILE_LIST}
do 
((j=j+1))
A=$i
B=$3
C="`echo $A%$B | bc`"
if [ "$C" == "0" ]
then
  if [ $(echo "$A >= $1"|bc) -eq 1 ]
  then
    if [ $(echo "$A <= $2"|bc) -eq 1 ]
    then
      if [ "$6" == "orbs.dat" ];
      then
        echo "$A""orbs.dat" >> TMP
      elif [ "$6" == "coef.dat" ]; 
        then
        echo "$A""coef.dat" >> TMP
      else
        echo "$A""N$5M$4$6" >> TMP
      fi
    fi
  fi
fi

done

echo "`cat TMP | sort -g `" > TMP1
echo "`cat TMP1`"

rm TMP
}
