#!/bin/bash
source ~/.mctdhxrc
shopt -s expand_aliases


#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#!!! DATA-MINER SCRIPT !!!!!!!!!!!!!!!!!
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#! This script parses all the subdirectory of the current
#! directory for MCTDHX calculations and applies the 
#! analysis program to them. The configuration is interactive.
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

rm -f prop.1 prop.2


DATASETS=`find ./ -name "NO_PR.out"`
#echo $DATASETS
PARDIR=`pwd`

USETEMPLATE="no"

template="analysis.inp"
PSI=".T."
FTPSI=".T."
CIC=".T."
dilation="1" 
autodilation="F"
kdip="0.00001"        
MErho=".F."
MEH=".F."
geta=".F."        
entr=".F."        
entrN=".F."       
entr2=".F."       
cavityO=".F."     
XD=".F."
KD=".F."
NON=".F."
XST="-3.0"
XSP="3.0"
YST="-14.0"
YSP="0.0"
ZST="0.0"
ZSP="0.0"
PHAS=".F."
GRAD=".F."
FCORRX=".F."
FCORRK=".F."
SCORRX=".F."
SCORRK=".F."
RCORR1X=".F."
xini1="0.0"
xfin1="0.0"
xpts1="0"
RCORR1K=".F."
kini1="0.0"
kfin1="0.0"
kpts1="0"
RCORR2X=".F."
xini2="0.0"
xfin2="0.0"
xpts2="0"
RCORR2K=".F."
kini2="0.0"
kfin2="0.0"
kpts2="0"
loss=".F."
border="0.0"
MOM2D=".F."
REAL2D=".F."
x1const=".T."
x1slice="0.0"
x2const=".T."
x2slice="0.0"
y1const=".F."
y1slice="0.0"
y2const=".F."
y2slice="0.0"
veff2d=".F."                        
LZ=".F."
step=1.0
quit="no"

while [ $quit != "yes" ]
do
echo "Which analysis setting do you want to change?
0)  Start analysis using this template file (all other parameters are overridden): $template
1)  Output of orbitals: $PSI
2)  Output of Fourier-transformed orbitals: $FTPSI
3)  Output of coefficients, CURRENT: $CIC
4)  Output of matrix elements of the reduced one- and two-body density matrix: $MErho
5)  Output of matrix elements of the one- and two-body Hamiltonian: $MEH
6)  Output of matrix elements of the A2=W_kkjj and A1=W_jjjj elements of the two-body Hamiltonian: $geta
7)  By which factor to increase the resolution of k-space: $dilation 
8)  Automatically increase the resolution of k-space: $autodilation 
9)  Threshold for automatic increase of resolution of k-space: $kdip
10) Output of entropies: $entr
11) Output of Nbody-entropy (maybe expensive!): $entrN
12) Output of two-body entropy: $entr2
13) Computation of cavity order-parameter: $cavityO
14) Output of real space density: $XD 
15) Output of momentum space density: $KD 
16) Output of nonescape probability: $NON 
17) X-Starting point for nonesc. prob.: $XST
18) X-Stopping point for nonesc. prob.: $XSP
19) Y-Starting point for nonesc. prob.: $YST
20) Y-Stopping point for nonesc. prob.: $YSP
21) Z-Starting point for nonesc. prob.: $ZST
22) Z-Stopping point for nonesc. prob.: $ZSP
23) Full output of Correlations in real space: $FCORRX
24) Full output of Correlations in momentum space: $FCORRK
25) Skew output of Correlations in real space: $SCORRX
26) Skew output of Correlations in momentum space: $SCORRK
27) g^{(1)} on restricted real space: $RCORR1X
28) start of g^{(1)} on restricted real space: $xini1
29) stop of g^{(1)} on restricted real space: $xfin1
30) number of points for g^{(1)} on restricted real space: $xpts1
31) g^{(1)} on restricted momentum space: $RCORR1K
32) start of g^{(1)} on restricted momentum space: $kini1
33) stop of g^{(1)} on restricted momentum space: $kfin1
34) number of points for g^{(1)} on restricted momentum space: $kpts1
35) g^{(2)} on restricted real space: $RCORR2X
36) start of g^{(2)} on restricted real space: $xini2
37) stop of g^{(2)} on restricted real space: $xfin2
38) number of points for g^{(2)} on restricted real space: $xpts2
39) g^{(2)} on restricted momentum space: $RCORR2K
40) start of g^{(2)} on restricted momentum space: $kini2
41) stop of g^{(2)} on restricted momentum space: $kfin2
42) number of points for g^{(2)} on restricted momentum space: $kpts2
43) two-boson loss-operators: $loss
44) two-boson loss-operators boundary: $border
45) Correlation/Reduced density slices in 2D real space (RDM2DX): $REAL2D
46) Correlation/Reduced density slices in 2D momentum space (RDM2DK): $MOM2D
47) Keep first coordinate of RDM2DX constant: $x1const
48) Keep first coordinate of RDM2DX constant at value: $x1slice
49) Keep 2nd coordinate of RDM2DX constant: $y1const
50) Keep 2nd coordinate of RDM2DX constant at value: $y1slice
51) Keep 3rd coordinate of RDM2DX constant: $x2const
52) Keep 3rd coordinate of RDM2DX constant at value: $x2slice
53) Keep 4th coordinate of RDM2DX constant: $y2const
54) Keep 4th coordinate of RDM2DX constant at value: $y2slice
55) Calculate effective 1d potentials: $veff2d
56) Output of angular momentum matrix elements and expectation: $LZ
57) Adjust the time-step of the analysis: $step
58) Output of orbital and average phases: $PHAS
59) Output of orbital and average phase gradients: $GRAD
60) Done configuring -- run the analysis already!"

echo -n " Your choice? : "
read choice

case $choice in
0)  echo "enter the value:";  read template ; USETEMPLATE="yes"; quit="yes"     ;;
1)  echo "enter the value:";  read PSI                           ;;
2)  echo "enter the value:";  read FTPSI                           ;;
3)  echo "enter the value:";  read CIC                           ;;
4)  echo "enter the value:";  read MEH                           ;;
5)  echo "enter the value:";  read MErho                           ;;
6)  echo "enter the value:";  read geta                          ;;
7)  echo "enter the value:";  read dilation                      ;;
8)  echo "enter the value:";  read autodilation                      ;;
9)  echo "enter the value:";  read kdip                      ;;
10)  echo "enter the value:";  read entr                      ;;
11)  echo "enter the value:";  read entrN                      ;;
12)  echo "enter the value:";  read entr2                      ;;
13)  echo "enter the value:"; read cavityO                      ;;
14)  echo "enter the value:"; read XD                            ;;
15)  echo "enter the value:"; read KD                            ;;
16)  echo "enter the value:"; read NON                           ;;
17)  echo "enter the value:"; read XST                           ;;
18)  echo "enter the value:"; read XSP                           ;;
19)  echo "enter the value:"; read YST                           ;;
20)  echo "enter the value:"; read YSP                           ;;
21)  echo "enter the value:"; read ZST                           ;;
22)  echo "enter the value:"; read ZSP                           ;;
23)  echo "enter the value:"; read FCORRX                        ;;
24)  echo "enter the value:"; read FCORRK                        ;;
25)  echo "enter the value:"; read SCORRX                        ;;
26)  echo "enter the value:"; read SCORRK                        ;;
27)  echo "enter the value:"; read RCORR1X                       ;;
28)  echo "enter the value:"; read xini1                         ;;
29)  echo "enter the value:"; read xfin1                         ;;
30)  echo "enter the value:"; read xpts1                           ;;
31)  echo "enter the value:"; read RCORR1K                       ;;
32)  echo "enter the value:"; read kini1                         ;;
33)  echo "enter the value:"; read kfin1                         ;;
34)  echo "enter the value:"; read kpts1                           ;;
35)  echo "enter the value:"; read RCORR2X                       ;;
36)  echo "enter the value:"; read xini2                         ;;
37)  echo "enter the value:"; read xfin2                         ;;
38)  echo "enter the value:"; read xpts2                           ;;
39)  echo "enter the value:"; read RCORR2K                       ;;
40)  echo "enter the value:"; read kini2                         ;;
41)  echo "enter the value:"; read kfin2                         ;;
42)  echo "enter the value:"; read kpts2                           ;;
43)  echo "enter the value:"; read loss                          ;;
44)  echo "enter the value:"; read border                        ;;
45)  echo "enter the value:"; read MOM2D                         ;;
46)  echo "enter the value:"; read REAL2D                       ;;
47)  echo "enter the value:"; read x1const                      ;;
48)  echo "enter the value:"; read x1slice                      ;;
49)  echo "enter the value:"; read x2const                      ;;
50)  echo "enter the value:"; read x2slice                      ;;
51)  echo "enter the value:"; read y1const                      ;;
52)  echo "enter the value:"; read y1slice                      ;;
53)  echo "enter the value:"; read y2const                      ;;
54)  echo "enter the value:"; read y2slice                      ;;
55)  echo "enter the value:"; read veff2d                        ;;
56)  echo "enter the value:"; read LZ                            ;;
57)  echo "enter the value:"; read step                          ;;
58)  echo "enter the value:"; read PHAS                          ;;
59)  echo "enter the value:"; read GRAD                          ;;
60)  quit="yes"                               ;;
*) echo "\"$choice\" is not valid "; sleep 2 ;;
esac
done


if [ "$USETEMPLATE" == "no" ]; 
then 
echo "&ZERO_body" > analysis.inp
echo " Orbitals_Output=     $PSI             " >> analysis.inp
echo " Total_Energy = .T. " >> analysis.inp
echo " FTOrbitals_Output=     $FTPSI         " >> analysis.inp
echo " HamiltonianElements_Output=     $MEH         " >> analysis.inp
echo " MatrixElements_Output=     $MErho         " >> analysis.inp
echo "GetA= $geta" >> analysis.inp
echo "dilation=$dilation" >> analysis.inp
echo "autodilation=$autodilation" >> analysis.inp
echo "kdip=$kdip" >> analysis.inp
echo " Coefficients_Output=     $CIC             " >> analysis.inp
echo " Time_From=              " >> analysis.inp
echo " Time_to=               " >> analysis.inp
echo " Time_Points=             " >> analysis.inp
echo " /                       " >> analysis.inp
echo " &ONE_body               " >> analysis.inp
echo " Density_x=           $XD            " >> analysis.inp
echo " Density_k=           $KD            " >> analysis.inp
echo " Pnot=           $NON           " >> analysis.inp
echo " xstart=         $XST        " >> analysis.inp
echo " xend=           $XSP        " >> analysis.inp
echo " ystart=         $YST        " >> analysis.inp
echo " yend=           $YSP        " >> analysis.inp
echo " zstart=         $ZST        " >> analysis.inp
echo " zend=           $ZSP        " >> analysis.inp
echo " Phase=          $PHAS        " >> analysis.inp
echo " Gradient=       $GRAD        " >> analysis.inp
echo " Entropy=       $entr        " >> analysis.inp
echo " NBody_C_Entropy=       $entrN        " >> analysis.inp
echo " TwoBody_Entropy=       $entr2        " >> analysis.inp
echo " /                       " >> analysis.inp
echo " &TWO_body               " >> analysis.inp
echo " Correlations_X=        $FCORRX          " >> analysis.inp
echo " Correlations_K=        $FCORRK         " >> analysis.inp
echo " SkewCorr1D_X=        $SCORRX          " >> analysis.inp
echo " SkewCorr1D_K=        $SCORRK         " >> analysis.inp
echo " corr1restr=    $RCORR1X        " >> analysis.inp
echo " xini1=         $xini1       " >> analysis.inp
echo " xfin1=         $xfin1         " >> analysis.inp
echo " xpts1=         $xpts1         " >> analysis.inp
echo " corr1restrmom= $RCORR1K        " >> analysis.inp
echo " kxini1=        $kini1        " >> analysis.inp
echo " kxfin1=        $kfin1         " >> analysis.inp
echo " kpts1=         $kpts1          " >> analysis.inp
echo " corr2restr=    $RCORR2X        " >> analysis.inp
echo " xini2=         $xini2       " >> analysis.inp
echo " xfin2=         $xfin2         " >> analysis.inp
echo " xpts2=         $xpts2         " >> analysis.inp
echo " corr2restrmom= $RCORR2K        " >> analysis.inp
echo " kxini2=        $kini2        " >> analysis.inp
echo " kxfin2=        $kfin2         " >> analysis.inp
echo " kpts2=         $kpts2         " >> analysis.inp
echo " lossops=       $loss           " >> analysis.inp
echo " border=        $border        " >> analysis.inp
echo " /                       " >> analysis.inp
echo " &TWO_D                  " >> analysis.inp
echo " MOMSPACE2D=    $MOM2D          " >> analysis.inp
echo " REALSPACE2D=   $REAL2D         " >> analysis.inp
echo " x1const=       $x1const        " >> analysis.inp
echo " x1slice=       $x1slice       " >> analysis.inp
echo " y1const=       $x2const       " >> analysis.inp
echo " y1slice=       $x2slice      " >> analysis.inp
echo " x2const=       $y1const      " >> analysis.inp
echo " x2slice=       $y1slice      " >> analysis.inp
echo " y2const=       $y2const      " >> analysis.inp
echo " y2slice=       $y2slice      " >> analysis.inp
echo " PROJ_X=        $veff2d        " >> analysis.inp
echo "                               " >> analysis.inp
echo " DIR='B'                " >> analysis.inp
echo " L_Z=           $LZ     " >> analysis.inp
echo " /                      " >> analysis.inp
echo "     " >> analysis.inp

elif [ "$USETEMPLATE" == "yes" ];
  then
    if [ ! -f $template ];
       then
       echo "Specified analysis.inp template $template does not exist!!!"
       exit 1
    fi
fi


for d in $DATASETS
 do 
  echo $PWD
  STARTTIME=`head -n 1 $d | sed s'/E\-[0-9][0-9]//' | awk '{ print $1 } '`
  STRT_TMP=`echo "$STARTTIME/1" | bc`
  STRT=`echo " scale=6; $STRT_TMP/1+1" | bc`
  echo $STRT 
  echo $PWD
  STOPTIME=`tail -n 1 $d | sed s'/E\-[0-9][0-9]//' | awk '{ print $1 } '`
  STP_TMP=`echo "$STOPTIME/1" | bc`
  STP=`echo " scale=6; $STP_TMP/1" | bc`
  echo $STP 
  NPOINTS=`echo "scale=0; ($STP-$STRT)/$step+1" | bc`
  echo "NPOINTS IS " "$NPOINTS"

# isrelaxation is 1 for a propagation and 0 for a relaxation
  isrelaxation=$(echo "`cat MCTDHX.inp | grep -i "job_prefactor" | sed 's/[^0-9\-\.\,d]*//g' | cut -c 1-3` == 0" | bc)

  if [ "$isrelaxation" -eq "0" ]; 
    then
    echo "found relaxation, using last point in time only."
    STRT=$STP
    NPOINTS=1
  fi
 
  if [ "$USETEMPLATE" == "yes" ]; 
    then
#      cat $template | sed '4s/.*/Time_From='$STRT'/' > prop.1
      cp $template analysis.inp
  elif [ "$USETEMPLATE" == "no" ];
    then
      cat analysis.inp | sed '4s/.*/Time_From='$STRT'/' > prop.1
      cat prop.1 | sed '5s/.*/Time_To='$STP'/' > prop.2
      cat prop.2 | sed '6s/.*/Time_Points='$NPOINTS'/' > analysis.inp
  fi

  DIR=`echo $d | sed s'/\/NO\_PR\.out//'`
  cp analysis.inp $DIR
  bincp
  cp MCTDHX_analysis_* $DIR
  cd $DIR
  CRAY=`set | grep -i "cray" | wc -l`

    if [ "$CRAY" -ge "5" ]
        then 
          aprun -N 1 -n 1 -d 1 ./MCTDHX_analysis_*
    else
          ./MCTDHX_analysis_*
    fi

   echo $DIR
   cd $PARDIR
 done

