#!/bin/bash
### I add an element to an array if it does already contain it
### Usage: newarr=(`xadd $ele ${arr[@]}`)
### Created by storm on Sept 18, 2014
### Tested by storm on Sept 18, 2014
### updated usage documetnation on Oct 6,2014
function xadd()
{
#declare -a arr=("${!1}")
if [ "$#" -eq 0 ]; then
 echo "Usage: newarr=(\`xadd \$ele \${arr[@]} \`)"
 return
fi

ele=$1
shift
arr=("${@}")
##echo ${@}

##echo "Input array: ${arr[@]}"
##echo "Input array size: ${#arr[@]}"
##echo "New element: $ele"
IsIn_STR=`printf "%s\n" "${arr[@]}" | grep "^${ele}*$"`
    if [ -z "$IsIn_STR" ]  ; then
##        echo "Element detected as unique"
        #Add $ele to $arr
        #echo "new element detected"
        arr[${#arr[@]}]=$ele
     fi  
##echo "New Array:"
 echo ${arr[@]}
 
}
