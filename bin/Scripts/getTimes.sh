#!/bin/bash

### here are a few functions the grab begining and end time values from L_z_expectation.dat and NO_PR.out
### created 10/29/14 by storm
### tested 10/29/14

function getInitialAnalTime()
{
  if [ $# != 1 ]; then
   echo "Usage: getInitialAnalTime \$dir"
   return 2
  fi
  dir=$1
  if [ -s $dir/L_z_expectation.dat ]; then
   time=$( cat $dir/L_z_expectation.dat | head -n 1 | awk '{print $1}' )
   echo $time
  else
   echo "99999"
  fi


}

function getFinalAnalTime()
{
  if [ $# != 1 ]; then
   echo "Usage: getFinalAnalTime \$dir"
   return 2
  fi
  dir=$1
  if [ -s $dir/L_z_expectation.dat ]; then
   time=$( cat $dir/L_z_expectation.dat | tail -n 1 | awk '{print $1}' )
   echo $time
  else
   echo "99999"
  fi
}

function getInitialPropTime()
{
  if [ $# != 1 ]; then
   echo "Usage: getInitialPropTime \$dir"
   return 2
  fi
  dir=$1
  if [ -s $dir/NO_PR.out ] && [ -s $dir/MCTDHX.inp ]; then
   time=$(cat $dir/NO_PR.out | head -n 1 | awk '{print $1}')
#   echo "$time"
   time=${time/[eE]-/*10^-}
#   echo "$time"
   Output_TimeStep=$(cat $dir/MCTDHX.inp | grep -o "Output_TimeStep *= *[0-9]*\.[0-9]*" | sed 's|Output\_TimeStep *=||' | sed 's|d0||')
#   echo "$Output_TimeStep"
   TimeSteps=$(echo "scale=0;$time/$Output_TimeStep" | bc )
#   echo "$TimeSteps"
   time=$(echo "$Output_TimeStep*$TimeSteps" | bc )
   echo $time
  else
   echo "999999"
  fi
}

function getFinalPropTime()
{
  if [ $# != 1 ]; then
   echo "Usage: getFinalPropTime \$dir"
   return 2
  fi
  dir=$1
  if [ -s $dir/NO_PR.out ] && [ -s $dir/MCTDHX.inp ] ; then
   time=$(cat $dir/NO_PR.out | tail -n 1 | awk '{print $1}')
   time=${time/[eE]-/*10^-}
   Output_TimeStep=$(cat $dir/MCTDHX.inp  | grep -o "Output_TimeStep *= *[0-9]*\.[0-9]*" | sed 's|Output\_TimeStep *=||' | sed 's|d0||')
   TimeSteps=$(echo "scale=0;$time/$Output_TimeStep" | bc )
   time=$(echo "$Output_TimeStep*$TimeSteps" | bc )
   echo $time
  else
   echo "999999"
  fi
}
