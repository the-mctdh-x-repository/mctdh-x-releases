#!/bin/bash
source ~/.bashrc > /dev/null 2>&1
## I assemble and manage MPMD runscripts for relaxations
##USAGE: MPMDrun_relax $PARDIR $COMPDIR $runSTR $NUMNODES $ForceRun
## Created by Storm on Sept 29, 2014
## Tested by Storm on Sept 29, 2014


function MPMDrun_relax()
{
PARDIR=$1
COMPDIR=$2
runSTR=$3
NUMNODES=$4
ForceRun=$5



echo "MPMDrun_relax forhost $forhost"
echo "MPMDrun_relax runSTR $runSTR"

if [ $# -le 4 ] ; then
echo "USAGE: MPMDrun_relax \$PARDIR \$COMPDIR \$runSTR \$NUMNODES \$ForceRun"
fi

. $PARDIR/ParameterScan.inp
. $MCTDHXDIR/Scripts/get_NumberOfJobs.sh
if [ "$MPMD" != "T" ]; then
  MPMDnodes=1
fi
#echo "in MPMD_relax!!!!!"
## If no runscripts in the $PARDIR, copy one and strip of the line which runs the program
if [ -z "$(ls $PARDIR/[0-9]*Relax_run_$MonsterName.sh 2> /dev/null)"  ] ; then

 if [ "$forhost" == "maia" ]; then 
   cp $MCTDHXDIR/../PBS_Scripts/run-example-maia.sh $PARDIR/Relax_run_$MonsterName.tmp
   sed 's|mpirun.*||' < $PARDIR/Relax_run_$MonsterName.tmp > $PARDIR/Relax_run_$MonsterName.sh
   jobtime=86400
 elif [ "$forhost" == "hermit" ]; then
   cp $MCTDHXDIR/../PBS_Scripts/run-example-hermit.sh $PARDIR/Relax_run_$MonsterName.tmp
   sed 's|aprun.*||' < $PARDIR/Relax_run_$MonsterName.tmp > $PARDIR/Relax_run_$MonsterName.sh
   jobtime=86400
 elif [ "$forhost" == "hornet" ]; then
   cp $MCTDHXDIR/../PBS_Scripts/run-example-hornet.sh $PARDIR/Relax_run_$MonsterName.tmp
   sed 's|aprun.*||' < $PARDIR/Relax_run_$MonsterName.tmp > $PARDIR/Relax_run_$MonsterName.sh
   jobtime=86400
 elif  [ "$forhost" == "brutus" ] ||  [ "$forhost" == "euler" ]; then
   cp $MCTDHXDIR/../PBS_Scripts/run-example-brutus.sh $PARDIR/Relax_run_$MonsterName.tmp
   sed 's|mpirun.*||' < $PARDIR/Relax_run_$MonsterName.tmp > $PARDIR/Relax_run_$MonsterName.sh
   jobtime=86400
 elif [ "$forhost" == "bwgrid" ]; then
   cp $MCTDHXDIR/../PBS_Scripts/run-example-BWGRID.sh $PARDIR/Relax_run_$MonsterName.tmp
   sed 's|mpirun.*||' < $PARDIR/Relax_run_$MonsterName.tmp > $PARDIR/Relax_run_$MonsterName.sh
   jobtime=86400
 elif [ "$forhost" == "nemo" ]; then
   cp $MCTDHXDIR/../PBS_Scripts/run-example-nemo.sh $PARDIR/Relax_run_$MonsterName.tmp
   sed 's|mpiexec.*||' < $PARDIR/Relax_run_$MonsterName.tmp > $PARDIR/Relax_run_$MonsterName.sh
   jobtime=86400
 fi 

 if  [ "$forhost" != "brutus" ] || [ "$forhost" != "euler" ] || [ "$forhost" != "PC" ]; then
  sed 's|nodes=[0-9]*|nodes=0|' < $PARDIR/Relax_run_$MonsterName.sh > $PARDIR/Relax_run_$MonsterName.tmp
  sed 's|#PBS -N.*|#PBS -N MSTR_'$MonsterName'|' < $PARDIR/Relax_run_$MonsterName.tmp > $PARDIR/Relax_run_$MonsterName.sh_
 fi

 if  [ "$forhost" != "PC" ]; then
   rm $PARDIR/Relax_run_$MonsterName.sh
   rm $PARDIR/Relax_run_$MonsterName.tmp
   chmod 755 $PARDIR/Relax_run_$MonsterName.sh_
   echo ". $MCTDHXDIR/Scripts/RunFlagSleeper.sh" >> $PARDIR/Relax_run_$MonsterName.sh_
   echo ". $MCTDHXDIR/Scripts/check_convergence.sh" >> $PARDIR/Relax_run_$MonsterName.sh_
   echo "RunFlagSleeper $jobtime RunFlag &" >> $PARDIR/Relax_run_$MonsterName.sh_
   echo "(" >> $PARDIR/Relax_run_$MonsterName.sh_
   cp $PARDIR/Relax_run_$MonsterName.sh_ $PARDIR/1Relax_run_$MonsterName.sh
 else
   if [ ! -f "$PARDIR/1Relax_run_$MonsterName.sh" ]; then
     touch $PARDIR/1Relax_run_$MonsterName.sh
     chmod 755 $PARDIR/1Relax_run_$MonsterName.sh
     echo "#!/bin/bash" >> $PARDIR/1Relax_run_$MonsterName.sh
     echo ". $MCTDHXDIR/Scripts/check_convergence.sh" >> $PARDIR/1Relax_run_$MonsterName.sh
   fi
 fi
 echo "(" >> $PARDIR/Relax_run_$MonsterName.sh_
 cp $PARDIR/Relax_run_$MonsterName.sh_ $PARDIR/1Relax_run_$MonsterName.sh
fi
### Add $runSTR to the most recent runscript
### find runscript$N.sh (run1.sh, run 2.sh, etc)  that has numnodes less than $MPMDnodes
#qq=$(ls $PARDIR/[0-9]*Relax_run_$MonsterName.sh | sort -rg | head -n 1 )

qq=$(ls $PARDIR/*Relax_run_$MonsterName.sh | grep -o "/[0-9]*Relax_run_$MonsterName.sh" | cut -c 2- | sort -rg | head -n 1)
qq=$PARDIR/$qq

if [ "$forhost" != "PC" ]; then  # not running MPMD on a workstation
 if  [ "$forhost" == "brutus" ] || [ "$forhost" == "euler" ]; then
   NodesInRun="1"
 else
   NodesInRun=$(cat $qq | grep -o "nodes *= *[0-9]*" | grep -o "[0-9]*")
 fi
else
  if [ -f $qq ]; then
   NodesInRun=$(cat $qq | grep -o "MCTDHX" | wc -l)
  else
   NodesInRun="0"
  fi
fi

  if [ "$NodesInRun" -lt "$MPMDnodes"  ]; then
   ## The number of nodes already specified in the runscript doesnt exceed the maximum, so add on another job
   if [ "$forhost" != "PC" ]; then  # not running MPMD on a workstation
     NewNodes=$(echo "$NodesInRun+$NUMNODES" | bc)
     RunCount=$(echo $qq | grep -o '[0-9]*Prop_run_'$MonsterName.sh | grep -o '^[0-9]*')
     ## Start each aprun command in its own subshell
     ## wait for 24 hrs after aprun, to guarantee that no subshell finishes before the job is done
     sed 's|nodes=[0-9]*|nodes='$NewNodes'|' < $qq > $qq-1
     sed 's|\(RunFlagSleeper .*\)\&|\1 '$COMPDIR' \&|' <$qq-1 >$qq
     sed 's|#PBS -N.*|#PBS -N MSTR_'$MonsterName'_'$RunCount'|' < $qq > $qq-1
     mv $qq-1 $qq
       echo "  (" >> $qq
       ## Sleeps for $jobtime - 5 mins, then removes runflag. in case aprun doesnt finish in $jobtime 
       echo "    cd $COMPDIR" >> $qq
       echo "    $runSTR &" >> $qq
       echo "    PID_relax=\$!"  >> $qq
       echo "    conv=0" >> $qq
       echo "    while [ \"\$conv\" -lt \"1\"  ]; do" >> $qq
       echo "      count=\$(get_convergence $COMPDIR $ConvDig)" >> $qq
       echo "      sleep 1800" >> $qq
       echo "    done" >> $qq
       echo "    kill \$PID_relax" >> $qq
       ## remove runflag after aprun command has finished
       echo "    rm $COMPDIR/RunFlag" >> $qq
       echo "  ) &" >> $qq 
     ## put a flag in the computation directory so it isnt computed multiple times
     touch $COMPDIR/RunFlag
   else # running MPMD on a workstation
     NewNodes=$(echo "$NodesInRun+1" | bc)
     RunCount=1
     ## Start each command in its own subshell
     ## Sleeps for $jobtime - 5 mins, then removes runflag. in case aprun doesnt finish in $jobtime 
     echo "    cd $COMPDIR" >> $qq
     echo "    $runSTR &" >> $qq
     echo "    PID_relax=\$!"  >> $qq
     echo "    conv=0" >> $qq
     echo "    while [ \"\$conv\" -lt \"1\"  ]; do" >> $qq
     echo "      count=\$(get_convergence $COMPDIR $ConvDig)" >> $qq
     echo "      sleep 10" >> $qq
     if [ "$forhost" == "nemo" ]; then
       echo "EXIST=\$( kill -0 \$PID_relax 2>&1 | grep \"process\" | wc -l )  " >> $qq
       echo "let \"conv=\$conv+\$EXIST\" "  >> $qq
     fi 
     echo "    done" >> $qq
     echo "    kill \$PID_relax" >> $qq
     if [ "$forhost" == "nemo" ]; then
      echo "    kill \$Sleeper_PID " >> $qq
     fi
     ## remove runflag after aprun command has finished
     echo " NewNodes: $NewNodes MPMDNodes: $MPMDnodes"
   fi 
   if [[ "$NewNodes" -ge "$MPMDnodes" || "$ForceRun" == "T"  ]]; then
    ## This last process made the nodecount exceed the maximum, or there is a ForceRun flag passed
    ## So now, exceute the runscript and start another
    if  [ "$forhost" != "PC" ]; then

      echo ")&" >> $qq
      echo "PID=\$!" >> $qq
      echo "wait \$PID" >> $qq
  
      ## Ensure the queue has room for the next job, if not, wait for it
      Njobs=$(get_NumberOfJobs $runhost)
      while [ "$Njobs" -ge "$maxjobs" ]; do
        echo "Njobs=$Njobs and maxjobs=$maxjobs"
        echo "Waiting 30 minutes for que to clear"
        sleep 1800
      done

      if  [ "$forhost" != "brutus" ] ||  [ "$forhost" != "euler" ]; then
          echo "qsub $qq"
          qsub $qq
      else 
          echo "bsub $qq"
          bsub $qq
      fi
    done

    if  [ "$forhost" != "brutus" ] || [ "$forhost" != "euler" ]; then
        if [ "$forhost" == "nemo" ]; then
           echo "msub $qq"
           msub $qq
        else
           echo "qsub $qq"
           qsub $qq
        fi
    else 
        echo "bsub $qq"
        bsub $qq
    fi

    RunCount=$(echo $qq | grep -o '[0-9]*Relax_run_'$MonsterName.sh | grep -o '^[0-9]*')
    let "RunCount++"
    cp $PARDIR/Relax_run_$MonsterName.sh_ $PARDIR/$RunCount"Relax_run_$MonsterName".sh
   fi
  fi
}


