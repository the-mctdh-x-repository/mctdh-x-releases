#!/bin/bash

function IterateParameters_prop()
{
if [ "$#" -ge 7 ]; then
 echo "Usage: IterateParameters \$PARDIR \$Par1 \$Par2 \$Par3 \$Par4 \$Par5 "
 exit 1
fi
PARDIR=$1
m=$2
l=$3
k=$4
j=$5
i=$6


. ./ParameterScan.inp
. $MCTDHXDIR/Scripts/assemble_runscript.sh
. $MCTDHXDIR/Scripts/ContinueProp.sh
. $MCTDHXDIR/Scripts/ExclusiveAdd.sh
. $MCTDHXDIR/Scripts/FindContinuation.sh
. $MCTDHXDIR/Scripts/Adjust_Inp_Prop.sh
. $MCTDHXDIR/Scripts/RemoveArrayElement.sh
. $MCTDHXDIR/Scripts/rbincp.sh
. $MCTDHXDIR/Scripts/readarray.sh
Prop_Array=($(readarray $RELAXDIR/$Prop_Array_File))
Completed_Array=($(readarray $RELAXDIR/$Completed_Array_File))
#echo "Prop_Array: ${Prop_Array[@]}"
  if [ "$Prop_NParameters" -eq 1  ]; then
    PROPSUBDIR=Prop_$Prop_Parameter1-$m/
    PROPMAIN=$RELAXDIR/$PROPSUBDIR
  elif [ "$Prop_NParameters" -eq 2  ]; then
    PROPSUBDIR=Prop_$Prop_Parameter1-$m"_"$Prop_Parameter2-$l/
    PROPMAIN=$RELAXDIR/$PROPSUBDIR
  elif [ "$Prop_NParameters" -eq 3  ]; then
    PROPSUBDIR=Prop_$Prop_Parameter1-$m"_"$Prop_Parameter2-$l"_"$Prop_Parameter3-$k/
    PROPMAIN=$RELAXDIR/$PROPSUBDIR
  elif [ "$Prop_NParameters" -eq 4  ]; then
    PROPSUBDIR=Prop_$Prop_Parameter1-$m"_"$Prop_Parameter2-$l"_"$Prop_Parameter3-$k"_"$Prop_Parameter4-$j/
    PROPMAIN=$RELAXDIR/$PROPSUBDIR
  elif [ "$Prop_NParameters" -eq 5  ]; then
    PROPSUBDIR=Prop_$Prop_Parameter1-$m"_"$Prop_Parameter2-$l"_"$Prop_Parameter3-$k"_"$Prop_Parameter4-$j"_"$Prop_Parameter5-$i/
    PROPMAIN=$RELAXDIR/$PROPSUBDIR
  else
    PROPSUBDIR=Propagation_noscan
    PROPMAIN=$RELAXDIR/$PROPSUBDIR
  fi
   mkdir $PROPMAIN 2> /dev/null
  #PROPMAIN_jobSubmitted=$( ls $PROPMAIN/RunFlag 2> /dev/null  )
  PROPMAIN_jobSubmitted=$( ls $PROPMAIN/RunFlag 2> /dev/null)$( ls $PROPMAIN/Continue*/RunFlag 2> /dev/null)
#  cd $PROPMAIN
 # NO_PR=$(find -maxdepth 0 -size +0 -print | find -name "NO_PR.out")
  cd $PARDIR
  if [[ ! -f $PROPMAIN/NO_PR.out && -z "$PROPMAIN_jobSubmitted" ]] ; then
   ### NO_PR.out does not exist yet, and no job is submitted in $PROPMAIN
   ### this must be the first computation for this paramater set
     echo "Beginning first propagation in: $PROPMAIN"
     echo "Copying Binaries and Libraries"
     if [[ "$Do_Relax" == "T" || "$Propagation_Start" == "BINR"   ]]; then
      ## Only copy bin's if propagation from relaxation, or starting from BINR
      rbincp $PROPMAIN/../ $PROPMAIN
     fi

     bin=$(echo "$binary" | tr ' ' '\n' | grep MCTDH | sed 's/\.\///')
     cp $PARDIR/$bin $PROPMAIN
     cp $PARDIR/libmctdhx.so $PROPMAIN

     ### Adjust MCTDHX.inp
     echo "Adjusting MCTDHX.inp"
     Adjust_Inp_Prop $PROPMAIN $PARDIR $m $l $k $j $i
     
     ###build and submit runscript
     echo "Building and submitting job"
     cd $PROPMAIN
     echo "NUMNODES = $NUMNODES"
     assemble_runscript "$PROPMAIN" "$runhost" "$binary" "$NUMNODES" "$MPMD" "$PARDIR" "$ForceRun" "prop"
     cd $PARDIR
     ###add $PROPMAIN to Prop_Array if its not already there
     echo "Updating \$Prop_Array:"
     Prop_Array=(`xadd $PROPSUBDIR ${Prop_Array[@]}`)               
     echo "${#Prop_Array[@]} elements in \$Prop_Array"
  elif [[ ! -f $PROPMAIN/NO_PR.out &&  -n "$PROPMAIN_jobSubmitted" ]] ; then
    ### NO_PR.out does not exist, but there is a submitted job, wait it out
    echo "job running, lets wait for it to finish "
    echo "Updating \$Prop_Array"
    Prop_Array=(`xadd $PROPSUBDIR ${Prop_Array[@]}`)               
    echo "${#Prop_Array[@]} elements in \$Prop_Array"
  elif [[  -f $PROPMAIN/NO_PR.out &&  -n "$PROPMAIN_jobSubmitted" ]] ; then
    ### NO_PR.out does exist and there is a submitted job, wait it out
    echo "job running, lets wait for it to finish "
    echo "Updating \$Prop_Array"
    Prop_Array=(`xadd $PROPSUBDIR ${Prop_Array[@]}`)               
    echo "${#Prop_Array[@]} elements in \$Prop_Array"
  elif [[ -f $PROPMAIN/NO_PR.out &&  -z "$PROPMAIN_jobSubmitted" && ! -e $PROPMAIN/Data/BoundaryViolation ]] ; then
   ### NO_PR.out does exist, and there is no submitted job for $PROPMAIN
   ### Pass $PROPMAIN to analysis script (NOT READY)
   ### must check the continuation directories!
    echo "Propagation has already started, lets continue it!"
    ### Find continuation with most progress 
    ContinueFrom=$(FindContinuation $PROPMAIN)
    echo "Furthest continuation directory: $ContinueFrom"
    ### check if there is any running jobs in $PROPMAIN
    Continue_jobSubmitted=$( ls $PROPMAIN/RunFlag 2> /dev/null)$( ls $PROPMAIN/Continue*/RunFlag 2> /dev/null)
#    echo "Continue_jobSubmitted=$Continue_jobSubmitted"
    if [ -z "$Continue_jobSubmitted" ]; then
       ### No job detected         
       ### Do continuation
       echo "continuation job is not running"
       ContinueProp $ContinueFrom $PARDIR $NUMNODES $ForceRun
       ContinueStatus=$?
       if [ "$ContinueStatus" == "1" ] ; then
         echo "Updating \$Prop_Array"
         ###add $PROPMAIN to Prop_Array if its not already there
         Prop_Array=(`xadd $PROPSUBDIR ${Prop_Array[@]}`)               
         echo "${#Prop_Array[@]} elements in \$Prop_Array"
       else 
         ### Continuation not started, because its done
         ### Remove from $Prop_Array
         echo "Propagaion is complete"
         echo "Updating \$Completed_Array"
#         Prop_Array=(`RmEle $PROPSUBDIR ${Prop_Array[@]}`)
         Completed_Array=(`xadd $PROPSUBDIR ${Completed_Array[@]}`)
         echo "${#Completed_Array[@]} elements in \$Completed_Array"
       fi
     else
       echo "RunFlag found in $Continue_jobSubmitted"
       echo "Updating \$Prop_Array"
       ###add $PROPMAIN to Prop_Array if its not already there
       Prop_Array=(`xadd $PROPSUBDIR ${Prop_Array[@]}`)               
       echo "${#Prop_Array[@]} elements in \$Prop_Array"
     fi
   elif [ -e $PROPMAIN/Data/BoundaryViolation ] ; then
      cat $PROPMAIN/Data/BoundaryViolation
      echo "Boundary Violation detected, we will not spend any more resources on this parameter set"
      echo "Updating \$Prop_Array"
      Prop_Array=(`RmEle $PROPSUBDIR ${Prop_Array[@]}`)               
      echo "${#Prop_Array[@]} elements in \$Prop_Array"
      echo "Updating \$Completed_Array"
#      Prop_Array=(`RmEle $PROPSUBDIR ${Prop_Array[@]}`)
      Completed_Array=(`xadd $PROPSUBDIR ${Completed_Array[@]}`)
      echo "${#Completed_Array[@]} elements in \$Completed_Array"
   fi
printf "%s\n" "${Prop_Array[@]}" > $RELAXDIR/$Prop_Array_File
printf "%s\n" "${Completed_Array[@]}" > $RELAXDIR/$Completed_Array_File
}
