#!/bin/bash

### I take in a $PROPMAIN directory and output an array of eligible directories that are ready for analysis
### Created by storm on 10/29/14
### Tested 10/29/14

function GetAnalArray()
{
if [ $# -lt 1 ] ; then
  echo "Usage: AnalArray=(\`GetAnalArray \$PROPMAIN1 \$PROPMAIN2 ... \`)"
  return 2
fi
PROPMAIN_List=("${@}")
unset AnalArray
declare -a AnalArray
for PROPMAIN in ${PROPMAIN_List[@]}; do
  unset DirList
  declare -a DirList
  DirList=$(ls -d $PROPMAIN/Continue* 2>/dev/null)
  DirList[${#DirList[@]}]=$PROPMAIN
  #echo ${DirList[@]}
  for dir in ${DirList[@]}; do
    ## A Directory is eligible for analysis if there are nonempty NO_PR.out, CIc_bin, and PSI_bin,
    ## And there is no RunFlag (propagation still running) or AnalFlag (analysis already running)
    if [ -s $dir/NO_PR.out ] && [ -s $dir/CIc_bin ] && [ -s $dir/PSI_bin ] && [ ! -e $dir/RunFlag ] && [ ! -e $dir/AnalFlag ]; then
  #    echo "Eligible Dir: $dir"
      AnalArray[${#AnalArray[@]}]=$dir
  #  else
  #    echo "Ineligible Dir: $dir"
    fi
  # echo $dir
  done
done
echo ${AnalArray[@]}

}
