#!/bin/bash
shopt -s expand_aliases
source ~/.bashrc

. $MCTDHXDIR/Scripts/dependencies
# this function makes a movie from the .png files listed in "TMP1"
# the input arguments are
# $1 resolution X
# $2 resolution Y
# $3 frames pre second
# $4 output filename prefix 
# $5 movie starting time
# $6 movie ending time

function make_movie {
if [ -f "TMP1" ]; then
 FILE_LIST="`cat TMP1`"
else 
 echo "Movie making script cannot find file list TMP1"
 exit 0
fi

rm qq.sort

for i in ${FILE_LIST} ; 
do
 if [ -f "$i.png" ];
   then
     echo "$i.png" >> qq.sort
   else
     echo "File $i.png not found -- the movie created will lack a frame!!!"
     echo "File $i.png not found -- the movie created will lack a frame!!!"
     echo "File $i.png not found -- the movie created will lack a frame!!!"
   fi
done

sort -g qq.sort > qq1.sort

FILE_LIST1="`cat qq1.sort`"
echo FILE_LIST1: ${FILE_LIST1}

#AUDIO=`audio_select.sh`
#$mencoder mf://@qq1.sort -mf w=$1:h=$2:fps=$3:type=png -ovc lavc -lavcopts vcodec=msmpeg4v2  -audiofile $AUDIO -oac copy -o $4_$5_$6.mpg
echo "Created Movie File is named: $4_$5_$6.mpg"
#echo "Your video was produced with the audio file $AUDIO from the free music archive, http://freemusicarchive.org/"

rm -f *.png
rm -f qq.sort
rm -f qq1.sort

}
