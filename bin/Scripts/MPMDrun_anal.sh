#!/bin/env - /bin/bash
source ~/.bashrc > /dev/null 2>&1
## I assemble and manage MPMD runscripts
##USAGE: MPMDrun $PARDIR $COMPDIR $runSTR $NUMNODES $ForceRun
## Created by Storm on Sept 29, 2014
## Tested by Storm on Sept 29, 2014


function MPMDrun_anal()
{
if [ $# -le 3 ] ; then
echo "USAGE: MPMDrun_anal \$PARDIR  \$COMPDIR \$runSTR \$NUMNODES \$ForceRun \$ForceTime"
fi
PARDIR=$1
COMPDIR=$2
runSTR=$3
NUMNODES=$4
ForceRun=$5
. $PARDIR/ParameterScan.inp
. $MCTDHXDIR/Scripts/get_NumberOfJobs.sh
if [ -n "$6" ] ; then
 AnalRunTime=$6
fi
echo $MONSTER_ID
echo $MONSTER_ID
echo $MONSTER_ID
echo $MONSTER_ID

echo "in MPMDrun_anal"
echo "PARDIR=$PARDIR"
echo "MPMD=$MPMD"
echo "COMPDIR=$COMPDIR"
echo "runSTR=$runSTR"
echo "NUMNODES=$NUMNODES"
echo "ForceRun=$ForceRun"
if [ "$MPMD" != "T" ]; then
  echo "setting MPMDnodes to 1"
  MPMDnodes=1
fi

## If no runscripts in the $PARDIR, copy one and strip of the line which runs the program
if [ -z "$(ls $PARDIR/[0-9]*anal_run_$MonsterName.$MONSTER_ID.sh 2> /dev/null)"  ] ; then

 if [ "$runhost" == "maia" ]; then 
   cp $MCTDHXDIR/../PBS_Scripts/run-example-maia.sh $PARDIR/anal_run_$MonsterName.$MONSTER_ID.tmp
   sed 's|mpirun.*||' < $PARDIR/anal_run_$MonsterName.$MONSTER_ID.tmp > $PARDIR/anal_run_$MonsterName.$MONSTER_ID.sh
   ## How is the walltime specified on maia?
   jobtime=86400
 elif [ "$runhost" == "hermit" ]; then
   cp $MCTDHXDIR/../PBS_Scripts/run-example-hermit.sh $PARDIR/anal_run_$MonsterName.$MONSTER_ID.tmp
   sed 's|aprun.*||' < $PARDIR/anal_run_$MonsterName.$MONSTER_ID.tmp > $PARDIR/anal_run_$MonsterName.$MONSTER_ID.sh
   sed 's|walltime=[0-9]*:[0-9]*:[0-9]*|walltime='$AnalRunTime'|' < $PARDIR/anal_run_$MonsterName.$MONSTER_ID.sh > $PARDIR/anal_run_$MonsterName.$MONSTER_ID.tmp
   cp $PARDIR/anal_run_$MonsterName.$MONSTER_ID.tmp $PARDIR/Tst
   mv $PARDIR/anal_run_$MonsterName.$MONSTER_ID.tmp  $PARDIR/anal_run_$MonsterName.$MONSTER_ID.sh
   jobtime=$(echo $AnalRunTime | sed 's|\([0-9]*\):\([0-9]*\):\([0-9]*\)| \1*3600 +\2*60 +\3|'  | bc)
 elif [ "$runhost" == "hornet" ]; then
   cp $MCTDHXDIR/../PBS_Scripts/run-example-hornet.sh $PARDIR/anal_run_$MonsterName.$MONSTER_ID.tmp
   sed 's|aprun.*||' < $PARDIR/anal_run_$MonsterName.$MONSTER_ID.tmp > $PARDIR/anal_run_$MonsterName.$MONSTER_ID.sh
   sed 's|walltime=[0-9]*:[0-9]*:[0-9]*|walltime='$AnalRunTime'|' < $PARDIR/anal_run_$MonsterName.$MONSTER_ID.sh > $PARDIR/anal_run_$MonsterName.$MONSTER_ID.tmp
   mv $PARDIR/anal_run_$MonsterName.$MONSTER_ID.tmp  $PARDIR/anal_run_$MonsterName.$MONSTER_ID.sh
   jobtime=$(echo $AnalRunTime | sed 's|\([0-9]*\):\([0-9]*\):\([0-9]*\)| \1*3600 +\2*60 +\3|'  | bc)
 elif [ "$runhost" == "bwgrid" ]; then
   cp $MCTDHXDIR/../PBS_Scripts/run-example-BWGRID.sh $PARDIR/anal_run_$MonsterName.$MONSTER_ID.tmp
   sed 's|mpirun.*||' < $PARDIR/anal_run_$MonsterName.$MONSTER_ID.tmp > $PARDIR/anal_run_$MonsterName.$MONSTER_ID.sh
   sed 's|walltime=[0-9]*:[0-9]*:[0-9]*|walltime='$AnalRunTime'|' < $PARDIR/anal_run_$MonsterName.$MONSTER_ID.sh > $PARDIR/anal_run_$MonsterName.$MONSTER_ID.tmp
   mv $PARDIR/anal_run_$MonsterName.$MONSTER_ID.tmp  $PARDIR/anal_run_$MonsterName.$MONSTER_ID.sh
   jobtime=$(echo $AnalRunTime | sed 's|\([0-9]*\):\([0-9]*\):\([0-9]*\)| \1*3600 +\2*60 +\3|'  | bc)
 elif [ "$forhost" == "nemo" ]; then
   cp $MCTDHXDIR/../PBS_Scripts/run-example-nemo.sh $PARDIR/anal_run_$MonsterName.$MONSTER_ID.tmp
   sed 's|mpiexec.*||' < $PARDIR/anal_run_$MonsterName.$MONSTER_ID.tmp > $PARDIR/anal_run_$MonsterName.$MONSTER_ID.sh
   jobtime=86400
 fi 
 sed 's|nodes=[0-9]*|nodes=0|' < $PARDIR/anal_run_$MonsterName.$MONSTER_ID.sh > $PARDIR/anal_run_$MonsterName.$MONSTER_ID.tmp
 sed 's|#PBS -N.*|#PBS -N MSTR_'$MonsterName.$MONSTER_ID'_1|' < $PARDIR/anal_run_$MonsterName.$MONSTER_ID.tmp > $PARDIR/anal_run_$MonsterName.$MONSTER_ID.sh_

 rm $PARDIR/anal_run_$MonsterName.$MONSTER_ID.sh 2>/dev/null
 rm $PARDIR/anal_run_$MonsterName.$MONSTER_ID.tmp 2>/dev/null
 chmod 755 $PARDIR/anal_run_$MonsterName.$MONSTER_ID.sh_
 echo "#taskcount=0" >> $PARDIR/anal_run_$MonsterName.$MONSTER_ID.sh_
 echo ". $MCTDHXDIR/Scripts/RunFlagSleeper.sh" >> $PARDIR/anal_run_$MonsterName.$MONSTER_ID.sh_
 ## Include check_convergence.sh for mixed relax/prop compatibility
 echo ". $MCTDHXDIR/Scripts/check_convergence.sh" >> $PARDIR/anal_run_$MonsterName.$MONSTER_ID.sh_ 
 echo "RunFlagSleeper $jobtime AnalFlag  &" >> $PARDIR/anal_run_$MonsterName.$MONSTER_ID.sh_

 if [ "$forhost" == "nemo" ]; then
    echo "Sleeper_PID=\$!" >> $PARDIR/anal_run_$MonsterName.$MONSTER_ID.sh_
 fi

 echo "(" >> $PARDIR/anal_run_$MonsterName.$MONSTER_ID.sh_
 cp $PARDIR/anal_run_$MonsterName.$MONSTER_ID.sh_ $PARDIR/1anal_run_$MonsterName.$MONSTER_ID.sh
fi
####### Add $runSTR to the most recent runscript
### find runscript$N.sh (run1.sh, run 2.sh, etc)  that has numnodes less than $MPMDnodes
#qq=$(ls $PARDIR/[0-9]*anal_run_$MonsterName.$MONSTER_ID.sh | sort -rg | head -n 1 )
qq=$(ls $PARDIR/[0-9]*anal_run_$MonsterName.$MONSTER_ID.sh | grep -o '/[0-9]*anal_run_'$MonsterName.$MONSTER_ID'.sh$' | cut -c 2- | sort -rg | head -n 1)
qq=$PARDIR/$qq
echo "working on runscript $(echo $qq | grep -o "[0-9]*anal_run_$MonsterName.$MONSTER_ID.sh")"
  NodesInRun=$(cat $qq | grep -o "nodes *= *[0-9]*" | grep -o "[0-9]*")
  taskcount=$(cat $qq | grep -o "taskcount=[0-9]*" | grep -o "[0-9]*")
  ppn=$(cat $qq | grep -o "ppn=[0-9]*" | grep -o "[0-9]*")
 
#  if [ "$(echo "$taskcount/$ppn" | bc)" -lt "$MPMDnodes"  ]; then

  if [ "$taskcount" -lt "$AnalFactor"  ]; then
   
   RunCount=$(echo $qq | grep -o '[0-9]*anal_run_'$MonsterName.$MONSTER_ID'.sh' | grep -o '^[0-9]*')
   ## The number of nodes already specified in the runscript doesnt exceed the maximum, so add on another job
   taskcount=$(echo "$taskcount+1" | bc) 
  
   RunCount=$(echo $qq | grep -o '[0-9]*anal_run_'$MonsterName.$MONSTER_ID'.sh' | grep -o '^[0-9]*')
   NewNodes=$(echo "1+($taskcount-1)/$ppn" | bc)

   echo "$NewNodes nodes and $taskcount tasks in current runscript"
   ## Start each aprun command in its own subshell
   sed 's|nodes=[0-9]*|nodes='$NewNodes'|' < $qq > $qq-1
   sed 's|\#taskcount=.*|\#taskcount='$taskcount'|' < $qq-1 > $qq 
   sed 's|\(RunFlagSleeper .*\)\&|\1 '$COMPDIR' \&|' <$qq >$qq-1
   sed 's|#PBS -N.*|#PBS -N MSTR_'$MonsterName.$MONSTER_ID'_'$RunCount'|' < $qq-1 > $qq
   rm $qq-1
     echo "  (" >> $qq
     ## Sleeps for $jobtime - 5 mins, then removes runflag. in case aprun doesnt finish in $jobtime 
     echo "    cd $COMPDIR" >> $qq
     echo "    $runSTR" >> $qq
     ## remove runflag after aprun command has finished
     echo "    rm $COMPDIR/AnalFlag" >> $qq
     echo "    touch $COMPDIR/CompleteFlag" >> $qq
     echo "  ) &" >> $qq 
   ## put a flag in the computation directory so it isnt computed multiple times
   touch $COMPDIR/AnalFlag
   
#   if [[ "$NewNodes" -ge "$MPMDnodes" || "$ForceRun" == "T"  ]]; then
   if [[ "$taskcount" -ge "$AnalFactor" || "$ForceRun" == "T"  ]]; then
    echo "Finishing $qq"
    ## This last process made the nodecount exceed the maximum, or there is a ForceRun flag passed
    ## So now, exceute the runscript and start another
    echo "  wait" >> $qq
    echo ") &" >> $qq
    echo "PID=\$!" >> $qq
    echo "wait \$PID" >> $qq

    if [ "$forhost" == "nemo" ]; then
      echo "    kill \$Sleeper_PID " >> $qq
    fi

    ## Ensure the queue has room for the next job, if not, wait for it
    Njobs=$(get_NumberOfJobs $runhost)
    while [ "$Njobs" -ge "$maxjobs" ]; do
      echo "Njobs=$Njobs and maxjobs=$maxjobs"
      if [ "$forhost" == "nemo" ]; then
         echo "Waiting 2 minutes for que to clear"
         sleep 120
         Njobs=$(get_NumberOfJobs $runhost)
      else
         echo "Waiting 30 minutes for que to clear"
         sleep 1800
         Njobs=$(get_NumberOfJobs $runhost)
      fi
    done

    if  [ "$forhost" != "brutus" ] ||  [ "$forhost" != "euler" ]; then
        if [ "$forhost" == "nemo" ]; then
           echo "msub $qq"
           msub $qq
        else
           echo "qsub $qq"
           qsub $qq
        fi
    else 
        echo "bsub $qq"
        bsub $qq
    fi

    RunCount=$(echo $qq | grep -o '[0-9]*anal_run_'$MonsterName.$MONSTER_ID'.sh' | grep -o '^[0-9]*')
    let "RunCount++"
    cp $PARDIR/anal_run_$MonsterName.$MONSTER_ID.sh_ $PARDIR/$RunCount"anal_run_$MonsterName.$MONSTER_ID.sh"
   fi
  fi

}


