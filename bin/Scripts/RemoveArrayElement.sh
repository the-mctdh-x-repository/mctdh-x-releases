#!/bin/bash


### If an array contains a particular element, delete it
### Usage: NewArr=(`RmEle $ele ${arr[@]} `)
### Created by storm on Sept 17, 2014
### cleaned and updated usage documentation on Oct 6, 2014


function RmEle()
{

if [ "$#" -eq 0 ]; then
 echo "Usage: NewArr=(\`RmEle \$ele \${arr[@]} \`)"
 return
fi
ele=$1
shift
arr=("${@}")
     declare -a NewArr 
     index=0
     for q in  ${arr[@]} ; do
      if [ "$q" != "$ele" ] ; then
       NewArr[index]=$q
       let "index += 1" 
      fi
     done

echo ${NewArr[@]}

}
 
