#!/bin/bash
shopt -s expand_aliases
source ~/.bashrc

function getpm3d {
echo "set terminal png size 1618,1000 linewidth 4"
echo "set term png enhanced fontscale 2.0"
echo "set border 4095 lw 4"
echo "set size square 1.0,1.0;"
echo "set output \"$1.png\""
echo "set notitle"  
echo "set pm3d interpolate 4,4 map;"
echo "set palette defined (0 \"white\", 0.5 \"blue\",1 \"turquoise\")"
}
