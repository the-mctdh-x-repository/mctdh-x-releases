#!/bin/bash

function restart_relaxation_upto5()
{

PARDIR=$PWD
# Computation directory
compdir=$1
# Input Template
template=$2
#final Time of relaxation
finaltime=$3
#binary to use
binary=$4
# Number of Parameters
NPar=$5
if [ "$5" -lt 1 ];
   then
   echo "for restart_relaxation_upto5.sh the number of parameters must be bigger or equal than 1!!!"
   echo "Syntax: newstart_relaxation_upto5 <directory> <input template> <finaltime> <binary> <number of parameters> <parameter1> <value1> ..."
   exit 1
fi
if [ $# -eq 7 ];
   then
# Parameter to modify
   Par1=$6
# Value of the parameter to set
   Val1=$7

elif [ $# -eq 9 ];
   then
# Parameter to modify
   Par1=$6
# Value of the parameter to set
   Val1=$7
   Par2=$8
   Val2=$9
elif [ $# -eq 11 ];
   then
 # Parameter to modify
   Par1=$6
# Value of the parameter to set
   Val1=$7
   Par2=$8
   Val2=$9
   Par3=${10}
   Val3=${11}
 elif [ $# -eq 13 ];
   then
 # Parameter to modify
   Par1=$6
# Value of the parameter to set
   Val1=$7
   Par2=$8
   Val2=$9
   Par3=${10}
   Val3=${11}
   Par4=${12}
   Val4=${13}
 elif [ $# -eq 15 ];
   then
 # Parameter to modify
   Par1=$6
# Value of the parameter to set
   Val1=$7
   Par2=$8
   Val2=$9
   Par3=${10}
   Val3=${11}
   Par4=${12}
   Val4=${13}
   Par5=${14}
   Val5=${15}
 else
   echo "Then number of arguments in restart_relaxation_upto5 does not comply!!!"
fi
if [ $# -eq 7 ];
   then
   sed 's/.*'"$Par1"'.*/'"$Par1"' = '"$Val1"'/g' $PARDIR/$template > $PARDIR/MCTDHX.tmp1
elif [ $# -eq 9 ];
   then
   sed 's/.*'"$Par1"'.*/'"$Par1"' = '"$Val1"'/g' $PARDIR/$template > $PARDIR/MCTDHX.t1
   sed 's/.*'"$Par2"'.*/'"$Par2"' = '"$Val2"'/g' $PARDIR/MCTDHX.t1 > $PARDIR/MCTDHX.tmp1

elif [ $# -eq 11 ];
   then
   sed 's/.*'"$Par1"'.*/'"$Par1"' = '"$Val1"'/g' $PARDIR/$template > $PARDIR/MCTDHX.t1
   sed 's/.*'"$Par2"'.*/'"$Par2"' = '"$Val2"'/g' $PARDIR/MCTDHX.t1 > $PARDIR/MCTDHX.t2
   sed 's/.*'"$Par3"'.*/'"$Par3"' = '"$Val3"'/g' $PARDIR/MCTDHX.t2 > $PARDIR/MCTDHX.tmp1
 elif [ $# -eq 13 ];
   then
   sed 's/.*'"$Par1"'.*/'"$Par1"' = '"$Val1"'/g' $PARDIR/$template > $PARDIR/MCTDHX.t1
   sed 's/.*'"$Par2"'.*/'"$Par2"' = '"$Val2"'/g' $PARDIR/MCTDHX.t1 > $PARDIR/MCTDHX.t2
   sed 's/.*'"$Par3"'.*/'"$Par3"' = '"$Val3"'/g' $PARDIR/MCTDHX.t2 > $PARDIR/MCTDHX.t3
   sed 's/.*'"$Par4"'.*/'"$Par4"' = '"$Val4"'/g' $PARDIR/MCTDHX.t3 > $PARDIR/MCTDHX.tmp1
 elif [ $# -eq 15 ];
   then
   sed 's/.*'"$Par1"'.*/'"$Par1"' = '"$Val1"'/g' $PARDIR/$template > $PARDIR/MCTDHX.t1
   sed 's/.*'"$Par2"'.*/'"$Par2"' = '"$Val2"'/g' $PARDIR/MCTDHX.t1 > $PARDIR/MCTDHX.t2
   sed 's/.*'"$Par3"'.*/'"$Par3"' = '"$Val3"'/g' $PARDIR/MCTDHX.t2 > $PARDIR/MCTDHX.t3
   sed 's/.*'"$Par4"'.*/'"$Par4"' = '"$Val4"'/g' $PARDIR/MCTDHX.t3 > $PARDIR/MCTDHX.t4
   sed 's/.*'"$Par5"'.*/'"$Par5"' = '"$Val5"'/g' $PARDIR/MCTDHX.t4 > $PARDIR/MCTDHX.tmp1
fi



sed 's/.*GUESS='\''HAND'\''.*/GUESS='\''BINR'\''/g' $PARDIR/MCTDHX.tmp1 > $PARDIR/MCTDHX.tmp2

STOP_TIME=`cat $compdir/NO_PR.out | tail -n 1 | awk '{print $1}' | cut -c 1-6`
## Rounds to the last Output timestep
Output_TimeStep=`cat $compdir/MCTDHX.inp | grep 'Output_TimeStep=' | sed 's/Output_TimeStep=//' | sed 's/!.*//' | sed 's/d0.*//'`
TimeSteps=`echo "scale=0;$STOP_TIME/$Output_TimeStep" | bc `
STOP_TIME=`echo "scale=8;$TimeSteps*$Output_TimeStep" | bc `

echo "Restarting at $STOP_TIME"
sed 's/.*Binary_Start_Time.*/Binary_Start_Time='$STOP_TIME'/g' $PARDIR/MCTDHX.tmp2 > $PARDIR/MCTDHX.tmp3      
sed 's/.*Time_Final.*/Time_Final='"$finaltime"'/g' $PARDIR/MCTDHX.tmp3 > MCTDHX.inp
echo "Found non-converged Relaxation in $compdir -- Restarting from $STOP_TIME"

cp $PARDIR/MCTDHX.inp $compdir
cp $PARDIR/$binary $PARDIR/libmctdhx.so $compdir

}
