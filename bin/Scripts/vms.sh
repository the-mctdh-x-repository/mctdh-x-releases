#!/bin/bash
shopt -s expand_aliases
source ~/.mctdhxrc

echo -e "\nVMS: Welcome to Visualisation Master Script - let's make some movies!\n"
echo -e "\nVMS: I take 7 arguments. The first two are mandatory and the rest optional.
             1:Type of movie
             2:Working Directory
             3:Plot range left
             4:Plot range right
             5:Number of Plot points
             6 and 7:Slices for 2D output\n"


#### Definitions ####
VSCRIPTSDIR=$MCTDHXDIR/../Visualization_Scripts

SCRIPTLIST=`ls $VSCRIPTSDIR`


possible="NAY"

for i in ${SCRIPTLIST}
do
  if [ "$i" == "$1" ]; 
  then
     possible="yo"
  fi
done

if [ "$1" == "all" ]; 
then
   possible="yo"
fi
 
if [ "$possible" == "NAY" ]; then
  echo "possible:$possible"
  echo "Your requested visualization script, $1, does not exits."
  echo "The available visualization options are
        $SCRIPTLIST"
  exit 0
fi

# getArgs gets the arguments for dT, time from, time till, number of orbitals and the number of particles
# Precondition: Input arguments are $1=<Type of movie> and $2=<Path to analysis directory>
# Postcondition: Required quantities are stored by their respective variables
getArgs ()
{
        echo -e "VMS: The arguments used to create your '"$1"' movie are:"

#Get and store required variables
timeIncrement=`cat $2/MCTDHX.inp | grep "Output_TimeStep" | grep -o '[0-9]*\.[0-9]*' | grep '[0-9]'` # Store the number of orbitals in timeIncriment
timeInitial=`cat $2/MCTDHX.inp | grep "Time_Begin" | grep -o '[0-9]*\.[0-9]*'` # Store the number of orbitals in timeInitial
timeFinal=`cat $2/NO_PR.out | tail -n 1 | awk '{print $1}' | cut -c 1-10`
timeFinal=`echo "scale=1;$timeFinal/1.0" | bc `
M_Orbs=`cat $2/MCTDHX.inp | grep 'Morb' | grep -o '[0-9]*'` # Store the number of orbitals in M_Orbs
N_Pars=`cat $2/MCTDHX.inp | grep 'Npar' | grep -o '[0-9]*'` # Store the number of orbitals in N_Pars
DIM=`cat $2/MCTDHX.inp | grep 'DIM_MCTDH' | grep -o '[0-9]*'` # Store the number of orbitals in N_Pars

# Output stored variables for error checking
echo -e "\tTime Increment= \t\t$timeIncrement"
echo -e "\tInitial Time= \t\t\t$timeInitial"
echo -e "\tFinal Time= \t\t\t$timeFinal"
echo -e "\tM Orbitals = \t\t\t$M_Orbs"
echo -e "\tN Particles = \t\t\t$N_Pars\n"
echo -e "    Movie Script: $1
             Directory: $2
             Plot From: $3
             Plot To: $4
             Number of Gridpoints: $5
             Slices 1 and Slice 2: $6 and $7 \n"
}

list_contains() {
  local tgt="$1"; shift
  while (( $# )); do
    if [[ $1 = "$tgt" ]] ; then
      return 0
    fi
    shift
  done
  return 1
}

do_Analysis()
{

if [[ -z "$3" ]] || [[ -z "$4" ]]; then

  RANGE_L="-5.0"
  RANGE_R="+5.0"

else

  RANGE_L="$3"
  RANGE_R="$4"

fi

if [[ -z "$5" ]]; then
  GRIDP=99999999
else
  GRIDP=$5
fi

if [[ -z "$6" ]] || [[ -z "$7" ]]; then
  SLICEX1=0.0
  SLICEX2=0.0
else
  SLICEX1="$6"
  SLICEX2="$7"
fi

NPOINTS=`echo "scale=0; ($timeFinal-$timeInitial)/$timeIncrement+1" | bc`

########### SET DEFAULTS OF ANALYSIS INPUT
PSI=".T."
CIC=".T."
XD=".T."
KD=".T."
NON=".F."
XST="0.0"
XSP="0.0"
FCORRX=".F."
FCORRK=".F."
dilation="1"
autodilation=".F."

if [ "$DIM" = "2" ]; then
PHAS=".T."
GRAD=".T."
dilation="4"

RCORR1X=".F."
RCORR1K=".F."
RCORR2X=".F."
RCORR2K=".F."

corr2d=`echo "$1" | grep "CORR"`

if [ "$1" = "all" ] || [ ! -z "$corr2d" ]; then
MOM2D=".T."
REAL2D=".T."
else
MOM2D=".F."
REAL2D=".F."
fi

LZ=".T."

elif [ "$DIM" = 1 ]; then

PHAS=".F."
GRAD=".F."
autodilation=".T."
kdip="0.00001"

corr1d=`echo "$1" | grep "CORR"`

if [ "$1" = "all" ] || [ ! -z "$corr1d" ]; then

RCORR1X=".T."
RCORR1K=".T."
RCORR2X=".T."
RCORR2K=".T."
else
RCORR1X=".F."
RCORR1K=".F."
RCORR2X=".F."
RCORR2K=".F."
fi

MOM2D=".F."
REAL2D=".F."
LZ=".F."

else

echo " FOR 3D Simulations, please do the analysis manually/implement routines!"
exit 0

fi

xini1=$RANGE_L
xfin1=$RANGE_R
xpts1=$GRIDP
kini1=$RANGE_L
kfin1=$RANGE_R
kpts1=$GRIDP
xini2=$RANGE_L
xfin2=$RANGE_R
xpts2=$GRIDP
kini2=$RANGE_L
kfin2=$RANGE_R
kpts2=$GRIDP

loss=".F."
border="0.0"

x1const=".T."
x1slice="$SLICEX1"
x2const=".T."
x2slice="$SLICEX2"
y1const=".F."
y1slice="0.0"
y2const=".F."
y2slice="0.0"
dil2d=2
veff2d=".F."                        




echo "&ZERO_body"                              > analysis.inp
echo " Orbitals_Output=     $PSI             " >> analysis.inp
echo " Coefficients_Output= $CIC             " >> analysis.inp
echo " Time_From=           $timeInitial     " >> analysis.inp
echo " Time_to=             $timeFinal       " >> analysis.inp
echo " Time_Points=         $NPOINTS   " >> analysis.inp
echo " Dilation =           $dilation  " >> analysis.inp
echo " AutoDilation =       $autodilation  " >> analysis.inp
echo " Kdip =       $kdip  " >> analysis.inp
echo " /                                     " >> analysis.inp
echo " &ONE_body                             " >> analysis.inp
echo " Density_x=           $XD              " >> analysis.inp
echo " Density_k=           $KD              " >> analysis.inp
echo " Pnot=           $NON                  " >> analysis.inp
echo " xstart=         $XST                  " >> analysis.inp
echo " xend=           $XSP                  " >> analysis.inp
echo " Phase=          $PHAS                 " >> analysis.inp
echo " Gradient=       $GRAD                 " >> analysis.inp
echo " /                                     " >> analysis.inp
echo " &TWO_body                             " >> analysis.inp
echo " Correlations_X=        $FCORRX        " >> analysis.inp
echo " Correlations_K=        $FCORRK        " >> analysis.inp
echo " corr1restr=    $RCORR1X               " >> analysis.inp
echo " xini1=         $xini1                 " >> analysis.inp
echo " xfin1=         $xfin1                 " >> analysis.inp
echo " xpts1=         $xpts1                 " >> analysis.inp
echo " corr1restrmom= $RCORR1K               " >> analysis.inp
echo " kxini1=        $kini1                 " >> analysis.inp
echo " kxfin1=        $kfin1                 " >> analysis.inp
echo " kpts1=         $kpts1                 " >> analysis.inp
echo " corr2restr=    $RCORR2X               " >> analysis.inp
echo " xini2=         $xini2                 " >> analysis.inp
echo " xfin2=         $xfin2                 " >> analysis.inp
echo " xpts2=         $xpts2                 " >> analysis.inp
echo " corr2restrmom= $RCORR2K               " >> analysis.inp
echo " kxini2=        $kini2                 " >> analysis.inp
echo " kxfin2=        $kfin2                 " >> analysis.inp
echo " kpts2=         $kpts2                 " >> analysis.inp
echo " lossops=       $loss                  " >> analysis.inp
echo " border=        $border                " >> analysis.inp
echo " /                                     " >> analysis.inp
echo " &TWO_D                                " >> analysis.inp
echo " MOMSPACE2D=    $MOM2D                 " >> analysis.inp
echo " REALSPACE2D=   $REAL2D                " >> analysis.inp
echo " x1const=       $x1const               " >> analysis.inp
echo " x1slice=       $x1slice               " >> analysis.inp
echo " y1const=       $x2const               " >> analysis.inp
echo " y1slice=       $x2slice               " >> analysis.inp
echo " x2const=       $y1const               " >> analysis.inp
echo " x2slice=       $y1slice               " >> analysis.inp
echo " y2const=       $y2const               " >> analysis.inp
echo " y2slice=       $y2slice               " >> analysis.inp
echo " PROJ_X=        $veff2d                " >> analysis.inp
echo "                                       " >> analysis.inp
echo " DIR='B'                               " >> analysis.inp
echo " L_Z=           $LZ                    " >> analysis.inp
echo " /                                     " >> analysis.inp
echo "     " >> analysis.inp

echo "assembled analysis.inp"
echo "assembled analysis.inp"
echo "assembled analysis.inp"
echo "assembled analysis.inp"
echo "assembled analysis.inp"
echo "assembled analysis.inp"
echo "assembled analysis.inp"
echo "assembled analysis.inp"
echo "assembled analysis.inp"
echo "running analysis program"
echo "running analysis program"
echo "running analysis program"
echo "running analysis program"
echo "running analysis program"
echo "running analysis program"
echo "running analysis program"
echo "running analysis program"

if [ `set | grep -i 'cray' | wc -l` -gt 10 ]; 
then 

aprun -n 1 -N 1 MCTDHX_analysis > /dev/null

else
MCTDHX_analysis > /dev/null
fi

}

makeVideo()
{

SCRIPTLIST=`ls $VSCRIPTSDIR`
echo -e " in makeVideo()"
echo -e "arguments $1 $2 $DIM"
  
echo -e " executing command  $1 $timeInitial $timeFinal $timeIncrement $M_Orbs $N_Pars" 
if [ "$DIM" -eq "3" ]; then
 SCRIPTS3D=`ls $VSCRIPTSDIR/3D*`
 ALLSCRIPTS=$SCRIPTS3D
elif [ "$DIM" -eq "2" ]; then
 SCRIPTS2D=`ls $VSCRIPTSDIR/2D*`
 ALLSCRIPTS=$SCRIPTS2D
elif [ "$DIM" -eq "1" ]; then
 SCRIPTS1D=`ls $VSCRIPTSDIR/1D*`
 ALLSCRIPTS=$SCRIPTS1D
fi

echo -e "ALLSCRIPTS: $ALLSCRIPTS"

if [ "$1" == "all" ]; then
  for k in ${ALLSCRIPTS} 
  do     
     echo -e " executing command  $k $timeInitial $timeFinal $timeIncrement $M_Orbs $N_Pars" 
     cd $2
     if [ `set | grep -i 'cray' | wc -l` -gt 10 ]; 
     then 
       aprun -N 1 -n 1 -d 1 $k $timeInitial $timeFinal $timeIncrement $M_Orbs $N_Pars 
     else
       $k $timeInitial $timeFinal $timeIncrement $M_Orbs $N_Pars
     fi
  done
else
  for k in ${SCRIPTLIST}
  do
    if [ "$k" == "$1" ]; then
       echo -e " executing command  $k $timeInitial $timeFinal $timeIncrement $M_Orbs $N_Pars" 
       cd $2
       if [ `set | grep -i 'cray' | wc -l` -gt 10 ]; 
       then 
         aprun -N 1 -n 1 -d 1  $VSCRIPTSDIR/$1 $timeInitial $timeFinal $timeIncrement $M_Orbs $N_Pars 
       else 
         $VSCRIPTSDIR/$1 $timeInitial $timeFinal $timeIncrement $M_Orbs $N_Pars 
       fi
    fi
  done
fi
}
getArgs $1 $2
do_Analysis $1 $2 $3 $4 $5 $6 $7
makeVideo $1 $2
