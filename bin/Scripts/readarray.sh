#!/bin/bash
# Creates an array with each element the lines of a file, $1
# Mimics readarray from bash 4
function readarray()
{
IFS=$'\n'
read -d '' -r -a Arr < $1
echo ${Arr[@]}
}
