#!/bin/bash

source ~/.mctdhxrc
shopt -s expand_aliases

. $MCTDHXDIR/Scripts/dependencies
. $MCTDHXDIR/Scripts/visualization_pm3d_settings.sh

if [ ! -f "$1/MCTDHX.inp" ];
  then
  echo "No input in the specified directory"
  exit 1
fi
if [ ! -f "$1/NO_PR.out" ];
  then
  echo "No NO_PR.out in the specified directory"
  exit 1
fi

timeIncrement=`cat $1/MCTDHX.inp | grep "Output_TimeStep" | grep -o '[0-9]*\.[0-9]*' | grep '[0-9]'` 
timeInitial=`cat $1/MCTDHX.inp | grep "Time_Begin" | grep -o '[0-9]*\.[0-9]*'` 
timeFinal=`cat $1/NO_PR.out | tail -n 1 | awk '{print +$1}'`
M_Orbs=`cat $1/MCTDHX.inp | grep 'Morb' | grep -o '[0-9]*'` # Store the number of orbitals in M_Orbs
dimension=`cat $1/MCTDHX.inp | grep 'DIM_MCTDH' | grep -o '[0-9]*'` # Store the number of orbitals in M_Orbs


NX=`cat MCTDHX.inp | grep 'NDVR_X' | sed 's/NDVR_X=//' | grep -P -o '[0-9]+'`
NY=`cat MCTDHX.inp | grep 'NDVR_Y' | sed 's/NDVR_Y=//' | grep -P -o '[0-9]+'`
NZ=`cat MCTDHX.inp | grep 'NDVR_Z' | sed 's/NDVR_Z=//' | grep -P -o '[0-9]+'`

Nsteps=`echo "scale=0;($timeFinal-$timeInitial)/($timeIncrement)+1" | bc`

LastTime=`echo "scale=4;($timeInitial+(($Nsteps-1)*$timeIncrement))" | bc`

#echo "scale=4;($timeInitial+(($Nsteps-1)*$timeIncrement))"
#echo $Nsteps
#echo $timeFinal
#echo $timeInitial
#echo $timeIncrement
#echo $LastTime
#exit 0

getk="no"
getx="no"

if [ "$dimension" -eq 1 ]; then
  densityfile="*$LastTime*correlations.dat"
elif [ "$dimension" -eq 2 ]; then
  densityfile="*$LastTime*-P0.00c1-P0.00c2-correlations.dat"
fi

numdensityfiles=`ls $densityfile | wc -l`

if [ "$numdensityfiles" -lt 2 ]; # analysis needs to be done
  then 
echo "&ZERO_body" > analysis.inp
echo " Time_From=$LastTime              " >> analysis.inp
echo " Time_to=$LastTime               " >> analysis.inp
if [ "$dimension" -eq 1 ];
  then
  echo " AutoDilation=.T.            " >> analysis.inp
  echo " Kdip=0.00001 " >> analysis.inp
elif [ "$dimension" -gt 1 ];
  then
  echo " Dilation=4" >> analysis.inp
fi
echo " Time_Points=1            " >> analysis.inp
echo " /                       " >> analysis.inp
echo " &ONE_body               " >> analysis.inp
echo " /                       " >> analysis.inp
echo " &TWO_body               " >> analysis.inp

if [ "$dimension" -lt "2" ]; then
  echo " Correlations_k=.T.            " >> analysis.inp
  echo " Correlations_x=.T.            " >> analysis.inp
fi

echo " /                       " >> analysis.inp
echo " &MANY_body               " >> analysis.inp
echo " /                       " >> analysis.inp
echo " &TWO_D                  " >> analysis.inp
if [ "$dimension" -eq "2" ]; then
 echo "MOMSPACE2D=.T.  " >> analysis.inp
 echo "REALSPACE2D=.T.  " >> analysis.inp
 echo " x1const=.T.    " >> analysis.inp    
 echo "x1slice=0.d0    " >> analysis.inp   
 echo "y1const=.T.     " >> analysis.inp  
 echo "y1slice=0.d0    " >> analysis.inp  
 echo "x2const=.F.     " >> analysis.inp 
 echo "x2slice=0.d0    " >> analysis.inp
 echo "y2const=.F.     " >> analysis.inp
 echo "y2slice=0.d0    " >> analysis.inp
fi

echo " /                      " >> analysis.inp
MCTDHX_analysis
fi

correlationfiles=`ls $densityfile`

for i in ${correlationfiles};
do

 if [ "$dimension" -eq 1 ]; then
   unset kdens
   kdens=`echo $i | grep k-correlations.dat`
 elif [ "$dimension" -eq 2 ]; then
   unset kdens
   kdens=`echo $i | grep k-P0.00c1-P0.00c2-correlations.dat`
 fi
 

 if [ ! -z "$kdens" ]; then
    out="momentum_correlation.png"
   if [ "$dimension" -eq 1 ];
     then
     xlabel="Momentum K"
     ylabel="Momentum K'"
     title="K-space correlation g^{(1)}(K,K') MCTDHX(M=$M_Orbs)"
   elif [ "$dimension" -eq 2 ];
     then
     xlabel="Momentum K"
     ylabel="Momentum K'"
     title="Cut of K-space correlations g^{(1)}(k=(K,0),k'=(K',0)) MCTDHX(M=$M_Orbs)"
   elif [ "$dimension" -eq 3 ];
     then
     echo "3D momentum quantities not implemented yet..."
   fi
 else
    out="correlation.png"
    title="Density MCTDHX(M=$M_Orbs)"
    if [ "$dimension" -eq 1 ];
     then
     xlabel="Coordinate X"
     ylabel="Coordinate X'"
     title="Correlations g^{(1)}(X,X') MCTDHX(M=$M_Orbs)"
   elif [ "$dimension" -eq 2 ];
     then
     xlabel="Coordinate X"
     ylabel="Coordinate X'"
     title="Cut of correlations g^{(1)}(x=(X,0),x'=(X',0)) MCTDHX(M=$M_Orbs)"
   fi
 fi

 if [ "$dimension" -eq 1 ];
   then
 $gnuplot << EOF
 set terminal png enhanced truecolor font "Helvetica,30" size 1618,1000
 $(getpm3d $i)
 set border 4095 lw 4.0
 set output "$out"
 set xlabel "$xlabel"
 set ylabel "$ylabel"
 set title "$title"
 splot "$i" u 1:4:((\$8**2+\$9**2)/(\$7*\$10)) t ""
EOF
 elif [ "$dimension" -eq 2 ];
    then
$gnuplot << EOF
set terminal png enhanced truecolor font "Helvetica,30" size 1618,1000
$(getpm3d $i)
set pm3d interpolate 4,4 map
set border 4095 lw 4.0
set output "$out"
set xlabel "$xlabel"
set ylabel "$ylabel"
set title "$title"
splot "$i" u 1:2:(sqrt(\$5**2)>=0.001 && sqrt(\$6**2)>=0.001 ? ((\$7**2+\$8**2)/(\$5*\$6)):1/0) t ""
EOF
 fi
done
