#!/bin/sh

echo "Compiler variable: $1"

if [ -z "$1" ]; then
   echo "You need to select a compiler from Intel/GNU"
   echo "You need to select a compiler from Intel/GNU"
   echo "You need to select a compiler from Intel/GNU"
   echo "make -f Makefile.Platform cmp=compiler"
   echo "make -f Makefile.Platform cmp=compiler"
   echo "make -f Makefile.Platform cmp=compiler"
   exit 1
elif [ "$1" = "GNU" ]; then 
   echo "You selected GNU compilers"
   echo "You selected GNU compilers"
   echo "You selected GNU compilers"
   exit 0
elif [ "$1" = "intel" ]; then
   echo "You selected intel compilers"
   echo "You selected intel compilers"
   echo "You selected intel compilers"
   exit 0
else
   echo "Selected Compiler unknown!!!"
   echo "You need to select a compiler from Intel/GNU"
   echo "You need to select a compiler from Intel/GNU"
   echo "You need to select a compiler from Intel/GNU"
   echo "make -f Makefile.Platform cmp=compiler"
   echo "make -f Makefile.Platform cmp=compiler"
   echo "make -f Makefile.Platform cmp=compiler"
   exit 1
fi

