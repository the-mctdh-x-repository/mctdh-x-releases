#!/bin/bash
source ~/.bashrc > /dev/null 2>&1
## I assemble and manage MPMD runscripts
## USAGE: MPMDrun $PARDIR $COMPDIR $runSTR $NUMNODES $ForceRun
## Created by Storm on Sept 29, 2014
## Tested by Storm on Sept 29, 2014


function MPMDrun()
{
PARDIR=$1
COMPDIR=$2
runSTR=$3
NUMNODES=$4
ForceRun=$5

if [ $# -le 3 ] ; then
echo "USAGE: MPMDrun \$PARDIR \$COMPDIR \$runSTR \$NUMNODES \$ForceRun"
fi

. $PARDIR/ParameterScan.inp
if [ "$MPMD" != "T" ]; then
  MPMDnodes=1
fi

## If no runscripts in the $PARDIR, copy one and strip of the line which runs the program
if [ -z "$(ls $PARDIR/run*.sh)"  ] ; then

 if [ "$forhost" == "maia" ]; then 
   cp $MCTDHXDIR/../PBS_Scripts/run-example-maia.sh $PARDIR/run.tmp
   sed 's|mpirun.*||' < $PARDIR/run.tmp > $PARDIR/run.sh
 elif [ "$forhost" == "hermit" ]; then
   cp $MCTDHXDIR/../PBS_Scripts/run-example-hermit.sh $PARDIR/run.tmp
   sed 's|aprun.*||' < $PARDIR/run.tmp > $PARDIR/run.sh
 elif [ "$forhost" == "hornet" ]; then
   cp $MCTDHXDIR/../PBS_Scripts/run-example-hornet.sh $PARDIR/run.tmp
   sed 's|aprun.*||' < $PARDIR/run.tmp > $PARDIR/run.sh
 
 elif [ "$forhost" == "bwgrid" ]; then
   cp $MCTDHXDIR/../PBS_Scripts/run-example-BWGRID.sh $PARDIR/run.tmp
   sed 's|mpirun.*||' < $PARDIR/run.tmp > $PARDIR/run.sh
 fi 
 sed 's|nodes=[0-9]*|nodes=0|' < $PARDIR/run.sh > $PARDIR/run.tmp
 sed 's|#PBS -N.*|#PBS -N MONSTER_'$MonsterName'|' < $PARDIR/run.tmp > $PARDIR/run.sh_

 rm $PARDIR/run.sh
 rm $PARDIR/run.tmp
 chmod 755 $PARDIR/run.sh_
 cp $PARDIR/run.sh_ $PARDIR/run1.sh
fi
### Add $runSTR to the most recent runscript
### find runscript$N.sh (run1.sh, run 2.sh, etc)  that has numnodes less than $MPMDnodes
for qq in $(ls $PARDIR/run[0-9]*.sh | sort -g ); do
  NodesInRun=$(cat $qq | grep -o "nodes *= *[0-9]*" | grep -o "[0-9]*")
  if [ "$NodesInRun" -lt "$MPMDnodes"  ]; then
   ## The number of nodes already specified in the runscript doesnt exceed the maximum, so add on another job
   NewNodes=$(echo "$NodesInRun+$NUMNODES" | bc)
   ## Start each aprun command in its own subshell
   ## wait for 24 hrs after aprun, to guarantee that no subshell finishes before the job is done
   sed 's|nodes=[0-9]*|nodes='$NewNodes'|' < $qq > $qq-1
     echo "(" >> $qq-1
     ## Sleeps for 24 hr - 5 mins, then removes runflag. in case aprun doesnt finish in 24 hrs 
     echo "$MCTDHXDIR/RunFlagSleeper.sh $COMPDIR &" >> $qq-1
     echo "cd $COMPDIR" >> $qq-1
     echo "$runSTR" >> $qq-1
     ## remove runflag after aprun command has finished
     echo "rm $COMPDIR/RunFlag" >> $qq-1
     echo "sleep 86400" >> $qq-1
     echo ") &" >> $qq-1 
   mv $qq-1 $qq
   ## put a flag in the computation directory so it isnt computed multiple times
   touch $COMPDIR/RunFlag
   
   if [[ "$NewNodes" -ge "$MPMDnodes" || "$ForceRun" == "T"  ]]; then
    ## This last process made the nodecount exceed the maximum, or there is a ForceRun flag passed
    ## So now, exceute the runscript and start another
    echo "wait" >> $qq
    echo "qsub $qq"
#    qsub $qq
    RunCount=$(echo $qq | grep -o 'run[0-9]*.sh' | grep -o '[0-9]*')
    let "RunCount++"
    cp $PARDIR/run.sh_ $PARDIR/run$RunCount.sh
   fi
  fi
done
}


