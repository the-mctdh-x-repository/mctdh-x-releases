#!/bin/bash
shopt -s expand_aliases
source ~/.bashrc > /dev/null 2>&1

#### function get_jobstatus: Returns a Jobstring that indicates the status of a computation
####      This can be either "Running" "Restart" "Converged" or "Newstart".
#### arguments: 1) host to run on
####            2) computation directory
####            3) jobid (optional)
####            4) array of running jobs (optional)
##  UPDATE: use "2>&1" to grep the stderror when checking for unknown job id's

. $MCTDHXDIR/Scripts/check_convergence.sh

function get_jobstatus()
{

forhost=$1
cdir=$2
jobid=$3
name=$4[@]
rlxarray=("${!name}")

if [ "$jobid" == "A" ];
   then
   unset jobid
fi

unset JobStr
  

if [ "$forhost" != "PC" ] ; ### on a cluster we have to find out if the job is running currently
then
    unset notrunning
    if [ -f TMP ]; 
       then 
       rm TMP
    fi

    if [ ! -z $jobid ];
       then
#        echo "non-empty jobid: $jobid"  
       if [ "$forhost" == "maia" ];
          then
            qstat -j $jobid &> TMP
            notrunning=`cat TMP | grep "Following jobs do not exist"` # job is not running --> Restart?
       elif [ "$forhost" == "bwgrid" ] || [ "$forhost" == "hermit" ];
          then
            notrunning=$(qstat -f $jobid 2>&1 | grep "Unknown Job Id")
       elif [ "$forhost" == "hornet" ]; then
            notrunning=$(qstat -f $jobid 2>&1 | grep "Unknown Job Id") 
       elif [ "$forhost" == "brutus" ] || [ "$forhost" == "euler" ];
          then
           notrunning=$(bjobs $jobid 2>&1 | grep "is not found") 
       fi
       
    else # jobid empty --> check if queue contains job with working directory
#        echo "empty jobid"  
        if [ "$forhost" == "brutus" ] || [ "$forhost" == "euler" ];
        then
          joblist=$(bjobs -a | grep "RUN" | awk '{print $1}')
          joblist+=" "$(bjobs -a | grep "PEND" | awk '{print $1}')
        else
           joblist=`qstat | sed -n '/[0-9][0-9].*/{p;}' | awk '{print $1}'`   # determine list of running jobs        
        fi

        for n in ${joblist} ;
              do
                if [ "$forhost" == "maia" ];
                   then
                   workdir=`qstat -j $n | grep sge_o_workdir | sed 's/sge_o_workdir://' | sed -e 's/^[ \t]*//'`
                elif [ "$forhost" == "bwgrid" ];
                   then
                   workdir=`qstat -f $n | grep init_work_dir | sed 's|init_work_dir\ =\ ||' |  sed -e 's/^[ \t]*//'`
                elif [ "$forhost" == "brutus" ] || [ "$forhost" == "euler" ];
                   then
                   workdir=`bjobs -l $n | tr -d '\n' | sed -n -e 's/^.*Execution\ CWD//p' | grep -o -P '(?<=<).*(?=>)' | tr -d ' '`
#                echo "checking job $k in $workdir"
                elif [ "$forhost" == "hermit" ];
                   then
                   workdir=`qstat -f $p | grep -A1 Output_Path | sed ':a;N;$!ba;s/\n//g' | sed 's|Output_Path\ =\ ||' | sed 's/^[ \t]*//' | sed 's/^[^\/]*\//\//' | awk '{$1=$1}{print}' | sed 's/ //g' | sed 's/\.o.*$//g'`
                fi

                if [ "$workdir" == "$cdir" ];
                   then
                   if [ "$forhost" == "maia" ];
                     then
                     qstat -j $n &> runTMP
                     unset notrunning
                     notrunning=`cat runTMP | grep "Following jobs do not exist"`
                     rm runTMP
                   elif [ "$forhost" == "bwgrid" ] || [ "$forhost" == "hermit" ];
                     then
                     notrunning=$(qstat -f $n 2>&1 | grep "Unknown Job Id")
                   fi

                  jobid=$n
                fi
        done
        if [ -z "$joblist" ]; # no jobs running
           then
              notrunning="AAA"
        fi
    fi

#    echo "after jobid-exception $JobStr $jobid $notrunning"

    if [ ! -z "$notrunning" ];
       then
         JobStr="NotRunning"
    else  # notrunning is empty --> job is running
      if [ -z $jobid ] && [ -s "$cdir/PSI_bin" ] && [ -s "$cdir/CIc_bin" ] && [ -s "$cdir/NO_PR.out" ];
            then
            Count=$(get_convergence $cdir $ConvDig)
            if [ "$Count" -lt "10" ]; # check convergence
               then
                  JobStr="Restart"
               else
                  JobStr="Converged"
            fi
      elif [ ! -z $jobid ];
         then
         Count=$(get_convergence $cdir $ConvDig)
         if [ "$Count" -lt "10" ]; # check convergence
            then
               JobStr="Running"
         elif [ "$Count" -eq "10" ];
            then
               JobStr="Converged"
         fi   
      elif [ -z $jobid ];
         then
         JobStr="NotRunning"
      fi
    fi                 

#    echo "after jobid-exception $JobStr $jobid $notrunning"

    unset IsIn_STR
    if [ "$JobStr" == "NotRunning" ]; # If job is not running, check if it's in the array of relaxations
       then
         IsIn_STR=`printf "%s\n" "${rlxarray[@]}" | grep "^${cdir}*$"`
    fi                           


    if [ ! -z $IsIn_STR ] && [ "$JobStr" == "NotRunning" ]; # Job is in array and is not running --> Restart it
       then
         if [ -s "$cdir/PSI_bin" ] && [ -s "$cdir/CIc_bin" ] && [ -s "$cdir/NO_PR.out" ];
            then
            Count=$(get_convergence $cdir $ConvDig)
            if [ "$Count" -lt "10" ]; # check convergence
               then
                 JobStr="Restart"
               else
                 JobStr="Converged"
            fi
         else
            JobStr="Newstart"
         fi
    elif [ -z $IsIn_STR ] && [ "$JobStr" == "NotRunning" ]; # If it's not in the array and it's not running, then the job
         then                                                # is either new to start, to be restarted or converged

         if [ -s "$cdir/PSI_bin" ] && [ -s "$cdir/CIc_bin" ] && [ -s "$cdir/NO_PR.out" ];
            then
            Count=$(get_convergence $cdir $ConvDig)
            if [ "$Count" -lt "10" ]; # check convergence
               then
                  JobStr="Restart"
               else
                  JobStr="Converged"
            fi
         else
           JobStr="Newstart"
         fi
    fi
#    echo "Set Job-String: $Job_Str"
elif [ "$forhost" == "PC" ];
    then

    Count="0"

    if [ -s "$cdir/PSI_bin" ] && [ -s "$cdir/CIc_bin" ] && [ -s "$cdir/NO_PR.out" ] ;
       then 
       Count=$(get_convergence $cdir $ConvDig)
    else
       JobStr="NewStart"
    fi

    if [ "$Count" -lt "10" ] && [ "$JobStr" != "NewStart" ]; # check convergence
    then
       JobStr="Restart"
    else
       JobStr="Converged"
    fi
fi

echo $JobStr
}
