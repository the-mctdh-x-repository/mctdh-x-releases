#!/bin/bash

### I adjust the analysis.inp file for a particular directory
### Created 10/29/14 by storm
### tested 10/29/14

function AdjustAnalInp()
{
 if [ $# != 3 ]; then
   echo "Usage: AdjustAnalInp \$PARDIR \$AnalDir"
   return 2
 fi 
echo "In AdjustAnalInp"

 PARDIR=$1
 AnalDir=$2 
 Mode=$(echo $3 | tr '[:upper:]' '[:lower:]')
 echo "Mode=$Mode"
 echo "PARDIR=$PARDIR"
 echo "AnalDir=$AnalDir"
 . $PARDIR/ParameterScan.inp
 . $MCTDHXDIR/Scripts/getTimes.sh
 . $MCTDHXDIR/Scripts/IsRelaxation.sh

 Output_TimeStep=$(cat $AnalDir/MCTDHX.inp | grep -o "Output_TimeStep *= *[0-9]*\.[0-9]*" | sed 's|Output\_TimeStep *=||' | sed 's|d0||')
 InitialPropTime=$(getInitialPropTime $AnalDir)
 FinalPropTime=$(getFinalPropTime $AnalDir)
 ## Adjust Analysis times to compensate for $AnalFactor and $Output_TimeStep    
 InitialAnalTime=$( echo "$InitialPropTime+$Output_TimeStep*$AnalFactor-($InitialPropTime%($Output_TimeStep*$AnalFactor))" | bc ) 
 FinalAnalTime=$( echo "$FinalPropTime-($FinalPropTime%($Output_TimeStep*$AnalFactor))" | bc ) 
 Time_Points=$( echo "($FinalAnalTime-$InitialAnalTime)/($AnalFactor*$Output_TimeStep)" | bc)
 if [ "${Mode:0:4}" == "prop" ]; then
  echo "PropMode!"
  sed "s|Time_From *=.*|Time_From="$InitialAnalTime"d0|" < $AnalDir/analysis.inp > $AnalDir/analysis.inp.1 
  sed "s|Time_To *=.*|Time_To="$FinalAnalTime"d0|" < $AnalDir/analysis.inp.1 > $AnalDir/analysis.inp
  sed "s|Time_Points *=.*|Time_Points="$Time_Points"|" < $AnalDir/analysis.inp > $AnalDir/analysis.inp.1
  cp $AnalDir/analysis.inp.1 $AnalDir/analysis.inp
  rm $AnalDir/analysis.inp.1
 elif [ "${Mode:0:4}" == "rela" ]; then
 echo "relax mode!"
  sed "s|Time_From *=.*|Time_From="$FinalAnalTime"d0|" < $AnalDir/analysis.inp > $AnalDir/analysis.inp.1 
  sed "s|Time_To *=.*|Time_To="$FinalAnalTime"d0|" < $AnalDir/analysis.inp.1 > $AnalDir/analysis.inp
  sed "s|Time_Points *=.*|Time_Points=1|" < $AnalDir/analysis.inp > $AnalDir/analysis.inp.1
  cp $AnalDir/analysis.inp.1 $AnalDir/analysis.inp
  rm $AnalDir/analysis.inp.1
 elif [ "${Mode:0:3}" == "any" ]; then
  sed "s|Time_From *=.*|Time_From="$FinalAnalTime"d0|" < $AnalDir/analysis.inp > $AnalDir/analysis.inp.1 
  sed "s|Time_To *=.*|Time_To="$FinalAnalTime"d0|" < $AnalDir/analysis.inp.1 > $AnalDir/analysis.inp
  REL=$(IsRelaxation $AnalDir)
  if [ $REL -eq "0" ] ; then
     Time_Points=1
  fi
  sed "s|Time_Points *=.*|Time_Points=$Time_Points|" < $AnalDir/analysis.inp > $AnalDir/analysis.inp.1
  cp $AnalDir/analysis.inp.1 $AnalDir/analysis.inp
  rm $AnalDir/analysis.inp.1
 fi
} 
