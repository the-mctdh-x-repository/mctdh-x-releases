#!/bin/bash

### function to find out if a relaxation is converged
### checks the last 10 energy values of a computation.
### the computation directory is the input and the 
### number of identical energies 
### Oct 8,2014: adjusted output to be 1 for converged, 0 otherwise
function get_convergence()
{
export LANG=en_US.UTF-8
if [ "$#" -lt 2 ]; then
 echo "usage: get_convergence \$compdir \$ConvDig "
 return 0
fi
compdir=$1

if [ ! -s "$compdir/PSI_bin" ] || [ ! -s "$compdir/CIc_bin" ] || [ ! -s "$compdir/NO_PR.out" ];
   then 
     count=0
     echo "$count"
     return 0
fi


N=$(cat $compdir/MCTDHX.inp | grep 'Npar' | sed 's/ //g' | sed 's/Npar=//' | sed 's/!.*//' | sed 's/d0.*//')
M=$(cat $compdir/MCTDHX.inp | grep 'Morb' | sed 's/ //g' | sed 's/Morb=//' | sed 's/!.*//' | sed 's/d0.*//')

Energy_Column=$(echo "$M+2" | bc )
rho1_Column=$(echo "$M+1" | bc )

Energy_List=$(cat $compdir/NO_PR.out | tail -n 10 | awk -v Col=$Energy_Column '{print $Col}' | cut -c 1-14)
rho1_List=$(cat $compdir/NO_PR.out | tail -n 10 | awk -v Col=$rho1_Column '{print $Col}' | cut -c 1-14)
Count=1
Temp=0
let Digits=$2+1

for e in $Energy_List
do
  ebyN=$(echo "scale=$Digits;$e/$N" | bc)
  e=$(round $ebyN $2) 
#  echo "$e"
  bool=$(echo "$Temp == $e" | bc)
  Count=$(echo "$Count + $bool" | bc)
  Temp=$e
done

rho1_count=1
Temp=0
for rho in $rho1_List
do
  rho_trunc=$(echo "scale=$Digits;$rho" | bc)
  e=$(round $rho_trunc $2) 
#  echo "$e"
  bool=$(echo "$Temp == $e" | bc)
  rho1_count=$(echo "$rho1_count + $bool" | bc)
  Temp=$e
done

echo $(echo "($Count + $rho1_count)/20" | bc)

return 0

}

round()
{
export LANG=en_US.UTF-8
echo $(printf %.$2f $(echo "scale=$2;(((10^$2)*$1)+0.5)/(10^$2)" | bc))
};
