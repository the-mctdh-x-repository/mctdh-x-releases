#!/bin/bash

source ~/.mctdhxrc
shopt -s expand_aliases

. $MCTDHXDIR/Scripts/dependencies
. $MCTDHXDIR/Scripts/visualization_pm3d_settings.sh

if [ ! -f "$1/MCTDHX.inp" ];
  then
  echo "No input in the specified directory"
  exit 1
fi
if [ ! -f "$1/NO_PR.out" ];
  then
  echo "No NO_PR.out in the specified directory"
  exit 1
fi

timeIncrement=`cat $1/MCTDHX.inp | grep "Output_TimeStep" | grep -o '[0-9]*\.[0-9]*' | grep '[0-9]'` 
timeInitial=`cat $1/MCTDHX.inp | grep "Time_Begin" | grep -o '[0-9]*\.[0-9]*'` 
timeFinal=`cat $1/NO_PR.out | tail -n 1 | awk '{print +$1}'`
M_Orbs=`cat $1/MCTDHX.inp | grep 'Morb' | grep -o '[0-9]*'` # Store the number of orbitals in M_Orbs
dimension=`cat $1/MCTDHX.inp | grep 'DIM_MCTDH' | grep -o '[0-9]*'` # Store the number of orbitals in M_Orbs

Nsteps=`echo "scale=0;($timeFinal-$timeInitial)/($timeIncrement)+1" | bc`

LastTime=`echo "scale=4;($timeInitial+(($Nsteps-1)*$timeIncrement))" | bc`

#echo "scale=4;($timeInitial+(($Nsteps-1)*$timeIncrement))"
#echo $Nsteps
#echo $timeFinal
#echo $timeInitial
#echo $timeIncrement
#echo $LastTime
#exit 0

getk="no"
getx="no"

densityfile="*$LastTime*density.dat"

numdensityfiles=`ls $densityfile | wc -l`

if [ "$numdensityfiles" -lt 2 ]; # analysis needs to be done
  then 
echo "&ZERO_body" > analysis.inp
echo " Time_From=$LastTime              " >> analysis.inp
echo " Time_to=$LastTime               " >> analysis.inp
if [ "$dimension" -eq 1 ];
  then
  echo " AutoDilation=.T.            " >> analysis.inp
  echo " Kdip=0.00001 " >> analysis.inp
elif [ "$dimension" -gt 1 ];
  then
  echo " Dilation=4" >> analysis.inp
fi
echo " Time_Points=1            " >> analysis.inp
echo " /                       " >> analysis.inp
echo " &ONE_body               " >> analysis.inp
echo " Density_x=.T.            " >> analysis.inp
echo " Density_k=.T.            " >> analysis.inp
echo " /                       " >> analysis.inp
echo " &TWO_body               " >> analysis.inp
echo " /                       " >> analysis.inp
echo " &MANY_body               " >> analysis.inp
echo " /                       " >> analysis.inp
echo " &TWO_D                  " >> analysis.inp
echo " /                      " >> analysis.inp
MCTDHX_analysis
fi

densityfiles=`ls $densityfile`

for i in ${densityfiles};
do

 unset kdens
 kdens=`echo $i | grep k-density.dat`

 if [ ! -z "$kdens" ]; then
    out="momentum_density.png"
    title="K-space density MCTDHX(M=$M_Orbs)"
   if [ "$dimension" -eq 1 ];
     then
     xlabel="Momentum K"
     ylabel="{/Symbol=30 r}(K)"
   elif [ "$dimension" -eq 2 ];
     then
     xlabel="Momentum K_x"
     ylabel="Momentum K_y"
   elif [ "$dimension" -eq 3 ];
     then
     echo "3D momentum density not implemented yet..."
   fi
 else
    out="density.png"
    title="Density MCTDHX(M=$M_Orbs)"
    if [ "$dimension" -eq 1 ];
     then
     xlabel="Coordinate X"
     ylabel="{/Symbol=30 r}(X)"
   elif [ "$dimension" -eq 2 ];
     then
     xlabel="Coordinate X"
     ylabel="Coordinate Y"
   elif [ "$dimension" -eq 3 ];
     then
     xlabel="Coordinate X"
     ylabel="Coordinate Y"
     title="Density MCTDHX(M=$M_Orbs) at Z=0"
   fi
 fi

 if [ "$dimension" -eq 1 ];
   then
 $gnuplot << EOF
 set terminal png enhanced truecolor font "Helvetica,30" size 1618,1000
 set border 4095 lw 4.0
 set output "$out"
 set xlabel "$xlabel"
 set ylabel "$ylabel"
 set title "$title"
 plot "$i" u 1:4 t ""  w l lw 4 
EOF
 elif [ "$dimension" -eq 2 ];
    then
$gnuplot << EOF
set terminal png enhanced truecolor font "Helvetica,30" size 1618,1000
$(getpm3d $i)
set output "$out"
set xlabel "$xlabel"
set ylabel "$ylabel"
set title "$title"
splot "$i" u 1:2:4 t "" 
EOF
 elif [ "$dimension" -eq 3 ];
    then
   if [ -z "$kdens" ]; then
  gnuplot << EOF
set terminal png enhanced truecolor font "Helvetica,30" size 1618,1000
$(getpm3d $i)
set output "$out"
set xlabel "$xlabel"
set ylabel "$ylabel"
set title "$title"
splot "$i" u 1:2:(sqrt(\$3**2)<=0.05?\$4:1/0) t "" 
EOF
   fi
 fi
done
