#!/bin/bash
# I am a wrapper for the relaxatio and propagation parameter scans
# I am to be excecuted in the parent directory, $PARDIR

cat $MCTDHXDIR/../Computation_Scripts/logo
echo "LETS SIMULATE SOME QUANTUM MANY-BODY DYNAMICS!!!!!!!!!!!"
sleep 2
cat $MCTDHXDIR/../Computation_Scripts/alice 
#cat monster

# clean up
rm -f $(ls ./*Prop_run*sh)
rm -f $(ls ./*Relax_run*sh )
rm -f $(ls ./*anal_run*sh )
rm -f $(ls ./Relax_Array )
rm -f $(ls ./Sent_To_Prop )

MONSTER_ID=$RANDOM$(date +%N)

source ~/.bashrc > /dev/null 2>&1
. ./ParameterScan.inp
. $MCTDHXDIR/../Computation_Scripts/ParameterScan_Propagation.sh
. $MCTDHXDIR/../Computation_Scripts/ParameterScan_Relaxation.sh
. $MCTDHXDIR/../Computation_Scripts/do_Analysis.sh

if [ -n "$1"  ]; then
 PARDIR=$1
else
 PARDIR=$PWD
fi
## CASE 1 : relaxations 
if [ "$Do_Relax" == "T"  ]; then 
  bin=$(echo "$binary" | tr ' ' '\n' | grep MCTDH | sed 's/\.\///')
  if [[ -n "$(ls $PARDIR/libmctdhx.so)" && -n "$(ls $PARDIR/$bin)" && -n "$(ls $PARDIR/$Relaxation_Template)"  ]] ; then
    echo "Starting Relaxation Scan!"
    Relax "$PARDIR" 2>&1 | tee -a $OutputFile
  else
    echo "either the library, binary, or template are not found.  Check the parent director and the input file"  
  fi

## CASE 2 : No relaxation, but propagations
elif [ "$Do_Propagations" == "T"  ]; then
  echo "Skipping Relaxations"
  if [ "$Propagation_Start" == "BINR" ]; then
    ## CASE 2a: Propagations started from some binaries
    ## Binaries, PSI_bin and CIc_BIN must be in $PARDIR
    if [[ -n "$(ls $PARDIR/PSI_bin 2>/dev/null)" && -n "$(ls $PARDIR/CIc_bin 2>/dev/null)" ]]; then
      echo "Starting Propagations from binaries in $PARDIR"
      Propagation "$PARDIR"  2>&1 | tee -a $OutputFile
    else
      echo "PSI_bin or CIc_bin not found, try again"
    fi
  elif [ "$Propagation_Start" == "HAND" ]; then
    ## CASE 2b: Propagations started from initial guess, HAND
    echo "Starting propagations from HAND picked orbitals"
    Propagation "$PARDIR" 2>&1 | tee -a $OutputFile
  else
    echo "Invalid choice for \$Propagation_Start=$Propagation_Start"
    echo "Must be \"BIN\" or \"HAND\""
  fi

elif [ "$Do_Analysis" == "T"  ]; then
## CASE 3 : only analysis
 echo "In Analysis-Only case" 
 do_Analysis "$PARDIR" "any" 2>&1 | tee -a $OutputFile
 
else
 cat $MCTDHXDIR/../Computation_Scripts/alice
 echo "Nothing to do! check the input file and try again"
fi
