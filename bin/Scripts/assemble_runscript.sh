#!/bin/env - /bin/bash
#shopt -s expand_aliases
#source ~/.bashrc > /dev/null 2>&1

#function that assembles a runscript to run computations on a cluster
#as arguments the computation directory, the host, the executable, 
#number of nodes per job and the number of jobs per runscript 
#in the case of MPMD, ForceRun may be supplied.
#if MPMD is false: the function returns echoing the command to submit the created runscript.

## Oct 3, 2014: hermit case is updated with proper MPMD treatment, and RunFlag handling.  maia, bwgrid cases need to be updated
## Oct 8, 2014: added relaxation case
function assemble_runscript()
{
compdir=$1
forhost=$2
binary=$3
nnodes=$4
MPMD=$5
PARDIR=$6
ForceRun=$7 
mode=$(echo $8 | tr '[:upper:]' '[:lower:]')
ForceTime=$9
echo "in assemble_runscript"
echo "compdir=$compdir" #
echo "forhost=$forhost" #
echo "binary=$binary" #
echo "nnodes=$nnodes" #
echo "MPMD=$MPMD" #
echo "PARDIR=$PARDIR" #
echo "ForceRun=$ForceRun" #
echo "mode=$mode" #
echo "ForceTime=$ForceTime" #
. $MCTDHXDIR/Scripts/MPMDrun_prop.sh
. $MCTDHXDIR/Scripts/MPMDrun_relax.sh
. $MCTDHXDIR/Scripts/MPMDrun_anal.sh
if [ "$forhost" == "maia" ]; then
    cp $MCTDHXDIR/../PBS_Scripts/run-example-maia.sh $compdir/run.tmp
    cat $compdir/run.tmp | sed 's|\.\/MCTDHX.*|'"$binary"'|' > $compdir/run.sh
    np=$(echo "$nnodes*8" | bc)
    touch $compdir/RunFlag
    echo "qsub -pe mpi_openmp_8 $np $compdir/run.sh"
    MPMDrun_$mode $PARDIR $compdir "$runSTR" $nnodes $ForceRun 
elif [ "$forhost" == "brutus" ] || [ "$forhost" == "euler" ]; then
    cp $MCTDHXDIR/../PBS_Scripts/run-example-brutus.sh $compdir/run.tmp
    cat $compdir/run.tmp | sed 's|\.\/MCTDHX.*|'"$binary"'|' > $compdir/run.sh
    np="4"
    touch $compdir/RunFlag
    echo "bsub -n $np $compdir/run.sh"
    cd $compdir
    bsub -n $np ./run.sh
    cd -
#    MPMDrun_$mode $PARDIR $compdir "$runSTR" $nnodes $ForceRun 
elif [ "$forhost" == "inScript" ]; then
    cd $compdir && eval $binary && cd -
elif [ "$forhost" == "PC" ]; then
    #touch $compdir/RunFlag
    if [ "$MPMD" == "T" ]; then
      MPMDrun_$mode $PARDIR $compdir "$binary" $nnodes $ForceRun $ForceTime 
    else
      cd $compdir && ./$binary && cd - 
    fi
elif [ "$forhost" == "hornet"  ]; then
    if [ $mode == "anal" ]; then
      nproc=1
      taskpn=1
      threads=1
    else
      nproc=$(echo "$nnodes*2" | bc)
      taskpn=2
      threads=12
    fi
    runSTR="aprun -n $nproc -N $taskpn -d $threads -j 1 \$PWD/$binary"
    MPMDrun_$mode $PARDIR $compdir "$runSTR" $nnodes $ForceRun $ForceTime 
elif [ "$forhost" == "hermit" ] ; then 
    if [ $mode == "anal" ]; then
      nproc=1
      taskpn=1
      threads=1
    else
      nproc=$(echo "$nnodes*2" | bc)
      taskpn=2
      threads=14
    fi
    runSTR="aprun -n $nproc -N $taskpn -d $threads \$PWD/$binary"
    MPMDrun_$mode $PARDIR $compdir "$runSTR" $nnodes $ForceRun $ForceTime
elif [ "$forhost" == "nemo" ]; then
    cp $MCTDHXDIR/../PBS_Scripts/run-example-nemo.sh $compdir/run.tmp
    cat $compdir/run.tmp | sed "s|nodes=[0-9]*|nodes=$nnodes|" > $compdir/run.tmp1
    cat $compdir/run.tmp1 | sed 's|MCTDHX.*|'"$binary"'|' > $compdir/run.tmp2
    cat $compdir/run.tmp2 | sed "s|-np [0-9]*|-np $nnodes|" > $compdir/run.sh
    touch $compdir/RunFlag
    echo "msub $compdir/run.sh"
elif [ "$forhost" == "bwgrid" ]; then
    cp $MCTDHXDIR/../PBS_Scripts/run-example-BWGRID.sh $compdir/run.tmp
    cat $compdir/run.tmp | sed "s|nodes=[0-9]*|nodes=$nnodes|" > $compdir/run.tmp1
    cat $compdir/run.tmp1 | sed 's|MCTDHX.*|'"$binary"'|' > $compdir/run.tmp2
    cat $compdir/run.tmp2 | sed "s|-np [0-9]*|-np $nnodes|" > $compdir/run.sh
    touch $compdir/RunFlag
    echo "qsub $compdir/run.sh"
fi
if [ "$MPMD" != "T" ]; then
  chmod 755 $compdir/run.sh
  rm $compdir/run.tmp*
fi
}
