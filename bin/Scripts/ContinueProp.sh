#!/bin/bash

### I continue a propagation if appropriate, including building the runscript.  My output is a string to excecute the runscript
### USAGE: ContinueProp $ContinueFrom $PARDIR
### Created by storm on Sept 17, 2014
### Tested by storm on Sept 17, 2014
### Updated by storm on Sept 29, 2014: builds runscript and outputs $runSTR

function ContinueProp()
{
if [ "$#" -le 2 ] ; then
echo "USAGE: ContinueProp \$ContinueFrom \$PARIDIR \$NUMNODES \$ForceRun"
return
fi
    # ForceRun is an optional input for MPMD mode
    local ContinueFrom
    local PROPMAIN
    local Output_TimeStep
    local REST_TIME
    local TimeSteps
    ContinueFrom=$1
    PARDIR=$2
    NUMNODES=$3
   . $PARDIR/ParameterScan.inp
   . $MCTDHXDIR/Scripts/Adjust_Inp_Prop.sh
   . $MCTDHXDIR/Scripts/assemble_runscript.sh
   . $MCTDHXDIR/Scripts/rbincp.sh 
    PROPMAIN=$(echo $ContinueFrom | sed 's|Continue_.*||') #Strips off the continuation part
echo "PROPMAIN=$PROPMAIN"
echo "ContinueFrom=$ContinueFrom"
    Output_TimeStep=$(cat $PROPMAIN/MCTDHX.inp | grep -o "Output_TimeStep *= *[0-9]*\.[0-9]*" | grep -o "[0-9]*\.[0-9*]")
#echo "Output_TimeStep=$Output_TimeStep"
    REST_TIME=$(cat $ContinueFrom/NO_PR.out | tail -n 1 | awk '{print $1}' | cut -c 1-24)
#echo "REST_TIME=$REST_TIME"
    REST_TIME=${REST_TIME/[eE]-/*10^-}  #converts scientific notation into bash-readable format
#echo "REST_TIME=$REST_TIME"
    TimeSteps=$(echo "scale=0;$REST_TIME/$Output_TimeStep" | bc )
#echo "TimeSteps=$TimeSteps"
    REST_TIME=$(echo "$TimeSteps*$Output_TimeStep" | bc )
echo "REST_TIME=$REST_TIME"
    if [ " $(echo "$REST_TIME < $Prop_Time_Final" | bc) "  -eq 1 ] ; then
     ### Propagation is not finished
     echo "Propagation is not complete, so we continue!"
     NewProp=$PROPMAIN/Continue_$REST_TIME
     mkdir $NewProp 2> /dev/null
     echo "Copying binaries and library"
     rbincp $ContinueFrom $NewProp
     cp $PARDIR/$binary $PARDIR/libmctdhx.so $NewProp
     ## Make sure binaries copied properly:
    
     echo "Adjusting MCTDHX.inp"     
     Adjust_Inp_Prop $NewProp $PARDIR $PROPMAIN
     echo "Assembling runscript"
     assemble_runscript $NewProp $runhost $binary $NUMNODES $MPMD $PARDIR $ForceRun "prop"  
     return  1
    else
     ### Propagation is finished
     return 2
    fi

}
