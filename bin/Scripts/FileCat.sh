#!/bin/bash

## I take in a path to NO_PR.out or L_z_expectation.dat, and a desired new directory
## I then merge the file with the one existing in the new directory
## Created by Storm 10/30/2014
## Tested 10/30/2014

function FileCat()
{
if [ $# -lt 2 ]; then
 echo "Usage: \$Path \$File1 \$File2 ... "
 return 2
fi

Path=$1
shift
FileArr=("${@}")

for qq in ${FileArr[@]}; do
 Name=$(echo $qq | sed 's|.*/\(.*$\)|\1|')
  cat $qq >> $Path/$Name
  cat $Path/$Name | sed 's|^ *[0-9]*\.[0-9]*E.*||g' | sort -n -u > $Path/$Name.tmp
  mv $Path/$Name.tmp $Path/$Name 
done
}

