#!/bin/bash
shopt -s extglob

# function to check if an argument is an integer
# returns 0 if integer
# returns 1 if not integer

function check_integer()
{

varname=$1

if [ -n "$varname" ] && [ -z "${varname##[0-9]}" ];
 then
   echo "0"
   return 0
 else
   echo "1"
   return 1
fi
}
