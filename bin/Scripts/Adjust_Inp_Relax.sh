#!/bin/bash

### I take in a directory and parameter values, and adjust the 
### MCTDHX.inp file for Relaxations 
### I can tell if this is a first relaxation, or a continuation 
### and adjust accordingly!
### made by storm on Sept 17, 2014
function Adjust_Inp_Relax()
{
if [ $# -le 2 ] ; then
echo "Usage: Adjust_Inp_Relax \$PARDIR \$COMPDIR \$FirstTime (\$P1 \$P2 \$P3 \$P4 \$P5)"
return 0 
fi
 PARDIR=$1
 COMPDIR=$2
 FirstTime=$3
. $PARDIR/ParameterScan.inp
. $MCTDHXDIR/Scripts/RestTime.sh

Integration_Stepsize=1.d-1
Orbital_Integrator_MaximalStep=0.1

if [ "$FirstTime" -eq "1"  ]; then
 P1=$4
 P2=$5
 P3=$6
 P4=$7
 P5=$8
 ### First Relaxation 
 cp $PARDIR/$Relaxation_Template $COMPDIR/MCTDHX.inp
 ### Changes that are necessary for all relaxations 
 sed "s|^GUESS *=.*|GUESS=\'HAND\'|" < $COMPDIR/MCTDHX.inp > $COMPDIR/MCTDHX.inp.1
 sed 's|Job_Prefactor *=.*|Job_Prefactor=(-1.0d0,0.0d0)|' < $COMPDIR/MCTDHX.inp.1 > $COMPDIR/MCTDHX.inp 
 sed 's|Time_Final *=.*|Time_Final='$Relaxtime'|' < $COMPDIR/MCTDHX.inp > $COMPDIR/MCTDHX.inp.1
 sed 's|Integration_Stepsize *=.*|Integration_Stepsize='$Integration_Stepsize'|' < $COMPDIR/MCTDHX.inp.1 > $COMPDIR/MCTDHX.inp
 sed 's|Time_Begin *=.*|Time_Begin=0.d0|' < $COMPDIR/MCTDHX.inp > $COMPDIR/MCTDHX.inp.1
 sed "s|Coefficients_Integrator *=.*|Coefficients_Integrator=\'DAV\'\/ |" < $COMPDIR/MCTDHX.inp.1 > $COMPDIR/MCTDHX.inp
 sed 's|Orbital_Integrator_MaximalStep *=.*|Orbital_Integrator_MaximalStep='$Orbital_Integrator_MaximalStep'|' < $COMPDIR/MCTDHX.inp > $COMPDIR/MCTDHX.inp.1
 mv $COMPDIR/MCTDHX.inp.1 $COMPDIR/MCTDHX.inp
 ##rm -rf $COMPDIR/MCTDHX.inp.1
 ### Incorporate current set of parameters in scan 
  if [ "$NParameters" -ge 1 ] && [ -n "$Parameter1"  ] ; then
     sed "s|$Parameter1 *=.*|$Parameter1=$P1|" < $COMPDIR/MCTDHX.inp > $COMPDIR/MCTDHX.inp.1
     mv $COMPDIR/MCTDHX.inp.1 $COMPDIR/MCTDHX.inp
  fi
  if [ "$NParameters" -ge 2 ] && [ -n "$Parameter2"  ] ; then
     sed "s|$Parameter2 *=.*|$Parameter2=$P2|" < $COMPDIR/MCTDHX.inp > $COMPDIR/MCTDHX.inp.1
     mv $COMPDIR/MCTDHX.inp.1 $COMPDIR/MCTDHX.inp
  fi
  if [ "$NParameters" -ge 3 ] && [ -n "$Parameter3"  ] ; then
     sed "s|$Parameter3 *=.*|$Parameter3=$P3|" < $COMPDIR/MCTDHX.inp > $COMPDIR/MCTDHX.inp.1
     mv $COMPDIR/MCTDHX.inp.1 $COMPDIR/MCTDHX.inp
  fi
  if [ "$NParameters" -ge 4 ] && [ -n "$Parameter4"  ] ; then
     sed "s|$Parameter4 *=.*|$Parameter4=$P4|" < $COMPDIR/MCTDHX.inp > $COMPDIR/MCTDHX.inp.1
     mv $COMPDIR/MCTDHX.inp.1 $COMPDIR/MCTDHX.inp
  fi
  if [ "$NParameters" -ge 5 ] && [ -n "$Parameter5"  ] ; then
     sed "s|$Parameter5 *=.*|$Parameter5=$P5|" < $COMPDIR/MCTDHX.inp > $COMPDIR/MCTDHX.inp.1
     mv $COMPDIR/MCTDHX.inp.1 $COMPDIR/MCTDHX.inp
  fi
  
else
  ### Continued computation
  REST_TIME=$(RestTime $COMPDIR)
  ### Adjust times in MCTDHX.inp
  sed 's|Binary_Start_Time *=.*|Binary_Start_Time='$REST_TIME'|' < $COMPDIR/MCTDHX.inp > $COMPDIR/MCTDHX.inp.2
  sed 's|Time_Final *=.*|Time_Final='$Relaxtime'|' < $COMPDIR/MCTDHX.inp.2 > $COMPDIR/MCTDHX.inp.1
  sed "s|^GUESS *=.*|GUESS=\'BINR\'|" < $COMPDIR/MCTDHX.inp.1 > $COMPDIR/MCTDHX.inp
  rm $COMPDIR/MCTDHX.inp.1
  rm $COMPDIR/MCTDHX.inp.2
  echo "ADJUSTED INPUT, REST_TIME:" $REST_TIME
  echo "ADJ INP REL COMPDIR:" $COMPDIR
fi
}
