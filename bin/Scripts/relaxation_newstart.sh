#!/bin/bash

function newstart_relaxation {
if [ "$#" -ne 6 ]; then
    echo "relaxation_newstart: Illegal number of parameters"
    echo "Syntax: newstart_relaxation <Parameter to modify> <Value of parameter> <Computation directory> <Input file template> <final relaxation time> <executable to use>"
    return 1
fi

PARDIR=$PWD
# Parameter to modify
Par=$1
# Value of the parameter to set
Value=$2
# Computation directory
compdir=$3
# Input Template
template=$4
#final Time of relaxation
finaltime=$5
#binary to use
binary=$6

if [ ! -d "$compdir" ] ; 
  then

  mkdir $compdir

fi

sed 's/.*'"$Par"'.*/'"$Par"' = '"$Value"'/g' $PARDIR/$template > $PARDIR/MCTDHX.tmp1
sed 's/.*Time_Final.*/Time_Final='"$finaltime"'/g' $PARDIR/MCTDHX.tmp1 > MCTDHX.inp

cp $PARDIR/MCTDHX.inp $compdir
cp $PARDIR/$binary $PARDIR/libmctdhx.so $compdir

}
