#!/bin/bash/

## I copy the bin files and make sure the are the right size
## There was some issue where the copied bins are smaller and propagations could not continue
## Created by storm on Oct 7, 2014
function rbincp()
{
if [ "$#" -ge 3  ] || [ "$#" -lt 2 ]; then
echo "Usage: rbincp \$Old \$New"
return
fi
Old=$1
New=$2

OldPSI=$(ls -ltr $Old/PSI_bin | grep -o " [0-9][0-9][0-9][0-9]* " | grep -o "[0-9]*" )
OldCIc=$(ls -ltr $Old/CIc_bin | grep -o " [0-9][0-9][0-9][0-9]* " | grep -o "[0-9]*" )
NewPSI=$(ls -ltr $New/PSI_bin 2>/dev/null | grep -o " [0-9][0-9][0-9][0-9]* " | grep -o "[0-9]*" )
NewCIc=$(ls -ltr $New/CIc_bin 2>/dev/null | grep -o " [0-9][0-9][0-9][0-9]* " | grep -o "[0-9]*" )
count=0
##echo "OLDPSI=$OldPSI"
##echo "OldCIc-$OldCIc"
##echo "NewPSI=$NewPSI"
##echo "NewCIc=$NewCIc"
while [ "$OldPSI" != "$NewPSI" ] && [ "$OldCIc" != "$NewCIc" ] ; do
 if [ "$count" -ge 1 ]; then
   echo "Binaries did not copy entirely, lets try again"
##echo "OLDPSI=$OldPSI"
##echo "OldCIc-$OldCIc"
##echo "NewPSI=$NewPSI"
##echo "NewCIc=$NewCIc"
 fi
  cp $Old/PSI_bin $New &
  cp $Old/CIc_bin $New &
  wait
  NewPSI=$(ls -ltr $New/PSI_bin | grep -o " [0-9][0-9][0-9][0-9]* " | grep -o "[0-9]*" )
  NewCIc=$(ls -ltr $New/CIc_bin | grep -o " [0-9][0-9][0-9][0-9]* " | grep -o "[0-9]*" )
  let "count+=1"
done
if [ "$count" == "1" ] ; then
  echo "Copying worked on the first try"
elif [ "$count" == "0" ]; then
  echo "binaries are already copied"
else
  echo "Copying worked on try #$count"
fi
}
