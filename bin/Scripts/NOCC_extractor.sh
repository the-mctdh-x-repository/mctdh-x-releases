NO_LIST=`find ./ -name "NO_PR.out"`
PARDIR=$PWD


for i in $NO_LIST 
  do
   STRT=`head -n 1 $i | sed s'/E\-[0-9][0-9]//' | awk '{print $1}'`
   START=`echo "scale=0; ($STRT)/1" | bc`
   echo "scale=0; ($STRT)/1"
   echo "FOUND" $START "IN" $i
   FILENAME=$START"_NOCC"
   cp $i $PARDIR/$FILENAME
  done

SORTED_NOCCS=`ls *NOCC | sort -n`

echo $SORTED_NOCCS

rm $PARDIR/NOCCS_EVOLUTION
touch $PARDIR/NOCCS_EVOLUTION

for i in $SORTED_NOCCS
  do
   cat $i >> $PARDIR/NOCCS_EVOLUTION
  done

rm *_NOCC
