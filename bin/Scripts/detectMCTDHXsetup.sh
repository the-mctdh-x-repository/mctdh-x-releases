#!/bin/bash
# This script extracts information on the MCTDHX installation for the GUI
# and writes this information to the file ~/.guantumrc .
shopt -s expand_aliases

if [ -z "$MCTDHXDIR" ]; then
   echo "Your MCTDHXDIR environment variable is empty -- it is likely that you do not have an installed copy of MCTDHX or you have not sourced your .bashrc"
   exit 1
else
   cd $MCTDHXDIR/../
   DIR=`pwd`"/"
fi

craycount=`set | grep -i "cray" | wc -l`

if [ "$craycount" -ge 10 ]; then
   PLATFORM="CRAY"
   MPICOMMAND="aprun"
else
  if ! type "qsub" > /dev/null 2>&1; then
    PLATFORM="PC"
    MPICOMMAND="none"
  else
    PLATFORM="CLUSTER"
    MPICOMMAND="mpirun"
  fi
fi

USER=`echo $USER`

echo "PLATFORM=$PLATFORM" > ~/.guantumrc
echo "DIRbin=$MCTDHXDIR" >> ~/.guantumrc 
echo "DIR=$DIR" >> ~/.guantumrc
echo "MPIWRAPPER=$MPICOMMAND" >> ~/.guantumrc
echo "COMPILER=$1" >> ~/.guantumrc
echo "USER=$USER" >> ~/.guantumrc
