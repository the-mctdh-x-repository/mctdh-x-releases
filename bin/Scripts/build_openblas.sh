#!/bin/bash
ME=$(readlink -f $0)
INSTDIR=$(echo $ME | sed 's|/bin/Scripts/build_openblas.sh|/External_Software/BLAS|')
cd $INSTDIR
make -j 
make PREFIX=$INSTDIR install

