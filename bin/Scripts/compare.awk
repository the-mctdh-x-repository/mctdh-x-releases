# this script compares the two files given as command line arguments
# and outputs to stdout
function abs(x) { 
                 return (((x < 0) ? -x : x) + 0.0e0);
                 } 

FNR==NR {  #if (NR == 1)  title1 = $0
           #if (NR == 2)  title2 = $0
           arr[NR]=$2; 
           nam[NR]=$1;
           next
         } 
        { 
         if (FNR <= 2) print $0
#         else if (FNR == 2) print title2
         else { if (abs($2-arr[FNR]) < 0.00000001e0) {test=" is OK "} 
                 else { test=" problematic " }
                 printf "%s %s %14.10e %s %14.10e %s %14.10e %s \n " ,$1,"reference ", $2, "current ", arr[FNR], "difference ", abs($2-arr[FNR]), test                 
                }
         }
