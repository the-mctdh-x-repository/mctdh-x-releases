#!/bin/bash

# Version 1:
apps=$(mdfind "kMDItemKind == 'Application'")

if [[ "$apps" == *"conda"* ]]; then
  echo "Conda installation found! Deactivating it..."
fi

# Version 2:
isthereconda=$(which conda)
if [[ "$isthereconda" != *"not found"* ]]; then
  echo "Conda installation found! Deactivating it..."
  conda deactivate
fi 

