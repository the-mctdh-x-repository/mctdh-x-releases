#!/bin/bash

if [ ! -f "$1/MCTDHX.inp" ];
  then
  echo "No input in the specified directory"
  exit 1
fi
if [ ! -f "$1/NO_PR.out" ];
  then
  echo "No NO_PR.out in the specified directory"
  exit 1
fi

LastEnergy=`cat $1/NO_PR.out | tail -n 1 | awk '{print $1}'`

echo $LastEnergy


#Ecol=`echo "scale=0; $M_Orbs+2" | bc`


#timeIncrement=`cat $1/MCTDHX.inp | grep "Output_TimeStep" | grep -o '[0-9]*\.[0-9]*' | grep '[0-9]'` 
#timeInitial=`cat $1/MCTDHX.inp | grep "Time_Begin" | grep -o '[0-9]*\.[0-9]*'` 
#timeFinal=`cat $1/NO_PR.out | tail -n 1 | awk '{print $1}' | cut -c 1-10`


#Nsteps=`echo "scale=0;($timeFinal-$timeInitial)/($timeIncrement)+1" | bc`
#LastTime=`echo "scale=6; $timeInitial+(($Nsteps-1)*$time_Increment)" | bc`


