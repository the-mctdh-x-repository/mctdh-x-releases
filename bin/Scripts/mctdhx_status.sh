#!/bin/bash
shopt -s expand_aliases
source ~/.bashrc

. $MCTDHXDIR/Scripts/dependencies


DIM=`cat MCTDHX.inp | grep 'DIM_MCTDH' | grep -o '[0-9]*'` # Store the number of orbitals in N_Pars
M_Orbs=`cat MCTDHX.inp | grep 'Morb' | grep -o '[0-9]*'` # Store the number of orbitals in M_Orbs
ASCII=`cat MCTDHX.inp | grep -i 'Write_Ascii' | cut -b 1-18 | awk 'match($0,"="){print substr($0,RSTART+1,3)}' | sed -e 's/\.//g' `
timeFinal=`cat NO_PR.out | tail -n 1 | awk '{print $1}' | cut -c 1-10`
timeIncrement=`cat MCTDHX.inp | grep -i "Output_TimeStep" | grep -o '[0-9]*\.[0-9]*' | grep '[0-9]'`
NSTEPS=`echo "scale=0;($timeFinal/$timeIncrement)" | bc`
timeFinal=`echo "scale=8;($NSTEPS*$timeIncrement)" | bc `



FRAGCOL=`echo "scale=0; 1+$M_Orbs/1.0" | bc `
ECOL=`echo "scale=0; 2+$M_Orbs/1.0" | bc `
Energy=`tail -n 1 NO_PR.out | awk '{print $'$ECOL'}'` 

if [ "$ASCII" == "F" ];
then
$gnuplot << EOF
set terminal png size 1618,1000 font "Helvetica,25"
set output "status.png"
set xlabel "Time"
set ylabel "Fragmentation"
set title "Status of the computation"
set label "Energy= $Energy" at 1,0.9
set key top right
plot [*:*][*:1.03] "NO_PR.out" u 1:(1-\$$FRAGCOL) t "Fragmentation" w l lt 1 lw 3
EOF
else
  densityfile=`ls $timeFinal*orbs.dat | tail -n 1`
  if [ "$DIM" -eq 1 ];
  then
$gnuplot << EOF
set terminal png size 1618,1000 font "Helvetica,14"
set output "status.png"
set title "Status of the computation"
set multiplot
set size 0.5,1.0
set origin 0,0
set xlabel "Time"
set ylabel "Fragmentation"
set label "Energy= $Energy" at 1,0.9
set key top right
plot [*:*][*:1.03] "NO_PR.out" u 1:(1-\$$FRAGCOL) t "Fragmentation" w l lt 1 lw 3
set origin 0.5,0
set size 0.5,1.0
set xlabel "X"
set ylabel "Density(X)"
unset label
unset title
plot "$densityfile" u 1:8 t "Density" w l lt 1 lw 3
EOF
  elif [ "$DIM" -eq 2 ];
  then
$gnuplot << EOF
set terminal png size 1618,1000 font "Helvetica,14"
set output "status.png"
set title "Status of the computation"
set multiplot
set size 0.5,1.0
set origin 0,0
set xlabel "Time"
set ylabel "Fragmentation"
set label "Energy= $Energy" at 1,0.9
set key top right
plot [*:*][*:1.03] "NO_PR.out" u 1:(1-\$$FRAGCOL) t "Fragmentation" w l lt 1 lw 3
set origin 0.5,0
set size 0.5,1.0
set pm3d map 
set size 0.5,1.0
unset label
set xlabel "X"
set ylabel "Y"
set title "Density"
splot "$densityfile" u 1:2:8 t "Density"
EOF
  fi
fi
