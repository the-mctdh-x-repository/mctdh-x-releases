#!/bin/bash
shopt -s expand_aliases
source ~/.bashrc > /dev/null 2>&1

#### function get_jobid
#### arguments: 1) "cdir" is the directory where the job is running
####            2) "runhost" is the computer


function get_jobid()
{

cdir=$1
runhost=$2
if [ "$runhost" == "brutus" ] || [ "$runhost" == "euler" ];
   then
      joblist=$(bjobs -a | grep "RUN" | awk '{print $1}')
      joblist+=" "$(bjobs -a | grep "PEND" | awk '{print $1}')
      for o in ${joblist} ;
        do
          workdir=`bjobs -l $n | tr -d '\n' | sed -n -e 's/^.*Execution\ CWD//p' | grep -o -P '(?<=<).*(?=>)' | tr -d ' '`
          if [ "$workdir" == "$cdir" ];
             then
               jobid=$o
          fi
      done
fi
if [ "$runhost" == "maia" ];
   then
      joblist=`qstat | sed -n '/[0-9][0-9].*/{p;}' | awk '{print $1}'`   # determine list of running jobs        
      for o in ${joblist} ;
        do
          workdir=`qstat -j $o | grep sge_o_workdir | sed 's/sge_o_workdir://' | sed -e 's/^[ \t]*//'`
          if [ "$workdir" == "$cdir" ];
             then
               jobid=$o
          fi
      done
fi
if [ "$runhost" == "bwgrid" ];
   then
      joblist=`qstat | sed -n '/[0-9][0-9].*/{p;}' | awk '{print $1}'`   # determine list of running jobs        
      for o in ${joblist} ;
        do
          workdir=`qstat -f $o | grep init_work_dir | sed 's/init_work_dir\ =\ //' | sed -e 's/^[ \t]*//'`
          if [ "$workdir" == "$cdir" ];
             then
               jobid=$o
          fi
      done
fi

if [ "$runhost" == "hermit" ];
   then
      joblist=`qstat -u $USER | sed -n '/[0-9][0-9].*/{p;}' | awk '{print $1}'`   # determine list of running jobs        
      for o in ${joblist} ;
        do
          workdir=`qstat -f $o | grep -A1 Output_Path | sed ':a;N;$!ba;s/\n//g' | sed 's|Output_Path\ =\ ||' | sed 's/^[ \t]*//' | sed 's/^[^\/]*\//\//' | awk '{$1=$1}{print}' | sed 's/ //g' | sed 's/\.o.*$//g'`
          if [ "$workdir" == "$cdir" ];
             then
               jobid=$o
          fi
      done
fi

echo $jobid
}
