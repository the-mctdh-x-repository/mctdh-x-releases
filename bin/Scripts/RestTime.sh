#!/bin/bash


## I compute the rest time in units of output_time_step

function RestTime()
{
if [ "$#" -ge 2 ]; then
  echo "USAGE: RestTime \$DIR"
fi
DIR=$1
Output_TimeStep=$(cat $DIR/MCTDHX.inp | grep -o "Output_TimeStep *= *[0-9]*\.[0-9]*" | grep -o "[0-9]*\.[0-9]*")
REST_TIME=$(cat $DIR/NO_PR.out | tail -n 1 | awk '{print $1}' | cut -c 1-24)
REST_TIME=${REST_TIME/[eE]-/*10^-}  #converts scientific notation into bash-readable format
TimeSteps=$(echo "scale=0;$REST_TIME/$Output_TimeStep" | bc )
REST_TIME=$(echo "$TimeSteps*$Output_TimeStep" | bc )
echo $REST_TIME
}
