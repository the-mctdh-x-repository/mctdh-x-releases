#!/bin/bash/

function RunFlagSleeper()
{
# sleep for $1 - 5 mins
## Usage: RunFlagSleeper \$Time \$File \$Dir1 \$Dir2 ....
time=$(echo "$1-300" | bc)
sleep $time 
shift
File=$1
shift
arr=("${@}")
for qq in ${arr[@]}; do
  rm $qq/$File
done
}
