#!/bin/bash

# I find the computations in a directory and echo their directories

. $MCTDHXDIR/Scripts/RemoveArrayElement.sh

function FindComputations()
{
DATASETS=`find $1 -name "NO_PR.out" | sed 's|/NO_PR.out||'`
DATASETS2=$DATASETS

for k in ${DATASETS2}; 
do
  if [[ ! -f $k/PSI_bin || ! -f $k/CIc_bin ]] ; then
     DATASETS=$(RmEle $k ${DATASETS})
  fi
done
echo $DATASETS
}
