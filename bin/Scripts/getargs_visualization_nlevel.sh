#!/bin/bash
shopt -s expand_aliases
source ~/.bashrc

function getargs_nlevel {

rm -f *gnu*
rm -f *ppm*
rm -f *gif*
rm -f *sort*
rm TMP

# process args
case "$1" in
    clean|-cl*)   
rm -f *gnu*
rm -f *ppm*
rm -f *gif*
rm -f *sort*
  exit 0 ;;
    help|-h*)   
echo "Usage: <script> <starting time> <stopping time> <time increment> <number of orbitals> <number of particles>"
echo "Example: 2D-DENSITY_X 0 15 1 10 20 will generate a movie from t=0 to t=15 in steps of 1 for N=20 particles and M=10 orbitals."
echo "Note: the ASCII *.dat must already exist in the working directory"
  exit 0 ;;
    "")   
echo "Usage: <script> <starting time> <stopping time> <time increment> <number of orbitals> <number of particles>"
echo "Example: 2D-DENSITY_X 0 15 1 10 20 will generate a movie from t=0 to t=15 in steps of 1 for N=20 particles and M=10 orbitals."
echo "Note: the ASCII *.dat must already exist in the working directory"
exit 1 ;
esac

case "$1" in
    [0-9]*)
echo "Plot from time" $1
echo "Plot until time" $2

if [ ! "$3" ]
        then
                echo "Step is not defined - trying default: dt= 0.1"
        else
                echo "Step is defined, dt="$3
fi

if [ ! "$4" ]
        then
                echo "Number Of orbitals is not defined - I stop"
echo "Usage: <script> <starting time> <stopping time> <time increment> <number of orbitals> <number of particles>"
echo "Example: 2D-DENSITY_X 0 15 1 10 20 will generate a movie from t=0 to t=15 in steps of 1 for N=20 particles and M=10 orbitals."
echo "Note: the ASCII *.dat must already exist in the working directory"
          exit 1
fi
if [ ! "$5" ]
        then
                echo "Number Of Bosons is not defined - I stop"
echo "Usage: <script> <starting time> <stopping time> <time increment> <number of orbitals> <number of particles>"
echo "Example: 2D-DENSITY_X 0 15 1 10 20 will generate a movie from t=0 to t=15 in steps of 1 for N=20 particles and M=10 orbitals."
echo "Note: the ASCII *.dat must already exist in the working directory"
          exit 1
fi

if [ ! "$6" ]
        then
                echo "Number Of Bosons is not defined - I stop"
echo "Usage: <script> <starting time> <stopping time> <time increment> <number of orbitals> <number of particles>"
echo "Example: 2D-DENSITY_X 0 15 1 10 20 will generate a movie from t=0 to t=15 in steps of 1 for N=20 particles and M=10 orbitals."
echo "Note: the ASCII *.dat must already exist in the working directory"
          exit 1
fi



esac
}
