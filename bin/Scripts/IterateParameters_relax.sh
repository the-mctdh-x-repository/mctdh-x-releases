#!/bin/bash

function IterateParameters()
{

if [ "$#" -ge 6 ]; then
 echo "Usage: IterateParameters \$Par1 \$Par2 \$Par3 \$Par4 \$Par5 "
 exit 1
fi
m=$1
l=$2
k=$3
j=$4
i=$5
. $MCTDHXDIR/Scripts/check_convergence.sh
. $MCTDHXDIR/Scripts/check_integer.sh
. $MCTDHXDIR/Scripts/assemble_runscript.sh
. $PARDIR/ParameterScan.inp
## Added by Storm
. $MCTDHXDIR/../Computation_Scripts/ParameterScan_Propagation.sh
. $MCTDHXDIR/../Computation_Scripts/do_Analysis.sh
. $MCTDHXDIR/Scripts/Adjust_Inp_Relax.sh
. $MCTDHXDIR/Scripts/ExclusiveAdd.sh
. $MCTDHXDIR/Scripts/RemoveArrayElement.sh

Relax_Array=($(readarray $PARDIR/$Relax_Array_File))
Sent_To_Prop=($(readarray $PARDIR/Sent_To_Prop))
          if [ "$NParameters" -eq 1 ] ; then
                  compsubdir="$Parameter1"_"$m"
          elif [ "$NParameters" -eq 2 ] ; then
                  compsubdir="$Parameter2"_"$l"_"$Parameter1"_"$m"
          elif [ "$NParameters" -eq 3 ] ; then
                  compsubdir="$Parameter3"_"$k"_"$Parameter2"_"$l"_"$Parameter1"_"$m"
          elif [ "$NParameters" -eq 4 ] ; then
                  compsubdir="$Parameter4"_"$j"_"$Parameter3"_"$k"_"$Parameter2"_"$l"_"$Parameter1"_"$m"
          elif [ "$NParameters" -eq 5 ] ; then
                  compsubdir="$Parameter5"_"$i"_"$Parameter4"_"$j"_"$Parameter3"_"$k"_"$Parameter2"_"$l"_"$Parameter1"_"$m"
          fi
          compdir=$PARDIR/$compsubdir 
          echo "Computation dir: $compdir"
          mkdir $compdir 2> /dev/null
          compdir_jobsubmitted=$( ls $compdir/RunFlag 2> /dev/null)
          if [ -z "$compdir_jobsubmitted" ]; then
            ## No job running, check for convergence
            if [ "$(get_convergence $compdir $ConvDig)" -eq 1  ]; then
              ## calculation is already converged, remove from $relax_array
              echo "Relaxation is converged"
              Relax_Array=(`RmEle $compsubdir ${Relax_Array[@]}`)
              IsIn_STR=$(printf "%s\n" "${Sent_To_Prop[@]}" | grep "^${compsubdir}*$")
              if [ -z $IsIn_STR ]; then
               Sent_To_Prop=(`xadd $compsubdir ${Sent_To_Prop[@]}`)
               if [ "$Do_Propagations" == "T" ]; then
                 echo "Sending to Propagation!"
                 if [ -s $compdir/Prop_$OutputFile ]; then
                  OutCount=$( ls $compdir/Prop_$OutputFile"_[0-9]*" 2>&1 | sort -n | tail -n 1  )
                  let "Outcount += 1"
                  File=Prop_$OutputFile"_"$Outcount
                 else
                  File=Prop_$OutputFile
                 fi
                 echo "Propagation output file: $compdir/$File"
                 Propagation $compdir > $compdir/$File 2>&1  &
                 echo "Running in bg with PID=$!"
               else
                 echo "skipping Propagation"
               fi
               if [ "$Do_Analysis" == "T" ]; then
                 echo "Sending To analysis!"
                 if [ -s $compdir/Anal_$OutputFile ]; then
                  OutCount=$( ls $compdir/Anal_$OutputFile"_[0-9]*" 2>&1 | sort -n | tail -n 1  )
                  let "Outcount += 1"
                  File=Anal_$OutputFile"_"$Outcount
                 else
                  File=Anal_$OutputFile
                 fi
                 echo "Analysis output file: $compdir/$File"
                 do_Analysis $PARDIR relax $compdir > $compdir/$File 2>&1 &
                 echo "Running in bg with PID=$!"
               else
                echo "Skipping Analysis"
               fi
              else
               echo "Already sent to Propagation or skipped"
              fi
              echo "Relax_Array: ${#Relax_Array[@]} Elements"
              echo ${Relax_Array[@]}

            else
              ## computation is not converged, restart it
              echo "Relaxation is not converged, lets do something about that"
              if [ ! -s "$compdir/PSI_bin" ] || [ ! -s "$compdir/CIc_bin" ] || [ ! -s "$compdir/NO_PR.out" ] || [ ! -s "$compdir/MCTDHX.inp" ] ; then
               ## if one of these files is missing, treat it like an initial relaxation
               echo "Either a binary, NO_PR.out, or MCTDHX.inp is missing: Begining first relaxation"
               echo "Copying executable and library"
               bin=$(echo "$binary" | tr ' ' '\n' | grep MCTDH | sed 's/\.\///')
               cp $PARDIR/$bin $PARDIR/libmctdhx.so $compdir
               echo "Adjusting input"
               Adjust_Inp_Relax $PARDIR $compdir 1 $m $l $k $j $i
              else
               ## Treat this like a continued relaxation
               echo "Binaries, NO_PR.out, and MCTDHX.inp detected: continuing relaxation"
               Adjust_Inp_Relax $PARDIR $compdir 0
              fi 
              echo "Assembling runscript"
              Morb=$(cat $compdir/MCTDHX.inp | grep -o "Morb *= *[0-9]*" | grep -o "[0-9]*" )   
              NUMNODES=$(echo "scale=0;($Morb+1)/2.0" | bc )
              if [ "$NUMNODES" -gt "$MaxNodes" ] ; then  NUMNODES=$MaxNodes  ; fi
              echo "Number of Nodes:" $NUMNODES "Maximal number of nodes:" $MaxNodes
              assemble_runscript "$compdir" "$runhost" "$binary" "$NUMNODES" "$MPMD" "$PARDIR" "$ForceRun" "relax"
              Relax_Array=(`xadd $compsubdir ${Relax_Array[@]}`)
              echo "Relax Array: ${#Relax_Array[@]} Elements"
              echo "${Relax_Array[@]}"
            fi           
          else
            ## Job already running, wait it out
            echo "RunFlag found, waiting for job to finish"
          fi
printf "%s\n" "${Sent_To_Prop[@]}" > $PARDIR/Sent_To_Prop
printf "%s\n" "${Relax_Array[@]}" > $PARDIR/$Relax_Array_File
}
