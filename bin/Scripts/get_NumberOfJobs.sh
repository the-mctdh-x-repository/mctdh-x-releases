#!/bin/bash
shopt -s expand_aliases
source ~/.bashrc > /dev/null 2>&1

function get_NumberOfJobs()
{
forhost=$1
if [ "$forhost" == "PC" ];  then
   jobnumber=1
elif [ "$forhost" == "maia" ] || [ "$forhost" == "bwgrid" ];  then
   ### Add a grep to only get Q'd and running jobs
   jobnumber=$(qstat | sed -n '/[0-9][0-9].*/{p;}' | awk '{print $1}' | wc -l)   # determine list of running jobs        
elif [ "$forhost" == "nemo" ]; then
   jobnumber=$(showq | grep $USER | grep Running | wc -l)
elif [ "$forhost" == "hermit" ] || [ "$forhost" == "hornet" ] ; then ### hermit 
   jobnumber=$(qstat -u $USER | sed -n '/[0-9][0-9].*/{p;}' | awk '{print $10}' | grep -o [Q,R,E]| wc -l)   # determine list of running and q'd jobs        
elif [ "$forhost" == "brutus" ] || [ "$forhost" == "euler" ] ; then ### hermit 
   jobnumber=$( bjobs -a | grep "RUN" | awk '{print $1}' | wc -l)   # determine list of running and q'd jobs        
fi 

echo $jobnumber

}

