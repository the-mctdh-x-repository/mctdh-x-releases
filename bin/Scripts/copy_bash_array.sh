#!/bin/sh

function cp_hash
{ ### This function copies array $1 to $2.  I didn't find a built-in mechanism for this, so I copied this snippet from the internet
    local original_hash_name="$1"
    local copy_hash_name="$2"
    local __copy__=$(declare -p $original_hash_name);
    eval declare -a __copy__="${__copy__:$(expr index "${__copy__}" =)}";
  ### For some reason "-A" sporadically caused the program to crash on the Hermit but "-a" has no problems
    for c in "${!__copy__[@]}"
    do
        eval ${copy_hash_name}[$c]=${__copy__[$c]}
    done
}
