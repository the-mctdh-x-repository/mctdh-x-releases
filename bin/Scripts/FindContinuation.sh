#!/bin/bash
#usage: FindContinuation $PROPMAIN
### I look inside a $PROPMAIN directory and figure out where to start the continuation
### Created by storm on Sept 17, 2014
### Tested by storm on Sept 17, 2014

function FindContinuation()
{
if [ "$#" -eq 0 ]; then
echo "USAGE: FindContinuation \$PROPMAIN"
return
fi

local PROPMAIN=$1

for qq in $(ls -d $PROPMAIN/Continue_* 2> /dev/null | grep -o "Continue_[0-9]*\.[0-9]*" | sort -t _ -k 2 -g -r ) ; do
 ### Start at continuation with furthest time
 ### Check for nonempty NO_PR.out, CIc_bin, PSI_bin
 #echo $qq
 cd $PROPMAIN/$qq
 if [ -n "$(ls ./NO_PR.out 2> /dev/null)" ]; then
  NO_PR=$(ls -dltr "NO_PR.out" | awk '{print $5}')
 else
  NO_PR=0
 fi
 if [ -n "$(ls ./CIc_bin 2> /dev/null )" ]; then
  CIc=$(ls -dltr "CIc_bin" | awk '{print $5}')
 else
  CIc=0
 fi
 if [ -n "$(ls ./PSI_bin 2> /dev/null )" ]; then
  PSI=$(ls -dltr "PSI_bin" | awk '{print $5}')
 else
  PSI=0
 fi
 #echo "NO_PR: $NO_PR CIc: $CIc PSI: $PSI"
 cd - > /dev/null
 if [ "$NO_PR" -ge "25" ] && [ "$CIc" -ge "25" ] && [ "$PSI" -ge "25" ] ; then
  echo "$PROPMAIN/$qq"
  return
 fi
 
done
 echo $PROPMAIN
 return

}
