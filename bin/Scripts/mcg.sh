SRC_f=`find ./source/ -name  "*.f"`
SRC_F=`find ./source/ -name  "*.F"`
SRC_f90=`find ./source/ -name  "*.f90"`
SRC_sh=`find ./ -name "*.sh"`

echo "PARSING MCTDHX SOURCE FILES for your request "$1""

if [ -z "$2" ] 
then

for i in $SRC_f $SRC_F $SRC_f90 $SRC_sh
do
echo "FILE "$i""
grep -hirn "$1" $i
done

fi 


if [ ! -z "$2" ] 
then

if [ "$2" == "f" ] 
then

SRC=$SRC_f

elif [ "$2" == "F" ] 
then

SRC=$SRC_F

elif [ "$2" == "f90" ]
then

SRC=$SRC_f90

elif [ "$2" == "sh" ]
then

SRC=$SRC_sh

else
"Unknown ending: sh, f , F or f90 is available"
exit 0; 
fi

for i in $SRC
do
echo "FILE "$i""
grep -hirn "$1" $i
done

fi


