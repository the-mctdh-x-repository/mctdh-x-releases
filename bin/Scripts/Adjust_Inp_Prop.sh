#!/bin/bash

### I take in a directory and parameter values, and adjust the 
### MCTDHX.inp file for propagations
### I can tell if this is a first propagation, or a continuation 
### and adjust accordingly!
### USAGE:
### Adjust_Inp_Prop $PROPDIR $PARDIR $P1 $P2 $P3 $P4 $P5
### Adjust_Inp_Prop $NewProp $PARDIR $PROPMAIN
### made by storm on Sept 17, 2014
### tested by storm on Sept 18, 2014
### todo: make $PARDIR first argument
function Adjust_Inp_Prop()
{
if [ $# -le 2 ] ; then
echo "Usage: Adjust_Inp_Prop \$PROPDIR \$PARDIR \$P1 \$P2 \$P3 \$P4 \$P5"
echo "Usage: Adjust_Inp_Prop \$NewProp \$PARDIR \$PROPMAIN"
return 0 
fi
 PARDIR=$2
. $PARDIR/ParameterScan.inp


Integration_Stepsize=1.d-5
Orbital_Integrator_MaximalStep=0.001


ContStr=`echo $1 | grep -o Continue_[0-9]*\.[0-9]`

if [ -z "$ContStr"  ]; then
 PROPDIR=$1
 P1=$3
 P2=$4
 P3=$5
 P4=$6
 P5=$7
 ### First computation
 if [ "$Relaxation_Template" != "$Propagation_Template" ];
   then
   cp $PARDIR/$Propagation_Template $PROPDIR/MCTDHX.inp
   echo " cp $PARDIR/$Propagation_Template $PROPDIR/MCTDHX.inp"
   echo " cp $PARDIR/$Propagation_Template $PROPDIR/MCTDHX.inp"
   echo " cp $PARDIR/$Propagation_Template $PROPDIR/MCTDHX.inp"
   else 
   cp $PROPDIR/../MCTDHX.inp $PROPDIR
 fi

 if [ "$Propagation_Start" == "BINR"  ]; then
   START_TIME=`cat $PROPDIR/../NO_PR.out | tail -n 1 | awk '{print $1}' | cut -c 1-10`
   START_TIME=`echo "scale=1;$START_TIME/1.0" | bc | sed 's|^\.|0\.|'`
   sed 's|Binary_Start_Time *=.*|Binary_Start_Time='$START_TIME'd0|' < $PROPDIR/MCTDHX.inp > $PROPDIR/MCTDHX.inp.1
   mv $PROPDIR/MCTDHX.inp.1 $PROPDIR/MCTDHX.inp
 fi
 ### Changes that are necessary for all propagations
 sed "s|^GUESS *=.*|GUESS=\'$Propagation_Start\'|" < $PROPDIR/MCTDHX.inp > $PROPDIR/MCTDHX.inp.1
 sed 's|Job_Prefactor *=.*|Job_Prefactor=(0.0d0,-1.0d0)|' < $PROPDIR/MCTDHX.inp.1 > $PROPDIR/MCTDHX.inp 
 sed 's|Time_Final *=.*|Time_Final='$Prop_Time_Final'|' < $PROPDIR/MCTDHX.inp > $PROPDIR/MCTDHX.inp.1
 sed 's|Integration_Stepsize *=.*|Integration_Stepsize='$Integration_Stepsize'|' < $PROPDIR/MCTDHX.inp.1 > $PROPDIR/MCTDHX.inp
 sed 's|Time_Begin *=.*|Time_Begin=0.d0|' < $PROPDIR/MCTDHX.inp > $PROPDIR/MCTDHX.inp.1
 sed "s|Coefficients_Integrator *=.*|Coefficients_Integrator=\'MCS\'\/ |" < $PROPDIR/MCTDHX.inp.1 > $PROPDIR/MCTDHX.inp
 sed 's|Orbital_Integrator_MaximalStep *=.*|Orbital_Integrator_MaximalStep='$Orbital_Integrator_MaximalStep'|' < $PROPDIR/MCTDHX.inp > $PROPDIR/MCTDHX.inp.1
 mv $PROPDIR/MCTDHX.inp.1 $PROPDIR/MCTDHX.inp
 ##rm -rf $PROPDIR/MCTDHX.inp.1
 ### Incorporate current set of parameters in scan 
  if [ "$Prop_NParameters" -ge 1 ] && [ -n "$Prop_Parameter1"  ] ; then
     sed "s|$Prop_Parameter1 *=.*|$Prop_Parameter1=$P1|" < $PROPDIR/MCTDHX.inp > $PROPDIR/MCTDHX.inp.1
     mv $PROPDIR/MCTDHX.inp.1 $PROPDIR/MCTDHX.inp
  fi
  if [ "$Prop_NParameters" -ge 2 ] && [ -n "$Prop_Parameter2"  ] ; then
     sed "s|$Prop_Parameter2 *=.*|$Prop_Parameter2=$P2|" < $PROPDIR/MCTDHX.inp > $PROPDIR/MCTDHX.inp.1
     mv $PROPDIR/MCTDHX.inp.1 $PROPDIR/MCTDHX.inp
  fi
  if [ "$Prop_NParameters" -ge 3 ] && [ -n "$Prop_Parameter3"  ] ; then
     sed "s|$Prop_Parameter3 *=.*|$Prop_Parameter3=$P3|" < $PROPDIR/MCTDHX.inp > $PROPDIR/MCTDHX.inp.1
     mv $PROPDIR/MCTDHX.inp.1 $PROPDIR/MCTDHX.inp
  fi
  if [ "$Prop_NParameters" -ge 4 ] && [ -n "$Prop_Parameter4"  ] ; then
     sed "s|$Prop_Parameter4 *=.*|$Prop_Parameter4=$P4|" < $PROPDIR/MCTDHX.inp > $PROPDIR/MCTDHX.inp.1
     mv $PROPDIR/MCTDHX.inp.1 $PROPDIR/MCTDHX.inp
  fi
  if [ "$Prop_NParameters" -ge 5 ] && [ -n "$Prop_Parameter5"  ] ; then
     sed "s|$Prop_Parameter5 *=.*|$Prop_Parameter5=$P5|" < $PROPDIR/MCTDHX.inp > $PROPDIR/MCTDHX.inp.1
     mv $PROPDIR/MCTDHX.inp.1 $PROPDIR/MCTDHX.inp
  fi
  
else
  NewProp=$1
  PROPMAIN=$3
  REST_TIME=`echo "$NewProp" | grep -o "Continue_[0-9]*\.[0-9]*" | sed 's|Continue_||'`

  ### Continued computation

  ### Adjust times in MCTDHX.inp
  sed 's|Binary_Start_Time *=.*|Binary_Start_Time='$REST_TIME'|' < $PROPMAIN/MCTDHX.inp > $NewProp/MCTDHX.inp
  sed 's|Time_Begin *=.*|Time_Begin='$REST_TIME'|' < $NewProp/MCTDHX.inp > $NewProp/MCTDHX.inp.1
  sed 's|Time_Final *=.*|Time_Final='$Prop_Time_Final'|' < $NewProp/MCTDHX.inp.1 > $NewProp/MCTDHX.inp
  sed 's|Orbital_Integrator_MaximalStep *=.*|Orbital_Integrator_MaximalStep='$Orbital_Integrator_MaximalStep'|' < $NewProp/MCTDHX.inp > $NewProp/MCTDHX.inp.1
  sed 's|Integration_Stepsize *=.*|Integration_Stepsize='$Integration_Stepsize'|' < $NewProp/MCTDHX.inp.1 > $NewProp/MCTDHX.inp
 
  rm $NewProp/MCTDHX.inp.1
fi

}
