#!/bin/bash
shopt -s expand_aliases
source ~/.bashrc
export btl_sm_use_knem=0


if [ "$#" -lt "2" ];
  then
    echo "Not enough input arguments, please use the test-suite as follows: 
testMCTDHX.sh <Test to Perform> <Compiler used for executable>
Choose the test from <1,...,24,all,DVR,ORB,CI,PROP,WW> and the Compiler from <gcc,intel>.
<all> will run all 24 tests, <DVR> will run tests with all DVRs in 1/2/3 dimensions, 
<ORB> and <CI> will test the different integrators for the orbital and coefficient equations of motion, respectively.
<PROP> will test wether the long-time evolution is correctly implemented and 
<WW> will run tests with the different possible ways of representing the interparticle interaction potential."
exit 1
fi



function doTest ()
{
if [ "$MCTDHXDIR" == " " ]; 
then 
echo "Your MCTDHXDIR environment variable is not set -- did the program install correctly?"
exit 1
fi
if  test -e $MCTDHXDIR/libmctdhx.so; then
echo "Library exists, continuing..."
else 
echo "Library does not exist, exiting..."
exit 1
fi

if test -e $MCTDHXDIR/MCTDHX_$2; then
echo "Executable exists, continuing..."
else
echo "Executable does not exist, exiting..."
fi

### Since we checked for a compiled installation, no the test can be performed


# create tests subdir if nonexistant
if [ ! -d "$MCTDHXDIR/tests" ]; then
mkdir $MCTDHXDIR/tests
fi

# create tests sub - subdir if nonexistant
if [ ! -d "$MCTDHXDIR/tests/$1" ]; then
mkdir $MCTDHXDIR/tests/$1
fi

TESTDIR=$MCTDHXDIR/tests/$1
cd $TESTDIR

# all necessary files are here
if [ "$1" == "all" ]; then
   # if "all" tests are desired, then all the available tests will be performed
   for i in `ls $MCTDHXDIR/Scripts/test-references`; do
       runtest $i $2
   done 
elif [ "$1" == "DVR" ]; then
   for i in `seq 1 12`; do
       runtest $i $2
   done 
elif [ "$1" == "ORB" ]; then
   for i in `seq 13 17`; do
       runtest $i $2
   done 
elif [ "$1" == "CI" ]; then
   for i in `seq 18 19`; do
       runtest $i $2
   done 
elif [ "$1" == "PROP" ]; then
   runtest 20 $2
elif [ "$1" == "WW" ]; then
   for i in `seq 21 24`; do
       runtest $i $2
   done 
else
   # if only a single test is desired
  runtest $1 $2
fi

}

#runtest runs the test, it takes as argument 1 the test number and as argument 2 the compiler
function runtest ()
{
CRAY=`set | grep -i "cray" | wc -l`

cp $MCTDHXDIR/Scripts/test-references/$1 ./reference
cp $MCTDHXDIR/Scripts/testMCTDHX.inp ./MCTDHX.inp$1 

#### Tests 1-12 are for the DVR in 1/2/3D
if [ "$1" -eq "1" ]; then
   cat ./MCTDHX.inp$1 > ./MCTDHX.inp
fi
if [ "$1" -eq "2" ]; then
   sed -e 's|^DVR_X.*|DVR_X=3|I' ./MCTDHX.inp$1 > ./MCTDHX.inp
fi
if [ "$1" -eq "3" ]; then
   sed -e 's|^DVR_X.*|DVR_X=4|I' ./MCTDHX.inp$1 > ./MCTDHX.inp
fi
if [ "$1" -eq "4" ]; then
   sed -e 's|^DVR_X.*|DVR_X=5|I' ./MCTDHX.inp$1 > ./MCTDHX.inp
fi
if [ "$1" -eq "5" ]; then
   sed -e 's|^DVR_X.*|DVR_X=1|I' ./MCTDHX.inp$1 > ./MCTDHX.inp_a
   sed -e 's|^DVR_Y.*|DVR_Y=1|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^NDVR_X.*|NDVR_X=13|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^NDVR_Y.*|NDVR_Y=13|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^time_final.*|time_final=30\.d0|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^whichpot.*|whichpot=\"HO2D\"|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^DIM_MCTDH.*|DIM_MCTDH=2|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^parameter2.*|parameter2=2\.d0|I' ./MCTDHX.inp_a > ./MCTDHX.inp
fi

if [ "$1" -eq "6" ]; then
   sed -e 's|^DVR_X.*|DVR_X=3|I' ./MCTDHX.inp$1 > ./MCTDHX.inp_a
   sed -e 's|^DVR_Y.*|DVR_Y=3|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^NDVR_X.*|NDVR_X=13|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^NDVR_Y.*|NDVR_Y=13|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^time_final.*|time_final=30\.d0|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^whichpot.*|whichpot=\"HO2D\"|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^DIM_MCTDH.*|DIM_MCTDH=2|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^parameter2.*|parameter2=2\.d0|I' ./MCTDHX.inp_a > ./MCTDHX.inp
fi
if [ "$1" -eq "7" ]; then
   sed -e 's|^DVR_X.*|DVR_X=4|I' ./MCTDHX.inp$1 > ./MCTDHX.inp_a
   sed -e 's|^DVR_Y.*|DVR_Y=4|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^NDVR_X.*|NDVR_X=13|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^NDVR_Y.*|NDVR_Y=13|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^time_final.*|time_final=30\.d0|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^whichpot.*|whichpot=\"HO2D\"|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^DIM_MCTDH.*|DIM_MCTDH=2|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^parameter2.*|parameter2=2\.d0|I' ./MCTDHX.inp_a > ./MCTDHX.inp
fi
if [ "$1" -eq "8" ]; then
   sed -e 's|^DVR_X.*|DVR_X=5|I' ./MCTDHX.inp$1 > ./MCTDHX.inp_a
   sed -e 's|^DVR_Y.*|DVR_Y=5|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^NDVR_X.*|NDVR_X=13|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^NDVR_Y.*|NDVR_Y=13|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^time_final.*|time_final=30\.d0|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^whichpot.*|whichpot=\"HO2D\"|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^DIM_MCTDH.*|DIM_MCTDH=2|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^parameter2.*|parameter2=2\.d0|I' ./MCTDHX.inp_a > ./MCTDHX.inp
fi

if [ "$1" -eq "9" ]; then
   sed -e 's|^DVR_X.*|DVR_X=1|I' ./MCTDHX.inp$1 > ./MCTDHX.inp_a
   sed -e 's|^DVR_Y.*|DVR_Y=1|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^DVR_Z.*|DVR_Z=1|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^NDVR_Z.*|NDVR_Z=13|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^NDVR_X.*|NDVR_X=13|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^NDVR_Y.*|NDVR_Y=13|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^time_final.*|time_final=20\.d0|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^whichpot.*|whichpot=\"HO3D\"|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^DIM_MCTDH.*|DIM_MCTDH=3|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^parameter2.*|parameter2=2\.d0|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^parameter3.*|parameter3=0\.3d0|I' ./MCTDHX.inp_b > ./MCTDHX.inp
fi
if [ "$1" -eq "10" ]; then
   sed -e 's|^DVR_X.*|DVR_X=3|I' ./MCTDHX.inp$1 > ./MCTDHX.inp_a
   sed -e 's|^DVR_Y.*|DVR_Y=3|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^DVR_Z.*|DVR_Z=3|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^NDVR_Z.*|NDVR_Z=13|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^NDVR_X.*|NDVR_X=13|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^NDVR_Y.*|NDVR_Y=13|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^time_final.*|time_final=20\.d0|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^whichpot.*|whichpot=\"HO3D\"|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^DIM_MCTDH.*|DIM_MCTDH=3|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^parameter2.*|parameter2=2\.d0|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^parameter3.*|parameter3=0\.3d0|I' ./MCTDHX.inp_b > ./MCTDHX.inp
fi
if [ "$1" -eq "11" ]; then
   sed -e 's|^DVR_X.*|DVR_X=4|I' ./MCTDHX.inp$1 > ./MCTDHX.inp_a
   sed -e 's|^DVR_Y.*|DVR_Y=4|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^DVR_Z.*|DVR_Z=4|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^NDVR_Z.*|NDVR_Z=13|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^NDVR_X.*|NDVR_X=13|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^NDVR_Y.*|NDVR_Y=13|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^time_final.*|time_final=20\.d0|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^whichpot.*|whichpot=\"HO3D\"|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^DIM_MCTDH.*|DIM_MCTDH=3|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^parameter2.*|parameter2=2\.d0|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^parameter3.*|parameter3=0\.3d0|I' ./MCTDHX.inp_b > ./MCTDHX.inp
fi
if [ "$1" -eq "12" ]; then
   sed -e 's|^DVR_X.*|DVR_X=5|I' ./MCTDHX.inp$1 > ./MCTDHX.inp_a
   sed -e 's|^DVR_Y.*|DVR_Y=5|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^DVR_Z.*|DVR_Z=5|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^NDVR_Z.*|NDVR_Z=7|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^NDVR_X.*|NDVR_X=7|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^NDVR_Y.*|NDVR_Y=7|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^time_final.*|time_final=120\.d0|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^whichpot.*|whichpot=\"HO3D\"|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^DIM_MCTDH.*|DIM_MCTDH=3|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^parameter2.*|parameter2=2\.d0|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^parameter3.*|parameter3=0\.3d0|I' ./MCTDHX.inp_b > ./MCTDHX.inp
fi

## tests 13 -- 17 are for the various orbital equation integrators
if [ "$1" -eq "13" ]; then
   sed -e 's|^DVR_X.*|DVR_X=4|I' ./MCTDHX.inp$1 > ./MCTDHX.inp_a
   sed -e 's|^DVR_Y.*|DVR_Y=4|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^NDVR_X.*|NDVR_X=16|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^NDVR_Y.*|NDVR_Y=16|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^time_final.*|time_final=20\.d0|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^whichpot.*|whichpot=\"HO2D\"|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^DIM_MCTDH.*|DIM_MCTDH=2|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^Orbital_Integrator.*|Orbital_Integrator=\"ABM\"|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^parameter2.*|parameter2=2\.d0|I' ./MCTDHX.inp_b > ./MCTDHX.inp
fi
if [ "$1" -eq "14" ]; then
   sed -e 's|^DVR_X.*|DVR_X=4|I' ./MCTDHX.inp$1 > ./MCTDHX.inp_a
   sed -e 's|^DVR_Y.*|DVR_Y=4|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^NDVR_X.*|NDVR_X=16|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^NDVR_Y.*|NDVR_Y=16|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^time_final.*|time_final=20\.d0|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^whichpot.*|whichpot=\"HO2D\"|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^DIM_MCTDH.*|DIM_MCTDH=2|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^Orbital_Integrator.*|Orbital_Integrator=\"OMPABM\"|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^parameter2.*|parameter2=2\.d0|I' ./MCTDHX.inp_b > ./MCTDHX.inp
fi
if [ "$1" -eq "15" ]; then
   sed -e 's|^DVR_X.*|DVR_X=4|I' ./MCTDHX.inp$1 > ./MCTDHX.inp_a
   sed -e 's|^DVR_Y.*|DVR_Y=4|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^NDVR_X.*|NDVR_X=16|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^NDVR_Y.*|NDVR_Y=16|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^time_final.*|time_final=20\.d0|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^whichpot.*|whichpot=\"HO2D\"|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^DIM_MCTDH.*|DIM_MCTDH=2|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^Orbital_Integrator.*|Orbital_Integrator=\"BS\"|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^parameter2.*|parameter2=2\.d0|I' ./MCTDHX.inp_b > ./MCTDHX.inp
fi
if [ "$1" -eq "16" ]; then
   sed -e 's|^DVR_X.*|DVR_X=4|I' ./MCTDHX.inp$1 > ./MCTDHX.inp_a
   sed -e 's|^DVR_Y.*|DVR_Y=4|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^NDVR_X.*|NDVR_X=16|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^NDVR_Y.*|NDVR_Y=16|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^time_final.*|time_final=20\.d0|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^whichpot.*|whichpot=\"HO2D\"|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^DIM_MCTDH.*|DIM_MCTDH=2|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^Orbital_Integrator=.*|Orbital_Integrator=\"RK\"|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^parameter2.*|parameter2=2\.d0|I' ./MCTDHX.inp_b > ./MCTDHX.inp
fi
if [ "$1" -eq "17" ]; then
   sed -e 's|^DVR_X.*|DVR_X=4|I' ./MCTDHX.inp$1 > ./MCTDHX.inp_a
   sed -e 's|^DVR_Y.*|DVR_Y=4|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^NDVR_X.*|NDVR_X=16|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^NDVR_Y.*|NDVR_Y=16|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^time_final.*|time_final=20\.d0|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^whichpot.*|whichpot=\"HO2D\"|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^DIM_MCTDH.*|DIM_MCTDH=2|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^Orbital_Integrator=.*|Orbital_Integrator=\"RK\"|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^Orbital_Integrator_Order=.*|Orbital_Integrator_Order=5|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^parameter2.*|parameter2=2\.d0|I' ./MCTDHX.inp_a > ./MCTDHX.inp
fi

#### test 18 and 19 are for the coefficients EOM integrators
if [ "$1" -eq "18" ]; then
   sed -e 's|^DVR_X.*|DVR_X=4|I' ./MCTDHX.inp$1 > ./MCTDHX.inp_a
   sed -e 's|^DVR_Y.*|DVR_Y=4|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^NDVR_X.*|NDVR_X=16|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^NDVR_Y.*|NDVR_Y=16|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^time_final.*|time_final=15\.d0|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^whichpot.*|whichpot=\"HO2D\"|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^DIM_MCTDH.*|DIM_MCTDH=2|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^Coefficients_Integratro=.*|Coefficients_Integrator=\"MCS\"|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^parameter2.*|parameter2=2\.d0|I' ./MCTDHX.inp_b > ./MCTDHX.inp
fi

if [ "$1" -eq "19" ]; then
   sed -e 's|^DVR_X.*|DVR_X=4|I' ./MCTDHX.inp$1 > ./MCTDHX.inp_a
   sed -e 's|^DVR_Y.*|DVR_Y=4|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^NDVR_X.*|NDVR_X=16|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^NDVR_Y.*|NDVR_Y=16|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^time_final.*|time_final=15\.d0|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^whichpot.*|whichpot=\"HO2D\"|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^DIM_MCTDH.*|DIM_MCTDH=2|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^Coefficients_Integratro=.*|Coefficients_Integrator=\"DSL\"|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^parameter2.*|parameter2=2\.d0|I' ./MCTDHX.inp_b > ./MCTDHX.inp
fi

##### test 20 is for propagations
if [ "$1" -eq "20" ]; then
   sed -e 's|^DVR_X.*|DVR_X=4|I' ./MCTDHX.inp$1 > ./MCTDHX.inp_a
   sed -e 's|^DVR_Y.*|DVR_Y=4|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^NDVR_X.*|NDVR_X=16|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^NDVR_Y.*|NDVR_Y=16|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^time_final.*|time_final=15\.d0|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^whichpot.*|whichpot=\"HO2D\"|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^DIM_MCTDH.*|DIM_MCTDH=2|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^Coefficients_Integratro=.*|Coefficients_Integrator=\"DSL\"|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^parameter2.*|parameter2=2\.d0|I' ./MCTDHX.inp_b > ./MCTDHX.inp
   cp $MCTDHXDIR/libmctdhx.so ./

   echo "Running test relaxation $1: CRAY:$CRAY"
   if [ "$CRAY" -ge 10 ];
      then
      aprun -N 1 -n 1 -d 1 MCTDHX_$2 > /dev/null 
   else
      MCTDHX_$2 # > /dev/null
   fi

   sed -e 's|^Job_Prefactor.*|Job_Prefactor=(0\.0d0,-1\.0d0) |I' ./MCTDHX.inp > ./MCTDHX.inp_a
   sed -e 's|^time_final.*|time_final=150\.d0|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^Binary_Start_Time.*|Binary_Start_time=15\.d0|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e "s|^GUESS='HAND'.*|GUESS='BINR'|I" ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^whichpot.*|whichpot=\"stir2D\"|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^parameter1.*|parameter1=1\.d0|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^parameter2.*|parameter2=1\.d0|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^parameter3.*|parameter3=10\.d0|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^parameter4.*|parameter4=0\.1d0|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^Coefficients_Integrator=.*|Coefficients_Integrator=\"MCS\"|I' ./MCTDHX.inp_a > ./MCTDHX.inp
fi

### test 21-24 are for the different possibilities to evaluate the interaction operators
if [ "$1" -eq "21" ]; then
   sed -e 's|^DVR_X.*|DVR_X=4|I' ./MCTDHX.inp$1 > ./MCTDHX.inp_a
   sed -e 's|^DVR_Y.*|DVR_Y=4|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^NDVR_X.*|NDVR_X=16|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^NDVR_Y.*|NDVR_Y=1|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^time_final.*|time_final=15\.d0|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^whichpot.*|whichpot=\"HO1D\"|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^DIM_MCTDH.*|DIM_MCTDH=1|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^Interaction_Type.*|Interaction_Type=1|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^parameter1.*|parameter1=1\.4d0|I' ./MCTDHX.inp_b > ./MCTDHX.inp
fi

if [ "$1" -eq "22" ]; then
   sed -e 's|^DVR_X.*|DVR_X=4|I' ./MCTDHX.inp$1 > ./MCTDHX.inp_a
   sed -e 's|^DVR_Y.*|DVR_Y=4|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^NDVR_X.*|NDVR_X=16|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^NDVR_Y.*|NDVR_Y=1|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^time_final.*|time_final=15\.d0|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^whichpot.*|whichpot=\"HO1D\"|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^DIM_MCTDH.*|DIM_MCTDH=1|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^Interaction_Type.*|Interaction_Type=2|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^parameter1.*|parameter1=1\.4d0|I' ./MCTDHX.inp_b > ./MCTDHX.inp
fi

if [ "$1" -eq "23" ]; then
   sed -e 's|^DVR_X.*|DVR_X=4|I' ./MCTDHX.inp$1 > ./MCTDHX.inp_a
   sed -e 's|^DVR_Y.*|DVR_Y=4|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^NDVR_X.*|NDVR_X=16|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^NDVR_Y.*|NDVR_Y=1|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^time_final.*|time_final=15\.d0|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^whichpot.*|whichpot=\"HO1D\"|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^DIM_MCTDH.*|DIM_MCTDH=1|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^Interaction_Type.*|Interaction_Type=3|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^parameter1.*|parameter1=1\.4d0|I' ./MCTDHX.inp_b > ./MCTDHX.inp
fi                                                
if [ "$1" -eq "24" ]; then
   sed -e 's|^DVR_X.*|DVR_X=4|I' ./MCTDHX.inp$1 > ./MCTDHX.inp_a
   sed -e 's|^DVR_Y.*|DVR_Y=4|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^NDVR_X.*|NDVR_X=16|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^NDVR_Y.*|NDVR_Y=1|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^time_final.*|time_final=15\.d0|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^whichpot.*|whichpot=\"HO1D\"|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^DIM_MCTDH.*|DIM_MCTDH=1|I' ./MCTDHX.inp_b > ./MCTDHX.inp_a
   sed -e 's|^Interaction_Type.*|Interaction_Type=4|I' ./MCTDHX.inp_a > ./MCTDHX.inp_b
   sed -e 's|^parameter1.*|parameter1=1\.4d0|I' ./MCTDHX.inp_b > ./MCTDHX.inp
fi                                                

cp $MCTDHXDIR/libmctdhx.so ./

echo "Running test $1"
if [ "$CRAY" -ge 10 ];
  then
  aprun -N 1 -n 1 -d 1 MCTDHX_$2 > /dev/null 
else
  MCTDHX_$2  > /dev/null
fi
# run comparison with reference

if [ -s "./current" ]; then
  awk -f $MCTDHXDIR/Scripts/compare.awk ./current ./reference > ./test_$1.out
else 
  echo "Program did not terminate test $1 , $PWD/test_$1.out file empty, exiting ...."
  echo "Program did not terminate test $1 , $PWD/test_$1.out file empty, exiting ...."
  echo "Program did not terminate test $1 , $PWD/test_$1.out file empty, exiting ...."
  exit 1
fi
# check wether test was successful

FAIL=`cat test_$1.out | grep "problematic" | wc -l`

if [ "$FAIL" -gt "0" ]; then
  echo "Test $1 has failed, check the output file $PWD/test_$1.out"
  echo "Test $1 has failed, check the output file $PWD/test_$1.out" >> $MCTDHXDIR/../Tests.log
  REMOVAL=0
else
  echo "Test $1 successful."
  echo "Test $1 successful." >> $MCTDHXDIR/../Tests.log
  REMOVAL=1
fi

}


rm $MCTDHXDIR/../Tests.log
REMOVAL=0

doTest $1 $2

if [ "$REMOVAL" -eq "1" ];
  then
  rm -rf $MCTDHXDIR/tests
fi

