#!/bin/env - /bin/bash
source ~/.bashrc > /dev/null 2>&1
## I assemble and manage MPMD runscripts
##USAGE: MPMDrun $PARDIR $COMPDIR $runSTR $NUMNODES $ForceRun
## Created by Storm on Sept 29, 2014
## Tested by Storm on Sept 29, 2014


function MPMDrun_prop()
{
PARDIR=$1
COMPDIR=$2
runSTR=$3
NUMNODES=$4
ForceRun=$5

if [ $# -le 4 ] ; then
echo "USAGE: MPMDrun_prop \$PARDIR \$COMPDIR \$runSTR \$NUMNODES \$ForceRun"
fi
echo "in MPMDrun_Prop"
echo "PARDIR=$PARDIR"
echo "COMPDIR=$COMPDIR"
echo "runSTR=$runSTR"
echo "NUMNOdes=$NUMNODES"
echo "ForceRun=$ForceRun"
. $PARDIR/ParameterScan.inp
. $MCTDHXDIR/Scripts/get_NumberOfJobs.sh

if [ "$MPMD" != "T" ]; then
  MPMDnodes=1
fi
## If no runscripts in the $PARDIR, copy one and strip of the line which runs the program
if [ -z "$(ls $PARDIR/[0-9]*Prop_run_$MonsterName.$MONSTER_ID.sh 2> /dev/null)"  ] ; then

 if [ "$forhost" == "maia" ]; then 
   cp $MCTDHXDIR/../PBS_Scripts/run-example-maia.sh $PARDIR/Prop_run_$MonsterName.$MONSTER_ID.tmp
   sed 's|mpirun.*||' < $PARDIR/Prop_run_$MonsterName.$MONSTER_ID.tmp > $PARDIR/Prop_run_$MonsterName.$MONSTER_ID.sh
   jobtime=86400
 elif [ "$forhost" == "hermit" ]; then
   cp $MCTDHXDIR/../PBS_Scripts/run-example-hermit.sh $PARDIR/Prop_run_$MonsterName.$MONSTER_ID.tmp
   sed 's|aprun.*||' < $PARDIR/Prop_run_$MonsterName.$MONSTER_ID.tmp > $PARDIR/Prop_run_$MonsterName.$MONSTER_ID.sh
   jobtime=86400
 elif [ "$forhost" == "hornet" ]; then
   cp $MCTDHXDIR/../PBS_Scripts/run-example-hornet.sh $PARDIR/Prop_run_$MonsterName.$MONSTER_ID.tmp
   sed 's|aprun.*||' < $PARDIR/Prop_run_$MonsterName.$MONSTER_ID.tmp > $PARDIR/Prop_run_$MonsterName.$MONSTER_ID.sh
   jobtime=86400
 elif [ "$forhost" == "bwgrid" ]; then
   cp $MCTDHXDIR/../PBS_Scripts/run-example-BWGRID.sh $PARDIR/Prop_run_$MonsterName.$MONSTER_ID.tmp
   sed 's|mpirun.*||' < $PARDIR/Prop_run_$MonsterName.$MONSTER_ID.tmp > $PARDIR/Prop_run_$MonsterName.$MONSTER_ID.sh
   jobtime=86400
 elif [ "$forhost" == "nemo" ]; then
   cp $MCTDHXDIR/../PBS_Scripts/run-example-nemo.sh $PARDIR/Prop_run_$MonsterName.$MONSTER_ID.tmp
   sed 's|mpiexec.*||' < $PARDIR/Prop_run_$MonsterName.$MONSTER_ID.tmp > $PARDIR/Prop_run_$MonsterName.$MONSTER_ID.sh
   jobtime=86400
 fi 

 sed 's|nodes=[0-9]*|nodes=0|' < $PARDIR/Prop_run_$MonsterName.$MONSTER_ID.sh > $PARDIR/Prop_run_$MonsterName.$MONSTER_ID.tmp
 sed 's|#PBS -N.*|#PBS -N MSTR_'$MonsterName.$MONSTER_ID'|' < $PARDIR/Prop_run_$MonsterName.$MONSTER_ID.tmp > $PARDIR/Prop_run_$MonsterName.$MONSTER_ID.sh_

 rm $PARDIR/Prop_run_$MonsterName.$MONSTER_ID.sh
 rm $PARDIR/Prop_run_$MonsterName.$MONSTER_ID.tmp
 chmod 755 $PARDIR/Prop_run_$MonsterName.$MONSTER_ID.sh_
 echo ". $MCTDHXDIR/Scripts/RunFlagSleeper.sh" >> $PARDIR/Prop_run_$MonsterName.$MONSTER_ID.sh_
 ## Include check_convergence.sh for mixed relax/prop compatibility
 echo ". $MCTDHXDIR/Scripts/check_convergence.sh" >> $PARDIR/Prop_run_$MonsterName.$MONSTER_ID.sh_ 
 echo "RunFlagSleeper $jobtime RunFlag &" >> $PARDIR/Prop_run_$MonsterName.$MONSTER_ID.sh_
 if [ "$forhost" == "nemo" ]; then
    echo "Sleeper_PID=\$!" >> $PARDIR/Prop_run_$MonsterName.$MONSTER_ID.sh_
 fi
 echo "(" >> $PARDIR/Prop_run_$MonsterName.$MONSTER_ID.sh_
 cp $PARDIR/Prop_run_$MonsterName.$MONSTER_ID.sh_ $PARDIR/1Prop_run_$MonsterName.$MONSTER_ID.sh
fi
####### Add $runSTR to the most recent runscript
### find runscript$N.sh (Prop_run_$MonsterName.$MONSTER_ID1.sh, Prop_run_$MonsterName 2.sh, etc)  that has numnodes less than $MPMDnodes
#qq=$(ls $PARDIR/[0-9]*Prop_run_$MonsterName.$MONSTER_ID.sh | sort -rg | head -n 1 )


qq=$(ls $PARDIR/*Prop_run_$MonsterName.$MONSTER_ID.sh | grep -o "/[0-9]*Prop_run_$MonsterName.$MONSTER_ID.sh" | cut -c 2- | sort -rg | head -n 1)
qq=$PARDIR/$qq

if  [ "$forhost" == "brutus" ] ||  [ "$forhost" == "euler" ]; then
  NodesInRun="9999999"
else
  NodesInRun=$(cat $qq | grep -o "nodes *= *[0-9]*" | grep -o "[0-9]*")
fi

  echo "Current runscript: $(echo $qq | sed 's|'$PARDIR'||')"
  if [ "$NodesInRun" -lt "$MPMDnodes"  ]; then
   ## The number of nodes already specified in the runscript doesnt exceed the maximum, so add on another job
   NewNodes=$(echo "$NodesInRun+$NUMNODES" | bc)
   echo "$NewNodes nodes in $(echo $qq | grep -o "/.*$")"
   RunCount=$(echo $qq | grep -o '[0-9]*Prop_run_'$MonsterName.$MONSTER_ID.sh | grep -o '^[0-9]*')
   ## Start each aprun command in its own subshell
   ## wait for 24 hrs after aprun, to guarantee that no subshell finishes before the job is done
   sed 's|nodes=[0-9]*|nodes='$NewNodes'|' < $qq > $qq-1
   sed 's|\(RunFlagSleeper .*\)\&|\1 '$COMPDIR' \&|' <$qq-1 >$qq
   sed 's|#PBS -N.*|#PBS -N MSTR_'$MonsterName.$MONSTER_ID'_'$RunCount'|' < $qq > $qq-1
   mv $qq-1 $qq
     echo "  (" >> $qq
     ## Sleeps for $jobtime - 5 mins, then removes runflag. in case aprun doesnt finish in $jobtime 
     echo "    cd $COMPDIR" >> $qq
     echo "    $runSTR" >> $qq
     ## remove runflag after aprun command has finished
     echo "    rm $COMPDIR/RunFlag" >> $qq
#     echo "sleep $jobtime" >> $qq-1
     if [ "$forhost" == "nemo" ]; then
      echo "    kill \$Sleeper_PID " >> $qq
     fi

     echo "  ) &" >> $qq 
#   mv $qq-1 $qq
   ## put a flag in the computation directory so it isnt computed multiple times
   touch $COMPDIR/RunFlag
   
   if [[ "$NewNodes" -ge "$MPMDnodes" || "$ForceRun" == "T"  ]]; then
    echo "Finishing $qq"
    ## This last process made the nodecount exceed the maximum, or there is a ForceRun flag passed
    ## So now, exceute the runscript and start another
    echo "  wait" >> $qq
    echo ") &" >> $qq
    echo "PID=\$!" >> $qq
    echo "wait \$PID" >> $qq
    ## Ensure the queue has room for the next job, if not, wait for it

    Njobs=$(get_NumberOfJobs $runhost)

    while [ "$Njobs" -ge "$maxjobs" ]; do
      echo "Njobs=$Njobs and maxjobs=$maxjobs"
      if [ "$forhost" == "nemo" ]; then
         echo "Waiting 2 minutes for que to clear"
         sleep 120
         Njobs=$(get_NumberOfJobs $runhost)
      else
         echo "Waiting 30 minutes for que to clear"
         sleep 1800
         Njobs=$(get_NumberOfJobs $runhost)
      fi
    done


if  [ "$forhost" != "brutus" ] ||  [ "$forhost" != "euler" ]; then
if [ "$forhost" == "nemo" ]; then
   echo "msub $qq"
   msub $qq
else
   echo "qsub $qq"
   qsub $qq
fi
else 
echo "bsub $qq"
bsub $qq
fi


RunCount=$(echo $qq | grep -o '[0-9]*Prop_run_'$MonsterName.$MONSTER_ID.sh | grep -o '^[0-9]*')
echo "Old RunCount=$RunCount"
let "RunCount++"
echo "New RunCount=$RunCount"
cp $PARDIR/Prop_run_$MonsterName.$MONSTER_ID.sh_ $PARDIR/$RunCount"Prop_run_$MonsterName.$MONSTER_ID.sh"
   fi
  fi

}


