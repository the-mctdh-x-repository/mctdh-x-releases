!>GPLv3
!    MCTDH-X: the multiconfigurational time-dependent Hartree for 
!    indistinguishable particles software
!
!    Copyright (C) 2021  A. U. J. Lode, M. C. Tsatsos, 
!                        E. Fasshauer, S. E. Weiner, 
!                        R. Lin, L. Papariello, P. Molignini, 
!                        C. Lévêque, M. Büttner , J. Xiang, S. Dutta
!
!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <https://www.gnu.org/licenses/>.
!
!
!
!
!<GPLv3










!> This module collects useful functions and vector routines
      MODULE Function_Library
      CONTAINS

!>  Compute factorial.
      FUNCTION Factorial(N)

      real*8 Factorial
      integer N,k

      Factorial=1.0d0
      Do k=1,N
         Factorial=Factorial*k
      EndDo

      end FUNCTION Factorial


!> Compute logarithm of factorial.
      FUNCTION LogFactorial(N)

      real*8 logFactorial
      integer N,k

      logFactorial=0.0d0
      Do k=2,N
         LogFactorial=LogFactorial + Log(1.0d0*k)
      EndDo

      end FUNCTION LogFactorial


!> Compute a Binomial coefficient.
      FUNCTION BinomialCoefficient(N,K)

      real*8 :: BinomialCoefficient!,beta
      integer :: N,K

      if(N.lt.K) then
        BinomialCoefficient=0.0d0
      return 
      endif

      BinomialCoefficient= 1/beta(DBLE(N-K+1),DBLE(K+1))/(N+1)

      end FUNCTION BinomialCoefficient


!> Compute the Gamma function.
      function ddgamma(x)

      implicit real*8 (a - h, o - z)
      real*8 p0,p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13
      real*8  ddgamma,w,y,x
      integer n,k

      parameter (
     &    p0 = 0.999999999999999990d+00, 
     &    p1 = -0.422784335098466784d+00, 
     &    p2 = -0.233093736421782878d+00, 
     &    p3 = 0.191091101387638410d+00, 
     &    p4 = -0.024552490005641278d+00, 
     &    p5 = -0.017645244547851414d+00, 
     &    p6 = 0.008023273027855346d+00)
      parameter (
     &    p7 = -0.000804329819255744d+00, 
     &    p8 = -0.000360837876648255d+00, 
     &    p9 = 0.000145596568617526d+00, 
     &    p10 = -0.000017545539395205d+00, 
     &    p11 = -0.000002591225267689d+00, 
     &    p12 = 0.000001337767384067d+00, 
     &    p13 = -0.000000199542863674d+00)

      n = nint(x - 2)
      w = x - (n + 2)
      y = ((((((((((((p13 * w + p12) * w + p11) * w + p10) * 
     &    w + p9) * w + p8) * w + p7) * w + p6) * w + p5) * 
     &    w + p4) * w + p3) * w + p2) * w + p1) * w + p0

      if (n .gt. 0) then
          w = x - 1
          do k = 2, n
              w = w * (x - k)
          end do
      else
          w = 1
          do k = 0, -n - 1
              y = y * (x + k)
          end do
      end if

      ddgamma = w / y

      end function ddgamma

!> Compute the logarithm of the Gamma function.
      function dlgama ( x )

      implicit none

      double precision c(7)
      double precision corr
      double precision d1
      double precision d2
      double precision d4
      double precision dlgama
      double precision eps
      double precision frtbig
      double precision four
      double precision half
      integer i
      double precision one
      double precision pnt68
      double precision p1(8)
      double precision p2(8)
      double precision p4(8)
      double precision q1(8)
      double precision q2(8)
      double precision q4(8)
      double precision res
      double precision sqrtpi
      double precision thrhal
      double precision twelve
      double precision two
      double precision x
      double precision xbig
      double precision xden
      double precision xinf
      double precision xm1
      double precision xm2
      double precision xm4
      double precision xnum
      double precision y
      double precision ysq
      double precision zero
      data one /1.0D+00/
      data half /0.5D+00/
      data twelve /12.0D+00/
      data zero /0.0D+00/
      data four /4.0D+00/
      data thrhal /1.5D+00/
      data two /2.0D+00/
      data pnt68 /0.6796875D+00/
      data sqrtpi /0.9189385332046727417803297D+00/
      data xbig /2.55D+305/
      data xinf /1.79D+308/
      data eps /2.22D-16/
      data frtbig /2.25D+76/
      data d1/-5.772156649015328605195174D-01/
      data p1/
     &   4.945235359296727046734888D+00,
     &   2.018112620856775083915565D+02,
     &   2.290838373831346393026739D+03,
     &   1.131967205903380828685045D+04,
     &   2.855724635671635335736389D+04,
     &   3.848496228443793359990269D+04,
     &   2.637748787624195437963534D+04,
     &   7.225813979700288197698961D+03/
      data q1/
     &   6.748212550303777196073036D+01,
     &   1.113332393857199323513008D+03,
     &   7.738757056935398733233834D+03,
     &   2.763987074403340708898585D+04,
     &   5.499310206226157329794414D+04,
     &   6.161122180066002127833352D+04,
     &   3.635127591501940507276287D+04,
     &   8.785536302431013170870835D+03/
      data d2/4.227843350984671393993777D-01/
      data p2/
     &   4.974607845568932035012064D+00,
     &   5.424138599891070494101986D+02,
     &   1.550693864978364947665077D+04,
     &   1.847932904445632425417223D+05,
     &   1.088204769468828767498470D+06,
     &   3.338152967987029735917223D+06,
     &   5.106661678927352456275255D+06,
     &   3.074109054850539556250927D+06/
      data q2/
     &   1.830328399370592604055942D+02,
     &   7.765049321445005871323047D+03,
     &   1.331903827966074194402448D+05,
     &   1.136705821321969608938755D+06,
     &   5.267964117437946917577538D+06,
     &   1.346701454311101692290052D+07,
     &   1.782736530353274213975932D+07,
     &   9.533095591844353613395747D+06/
      data d4/1.791759469228055000094023D+00/
      data p4/
     &   1.474502166059939948905062D+04,
     &   2.426813369486704502836312D+06,
     &   1.214755574045093227939592D+08,
     &   2.663432449630976949898078D+09,
     &   2.940378956634553899906876D+10,
     &   1.702665737765398868392998D+11,
     &   4.926125793377430887588120D+11,
     &   5.606251856223951465078242D+11/
      data q4/
     &   2.690530175870899333379843D+03,
     &   6.393885654300092398984238D+05,
     &   4.135599930241388052042842D+07,
     &   1.120872109616147941376570D+09,
     &   1.488613728678813811542398D+10,
     &   1.016803586272438228077304D+11,
     &   3.417476345507377132798597D+11,
     &   4.463158187419713286462081D+11/
      data c/
     &  -1.910444077728D-03,
     &   8.4171387781295D-04,
     &  -5.952379913043012D-04,
     &   7.93650793500350248D-04,
     &  -2.777777777777681622553D-03,
     &   8.333333333333333331554247D-02,
     &   5.7083835261D-03/

      if (x.lt.1.d-15) then
         dlgama=0.d0
         return
      endif

      y = x

      if ( zero .lt. y .and. y .le. xbig ) then

        if ( y .le. eps ) then

          res = - dlog ( y )
        else if ( y .le. thrhal ) then

          if ( y .lt. pnt68 ) then
            corr = - dlog ( y )
            xm1 = y
          else
            corr = zero
            xm1 = ( y - half ) - half
          end if

          if ( y .le. half .or. pnt68 .le. y ) then

            xden = one
            xnum = zero
            do i = 1, 8
              xnum = xnum * xm1 + p1(i)
              xden = xden * xm1 + q1(i)
            end do

            res = corr + ( xm1 * ( d1 + xm1 * ( xnum / xden ) ) )

          else

            xm2 = ( y - half ) - half
            xden = one
            xnum = zero
            do i = 1, 8
              xnum = xnum * xm2 + p2(i)
              xden = xden * xm2 + q2(i)
            end do

            res = corr + xm2 * ( d2 + xm2 * ( xnum / xden ) )

          end if
        else if ( y .le. four ) then

          xm2 = y - two
          xden = one
          xnum = zero
          do i = 1, 8
            xnum = xnum * xm2 + p2(i)
            xden = xden * xm2 + q2(i)
          end do

          res = xm2 * ( d2 + xm2 * ( xnum / xden ) )
        else if ( y .le. twelve ) then

          xm4 = y - four
          xden = -one
          xnum = zero
          do i = 1, 8
            xnum = xnum * xm4 + p4(i)
            xden = xden * xm4 + q4(i)
          end do

          res = d4 + xm4 * ( xnum / xden )
        else

          res = zero

          if ( y .le. frtbig ) then

            res = c(7)
            ysq = y * y

            do i = 1, 6
              res = res / ysq + c(i)
            end do

          end if

          res = res / y
          corr = dlog ( y )
          res = res + sqrtpi - half * corr
          res = res + y * ( corr - one )

        end if
      else

        res = xinf

      end if
      dlgama = res

      return
      end function dlgama

!> Compute the beta function.
      function beta ( a, b )

      implicit none

      double precision a
      double precision b
      double precision beta
C      double precision dlgama

      if ( a <= 0.0D+00 .or. b <= 0.0D+00 ) then
        write ( *, '(a)' ) ' '
        write ( *, '(a)' ) 'BETA - Fatal error!'
        write ( 6, * ) 'A,B',A,B
        write ( *, '(a)' ) '  Both A and B must be greater than 0.'
        stop
      end if

      beta = exp ( dlgama ( a ) 
     &           + dlgama ( b ) 
     &           - dlgama ( a + b ) )

      return

      end function beta

!> Calculate the norm of a real vector vec of dimension dim.
      subroutine normvxd(vec,norm,dim)
     
      implicit none

      integer*8 i,dim
      real*8  vec(dim),norm
     
      norm=vec(1)*vec(1)
      do i=2,dim
         norm=norm+vec(i)*vec(i)
      enddo
      norm=sqrt(norm)

      return
      end subroutine normvxd

!> Calculate the norm of a complex vector vec of dimension dim.
      subroutine normvxz(vec,norm,dim)
      
      implicit none

      integer*8     i,dim
      real*8      norm
      complex*16  vec(dim)
      
      norm=0.
      do i=1,dim
         norm=norm+dconjg(vec(i))*vec(i)
      enddo
      norm=sqrt(norm)

      return
      end subroutine normvxz


!> Set matrix mat of dimension dim1 x dim2 to zero.
      subroutine zeromxd(mat,dim1,dim2)

      implicit none

      integer*8    dim1,dim2,i,j
      real*8     mat(dim1,dim2)

      do j=1,dim2
         do i=1,dim1
            mat(i,j)=0.0d0
         enddo
      enddo

      return
      end subroutine zeromxd


!> Set real vector vec of dimension dim to 0.d0.
      subroutine zerovxd(vec,dim)

      implicit none

      integer*8    dim,i
      real*8     vec(dim)

      do i=1,dim
         vec(i)=0.0d0
      enddo

      return
      end subroutine zerovxd

!> Transpose a real square dim x dim matrix a. 
      subroutine tranqxd(a,dim)

      implicit none

      integer*8 dim,i,j
      real*8 a(dim,dim),b

      do i=2,dim
         do j=1,i-1
            b=a(i,j)
            a(i,j)=a(j,i)
            a(j,i)=b
         enddo
      enddo

      return
      end subroutine tranqxd

!> Copy the real vector v to the real part of the complex vector w.
      subroutine cpvxdz (v,w,dim)

      integer*8 dim,i
      real*8  v(dim)
      complex*16  w(dim)

      do i = 1,dim
         w(i) = v(i)
      enddo

      return
      end subroutine cpvxdz


!> Copy the real vector v of dimension dim to the real vector w.
      subroutine cpvxd (v,w,dim)

      integer*8 dim,i
      real*8  v(dim),w(dim)

      do i = 1,dim
         w(i) = v(i)
      enddo

      return
      end subroutine cpvxd

!> Schmidt Orthonormalization of a set of dim complex vectors of dimension gdim stored in psi.       
      subroutine schmidtortho_nlevel(psi,gdim,dim,nlevel,ierr)

      implicit none

      integer*8    gdim,dim,e,e1,e2,ierr,nlevel
      complex*16 psi(gdim,dim,nlevel),overlap
      real*8     norm(nlevel,dim),totalnorm

     
      Complex*16 ZDOTC
      REAL*8 DZNRM2

      EXTERNAL DZNRM2,ZDOTC

      ierr = 0
      do e=1,dim
         do e1=1,e-1

           Overlap=dcmplx(0.d0,0.d0)
           
!> Calculate orbital overlap summed over all nlevels
           do e2=1,nlevel
             Overlap = Overlap+ZDOTC(gdim,PSI(:,e1,e2),1,PSI(:,e,e2),1)
           enddo

!> create new vector psi(:,e,e2), which is orthogonal to all other earlier
!> vectors psi(:,e1,e2)
           do e2=1,nlevel
             CALL ZAXPY(gdim,-1.d0*overlap,psi(:,e1,e2),1,
     .                   psi(:,e,e2),1)
           enddo
            
         enddo
!> end of orthogonalizing

!> calculate norm for every single level
         do e2=1,nlevel
            norm(e2,e) = DZNRM2(gdim,Psi(:,e,e2),1)
            if (norm(e2,e) .le. 1.0d-99 ) norm(e2,e) = 1.0d99
         enddo

!> scale every single orbital to be normalized over the levels
         do e2=1,nlevel
            CALL ZDSCAL(gdim,1.d0/sqrt(sum(norm(:,e)**2)),Psi(:,e,e2),1)
         end do

      enddo

      do e=1,dim
         do e1=1,e-1
            Overlap=dcmplx(0.d0,0.d0)

           do e2=1,nlevel
             Overlap = Overlap+ZDOTC(gdim,PSI(:,e1,e2),1,PSI(:,e,e2),1)
           enddo
           do e2=1,nlevel
             CALL ZAXPY(gdim,-1.d0*overlap,psi(:,e1,e2),1,
     .                  psi(:,e,e2),1)
           enddo
           
         enddo

         do e2=1,nlevel
            norm(e2,e) = DZNRM2(gdim,Psi(:,e,e2),1)
         end do

         if (sqrt(sum(norm(:,e)**2)).le. 0.8d0 ) then
            ierr = e 
            return
         end if

         do e2=1,nlevel
            CALL ZDSCAL(gdim,1.d0/sqrt(sum(norm(:,e)**2)),Psi(:,e,e2),1)
         enddo

      enddo

      return
      end subroutine schmidtortho_nlevel

! MPI MULTI_LEVEL NEEDED
! This subroutine is not yet coded properly
      subroutine schmidtortho_nlevel_MPI(psi,gdim,dim,nlevel,ierr)
      use MPI

      implicit none

      integer*8    gdim,dim,e,e1,e2,ierr,nlevel
      complex*16 psi(gdim,dim,nlevel),overlap,localoverlap
      real*8     norm(nlevel,dim),totalnorm
      integer myid, i_error
     
      Complex*16 ZDOTC
      REAL*8 DZNRM2

      EXTERNAL DZNRM2,ZDOTC

      CALL MPI_COMM_RANK(MPI_COMM_WORLD,MYID,i_error)
      ierr = 0
      do e=1,dim
         do e1=1,e-1

           LocalOverlap=dcmplx(0.d0,0.d0)
           
!> Calculate orbital overlap summed over all nlevels
           do e2=1,nlevel
             LocalOverlap = 
     .            LocalOverlap+ZDOTC(gdim,PSI(:,e1,e2),1,PSI(:,e,e2),1)
           enddo

!> create new vector psi(:,e,e2), which is orthogonal to all other earlier
!> vectors psi(:,e1,e2)
            CALL MPI_ALLREDUCE(LocalOverlap,Overlap,1,
     .                MPI_DOUBLE_COMPLEX,MPI_SUM,
     .                MPI_COMM_WORLD,i_error)
           do e2=1,nlevel
             CALL ZAXPY(gdim,-1.d0*overlap,psi(:,e1,e2),1,
     .                   psi(:,e,e2),1)
           enddo
            
         enddo
!> end of orthogonalizing

!> calculate norm for every single level
         do e2=1,nlevel
            norm(e2,e) = DZNRM2(gdim,Psi(:,e,e2),1)
            if (norm(e2,e) .le. 1.0d-99 ) norm(e2,e) = 1.0d99
         enddo
         totalnorm = sum(norm(:,e)**2)
         CALL MPI_ALLREDUCE(MPI_IN_PLACE,totalnorm,1,
     .                MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,
     .                i_error)
!> scale every single orbital to be normalized over the levels
         do e2=1,nlevel
            CALL ZDSCAL(gdim,1.d0/sqrt(totalnorm),Psi(:,e,e2),1)
         end do

      enddo

      do e=1,dim
         do e1=1,e-1
            LocalOverlap=dcmplx(0.d0,0.d0)

           do e2=1,nlevel
             LocalOverlap = 
     .            LocalOverlap+ZDOTC(gdim,PSI(:,e1,e2),1,PSI(:,e,e2),1)
           enddo
            CALL MPI_ALLREDUCE(LocalOverlap,Overlap,1,
     .                MPI_DOUBLE_COMPLEX,MPI_SUM,
     .                MPI_COMM_WORLD,i_error)
           do e2=1,nlevel
             CALL ZAXPY(gdim,-1.d0*overlap,psi(:,e1,e2),1,
     .                  psi(:,e,e2),1)
           enddo
           
         enddo

         do e2=1,nlevel
            norm(e2,e) = DZNRM2(gdim,Psi(:,e,e2),1)
         end do
         totalnorm = sum(norm(:,e)**2)

         CALL MPI_ALLREDUCE(MPI_IN_PLACE,totalnorm,1,
     .                MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,
     .                i_error)

         if (sqrt(totalnorm).le. 0.8d0 ) then
            ierr = e 
            return
         end if

         do e2=1,nlevel
            CALL ZDSCAL(gdim,1.d0/sqrt(totalnorm),Psi(:,e,e2),1)
         enddo

      enddo

      return
      end subroutine schmidtortho_nlevel_MPI








!> Schmidt Orthonormalization of a set of dim complex vectors of dimension gdim stored in psi.       
      subroutine schmidtortho_nlevel2(psi,gdim,dim,nlevel,ierr)

      implicit none

      integer*8    gdim,dim,e,e1,e2,ierr,nlevel
      complex*16 psi(gdim,dim,nlevel),overlap
      real*8     norm(nlevel,dim),totalnorm

     
      Complex*16 ZDOTC
      REAL*8 DZNRM2

      EXTERNAL DZNRM2,ZDOTC

      do e=1,dim

         do e2=1,nlevel
           norm(e2,e) = DZNRM2(gdim,Psi(:,e,e2),1)
           if (norm(e2,e) .le. 1.0d-99 ) norm(e2,e) = 1.0d99
         end do

      end do

!> Schmidt orthogonalize or orthonormalize within each level
      ierr = 0
      do e2=1,nlevel
         call schmidtortho(psi(:,:,e2),gdim,dim,ierr)
      enddo

      IF (Ierr.ne.0) return

      do e=1,dim

         do e2=1,nlevel
            CALL ZDSCAL(gdim,norm(e2,e)/sum(norm(:,e)),
     &                  Psi(:,e,e2),1)
         end do

      enddo

      do e2=1,nlevel
         call schmidtortho(psi(:,:,e2),gdim,dim,ierr)
      enddo

      IF (Ierr.ne.0) return

      do e=1,dim

         totalnorm=sum(norm(:,e)) ! compute norm of current orbital

         if( totalnorm .le. 0.8d0 ) then
            ierr = e 
            return
         end if

         totalnorm=0.d0

         do e2=1,nlevel
            CALL ZDSCAL(gdim,norm(e2,e)/totalnorm,
     &                  Psi(:,e,e2),1)
         end do

      end do

      return
      end subroutine schmidtortho_nlevel2 


!> Schmidt Orthonormalization of a set of dim complex vectors of dimension gdim stored in psi.       
      subroutine schmidtortho(psi,gdim,dim,ierr)

      implicit none

      integer*8    gdim,dim,e,e1,ierr
      complex*16 psi(gdim,dim),overlap
      real*8     norm

      Complex*16 ZDOTC
      REAL*8 DZNRM2

      EXTERNAL DZNRM2,ZDOTC

      ierr = 0
      do e=1,dim
         do e1=1,e-1
            Overlap = ZDOTC(gdim,PSI(:,e1),1,PSI(:,e),1)
            CALL ZAXPY(gdim,-1.d0*overlap,psi(:,e1),1,psi(:,e),1)
         enddo
         norm = DZNRM2(gdim,Psi(:,e),1)
         if (norm .le. 1.0d-99 ) norm = 1.0d99
         CALL ZDSCAL(gdim,1.d0/norm,Psi(:,e),1)
      enddo

      do e=1,dim
         do e1=1,e-1
            Overlap = ZDOTC(gdim,PSI(:,e1),1,PSI(:,e),1)
            CALL ZAXPY(gdim,-1.d0*overlap,psi(:,e1),1,psi(:,e),1)
         enddo
         norm = DZNRM2(gdim,Psi(:,e),1)
         if( norm .le. 0.8d0 ) then
            ierr = e 
            return
         end if
         CALL ZDSCAL(gdim,1.d0/norm,Psi(:,e),1)
      enddo

      return
      end subroutine schmidtortho 

!> Schmidt Orthonormalization of a set of domain decomposed complex vectors of dimension gdim stored in psi.       
      subroutine schmidtortho_MPI(psi,gdim,dim,ierr)

      USE MPI

      implicit none

      integer*8    gdim,dim,e,e1,ierr
  
      integer i_error
      integer myid

      complex*16 psi(gdim,dim),overlap,LocalOverlap
      real*8     norm

      Complex*16 ZDOTC
      REAL*8 DZNRM2

      EXTERNAL DZNRM2,ZDOTC


      CALL MPI_COMM_RANK(MPI_COMM_WORLD,MYID,i_error)
      ierr = 0
      do e=1,dim
         do e1=1,e-1
            LocalOverlap = ZDOTC(gdim,PSI(:,e1),1,PSI(:,e),1)

            CALL MPI_ALLREDUCE(LocalOverlap,Overlap,1,
     .                MPI_DOUBLE_COMPLEX,MPI_SUM,
     .                MPI_COMM_WORLD,i_error)

            CALL ZAXPY(gdim,-1.d0*Overlap,psi(:,e1),1,psi(:,e),1)
         enddo

         norm =SUM(dconjg(Psi(:,e)*Psi(:,e)))
         CALL MPI_ALLREDUCE(MPI_IN_PLACE,norm,1,
     .                MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,
     .                i_error)

         norm=dsqrt(norm)

         if (norm .le. 1.0d-99 ) then
             norm = 1.0d99
         endif

         CALL ZDSCAL(gdim,1.d0/norm,Psi(:,e),1)

      enddo

      do e=1,dim
         do e1=1,e-1
            LocalOverlap = ZDOTC(gdim,PSI(:,e1),1,PSI(:,e),1)

            CALL MPI_ALLREDUCE(LocalOverlap,Overlap,1,
     .                MPI_DOUBLE_COMPLEX,MPI_SUM,MPI_COMM_WORLD,
     .                i_error)

            CALL ZAXPY(gdim,-1.d0*overlap,psi(:,e1),1,psi(:,e),1)

         enddo

!         norm = DZNRM2(gdim,Psi(:,e),1)
         norm =SUM(dconjg(Psi(:,e)*Psi(:,e)))

         CALL MPI_ALLREDUCE(MPI_IN_PLACE,norm,1,
     .                MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,
     .                i_error)

         norm=dsqrt(norm)

         if( norm .le. 0.8d0 ) then
            ierr = e 
            return
         end if
         CALL ZDSCAL(gdim,1.d0/norm,Psi(:,e),1)
      enddo

      return
      end subroutine schmidtortho_MPI 



!> Schmidt Orthonormalization of a set of dim real vectors of dimension gdim stored in psi.       
      subroutine schmidtorthod (psi,dim)

      implicit none

      integer*8    dim,e,e1,i
      real*8 psi(dim,dim),overlap,norm
      call tranqxd(psi,dim)

      do i=1,2
         do e=1,dim
            do e1=1,e-1
               call vvtxdd(psi(1,e1),psi(1,e),overlap,dim)
               call xvxxdds(overlap,psi(1,e1),psi(1,e),dim)
            enddo
            call normvxd(psi(1,e),norm,dim)
            call xvixddo(norm,psi(1,e),dim)
         enddo
      enddo

      call tranqxd(psi,dim)

      return
      end subroutine schmidtorthod 


!> Devides a real vector v of length dim by a real number x.
!> Called by schmidtortho.
      subroutine xvixddo (x,v,dim)
      implicit none

      integer*8 dim,i
      real*8  v(dim),x,inv

      inv=1.0d0/x
      do i = 1,dim
         v(i) = inv*v(i)
      enddo

      return
      end subroutine xvixddo

!> Devides a complex vector v of length dim by a real number x.
      subroutine xvixdzo (x,v,dim)

      implicit none

      integer*8 dim,i
      real*8  x,inv
      complex*16 v(dim)

      inv=1.0d0/x
      do i = 1,dim
         v(i) = inv*v(i)
      enddo

      return
      end subroutine xvixdzo


!> Subtract from a real vector w of dimension dim x (real) times the
!> real vector v of dimension dim.
      subroutine xvxxdds (x,v,w,dim)


      implicit none

      integer*8 dim,i
      real*8  v(dim),w(dim),x

      do i = 1,dim
         w(i) = w(i)-x*v(i)
      enddo

      return
      end subroutine xvxxdds 

!> Subtract from a complex vector w of dimension dim x (complex) times the
!> complex vector v of dimension dim.
      subroutine xvxxzzs (x,v,w,dim)


      implicit none

      integer*8 dim,i
      complex*16 v(dim),w(dim),x

      do i = 1,dim
         w(i) = w(i)-x*v(i)
      enddo

      return
      end subroutine xvxxzzs


!> Calculate the scalar product s of the two real vectors u,v of dimension
!> dim.
      subroutine vvtxdd (u,v,s,dim)

      implicit none

      integer*8 dim,i
      real*8  u(dim),v(dim),s

      s = u(1)*v(1)
      do i = 2,dim
         s = s+u(i)*v(i)
      enddo

      return
      end subroutine vvtxdd 

!> Calculate the scalar product s of the two complex vectors u,v of
!> dimension dim.
      subroutine vvaxzz (u,v,s,dim)
      implicit none

      integer*8     dim,i
      complex*16  u(dim),v(dim),s

      s = dconjg(u(1))*v(1)
      do i = 2,dim
         s = s+dconjg(u(i))*v(i)
      enddo

      return
      end subroutine vvaxzz
      END MODULE Function_Library
