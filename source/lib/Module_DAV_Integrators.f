!>GPLv3
!    MCTDH-X: the multiconfigurational time-dependent Hartree for 
!    indistinguishable particles software
!
!    Copyright (C) 2021  A. U. J. Lode, M. C. Tsatsos, 
!                        E. Fasshauer, S. E. Weiner, 
!                        R. Lin, L. Papariello, P. Molignini, 
!                        C. Lévêque, M. Büttner , J. Xiang, S. Dutta
!
!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <https://www.gnu.org/licenses/>.
!
!
!
!
!<GPLv3










!> Module for the davidson integrators.
        MODULE DAV_Integrators
        USE HamiltonianAction_Coefficients
        USE Function_Library
        CONTAINS
!>                 SUBROUTINE DAVSTEP                                 *
!>                                                                    *
!> Diagonalises the Hamiltonian represented in the SPFs.              *
!> Improved relaxation for Hermitian (complex) Hamiltonians.          *
!>                                                                    *
!>                                                                    *
!> Input parameters:                                                  *
!>   Psi:       The (complex) initial-value vector.                   *
!>   DtPsi:     Action of Hamiltonian on the initial-value vector,    *
!>              i.e. H|Psi> .                                         *
!>   PsiDim     Length of Psi and DtPsi vectors (i.e. adim).          *
!>   IntOrder:  Maximum interation order.                             *
!>   TolError:  Maximum error that is tolerated.                      *
!>   Steps:     Number of steps made so far (Steps is passed to       *
!>              "WriteStepDav").                                      *
!>   Krylov:    Matrix with minimum size PsiDim*IntOrder with         *
!>              columns containing the Davison vectors.               *
!>   HKrylov:   Matrix containing H*Krylov                            *
!>   rlxemin:   Lower energy to restrict the search for max. ovlp     *
!>   rlxemax:   Upper energy to restrict the search for max. ovlp     *
!>                                                                    *
!> Output parameters:                                                 *
!>   Psi:       Propagated Psi.                                       *
!>   Krylov:    Matrix with minimum size PsiDim*(IntOrder-1) with     *
!>              columns containing the Krylov vectors H^2|psi> to     *
!>              H^TrueOrder|psi>.                                     *
!>   TrueOrder: The order that has actually been used (may be less    *
!>              than IntOrder).                                       *
!>   Steps:     Same as on entry.                                     *
!>   ErrorCode: Error code having the following meaning:              *
!>              0: everything was o. k.,                              *
!>              1: illegal integration order,                         *
!>              3: diagonalization failed,                            *
!>                                                                    *
!> External routines:                                                 *
!>   Func:      Computes the action of the Hamiltonian on Psi.        *
!>              Called as                                             *
!>              Func(Abs(Time),Psi,DtPsi).    *
!>   WriteStep: Writes the stepsize and error to a file.              *
!>              Called as WriteStepDav(Steps,Order,E,beta,Error,Q,t). *
!> wrtrlxdav: Writes info to rlx_info file.                           *
!>                                                                    *
!> Some new Variables:                                                *
!>  rlaxnum : This is the argument of the keyword relaxation.         *
!>            If relaxation has no argument, rlaxnum=-1 .             *
!>            rlaxnum=0   : relaxation to ground-state.               *
!>            rlaxnum=n   : relaxation to n-th state (n=0...900).     *
!>            rlaxnum=920 : relaxation=follow.                        *
!>            rlaxnum=930 : relaxation=lock.                          *
!>                                                                    *
!> V8.3 06/2003 HDM                                                   *
 
      Subroutine DavStep(Psi,DtPsi,PsiDim,IntOrder,TolError,
     +                 rlaxnum,
     +                 ErrorCode,Time,rlxemin,rlxemax,
     +                 Func)
      
      Implicit None

      Integer    PsiDim,rlaxnum,
     +           TrueOrder,ErrorCode
      Integer*8  IntOrder
      Real*8     TolError,Time,rlxemin,rlxemax,
     +           EigenVal(0:IntOrder),ovlps(0:IntOrder)
      Complex*16 Psi(PsiDim),DtPsi(PsiDim),Krylov(PsiDim,IntOrder),
     +           HKrylov(PsiDim,IntOrder),HDiagonal(2*PsiDim),
     +           EigenVector(0:IntOrder,0:IntOrder),
     +           Tmat(0:IntOrder,0:IntOrder)
      Logical    FirstCall,jump,full
      External   Func,dznrm2
      REAL*8     dznrm2

      Integer    D,P,Q,Q1,i,j,shift,count
      Real*8     Error,olderr,ovl,ovl1,ovj,d2,E,E1,beta,beta1,
     +           workr(3*IntOrder+4)
      Complex*16 ovlp,workc(16*IntOrder+18),prefac

C---- WARNING: if(.not.lock) HDiagonal(PsiDim)

      data       FirstCall/.true./
      Save       FirstCall, full

C --- CHECK INTEGRATION ORDER ---

      If (IntOrder .LT. 2) Then
         ErrorCode = 1
         write(6,'(a,i4)') 'DavStep: illegal IntOrder =',IntOrder
         Return
      EndIf

C --- INITIALIZE VARIABLES ---

      if(FirstCall) Then
         if(rlaxnum .ge. 19999) then
            write(6,'(a)') 'ortho not allowed for DAV'
            stop
         endif
         if(rlaxnum .ge. 9999) then
            full = .true.
            rlaxnum = rlaxnum - 10000
         else
            full = .false.
         endif
         if(rlaxnum.lt.0 .or.(rlaxnum.gt.920 .and. rlaxnum.ne.930)) then
            write(6,'(a,i5,l3)') ' DAV : Incorrect rlaxnum',rlaxnum,full
            stop
         endif
      endif
      shift = 0
      count = 1
      d2    = 1.d-12
      jump  = .true. 
      ErrorCode = 0
      TrueOrder = 1
      Error = 1.d0
      beta1 = 0
      Q  = 0
      Q1 = 0

C.....davovlp1 sets the second part of Hdiagonal to the transformed 
C.....A-vector of psi_ref.  (DavStep)
C      if(rlaxnum.eq.930) then
C         call davovlp1(psi,HDiagonal,CData,time)
C      endif

C.....Compute the diagonal of the Hamilton matrix.
      call Get_HamiltonianDiagonal(Psi,HDiagonal)
   

C.....Compute the first trial energy.
      E=0.d0
      Do D = 1, PsiDim
         E = E + DConjg(Psi(D))*DtPsi(D)
      enddo

C.....Write very first energy expectation value to rlx_info file.
      if(FirstCall) then
         Q=0
         Q1=0
         E1=E
         ovl =1.d0
         ovl1=1.d0
         beta=1.d0
C         call wrtrlxdav(time,Q1,Q,Q1,E,E1,beta,beta,ovl,ovl1,
C     +                  EigenVal,ovlps,FirstCall,IntOrder)
      endif

C --- APPLY INVERSE OF DIAGONAL and ORTHONORMALIZE ---

      olderr=Error
      Error = 0.d0
      Do D = 1, PsiDim
         Error = Error + DConjg(DtPsi(D)-E*Psi(d))*(DtPsi(D)-E*Psi(D))
         Krylov(D,1) = (DtPsi(D)-E*Psi(D)) * 
     $    dble(HDiagonal(D)-E)/(dble(HDiagonal(D)-E)**2 + d2)
      enddo
      Error = Sqrt(Error) 
      ovlp =(0.d0,0.d0)
      Do D = 1,PsiDim
         ovlp = ovlp + DConjg(Psi(D))*Krylov(D,1)
      enddo
      Do D = 1,PsiDim
         Krylov(D,1)=Krylov(D,1)-ovlp*Psi(D)
      enddo
      ovl = 0.d0
      Do D = 1,PsiDim
         ovl = ovl + DConjg(Krylov(D,1))*Krylov(D,1)
      enddo
      Do D = 1,PsiDim
         Krylov(D,1)=Krylov(D,1)/sqrt(ovl)
      enddo
C.....Do the orthonormalisation twice.
      ovlp =(0.d0,0.d0)
      Do D = 1,PsiDim
         ovlp = ovlp + DConjg(Psi(D))*Krylov(D,1)
      enddo
      Do D = 1,PsiDim
         Krylov(D,1)=Krylov(D,1)-ovlp*Psi(D)
      enddo
      ovl = 0.d0
      Do D = 1,PsiDim
         ovl = ovl + DConjg(Krylov(D,1))*Krylov(D,1)
      enddo
      Do D = 1,PsiDim
         Krylov(D,1)=Krylov(D,1)/sqrt(ovl)
      enddo
C
C --- WRITE STEPSIZE AND ERROR IF DESIRED ---
      beta=1.d0
C     Call WriteStepDav(Steps,TrueOrder,E,beta,Error,Q,Time)

C --- BUILD HKRYLOV FOR FIRST STEP ---
      Call Func(Time,Krylov(1,1),HKrylov(1,1))


C --- BUILD T-MATRIX   (DavStep) ---
      
      Tmat(0,0) = E
      Tmat(1,1) = (0.d0,0.d0)
      Tmat(0,1) = (0.d0,0.d0)
      Do D = 1, PsiDim
         Tmat(0,1) = Tmat(0,1) +
     $        DConjg(Psi(D))*Hkrylov(D,1)     
         Tmat(1,1) = Tmat(1,1) +
     $        DConjg(Krylov(D,1))*HKrylov(D,1)     
      enddo

C --- DIAGONALISE T-MATRIX ---
      
      do j = 0, TrueOrder
         do i = 0, j
            EigenVector(i,j) = Tmat(i,j)
         enddo 
      enddo

      call ZHEEV( 'V', 'U', 2, EigenVector, INT(IntOrder+1,4),
     $           EigenVal, workc, INT(16*IntOrder+18,4), 
     $           workr, ErrorCode)
      If (ErrorCode .NE. 0) Then
         ErrorCode = 3
         Return
      EndIf

      If(abs(EigenVector(0,0)) .gt. abs(EigenVector(0,1)) ) Then
         Q = 0
      else
         Q = 1 
      endif
      if(rlaxnum.eq.0)  Q = 0 
      if(FirstCall .and. rlaxnum.lt.TrueOrder .and. 
     +                   rlaxnum.le.900)  Q = rlaxnum 

      E = EigenVal(Q)
      beta = abs(EigenVector(0,Q))**2

C      if(rlaxnum.eq.930 .and. .not.FirstCall) then
C........The call to davovlp2c sets "set" in davovlp2c.  
C         call davovlp2c(IntOrder,TrueOrder,ovlps,Psi,Krylov,
C     $                  EigenVector,Hdiagonal(PsiDim+1),FirstCall)
C........Add transformed initial state as extra vector.
C         TrueOrder = TrueOrder + 1
C         Do D = 1,PsiDim
C            Krylov(D,TrueOrder) = Hdiagonal(PsiDim+D)
C         enddo
C         Error = 1.d0
C         goto 200
C      endif



C --- LOOP : BUILD UP THE KRYLOV AND HKRYLOV SPACES ---

 100  Continue  !  Iteration Loop (DavStep)
         
C --- RE-INITIALIZE VARIABLES ---

      TrueOrder = TrueOrder+1
C.....Converge to lower states if sought state already converged.
      if(full .and. (TrueOrder .lt. IntOrder-2)) then
         if(Error.lt.TolError*2.d0) then
            shift = shift + count
            if(shift .gt. Q) then
               count = -count
               shift = count
            endif
         endif
         Q1 = Q 
         Q  = Q - shift
         E  = EigenVal(Q)
         if(Error.lt.TolError*2.d0)  then 
            E1    = EigenVal(Q1)
            beta  = Abs(EigenVector(0,Q))**2
            beta1 = Abs(EigenVector(0,Q1))**2
C            call wrtrlxdav(time,trueorder,Q,Q1,E,E1,beta,beta,ovl,ovl1,
C     +                     EigenVal,ovlps,FirstCall,IntOrder)
         endif
         Q1 = Q 
      endif

C --- Build Eigenvector ---

 120  Do D = 1, PsiDim
         Krylov(D,TrueOrder) = EigenVector(0,Q)*(DtPsi(D)-E*Psi(D))
      enddo
      Do j = 1, TrueOrder-1
         Do D = 1, PsiDim
            Krylov(D,TrueOrder) = Krylov(D,TrueOrder) + 
     $           EigenVector(j,Q) * (HKrylov(D,j) - E*Krylov(D,j))
         enddo
      enddo

C --- DETERMINE ERROR and APPLY INVERSE OF DIAGONAL ---

      olderr=Error
      Error = 0.d0
      Do D = 1, PsiDim
         Error = Error + DConjg(Krylov(D,TrueOrder))*Krylov(D,TrueOrder)
         Krylov(D,TrueOrder) = Krylov(D,TrueOrder) *
     $   dble(HDiagonal(D)-E)/(dble(HDiagonal(D)-E)**2 + d2)
      enddo
      Error = Sqrt(Error)

C ---  ORTHONORMALIZE (shortcut for loop) ---
 
 200  ovlp =(0.d0,0.d0)
      Do D = 1,PsiDim
         ovlp = ovlp + DConjg(Psi(D))*Krylov(D,TrueOrder)
      enddo
      Do D = 1,PsiDim
         Krylov(D,TrueOrder)=Krylov(D,TrueOrder)-ovlp*Psi(D)
      enddo

      Do i = 1, TrueOrder-1
         ovlp =(0.d0,0.d0)
         Do D = 1,PsiDim
            ovlp = ovlp + DConjg(Krylov(D,i))*Krylov(D,TrueOrder)
         enddo
         Do D = 1,PsiDim
            Krylov(D,TrueOrder) = Krylov(D,TrueOrder) - 
     +           ovlp*Krylov(D,i)
         enddo
      enddo
      ovl = 0.d0
      Do D = 1,PsiDim
         ovl = ovl+DConjg(Krylov(D,TrueOrder))*
     +        Krylov(D,TrueOrder)
      enddo
      if(ovl.le.1.d-39) then     ! If vectors are linearly dependent
!         write(6,'(a,3i5,2e12.4)') 'DAV: Steps, Order, Q, ovl, Error',
!     +                              Steps,TrueOrder,Q,ovl,Error
!         write(2,'(a,3i5,2e12.4)') 'DAV: Steps, Order, Q, ovl, Error',
!     +                              Steps,TrueOrder,Q,ovl,Error
         TrueOrder = TrueOrder - 1
!         write(6,'(2a)') 'Linearly dependent Davidson vectors. ',
!     +                   'Convergence assumed'
         goto 310  ! Converged 
      endif
      Do D = 1,PsiDim
         Krylov(D,TrueOrder)=Krylov(D,TrueOrder)/sqrt(ovl)
      enddo

C.....Orthonormalize twice. (DavStep)
      ovlp =(0.d0,0.d0)
      Do D = 1,PsiDim
         ovlp = ovlp + DConjg(Psi(D))*Krylov(D,TrueOrder)
      enddo
      Do D = 1,PsiDim
         Krylov(D,TrueOrder)=Krylov(D,TrueOrder)-ovlp*Psi(D)
      enddo

      Do i = 1, TrueOrder-1
         ovlp =(0.d0,0.d0)
         Do D = 1,PsiDim
            ovlp = ovlp + DConjg(Krylov(D,i))*Krylov(D,TrueOrder)
         enddo
         Do D = 1,PsiDim
            Krylov(D,TrueOrder) = Krylov(D,TrueOrder) -
     +           ovlp*Krylov(D,i)
         enddo
      enddo
      ovl = 0.d0
      Do D = 1,PsiDim
         ovl = ovl+DConjg(Krylov(D,TrueOrder))*
     +        Krylov(D,TrueOrder)
      enddo
      Do D = 1,PsiDim
         Krylov(D,TrueOrder)=Krylov(D,TrueOrder)/sqrt(ovl)
      enddo


C --- WRITE STEPSIZE AND ERROR IF DESIRED ---
C      Call WriteStepDav(Steps,TrueOrder,E,beta,Error,Q,Time)

C --- BUILD HKRYLOV  ---
      Call Func(Time,Krylov(1,TrueOrder),HKrylov(1,TrueOrder))

C --- BUILD NEW T-MATRIX ELEMENTS ---
      
      Tmat(0,TrueOrder) = (0.d0,0.d0)
       Do D = 1, PsiDim
         Tmat(0,TrueOrder) = Tmat(0,TrueOrder) +
     $        DConjg(Psi(D))*Hkrylov(D,TrueOrder)     
      enddo

      Do j = 1, TrueOrder
         Tmat(j,TrueOrder) = (0.d0,0.d0)
         Do D = 1, PsiDim
            Tmat(j,TrueOrder) = Tmat(j,TrueOrder) +
     $      DConjg(Krylov(D,j))*HKrylov(D,TrueOrder)     
         enddo
      enddo

        
C --- DIAGONALISE T-MATRIX ---
      
      do j = 0, TrueOrder
         do i = 0, TrueOrder
            EigenVector(i,j) = Tmat(i,j)
         enddo 
      enddo

      call ZHEEV( 'V', 'U', TrueOrder+1, EigenVector, INT(IntOrder+1,4),
     $             EigenVal, workc, INT(16*IntOrder+18,4), 
     $             workr, ErrorCode)
      If (ErrorCode .NE. 0) Then
         ErrorCode = 3
         Return
      EndIf



C --- FIND THE EIGENVECTOR WITH THE LARGEST OVERLAP (if not GS sought) -

      if(rlaxnum.eq.0) then
         Q = 0 
         beta= abs(EigenVector(0,0))**2
      elseif(FirstCall .and. rlaxnum.lt.TrueOrder .and. 
     +                       rlaxnum.le.900)  then
         Q = rlaxnum 
         beta= abs(EigenVector(0,Q))**2
      else
         beta=0.d0
         Do j = 0, TrueOrder
            beta1= abs(EigenVector(0,j))**2
            If(beta1 .gt. beta) then
               Q = j
               beta = beta1
            endif
         enddo
      endif
      E = EigenVal(Q)
      Q1 = Q
      E1 = E
      beta1 = beta
      ovl=0.d0


C --- CONTINUE IF NOT CONVERGED --- END OF LOOP ---
      If((TrueOrder.LT.IntOrder) .And.  (full .or.
     +  ( (Error.GT.TolError*10.d0) .And. jump)) )  Goto 100


C --- COMPUTE EXACT OVERLAPS  (DavStep)
 310  continue
C   if(rlaxnum.eq.930) then   ! lock
C........The array ovlps contains the overlaps**2 with startvector.
C         call davovlp2c(IntOrder,TrueOrder,ovlps,Psi,Krylov,
C     $                  EigenVector,Hdiagonal(PsiDim+1),FirstCall)
C         Q=-1
C         do j = 0, TrueOrder
C            ovj = ovlps(j)
C            if(j.eq.Q1) ovl1 = ovj
C            if(EigenVal(j).lt.rlxemin .or. 
C     +         EigenVal(j).gt.rlxemax) ovj=0.d0  
C            If(ovj .gt. ovl) then
C               Q = j
C               ovl = ovj
C            endif
C         enddo
C         if( Q .eq. -1 ) then
C            E = 0.5d0*(rlxemin+rlxemax)
C            ovj = 1.d50
C            do j = 0, TrueOrder
C               if(abs(E-EigenVal(j))/(ovlps(j)+1.d-4) .lt. ovj) then
C                  Q = j
C                  ovj=abs(E-EigenVal(j))/(ovlps(j)+1.d-4)
C               endif
C            enddo
C            ovl = ovlps(Q)
C            E = EigenVal(Q)
C            write(18,'(a,3f17.6)') '## No eigenvalue in window:',
C     +           EigenVal(max(0,Q-1))*27.2114d0, E*27.2114d0,
C     +           EigenVal(Q+1)*27.2114d0
C         endif
C         E = EigenVal(Q)
C         beta = abs(EigenVector(0,Q))**2
C         If((Q1.ne.Q) .and. (TrueOrder.LT.IntOrder) .and. jump ) then
C            jump = .false.  
C            call wrtrlxdav(time,trueorder,Q,Q1,E,E1,beta,beta,ovl,ovl1,
C     +                     EigenVal,ovlps,FirstCall,IntOrder)
C            goto 100
C         endif
C         If((TrueOrder.LT.IntOrder) .And. (full .or. 
C     +            (Error+olderr.GT.TolError)) ) Goto 100
C      endif
 
      If( TrueOrder .EQ. IntOrder ) Goto 300


C --- Build Eigenvector ---

      TrueOrder = TrueOrder + 1
      Do D = 1, PsiDim
         Krylov(D,TrueOrder) = EigenVector(0,Q)*(DtPsi(D)-E*Psi(D))
      enddo
      Do j = 1, TrueOrder-1
         Do D = 1, PsiDim
            Krylov(D,TrueOrder) = Krylov(D,TrueOrder) +
     $           EigenVector(j,Q) * (HKrylov(D,j) - E*Krylov(D,j))
         enddo
      enddo

C --- DETERMINE NEW ERROR and APPLY INVERSE OF DIAGONAL ---

      olderr= Error
      Error = 0.d0
      Do D = 1, PsiDim
         Error = Error + DConjg(Krylov(D,TrueOrder))*Krylov(D,TrueOrder)
         Krylov(D,TrueOrder) = Krylov(D,TrueOrder) *
     $   dble(HDiagonal(D)-E)/(dble(HDiagonal(D)-E)**2 + d2)
      enddo
      Error = Sqrt(Error)
 
      If( Error+olderr .GT. TolError ) Goto 200
 

C --- CONVERGENCE! Build new A-vector  (DavStep) ---

      TrueOrder = TrueOrder - 1
      prefac=dcmplx(0.d0,-1.d0)
 300  Do D = 1,PsiDim
         Psi(D)   = EigenVector(0,Q)*Psi(D)
         DtPsi(D) = EigenVector(0,Q)*DtPsi(D)
      EndDo
      Do P = 1,TrueOrder
         Do D = 1,PsiDim
            Psi(D)   =   Psi(D)+EigenVector(P,Q)*Krylov(D,P)
            DtPsi(D) = DtPsi(D)+EigenVector(P,Q)*HKrylov(D,P)
         EndDo
      EndDo

        Psi=(Psi/DZNRM2(PsiDim,Psi,1))

        DtPsi=(DtPsi/DZNRM2(PsiDim,DtPsi,1))
C      If(Q.ne.Q1) call wrtrlxdav(time,trueorder,Q,Q1,E,E1,beta,beta1,
C     +                    ovl,ovl1,EigenVal,ovlps,.false.,IntOrder)


      if(FirstCall .and. rlaxnum.ne.930) then
C.....This is for first diag. wrtrlxdav
         do j = 0, TrueOrder
            ovlps(j) = Abs(EigenVector(0,j))**2
         enddo
      endif
      Q1 = Q
C      call wrtrlxdav(time,trueorder,Q,Q1,E,E1,beta,beta,ovl,ovl1,
C     +               EigenVal,ovlps,FirstCall,IntOrder)
      FirstCall = .false.
      full      = .false.


!      write(6,*) "Davidson order:", TrueOrder
 
      Return
      End subroutine davstep

!> **********************************************************************
!> *                                                                    *
!> *                 SUBROUTINE BDAVSTEP                                *
!> *                                                                    *
!> * Block-Davidson diagonalisation. (single-set multi-packet run).     *
!> * Diagonalises the Hamiltonian represented in the SPFs.              *
!> * Assuming hermitian Hamiltonian and complex WF.                     *
!> *                                                                    *
!> *                                                                    *
!> *                                                                    *
!> * Input parameters:                                                  *
!> *   Psi:       The block of (complex) initial-value vectors.         *
!> *   DtPsi:     Action of Hamiltonian on the initial-value vector,    *
!> *              i.e. H|Psi> .                                         *
!> *   PsiDim     Length of Psi and DtPsi vectors (i.e. adim/npacket)   *
!> *   BDim       Block size. BDim=npacket (=gdim(feb)                  *
!> *   IntOrder:  Maximum interation order.                             *
!> *   TolError:  Maximum error that is tolerated.                      *
!> *   Krylov:    Matrix with minimum size PsiDim*IntOrder with         *
!> *              columns containing the Davison vectors. (complex)     *
!> *   HKrylov:   Matrix containing H*Krylov (complex)                  *
!> *                                                                    *
!> * Output parameters:                                                 *
!> *   Psi:       Propagated Psi.                                       *
!> *   Krylov:    Matrix with minimum size PsiDim*(IntOrder-1) with     *
!> *              columns containing the Krylov vectors H^2|psi> to     *
!> *              H^TrueOrder|psi>.                                     *
!> *   TrueOrder: The order that has actually been used (may be less    *
!> *              than IntOrder).                                       *
!> *   ErrorCode: Error code having the following meaning:              *
!> *              0: everything was o. k.,                              *
!> *              1: illegal integration order,                         *
!> *              3: diagonalization failed,                            *
!> *                                                                    *
!> * Other parameters:                                                  *
!> *   CData:     Complex*16 data needed in the Func routine.           *
!> *   RData:     Real*8 data needed in the Func routine.               *
!> *   IData:     Integer data needed in the Func routine.              *
!> *   LData:     Logical data needed in the Func routine.              *
!> *                                                                    *
!> * External routines:                                                 *
!> *   Func:      Computes the action of the Hamiltonian on Psi.        *
!> *              Called as                                             *
!> *              Func(Abs(Time),Psi,DtPsi,CData,RData,IData,LData).    *
!> * WriteStepBDav: Writes the energy,beta, and error to a file.        *
!>*   Called as WriteStepBDav(Steps,TrueOrder,E,beta,Error,Q,QQ,Time)  *
!>*                                                                    *
!>* Some new Variables:                                                *
!>*  rlaxnum : This is the argument of the keyword relaxation.         *
!>*            If relaxation has no argument, rlaxnum=-1 .             *
!>*            rlaxnum=0   : relaxation to ground-state.               *
!>*            rlaxnum=n   : relaxation to n-th state (n=0...900).     *
!>*            rlaxnum=920 : relaxation=follow.                        *
!>*            rlaxnum=930 : relaxation=lock.                          *
!>*                                                                    *
!>* V8.3 03/2010 HDM                                                   *
!>**********************************************************************
!>!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!>If you do some changes in bdavstep, please do them in mpibdavstep too!
!>!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
c      Subroutine DavStep(Psi,DtPsi,PsiDim,IntOrder,TolError,
c     +                 rlaxnum,
c     +                 ErrorCode,Time,rlxemin,rlxemax,
c     +                 Func)
 
      Subroutine BDavStep(Psi,DtPsi,PsiDim,IntOrder,TolError,
     $              rlaxnum,
     $              ErrorCode,Time,olsen,
     $              Func,rlxemin,rlxemax,Bdim,adim)

      Implicit None

      Integer    PsiDim,BDim,rlaxnum,adim,
     $           TrueOrder,ErrorCode
      Integer*8  IntOrder
      Real*8     TolError,Time,rlxemin,rlxemax,
     $           EigenVal(0:IntOrder),ovlps(0:IntOrder),
     $           RDiagonal(PsiDim)
      Complex*16 EigenVector(0:IntOrder,0:IntOrder),
     $           Tmat( ((IntOrder+1)*(IntOrder+2))/2 )        
      Complex*16 Psi(adim),DtPsi(adim),HDiagonal(PsiDim),
     $           Krylov(PsiDim,0:IntOrder),HKrylov(PsiDim,0:IntOrder)

      Complex*16 workc(24*(IntOrder+1)+48)
      Real*8     workr(3*(IntOrder+1)-2)
      Logical    FirstCall,full,olsen
      External   Func,dznrm2
      REAL*8     dznrm2

      Integer    mxbd,mxcv
      Parameter  (mxbd=320)
      Integer    D,P,Q,QQ,i,j,ij,blk,BOrder,lwork,qmin
      Integer    converged(0:8*BDim+9),qmap(0:8*BDim+9)
      Real*8     Error,ovl,d2,E,beta,beta1,olderr,epse,olden(0:2*mxbd+7)
      real*8     edav,cor2,ErrorM,oldenergy(0:mxbd-1)
      complex*16 cor1 
      save    oldenergy

      data       FirstCall/.true./
      Save       FirstCall, full

      mxcv = 8*BDim+9
      epse=0.d0
C --- CHECK INTEGRATION ORDER ---

      If (IntOrder .LT. 2*BDim) Then
         ErrorCode = 1
         write(6,'(a,i4)') 'IntOrder must be .GE. 2*packets'
         write(6,'(2(a,i4))') 'IntOrder =',IntOrder,' <',2*BDim
         Return
      EndIf

C --- INITIALIZE VARIABLES ---

      if(FirstCall) Then
         if(rlaxnum .ge. 9999) then
            full = .true.
            rlaxnum = rlaxnum - 10000
         else
            full = .false. 
         endif 
         if(rlaxnum.ne.0 .and. rlaxnum.ne.920) then
            write(6,'(a)')' BDAV: Only rlaxnum=0 or follow allowed'
            stop
         endif
         if(Bdim .gt. mxbd) then
            write(6,'(/a,i5,a,i5)') ' ERROR in bdavlib.f : packets =',
     +                          Bdim,' .gt.  mxbd =', mxbd
            write(6,'(a,/a)') ' Increase the parameter mxbd in both',
     +          ' source/lib/ode/bdavlib.f and source/propwf/mpibdav.F'
            write(2,'(/a,i5,a,i5)') ' ERROR in bdavlib.f : packets =',
     +                          Bdim,' .gt.  mxbd =', mxbd
            write(2,'(a,/a)') ' Increase the parameter mxbd in both',
     +          ' source/lib/ode/bdavlib.f and source/propwf/mpibdav.F'
            stop
         endif
      endif
      Do i = 0, mxcv
         qmap(i)=i
         converged(i)=0
      enddo 
      d2 = 1.d-18
      ErrorCode = 0
      ErrorM    = 1.d0
      beta1     = 0.d0
      Error     = 1.d0
      TrueOrder = Bdim - 1
      BOrder    = BDim
      lwork = 24*(IntOrder+1)+48
      call zerovxd(ovlps,IntOrder)

C.....Compute the diagonal of the Hamilton matrix.
      call Get_HamiltonianDiagonal(Psi(1:PsiDim),HDiagonal)
C.....Copy Psi and DtPsi to zero-th column of (complex) Krylov and HKrylov.
c      if(lmult) then
c         call psi2kryc(Psi,Krylov,0)
c         call psi2kryc(DtPsi,HKrylov,0)
c      else
         Do Q = 0, BDim-1
            Do D = 1, PsiDim
                Krylov(D,Q) =   Psi(D+PsiDim*Q)
               HKrylov(D,Q) = DtPsi(D+PsiDim*Q)
            enddo
         enddo
c      endif
      Do D = 1, PsiDim
         Rdiagonal(D) = dble(HDiagonal(D))
      enddo


C --- BUILD T-MATRIX  ---
      ij = 0
      do j = 0, BOrder - 1
         do i = 0, j
            ij = ij + 1
            Tmat(ij) = 0.d0
            Do D = 1, PsiDim
               Tmat(ij) = Tmat(ij) + DConjg(Krylov(D,i))*HKrylov(D,j)
            enddo
            EigenVector(i,j) = Tmat(ij)
         enddo
      enddo


C --- DIAGONALISE T-MATRIX IN THE INITIAL BLOCK OF STARTVECTORS ---
cc      call dsyev('V','U',BOrder,EigenVector,IntOrder+1,EigenVal,
cc     $               workr,lwork,ErrorCode)

      call ZHEEV( 'V', 'U', BOrder, EigenVector, INT(IntOrder+1,4),
     $           EigenVal, workc, lwork, workr, ErrorCode)
!      WRITE(6,*) 'BDAV ZHEEV ERRORCODE:', ErrorCode,workc(1)

      If (ErrorCode .NE. 0) Then
         ErrorCode = 3
         Return
      EndIf

      if(FirstCall) then
         i=1
         beta=1.d0
         beta1=0.d0
         Do Q = 0, BDim-1
            E=EigenVal(Q)
            oldenergy(Q)=E
            olden(Q)=E
            beta1=beta1+E
         enddo
         E=beta1/dble(BDim)  ! Averaged energy for pre-conditioner
         if(rlxemin.gt.-1.d99) E=rlxemin 
         if(rlxemax.lt. 1.d99) E=rlxemax 
      endif   ! firstcall

C --- LOOP : BUILD UP THE KRYLOV AND HKRYLOV SPACES ---

  99  continue  ! Block Iteration Loop
      olderr = ErrorM
      ErrorM = 0.d0
      QQ = 0

 100  Continue  !  Iteration Loop

C --- RE-INITIALIZE VARIABLES ---

      TrueOrder = TrueOrder+1
 42   if(converged(QQ).gt.0) then
         if(abs(EigenVal(qmap(QQ))-olden(QQ)).gt.epse) then
            if(abs(EigenVal(qmap(QQ))-olden(QQ)).gt. 10.d0*
     +       abs(EigenVal(qmap(QQ))-olden(QQ+1))) then ! state missing
               do i = QQ,mxcv-1
                  converged(i) = converged(i+1)
                  olden(i)     = olden(i+1)
               enddo
            elseif(abs(EigenVal(qmap(QQ))-olden(QQ)).gt. 10.d0*
     +       abs(EigenVal(qmap(QQ))-olden(max(0,QQ-1)))) then !st. added
               do i = mxcv, QQ+1, -1
                  converged(i) = converged(i-1)
                  olden(i)     = olden(i-1)
               enddo
               converged(QQ)=0
            else
               converged(QQ)=0
            endif
            goto 42  ! Try again, after converged is modified
         endif
         QQ=QQ+1     ! Skip the converged state
         goto 42
      endif
      if(qq.ge.mxcv) then
         write(6,'(a,2i6)')  'BRDAV: ## qq, q', qq, q
         write(6,'(a,i6,a)') 'BRDAV: Enlarge mxcv=',mxcv,
     +                       ' (also in rundav)'
         WRITE(6,'(a,/(20i4))') 'Qmap:     ',(qmap(i),i=0,mxcv)
         WRITE(6,'(a,/(20i4))') 'Converged:',(converged(i),i=0,mxcv)
         stop 6
      endif
      Q = qmap(QQ)
      E = EigenVal(Q)
      olden(QQ) = E

C --- Build residual vector |r> ---
      Do D = 1, PsiDim
         Krylov(D,TrueOrder) = 
     $           EigenVector(0,Q) * (HKrylov(D,0) - E*Krylov(D,0))
      enddo
      Do j = 1, BOrder-1
         Do D = 1, PsiDim
            Krylov(D,TrueOrder) = Krylov(D,TrueOrder) + 
     $           EigenVector(j,Q) * (HKrylov(D,j) - E*Krylov(D,j))
         enddo
      enddo


C --- DETERMINE ERROR and APPLY INVERSE OF DIAGONAL ---
         Error = 0.d0
         Do D = 1, PsiDim
            Error = Error + abs(Krylov(D,TrueOrder))**2
            Krylov(D,TrueOrder) = Krylov(D,TrueOrder)*
     $           (RDiagonal(D)-E)/( (RDiagonal(D)-E)**2 + d2 )
         enddo
      Error = Sqrt(Error)
      if(QQ.lt.Bdim) ErrorM = max(Error,ErrorM)
      if(Error .lt. 0.5*TolError .and. converged(QQ).eq.0) then
         converged(QQ) = -TrueOrder
      endif
      if(Error .lt. 0.05d0*TolError  .and. converged(QQ).le.0) then
         converged(QQ) = abs(converged(QQ))
      elseif(Error .gt. TolError) then
         converged(QQ) =  0
      endif

C --- OLSEN CORRECTION : cor1=<ev|H_0-E)^-1|r>, cor2=<ev|(H_0-E)^-1|ev>
C --- |r> = (H-E) * eigenvector
C --- Krylov(TrueOrder)  = (H_0-E)^-1 * |r>    
C --- workc(2*precon+D)  =  eigenvector(D)
C --- HKrylov(TrueOrder) =  (H_0-E)^-1 * eigenvector (used for precon)
C --- Correction : (cor1/cor2)*(H_0-E)^-1 * eigenvector
C --- Olsen Correction : Krylov(TrueOrder) = (H_0-E)^-1 * |r> - correction

      if(olsen) then
C --- Build EigenVector.
         Do D = 1, PsiDim
            workc(D) = EigenVector(0,Q) * Krylov(D,0)
         enddo
         Do j = 1, TrueOrder-1
            Do D = 1, PsiDim
               workc(D) = workc(D) + 
     $              EigenVector(j,Q) * Krylov(D,j)
            enddo
         enddo
         cor1=0.d0
         cor2=0.d0
            do D = 1, PsiDim
               cor1 = cor1 +
     $                DConjg(workc(D))*Krylov(D,TrueOrder)
               cor2 = cor2 + abs(workc(D))**2 * 
     $              (RDiagonal(D)-E)/( (RDiagonal(D)-E)**2 + d2 )
            enddo
            do D = 1, PsiDim
               Krylov(D,TrueOrder) = Krylov(D,TrueOrder) - 
     $              (cor1/cor2) * workc(D) *
     $              (RDiagonal(D)-E)/( (RDiagonal(D)-E)**2 + d2 )
            enddo

      endif     ! olsen


C ---  ORTHONORMALIZE  ---
      call orthonc(Krylov,Krylov(1,TrueOrder),ovl,TrueOrder-1,PsiDim)
      call orthonc(Krylov,Krylov(1,TrueOrder),ovl,TrueOrder-1,PsiDim)
      call orthonc(Krylov,Krylov(1,TrueOrder),ovl,TrueOrder-1,PsiDim)
      call orthonc(Krylov,Krylov(1,TrueOrder),ovl,TrueOrder-1,PsiDim)

C --- WRITE STEP AND ERROR IF DESIRED ---
      E = EigenVal(Q)
      beta = Abs(EigenVector(QQ,Q))**2

      if(TrueOrder .ge. BOrder+BDim-1) goto 41
      QQ = QQ + 1
      goto 100
 41   QQ = 0


C --- BUILD HKRYLOV  ---
c      if(lmult) then
c         call kryc2psi(Psi,Krylov,BOrder)
c      else
         Do blk = 0,BDim-1
            Do D = 1,PsiDim
               Psi(D+blk*PsiDim) = Krylov(D,BOrder+blk)
            enddo
         enddo
c      endif
      
      DO Q=BDim,1,-1 ! Loop through all CI vectors
        Call Func(Time,Psi((Q-1)*PsiDim+1:Q*PsiDim),
     .                 DtPsi((Q-1)*PsiDim+1:Q*PsiDim))
      END DO
c      if(lmult) then
c         call psi2kryc(DtPsi,HKrylov,BOrder)
c      else
         Do blk = 0,BDim-1
            Do D = 1,PsiDim
               HKrylov(D,BOrder+blk) = DtPsi(D+blk*PsiDim)
            enddo
         enddo
c      endif

C --- BUILD NEW T-MATRIX ELEMENTS ---

      do j = BOrder, TrueOrder
         do i = 0, j
            ij = ij + 1
            Tmat(ij) = 0.d0
            Do D = 1, PsiDim
               Tmat(ij) = Tmat(ij)+ DConjg(Krylov(D,i))*HKrylov(D,j)
            enddo
         enddo
      enddo

C --- DIAGONALISE T-MATRIX ---
      ij = 0
      do j = 0, TrueOrder
         do i = 0, j
            ij = ij + 1
            EigenVector(i,j) = Tmat(ij)
         enddo
      enddo

      BOrder=TrueOrder+1
cc      call pardsyev('V','U',BOrder,EigenVector,IntOrder+1,EigenVal,
cc     $               workr,lwork,ErrorCode)
      call ZHEEV( 'V', 'U', BOrder, EigenVector, INT(IntOrder+1,4),
     $           EigenVal, workc, lwork, workr, ErrorCode)
      If (ErrorCode .NE. 0) Then
         ErrorCode = 3
         Return
      EndIf


C --- FIND THE EIGENVECTOR WITH THE LARGEST OVERLAP (if not GS sought) -

      if(rlaxnum.eq.920) then   ! follow
         qmin=2*BDim
         Do Q = 0, BDim-1
            beta=0.d0
            j = -1
            Do P = 0, TrueOrder
               if(EigenVal(P).ge.rlxemin .and. 
     +            EigenVal(P).le.rlxemax) then 
                  beta1= Abs(EigenVector(Q,P))**2
                  do i = 0, Q-1
                     if(qmap(i).eq.P) beta1=-1.d0
                  enddo
                  if(beta1 .gt. beta) then
                     j = P
                     beta = beta1
                  endif
               endif
            enddo
            if(j.lt.0) then  ! Energy window too small.
               beta=1.d+99   ! Take vectors with energies closest to window.
               Do P = 0, TrueOrder
                  beta1 = min(abs(rlxemin-EigenVal(P)),
     +                        abs(EigenVal(P)-rlxemax))
                  if(beta1.lt.beta) then
                     beta=beta1
                     j = P
                  endif 
               enddo
            endif
            qmap(Q) = j
            qmin = min(qmin,j)
         enddo  ! Q

         if(full) qmin=0
         qmin=max(qmin-1,0)               ! Set higher entries of qmap.
         Do Q = BDim,min(mxcv,TrueOrder)  ! They may be used when some
   51       do i = 0, Q-1                 ! vectors converge. 
               if(qmap(i).eq.qmin) then
                  qmin=qmin+1
                  if(qmin.gt.TrueOrder) qmin=0
                  goto 51
               endif
            enddo
            qmap(Q)=qmin
         enddo   !  Q
      elseif(rlaxnum.eq.0 .and. rlxemin.gt.-1.d99) then
         QQ=0
         do Q = 0, TrueOrder
            if(EigenVal(Q).gt.rlxemin .and. QQ.lt.mxcv) then
               qmap(QQ)=Q
               QQ=QQ+1
               i=Q
               if(QQ.eq.Bdim) goto 52
            endif
         enddo
   52    Q=i+1
         if(QQ.lt.Bdim) then
            qmap(QQ)=Q
            QQ=QQ+1
            Q=Q+1
            goto 52
         endif
         if(qmap(0).gt.0) then  ! Take the first state below the block
            qmap(QQ)=qmap(0)-1  ! as additional vector (when others
            QQ=QQ+1             ! have converged).
         endif
   53    if(QQ.lt.mxcv) then    ! Take the states following the block
            qmap(QQ)=Q          ! as additional vectors (when others
            QQ=QQ+1             ! have converged).
            Q=Q+1
            goto 53
         endif
      endif      ! (if rlaxnum.eq.920 ...  elseif rlaxnum.eq.0)
      epse=(EigenVal(qmap(Bdim-1))-EigenVal(qmap(0)))/(Bdim*90.d0)

C --- CONTINUE IF NOT CONVERGED --- END OF LOOP  ---
      If( TrueOrder .GT. IntOrder-BDim ) Goto 300
      If( TrueOrder .GE. PsiDim-BDim   ) Goto 300
      If( full .or. (ErrorM+olderr.GT.TolError) ) Goto 99

C --- CONVERGENCE! Build new A-vector ---

 300  continue
c 300  if(lmult) then
c         call kryc2finalpsi(Psi,DtPsi,Krylov,HKrylov,
c     +                      EigenVector,TrueOrder,qmap)
c      else
         Do QQ = 0,BDim-1
            Q = qmap(QQ) 
            Do D = 1,PsiDim 
               Psi(D+QQ*PsiDim)   =  Krylov(D,0)*EigenVector(0,Q)
               DtPsi(D+QQ*PsiDim) = HKrylov(D,0)*EigenVector(0,Q)
            EndDo
            Do P = 1,TrueOrder
               Do D = 1,PsiDim
                  Psi(D+QQ*PsiDim)   = Psi(D+QQ*PsiDim) + 
     +                                  Krylov(D,P)*EigenVector(P,Q)
                  DtPsi(D+QQ*PsiDim) = DtPsi(D+QQ*PsiDim) +
     +                                 HKrylov(D,P)*EigenVector(P,Q)
               EndDo
            EndDo
         EndDo
c      endif
      DO Q=1,BDIM
        Psi((Q-1)*PsiDim+1:Q*PsiDim)=
     +        (Psi((Q-1)*PsiDim+1:Q*PsiDim)/
     +         DZNRM2(PsiDim,Psi((Q-1)*PsiDim+1:Q*PsiDim),1))

        DtPsi((Q-1)*PsiDim+1:Q*PsiDim)=
     +        (DtPsi((Q-1)*PsiDim+1:Q*PsiDim)/
     +        DZNRM2(PsiDim,DtPsi((Q-1)*PsiDim+1:Q*PsiDim),1))
      END DO

      Do QQ = 0, BDim-1
         Q = qmap(QQ)
         if(FirstCall) then
C........This is for first overlaps in wrtrlxbdav
            do j = 0, TrueOrder
               ovlps(j) = Abs(EigenVector(Q,j))**2
            enddo
         endif
         E=EigenVal(Q)
         beta=Abs(EigenVector(QQ,Q))**2
         j=QQ
         if(beta.lt.0.5d0 .and. TrueOrder.gt.Bdim) then
            beta=0.d0
            do i=0,Bdim+1
               if(Abs(EigenVector(i,Q))**2 .gt. beta) then
                  beta=Abs(EigenVector(i,Q))**2
                  j=i
               endif
            enddo
            if(beta.lt.0.67d0) then
               beta=Abs(EigenVector(QQ,Q))**2
               j=QQ
            endif
         endif
      enddo
      Do QQ = 0, BDim+1
         Q = qmap(QQ)
         oldenergy(QQ)=EigenVal(Q)
      enddo

      FirstCall = .false.
      full      = .false.
      edav = E
!      write(6,*) "Block Davidson order:", TrueOrder

      Return
      End subroutine Bdavstep

      Subroutine orthonc(Krylov,v,ovl,n,PsiDim)

      Implicit None

      Integer    PsiDim,n,i,D
      real*8     ovl
      complex*16 Krylov(PsiDim,0:n),v(PsiDim),ovlc

      Do i = 0, n
         ovlc = (0.d0,0.d0)
         Do D = 1,PsiDim
            ovlc = ovlc + DConjg(Krylov(D,i))*v(D)
         enddo
         Do D = 1,PsiDim
            v(D) = v(D)-ovlc*Krylov(D,i)
         enddo
      enddo
      ovl = 0.d0
      Do D = 1,PsiDim
         ovl = ovl + abs(v(D))**2
      enddo
      ovl=sqrt(ovl)
      if(ovl .le. 1.d-40) return
      Do D = 1,PsiDim
         v(D)=v(D)/ovl
      enddo

      Return
      End subroutine orthonc

      END MODULE DAV_Integrators
