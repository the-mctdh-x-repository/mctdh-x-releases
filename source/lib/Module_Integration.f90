!>GPLv3
!    MCTDH-X: the multiconfigurational time-dependent Hartree for 
!    indistinguishable particles software
!
!    Copyright (C) 2021  A. U. J. Lode, M. C. Tsatsos, 
!                        E. Fasshauer, S. E. Weiner, 
!                        R. Lin, L. Papariello, P. Molignini, 
!                        C. Lévêque, M. Büttner , J. Xiang, S. Dutta
!
!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <https://www.gnu.org/licenses/>.
!
!
!
!
!<GPLv3










!> This module contains the Integration wrappers that call the appropriate chosen integration routine to integrate the orbital or the coefficients equations of motion.
       MODULE Integration

       CONTAINS

      !---------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      !> @ingroup sevenstepintegration 
      !! @brief This Routine uses various Numerical Integrators to propagate the
      !! Coefficients' equations of motion.
      !!      
      !! ### Inputs and Outputs:
      !! @param[in]     Coeffs_Integrator ::  Character*3 specifies the chosen integrator 
      !! @param[in]     IntStep           ::  Real*8 specifies the time-step to integrate
      !! @param[in]     VIN               ::  Complex*16(Nconf) Initial CI-Vector
      !! @param[in]     VOUT              ::  Complex*16(Nconf) Will be overwritten
      !! @param[in]     CIntOrder         ::  Integer Integration Order
      !! @param[in]     ABSTIME           ::  Real*8 Absolute time
      !! @param[in]     whichstep         ::  Integer number of the Integration Step in the 
      !!                                      MCTDHB integration scheme. 
      !!                                      If whichstep=6, the SIL Routine also does the back
      !!                                      propagation of the CI-Vector for error evaluation.          
      !! @param[out]     VIN              ::  Propagated CI-Vector.   
      !! @param[out]     VOUT             ::  If whichstep=6, VOUT contains Back-propagated
      !!                                        CI-Vector for error evaluation.      
       SUBROUTINE INTEGRATOR_CI(Coeffs_Integrator,IntStep,&
                               VIN,VOUT,Error_SIL,&
                               ABSTIME,whichstep)


       USE Coefficients_Parameters
       USE Global_Parameters ,ONLY:STATE,Minimal_Krylov,&
                                  Maximal_Krylov,Job_Prefactor


!> @TODO: document the imports and variables
       USE Matrix_Elements
       Use OMP_lib
       USE Interaction_Parameters
       USE DVR_Parameters
       USE Orbital_Parallelization_Parameters
       USE ABM_Integrators
       USE SIL_Integrators
       USE DAV_Integrators
       USE HamiltonianAction_Coefficients

       USE MPI

       IMPLICIT NONE

       INTEGER :: IERR
       INTEGER :: whichstep
        
       COMPLEX*16 :: VIN(NCONF),VOUT(NCONF)
       REAL*8  :: Error_SIL,time,IntStep,dznrm2,ABSTIME
       REAL*8  :: rlxemin,rlxemax 
       LOGICAL :: SIL,DIAGONAL

       CHARACTER*3 :: Coeffs_Integrator

!!     MCTDH SIL STUFF
       INTEGER :: SIL_Errorcode,SIL_STEPS,BDAV_Errorcode
       Real*8  :: Integrated_Step
       LOGICAL :: Relaxation,SIL_Restart,Also_BackProp
       

       external DZNRM2

       TIME=ABSTIME
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1!!!!!!!!!!!!!!!!!!!
!!!!!! Exception for GP
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1!!!!!!!!!!!!!!!!!!!
       !> generalize this exception to include the TDHF case
       IF (Morb.eq.1) THEN
          SIL=.FALSE.
          DIAGONAL=.FALSE. 
          call MPI_BCAST(SIL,1,MPI_LOGICAL,0,MPI_COMM_WORLD,ierr)
          call MPI_BCAST(DIAGONAL,1,MPI_LOGICAL,0,MPI_COMM_WORLD,ierr)
          RETURN
       ENDIF
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1!!!!!!!!!!!!!!!!!!!
!!!!!! Exception for GP
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1!!!!!!!!!!!!!!!!!!!

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!        
!!!    MCTDH SIL DIAGONALIZATION ROUTINE
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!        

       IF (trim(Coeffs_Integrator).eq.'DSL') THEN
          IF (ABS(REAL(Job_Prefactor)).lt.0.001d0) then
             WRITE(6,*) "SIL DIAG,'DSL', is only for RELAXATION!!!!"
             STOP
          ENDIF
          CALL Get_HamiltonianAction_CI_Time(ABSTIME,VIN,VOUT)
          CALL SILDIAG(VIN,VOUT,NCONF,Maximal_Krylov,Error_SIL,&
                       .FALSE.,SIL_STEPS,&
                       SIL_Errorcode,STATE-1,Abstime,.TRUE.,&
                       Get_HamiltonianAction_CI_Time,&
                       Minimal_Krylov)
          VOUT=VIN
          SIL=.FALSE.
          DIAGONAL=.FALSE. 
          call MPI_BCAST(SIL,1,MPI_LOGICAL,0,MPI_COMM_WORLD,ierr)
          call MPI_BCAST(DIAGONAL,1,MPI_LOGICAL,0,MPI_COMM_WORLD,ierr)
       ENDIF
 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!        
!!!    MCTDH SIL PROPAGATION ROUTINE
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!        
       IF (trim(Coeffs_Integrator).eq.'MCS') THEN
          
          IF (ABS(REAL(Job_Prefactor)).ge.0.001d0) then
             RELAXATION=.TRUE.
          ELSE 
             RELAXATION=.FALSE.
          ENDIF
         !> whether to reuse previously constructed Krylov subspace vector
          SIL_RESTART=.TRUE.

          IF (WhichStep.eq.2) THEN
             Also_BackProp=.FALSE.
          ELSEIF (WhichStep.eq.6) THEN
             Also_BackProp=.TRUE.
          ELSE
             Write(6,*) "UNKNOWN PROPAGATION STEP,&
                         &Preemptively also Backpropagation is done..."
             Also_BackProp=.TRUE.
          ENDIF    

         !> Applies Hamiltonian onto the vector of the coefficients.
          CALL Get_HamiltonianAction_CI_Time(ABSTIME,VIN,VOUT)
          !> Now we have availble the vector of coefficients in VIN
          !> and the Hamiltonian applied to the vector in VOUT (which 
          !> gives the derivative wrt time b/c of Schrödinger equation)
               
          !> Time evolution: this solver takes as an input variable a function
          !> (Get_HamiltonianAction_CI_Time) which evaluates the action of the 
          !> Hamiltonian on the coefficients.
          !> There is a lot of parameters (documented inside the solver itself, Felicia). 
          CALL SILstep(VIN,VOUT,NCONF,IntStep,Maximal_Krylov,Error_SIL,&
                       RELAXATION,SIL_RESTART,.FALSE.,&
                       Integrated_Step,SIL_Errorcode,Abstime,&
                       Get_HamiltonianAction_CI_Time,&
                       STATE,Also_BackProp,Minimal_Krylov)
          !> flags to communicate to all the processors such that all the workers in the
          !> team know they are done helping calculating the action of the Ham on the coeffs.
          SIL=.FALSE.
          DIAGONAL=.FALSE. 
          call MPI_BCAST(SIL,1,MPI_LOGICAL,0,MPI_COMM_WORLD,ierr)
          call MPI_BCAST(DIAGONAL,1,MPI_LOGICAL,0,MPI_COMM_WORLD,ierr)
          !> Broadcasting = work on the coeffs is done.
          !> After receiving these flags they will await for another broadcast of
          !> cf_scl to restart 
       ENDIF

      
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!        
!!!    MCTDH DAVIDSON RELAXATION ROUTINE
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!        
       IF (TRIM(Coeffs_Integrator).eq.'DAV') THEN

          IF (ABS(REAL(Job_Prefactor)).lt.0.001d0) then
             WRITE(6,*) "DAV DIAG,'DAV', is only for RELAXATION!!!!"
             STOP
          ENDIF

          CALL Get_HamiltonianAction_CI_Time(ABSTIME,VIN,VOUT)
!!!   FOR STATE=930>--<RELAXATION=LOCK RLXEMIN/MAX MUST BE INPUT!!!
          rlxemin=0.d0
          rlxemax=0.d0
          

          call DavStep(VIN,VOUT,NCONF,INT(Maximal_Krylov,8),ERROR_SIL,&
                       STATE-1,&
                       SIL_ErrorCode,Time,rlxemin,rlxemax,&
                       Get_HamiltonianAction_CI_Time)

          SIL=.FALSE.
          DIAGONAL=.FALSE. 
          call MPI_BCAST(SIL,1,MPI_LOGICAL,0,MPI_COMM_WORLD,ierr)
          call MPI_BCAST(DIAGONAL,1,MPI_LOGICAL,0,MPI_COMM_WORLD,ierr)

       ENDIF
 
!!!!! Currently not working attempts to use ABM or RK5 for the
!!!!! coefficients...
!c       IF (trim(Coeffs_Integrator).eq.'ABM') THEN
!c          CNewRestart=NewStart
!c          CNRepeatedSteps=10
!c          CNSteps=0
!c          CiErrorCode=0
!c          VIN=dcmplx(0.d0,-1.d0)*VIN
!c          VTMP=VIN
!c          INTSTEP=INTSTEP
!c          CALL Get_HamiltonianAction_CI_Time_ABM(TIME,VIN,VOUT)
!c          CALL OMPABM(VIN,VOUT,NConf,IntStep,Time,CIntOrder, &
!c              IntStep,Error_SIL,.TRUE.,CNSteps,CNRepeatedSteps,&
!c              CiErrorCode,CAuxPsi,Get_HamiltonianAction_CI_Time_ABM)
!c              VOUT=VIN !*dcmplx(0.d0,1.d0)
!c          if (whichstep .eq. 6) then
!c             VIN=VTMP
!c             CALL Get_HamiltonianAction_CI_Time_ABM(TIME,VIN,VTMP)
!c             CALL OMPABM(VIN,VTMP,NConf,-1.d0*IntStep,Time,CIntOrder, &
!c               -1.d0*IntStep,Error_SIL,.TRUE.,CNSteps,CNRepeatedSteps,&
!c               CiErrorCode,CAuxPsi,Get_HamiltonianAction_CI_Time_ABM)
!c          endif
!!          IF (whichstep.eq.2) then
!!             VOUT=VOUT/DZNRM2(Nconf,VIN,1)
!!          ENDIF
!c          SIL=.FALSE.
!c          call MPI_BCAST(SIL,1,MPI_LOGICAL,0,MPI_COMM_WORLD,ierr)
!c
!c         IF(CiErrorCode.ne.0) write(6,*)"IN OMPABM !!!!!!! I -ERROR",&
!c              CiErrorCode
!c
!c      endif
!c       
!c       IF (trim(Coeffs_Integrator).eq.'RK5') THEN
!c          VTMP=VIN
!c          CALL Get_HamiltonianAction_CI_Time_ABM(ABSTIME,VIN,VOUT)
!c          call rk5(VIN,VOUT,NConf,IntStep,Time,IntStep,&
!c                    Error_SIL,CiErrorCode,CAuxPsi(1:NConf,1:7),&
!c                    Get_HamiltonianAction_CI_Time_ABM,&
!c                   .TRUE.,CNSteps)
!c!          VIN=VIN/DZNRM2(Nconf,VIN,1)
!c!          VOUT=VOUT/DZNRM2(Nconf,VOUT,1)
!c          SIL=.FALSE.
!c          call MPI_BCAST(SIL,1,MPI_LOGICAL,0,MPI_COMM_WORLD,ierr)
!c        
!c         
!c       ENDIF
      END SUBROUTINE INTEGRATOR_CI

      SUBROUTINE INTEGRATOR_BLOCK_CI(Coeffs_Integrator,IntStep,&
                              VIN,VOUT,Error_SIL,&
                              ABSTIME,whichstep)

      USE Coefficients_Parameters
      USE Global_Parameters ,ONLY:STATE,Minimal_Krylov,&
                                  Maximal_Krylov,Job_Prefactor,Olsen,&
                                  RLX_Emin,RLX_Emax,BlockSize
      USE Matrix_Elements
      Use OMP_lib
      USE Interaction_Parameters
      USE DVR_Parameters
      USE Orbital_Parallelization_Parameters
      USE ABM_Integrators
      USE SIL_Integrators
      USE DAV_Integrators
      USE HamiltonianAction_Coefficients

      USE MPI

      IMPLICIT NONE

      INTEGER :: IERR
      INTEGER :: whichstep
       
      COMPLEX*16 :: VIN(NCONF*Blocksize),VOUT(NCONF*Blocksize)
      REAL*8  :: Error_SIL,time,IntStep,dznrm2,ABSTIME
      REAL*8  :: rlxemin,rlxemax 
      LOGICAL :: SIL,DIAGONAL

      CHARACTER*3 :: Coeffs_Integrator

!!    MCTDH SIL STUFF
      INTEGER :: Q,SIL_Errorcode,SIL_STEPS,BDAV_Errorcode
      Real*8  :: Integrated_Step
      LOGICAL :: Relaxation,SIL_Restart,Also_BackProp
      

      external DZNRM2

      TIME=ABSTIME
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1!!!!!!!!!!!!!!!!!!!
!!!!!! Exception for GP
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1!!!!!!!!!!!!!!!!!!!
       IF (Morb.eq.1) THEN
          RETURN
       ENDIF
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1!!!!!!!!!!!!!!!!!!!
!!!!!! Exception for GP
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1!!!!!!!!!!!!!!!!!!!

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!        
!!!    MCTDH BLOCK DAVIDSON RELAXATION ROUTINE
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!        
       IF (TRIM(Coeffs_Integrator).eq.'BDV') THEN

          IF (ABS(REAL(Job_Prefactor)).lt.0.001d0) then
             WRITE(6,*) "BDV DIAG,'BDV', is only for RELAXATION!!!!"
             STOP
          ENDIF
          
          DO Q=BlockSize,1,-1 ! Loop through all CI vectors
             CALL Get_HamiltonianAction_CI_Time(ABSTIME,  &
                              VIN((Q-1)*Nconf+1:Q*Nconf), &
                              VOUT((Q-1)*Nconf+1:Q*Nconf))
          END DO
!!!   FOR STATE=930>--<RELAXATION=LOCK RLXEMIN/MAX MUST BE INPUT!!!

          call BDavStep(VIN,VOUT,NCONF,INT(Maximal_Krylov,8),ERROR_SIL,&
                       STATE-1,&
                       BDAV_Errorcode,Time,Olsen,&
                       Get_HamiltonianAction_CI_Time,rlx_emin,rlx_emax,&
                       BlockSize,INT(NCONF*BlockSize,4))

          SIL=.FALSE.
          DIAGONAL=.FALSE. 
          call MPI_BCAST(SIL,1,MPI_LOGICAL,0,MPI_COMM_WORLD,ierr)
          call MPI_BCAST(DIAGONAL,1,MPI_LOGICAL,0,MPI_COMM_WORLD,ierr)

       ENDIF
 
      END SUBROUTINE INTEGRATOR_BLOCK_CI



      !---------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      !> @ingroup sevenstepintegration 
      !! @brief Orbital equations Integration routine for all unified cases (standard, multi level and mpi).
      !! Under the hood, this routine reshapes Psi, OPsi (variables of FUNC etc.) to have the correct shape for 
      !! the different cases. This is done by employing pointers. The most general case (3d arrays for multi level)
      !! is taken as the baseline for the shape of the arrays.
      !! Some references:
      !! https://stackoverflow.com/questions/33779009/pass-1d-array-to-3d-array
      !! https://stackoverflow.com/questions/30308601/the-name-of-a-subroutine-can-be-a-variable-in-fortran
      !!
      !! ### Inputs and Outputs:
      !! @param[in]     Psi          : the value of the wavefunction.
      !! @param[in]     OPsi         : 
      !! @param[in]     Abstime      : the initial time of the integration (in absolute time).
      !! @param[in]     xIntPeriod   : length of integration interval (?).
      !! @param[in]     NewRestart   : 
      !! @param[in]     ABMError     : 
      !! @param[in]     MYID         :
      !>
      !> Outputs:
      !>
      !>
      !> PM, June 2021      
      SUBROUTINE Integrator_Orb_Unified(Psi,         &
                                       OPsi,         &
                                       AbsTime,         &
                                       xIntPeriod,      &
                                       NewRestart,      &
                                       ABMError,        &
                                       MYID)


      ! Modules used:
      use Global_Parameters         ! Defines global parameters
      use Runge_Kutta               ! Uses RungeKutta5, RungeKutta8 to perfom integration steps at different orders.
      use Bulirsch_Stoer            ! Uses BSStep - Driver routine for the BS integrator.
      use Stiff_Integrator          ! Uses ZVODE_wrapper - Integration routine for the case of stiff orbital EOM (black box).
      use ABM_Integrators           ! Uses OMPABM, ABM - the Adams-Bashforth-Moulton predictor-corrector integration routine.
      use Orbital_EquationOfMotion  !
      ! Paolo: I think this next routine is not used anymore in the unified version.
      !use Array_Interfaces          ! Uses Get_1dFrom3d_Array - to reshape the multicomponent orbitals in a way that the solver can digest them.
      use Orbital_Allocatables, ONLY:    LocalSize 
      use MPI
      use DVR_Parameters
      use Orbital_Parallelization_Parameters, ONLY: LocalPsiDim

      IMPLICIT NONE

      ! Variables defined and used in the subroutine:
      REAL*8  :: xIntPeriod, AbsTime, xInitStep, TolError, ABMError
      INTEGER :: IntOrder, NSteps, NRepeatedSteps, iErrorCode, PsiDim
      LOGICAL :: NewRestart
      REAL*8 :: RelTime, xIntPeriod_ORG
      REAL*8 :: ActualLargeStep, NextLargeStep
      INTEGER :: SmallSteps
      INTEGER :: MYID
      INTEGER, SAVE :: rkf45_mpi_flag=-1
      COMPLEX*16, DIMENSION(LocalPsiDim(MYID+1)*Morb*Nlevel,18) :: AuxPsi
      COMPLEX*16, DIMENSION(LocalPsiDim(MYID+1),Morb,Nlevel) :: Psi, OPsi


      PsiDim = LocalPsiDim(MYID+1)*Morb*Nlevel
      AuxPsi=Zero
      xIntPeriod_ORG=xIntPeriod
      SmallSteps=0
      TolError=ABMError
      xInitStep=xIntPeriod
      IntOrder=Orbital_Integrator_order
      NSteps=0
      NRepeatedSteps=10
      iErrorCode=0
      RelTime=AbsTime

      IF (MPI_ORBS.eqv..FALSE.) THEN

         IF (.NOT.((trim(Orbital_Integrator).eq.'OMPABM').or.(trim(Orbital_Integrator).eq.'ABM').or. &
             (trim(Orbital_Integrator).eq.'RK').or.(trim(Orbital_Integrator).eq.'BS').or. &
             (trim(Orbital_Integrator).eq.'STIFF'))) THEN
             write(6,*) "CHECK Orbital_Integrator?"
             stop
          ENDIF

      IF (trim(Orbital_Integrator).eq.'OMPABM') then
        if(Orbital_Integrator_order.gt.8) then 
         write(6,*)"ADAMS-BASHFORTH-MOULTON IS IMPLEMENTED TILL 8 ORDER"
         write(6,*)"CHANGE Orbital_Integrator_order<=8"
         stop "Integrator ABM"
        endif

        call  OMPABM(Psi,  &
                     OPsi, &
                     PsiDim,         &   
                     xIntPeriod,     &
                     RelTime,        &
                     IntOrder,       &
                     xInitStep,      &
                     TolError,       &
                     NewRestart,     &
                     NSteps,         &
                     NRepeatedSteps, &
                     iErrorCode,     &
                     AuxPsi,&
                     FUNC_No_MPI)

        IF(iErrorCode.ne.0) write(6,*)"IN OMPABM !!!!!!! I -ERROR",&
              iErrorCode
        OrbitalIntegrator_Steps=OrbitalIntegrator_Steps+NSteps
        write(1003,'(a30,i2,a22,i6,i16)')"OMP ABM of" &
           ,Orbital_Integrator_order," order takes SmallSteps", Nsteps,&
            OrbitalIntegrator_Steps
        endIF
 

      IF (trim(Orbital_Integrator).eq.'ABM') then
        if(Orbital_Integrator_order.gt.8) then 
     write(6,*)"ADAMS-BASHFORTH-MOULTON IS IMPLEMENTED TILL 8 ORDER"
     write(6,*)"CHANGE Orbital_Integrator_order<=8"
        stop "Integrator ABM"
        endif
      call  ABM(Psi,OPsi,PsiDim,xIntPeriod,RelTime,IntOrder, &
                  xInitStep,TolError,NewRestart,NSteps,NRepeatedSteps,&
                  iErrorCode,AuxPsi,FUNC_No_MPI,AbsABMError)
      IF(iErrorCode.ne.0) write(6,*)"IN ABM !!!!!!! I -ERROR",iErrorCode
         OrbitalIntegrator_Steps=OrbitalIntegrator_Steps+NSteps
         write(1003,'(a30,i3,a22,i6,i16)')"ADAMS-BASHFORTH-MOULTON of" &
                ,Orbital_Integrator_order," order takes SmallSteps",& 
                Nsteps,OrbitalIntegrator_Steps
      endIF


       IF (trim(Orbital_Integrator).eq.'RK') then
        if((Orbital_Integrator_order.ne.5).and.&
           (Orbital_Integrator_order.ne.8)) then 
           write(6,*)"RUNGE-KUTTA works with order 5 or 8"
           write(6,*)"CHANGE Orbital_Integrator_order=8 or =5"
        stop "Integrator RK5/8"
        endif
 
        if(Orbital_Integrator_order.eq.5) then
      call RungeKutta5(Psi,OPsi,PsiDim,xIntPeriod,RelTime,xInitStep,&
                    TolError,iErrorCode,AuxPsi(1:PsiDim,1:7),FUNC_No_MPI,&
                    NSteps)
        elseif (Orbital_Integrator_order.eq.8) then
      call RungeKutta8(Psi,OPsi,PsiDim,xIntPeriod,RelTime,xInitStep,&
                    TolError,iErrorCode,AuxPsi(1:PsiDim,1:12),FUNC_No_MPI,&
                    NSteps)
        endif 
        Reltime=Reltime+xIntPeriod 
        CALL FUNC_No_MPI(Reltime,Psi,Opsi) 

      IF(iErrorCode.ne.0) write(6,*)"IN RK !!!!!!! I -ERROR"&
               ,iErrorCode
            OrbitalIntegrator_Steps=OrbitalIntegrator_Steps+NSteps
            xIntPeriod=xIntPeriod_ORG
      endIF


      IF (trim(Orbital_Integrator).eq.'BS') then
        if(Orbital_Integrator_order.gt.16) then 
     write(6,*)"BULIRSCH-STOER IS IMPLEMENTED UNTIL 16th ORDER"
     write(6,*)"CHANGE Orbital_Integrator_order<=16"
        stop "Integrator BS"
        endif
89      continue 
            SmallSteps=0
          call  BSStep(Psi,OPsi,PsiDim,xIntPeriod,RelTime,IntOrder, &
                  xInitStep,TolError,& 
           ActualLargeStep, NextLargeStep,SmallSteps, &
                  iErrorCode,AuxPsi,FUNC_No_MPI,AbsBSError,PolyExtrapol)
         OrbitalIntegrator_Steps=OrbitalIntegrator_Steps+SmallSteps
        IF((xIntPeriod-ActualLargeStep).ge.TolError) then
       RelTime=RelTime+ActualLargeStep
       CALL FUNC_No_MPI(Reltime,Psi,OPsi) 
         xIntPeriod=xIntPeriod-ActualLargeStep
         xInitStep=min(NextLargeStep,xIntPeriod)
      IF(iErrorCode.ne.0) write(6,*)"IN BS !!!!!!! I -ERROR",iErrorCode
         goto 89
        ENDIF
         xIntPeriod=xIntPeriod_ORG
        endIF
      IF (trim(Orbital_Integrator).eq.'STIFF') then
891      continue 

         call  ZVODE_wrapper(FUNC_No_MPI,          &
                             PsiDim,        &
                             Psi, &
                             RelTime,       &
                             xIntPeriod,    &
                             TolError,      &
                             iErrorCode,    & 
                             AuxPsi, & 
                             Orbital_Integrator_order)
   
        IF(xIntperiod-(Reltime-Abstime).gt.TolError) then
           xIntperiod=(AbsTime+xIntPeriod_ORG)-Reltime
        
           IF(iErrorCode.ne.2)&
               write(6,*)"IN ZVODE!!!!!!! I -ERROR",iErrorCode

           OrbitalIntegrator_Steps=OrbitalIntegrator_Steps+1

           goto 891
         ENDIF
 
          write(6,'(a20,i3,i16)')"ZVODE took Steps:",1,&
                               OrbitalIntegrator_Steps

          IF(iErrorCode.eq.2) iErrorCode=0

          xIntPeriod=xIntPeriod_ORG

       ENDIF ! Orbital_Integrator



       ELSE ! MPI_ORBS .eqv. .TRUE.

          IF (.NOT.((trim(Orbital_Integrator).eq.'MPIRK').or.(trim(Orbital_Integrator).eq.'MPIABM'))) THEN
               If (trim(Orbital_Integrator).eq.'RK') Then
                  If (MYID.eq.0) Then
                  write(6,*) "Orbital_Integrator is set to MPIRK"
                  EndIf
                  Orbital_Integrator='MPIRK'
               ElseIf (trim(Orbital_Integrator).eq.'ABM') Then
                  If (MYID.eq.0) Then
                  write(6,*) "Orbital_Integrator is set to MPIABM"
                  EndIf
                  Orbital_Integrator='MPIABM'
               Else
                  write(6,*) "CHECK Orbital_Integrator?"
              stop
               Endif
          ENDIF
!       IF ((MPI_ORBS.eqv..FALSE.).and.((trim(Orbital_Integrator).eq.'MPIRK')  &
!                                 .or.((trim(Orbital_Integrator).eq.'MPIABM')))) THEN
!         write(6,*) "Something wrong CHANGE Orbital_Integrator?"
!         stop "Integrator ABM"
!      ENDIF

      IF (trim(Orbital_Integrator).eq.'MPIRK') THEN
        IF (Orbital_Integrator_Order.eq.4) THEN
            CALL Complex_rk4vec_MPI( RelTime, &
                                   INT(PsiDim,8), &
                                   PSI, &
                                   xIntPeriod, &
                                   FUNC_MPI, & 
                                   MYID)

        ELSEIF (Orbital_Integrator_Order.eq.5) THEN
           if ((abs(Reltime-Time_Begin).lt.1.d-12).and. &
                (ABS(DREAL(Job_Prefactor)).ge.1.d-10)) then
             rkf45_mpi_flag=-1
           endif

!           Write(6,*) "RKF45 FLAG, MYID", rkf45_mpi_flag, MYID

           CALL Complex_rkf45_MPI(FUNC_MPI, &
                                  INT(PsiDim,8), &
                                  PSI, OPSI,RelTime,   &
                                  RelTime+xIntPeriod, &
                                  TolError, TolError, &
                                  rkf45_mpi_flag, &
                                  MYID)
!
!           IF(abs(rkf45_mpi_flag).ne.2) THEN 
!               write(6,*)"RKF45_MPI error, MYID",rkf45_mpi_flag, MYID
!           ENDIF

        ELSE
          Write(6,*) "Orbital_Integrator_Order must be 4 or 5  &
                      for MPIRK!!!!"
          STOP
        ENDIF
      ELSE IF (trim(Orbital_Integrator).eq.'MPIABM') THEN

          call MPIABM(Psi,OPsi,INT(PsiDim,8), &
                       xIntPeriod,RelTime,INT8(IntOrder), &
                       xInitStep,TolError,NewRestart, &
                       INT8(NSteps),INT8(NRepeatedSteps),&
                       INT8(iErrorCode),AuxPsi,FUNC_MPI,MYID)

          IF(iErrorCode.ne.0) THEN 
             write(6,*)"IN MPIABM !!!!!!! I -ERROR",iErrorCode
          ENDIF

          OrbitalIntegrator_Steps=OrbitalIntegrator_Steps+NSteps

          write(1003,'(a30,i2,a22,i6,i16)')"MPI ABM of" &
             ,Orbital_Integrator_order," order takes SmallSteps",&
              Nsteps,&
              OrbitalIntegrator_Steps

          IF(iErrorCode.ne.0) THEN
              write(6,*)"I",iErrorCode,NSteps,NRepeatedSteps
          ENDIF

      ENDIF  ! Orbital_Integrator

      ENDIF  ! MPI_ORBS



      IF(iErrorCode.ne.0) write(6,*)"I",iErrorCode,NSteps,NRepeatedSteps


   END SUBROUTINE Integrator_Orb_Unified



!> Orbital equations Integration routine.
!> This subroutine is obsolete.
   SUBROUTINE Integrator_Orb &
                     (PSI,OPSI,AuxPsi,AbsTime,xIntPeriod,NewRestart,&
                           ABMError)

      use Global_Parameters
      use Runge_Kutta
      use bulirsch_stoer
      use Stiff_Integrator
      use ABM_Integrators
      use SIL_Integrators
      use DAV_Integrators
      USE Orbital_EquationOfMotion

      IMPLICIT NONE

      COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb) :: Psi,OPsi
      COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z*Morb,18) ::AuxPsi

      REAL*8 :: xIntPeriod, AbsTime,xInitStep,TolError,ABMError
      INTEGER ::IntOrder,NSteps,NRepeatedSteps,iErrorCode,PsiDim
      LOGICAL :: NewRestart

      REAL*8 :: RelTime,xIntPeriod_ORG
      REAL*8 :: ActualLargeStep, NextLargeStep
      INTEGER :: SmallSteps

      xIntPeriod_ORG=xIntPeriod
      SmallSteps=0


      TolError=ABMError
      xInitStep=xIntPeriod
      IntOrder=Orbital_Integrator_order
      NSteps=0
      NRepeatedSteps=10
      iErrorCode=0
      PSiDim=NDVR_X*NDVR_Y*NDVR_Z*Morb
      RelTime=AbsTime


      IF (trim(Orbital_Integrator).eq.'OMPABM') then
        if(Orbital_Integrator_order.gt.8) then 
         write(6,*)"ADAMS-BASHFORTH-MOULTON IS IMPLEMENTED TILL 8 ORDER"
         write(6,*)"CHANGE Orbital_Integrator_order<=8"
         stop "Integrator ABM"
        endif

        call  OMPABM(Psi,OPsi,PsiDim,xIntPeriod,RelTime,IntOrder, &
                  xInitStep,TolError,NewRestart,NSteps,NRepeatedSteps,&
                  iErrorCode,AuxPsi,FUNC)
        IF(iErrorCode.ne.0) write(6,*)"IN OMPABM !!!!!!! I -ERROR",&
              iErrorCode
        OrbitalIntegrator_Steps=OrbitalIntegrator_Steps+NSteps
        write(1003,'(a30,i2,a22,i6,i16)')"OMP ABM of" &
           ,Orbital_Integrator_order," order takes SmallSteps", Nsteps,&
            OrbitalIntegrator_Steps
        endIF
 
      IF (trim(Orbital_Integrator).eq.'ABM') then
        if(Orbital_Integrator_order.gt.8) then 
     write(6,*)"ADAMS-BASHFORTH-MOULTON IS IMPLEMENTED TILL 8 ORDER"
     write(6,*)"CHANGE Orbital_Integrator_order<=8"
        stop "Integrator ABM"
        endif
      call  ABM(Psi,OPsi,PsiDim,xIntPeriod,RelTime,IntOrder, &
                  xInitStep,TolError,NewRestart,NSteps,NRepeatedSteps,&
                  iErrorCode,AuxPsi,FUNC,AbsABMError)
      IF(iErrorCode.ne.0) write(6,*)"IN ABM !!!!!!! I -ERROR",iErrorCode
         OrbitalIntegrator_Steps=OrbitalIntegrator_Steps+NSteps
         write(1003,'(a30,i3,a22,i6,i16)')"ADAMS-BASHFORTH-MOULTON of" &
                ,Orbital_Integrator_order," order takes SmallSteps",& 
                Nsteps,OrbitalIntegrator_Steps
      endIF


       IF (trim(Orbital_Integrator).eq.'RK') then
        if((Orbital_Integrator_order.ne.5).and.&
           (Orbital_Integrator_order.ne.8)) then 
           write(6,*)"RUNGE-KUTTA works with order 5 or 8"
           write(6,*)"CHANGE Orbital_Integrator_order=8 or =5"
        stop "Integrator RK5/8"
        endif
 
        if(Orbital_Integrator_order.eq.5) then
      call RungeKutta5(Psi,OPsi,PsiDim,xIntPeriod,RelTime,xInitStep,&
                    TolError,iErrorCode,AuxPsi(1:PsiDim,1:7),FUNC,&
                    NSteps)
        elseif (Orbital_Integrator_order.eq.8) then
      call RungeKutta8(Psi,OPsi,PsiDim,xIntPeriod,RelTime,xInitStep,&
                    TolError,iErrorCode,AuxPsi(1:PsiDim,1:12),FUNC,&
                    NSteps)
        endif 
        Reltime=Reltime+xIntPeriod 
        CALL FUNC(Reltime,Psi,Opsi) 

      IF(iErrorCode.ne.0) write(6,*)"IN RK !!!!!!! I -ERROR"&
               ,iErrorCode
            OrbitalIntegrator_Steps=OrbitalIntegrator_Steps+NSteps
            xIntPeriod=xIntPeriod_ORG
      endIF

      IF (trim(Orbital_Integrator).eq.'BS') then
        if(Orbital_Integrator_order.gt.16) then 
     write(6,*)"BULIRSCH-STOER IS IMPLEMENTED UNTIL 16th ORDER"
     write(6,*)"CHANGE Orbital_Integrator_order<=16"
        stop "Integrator BS"
        endif
89      continue 
            SmallSteps=0
          call  BSStep(Psi,OPsi,PsiDim,xIntPeriod,RelTime,IntOrder, &
                  xInitStep,TolError,& 
           ActualLargeStep, NextLargeStep,SmallSteps, &
                  iErrorCode,AuxPsi,FUNC,AbsBSError,PolyExtrapol)
         OrbitalIntegrator_Steps=OrbitalIntegrator_Steps+SmallSteps
        IF((xIntPeriod-ActualLargeStep).ge.TolError) then
       RelTime=RelTime+ActualLargeStep
       CALL FUNC(Reltime,Psi,Opsi) 
         xIntPeriod=xIntPeriod-ActualLargeStep
         xInitStep=min(NextLargeStep,xIntPeriod)
      IF(iErrorCode.ne.0) write(6,*)"IN BS !!!!!!! I -ERROR",iErrorCode
         goto 89
        ENDIF
         xIntPeriod=xIntPeriod_ORG
        endIF
      IF (trim(Orbital_Integrator).eq.'STIFF') then
891      continue 
      call  ZVODE_wrapper(FUNC,PsiDim,Psi,RelTime,xIntPeriod,&
                TolError,iErrorCode,AuxPsi,Orbital_Integrator_order)
        IF(xIntperiod-(Reltime-Abstime).gt.TolError) then
           xIntperiod=(AbsTime+xIntPeriod_ORG)-Reltime
        
           IF(iErrorCode.ne.2)&
               write(6,*)"IN ZVODE!!!!!!! I -ERROR",iErrorCode

           OrbitalIntegrator_Steps=OrbitalIntegrator_Steps+1

           goto 891
         ENDIF
 
          write(6,'(a20,i3,i16)')"ZVODE took Steps:",1,&
                               OrbitalIntegrator_Steps

          IF(iErrorCode.eq.2) iErrorCode=0

          xIntPeriod=xIntPeriod_ORG

       endIF


      IF(iErrorCode.ne.0) write(6,*)"I",iErrorCode,NSteps,NRepeatedSteps

         END SUBROUTINE Integrator_Orb

!> Orbital equations Integration routine.
!> This subroutine is obsolete.
      SUBROUTINE Integrator_NlevelOrb &
                     (PSI,OPSI,AuxPsi,AbsTime,xIntPeriod,NewRestart,&
                           ABMError)

      use Global_Parameters
      use Runge_Kutta
      use bulirsch_stoer
      use Stiff_Integrator
      use ABM_Integrators
      use SIL_Integrators
      use DAV_Integrators
      USE  Orbital_EquationOfMotion
      USE Array_Interfaces

      IMPLICIT NONE

      COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb,Nlevel)& 
                           :: Psi,OPsi

!!! Declare pointers to the orbital arrays 
      COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z*Morb*Nlevel)&
                           :: Psi_Integrate, OPsi_Integrate
!!! Declare pointers to the orbital arrays 

      COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z*Morb*Nlevel,18)&
                           :: AuxPsi

      REAL*8 :: xIntPeriod, AbsTime,xInitStep,TolError,ABMError
      INTEGER ::IntOrder,NSteps,NRepeatedSteps,iErrorCode,PsiDim
      LOGICAL :: NewRestart

      REAL*8 :: RelTime,xIntPeriod_ORG
      REAL*8 :: ActualLargeStep, NextLargeStep
      INTEGER :: SmallSteps

      xIntPeriod_ORG=xIntPeriod
      SmallSteps=0


      TolError=ABMError
      xInitStep=xIntPeriod
      IntOrder=Orbital_Integrator_order
      NSteps=0
      NRepeatedSteps=10
      iErrorCode=0
      PSiDim=NDVR_X*NDVR_Y*NDVR_Z*Morb*nlevel
      RelTime=AbsTime

!!! Assign the orbital arrays for integration 
!!! Assign the orbital arrays for integration 
      CALL Get_1dFrom3d_Array(Psi_Integrate,Psi,PsiDim)
      CALL Get_1dFrom3d_Array(OPsi_Integrate,OPsi,PsiDim)
!!! Assign the orbital arrays for integration 
!!! Assign the orbital arrays for integration 

      IF (trim(Orbital_Integrator).eq.'OMPABM') then

        if(Orbital_Integrator_order.gt.8) then 
           write(6,*) "ADAMS-BASHFORTH-MOULTON &
                       & IS IMPLEMENTED TILL 8 ORDER"
           write(6,*) "CHANGE Orbital_Integrator_order<=8"
           stop "Integrator ABM"
        endif

        call  OMPABM(Psi_Integrate,OPsi_Integrate,&
                     PsiDim,xIntPeriod,RelTime,IntOrder, &
                     xInitStep,TolError,NewRestart,&
                     NSteps,NRepeatedSteps,&
                     iErrorCode,AuxPsi,FUNC_Nlevel_Wrapper)

        IF(iErrorCode.ne.0) THEN
           write(6,*)"IN OMPABM !!!!!!! I -ERROR",iErrorCode
        ENDIF

        OrbitalIntegrator_Steps=OrbitalIntegrator_Steps+NSteps

        write(1003,'(a30,i2,a22,i6,i16)')"OMP ABM of" &
           ,Orbital_Integrator_order," order takes SmallSteps", Nsteps,&
            OrbitalIntegrator_Steps

      endIF

 
      IF (trim(Orbital_Integrator).eq.'ABM') then
        if(Orbital_Integrator_order.gt.8) then 
           write(6,*)"ADAMS-BASHFORTH-MOULTON &
                      & IS IMPLEMENTED TILL 8 ORDER"
           write(6,*)"CHANGE Orbital_Integrator_order<=8"
           stop "Integrator ABM"
        endif

        call  ABM(Psi_Integrate,OPsi_Integrate,PsiDim, &
                  xIntPeriod,RelTime,IntOrder, &
                  xInitStep,TolError,NewRestart,NSteps,NRepeatedSteps,&
                  iErrorCode,AuxPsi,FUNC_Nlevel_Wrapper,AbsABMError)
         IF(iErrorCode.ne.0) THEN 
            write(6,*)"IN ABM !!!!!!! I -ERROR",iErrorCode
         ENDIF

         OrbitalIntegrator_Steps=OrbitalIntegrator_Steps+NSteps
         write(1003,'(a30,i3,a22,i6,i16)')"ADAMS-BASHFORTH-MOULTON of" &
                ,Orbital_Integrator_order," order takes SmallSteps",& 
                Nsteps,OrbitalIntegrator_Steps
      endIF


      IF (trim(Orbital_Integrator).eq.'RK') then

        if ((Orbital_Integrator_order.ne.5).and.&
            (Orbital_Integrator_order.ne.8)) then 

           write(6,*)"RUNGE-KUTTA works with order 5 or 8"
           write(6,*)"CHANGE Orbital_Integrator_order=8 or =5"
           stop "Integrator RK5/8"

        endif
 
        if(Orbital_Integrator_order.eq.5) then

          call RungeKutta5(Psi_Integrate,OPsi_Integrate,PsiDim,&
                    xIntPeriod,RelTime,xInitStep,&
                    TolError,iErrorCode,&
                    AuxPsi(1:PsiDim,1:7),FUNC_Nlevel_Wrapper,&
                    NSteps)

        elseif (Orbital_Integrator_order.eq.8) then

          call RungeKutta8(Psi_Integrate,OPsi_Integrate,PsiDim,&
                    xIntPeriod,RelTime,xInitStep,&
                    TolError,iErrorCode,&
                    AuxPsi(1:PsiDim,1:12),FUNC_Nlevel_Wrapper,&
                    NSteps)

        endif 

        Reltime=Reltime+xIntPeriod 
        CALL FUNC_Nlevel_Wrapper(Reltime,Psi_Integrate,Opsi_Integrate) 

        IF(iErrorCode.ne.0) THEN
            write(6,*)"IN RK !!!!!!! I -ERROR"&
                       ,iErrorCode
        ENDIF

        OrbitalIntegrator_Steps=OrbitalIntegrator_Steps+NSteps
        xIntPeriod=xIntPeriod_ORG

      endIF


      IF (trim(Orbital_Integrator).eq.'BS') then
        if(Orbital_Integrator_order.gt.16) then 
          write(6,*)"BULIRSCH-STOER IS IMPLEMENTED UNTIL 16th ORDER"
          write(6,*)"CHANGE Orbital_Integrator_order<=16"
          stop "Integrator BS"
        endif

89      continue 

        SmallSteps=0

        call  BSStep(Psi_Integrate,OPsi_Integrate,PsiDim,&
                  xIntPeriod,RelTime,IntOrder, &
                  xInitStep,TolError,& 
                  ActualLargeStep, NextLargeStep,SmallSteps, &
                  iErrorCode,AuxPsi,FUNC_Nlevel_Wrapper,&
                  AbsBSError,PolyExtrapol)

        OrbitalIntegrator_Steps=OrbitalIntegrator_Steps+SmallSteps

        IF((xIntPeriod-ActualLargeStep).ge.TolError) then ! BS did not finish and needs to be called again.

           RelTime=RelTime+ActualLargeStep
           CALL FUNC_Nlevel_Wrapper(Reltime, &
                                    Psi_Integrate,Opsi_Integrate) 
           xIntPeriod=xIntPeriod-ActualLargeStep
           xInitStep=min(NextLargeStep,xIntPeriod)

           IF(iErrorCode.ne.0) THEN
              write(6,*)"IN BS !!!!!!! I -ERROR",iErrorCode
           ENDIF

           goto 89

        ENDIF
           xIntPeriod=xIntPeriod_ORG
        endIF
      IF (trim(Orbital_Integrator).eq.'STIFF') then
891      continue 

         call  ZVODE_wrapper(FUNC_Nlevel_Wrapper,PsiDim,&
                  Psi_Integrate,RelTime,xIntPeriod,&
                  TolError,iErrorCode,AuxPsi,Orbital_Integrator_order)

        IF(xIntperiod-(Reltime-Abstime).gt.TolError) then ! ZVODE did not finish and must be restarted.

           xIntperiod=(AbsTime+xIntPeriod_ORG)-Reltime
        
           IF(iErrorCode.ne.2) THEN
               write(6,*)"IN ZVODE!!!!!!! I -ERROR",iErrorCode
           ENDIF

           OrbitalIntegrator_Steps=OrbitalIntegrator_Steps+1

           goto 891

         ENDIF

 
         write(1003,'(a20,i3,i16)')"ZVODE took Steps:",1,&
                               OrbitalIntegrator_Steps

          IF(iErrorCode.eq.2) THEN !Reset errorcode
             iErrorCode=0
          ENDIF

          xIntPeriod=xIntPeriod_ORG

       endIF


      IF(iErrorCode.ne.0) THEN
         write(6,*)"Orbital Integrator Error",&
                     iErrorCode,NSteps,NRepeatedSteps
      ENDIF
!!! Reassign the output orbital arrays 
!!! Reassign the output orbital arrays 
      CALL Get_3dFrom1d_Array(Psi_Integrate,Psi)
      CALL Get_3dFrom1d_Array(OPsi_Integrate,OPsi)
!!! Reassign the output orbital arrays 
!!! Reassign the output orbital arrays 

      END SUBROUTINE Integrator_NlevelOrb


!> Orbital equations Integration routine for domain decomposed orbitals.
!> This subroutine is obsolete.
      SUBROUTINE Integrator_MPI_Orb(PSI,OPSI,AuxPsi,&
                                    AbsTime,xIntPeriod,NewRestart,&
                                    ABMError,MYID)

      use Global_Parameters
      use Runge_Kutta
      use bulirsch_stoer
      use Stiff_Integrator
      use ABM_Integrators
      use SIL_Integrators
      use DAV_Integrators
      USE Orbital_EquationOfMotion
      USE Orbital_Parallelization_Parameters

      IMPLICIT NONE

      INTEGER :: MYID

      COMPLEX*16, DIMENSION(LocalPsiDim(MYID+1),Morb) :: Psi,OPsi
      COMPLEX*16, DIMENSION(LocalPsiDim(MYID+1)*Morb,18) ::AuxPsi

      REAL*8 :: xIntPeriod, AbsTime,xInitStep,TolError,ABMError
      INTEGER ::IntOrder,NSteps,NRepeatedSteps,iErrorCode,PsiDim
      LOGICAL :: NewRestart

      REAL*8 :: RelTime,xIntPeriod_ORG
      REAL*8 :: ActualLargeStep, NextLargeStep
      INTEGER :: SmallSteps
      INTEGER, SAVE :: rkf45_mpi_flag=-1

      xIntPeriod_ORG=xIntPeriod
      SmallSteps=0


      TolError=ABMError
      xInitStep=xIntPeriod
      IntOrder=Orbital_Integrator_order
      NSteps=0
      NRepeatedSteps=10
      iErrorCode=0
      RelTime=AbsTime


      IF (MPI_ORBS.eqv..FALSE.) THEN
         write(6,*) "Something wrong CHANGE Orbital_Integrator?"
         stop "Integrator ABM"
      ENDIF

      IF (trim(Orbital_Integrator).eq.'MPIRK') THEN
        IF (Orbital_Integrator_Order.eq.4) THEN
          CALL Complex_rk4vec_MPI( RelTime, Morb*LocalPsiDim(MYID+1), &
                                  PSI, xIntPeriod, func_MPI, MYID)
        ELSEIF (Orbital_Integrator_Order.eq.5) THEN
           if ((abs(Reltime-Time_Begin).lt.1.d-12).and. &
                (ABS(DREAL(Job_Prefactor)).ge.1.d-10)) then
             rkf45_mpi_flag=-1
           endif

!           Write(6,*) "RKF45 FLAG, MYID", rkf45_mpi_flag, MYID

           CALL Complex_rkf45_MPI(Func_MPI,LocalPsiDim(MYID+1)*Morb, &
                                  PSI,OPSI,RelTime, &
                                  RelTime+xIntPeriod, &
                                  TolError, TolError, rkf45_mpi_flag, &
                                  MYID)
!
!           IF(abs(rkf45_mpi_flag).ne.2) THEN 
!               write(6,*)"RKF45_MPI error, MYID",rkf45_mpi_flag, MYID
!           ENDIF

        ELSE
          Write(6,*) "Orbital_Integrator_Order must be 4 or 5  &
                      for MPIRK!!!!"
          STOP
        ENDIF
      ELSE IF (trim(Orbital_Integrator).eq.'MPIABM') THEN

          call MPIABM(Psi,OPsi,LocalPsiDim(MYID+1)*Morb, &
                       xIntPeriod,RelTime,INT8(IntOrder), &
                       xInitStep,TolError,NewRestart, &
                       INT8(NSteps),INT8(NRepeatedSteps),&
                       INT8(iErrorCode),AuxPsi,FUNC_MPI,MYID)

          IF(iErrorCode.ne.0) THEN 
             write(6,*)"IN MPIABM !!!!!!! I -ERROR",iErrorCode
          ENDIF

          OrbitalIntegrator_Steps=OrbitalIntegrator_Steps+NSteps

          write(1003,'(a30,i2,a22,i6,i16)')"MPI ABM of" &
             ,Orbital_Integrator_order," order takes SmallSteps",&
              Nsteps,&
              OrbitalIntegrator_Steps

          IF(iErrorCode.ne.0) THEN
              write(6,*)"I",iErrorCode,NSteps,NRepeatedSteps
          ENDIF

      ENDIF


         END SUBROUTINE Integrator_MPI_Orb

       END MODULE Integration
