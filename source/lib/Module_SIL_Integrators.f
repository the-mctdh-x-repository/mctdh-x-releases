!>GPLv3
!    MCTDH-X: the multiconfigurational time-dependent Hartree for 
!    indistinguishable particles software
!
!    Copyright (C) 2021  A. U. J. Lode, M. C. Tsatsos, 
!                        E. Fasshauer, S. E. Weiner, 
!                        R. Lin, L. Papariello, P. Molignini, 
!                        C. Lévêque, M. Büttner , J. Xiang, S. Dutta
!
!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <https://www.gnu.org/licenses/>.
!
!
!
!
!<GPLv3










!> In this module the Short Iterative Lanczos routines are collected.
!> \todo make the Krylov basis arrays allocatables
      MODULE SIL_Integrators

      USE Function_Library
      USE HamiltonianAction_Coefficients

      CONTAINS
!>*********************************************************************
!>                                                                    *
!>                 SHORT ITERATIVE LANCZOS (sillib.f)                *
!>                                                                   *
!>Library module containing a short iterative Lanczos integrator.    *
!>                                                                   *
!>Contains:                                                          *
!>  SILStep:      The (real) Lanczos integration routine.            *
!>  SILErrorMsg:  Returns for a given error number a corresponding   *
!>                error message.                                     *
!>  SILDiag:      The (real) Lanczos diagonalisation routine.        *
!>                                                                   *
!>*********************************************************************
!>
!>
!>********************************************************************
!>                                                                   *
!>                SUBROUTINE SILSTEP                                 *
!>                                                                   *
!>Integrates a system of complex linear first order differential     *
!>equations with constant and Hermitian Hamiltonian employing the    *
!>short iterative Lanczos method. The routine runs with both         *
!>variable step size and order. (First it increases the order to     *
!>achieve the desired accuracy. If this failes within the maximum    *
!>order, the stepsize is reduced.) SILStep makes only one single     *
!>integration step, so it has to be imbedded into a loop that calls  *
!>SILStep until the desired time interval is integrated. The ODE is  *
!>of the form i dPsi/dt = H|Psi> = Func(Time,Psi,DtPsi) =: DtPsi or  *
!>dPsi/dt = -i H|Psi> = Func(Time,Psi,DtPsi) =: DtPsi, depending on  *
!>the flag "StdForm". All computations are performed with double     *
!>precision.                                                         *
!>                                                                   *
!>Input parameters:                                                  *
!>  Psi:       The (complex) initial-value vector.                   *
!>  DtPsi:     Action of Hamiltonian on the initial-value vector,    *
!>             i.e. H|Psi> or -i H|Psi>, depending on "StdForm".     *
!>  PsiDim     Length of Psi and DtPsi vectors.                      *
!>  IntPeriod: Lenght of time interval to be integrated.             *
!>  IntOrder:  Maximum integration order.                            *
!>  TolError:  Maximum error that is tolerated.                      *
!>  Relax:     Flag for relaxation calculation. If true, Psi is      *
!>             relaxated, else propagated.                           *
!>  Restart:   Flag for restarting the integrator. If true, the      *
!>             Krylov space is built up before propagation, else the *
!>             old Krylov vectors are used.                          *
!>  StdForm:   If true, Func(Time,Psi,DtPsi) = -i H|Psi>, else       *
!>             Func(Time,Psi,DtPsi) = H|Psi>.                        *
!>  Steps:     Number of steps made so far (Steps is passed to       *
!>             "WriteStep").                                         *
!>  Krylov:    Matrix with minimum size PsiDim*(IntOrder-1) with     *
!>             columns containing the Krylov vectors H^2|psi> to     *
!>             H^TrueOrder|psi>. The Krylov vectors are needed on    *
!>             entry only when Restart is false.                     *
!>  Also_BackProp: Logical that decides if the constructed Krylov    *
!>                 vectors are used also to propagate Psi back in    *
!>                 time. If Also_BackProp it .true. then Psi is the  *
!>                 propagated and DtPsi the backpropagated function  *
!>                 on output.                                        *
!>Output parameters:                                                 *
!>  Psi:       Propagated Psi.                                       *
!>  DtPsi:     If Also_BackProp is .true. then DtPsi contains the    *
!>             backpropagated Psi and otherwise the                  *
!>             Normalised first Krylov vector, i. e. (H-<H>)|Psi>.   *
!>  Krylov:    Matrix with minimum size PsiDim*(IntOrder-1) with     *
!>             columns containing the Krylov vectors H^2|psi> to     *
!>             H^TrueOrder|psi>.                                     *
!>  Stepsize:  Time interval that actually has been integrated (can  *
!>             be lower than IntPeriod).                             *
!>  TrueOrder: The order that has actually been used (may be less    *
!>             than IntOrder).                                       *
!>  Steps:     Same as on entry.                                     *
!>                                                                   *
!>External routines:                                                 *
!>  Func:      Computes the action of the Hamiltonian on Psi.        *
!>             Called as                                             *
!>             Func(AbsTime,Psi,DtPsi,CData,RData,IData,LData).      *
!>Some new Variables:                                                *
!> Relax   : logical set true for a relaxation run.                  *
!>                                                                   *
!>V6.0 MB                                                            *
!>V8.2 05/01 HDM  Addition of 'relaxation' to exited states.         *
!>               Eigenvector-space now dynamically allocated.       *

      Subroutine SILStep(Psi,DtPsi,PsiDim,IntPeriod,IntOrder,TolError,
     +                   Relax,Restart,StdForm,Stepsize,
     +                   ErrorCode,Time,Func,STATE,Also_BackProp,
     +                   MinOrder)
 
!> TODO: create doxygen compatible documentation of the used/declared variables
      Implicit None

      Real*8     RelativeMinStep,Tiny
      Parameter  (RelativeMinStep = 1.0D-10,Tiny = 1.0D-18)

      Logical    Relax,Restart,StdForm,Also_BackProp
      Integer    PsiDim,IntOrder,ErrorCode,STATE
      Integer*8  TrueOrder !> variable to store the true integration order
      Integer    MinOrder
      Real*8     IntPeriod,TolError,Stepsize,Time,beta1,Time_Temp
      Complex*16 Psi(PsiDim),DtPsi(PsiDim),Krylov(PsiDim,2:IntOrder)
      Complex*16 Psi_Back(PsiDim)

      External   Func,DZNRM2
 
      Real*8     DZNRM2

      Integer    OldOrder,D,P,Q
      Real*8     Beta,Error,OldError,MinStepsize,OldStepsize,
     +           EigenVector(0:IntOrder,0:IntOrder+2),
     +           Diagonal(0:IntOrder),OffDiag(IntOrder),
     +           EigenVal(0:IntOrder),OffDg2(IntOrder+1)
      Complex*16 Alpha,CBeta,Sum,Sum_Back,PreFactor,CInverse

      Save       OldError,OldStepsize,OldOrder

C --- CHECK INTEGRATION ORDER ---

      If ( IntOrder .LT. 2 ) Then
         ErrorCode = 1
         write(6,'(a,i4,a)') 'IntOrder =',IntOrder,' is too small!'
         Return
      EndIf

C --- INITIALIZE VARIABLES ---

      Stepsize = IntPeriod
      MinStepsize = Abs(RelativeMinStep*IntPeriod)
      ErrorCode = 0
      If (.not.StdForm) Then
         PreFactor = (1.0D0,0.0D0)
      ElseIf (Relax) Then
         PreFactor = (-1.0D0,0.0D0)
      Else
         PreFactor = (0.0D0,-1.0D0)
      EndIf


C --- RETURN RESULT IF DIFFERENTIAL EQUATION IS NOT A SYSTEM ---

      If (PsiDim .Eq. 1) Then
         TrueOrder = 1
         Error = 0.0D0
!!         Call WriteStep(Steps,TrueOrder,Stepsize,Error,Time)
!!         Psi(1) = Exp(StepSize*DtPsi(1)/Psi(1))*Psi(1)
         Psi(1)=1.d0
         DtPsi(1)=1.d0
         Return
      EndIf

C --- SKIP CALCULATION OF KRYLOV VECTORS IF DESIRED ---

      If (Restart) Then
         OldError=0.0D0
         OldOrder=0
         OldStepsize=0.0D0
      Else
         If (Abs(Stepsize) .GT. Abs(OldStepsize)+MinStepsize) Then
            ErrorCode = 4
            Return
         EndIf
         TrueOrder = OldOrder
         Error = Abs(OldError*(Stepsize/OldStepsize)**TrueOrder)
!!         Call WriteStep(Steps,TrueOrder,Stepsize,Error,Time)
         Goto 200
      EndIf

C --- FURTHER INITIALIZE VARIABLES ---

      Alpha = (0.0D0,0.0D0)
      Beta = 0.0D0
      TrueOrder = 1

C --- COMPUTE FIRST DIAGONAL ELEMENT ---

      Do D = 1,PsiDim
         Alpha = Alpha+DConjg(Psi(D))*DtPsi(D)
      EndDo
      Diagonal(0) = Alpha/PreFactor

C --- DETERMINE CORRESPONDING BASIS VECTOR AND OFF-DIAGONAL ELEMENT ---

      Do D = 1,PsiDim
         DtPsi(D) = DtPsi(D)-Alpha*Psi(D)
         Beta = Beta+Dble(DConjg(DtPsi(D))*DtPsi(D))
      EndDo

      Beta = Sqrt(Beta)
      OffDiag(1) = Beta
      beta1 = Beta
      CBeta = PreFactor*Beta

C --- NORMALIZE BASIS VECTOR ---

      If (Beta .LT. 1.d-20) Then
         Beta = 0.0D0
         CInverse = (0.0D0,0.0D0)
      Else
         CInverse = 1.0D0/CBeta
      EndIf
      Do D = 1,PsiDim
         DtPsi(D) = CInverse*DtPsi(D)
      EndDo

C --- COMPUTE ERROR ESTIMATE ---

      Error = Abs(Beta*Stepsize)

C --- WRITE STEPSIZE AND ERROR IF DESIRED ---

!!      Call WriteStep(Steps,TrueOrder,Stepsize,Error,Time)

C --- BUILD UP THE KRYLOV SPACE ---
 
 100  Continue
        
C --- CHECK IF STEPSIZE IS TOO SMALL ---

      If (Abs(Stepsize) .LT. Abs(MinStepSize)) Then
         ErrorCode = 2
         Return
      EndIf

C --- RE-INITIALIZE VARIABLES ---

      Alpha = (0.0D0,0.0D0)
      Beta = 0.0D0
      TrueOrder = TrueOrder+1


C --- EVALUATE FUNCTION WITH LAST BASIS VECTOR ---

      If (TrueOrder .Eq. 2) Then
         Call Func(Time,DtPsi,Krylov(1,2))
      Else
         Call Func(Time,Krylov(1,TrueOrder-1),Krylov(1,TrueOrder))
      EndIf

C --- COMPUTE DIAGONAL ELEMENT ---

C Note that the Krylov vectors number zero and one aren't stored in
C "Krylov" but in "Psi" and "DtPsi", respectively.

      If (TrueOrder .Eq. 2) Then
         Do D = 1,PsiDim
            Alpha = Alpha+DConjg(DtPsi(D))*Krylov(D,2)
         EndDo
      Else
         Do D = 1,PsiDim
            Alpha = Alpha+DConjg(Krylov(D,TrueOrder-1))
     +              *Krylov(D,TrueOrder)
         EndDo
      EndIf
      Diagonal(TrueOrder-1) = Alpha/PreFactor

C --- COMPUTE OFF-DIAGONAL ELEMENT AND BASIS VECTOR ---

      If (TrueOrder .Eq. 2) Then
         Do D = 1,PsiDim
            Krylov(D,2) = Krylov(D,2)-Alpha*DtPsi(D)-CBeta*Psi(D)
            Beta = Beta+Dble(DConjg(Krylov(D,2))*Krylov(D,2))
         EndDo
      ElseIf (TrueOrder .Eq. 3) Then
         Do D = 1,PsiDim
            Krylov(D,3) = Krylov(D,3)-Alpha*Krylov(D,2)-CBeta*DtPsi(D)
            Beta = Beta+Dble(DConjg(Krylov(D,3))*Krylov(D,3))
         EndDo
      Else
         Do D = 1,PsiDim
            Krylov(D,TrueOrder) = Krylov(D,TrueOrder)
     +         -Alpha*Krylov(D,TrueOrder-1)-CBeta*Krylov(D,TrueOrder-2)
            Beta = Beta+DConjg(Krylov(D,TrueOrder))*Krylov(D,TrueOrder)
         EndDo
      EndIf

      Beta = Sqrt(Beta)
      OffDiag(TrueOrder) = Beta
      CBeta = PreFactor*Beta

C --- NORMALIZE BASIS VECTOR ---

      If (Beta .LT. 1.d-20) Then
         OffDiag(TrueOrder) = 0.0D0
         Beta = 0.0D0
         CInverse = (0.0D0,0.0D0)
      Else
         CInverse = 1.0D0/CBeta
      EndIf
      
      Do D = 1,PsiDim
         Krylov(D,TrueOrder) = CInverse*Krylov(D,TrueOrder)
      EndDo

C --- COMPUTE ERROR ESTIMATE ---
      
      Error = Abs(Error*Beta*Stepsize/Dble(TrueOrder))
      Time_temp=StepSize*(TolError/Error)**(1.d0/Dble(TrueOrder))

C --- WRITE STEPSIZE AND ERROR IF DESIRED ---

!!      Call WriteStep(Steps,TrueOrder,Stepsize,Error,Time)
      IF ((ABS(Time_temp).ge.ABS(StepSize))
     +    .And.(TrueOrder.ge.MinOrder)
     +    .And.(TrueOrder.lt.IntOrder)) THEN
         GOTO 102 
      ELSEIF (TrueOrder.lt.IntOrder) THEN 
         GOTO 100
      ENDIF

C --- DECREASE STEPSIZE IF LANCZOS DIDN'T CONVERGE ---
      
      If (Error .GT. TolError) Then
         Stepsize = Stepsize*(TolError/Error)**(1.0D0/Dble(TrueOrder))
         WRITE(6,*) "DECREASED STEPSIZE!"
!!         Call WriteStep(Steps,TrueOrder,Stepsize,TolError,Time)
      EndIf



C --- CONTINUE IF NOT CONVERGED ---
       
      If((TrueOrder.LT.IntOrder) .And. (Error.GT.TolError)
     +   .And. (Beta.gt.1.d-20) )  Goto 100

      IF (TrueOrder.LT.MinOrder) GOTO 100

!      IF (Time_Temp.lt.StepSize) THEN
!           StepSize=Time_Temp
!      ENDIF
 

 102  Continue

           
C --- ESTIMATE NEXT MATRIX ELEMENT ---

      Diagonal(TrueOrder) = Diagonal(TrueOrder-1)
c      Diagonal(TrueOrder) = Diagonal(TrueOrder-1) + beta


C --- SAVE SOME VALUES IN CASE THAT NEXT CALL IS NON-RESTART CALL ---

      OldOrder = TrueOrder
      OldStepsize = Stepsize
      OldError = Error
    
C --- Save Diagonal and off diagonal values since the diagonalisation ---
C --- routine will overwrite those. ---
      call  cpvxd(Diagonal,EigenVal,TrueOrder+1)
      call  cpvxd(OffDiag,OffDg2,TrueOrder)

C --- DIAGONALIZE LANCZOS MATRIX --- CALL LAPACK ROUTINE --- 
C --- EigenVector(*,IntOrder+1) serves as work array 
      call DSTEQR('I',TrueOrder+1,EigenVal,OffDg2,EigenVector,
     +            IntOrder+1,EigenVector(1,IntOrder+1),ErrorCode)

      If (ErrorCode .NE. 0) Then
         ErrorCode = 3
         Return
      EndIf


 200  Continue                  ! Jump to here, if not RESTART.

C --- PROPAGATE WAVEFUNCTION ---
C Note again that the Krylov vectors number zero and one aren't stored
C in "Krylov" but in "Psi" and "DtPsi", respectively.

      IF (.not.Relax) THEN
       IF (Also_BackProp.eqv..TRUE.) THEN
          Do P = 0,TrueOrder
            Sum = (0.0D0,0.0D0)
            Sum_Back = (0.d0,0.d0)
            Prefactor=(0.d0,-1.d0)
            Do Q = 0,TrueOrder
               Sum = Sum+Exp(PreFactor*EigenVal(Q)*Stepsize)*
     +               EigenVector(P,Q)*EigenVector(0,Q)
               Sum_Back = Sum_Back+
     +                    Exp(-1.d0*PreFactor*EigenVal(Q)*Stepsize)*
     +                    EigenVector(P,Q)*EigenVector(0,Q)
            EndDo
            If (P .Eq. 0) Then
               Do D = 1,PsiDim
                  Psi_Back(D) = Sum_Back*Psi(D)
                  Psi(D) = Sum*Psi(D)
               EndDo
            ElseIf (P .Eq. 1) Then
               Do D = 1,PsiDim
                  Psi_Back(D) = Psi_Back(D)+Sum_Back*DtPsi(D)
                  Psi(D) = Psi(D)+Sum*DtPsi(D)
               EndDo
            Else
               Do D = 1,PsiDim
                  Psi_Back(D) = Psi_Back(D)+Sum_Back*Krylov(D,P)
                  Psi(D) = Psi(D)+Sum*Krylov(D,P)
               EndDo
            EndIf
          EndDo
          DtPsi=Psi

          Psi=Psi_Back

        ELSEIF (Also_BackProp.eqv..FALSE.) THEN
         Do P = 0,TrueOrder
            Sum = (0.0D0,0.0D0)
            Prefactor=(0.d0,-1.d0)
            Do Q = 0,TrueOrder
               Sum = Sum+Exp(PreFactor*EigenVal(Q)*Stepsize)*
     +               EigenVector(0,Q)*EigenVector(P,Q)
            EndDo
            If (P .Eq. 0) Then
               Do D = 1,PsiDim
                  Psi(D) = Sum*Psi(D)
               EndDo
            ElseIf (P .Eq. 1) Then
               Do D = 1,PsiDim
                  Psi(D) = Psi(D)+Sum*DtPsi(D)
               EndDo
            Else
               Do D = 1,PsiDim
                  Psi(D) = Psi(D)+Sum*Krylov(D,P)
               EndDo
            EndIf
          EndDo
          DtPsi=Psi
        ENDIF !Also_BackProp-IF
      ELSE !RELAX-IF
! Initialize Backup of Psi
        Do D=1,PsiDim
           Psi_Back(D)=(0.d0,0.d0)
        EndDo
    
! Expand Relaxed Psi in Krylov Basis (Relaxation)
        Do P = 0,TrueOrder
           If (P .Eq. 0) Then
              Do D = 1,PsiDim
                 Psi_Back(D) = Psi_Back(D)+Psi(D)*EigenVector(P,STATE-1)
              EndDo
           ElseIf (P .Eq. 1) Then
              Do D = 1,PsiDim
                 Psi_Back(D) = Psi_Back(D)+DtPsi(D)*
     &                         EigenVector(P,STATE-1)
              EndDo
           Else
            Do D = 1,PsiDim
              Psi_Back(D) = Psi_Back(D)+Krylov(D,P)*
     &                      EigenVector(P,STATE-1)
            EndDo
           EndIf
        EndDo
       
        Do D=1,PsiDim
           DtPsi(D)=dcmplx(0.d0,-1.d0)*Psi_Back(D)
           Psi(D)=Psi_Back(D)
        EndDo
        Psi=Psi/DZNRM2(PsiDim,Psi,1)
        DtPsi=DtPsi/DZNRM2(PsiDim,DtPsi,1)
      ENDIF !RELAX-IF
       
      Return
      End subroutine SILstep


!>*********************************************************************
!>                                                                    *
!>                       SUBROUTINE SILERRORMSG                       *
!>                                                                   *
!>Generates for a given error number returned by "SILStep/SILStep2/  *
!>SILStep3" a corresponding error message.                           *
!>                                                                   *
!>Input parameters:                                                  *
!>  Error: Error code returned by SILStep/SILStep2.                  *
!>                                                                   *
!>Output parameters:                                                 *
!>  Msg:   Error message.                                            *
!>                                                                   *
!>V7.0 MB                                                            *
      Subroutine SILErrorMsg (Error,Msg)

      Implicit None

      Integer       Error
      Character*(*) Msg

C --- GENERATE ERROR MESSAGE ---

      If (Error .Eq. 1) Then
         Msg = 'Illegal integration order'
      ElseIf (Error .Eq. 2) Then
         Msg = 'Stepsize underflow'
      ElseIf (Error .Eq. 3) Then
         Msg = 'Diagonalisation failed'
      ElseIf (Error .Eq. 4) Then
         Msg = 'Illegal initial stepsize'
      Else
         Msg = 'Unknown error occurred'
      EndIf

      Return
      End Subroutine SILErrorMsg


!>*********************************************************************
!>                                                                    *
!>                 SUBROUTINE SILDIAG                                 *
!>                                                                    *
!> Diagonalises a (complex) Hermitian matrix by Lanczos-iterations.   *
!> This routine is used for improved relaxation and for meigenf.      *
!>                                                                    *
!> Input parameters:                                                  *
!>   Psi:       The (complex) initial-value vector.                   *
!>   DtPsi:     Action of Hamiltonian on the initial-value vector,    *
!>              i.e. H|Psi> or -i H|Psi>, depending on "StdForm".     *
!>   PsiDim     Length of Psi and DtPsi vectors.                      *
!>   IntPeriod: Lenght of time interval to be integrated.             *
!>   IntOrder:  Maximum integration order.                            *
!>   TolError:  Maximum error that is tolerated.                      *
!>   StdForm:   If true, Func(Time,Psi,DtPsi) = -i H|Psi>, else       *
!>              Func(Time,Psi,DtPsi) = H|Psi>.                        *
!>   Steps:     Number of steps made so far (Steps is passed to       *
!>              "WriteStep").                                         *
!>   Krylov:    Matrix with minimum size PsiDim*(IntOrder-1) with     *
!>              columns containing the Krylov vectors H^2|psi> to     *
!>              H^TrueOrder|psi>.                                     *
!>   TrueOrder: On input: the minimal order to be used.               *
!>   ortho:     if .true., then a full re-orthogonalisation of the    *
!>              Krylov vectors is performed.                          *
!>                                                                    *
!> Output parameters:                                                 *
!>   Psi:       Computed eigenvector. The number of the eigenvector   *
!>              to be chosen is specified by rlaxnum.                 *
!>   DtPsi:     Normalised first Krylov vector, i. e. (H-<H>)|Psi>.   *
!>   Krylov:    Matrix with minimum size PsiDim*(IntOrder-1) with     *
!>              columns containing the Krylov vectors H^2|psi> to     *
!>              H^TrueOrder|psi>.                                     *
!>   TrueOrder: The order that has actually been used (may be less    *
!>              than IntOrder).                                       *
!>   Steps:     Same as on entry.                                     *
!>   ErrorCode: Error code having the following meaning:              *
!>              0: everything was o. k.,                              *
!>              1: illegal integration order,                         *
!>              3: diagonalization failed,                            *
!>                                                                    *
!>                                                                    *
!> External routines:                                                 *
!>   Func:      Computes the action of the Hamiltonian on Psi.        *
!>              Called as                                             *
!>              Func(AbsTime,Psi,DtPsi,CData,RData,IData,LData).      *
!>                                                                    *
!> Some new Variables:                                                *
!>  rlaxnum : This is the argument of the keyword relaxation.         *
!>            If relaxation has no argument, rlaxnum=-1 .             *
!>                                                                    *
!> V8.3 10/03 HDM                                                     *
!>*********************************************************************
  
      Subroutine SILdiag(Psi,DtPsi,PsiDim,IntOrder,TolError,StdForm,
     +                   Steps,ErrorCode,rlaxnum,Time,
     +                   ortho,Func,minorder)
  
      Implicit None
  

      Logical    StdForm,FirstCall,full,ortho
      Integer    PsiDim,IntOrder,Steps,ErrorCode,rlaxnum
      Integer*8  TrueOrder
      Real*8     TolError,Stepsize,Time,beta1
      Complex*16 Psi(PsiDim),DtPsi(PsiDim),Krylov(PsiDim,2:IntOrder)
      External   Func


      Integer    minorder,D,P,Q,i,nd,relaxnum
      Real*8     Beta,Error,mxvc,err1,Tinit,
     +           EigenVector(0:IntOrder,0:IntOrder+2),
     +           Diagonal(0:IntOrder),OffDiag(IntOrder),
     +           EigenVal(0:IntOrder),OffDg2(IntOrder+1)
      Complex*16 Alpha,CBeta,PreFactor,CInverse,ovlp

      data       FirstCall/.true./
      Save       FirstCall, full
      Save       Tinit,relaxnum

C --- CHECK INTEGRATION ORDER ---

      If ( IntOrder .LT. 2 ) Then
         ErrorCode = 1
         write(6,'(a,i4,a)') 'IntOrder =',IntOrder,' is too small!'
         Return
      EndIf

C --- INITIALIZE VARIABLES ---

      nd = 2
      Stepsize = 0.d0
      ErrorCode = 0
      full = .false.
      if(FirstCall) Then
         Tinit = Time
         
         if(rlaxnum .ge. 9999) then
            full = .true.
            relaxnum = rlaxnum - 10000
            nd = IntOrder+1
         else
            relaxnum = rlaxnum
            full = .false.
         endif
         If (relaxnum .lt. 0) Then
            Write(6,*) ' relaxnum =',relaxnum, ' is .lt. 0. STOP'
            stop
         EndIf
      endif
      If (.not.StdForm) Then
         PreFactor = (1.0D0,0.0D0)
      Else
         PreFactor = (-1.0D0,0.0D0)
      EndIf

C --- FURTHER INITIALIZE VARIABLES ---

      Alpha = (0.0D0,0.0D0)
      Beta = 0.0D0
      TrueOrder = 1

C --- COMPUTE FIRST DIAGONAL ELEMENT ---
      Do D = 1,PsiDim
         Alpha = Alpha+DConjg(Psi(D))*DtPsi(D)
      EndDo
      Diagonal(0) = Alpha/PreFactor

C --- DETERMINE CORRESPONDING BASIS VECTOR AND OFF-DIAGONAL ELEMENT ---

      Do D = 1,PsiDim
         DtPsi(D) = DtPsi(D)-Alpha*Psi(D)
         Beta = Beta+Dble(DConjg(DtPsi(D))*DtPsi(D))
      EndDo

C --- Re-orthogonalize DtPsi
      if(ortho) then
         ovlp = (0.d0,0.d0)
         Do D = 1,PsiDim
            ovlp = ovlp + DConjg(Psi(D))*DtPsi(D)
         enddo
         Beta = 0.0D0
         Do D = 1,PsiDim
            DtPsi(D)=DtPsi(D)-ovlp*Psi(D)
            Beta = Beta+Dble(DConjg(DtPsi(D))*DtPsi(D))
         enddo
      endif

      Beta = Sqrt(Beta)
      OffDiag(1) = Beta
      beta1 = Beta
      CBeta = PreFactor*Beta

C --- NORMALIZE BASIS VECTOR ---

      If (Beta .LT. 1.d-20) Then
         Beta = 0.0D0
         CInverse = (0.0D0,0.0D0)
      Else
         CInverse = 1.0D0/CBeta
      EndIf
      Do D = 1,PsiDim
         DtPsi(D) = CInverse*DtPsi(D)
      EndDo

C --- COMPUTE ERROR ESTIMATE ---

      Error = Beta 

C --- WRITE STEPSIZE AND ERROR IF DESIRED ---

!!      Call WriteStep(Steps,TrueOrder,Stepsize,Error,Time)

C --- BUILD UP THE KRYLOV SPACE ---
 
 100  Continue
        
C --- RE-INITIALIZE VARIABLES ---

      Alpha = (0.0D0,0.0D0)
      Beta = 0.0D0
      TrueOrder = TrueOrder+1


C --- EVALUATE FUNCTION WITH LAST BASIS VECTOR ---

      If (TrueOrder .Eq. 2) Then
         Call Func(Time,DtPsi,Krylov(1,2))
      Else
         Call Func(Time,Krylov(1,TrueOrder-1),Krylov(1,TrueOrder))
      EndIf

C --- COMPUTE DIAGONAL ELEMENT ---

C Note that the Krylov vectors number zero and one aren't stored in
C "Krylov" but in "Psi" and "DtPsi", respectively.

      If (TrueOrder .Eq. 2) Then
         Do D = 1,PsiDim
            Alpha = Alpha+DConjg(DtPsi(D))*Krylov(D,2)
         EndDo
      Else
         Do D = 1,PsiDim
            Alpha = Alpha+DConjg(Krylov(D,TrueOrder-1))
     +              *Krylov(D,TrueOrder)
         EndDo
      EndIf
      Diagonal(TrueOrder-1) = Alpha/PreFactor

C --- COMPUTE OFF-DIAGONAL ELEMENT AND BASIS VECTOR ---

      If (TrueOrder .Eq. 2) Then
         Do D = 1,PsiDim
            Krylov(D,2) = Krylov(D,2)-Alpha*DtPsi(D)-CBeta*Psi(D)
            Beta = Beta+Dble(DConjg(Krylov(D,2))*Krylov(D,2))
         EndDo
      ElseIf (TrueOrder .Eq. 3) Then
         Do D = 1,PsiDim
            Krylov(D,3) = Krylov(D,3)-Alpha*Krylov(D,2)-CBeta*DtPsi(D)
            Beta = Beta+Dble(DConjg(Krylov(D,3))*Krylov(D,3))
         EndDo
      Else
         Do D = 1,PsiDim
            Krylov(D,TrueOrder) = Krylov(D,TrueOrder)
     +         -Alpha*Krylov(D,TrueOrder-1)-CBeta*Krylov(D,TrueOrder-2)
            Beta = Beta+DConjg(Krylov(D,TrueOrder))*Krylov(D,TrueOrder)
         EndDo
      EndIf


C --- Re-Orthogonalize against Psi and DtPsi  ---
      ovlp =(0.d0,0.d0)
      Do D = 1,PsiDim
         ovlp = ovlp + DConjg(Psi(D))*Krylov(D,TrueOrder)
      enddo
      Do D = 1,PsiDim
         Krylov(D,TrueOrder)=Krylov(D,TrueOrder)-ovlp*Psi(D)
      enddo
      ovlp =(0.d0,0.d0)
      Do D = 1,PsiDim
         ovlp = ovlp + DConjg(DtPsi(D))*Krylov(D,TrueOrder)
      enddo
      Do D = 1,PsiDim
         Krylov(D,TrueOrder)=Krylov(D,TrueOrder)-ovlp*DtPsi(D)
      enddo


C --- Re-Orthogonalize against Krylov  ---
      if((ortho.or.full) .and. TrueOrder .ge. 3) then
         Do i = 2, TrueOrder-1
            ovlp =(0.d0,0.d0)
            Do D = 1,PsiDim
               ovlp = ovlp + DConjg(Krylov(D,i))*Krylov(D,TrueOrder)
            enddo
            Do D = 1,PsiDim
               Krylov(D,TrueOrder) = Krylov(D,TrueOrder) - 
     +                 ovlp*Krylov(D,i)
            enddo
         enddo

C --- Determine Beta
         Beta = 0.d0
         Do D = 1,PsiDim
            Beta = Beta+DConjg(Krylov(D,TrueOrder))*
     +              Krylov(D,TrueOrder)
         enddo
      endif

      Beta = Sqrt(Beta)
      OffDiag(TrueOrder) = Beta
      CBeta = PreFactor*Beta

C --- NORMALIZE BASIS VECTOR ---

      If (Beta .LT. 1.d-20) Then
         OffDiag(TrueOrder) = 0.0D0
         Beta = 0.0D0
         CInverse = (0.0D0,0.0D0)
      Else
         CInverse = 1.0D0/CBeta
      EndIf
      
      Do D = 1,PsiDim
         Krylov(D,TrueOrder) = CInverse*Krylov(D,TrueOrder)
      EndDo

C --- COMPUTE ERROR ESTIMATE ---
      
      Error = Error*Beta/(0.5d0*Abs(alpha-Diagonal(0))+Beta)

C --- WRITE STEPSIZE AND ERROR IF DESIRED ---

!!      Call WriteStep(Steps,TrueOrder,Stepsize,Error,Time)

C --- CONTINUE IF NOT CONVERGED ---
      
      If((TrueOrder.LT.IntOrder) .And. (full.or.(Error.GT.TolError))
     +   .and. Beta.gt.1.d-20 .or. TrueOrder.LT.MinOrder )  Goto 100
            
C --- ESTIMATE NEXT MATRIX ELEMENT ---

c     Diagonal(TrueOrder) = Diagonal(TrueOrder-1)
      Diagonal(TrueOrder) = Diagonal(TrueOrder-1) + beta


C --- SET nd TO AVOID EXCESSIVE TRIAL DIAGONALISATIONS ---
      
      if(TrueOrder.LT.IntOrder .And. 
     +     Beta.gt.1.d-20 .And.  mod(TrueOrder,nd) .ne. 0 ) goto 100
      if(TrueOrder.ge. 20) nd = 5      !  This is to avoid excessive
      if(TrueOrder.ge.100) nd = 10     !  trial diagonalisations.
      if(TrueOrder.ge.200) nd = 20   
      if(TrueOrder.ge.300) nd = 40  
      if(TrueOrder.ge.500) nd = 80  

C --- Save Diagonal and off diagonal values since the diagonalisation ---
C --- routine will overwrite those. ---

      call  cpvxd(Diagonal,EigenVal,TrueOrder+1)
      call  cpvxd(OffDiag,OffDg2,TrueOrder)

C --- DIAGONALIZE LANCZOS MATRIX --- CALL LAPACK ROUTINE --- 
C --- EigenVector(*,IntOrder+1) serves as work array 
      if(beta.lt.1.d-18) TrueOrder=TrueOrder-1
      call DSTEQR('I',TrueOrder+1,EigenVal,OffDg2,EigenVector,
     +            IntOrder+1,EigenVector(1,IntOrder+1),ErrorCode)

      If (ErrorCode .NE. 0) Then
         ErrorCode = 3
         Return
      EndIf



C --- RELAX WAVEFUNCTION TO EIGENSTATE NO. Q ---
C --- SEARCH FOR THE EIGENVECTOR THAT IS CLOSEST TO PSI ---
      mxvc = 0.d0
      Do P = 0,TrueOrder
         OffDg2(p+1) = EigenVector(0,P)**2
         If(OffDg2(p+1) .gt. mxvc ) Then
            mxvc = OffDg2(p+1)
            Q = P
         EndIf
      EndDo
      
      if(time.eq.tinit .and. relaxnum.le.900) 
     +     Q = min(relaxnum,TrueOrder) 
      err1  = 1000.d0*Beta*EigenVector(TrueOrder,Q)**2
      if(.not.StdForm)               ! for meigenf
     + err1  = err1 + 100.d0*Beta*EigenVector(TrueOrder,minorder/4)**2
     +              + 100.d0*Beta*EigenVector(TrueOrder,minorder/2)**2 
      error = max(error,err1)
         
      if( TrueOrder.LT.IntOrder .And. Beta.gt.1.d-20 
     +     .And. err1.gt.TolError ) goto 100

C --- Build new A-vector.
      Do D = 1,PsiDim
         Psi(D) = EigenVector(0,Q)*Psi(D)+EigenVector(1,Q)*DtPsi(D)
      EndDo
      Do P = 2,TrueOrder
         Do D = 1,PsiDim
            Psi(D) = Psi(D)+EigenVector(P,Q)*Krylov(D,P)
         EndDo
      EndDo
      
C --- Write to rlx_info file. EigenVector(*,IntOrder+1) is a work array,
C     containing the error-estimates.     
      if(FirstCall) then
         do P = 0,TrueOrder
            EigenVector(p,IntOrder+1) = 
     +           2.d0*27.2114d0*Beta*EigenVector(TrueOrder,p)**2
C........The extra factor of 2 is due to the modification, line 358.  
            if(EigenVector(p,IntOrder+1).lt.1.d-30)
     +           EigenVector(p,IntOrder+1) = 0.d0
         enddo
      else
         EigenVector(Q,IntOrder+1) =
     +        2.d0*27.2114d0*Beta*EigenVector(TrueOrder,Q)**2
      endif
!      call wrtrlx(Q,TrueOrder,mxvc,beta1,time,EigenVal,
!     +     EigenVector(0,IntOrder+1),OffDg2,psi,FirstCall,StdForm)
      if(StdForm) then
         FirstCall = .false.    ! Normal case, improved relaxation
      else
         steps = Q              ! This is for meigenf
         do p =0, IntOrder
            EigenVector(p,IntOrder+2)=EigenVal(p)
         enddo 
      endif

      Return
      End Subroutine SILdiag

      END MODULE SIL_Integrators
