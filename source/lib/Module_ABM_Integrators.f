!>GPLv3
!    MCTDH-X: the multiconfigurational time-dependent Hartree for 
!    indistinguishable particles software
!
!    Copyright (C) 2021  A. U. J. Lode, M. C. Tsatsos, 
!                        E. Fasshauer, S. E. Weiner, 
!                        R. Lin, L. Papariello, P. Molignini, 
!                        C. Lévêque, M. Büttner , J. Xiang, S. Dutta
!
!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <https://www.gnu.org/licenses/>.
!
!
!
!
!<GPLv3










!> This module contains the different ADAMS-BASHFORTH-MOULTON integrators.
       MODULE ABM_Integrators
       CONTAINS
!>           OPENMP-ADAMS-BASHFORTH-MOULTON                          *
!>                                                                   *
!>Library module containing an Adams-Bashforth-Moulton predictor-    *
!>corrector integrator.                                              *
!>                                                                   *
!>Contains:                                                          *
!>  ABM:          The ABM integration routine.                       *
!>  AbsABMError:  Computes the absolute error of the current ABM     *
!>                integration step.                                  *
!>  RelABMError:  Computes the relative error of the current ABM     *
!>                integration step.                                  *
!>  WriteABMStep: In the current form WriteABMStep is just a dummy   *
!>                routine doing absolutely nothing; it is included   *
!>                here for formal reasons, but can (if desired) be   *
!>                extended easily such that it writes the size and   *
!>                error of the current integration step to a file.   *
!>  ABMErrorMsg:  Returns for a given error number a corresponding   *
!>               error message.                                     *
!>                                                                   *
!>********************************************************************
!>
!>
!>********************************************************************
!>                                                                   *
!>                          SUBROUTINE OMPABM                        *
!>                                                                   *
!>Integrates a system of complex first order differential equations  *
!>employing the Adams-Bashforth-Moulton predictor-corrector method.  *
!>The ABM routine runs with variable step sizes and (except for the  *
!>beginning) a fixed integration order. The ODE is of the form       *
!>dPsi/dt = Func(AbsTime,Psi) =: DtPsi. All computations are         *
!>performed with double precision. The routine allows a previous     *
!>integration to be resumed (e. g. after the integration had been    *
!>stopped to write Psi to a file), as long as the AuxPsi array       *
!>hasn't been overwritten and the integration order and error        *
!>tolerance hasn't been changed. To do so simply set the restart     *
!>flag "RestartABM" to ".false.".                                    *
!>                                                                   *
!>Input parameters:                                                  *
!>  Psi:           The (complex) initial-value vector.               *
!>  DtPsi:         Time derivative of the initial-value vector (must *
!>                 be passed to the integrator regardless of the     *
!>                 flag "RestartABM").                               *
!>  PsiDim         Length of Psi and DtPsi vectors.                  *
!>  IntPeriod:     Lenght of time interval to be integrated.         *
!>  AbsTime:       Absolute time, i. e. Psi_initial=Psi(AbsTime).    *
!>  IntOrder:      Desired integration order.                        *
!>  InitStep:      Size of first integration step.                   *
!>  TolError:      Maximum error that is tolerated.                  *
!>  RestartABM:    Restart flag; if false, a previous integration is *
!>                  continued, otherwise a new integration is started *
!>                  (see comment above for details).                  *
!>                                                                    *
!> Output parameters:                                                 *
!>   Psi:           Solution of the ODE at time AbsTime+IntPeriod.    *
!>   DtPsi:         DtPsi is undetermined.                            *
!>   Steps:         Counter for the overall number of integration     *
!>                  steps. (Note that Steps isn't set to zero at the  *
!>                  beginning.)                                       *
!>   RepeatedSteps: Counter for the number of failed and thus         *
!>                  repeated integration steps. (Note that            *
!>                  RepeatedSteps isn't set to zero at the            *
!>                  beginning.)                                       *
!>   ErrorCode:     Error code having the following meaning:          *
!>                  0: everything was o. k.,                          *
!>                  1: illegal integration order,                     *
!>                 2: stepsize underflow.                            *
!>                                                                   *
!>Other parameters:                                                  *
!>  AuxPsi:        Auxiliary array of (minimum) size                 *
!>                 PsiDim*(IntOrder+1).                              *
!>                                                                   *
!>External routines:                                                 *
!>  Func:          Computes the time derivative DtPsi of Psi at time *
!>                 AbsTime. Called as Func(AbsTime,Psi,DtPsi).       *
!>  CalcError:     Determines the error of the current integration   *
!>                 step. Called as CalcError(Predicted_Psi,          *
!>                 Corrected_Psi,PsiDim,Error,).                     *
!>  WriteStep:     Writes the stepsize and error to a file.          *
!>                 Called as WriteStep(Step,Order,Stepsize,Error).   *
!>                                                                   *
!>V6.0 MB                                                            *
!>                                                                   *
!>********************************************************************
      Subroutine OMPABM (Psi,DtPsi,PsiDim,IntPeriod,AbsTime,IntOrder,
     +                InitStep,TolError,RestartABM,Steps,RepeatedSteps,
     +                ErrorCode,AuxPsi,Func)
        use Global_Parameters
 
      Implicit None
 
      Real*8    One6th,One30th,One210th,RelativeMinStep
      Integer   MaxOrder
      Parameter (One6th = 1.0D0/6.0D0,One30th = 1.0D0/30.0D0,
     +           One210th = 1.0D0/210.0D0,RelativeMinStep = 1.0D-12,
     +           MaxOrder = 8)

      Logical    RestartABM
      Integer    PsiDim,IntOrder,Steps,RepeatedSteps,ErrorCode
      Real*8     IntPeriod,AbsTime,InitStep,TolError
      Complex*16 Psi(PsiDim),DtPsi(PsiDim),AuxPsi(PsiDim,IntOrder+1)
      External   Func
      
      REAL*8     singleerror,boundary

      Logical    StepIsRepeated,CurOrdEqIntOrd,UseOldH1,conv
      Integer    CurrentOrder,D,P,I,K
      Real*8     Time,NextTime,IntError,MinError,Error,D2,D3,D4,D5,D6,
     +           D7,H1,H1Sqr,MinusH1Cube,MinStepSize,Distance(MaxOrder),
     +           H(MaxOrder-1),PredCoef(MaxOrder),CorrCoef(MaxOrder),
     +           SumOfD(2:MaxOrder-2,2:MaxOrder-1),
     +           ProdOfD(2:MaxOrder-2,2:MaxOrder-1)
      Complex*16 InterimAuxPsi(MaxOrder)
      
      Save       IntError,Error,MinError,H,Distance,CurrentOrder,
     +           CurOrdEqIntOrd,StepIsRepeated,UseOldH1


         conv = .False.
C --- CHECK INTEGRATION ORDER ---

      If (IntOrder .GT. MaxOrder) Then
         ErrorCode = 1
         Return
      EndIf

C --- INITIALISE VARIABLES ---

      ErrorCode = 0
      Time = AbsTime
      MinStepSize = RelativeMinStep*IntPeriod
      
C --- CONTINUE INTEGRATION ---

      If (.Not. RestartABM) Then
         Goto 200
      EndIf

C --- INITIALISE VARIABLES ---

C "UseOldH1" is a flag set to true if the step size is artificially
C shortened in order to fit it to the remaining integration period
C before leaving the routine. This allows the next stepsize to be
C corrected back to the previous value.

      IntError = 0.40D0*TolError
      MinError = 0.01D0*TolError
      Do P = 1,IntOrder-1
         H(P) = InitStep
      EndDo
      CurrentOrder = 2
      CurOrdEqIntOrd = .False.
      StepIsRepeated = .False.
      UseOldH1 = .False.

C --- INITIALISE AUXPSI ---

!$OMP PARALLEL DO 
!$OMP& FIRSTPRIVATE(Psidim)
      Do D = 1,PsiDim
         AuxPsi(D,1) = Psi(D)
         AuxPsi(D,2) = DtPsi(D)
      EndDo
!$OMP END PARALLEL DO

C --- INTEGRATION LOOP ---

 100  Continue

C --- CHECK WETHER STEPSIZE IS TOO SMALL ---

      If ((H(1) .LT. MinStepSize) .And.
     +   (H(1) .LT. AbsTime+IntPeriod-Time)) Then
         ErrorCode = 2
        write(1003,*)InitStep," ABM STEp",H(1),MinStepSize
        write(1003,*)InitStep," ABM STEp",AbsTime+IntPeriod-Time
         Return
      EndIf

C --- CALCULATE PREDICTOR AND CORRECTOR COEFFICIENTS ---

      Do P = 1,CurrentOrder
         If (P .Eq. 1) Then
            H1          = H(1)
            Distance(1) = H1
            PredCoef(1) = H1
            CorrCoef(1) = H1
         ElseIf (P .Eq. 2) Then
            H1Sqr       = H1*H1
            Distance(2) = Distance(1)+H(2)
            PredCoef(2) = 0.5*H1Sqr
            CorrCoef(2) = -PredCoef(2)
         ElseIf (P .Eq. 3) Then
            MinusH1Cube = -H1Sqr*H1
            D2          = H(2)
            Distance(3) = Distance(2)+H(3)
            PredCoef(3) = One6th*H1Sqr*(3.0*D2+2.0*H1)
            CorrCoef(3) = One6th*MinusH1Cube
         ElseIf (P .Eq. 4) Then
            D3          = D2+H(3)
            Distance(4) = Distance(3)+H(4)
            SumOfD(2,3) = D2+D3
            ProdOfD(2,3) = D2*D3
            PredCoef(4) = One6th*H1Sqr*(3.0*ProdOfD(2,3)
     +                    +(2.0*SumOfD(2,3)+1.5*H1)*H1)
            CorrCoef(4) = One6th*MinusH1Cube*(D2+0.5*H1)
         ElseIf (P .Eq. 5) Then
            D4          = D3+H(4)
            Distance(5) = Distance(4)+H(5)
            SumOfD(3,3) = D3
            ProdOfD(3,3) = D3
            Do I = 2,3
               SumOfD(I,4)  = SumOfD(I,3)+D4
               ProdOfD(I,4) = ProdOfD(I,3)*D4
            EndDo
            PredCoef(5) = One30th*H1Sqr*(15.0*ProdOfD(2,4)
     +                    +(10.0*(D2*SumOfD(3,4)+ProdOfD(3,4))
     +                    +(7.5*SumOfD(2,4)+6.0*H1)*H1)*H1)
            CorrCoef(5) = One30th*MinusH1Cube*(5.0*ProdOfD(2,3)
     +                    +(2.5*SumOfD(2,3)+1.5*H1)*H1)
         ElseIf (P .Eq. 6) Then
            D5          = D4+H(5)
            Distance(6) = Distance(5)+H(6)
            SumOfD(4,4) = D4
            ProdOfD(4,4) = D4
            Do I = 2,4
               SumOfD(I,5)  = SumOfD(I,4)+D5
               ProdOfD(I,5) = ProdOfD(I,4)*D5
            EndDo
            PredCoef(6) = One30th*H1Sqr*(15.0*ProdOfD(2,5)
     +                    +(10.0*(D2*(D3*SumOfD(4,5)+ProdOfD(4,5))
     +                    +ProdOfD(3,5))
     +                    +(7.5*(D2*SumOfD(3,5)+D3*SumOfD(4,5)
     +                    +ProdOfD(4,5))
     +                    +(6.0*SumOfD(2,5)+5.0*H1)*H1)*H1)*H1)
            CorrCoef(6) = One30th*MinusH1Cube*(5.0*ProdOfD(2,4)
     +                    +(2.5*(D2*SumOfD(3,4)+ProdOfD(3,4))
     +                    +(1.5*SumOfD(2,4)+H1)*H1)*H1)
         ElseIf (P .Eq. 7) Then
            D6          = D5+H(6)
            Distance(7) = Distance(6)+H(7)
            SumOfD(5,5) = D5
            ProdOfD(5,5) = D5
            Do I = 2,5
               SumOfD(I,6)  = SumOfD(I,5)+D6
               ProdOfD(I,6) = ProdOfD(I,5)*D6
            EndDo
            PredCoef(7) = One210th*H1Sqr*(105.0*ProdOfD(2,6)
     +                    +(70.0*(D2*(D3*(D4*SumOfD(5,6)+ProdOfD(5,6))
     +                    +ProdOfD(4,6))+ProdOfD(3,6))
     +                    +(52.5*(D2*(D3*SumOfD(4,6)+D4*SumOfD(5,6)
     +                    +ProdOfD(5,6))+D3*(D4*SumOfD(5,6)
     +                    +ProdOfD(5,6))+ProdOfD(4,6))
     +                    +(42.0*(D2*SumOfD(3,6)+D3*SumOfD(4,6)
     +                    +D4*SumOfD(5,6)+ProdOfD(5,6))
     +                    +(35.0*SumOfD(2,6)+30.0*H1)*H1)*H1)*H1)*H1)
         CorrCoef(7) = One210th*MinusH1Cube*(35.0*ProdOfD(2,5)
     +                    +(17.5*(D2*(D3*SumOfD(4,5)+ProdOfD(4,5))
     +                    +ProdOfD(3,5))
     +                    +(10.5*(D2*SumOfD(3,5)+D3*SumOfD(4,5)
     +                    +ProdOfD(4,5))+(7.0*SumOfD(2,5)
     +                    +5.0*H1)*H1)*H1)*H1)
         ElseIf (P .Eq. 8) Then
            D7          = D6+H(7)
            SumOfD(6,6) = D6
            ProdOfD(6,6) = D6
            Do I = 2,6
               SumOfD(I,7)  = SumOfD(I,6)+D7
               ProdOfD(I,7) = ProdOfD(I,6)*D7
            EndDo
            PredCoef(8) = One210th*H1Sqr*(105.0*ProdOfD(2,7)
     +                    +(60.0*(D2*(D3*(D4*(D5*SumOfD(6,7)
     +                    +ProdOfD(6,7))+ProdOfD(5,7))+ProdOfD(4,7))
     +                    +ProdOfD(3,7))
     +                    +(52.5*(D2*(D3*(D4*SumOfD(5,7)+D5*SumOfD(6,7))
     +                    +ProdOfD(6,7))+D4*(D5*SumOfD(6,7)
     +                    +ProdOfD(6,7))+ProdOfD(5,7))
     +                    +D3*(D4*(D5*SumOfD(6,7)+ProdOfD(6,7)
     +                    +ProdOfD(5,7))+ProdOfD(4,7))
     +                    +(42.0*(D2*(D3*SumOfD(4,7)+D4*SumOfD(5,7)
     +                    +D5*SumOfD(6,7)+ProdOfD(6,7))
     +                    +D3*(D4*SumOfD(5,7)+D5*SumOfD(6,7)
     +                    +ProdOfD(6,7))+D4*(D5*SumOfD(6,7)
     +                    +ProdOfD(6,7))+ProdOfD(5,7))
     +                    +(35.0*(D2*SumOfD(3,7)+D3*SumOfD(4,7)
     +                    +D4*SumOfD(5,7)+D5*SumOfD(6,7)+ProdOfD(6,7))
     +                    +(30.0*SumOfD(2,7)
     +                    +26.25*H1)*H1)*H1)*H1)*H1)*H1)
            CorrCoef(8) = One210th*MinusH1Cube*(35.0*ProdOfD(2,6)
     +                    +(17.5*(D2*(D3*(D4*SumOfD(5,6)+ProdOfD(5,6))
     +                    +ProdOfD(4,6))+ProdOfD(3,6))
     +                    +(10.5*(D2*(D3*SumOfD(4,6)+D4*SumOfD(5,6)
     +                    +ProdOfD(5,6))+D3*(D4*SumOfD(5,6)
     +                    +ProdOfD(5,6))+ProdOfD(4,6))
     +                    +(7.0*(D2*SumOfD(3,6)+D3*SumOfD(4,6)
     +                    +D4*SumOfD(5,6)+ProdOfD(5,6))+(5.0*SumOfD(2,6)
     +                    +3.75*H1)*H1)*H1)*H1)*H1)
         Else
            ErrorCode = 1
            Return
         EndIf
      EndDo

C --- RESTORE ORIGINAL PSI WHEN STEP IS REPEATED ---

      If (StepIsRepeated) Then
!$OMP PARALLEL DO 
!$OMP& FIRSTPRIVATE(Psidim)
         Do D = 1,PsiDim
            Psi(D) = AuxPsi(D,1)
         EndDo
!$OMP END PARALLEL DO
         StepIsRepeated = .False.
      EndIf

C --- PREDICT PSI ---	

C As long as the order p is increased, the predictor must be used as a
C p-step method. Later on, the predictor is run as a (p+1)-step method.
      If (CurOrdEqIntOrd) Then
         K=currentorder
      else
         K=currentorder-1
      endif

!$OMP PARALLEL DO 
!$OMP& PRIVATE(D,P)
!$OMP& SHARED(PSI,PSIDIM,PREDCOEF,AUXPSI,K)
      Do D = 1,PsiDim
          Do P = 1,K
            Psi(D) = Psi(D)+PredCoef(P)*AuxPsi(D,P+1)
         EndDo
      EndDo
!$OMP END PARALLEL DO

      NextTime = Time+H(1)
 
      Call Func(NextTime,Psi,DtPsi)

C --- SAVE PREDICTED PSI IN DTPSI AND CORRECT PSI ---

!$OMP PARALLEL 
      Boundary = 1.0D0
      Error = 0.0D0
!$OMP DO REDUCTION(-:INTERIMAUXPSI) REDUCTION(MAX:ERROR)
!$OMP& FIRSTPRIVATE(PsiDim,CurrentOrder,Distance)
      Do D = 1,PsiDim
         InterimAuxPsi(1) = DtPsi(D)
         Do P = 2,CurrentOrder
            InterimAuxPsi(P) = (InterimAuxPsi(P-1)-AuxPsi(D,P))
     +                         /Distance(P-1)
         enddo
         DtPsi(D) = Psi(D)
         Psi(D) = AuxPsi(D,1)
         Do P = 1,CurrentOrder
            psi(d) = psi(d)+CorrCoef(P)*InterimAuxPsi(P)
         EndDo
C --- COMPUTE ERROR OF BOTH REAL AND IMAGINARY PART ---
         SingleError = DAbs(Dble(DtPsi(D))-Dble(Psi(D)))
         If (DAbs(Dble(Psi(D))) .GT. Boundary) Then
             SingleError = SingleError/DAbs(Dble(Psi(D)))
         EndIf
         Error = Max(Error,SingleError)
         SingleError = DAbs(DImag(DtPsi(D))-DImag(Psi(D)))
         If (DAbs(DImag(Psi(D))) .GT. Boundary) Then
           SingleError = SingleError/DAbs(DImag(Psi(D)))
         EndIf
         Error = Max(Error,SingleError)
      EndDo
!$OMP END DO
!$OMP END PARALLEL
      If (CurrentOrder .LT. IntOrder) Then
         Error = Error*(2*IntOrder+1.0D0)/(2*CurrentOrder+1.0D0)
      EndIf
      Steps = Steps+1
      If (Error .GT. TolError) Then
         H(1) = 0.8D0*H(1)*(IntError/Error)**(1.0D0/(CurrentOrder+1))
         RepeatedSteps = RepeatedSteps+1
         StepIsRepeated = .True.
         Goto 100
      EndIf

C --- WRITE STEPSIZE AND ERROR ---

Cstr      Call WriteStep(Steps,CurrentOrder,H(1),Error,Time)

C --- RETURN WHEN FINISHED ---

      Time = Time+H(1)
      If (Time .GE. AbsTime+IntPeriod-MinStepSize) Then
         Return
      EndIf
      If (conv .EQV. .True.) Then
         Return
      EndIf

C --- EVALUATE FUNCTION WITH CORRECTED PSI ---

      Call Func(NextTime,Psi,DtPsi)

 200  Continue

C --- INCREASE INTEGRATION ORDER ---

      If (CurrentOrder .LT. IntOrder) Then
         CurrentOrder = CurrentOrder+1
      Else
         CurOrdEqIntOrd = .True.
      EndIf

C --- COMPUTE NEW DEVIDED DIFFERENCES ---

C As long as the order p is increased, the predictor must be used as a
C p-step method. Later on, the predictor is run as a (p+1)-step method.
      If (CurOrdEqIntOrd) Then
         K=Currentorder+1
      Else
         K=Currentorder
      endif
!$OMP PARALLEL DO REDUCTION(-:INTERIMAUXPSI)
!$OMP& FIRSTPRIVATE(PsiDim,K,Distance)
      Do D = 1,PsiDim
            AuxPsi(D,1) = Psi(D)
            InterimAuxPsi(1) = DtPsi(D)
            Do P = 2,K-1
               InterimAuxPsi(P) = (InterimAuxPsi(P-1)-AuxPsi(D,P))
     +                            /Distance(P-1)
            EndDo
            Do P = 2,K
               AuxPsi(D,P) = InterimAuxPsi(P-1)
            EndDo
      EndDo
!$OMP END PARALLEL DO

C --- CALCULATE NEW STEP SIZE ---

C If the previous step had been shortened (see above), the next stepsize
C may be set (close) to the stepsize before that one.

      Error = Max(Error,MinError)
      Do P = CurrentOrder-1,2,-1
         H(P) = H(P-1)
      EndDo
      H(1) = H(1)*(IntError/Error)**(1.0D0/(CurrentOrder+1))
      If (UseOldH1) Then
         If (H(1) .LT. 0.98D0*H(3)) Then
            H(1) = 0.98D0*H(3)
         EndIf
         UseOldH1 = .False.
      EndIf
      If (Time+H(1) .GT. AbsTime+IntPeriod) Then
         H(1) = AbsTime+IntPeriod-Time
         UseOldH1 = .True.
         conv = .True.
      EndIf

C --- CONTINUE INTEGRATION ---

      Goto 100
      
      End subroutine ompabm



!> Adams-Bashforth-Moulton Predictor-Corrector integration routine of MCTDH.
      Subroutine ABM (Psi,DtPsi,PsiDim,IntPeriod,AbsTime,IntOrder,
     +                InitStep,TolError,RestartABM,Steps,RepeatedSteps,
     +                ErrorCode,AuxPsi,Func,CalcError)
        use Global_Parameters

      Implicit None

      Real*8    One6th,One30th,One210th,RelativeMinStep
      Integer   MaxOrder
      Parameter (One6th = 1.0D0/6.0D0,One30th = 1.0D0/30.0D0,
     +           One210th = 1.0D0/210.0D0,RelativeMinStep = 1.0D-20,
     +           MaxOrder = 8)

      Logical    RestartABM
      Integer    PsiDim,IntOrder,Steps,RepeatedSteps,ErrorCode
      Real*8     IntPeriod,AbsTime,InitStep,TolError
      Complex*16 Psi(PsiDim),DtPsi(PsiDim),AuxPsi(PsiDim,IntOrder+1)
      External   Func,CalcError

      Logical    StepIsRepeated,CurOrdEqIntOrd,UseOldH1,conv
      Integer    CurrentOrder,D,P,I
      Real*8     Time,NextTime,IntError,MinError,Error,D2,D3,D4,D5,D6,
     +           D7,H1,H1Sqr,MinusH1Cube,MinStepSize,Distance(MaxOrder),
     +           H(MaxOrder-1),PredCoef(MaxOrder),CorrCoef(MaxOrder),
     +           SumOfD(2:MaxOrder-2,2:MaxOrder-1),
     +           ProdOfD(2:MaxOrder-2,2:MaxOrder-1),
     +           Cavity_Error_Stored           
      Complex*16 InterimAuxPsi(MaxOrder)


      Save       IntError,Error,MinError,H,Distance,CurrentOrder,
     +           CurOrdEqIntOrd,StepIsRepeated,UseOldH1,
     +           Cavity_Error_Stored           

         conv = .False.

      If (IntOrder .GT. MaxOrder) Then
         ErrorCode = 1
         Return
      EndIf


      ErrorCode = 0
      Time = AbsTime
      MinStepSize = RelativeMinStep*IntPeriod
      

      If (.Not. RestartABM) Then
         Goto 200
      EndIf



      IntError = 0.40D0*TolError
      MinError = 0.01D0*TolError
      Do P = 1,IntOrder-1
         H(P) = InitStep
      EndDo
      CurrentOrder = 2
      CurOrdEqIntOrd = .False.
      StepIsRepeated = .False.
      UseOldH1 = .False.


      Do D = 1,PsiDim
         AuxPsi(D,1) = Psi(D)
         AuxPsi(D,2) = DtPsi(D)
      EndDo


 100  Continue


      If ((H(1) .LT. MinStepSize) .And.
     +   (H(1) .LT. AbsTime+IntPeriod-Time)) Then
         ErrorCode = 2
         write(1003,*)InitStep," ABM STEp",H(1),MinStepSize
         write(1003,*)InitStep," ABM STEp",AbsTime+IntPeriod-Time

         IF (Cavity_BEC.neqv..TRUE.) THEN
          Return
         EndIf
      ENDIF

      Do P = 1,CurrentOrder
         If (P .Eq. 1) Then
            H1          = H(1)
            Distance(1) = H1
            PredCoef(1) = H1
            CorrCoef(1) = H1
         ElseIf (P .Eq. 2) Then
            H1Sqr       = H1*H1
            Distance(2) = Distance(1)+H(2)
            PredCoef(2) = 0.5*H1Sqr
            CorrCoef(2) = -PredCoef(2)
         ElseIf (P .Eq. 3) Then
            MinusH1Cube = -H1Sqr*H1
            D2          = H(2)
            Distance(3) = Distance(2)+H(3)
            PredCoef(3) = One6th*H1Sqr*(3.0*D2+2.0*H1)
            CorrCoef(3) = One6th*MinusH1Cube
         ElseIf (P .Eq. 4) Then
            D3          = D2+H(3)
            Distance(4) = Distance(3)+H(4)
            SumOfD(2,3) = D2+D3
            ProdOfD(2,3) = D2*D3
            PredCoef(4) = One6th*H1Sqr*(3.0*ProdOfD(2,3)
     +                    +(2.0*SumOfD(2,3)+1.5*H1)*H1)
            CorrCoef(4) = One6th*MinusH1Cube*(D2+0.5*H1)
         ElseIf (P .Eq. 5) Then
            D4          = D3+H(4)
            Distance(5) = Distance(4)+H(5)
            SumOfD(3,3) = D3
            ProdOfD(3,3) = D3
            Do I = 2,3
               SumOfD(I,4)  = SumOfD(I,3)+D4
               ProdOfD(I,4) = ProdOfD(I,3)*D4
            EndDo
            PredCoef(5) = One30th*H1Sqr*(15.0*ProdOfD(2,4)
     +                    +(10.0*(D2*SumOfD(3,4)+ProdOfD(3,4))
     +                    +(7.5*SumOfD(2,4)+6.0*H1)*H1)*H1)
            CorrCoef(5) = One30th*MinusH1Cube*(5.0*ProdOfD(2,3)
     +                    +(2.5*SumOfD(2,3)+1.5*H1)*H1)
         ElseIf (P .Eq. 6) Then
            D5          = D4+H(5)
            Distance(6) = Distance(5)+H(6)
            SumOfD(4,4) = D4
            ProdOfD(4,4) = D4
            Do I = 2,4
               SumOfD(I,5)  = SumOfD(I,4)+D5
               ProdOfD(I,5) = ProdOfD(I,4)*D5
            EndDo
            PredCoef(6) = One30th*H1Sqr*(15.0*ProdOfD(2,5)
     +                    +(10.0*(D2*(D3*SumOfD(4,5)+ProdOfD(4,5))
     +                    +ProdOfD(3,5))
     +                    +(7.5*(D2*SumOfD(3,5)+D3*SumOfD(4,5)
     +                    +ProdOfD(4,5))
     +                    +(6.0*SumOfD(2,5)+5.0*H1)*H1)*H1)*H1)
            CorrCoef(6) = One30th*MinusH1Cube*(5.0*ProdOfD(2,4)
     +                    +(2.5*(D2*SumOfD(3,4)+ProdOfD(3,4))
     +                    +(1.5*SumOfD(2,4)+H1)*H1)*H1)
         ElseIf (P .Eq. 7) Then
            D6          = D5+H(6)
            Distance(7) = Distance(6)+H(7)
            SumOfD(5,5) = D5
            ProdOfD(5,5) = D5
            Do I = 2,5
               SumOfD(I,6)  = SumOfD(I,5)+D6
               ProdOfD(I,6) = ProdOfD(I,5)*D6
            EndDo
            PredCoef(7) = One210th*H1Sqr*(105.0*ProdOfD(2,6)
     +                    +(70.0*(D2*(D3*(D4*SumOfD(5,6)+ProdOfD(5,6))
     +                    +ProdOfD(4,6))+ProdOfD(3,6))
     +                    +(52.5*(D2*(D3*SumOfD(4,6)+D4*SumOfD(5,6)
     +                    +ProdOfD(5,6))+D3*(D4*SumOfD(5,6)
     +                    +ProdOfD(5,6))+ProdOfD(4,6))
     +                    +(42.0*(D2*SumOfD(3,6)+D3*SumOfD(4,6)
     +                    +D4*SumOfD(5,6)+ProdOfD(5,6))
     +                    +(35.0*SumOfD(2,6)+30.0*H1)*H1)*H1)*H1)*H1)
         CorrCoef(7) = One210th*MinusH1Cube*(35.0*ProdOfD(2,5)
     +                    +(17.5*(D2*(D3*SumOfD(4,5)+ProdOfD(4,5))
     +                    +ProdOfD(3,5))
     +                    +(10.5*(D2*SumOfD(3,5)+D3*SumOfD(4,5)
     +                    +ProdOfD(4,5))+(7.0*SumOfD(2,5)
     +                    +5.0*H1)*H1)*H1)*H1)
         ElseIf (P .Eq. 8) Then
            D7          = D6+H(7)
            SumOfD(6,6) = D6
            ProdOfD(6,6) = D6
            Do I = 2,6
               SumOfD(I,7)  = SumOfD(I,6)+D7
               ProdOfD(I,7) = ProdOfD(I,6)*D7
            EndDo
            PredCoef(8) = One210th*H1Sqr*(105.0*ProdOfD(2,7)
     +                    +(60.0*(D2*(D3*(D4*(D5*SumOfD(6,7)
     +                    +ProdOfD(6,7))+ProdOfD(5,7))+ProdOfD(4,7))
     +                    +ProdOfD(3,7))
     +                    +(52.5*(D2*(D3*(D4*SumOfD(5,7)+D5*SumOfD(6,7))
     +                    +ProdOfD(6,7))+D4*(D5*SumOfD(6,7)
     +                    +ProdOfD(6,7))+ProdOfD(5,7))
     +                    +D3*(D4*(D5*SumOfD(6,7)+ProdOfD(6,7)
     +                    +ProdOfD(5,7))+ProdOfD(4,7))
     +                    +(42.0*(D2*(D3*SumOfD(4,7)+D4*SumOfD(5,7)
     +                    +D5*SumOfD(6,7)+ProdOfD(6,7))
     +                    +D3*(D4*SumOfD(5,7)+D5*SumOfD(6,7)
     +                    +ProdOfD(6,7))+D4*(D5*SumOfD(6,7)
     +                    +ProdOfD(6,7))+ProdOfD(5,7))
     +                    +(35.0*(D2*SumOfD(3,7)+D3*SumOfD(4,7)
     +                    +D4*SumOfD(5,7)+D5*SumOfD(6,7)+ProdOfD(6,7))
     +                    +(30.0*SumOfD(2,7)
     +                    +26.25*H1)*H1)*H1)*H1)*H1)*H1)
            CorrCoef(8) = One210th*MinusH1Cube*(35.0*ProdOfD(2,6)
     +                    +(17.5*(D2*(D3*(D4*SumOfD(5,6)+ProdOfD(5,6))
     +                    +ProdOfD(4,6))+ProdOfD(3,6))
     +                    +(10.5*(D2*(D3*SumOfD(4,6)+D4*SumOfD(5,6)
     +                    +ProdOfD(5,6))+D3*(D4*SumOfD(5,6)
     +                    +ProdOfD(5,6))+ProdOfD(4,6))
     +                    +(7.0*(D2*SumOfD(3,6)+D3*SumOfD(4,6)
     +                    +D4*SumOfD(5,6)+ProdOfD(5,6))+(5.0*SumOfD(2,6)
     +                    +3.75*H1)*H1)*H1)*H1)*H1)
         Else
            ErrorCode = 1
            Return
         EndIf
      EndDo


      If (StepIsRepeated) Then
         Do D = 1,PsiDim
            Psi(D) = AuxPsi(D,1)
         EndDo
         StepIsRepeated = .False.
      EndIf



      Do D = 1,PsiDim
         If (CurOrdEqIntOrd) Then
            Do P = 1,CurrentOrder
               Psi(D) = Psi(D)+PredCoef(P)*AuxPsi(D,P+1)
            EndDo
         Else
            Do P = 1,CurrentOrder-1
               Psi(D) = Psi(D)+PredCoef(P)*AuxPsi(D,P+1)
            EndDo
         EndIf
      EndDo
                      
      NextTime = Time+H(1)
      Call Func(NextTime,Psi,DtPsi)
      

      Do D = 1,PsiDim
         InterimAuxPsi(1) = DtPsi(D)
         Do P = 2,CurrentOrder
            InterimAuxPsi(P) = (InterimAuxPsi(P-1)-AuxPsi(D,P))
     +                         /Distance(P-1)
         EndDo
         DtPsi(D) = Psi(D)
         Psi(D) = AuxPsi(D,1)
         Do P = 1,CurrentOrder
            Psi(D) = Psi(D)+CorrCoef(P)*InterimAuxPsi(P)
         EndDo
      EndDo


      Call CalcError(DtPsi,Psi,PsiDim,Error)

      If (Cavity_BEC.eqv..TRUE.) THEN
! If the convergence is still bad after a given number of attempts, quit
          Cav_Integrator_Loop_Count = Cav_Integrator_Loop_Count + 1
          If (Cav_Integrator_Loop_Count.gt.Cav_Integrator_Loop_Max) Then
              ErrorCode = 2
              Return
          EndIf

! Detect a convergence to an error above tolerance
          If ((Error.gt.TolError).and.
     +         (abs(Cavity_Error_Stored-Error).lt.TolError)) Then
                  ErrorCode = 2
                  Return
          Else
                  Cavity_Error_Stored = Error
          EndIf
      ENDIF


      If (CurrentOrder .LT. IntOrder) Then
         Error = Error*(2*IntOrder+1.0D0)/(2*CurrentOrder+1.0D0)
      EndIf
      Steps = Steps+1
      If (Error .GT. TolError) Then
         H(1) = 0.8D0*H(1)*(IntError/Error)**(1.0D0/(CurrentOrder+1))
         RepeatedSteps = RepeatedSteps+1
         StepIsRepeated = .True.
         Goto 100
      EndIf




      Time = Time+H(1)
      If (Time .GE. AbsTime+IntPeriod-MinStepSize) Then
         Return
      EndIf
      If (conv .EQV. .True.) Then
         Return
      EndIf


      Call Func(NextTime,Psi,DtPsi)

 200  Continue


      If (CurrentOrder .LT. IntOrder) Then
         CurrentOrder = CurrentOrder+1
      Else
         CurOrdEqIntOrd = .True.
      EndIf



      If (CurOrdEqIntOrd) Then
         Do D = 1,PsiDim
            AuxPsi(D,1) = Psi(D)
            InterimAuxPsi(1) = DtPsi(D)
            Do P = 2,CurrentOrder
               InterimAuxPsi(P) = (InterimAuxPsi(P-1)-AuxPsi(D,P))
     +                            /Distance(P-1)
            EndDo
            Do P = 2,CurrentOrder+1
               AuxPsi(D,P) = InterimAuxPsi(P-1)
            EndDo
         EndDo
      Else
         Do D = 1,PsiDim
            AuxPsi(D,1) = Psi(D)
            InterimAuxPsi(1) = DtPsi(D)
            Do P = 2,CurrentOrder-1
               InterimAuxPsi(P) = (InterimAuxPsi(P-1)-AuxPsi(D,P))
     +                            /Distance(P-1)
            EndDo
            Do P = 2,CurrentOrder
               AuxPsi(D,P) = InterimAuxPsi(P-1)
            EndDo
         EndDo
      EndIf



      Error = Max(Error,MinError)
      Do P = CurrentOrder-1,2,-1
         H(P) = H(P-1)
      EndDo
      H(1) = H(1)*(IntError/Error)**(1.0D0/(CurrentOrder+1))
      If (UseOldH1) Then
         If (H(1) .LT. 0.98D0*H(3)) Then
            H(1) = 0.98D0*H(3)
         EndIf
         UseOldH1 = .False.
      EndIf
      If (Time+H(1) .GT. AbsTime+IntPeriod) Then
         H(1) = AbsTime+IntPeriod-Time
         UseOldH1 = .True.
         conv = .True.
      EndIf


      Goto 100
      
      End subroutine abm

!> Subroutine for the error evaluation in ABM.
      Subroutine AbsABMError (Predicted,Corrected,Psidim,Error)

      Implicit None

      Integer    PsiDim
      Real*8     Error
      Complex*16 Predicted(PsiDim),Corrected(PsiDim)

      Integer D
      Real*8  SingleError,Boundary



      Boundary = 1.0D0
      Error = 0.0D0


      Do D = 1,PsiDim
         SingleError = DAbs(Dble(Predicted(D))-Dble(Corrected(D)))
         If (DAbs(Dble(Corrected(D))) .GT. Boundary) Then
            SingleError = SingleError/DAbs(Dble(Corrected(D)))
         EndIf
         Error = Max(Error,SingleError)
         SingleError = DAbs(DImag(Predicted(D))-DImag(Corrected(D)))
         If (DAbs(DImag(Corrected(D))) .GT. Boundary) Then
            SingleError = SingleError/DAbs(DImag(Corrected(D)))
         EndIf
         Error = Max(Error,SingleError)
      EndDo

      Return
      End  Subroutine AbsABMError


!> Subroutine for the relative error evaluation in ABM.
      Subroutine RelABMError (Predicted,Corrected,Psidim,Error)

      Implicit None

      Real*8 Tiny
      Parameter (Tiny = 1.0D-30)

      Integer    PsiDim
      Real*8     Error
      Complex*16 Predicted(PsiDim),Corrected(PsiDim)

      Integer D
      Real*8  Maximum


      Error = 0.0D0
      Maximum = Tiny
      Do D = 1,PsiDim
         Maximum = Max(Maximum,DAbs(Dble(Corrected(D))),
     +                 DAbs(DImag(Corrected(D))))
         Error = Max(Error,DAbs(Dble(Predicted(D))-Dble(Corrected(D))),
     +               DAbs(DImag(Predicted(D))-DImag(Corrected(D))))
      EndDo
      Error = Error/Maximum

      Return
      End SubRoutine RelABMError

!> Writes the ABM steps.
      Subroutine WriteABMStep (Step,Order,Stepsize,Error)

      Implicit None

      Integer Step,Order
      Real*8  Stepsize,Error

       write(1003,*) "ABM Steps",Step,Order,Stepsize,Error

      Return
      End Subroutine WriteABMStep

!> Writes ABM error messages.
      Subroutine ABMErrorMsg (Error,Msg)

      Implicit None

      Integer      Error
      Character*(*) Msg


      If (Error .Eq. 1) Then
         Msg = 'Illegal integration order'
      ElseIf (Error .Eq. 2) Then
         Msg = 'Stepsize underflow'
      Else
         Msg = 'Unknown error occurred'
      EndIf

      Return
      End subroutine ABMErrorMsg

!> Coefficients ABM integrator (not working...).
      Subroutine COEFF_OMPABM (Psi,DtPsi,PsiDim,
     +                IntPeriod,AbsTime,IntOrder,
     +                InitStep,TolError,RestartABM,Steps,RepeatedSteps,
     +                ErrorCode,AuxPsi,Func)

      Implicit None
      Real*8    One6th,One30th,One210th,RelativeMinStep
      Integer   MaxOrder
      Parameter (One6th = 1.0D0/6.0D0,One30th = 1.0D0/30.0D0,
     +           One210th = 1.0D0/210.0D0,RelativeMinStep = 1.0D-20,
     +           MaxOrder = 8)

      Logical    RestartABM
      Integer    PsiDim,IntOrder,Steps,RepeatedSteps,ErrorCode
      Real*8     IntPeriod,AbsTime,InitStep,TolError
      Complex*16 Psi(PsiDim),DtPsi(PsiDim),AuxPsi(PsiDim,IntOrder+1)
      External   Func,CalcError
      
      REAL*8     singleerror,boundary

      Logical    StepIsRepeated,CurOrdEqIntOrd,UseOldH1,conv
      Integer    CurrentOrder,D,P,I,K
      Real*8     Time,NextTime,IntError,MinError,Error,D2,D3,D4,D5,D6,
     +           D7,H1,H1Sqr,MinusH1Cube,MinStepSize,Distance(MaxOrder),
     +           H(MaxOrder-1),PredCoef(MaxOrder),CorrCoef(MaxOrder),
     +           SumOfD(2:MaxOrder-2,2:MaxOrder-1),
     +           ProdOfD(2:MaxOrder-2,2:MaxOrder-1)
      Complex*16 InterimAuxPsi(MaxOrder)
      

      Save       IntError,Error,MinError,H,Distance,CurrentOrder,
     +           CurOrdEqIntOrd,StepIsRepeated,UseOldH1

         conv = .False.

      If (IntOrder .GT. MaxOrder) Then
         ErrorCode = 1
         Return
      EndIf


      ErrorCode = 0
      Time = AbsTime
      MinStepSize = RelativeMinStep*IntPeriod
      

      If (.Not. RestartABM) Then
         Goto 200
      EndIf



      IntError = 0.40D0*TolError
      MinError = 0.01D0*TolError
      Do P = 1,IntOrder-1
         H(P) = InitStep
      EndDo
      CurrentOrder = 2
      CurOrdEqIntOrd = .False.
      StepIsRepeated = .False.
      UseOldH1 = .False.


!$OMP PARALLEL DO 
!$OMP& FIRSTPRIVATE(Psidim)
      Do D = 1,PsiDim
         AuxPsi(D,1) = Psi(D)
         AuxPsi(D,2) = DtPsi(D)
      EndDo
!$OMP END PARALLEL DO


 100  Continue


      If ((H(1) .LT. MinStepSize) .And.
     +   (H(1) .LT. AbsTime+IntPeriod-Time)) Then
         ErrorCode = 2
        write(1003,*)InitStep," ABM STEp",H(1),MinStepSize
        write(1003,*)InitStep," ABM STEp",AbsTime+IntPeriod-Time
         Return
      EndIf


      Do P = 1,CurrentOrder
         If (P .Eq. 1) Then
            H1          = H(1)
            Distance(1) = H1
            PredCoef(1) = H1
            CorrCoef(1) = H1
         ElseIf (P .Eq. 2) Then
            H1Sqr       = H1*H1
            Distance(2) = Distance(1)+H(2)
            PredCoef(2) = 0.5*H1Sqr
            CorrCoef(2) = -PredCoef(2)
         ElseIf (P .Eq. 3) Then
            MinusH1Cube = -H1Sqr*H1
            D2          = H(2)
            Distance(3) = Distance(2)+H(3)
            PredCoef(3) = One6th*H1Sqr*(3.0*D2+2.0*H1)
            CorrCoef(3) = One6th*MinusH1Cube
         ElseIf (P .Eq. 4) Then
            D3          = D2+H(3)
            Distance(4) = Distance(3)+H(4)
            SumOfD(2,3) = D2+D3
            ProdOfD(2,3) = D2*D3
            PredCoef(4) = One6th*H1Sqr*(3.0*ProdOfD(2,3)
     +                    +(2.0*SumOfD(2,3)+1.5*H1)*H1)
            CorrCoef(4) = One6th*MinusH1Cube*(D2+0.5*H1)
         ElseIf (P .Eq. 5) Then
            D4          = D3+H(4)
            Distance(5) = Distance(4)+H(5)
            SumOfD(3,3) = D3
            ProdOfD(3,3) = D3
            Do I = 2,3
               SumOfD(I,4)  = SumOfD(I,3)+D4
               ProdOfD(I,4) = ProdOfD(I,3)*D4
            EndDo
            PredCoef(5) = One30th*H1Sqr*(15.0*ProdOfD(2,4)
     +                    +(10.0*(D2*SumOfD(3,4)+ProdOfD(3,4))
     +                    +(7.5*SumOfD(2,4)+6.0*H1)*H1)*H1)
            CorrCoef(5) = One30th*MinusH1Cube*(5.0*ProdOfD(2,3)
     +                    +(2.5*SumOfD(2,3)+1.5*H1)*H1)
         ElseIf (P .Eq. 6) Then
            D5          = D4+H(5)
            Distance(6) = Distance(5)+H(6)
            SumOfD(4,4) = D4
            ProdOfD(4,4) = D4
            Do I = 2,4
               SumOfD(I,5)  = SumOfD(I,4)+D5
               ProdOfD(I,5) = ProdOfD(I,4)*D5
            EndDo
            PredCoef(6) = One30th*H1Sqr*(15.0*ProdOfD(2,5)
     +                    +(10.0*(D2*(D3*SumOfD(4,5)+ProdOfD(4,5))
     +                    +ProdOfD(3,5))
     +                    +(7.5*(D2*SumOfD(3,5)+D3*SumOfD(4,5)
     +                    +ProdOfD(4,5))
     +                    +(6.0*SumOfD(2,5)+5.0*H1)*H1)*H1)*H1)
            CorrCoef(6) = One30th*MinusH1Cube*(5.0*ProdOfD(2,4)
     +                    +(2.5*(D2*SumOfD(3,4)+ProdOfD(3,4))
     +                    +(1.5*SumOfD(2,4)+H1)*H1)*H1)
         ElseIf (P .Eq. 7) Then
            D6          = D5+H(6)
            Distance(7) = Distance(6)+H(7)
            SumOfD(5,5) = D5
            ProdOfD(5,5) = D5
            Do I = 2,5
               SumOfD(I,6)  = SumOfD(I,5)+D6
               ProdOfD(I,6) = ProdOfD(I,5)*D6
            EndDo
            PredCoef(7) = One210th*H1Sqr*(105.0*ProdOfD(2,6)
     +                    +(70.0*(D2*(D3*(D4*SumOfD(5,6)+ProdOfD(5,6))
     +                    +ProdOfD(4,6))+ProdOfD(3,6))
     +                    +(52.5*(D2*(D3*SumOfD(4,6)+D4*SumOfD(5,6)
     +                    +ProdOfD(5,6))+D3*(D4*SumOfD(5,6)
     +                    +ProdOfD(5,6))+ProdOfD(4,6))
     +                    +(42.0*(D2*SumOfD(3,6)+D3*SumOfD(4,6)
     +                    +D4*SumOfD(5,6)+ProdOfD(5,6))
     +                    +(35.0*SumOfD(2,6)+30.0*H1)*H1)*H1)*H1)*H1)
         CorrCoef(7) = One210th*MinusH1Cube*(35.0*ProdOfD(2,5)
     +                    +(17.5*(D2*(D3*SumOfD(4,5)+ProdOfD(4,5))
     +                    +ProdOfD(3,5))
     +                    +(10.5*(D2*SumOfD(3,5)+D3*SumOfD(4,5)
     +                    +ProdOfD(4,5))+(7.0*SumOfD(2,5)
     +                    +5.0*H1)*H1)*H1)*H1)
         ElseIf (P .Eq. 8) Then
            D7          = D6+H(7)
            SumOfD(6,6) = D6
            ProdOfD(6,6) = D6
            Do I = 2,6
               SumOfD(I,7)  = SumOfD(I,6)+D7
               ProdOfD(I,7) = ProdOfD(I,6)*D7
            EndDo
            PredCoef(8) = One210th*H1Sqr*(105.0*ProdOfD(2,7)
     +                    +(60.0*(D2*(D3*(D4*(D5*SumOfD(6,7)
     +                    +ProdOfD(6,7))+ProdOfD(5,7))+ProdOfD(4,7))
     +                    +ProdOfD(3,7))
     +                    +(52.5*(D2*(D3*(D4*SumOfD(5,7)+D5*SumOfD(6,7))
     +                    +ProdOfD(6,7))+D4*(D5*SumOfD(6,7)
     +                    +ProdOfD(6,7))+ProdOfD(5,7))
     +                    +D3*(D4*(D5*SumOfD(6,7)+ProdOfD(6,7)
     +                    +ProdOfD(5,7))+ProdOfD(4,7))
     +                    +(42.0*(D2*(D3*SumOfD(4,7)+D4*SumOfD(5,7)
     +                    +D5*SumOfD(6,7)+ProdOfD(6,7))
     +                    +D3*(D4*SumOfD(5,7)+D5*SumOfD(6,7)
     +                    +ProdOfD(6,7))+D4*(D5*SumOfD(6,7)
     +                    +ProdOfD(6,7))+ProdOfD(5,7))
     +                    +(35.0*(D2*SumOfD(3,7)+D3*SumOfD(4,7)
     +                    +D4*SumOfD(5,7)+D5*SumOfD(6,7)+ProdOfD(6,7))
     +                    +(30.0*SumOfD(2,7)
     +                    +26.25*H1)*H1)*H1)*H1)*H1)*H1)
            CorrCoef(8) = One210th*MinusH1Cube*(35.0*ProdOfD(2,6)
     +                    +(17.5*(D2*(D3*(D4*SumOfD(5,6)+ProdOfD(5,6))
     +                    +ProdOfD(4,6))+ProdOfD(3,6))
     +                    +(10.5*(D2*(D3*SumOfD(4,6)+D4*SumOfD(5,6)
     +                    +ProdOfD(5,6))+D3*(D4*SumOfD(5,6)
     +                    +ProdOfD(5,6))+ProdOfD(4,6))
     +                    +(7.0*(D2*SumOfD(3,6)+D3*SumOfD(4,6)
     +                    +D4*SumOfD(5,6)+ProdOfD(5,6))+(5.0*SumOfD(2,6)
     +                    +3.75*H1)*H1)*H1)*H1)*H1)
         Else
            ErrorCode = 1
            Return
         EndIf
      EndDo


      If (StepIsRepeated) Then
!$OMP PARALLEL DO 
!$OMP& FIRSTPRIVATE(Psidim)
         Do D = 1,PsiDim
            Psi(D) = AuxPsi(D,1)
         EndDo
!$OMP END PARALLEL DO
         StepIsRepeated = .False.
      EndIf


      If (CurOrdEqIntOrd) Then
         K=currentorder
      else
         K=currentorder-1
      endif

!$OMP PARALLEL DO 
!$OMP& PRIVATE(D,P)
!$OMP& SHARED(PSI,PSIDIM,PREDCOEF,AUXPSI,K)
      Do D = 1,PsiDim
          Do P = 1,K
            Psi(D) = Psi(D)+PredCoef(P)*AuxPsi(D,P+1)
         EndDo
      EndDo
!$OMP END PARALLEL DO


        NextTime = Time+H(1)
         
      WRITE(6,*) "COEFFABM PSI,DTPSI", PSI(1),DTPSI(1) 
      Call Func(Psi,DtPsi)
      WRITE(6,*) "COEFFABM PSI,DTPSI AFTER FUNC", PSI(1),DTPSI(1) 
             
!$OMP PARALLEL 
      Boundary = 1.0D0
      Error = 0.0D0
!$OMP DO REDUCTION(-:INTERIMAUXPSI) REDUCTION(MAX:ERROR)
!$OMP& FIRSTPRIVATE(PsiDim,CurrentOrder,Distance)
      Do D = 1,PsiDim
         InterimAuxPsi(1) = DtPsi(D)
         Do P = 2,CurrentOrder
            InterimAuxPsi(P) = (InterimAuxPsi(P-1)-AuxPsi(D,P))
     +                         /Distance(P-1)
         enddo
         DtPsi(D) = Psi(D)
         Psi(D) = AuxPsi(D,1)
         Do P = 1,CurrentOrder
            psi(d) = psi(d)+CorrCoef(P)*InterimAuxPsi(P)
         EndDo
         SingleError = DAbs(Dble(DtPsi(D))-Dble(Psi(D)))
         If (DAbs(Dble(Psi(D))) .GT. Boundary) Then
             SingleError = SingleError/DAbs(Dble(Psi(D)))
         EndIf
         Error = Max(Error,SingleError)
         SingleError = DAbs(DImag(DtPsi(D))-DImag(Psi(D)))
         If (DAbs(DImag(Psi(D))) .GT. Boundary) Then
           SingleError = SingleError/DAbs(DImag(Psi(D)))
         EndIf
         Error = Max(Error,SingleError)
      EndDo
!$OMP END DO
!$OMP END PARALLEL
      If (CurrentOrder .LT. IntOrder) Then
         Error = Error*(2*IntOrder+1.0D0)/(2*CurrentOrder+1.0D0)
      EndIf
      Steps = Steps+1
      If (Error .GT. TolError) Then
         H(1) = 0.8D0*H(1)*(IntError/Error)**(1.0D0/(CurrentOrder+1))
         RepeatedSteps = RepeatedSteps+1
         StepIsRepeated = .True.
         Goto 100
      EndIf




      Time = Time+H(1)
      If (Time .GE. AbsTime+IntPeriod-MinStepSize) Then
         Return
      EndIf
cSTR added to fix sunf90 problem 
      If (conv .EQV. .True.) Then
         Return
      EndIf
cSTR added to fix sunf90 problem 

      WRITE(1003,*) "COEFFABM PSI,DTPSI", PSI(1),DTPSI(1) 
      Call Func(Psi,DtPsi)
      WRITE(1003,*) "COEFFABM PSI,DTPSI AFTER FUNC", PSI(1),DTPSI(1) 

 200  Continue


      If (CurrentOrder .LT. IntOrder) Then
         CurrentOrder = CurrentOrder+1
      Else
         CurOrdEqIntOrd = .True.
      EndIf


      If (CurOrdEqIntOrd) Then
         K=Currentorder+1
      Else
         K=Currentorder
      endif
!$OMP PARALLEL DO REDUCTION(-:INTERIMAUXPSI)
!$OMP& FIRSTPRIVATE(PsiDim,K,Distance)
      Do D = 1,PsiDim
            AuxPsi(D,1) = Psi(D)
            InterimAuxPsi(1) = DtPsi(D)
            Do P = 2,K-1
               InterimAuxPsi(P) = (InterimAuxPsi(P-1)-AuxPsi(D,P))
     +                            /Distance(P-1)
            EndDo
            Do P = 2,K
               AuxPsi(D,P) = InterimAuxPsi(P-1)
            EndDo
      EndDo
!$OMP END PARALLEL DO



      Error = Max(Error,MinError)
      Do P = CurrentOrder-1,2,-1
         H(P) = H(P-1)
      EndDo
      H(1) = H(1)*(IntError/Error)**(1.0D0/(CurrentOrder+1))
      write(1003,*) "ABM STEPSIZE", interror,error,CurrentOrder+1
      If (UseOldH1) Then
         If (H(1) .LT. 0.98D0*H(3)) Then
            H(1) = 0.98D0*H(3)
            write(1003,*) "C_ABM uses H(3)"
         EndIf
         UseOldH1 = .False.
      EndIf
      If (Time+H(1) .GT. AbsTime+IntPeriod) Then
         H(1) = AbsTime+IntPeriod-Time
         UseOldH1 = .True.
         conv = .True.
      EndIf


      Goto 100
      
      End subroutine COEFF_ompabm
                
!>           MPI-ADAMS-BASHFORTH-MOULTON                          *
!>                                                                   *
!>Library module containing an Adams-Bashforth-Moulton predictor-    *
!>corrector integrator.                                              *
!>                                                                   *
!>Contains:                                                          *
!>  ABM:          The ABM integration routine.                       *
!>  AbsABMError:  Computes the absolute error of the current ABM     *
!>                integration step.                                  *
!>  RelABMError:  Computes the relative error of the current ABM     *
!>                integration step.                                  *
!>  WriteABMStep: In the current form WriteABMStep is just a dummy   *
!>                routine doing absolutely nothing; it is included   *
!>                here for formal reasons, but can (if desired) be   *
!>                extended easily such that it writes the size and   *
!>                error of the current integration step to a file.   *
!>  ABMErrorMsg:  Returns for a given error number a corresponding   *
!>               error message.                                     *
!>                                                                   *
!>********************************************************************
!>
!>
!>********************************************************************
!>                                                                   *
!>                          SUBROUTINE MPIABM                        *
!>                                                                   *
!>Integrates a system of complex first order differential equations  *
!>employing the Adams-Bashforth-Moulton predictor-corrector method.  *
!>The ABM routine runs with variable step sizes and (except for the  *
!>beginning) a fixed integration order. The ODE is of the form       *
!>dPsi/dt = Func(AbsTime,Psi) =: DtPsi. All computations are         *
!>performed with double precision. The routine allows a previous     *
!>integration to be resumed (e. g. after the integration had been    *
!>stopped to write Psi to a file), as long as the AuxPsi array       *
!>hasn't been overwritten and the integration order and error        *
!>tolerance hasn't been changed. To do so simply set the restart     *
!>flag "RestartABM" to ".false.".                                    *
!>                                                                   *
!>Input parameters:                                                  *
!>  Psi:           The (complex) initial-value vector.               *
!>  DtPsi:         Time derivative of the initial-value vector (must *
!>                 be passed to the integrator regardless of the     *
!>                 flag "RestartABM").                               *
!>  PsiDim         Length of Psi and DtPsi vectors.                  *
!>  IntPeriod:     Lenght of time interval to be integrated.         *
!>  AbsTime:       Absolute time, i. e. Psi_initial=Psi(AbsTime).    *
!>  IntOrder:      Desired integration order.                        *
!>  InitStep:      Size of first integration step.                   *
!>  TolError:      Maximum error that is tolerated.                  *
!>  RestartABM:    Restart flag; if false, a previous integration is *
!>                  continued, otherwise a new integration is started *
!>                  (see comment above for details).                  *
!>                                                                    *
!> Output parameters:                                                 *
!>   Psi:           Solution of the ODE at time AbsTime+IntPeriod.    *
!>   DtPsi:         DtPsi is undetermined.                            *
!>   Steps:         Counter for the overall number of integration     *
!>                  steps. (Note that Steps isn't set to zero at the  *
!>                  beginning.)                                       *
!>   RepeatedSteps: Counter for the number of failed and thus         *
!>                  repeated integration steps. (Note that            *
!>                  RepeatedSteps isn't set to zero at the            *
!>                  beginning.)                                       *
!>   ErrorCode:     Error code having the following meaning:          *
!>                  0: everything was o. k.,                          *
!>                  1: illegal integration order,                     *
!>                 2: stepsize underflow.                            *
!>                                                                   *
!>Other parameters:                                                  *
!>  AuxPsi:        Auxiliary array of (minimum) size                 *
!>                 PsiDim*(IntOrder+1).                              *
!>                                                                   *
!>External routines:                                                 *
!>  Func:          Computes the time derivative DtPsi of Psi at time *
!>                 AbsTime. Called as Func(AbsTime,Psi,DtPsi).       *
!>  CalcError:     Determines the error of the current integration   *
!>                 step. Called as CalcError(Predicted_Psi,          *
!>                 Corrected_Psi,PsiDim,Error,).                     *
!>  WriteStep:     Writes the stepsize and error to a file.          *
!>                 Called as WriteStep(Step,Order,Stepsize,Error).   *
!>                                                                   *
!>V6.0 MB                                                            *
!>                                                                   *
!>********************************************************************
      Subroutine MPIABM (Psi,DtPsi,LocalPsiDim,IntPeriod,AbsTime,
     +                IntOrder,
     +                InitStep,TolError,RestartABM,Steps,RepeatedSteps,
     +                ErrorCode,AuxPsi,Func,MYID)
      USE Global_Parameters,ONLY: ABM_Time, Job_Prefactor

      USE MPI

      Implicit None

      Real*8    One6th,One30th,One210th,RelativeMinStep
      Integer   MaxOrder,ierr
      Parameter (One6th = 1.0D0/6.0D0,One30th = 1.0D0/30.0D0,
     +           One210th = 1.0D0/210.0D0,RelativeMinStep = 1.0D-12,
     +           MaxOrder = 8)

      INTEGER    MYID,I_Error

      Logical    RestartABM
      Integer*8  LocalPsiDim,IntOrder,Steps,RepeatedSteps,ErrorCode
      Real*8     IntPeriod,AbsTime,InitStep,TolError
      Complex*16 Psi(LocalPsiDim),DtPsi(LocalPsiDim),
     +           AuxPsi(LocalPsiDim,IntOrder+1)
      External   Func
      
      REAL*8     singleerror,boundary

      Logical    StepIsRepeated,CurOrdEqIntOrd,UseOldH1,conv
      Integer    CurrentOrder,D,P,I,K
      Real*8     Time,NextTime,IntError,MinError,Error,D2,D3,D4,D5,D6,
     +           D7,H1,H1Sqr,MinusH1Cube,MinStepSize,Distance(MaxOrder),
     +           H(MaxOrder-1),PredCoef(MaxOrder),CorrCoef(MaxOrder),
     +           SumOfD(2:MaxOrder-2,2:MaxOrder-1),
     +           ProdOfD(2:MaxOrder-2,2:MaxOrder-1)
      REAL*8 Start_Time,Stop_Time
      Complex*16 InterimAuxPsi(MaxOrder)

      Complex*16 InterimAuxPsi2(LocalPsiDim,MaxOrder)
      
      Save       IntError,Error,MinError,H,Distance,CurrentOrder,
     +           CurOrdEqIntOrd,StepIsRepeated,UseOldH1
       
   
!      Write(6,*) "MYID",MYID,"in MPI ABM",PSI(1)

         conv = .False.
C --- CHECK INTEGRATION ORDER ---

      If (IntOrder .GT. MaxOrder) Then
         ErrorCode = 1
         Return
      EndIf

C --- INITIALISE VARIABLES ---

      ErrorCode = 0
      Time = AbsTime
      MinStepSize = RelativeMinStep*IntPeriod
      
C --- CONTINUE INTEGRATION ---

      If (.Not. RestartABM) Then
         Goto 200
      EndIf

C --- INITIALISE VARIABLES ---

C "UseOldH1" is a flag set to true if the step size is artificially
C shortened in order to fit it to the remaining integration period
C before leaving the routine. This allows the next stepsize to be
C corrected back to the previous value.

      IntError = 0.40D0*TolError
      MinError = 0.01D0*TolError
      Do P = 1,IntOrder-1
         H(P) = InitStep
      EndDo
      CurrentOrder = 2
      CurOrdEqIntOrd = .False.
      StepIsRepeated = .False.
      UseOldH1 = .False.

C --- INITIALISE AUXPSI ---

C!$OMP PARALLEL DO 
C!$OMP& FIRSTPRIVATE(LocalPsidim)
      Do D = 1,LocalPsiDim
         AuxPsi(D,1) = Psi(D)
         AuxPsi(D,2) = DtPsi(D)
      EndDo
C!$OMP END PARALLEL DO

C --- INTEGRATION LOOP ---

 100  Continue

C --- CHECK WETHER STEPSIZE IS TOO SMALL ---

      If ((H(1) .LT. MinStepSize) .And.
     +   (H(1) .LT. AbsTime+IntPeriod-Time)) Then
         ErrorCode = 2
        write(1003,*)InitStep," ABM STEp",H(1),MinStepSize
        write(1003,*)InitStep," ABM STEp",AbsTime+IntPeriod-Time
         Return
      EndIf

C --- CALCULATE PREDICTOR AND CORRECTOR COEFFICIENTS ---

      Do P = 1,CurrentOrder
         If (P .Eq. 1) Then
            H1          = H(1)
            Distance(1) = H1
            PredCoef(1) = H1
            CorrCoef(1) = H1
         ElseIf (P .Eq. 2) Then
            H1Sqr       = H1*H1
            Distance(2) = Distance(1)+H(2)
            PredCoef(2) = 0.5*H1Sqr
            CorrCoef(2) = -PredCoef(2)
         ElseIf (P .Eq. 3) Then
            MinusH1Cube = -H1Sqr*H1
            D2          = H(2)
            Distance(3) = Distance(2)+H(3)
            PredCoef(3) = One6th*H1Sqr*(3.0*D2+2.0*H1)
            CorrCoef(3) = One6th*MinusH1Cube
         ElseIf (P .Eq. 4) Then
            D3          = D2+H(3)
            Distance(4) = Distance(3)+H(4)
            SumOfD(2,3) = D2+D3
            ProdOfD(2,3) = D2*D3
            PredCoef(4) = One6th*H1Sqr*(3.0*ProdOfD(2,3)
     +                    +(2.0*SumOfD(2,3)+1.5*H1)*H1)
            CorrCoef(4) = One6th*MinusH1Cube*(D2+0.5*H1)
         ElseIf (P .Eq. 5) Then
            D4          = D3+H(4)
            Distance(5) = Distance(4)+H(5)
            SumOfD(3,3) = D3
            ProdOfD(3,3) = D3
            Do I = 2,3
               SumOfD(I,4)  = SumOfD(I,3)+D4
               ProdOfD(I,4) = ProdOfD(I,3)*D4
            EndDo
            PredCoef(5) = One30th*H1Sqr*(15.0*ProdOfD(2,4)
     +                    +(10.0*(D2*SumOfD(3,4)+ProdOfD(3,4))
     +                    +(7.5*SumOfD(2,4)+6.0*H1)*H1)*H1)
            CorrCoef(5) = One30th*MinusH1Cube*(5.0*ProdOfD(2,3)
     +                    +(2.5*SumOfD(2,3)+1.5*H1)*H1)
         ElseIf (P .Eq. 6) Then
            D5          = D4+H(5)
            Distance(6) = Distance(5)+H(6)
            SumOfD(4,4) = D4
            ProdOfD(4,4) = D4
            Do I = 2,4
               SumOfD(I,5)  = SumOfD(I,4)+D5
               ProdOfD(I,5) = ProdOfD(I,4)*D5
            EndDo
            PredCoef(6) = One30th*H1Sqr*(15.0*ProdOfD(2,5)
     +                    +(10.0*(D2*(D3*SumOfD(4,5)+ProdOfD(4,5))
     +                    +ProdOfD(3,5))
     +                    +(7.5*(D2*SumOfD(3,5)+D3*SumOfD(4,5)
     +                    +ProdOfD(4,5))
     +                    +(6.0*SumOfD(2,5)+5.0*H1)*H1)*H1)*H1)
            CorrCoef(6) = One30th*MinusH1Cube*(5.0*ProdOfD(2,4)
     +                    +(2.5*(D2*SumOfD(3,4)+ProdOfD(3,4))
     +                    +(1.5*SumOfD(2,4)+H1)*H1)*H1)
         ElseIf (P .Eq. 7) Then
            D6          = D5+H(6)
            Distance(7) = Distance(6)+H(7)
            SumOfD(5,5) = D5
            ProdOfD(5,5) = D5
            Do I = 2,5
               SumOfD(I,6)  = SumOfD(I,5)+D6
               ProdOfD(I,6) = ProdOfD(I,5)*D6
            EndDo
            PredCoef(7) = One210th*H1Sqr*(105.0*ProdOfD(2,6)
     +                    +(70.0*(D2*(D3*(D4*SumOfD(5,6)+ProdOfD(5,6))
     +                    +ProdOfD(4,6))+ProdOfD(3,6))
     +                    +(52.5*(D2*(D3*SumOfD(4,6)+D4*SumOfD(5,6)
     +                    +ProdOfD(5,6))+D3*(D4*SumOfD(5,6)
     +                    +ProdOfD(5,6))+ProdOfD(4,6))
     +                    +(42.0*(D2*SumOfD(3,6)+D3*SumOfD(4,6)
     +                    +D4*SumOfD(5,6)+ProdOfD(5,6))
     +                    +(35.0*SumOfD(2,6)+30.0*H1)*H1)*H1)*H1)*H1)
         CorrCoef(7) = One210th*MinusH1Cube*(35.0*ProdOfD(2,5)
     +                    +(17.5*(D2*(D3*SumOfD(4,5)+ProdOfD(4,5))
     +                    +ProdOfD(3,5))
     +                    +(10.5*(D2*SumOfD(3,5)+D3*SumOfD(4,5)
     +                    +ProdOfD(4,5))+(7.0*SumOfD(2,5)
     +                    +5.0*H1)*H1)*H1)*H1)
         ElseIf (P .Eq. 8) Then
            D7          = D6+H(7)
            SumOfD(6,6) = D6
            ProdOfD(6,6) = D6
            Do I = 2,6
               SumOfD(I,7)  = SumOfD(I,6)+D7
               ProdOfD(I,7) = ProdOfD(I,6)*D7
            EndDo
            PredCoef(8) = One210th*H1Sqr*(105.0*ProdOfD(2,7)
     +                    +(60.0*(D2*(D3*(D4*(D5*SumOfD(6,7)
     +                    +ProdOfD(6,7))+ProdOfD(5,7))+ProdOfD(4,7))
     +                    +ProdOfD(3,7))
     +                    +(52.5*(D2*(D3*(D4*SumOfD(5,7)+D5*SumOfD(6,7))
     +                    +ProdOfD(6,7))+D4*(D5*SumOfD(6,7)
     +                    +ProdOfD(6,7))+ProdOfD(5,7))
     +                    +D3*(D4*(D5*SumOfD(6,7)+ProdOfD(6,7)
     +                    +ProdOfD(5,7))+ProdOfD(4,7))
     +                    +(42.0*(D2*(D3*SumOfD(4,7)+D4*SumOfD(5,7)
     +                    +D5*SumOfD(6,7)+ProdOfD(6,7))
     +                    +D3*(D4*SumOfD(5,7)+D5*SumOfD(6,7)
     +                    +ProdOfD(6,7))+D4*(D5*SumOfD(6,7)
     +                    +ProdOfD(6,7))+ProdOfD(5,7))
     +                    +(35.0*(D2*SumOfD(3,7)+D3*SumOfD(4,7)
     +                    +D4*SumOfD(5,7)+D5*SumOfD(6,7)+ProdOfD(6,7))
     +                    +(30.0*SumOfD(2,7)
     +                    +26.25*H1)*H1)*H1)*H1)*H1)*H1)
            CorrCoef(8) = One210th*MinusH1Cube*(35.0*ProdOfD(2,6)
     +                    +(17.5*(D2*(D3*(D4*SumOfD(5,6)+ProdOfD(5,6))
     +                    +ProdOfD(4,6))+ProdOfD(3,6))
     +                    +(10.5*(D2*(D3*SumOfD(4,6)+D4*SumOfD(5,6)
     +                    +ProdOfD(5,6))+D3*(D4*SumOfD(5,6)
     +                    +ProdOfD(5,6))+ProdOfD(4,6))
     +                    +(7.0*(D2*SumOfD(3,6)+D3*SumOfD(4,6)
     +                    +D4*SumOfD(5,6)+ProdOfD(5,6))+(5.0*SumOfD(2,6)
     +                    +3.75*H1)*H1)*H1)*H1)*H1)
         Else
            ErrorCode = 1
            Return
         EndIf
      EndDo

C --- RESTORE ORIGINAL PSI WHEN STEP IS REPEATED ---

      If (StepIsRepeated) Then
!$OMP PARALLEL DO 
!$OMP& FIRSTPRIVATE(LocalPsidim)
         Do D = 1,LocalPsiDim
            Psi(D) = AuxPsi(D,1)
         EndDo
!$OMP END PARALLEL DO
         StepIsRepeated = .False.
      EndIf

C --- PREDICT PSI ---	

C As long as the order p is increased, the predictor must be used as a
C p-step method. Later on, the predictor is run as a (p+1)-step method.
      If (CurOrdEqIntOrd) Then
         K=currentorder
      else
         K=currentorder-1
      endif

!$OMP PARALLEL DO 
!$OMP& PRIVATE(D,P)
!$OMP& SHARED(PSI,LocalPSIDIM,PREDCOEF,AUXPSI,K)
      Do D = 1,LocalPsiDim
          Do P = 1,K
            Psi(D) = Psi(D)+PredCoef(P)*AuxPsi(D,P+1)
         EndDo
      EndDo
!$OMP END PARALLEL DO

      NextTime = Time+H(1)  

      Call Func(NextTime,Psi,DtPsi,MYID)

!      Write(6,*) "MYID",MYID,"in MPI ABM after func",PSI(1)
C --- SAVE PREDICTED PSI IN DTPSI AND CORRECT PSI ---

      Boundary = 1.0D0
      Error = 0.0D0

      Do D = 1,LocalPsiDim
         InterimAuxPsi(1) = DtPsi(D)
         Do P = 2,CurrentOrder
            InterimAuxPsi(P) = (InterimAuxPsi(P-1)-AuxPsi(D,P))
     +                         /Distance(P-1)
         enddo
         DtPsi(D) = Psi(D)
         Psi(D) = AuxPsi(D,1)
         Do P = 1,CurrentOrder
            psi(d) = psi(d)+CorrCoef(P)*InterimAuxPsi(P)
         EndDo
C --- COMPUTE ERROR OF BOTH REAL AND IMAGINARY PART ---
         SingleError = DAbs(Dble(DtPsi(D))-Dble(Psi(D)))

         If (DAbs(Dble(Psi(D))) .GT. Boundary) Then
             SingleError = SingleError/DAbs(Dble(Psi(D)))
         EndIf

         Error = Max(Error,SingleError)

         SingleError = DAbs(DImag(DtPsi(D))-DImag(Psi(D)))
         If (DAbs(DImag(Psi(D))) .GT. Boundary) Then
           SingleError = SingleError/DAbs(DImag(Psi(D)))
         EndIf
         Error = Max(Error,SingleError)
      EndDo

!!!! Get maximal error from all processes and send result to all
      CALL MPI_ALLREDUCE(MPI_IN_PLACE,Error,1,
     .                MPI_DOUBLE_PRECISION,
     .                MPI_MAX,MPI_COMM_WORLD,ierr)
!!!! Get maximal error from all processes

!      Write(6,*) "MYID",MYID,"in MPI ABMi after ALLREDUCE",Error
      If (CurrentOrder .LT. IntOrder) Then
         Error = Error*(2*IntOrder+1.0D0)/(2*CurrentOrder+1.0D0)
      EndIf
      Steps = Steps+1

!!! For relaxations: do at most 50 small steps
!      IF ((ABS(DREAL(Job_Prefactor)).ge.1.d-10).and.
!     &    (CurrentOrder .EQ. IntOrder).and.
!     &    (Steps.ge.50)) THEN
!         Conv=.TRUE.
!         Error=0.d0
!      ENDIF


      If (Error .GT. TolError) Then
         H(1) = 0.8D0*H(1)*(IntError/Error)**(1.0D0/(CurrentOrder+1))
         RepeatedSteps = RepeatedSteps+1
         StepIsRepeated = .True.
         Goto 100
      EndIf

C --- WRITE STEPSIZE AND ERROR ---

Cstr      Call WriteStep(Steps,CurrentOrder,H(1),Error,Time)

C --- RETURN WHEN FINISHED ---

      Time = Time+H(1)
      If (Time .GE. AbsTime+IntPeriod-MinStepSize) Then
         Return
      EndIf
      If (conv .EQV. .True.) Then
         Return
      EndIf

C --- EVALUATE FUNCTION WITH CORRECTED PSI ---

      Call Func(NextTime,Psi,DtPsi,MYID)

 200  Continue

C --- INCREASE INTEGRATION ORDER ---
      If (CurrentOrder .LT. IntOrder) Then
         CurrentOrder = CurrentOrder+1
      Else
         CurOrdEqIntOrd = .True.
      EndIf

C --- COMPUTE NEW DEVIDED DIFFERENCES ---

C As long as the order p is increased, the predictor must be used as a
C p-step method. Later on, the predictor is run as a (p+1)-step method.
      If (CurOrdEqIntOrd) Then
         K=Currentorder+1
      Else
         K=Currentorder
      endif

      Do D = 1,LocalPsiDim
            AuxPsi(D,1) = Psi(D)
            InterimAuxPsi(1) = DtPsi(D)
            Do P = 2,K-1
               InterimAuxPsi(P) = (InterimAuxPsi(P-1)-AuxPsi(D,P))
     +                            /Distance(P-1)
            EndDo
            Do P = 2,K
               AuxPsi(D,P) = InterimAuxPsi(P-1)
            EndDo
      EndDo




C --- CALCULATE NEW STEP SIZE ---

C If the previous step had been shortened (see above), the next stepsize
C may be set (close) to the stepsize before that one.

      Error = Max(Error,MinError)

      CALL MPI_ALLREDUCE(MPI_IN_PLACE,Error,1,
     .                MPI_DOUBLE_PRECISION,
     .                MPI_MAX,MPI_COMM_WORLD,ierr)


      Do P = CurrentOrder-1,2,-1
         H(P) = H(P-1)
      EndDo
      H(1) = H(1)*(IntError/Error)**(1.0D0/(CurrentOrder+1))
      If (UseOldH1) Then
         If (H(1) .LT. 0.98D0*H(3)) Then
            H(1) = 0.98D0*H(3)
         EndIf
         UseOldH1 = .False.
      EndIf
      If (Time+H(1) .GT. AbsTime+IntPeriod) Then
         H(1) = AbsTime+IntPeriod-Time
         UseOldH1 = .True.
         conv = .True.
      EndIf

C --- CONTINUE INTEGRATION ---

      Goto 100
      
      End subroutine mpiabm





      END MODULE ABM_Integrators
