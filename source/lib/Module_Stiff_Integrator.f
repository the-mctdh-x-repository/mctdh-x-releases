!>GPLv3
!    MCTDH-X: the multiconfigurational time-dependent Hartree for 
!    indistinguishable particles software
!
!    Copyright (C) 2021  A. U. J. Lode, M. C. Tsatsos, 
!                        E. Fasshauer, S. E. Weiner, 
!                        R. Lin, L. Papariello, P. Molignini, 
!                        C. Lévêque, M. Büttner , J. Xiang, S. Dutta
!
!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <https://www.gnu.org/licenses/>.
!
!
!
!
!<GPLv3










!> This module contains the Integration routines for the case of stiff orbital EOM.
      MODULE Stiff_Integrator
      CONTAINS
!> Wrapper routine to call the ZVODE integrator.
      subroutine ZVODE_wrapper(Func,PsiDim,Psi,AbsTime,IntPeriod,
     +           TolError,ErrorCode,ZWORK,Arr)

      IMPLICIT NONE
      
      INTEGER PsiDim
      INTEGER LZW,LRW,LIW,ISTATE,NERR
      INTEGER Arr

      EXTERNAL Func

      DOUBLE COMPLEX Psi(PsiDim), RPAR
      DOUBLE PRECISION RWORK(20+PsiDim)
      INTEGER IWORK(30+PsiDim)
      DOUBLE COMPLEX ZWORK(10*PsiDim + (5)*PsiDim)
      DOUBLE PRECISION AbsTime, IntPeriod,TOUT
      INTEGER ErrorCode
      INTEGER ITOL,ITASK,IOPT
      DOUBLE PRECISION RTOL,TolError,TolErrorArray(PsiDim)
 
      RTOL = 0.d0 
      IF (Arr.eq.1) then
         ITOL = 1
      ELSE IF (Arr.eq.2) then
         ITOL = 2
         TolErrorArray=Tolerror
      ELSE
         write(6,*) 'Integration Order must be set to 1 or 2 for ZVODE!'
      ENDIF

      ITASK = 1
      IOPT=0
      ISTATE=1
      IWORK(1) = 1
      IWORK(2) = 1
      RPAR = DCMPLX(0.0D0,1.0D0)
      NERR = 0

      LZW=10*PsiDim + (5)*PsiDim
      LRW=20+PsiDim
      LIW=30+PsiDim

      TOUT=AbsTime+IntPeriod
      IF (ITOL.eq.1) then
         CALL ZVODE(Func,PsiDim,Psi,AbsTime,TOUT,
     +           ITOL,[RTOL],[TolError],ITASK,ISTATE,IOPT,
     +           ZWORK,LZW,RWORK,LRW,IWORK,LIW,JEX,25)
      ELSE IF (ITOL.eq.2) then
          CALL ZVODE(Func,PsiDim,Psi,AbsTime,TOUT,
     +           ITOL,[RTOL],TolErrorArray,ITASK,ISTATE,IOPT,
     +           ZWORK,LZW,RWORK,LRW,IWORK,LIW,JEX,25)
      ENDIF
  
      ErrorCode=ISTATE

      RETURN
      END subroutine ZVODE_wrapper



!> ZVODE integrator.
      SUBROUTINE ZVODE (F, NEQ, Y, T, TOUT, ITOL, RTOL, ATOL, ITASK,
     1            ISTATE, IOPT, ZWORK, LZW, RWORK, LRW, IWORK, LIW,
     2            JAC, MF)
      EXTERNAL F, JAC
      DOUBLE COMPLEX Y, ZWORK
      DOUBLE PRECISION T, TOUT, RTOL, ATOL, RWORK
      INTEGER NEQ, ITOL, ITASK, ISTATE, IOPT, LZW, LRW, IWORK, LIW,
     1        MF
      DIMENSION Y(*), RTOL(*), ATOL(*), ZWORK(LZW), RWORK(LRW),
     1          IWORK(LIW)
      DOUBLE PRECISION ACNRM, CCMXJ, CONP, CRATE, DRC, EL,
     1     ETA, ETAMAX, H, HMIN, HMXI, HNEW, HRL1, HSCAL, PRL1,
     2     RC, RL1, SRUR, TAU, TQ, TN, UROUND
      INTEGER ICF, INIT, IPUP, JCUR, JSTART, JSV, KFLAG, KUTH,
     1        L, LMAX, LYH, LEWT, LACOR, LSAVF, LWM, LIWM,
     2        LOCJS, MAXORD, METH, MITER, MSBJ, MXHNIL, MXSTEP,
     3        N, NEWH, NEWQ, NHNIL, NQ, NQNYH, NQWAIT, NSLJ,
     4        NSLP, NYH
      DOUBLE PRECISION HU
      INTEGER NCFN, NETF, NFE, NJE, NLU, NNI, NQU, NST
      LOGICAL IHIT
      DOUBLE PRECISION ATOLI, BIG, EWTI, FOUR, H0, HMAX, HMX, HUN, ONE,
     1   PT2, RH, RTOLI, SIZE, TCRIT, TNEXT, TOLSF, TP, TWO, ZERO
      INTEGER I, IER, IFLAG, IMXER, JCO, KGO, LENIW, LENJ, LENP, LENZW,
     1   LENRW, LENWM, LF0, MBAND, MFA, ML, MORD, MU, MXHNL0, MXSTP0,
     2   NITER, NSLAST
      CHARACTER*80 MSG
      DIMENSION MORD(2)
      SAVE MORD, MXHNL0, MXSTP0
      SAVE ZERO, ONE, TWO, FOUR, PT2, HUN
      COMMON /ZVOD01/ ACNRM, CCMXJ, CONP, CRATE, DRC, EL(13), ETA,
     1                ETAMAX, H, HMIN, HMXI, HNEW, HRL1, HSCAL, PRL1,
     2                RC, RL1, SRUR, TAU(13), TQ(5), TN, UROUND,
     3                ICF, INIT, IPUP, JCUR, JSTART, JSV, KFLAG, KUTH,
     4                L, LMAX, LYH, LEWT, LACOR, LSAVF, LWM, LIWM,
     5                LOCJS, MAXORD, METH, MITER, MSBJ, MXHNIL, MXSTEP,
     6                N, NEWH, NEWQ, NHNIL, NQ, NQNYH, NQWAIT, NSLJ,
     7                NSLP, NYH
      COMMON /ZVOD02/ HU, NCFN, NETF, NFE, NJE, NLU, NNI, NQU, NST
      DATA  MORD(1) /12/, MORD(2) /5/, MXSTP0 /500/, MXHNL0 /10/
      DATA ZERO /0.0D0/, ONE /1.0D0/, TWO /2.0D0/, FOUR /4.0D0/,
     1     PT2 /0.2D0/, HUN /100.0D0/
      IF (ISTATE .LT. 1 .OR. ISTATE .GT. 3) GO TO 601
      IF (ITASK .LT. 1 .OR. ITASK .GT. 5) GO TO 602
      IF (ISTATE .EQ. 1) GO TO 10
      IF (INIT .NE. 1) GO TO 603
      IF (ISTATE .EQ. 2) GO TO 200
      GO TO 20
 10   INIT = 0
      IF (TOUT .EQ. T) RETURN
 20   IF (NEQ .LE. 0) GO TO 604
      IF (ISTATE .EQ. 1) GO TO 25
      IF (NEQ .GT. N) GO TO 605
 25   N = NEQ
      IF (ITOL .LT. 1 .OR. ITOL .GT. 4) GO TO 606
      IF (IOPT .LT. 0 .OR. IOPT .GT. 1) GO TO 607
      JSV = SIGN(1,MF)
      MFA = ABS(MF)
      METH = MFA/10
      MITER = MFA - 10*METH
      IF (METH .LT. 1 .OR. METH .GT. 2) GO TO 608
      IF (MITER .LT. 0 .OR. MITER .GT. 5) GO TO 608
      IF (MITER .LE. 3) GO TO 30
      ML = IWORK(1)
      MU = IWORK(2)
      IF (ML .LT. 0 .OR. ML .GE. N) GO TO 609
      IF (MU .LT. 0 .OR. MU .GE. N) GO TO 610
 30   CONTINUE
      IF (IOPT .EQ. 1) GO TO 40
      MAXORD = MORD(METH)
      MXSTEP = MXSTP0
      MXHNIL = MXHNL0
      IF (ISTATE .EQ. 1) H0 = ZERO
      HMXI = ZERO
      HMIN = ZERO
      GO TO 60
 40   MAXORD = IWORK(5)
      IF (MAXORD .LT. 0) GO TO 611
      IF (MAXORD .EQ. 0) MAXORD = 100
      MAXORD = MIN(MAXORD,MORD(METH))
      MXSTEP = IWORK(6)
      IF (MXSTEP .LT. 0) GO TO 612
      IF (MXSTEP .EQ. 0) MXSTEP = MXSTP0
      MXHNIL = IWORK(7)
      IF (MXHNIL .LT. 0) GO TO 613
      IF (MXHNIL .EQ. 0) MXHNIL = MXHNL0
      IF (ISTATE .NE. 1) GO TO 50
      H0 = RWORK(5)
      IF ((TOUT - T)*H0 .LT. ZERO) GO TO 614
 50   HMAX = RWORK(6)
      IF (HMAX .LT. ZERO) GO TO 615
      HMXI = ZERO
      IF (HMAX .GT. ZERO) HMXI = ONE/HMAX
      HMIN = RWORK(7)
      IF (HMIN .LT. ZERO) GO TO 616
 60   LYH = 1
      IF (ISTATE .EQ. 1) NYH = N
      LWM = LYH + (MAXORD + 1)*NYH
      JCO = MAX(0,JSV)
      IF (MITER .EQ. 0) LENWM = 0
      IF (MITER .EQ. 1 .OR. MITER .EQ. 2) THEN
        LENWM = (1 + JCO)*N*N
        LOCJS = N*N + 1
      ENDIF
      IF (MITER .EQ. 3) LENWM = N
      IF (MITER .EQ. 4 .OR. MITER .EQ. 5) THEN
        MBAND = ML + MU + 1
        LENP = (MBAND + ML)*N
        LENJ = MBAND*N
        LENWM = LENP + JCO*LENJ
        LOCJS = LENP + 1
        ENDIF
      LSAVF = LWM + LENWM
      LACOR = LSAVF + N
      LENZW = LACOR + N - 1
      IWORK(17) = LENZW
      LEWT = 21
      LENRW = 20 + N
      IWORK(18) = LENRW
      LIWM = 1
      LENIW = 30 + N
      IF (MITER .EQ. 0 .OR. MITER .EQ. 3) LENIW = 30
      IWORK(19) = LENIW
      IF (LENZW .GT. LZW) GO TO 628
      IF (LENRW .GT. LRW) GO TO 617
      IF (LENIW .GT. LIW) GO TO 618
      RTOLI = RTOL(1)
      ATOLI = ATOL(1)
      DO 70 I = 1,N
        IF (ITOL .GE. 3) RTOLI = RTOL(I)
        IF (ITOL .EQ. 2 .OR. ITOL .EQ. 4) ATOLI = ATOL(I)
        IF (RTOLI .LT. ZERO) GO TO 619
        IF (ATOLI .LT. ZERO) GO TO 620
 70     CONTINUE
      IF (ISTATE .EQ. 1) GO TO 100
      JSTART = -1
      IF (NQ .LE. MAXORD) GO TO 200
      CALL ZCOPY (N, ZWORK(LWM), 1, ZWORK(LSAVF), 1)
      GO TO 200
 100  UROUND = DUMACH()
      TN = T
      IF (ITASK .NE. 4 .AND. ITASK .NE. 5) GO TO 110
      TCRIT = RWORK(1)
      IF ((TCRIT - TOUT)*(TOUT - T) .LT. ZERO) GO TO 625
      IF (H0 .NE. ZERO .AND. (T + H0 - TCRIT)*H0 .GT. ZERO)
     1   H0 = TCRIT - T
 110  JSTART = 0
      IF (MITER .GT. 0) SRUR = SQRT(UROUND)
      CCMXJ = PT2
      MSBJ = 50
      NHNIL = 0
      NST = 0
      NJE = 0
      NNI = 0
      NCFN = 0
      NETF = 0
      NLU = 0
      NSLJ = 0
      NSLAST = 0
      HU = ZERO
      NQU = 0
      LF0 = LYH + NYH
      CALL F (T, Y, ZWORK(LF0))
      NFE = 1
      CALL ZCOPY (N, Y, 1, ZWORK(LYH), 1)
      NQ = 1
      H = ONE
      CALL ZEWSET (N, ITOL, RTOL, ATOL, ZWORK(LYH), RWORK(LEWT))
      DO 120 I = 1,N
        IF (RWORK(I+LEWT-1) .LE. ZERO) GO TO 621
        RWORK(I+LEWT-1) = ONE/RWORK(I+LEWT-1)
 120  ENDDO
      IF (H0 .NE. ZERO) GO TO 180
      CALL ZVHIN (N, T, ZWORK(LYH), ZWORK(LF0), F, TOUT,
     1   UROUND, RWORK(LEWT), ITOL, ATOL, Y, ZWORK(LACOR), H0,
     2   NITER, IER)

 
      NFE = NFE + NITER
      IF (IER .NE. 0) GO TO 622
 180  RH = ABS(H0)*HMXI
      IF (RH .GT. ONE) H0 = H0/RH
      H = H0
      CALL DZSCAL (N, H0, ZWORK(LF0), 1)
      GO TO 270
 200  NSLAST = NST
      KUTH = 0
      GO TO (210, 250, 220, 230, 240), ITASK
 210  IF ((TN - TOUT)*H .LT. ZERO) GO TO 250
      CALL ZVINDY (TOUT, 0, ZWORK(LYH), NYH, Y, IFLAG)
      IF (IFLAG .NE. 0) GO TO 627
      T = TOUT
      GO TO 420
 220  TP = TN - HU*(ONE + HUN*UROUND)
      IF ((TP - TOUT)*H .GT. ZERO) GO TO 623
      IF ((TN - TOUT)*H .LT. ZERO) GO TO 250
      GO TO 400
 230  TCRIT = RWORK(1)
      IF ((TN - TCRIT)*H .GT. ZERO) GO TO 624
      IF ((TCRIT - TOUT)*H .LT. ZERO) GO TO 625
      IF ((TN - TOUT)*H .LT. ZERO) GO TO 245
      CALL ZVINDY (TOUT, 0, ZWORK(LYH), NYH, Y, IFLAG)
      IF (IFLAG .NE. 0) GO TO 627
      T = TOUT
      GO TO 420
 240  TCRIT = RWORK(1)
      IF ((TN - TCRIT)*H .GT. ZERO) GO TO 624
 245  HMX = ABS(TN) + ABS(H)
      IHIT = ABS(TN - TCRIT) .LE. HUN*UROUND*HMX
      IF (IHIT) GO TO 400
      TNEXT = TN + HNEW*(ONE + FOUR*UROUND)
      IF ((TNEXT - TCRIT)*H .LE. ZERO) GO TO 250
      H = (TCRIT - TN)*(ONE - FOUR*UROUND)
      KUTH = 1
 250  CONTINUE
      IF ((NST-NSLAST) .GE. MXSTEP) GO TO 500
      CALL ZEWSET (N, ITOL, RTOL, ATOL, ZWORK(LYH), RWORK(LEWT))
      DO 260 I = 1,N
        IF (RWORK(I+LEWT-1) .LE. ZERO) GO TO 510
        RWORK(I+LEWT-1) = ONE/RWORK(I+LEWT-1)
260   ENDDO   
270   TOLSF = UROUND*ZVNORM (N, ZWORK(LYH), RWORK(LEWT))
      IF (TOLSF .LE. ONE) GO TO 280
      TOLSF = TOLSF*TWO
      IF (NST .EQ. 0) GO TO 626
      GO TO 520
 280  IF ((TN + H) .NE. TN) GO TO 290
      NHNIL = NHNIL + 1
      IF (NHNIL .GT. MXHNIL) GO TO 290
      MSG = 'ZVODE--  Warning: internal T (=R1) and H (=R2) are'
      CALL XERRWD (MSG, 1, 0, 0, 0, 0, ZERO, ZERO)
      MSG='      such that in the machine, T + H = T on the next step  '
      CALL XERRWD (MSG, 1, 0, 0, 0, 0, ZERO, ZERO)
      MSG = '      (H = step size). solver will continue anyway'
      CALL XERRWD (MSG, 1, 0, 0, 0, 2, TN, H)
      IF (NHNIL .LT. MXHNIL) GO TO 290
      MSG = 'ZVODE--  Above warning has been issued I1 times.  '
      CALL XERRWD (MSG, 1, 0, 0, 0, 0, ZERO, ZERO)
      MSG = '      it will not be issued again for this problem'
      CALL XERRWD (MSG, 1, 1, MXHNIL, 0, 0, ZERO, ZERO)
 290  CONTINUE
      CALL ZVSTEP (Y, ZWORK(LYH), NYH, ZWORK(LYH), RWORK(LEWT),
     1   ZWORK(LSAVF), ZWORK(LACOR), ZWORK(LWM), IWORK(LIWM),
     2   F, JAC, F, ZVNLSD)
 
      KGO = 1 - KFLAG
      GO TO (300, 530, 540), KGO
 300  INIT = 1
      KUTH = 0
      GO TO (310, 400, 330, 340, 350), ITASK
 310  IF ((TN - TOUT)*H .LT. ZERO) GO TO 250
      CALL ZVINDY (TOUT, 0, ZWORK(LYH), NYH, Y, IFLAG)
      T = TOUT
      GO TO 420
 330  IF ((TN - TOUT)*H .GE. ZERO) GO TO 400
      GO TO 250
 340  IF ((TN - TOUT)*H .LT. ZERO) GO TO 345
      CALL ZVINDY (TOUT, 0, ZWORK(LYH), NYH, Y, IFLAG)
      T = TOUT
      GO TO 420
 345  HMX = ABS(TN) + ABS(H)
      IHIT = ABS(TN - TCRIT) .LE. HUN*UROUND*HMX
      IF (IHIT) GO TO 400
      TNEXT = TN + HNEW*(ONE + FOUR*UROUND)
      IF ((TNEXT - TCRIT)*H .LE. ZERO) GO TO 250
      H = (TCRIT - TN)*(ONE - FOUR*UROUND)
      KUTH = 1
      GO TO 250
 350  HMX = ABS(TN) + ABS(H)
      IHIT = ABS(TN - TCRIT) .LE. HUN*UROUND*HMX
 400  CONTINUE
      CALL ZCOPY (N, ZWORK(LYH), 1, Y, 1)
      T = TN
      IF (ITASK .NE. 4 .AND. ITASK .NE. 5) GO TO 420
      IF (IHIT) T = TCRIT
 420  ISTATE = 2
      RWORK(11) = HU
      RWORK(12) = HNEW
      RWORK(13) = TN
      IWORK(11) = NST
      IWORK(12) = NFE
      IWORK(13) = NJE
      IWORK(14) = NQU
      IWORK(15) = NEWQ
      IWORK(20) = NLU
      IWORK(21) = NNI
      IWORK(22) = NCFN
      IWORK(23) = NETF
      RETURN
 500  MSG = 'ZVODE--  At current T (=R1), MXSTEP (=I1) steps   '
      CALL XERRWD (MSG, 1, 0, 0, 0, 0, ZERO, ZERO)
      MSG = '      taken on this call before reaching TOUT     '
      CALL XERRWD (MSG, 1, 1, MXSTEP, 0, 1, TN, ZERO)
      ISTATE = -1
      GO TO 580
 510  EWTI = RWORK(LEWT+I-1)
      MSG = 'ZVODE--  At T (=R1), EWT(I1) has become R2 .le. 0.'
      CALL XERRWD (MSG, 1, 1, I, 0, 2, TN, EWTI)
      ISTATE = -6
      GO TO 580
 520  MSG = 'ZVODE--  At T (=R1), too much accuracy requested  '
      CALL XERRWD (MSG, 1, 0, 0, 0, 0, ZERO, ZERO)
      MSG = '      for precision of machine:   see TOLSF (=R2) '
      CALL XERRWD (MSG, 1, 0, 0, 0, 2, TN, TOLSF)
      RWORK(14) = TOLSF
      ISTATE = -2
      GO TO 580
 530  MSG = 'ZVODE--  At T(=R1) and step size H(=R2), the error'
      CALL XERRWD (MSG, 1, 0, 0, 0, 0, ZERO, ZERO)
      MSG = '      test failed repeatedly or with abs(H) = HMIN'
      CALL XERRWD (MSG, 1, 0, 0, 0, 2, TN, H)
      ISTATE = -4
      GO TO 560
 540  MSG = 'ZVODE--  At T (=R1) and step size H (=R2), the    '
      CALL XERRWD (MSG, 1, 0, 0, 0, 0, ZERO, ZERO)
      MSG = '      corrector convergence failed repeatedly     '
      CALL XERRWD (MSG, 1, 0, 0, 0, 0, ZERO, ZERO)
      MSG = '      or with abs(H) = HMIN   '
      CALL XERRWD (MSG, 1, 0, 0, 0, 2, TN, H)
      ISTATE = -5
 560  BIG = ZERO
      IMXER = 1
      DO 570 I = 1,N
        SIZE = ABS(ZWORK(I+LACOR-1))*RWORK(I+LEWT-1)
        IF (BIG .GE. SIZE) GO TO 570
        BIG = SIZE
        IMXER = I
 570    CONTINUE
      IWORK(16) = IMXER
 580  CONTINUE
      CALL ZCOPY (N, ZWORK(LYH), 1, Y, 1)
      T = TN
      RWORK(11) = HU
      RWORK(12) = H
      RWORK(13) = TN
      IWORK(11) = NST
      IWORK(12) = NFE
      IWORK(13) = NJE
      IWORK(14) = NQU
      IWORK(15) = NQ
      IWORK(20) = NLU
      IWORK(21) = NNI
      IWORK(22) = NCFN
      IWORK(23) = NETF
      RETURN
 601  MSG = 'ZVODE--  ISTATE (=I1) illegal '
      CALL XERRWD (MSG, 1, 1, ISTATE, 0, 0, ZERO, ZERO)
      IF (ISTATE .LT. 0) GO TO 800
      GO TO 700
 602  MSG = 'ZVODE--  ITASK (=I1) illegal  '
      CALL XERRWD (MSG, 1, 1, ITASK, 0, 0, ZERO, ZERO)
      GO TO 700
 603  MSG='ZVODE--  ISTATE (=I1) .gt. 1 but ZVODE not initialized      '
      CALL XERRWD (MSG, 1, 1, ISTATE, 0, 0, ZERO, ZERO)
      GO TO 700
 604  MSG = 'ZVODE--  NEQ (=I1) .lt. 1     '
      CALL XERRWD (MSG, 1, 1, NEQ, 0, 0, ZERO, ZERO)
      GO TO 700
 605  MSG = 'ZVODE--  ISTATE = 3 and NEQ increased (I1 to I2)  '
      CALL XERRWD (MSG, 1, 2, N, NEQ, 0, ZERO, ZERO)
      GO TO 700
 606  MSG = 'ZVODE--  ITOL (=I1) illegal   '
      CALL XERRWD (MSG, 1, 1, ITOL, 0, 0, ZERO, ZERO)
      GO TO 700
 607  MSG = 'ZVODE--  IOPT (=I1) illegal   '
      CALL XERRWD (MSG, 1, 1, IOPT, 0, 0, ZERO, ZERO)
      GO TO 700
 608  MSG = 'ZVODE--  MF (=I1) illegal     '
      CALL XERRWD (MSG, 1, 1, MF, 0, 0, ZERO, ZERO)
      GO TO 700
 609  MSG = 'ZVODE--  ML (=I1) illegal:  .lt.0 or .ge.NEQ (=I2)'
      CALL XERRWD (MSG, 1, 2, ML, NEQ, 0, ZERO, ZERO)
      GO TO 700
 610  MSG = 'ZVODE--  MU (=I1) illegal:  .lt.0 or .ge.NEQ (=I2)'
      CALL XERRWD (MSG, 1, 2, MU, NEQ, 0, ZERO, ZERO)
      GO TO 700
 611  MSG = 'ZVODE--  MAXORD (=I1) .lt. 0  '
      CALL XERRWD (MSG, 1, 1, MAXORD, 0, 0, ZERO, ZERO)
      GO TO 700
 612  MSG = 'ZVODE--  MXSTEP (=I1) .lt. 0  '
      CALL XERRWD (MSG, 1, 1, MXSTEP, 0, 0, ZERO, ZERO)
      GO TO 700
 613  MSG = 'ZVODE--  MXHNIL (=I1) .lt. 0  '
      CALL XERRWD (MSG, 1, 1, MXHNIL, 0, 0, ZERO, ZERO)
      GO TO 700
 614  MSG = 'ZVODE--  TOUT (=R1) behind T (=R2)      '
      CALL XERRWD (MSG, 1, 0, 0, 0, 2, TOUT, T)
      MSG = '      integration direction is given by H0 (=R1)  '
      CALL XERRWD (MSG, 1, 0, 0, 0, 1, H0, ZERO)
      GO TO 700
 615  MSG = 'ZVODE--  HMAX (=R1) .lt. 0.0  '
      CALL XERRWD (MSG, 1, 0, 0, 0, 1, HMAX, ZERO)
      GO TO 700
 616  MSG = 'ZVODE--  HMIN (=R1) .lt. 0.0  '
      CALL XERRWD (MSG, 1, 0, 0, 0, 1, HMIN, ZERO)
      GO TO 700
 617  CONTINUE
      MSG='ZVODE--  RWORK length needed, LENRW (=I1), exceeds LRW (=I2)'
      CALL XERRWD (MSG, 1, 2, LENRW, LRW, 0, ZERO, ZERO)
      GO TO 700
 618  CONTINUE
      MSG='ZVODE--  IWORK length needed, LENIW (=I1), exceeds LIW (=I2)'
      CALL XERRWD (MSG, 1, 2, LENIW, LIW, 0, ZERO, ZERO)
      GO TO 700
 619  MSG = 'ZVODE--  RTOL(I1) is R1 .lt. 0.0        '
      CALL XERRWD (MSG, 1, 1, I, 0, 1, RTOLI, ZERO)
      GO TO 700
 620  MSG = 'ZVODE--  ATOL(I1) is R1 .lt. 0.0        '
      CALL XERRWD (MSG, 1, 1, I, 0, 1, ATOLI, ZERO)
      GO TO 700
 621  EWTI = RWORK(LEWT+I-1)
      MSG = 'ZVODE--  EWT(I1) is R1 .le. 0.0         '
      CALL XERRWD (MSG, 1, 1, I, 0, 1, EWTI, ZERO)
      GO TO 700
 622  CONTINUE
      MSG='ZVODE--  TOUT (=R1) too close to T(=R2) to start integration'
      CALL XERRWD (MSG, 1, 0, 0, 0, 2, TOUT, T)
      GO TO 700
 623  CONTINUE
      MSG='ZVODE--  ITASK = I1 and TOUT (=R1) behind TCUR - HU (= R2)  '
      CALL XERRWD (MSG, 1, 1, ITASK, 0, 2, TOUT, TP)
      GO TO 700
 624  CONTINUE
      MSG='ZVODE--  ITASK = 4 or 5 and TCRIT (=R1) behind TCUR (=R2)   '
      CALL XERRWD (MSG, 1, 0, 0, 0, 2, TCRIT, TN)
      GO TO 700
 625  CONTINUE
      MSG='ZVODE--  ITASK = 4 or 5 and TCRIT (=R1) behind TOUT (=R2)   '
      CALL XERRWD (MSG, 1, 0, 0, 0, 2, TCRIT, TOUT)
      GO TO 700
 626  MSG = 'ZVODE--  At start of problem, too much accuracy   '
      CALL XERRWD (MSG, 1, 0, 0, 0, 0, ZERO, ZERO)
      MSG='      requested for precision of machine:   see TOLSF (=R1) '
      CALL XERRWD (MSG, 1, 0, 0, 0, 1, TOLSF, ZERO)
      RWORK(14) = TOLSF
      GO TO 700
 627  MSG='ZVODE--  Trouble from ZVINDY.  ITASK = I1, TOUT = R1.       '
      CALL XERRWD (MSG, 1, 1, ITASK, 0, 1, TOUT, ZERO)
      GO TO 700
 628  CONTINUE
      MSG='ZVODE--  ZWORK length needed, LENZW (=I1), exceeds LZW (=I2)'
      CALL XERRWD (MSG, 1, 2, LENZW, LZW, 0, ZERO, ZERO)
 700  CONTINUE
      ISTATE = -3
      RETURN
 800  MSG = 'ZVODE--  Run aborted:  apparent infinite loop     '
      CALL XERRWD (MSG, 2, 0, 0, 0, 0, ZERO, ZERO)
      RETURN
      END SUBROUTINE ZVODE 
*DECK ZVHIN
      SUBROUTINE ZVHIN (N, T0, Y0, YDOT, F, TOUT, UROUND,
     1   EWT, ITOL, ATOL, Y, TEMP, H0, NITER, IER)
      EXTERNAL F
      DOUBLE COMPLEX Y0, YDOT, Y, TEMP
      DOUBLE PRECISION T0, TOUT, UROUND, EWT, ATOL, H0
      INTEGER N, ITOL, NITER, IER
      DIMENSION Y0(*), YDOT(*), EWT(*), ATOL(*), Y(*),
     1   TEMP(*)

      DOUBLE PRECISION AFI, ATOLI, DELYI, H, HALF, HG, HLB, HNEW, HRAT,
     1     HUB, HUN, PT1, T1, TDIST, TROUND, TWO, YDDNRM
      INTEGER I, ITER
      SAVE HALF, HUN, PT1, TWO
      DATA HALF /0.5D0/, HUN /100.0D0/, PT1 /0.1D0/, TWO /2.0D0/
      NITER = 0
      TDIST = ABS(TOUT - T0)
      TROUND = UROUND*MAX(ABS(T0),ABS(TOUT))
      IF (TDIST .LT. TWO*TROUND) GO TO 100
      HLB = HUN*TROUND
      HUB = PT1*TDIST
      ATOLI = ATOL(1)
      DO 10 I = 1, N
        IF (ITOL .EQ. 2 .OR. ITOL .EQ. 4) ATOLI = ATOL(I)
        DELYI = PT1*ABS(Y0(I)) + ATOLI
        AFI = ABS(YDOT(I))
        IF (AFI*HUB .GT. DELYI) HUB = DELYI/AFI
 10     CONTINUE
      ITER = 0
      HG = SQRT(HLB*HUB)
      IF (HUB .LT. HLB) THEN
        H0 = HG
        GO TO 90
      ENDIF
 50   CONTINUE
      H = SIGN (HG, TOUT - T0)
      T1 = T0 + H
      DO 60 I = 1, N
        Y(I) = Y0(I) + H*YDOT(I)
 60   ENDDO
        CALL F (T1, Y, TEMP)
      DO 70 I = 1, N
        TEMP(I) = (TEMP(I) - YDOT(I))/H
 70   ENDDO
      YDDNRM = ZVNORM (N, TEMP, EWT)
      IF (YDDNRM*HUB*HUB .GT. TWO) THEN
        HNEW = SQRT(TWO/YDDNRM)
      ELSE
        HNEW = SQRT(HG*HUB)
      ENDIF
      ITER = ITER + 1
      IF (ITER .GE. 4) GO TO 80
      HRAT = HNEW/HG
      IF ( (HRAT .GT. HALF) .AND. (HRAT .LT. TWO) ) GO TO 80
      IF ( (ITER .GE. 2) .AND. (HNEW .GT. TWO*HG) ) THEN
        HNEW = HG
        GO TO 80
      ENDIF
      HG = HNEW
      GO TO 50
 80   H0 = HNEW*HALF
      IF (H0 .LT. HLB) H0 = HLB
      IF (H0 .GT. HUB) H0 = HUB
 90   H0 = SIGN(H0, TOUT - T0)
      NITER = ITER
      IER = 0
      RETURN
 100  IER = -1
      RETURN
      END SUBROUTINE ZVHIN
*DECK ZVINDY
      SUBROUTINE ZVINDY (T, K, YH, LDYH, DKY, IFLAG)
      DOUBLE COMPLEX YH, DKY
      DOUBLE PRECISION T
      INTEGER K, LDYH, IFLAG
      DIMENSION YH(LDYH,*), DKY(*)
      DOUBLE PRECISION ACNRM, CCMXJ, CONP, CRATE, DRC, EL,
     1     ETA, ETAMAX, H, HMIN, HMXI, HNEW, HRL1, HSCAL, PRL1,
     2     RC, RL1, SRUR, TAU, TQ, TN, UROUND
      INTEGER ICF, INIT, IPUP, JCUR, JSTART, JSV, KFLAG, KUTH,
     1        L, LMAX, LYH, LEWT, LACOR, LSAVF, LWM, LIWM,
     2        LOCJS, MAXORD, METH, MITER, MSBJ, MXHNIL, MXSTEP,
     3        N, NEWH, NEWQ, NHNIL, NQ, NQNYH, NQWAIT, NSLJ,
     4        NSLP, NYH
      DOUBLE PRECISION HU
      INTEGER NCFN, NETF, NFE, NJE, NLU, NNI, NQU, NST
      DOUBLE PRECISION C, HUN, R, S, TFUZZ, TN1, TP, ZERO
      INTEGER I, IC, J, JB, JB2, JJ, JJ1, JP1
      CHARACTER*80 MSG
      SAVE HUN, ZERO
      COMMON /ZVOD01/ ACNRM, CCMXJ, CONP, CRATE, DRC, EL(13), ETA,
     1                ETAMAX, H, HMIN, HMXI, HNEW, HRL1, HSCAL, PRL1,
     2                RC, RL1, SRUR, TAU(13), TQ(5), TN, UROUND,
     3                ICF, INIT, IPUP, JCUR, JSTART, JSV, KFLAG, KUTH,
     4                L, LMAX, LYH, LEWT, LACOR, LSAVF, LWM, LIWM,
     5                LOCJS, MAXORD, METH, MITER, MSBJ, MXHNIL, MXSTEP,
     6                N, NEWH, NEWQ, NHNIL, NQ, NQNYH, NQWAIT, NSLJ,
     7                NSLP, NYH
      COMMON /ZVOD02/ HU, NCFN, NETF, NFE, NJE, NLU, NNI, NQU, NST
      DATA HUN /100.0D0/, ZERO /0.0D0/
      IFLAG = 0
      IF (K .LT. 0 .OR. K .GT. NQ) GO TO 80
      TFUZZ = HUN*UROUND*SIGN(ABS(TN) + ABS(HU), HU)
      TP = TN - HU - TFUZZ
      TN1 = TN + TFUZZ
      IF ((T-TP)*(T-TN1) .GT. ZERO) GO TO 90
      S = (T - TN)/H
      IC = 1
      IF (K .EQ. 0) GO TO 15
      JJ1 = L - K
      DO 10 JJ = JJ1, NQ
        IC = IC*JJ
 10   ENDDO
 15   C = REAL(IC)
      DO 20 I = 1, N
        DKY(I) = C*YH(I,L)
 20   ENDDO 
      IF (K .EQ. NQ) GO TO 55
      JB2 = NQ - K
      DO 50 JB = 1, JB2
        J = NQ - JB
        JP1 = J + 1
        IC = 1
        IF (K .EQ. 0) GO TO 35
        JJ1 = JP1 - K
        DO 30 JJ = JJ1, J
          IC = IC*JJ
 30     ENDDO
 35     C = REAL(IC)
        DO 40 I = 1, N
          DKY(I) = C*YH(I,JP1) + S*DKY(I)
 40     ENDDO
 50     CONTINUE
      IF (K .EQ. 0) RETURN
 55   R = H**(-K)
      CALL DZSCAL (N, R, DKY, 1)
      RETURN
 80   MSG = 'ZVINDY-- K (=I1) illegal      '
      CALL XERRWD (MSG, 1, 1, K, 0, 0, ZERO, ZERO)
      IFLAG = -1
      RETURN
 90   MSG = 'ZVINDY-- T (=R1) illegal      '
      CALL XERRWD (MSG, 1, 0, 0, 0, 1, T, ZERO)
      MSG='      T not in interval TCUR - HU (= R1) to TCUR (=R2)      '
      CALL XERRWD (MSG, 1, 0, 0, 0, 2, TP, TN)
      IFLAG = -2
      RETURN
      END SUBROUTINE ZVINDY 
*DECK ZVSTEP
      SUBROUTINE ZVSTEP (Y, YH, LDYH, YH1, EWT, SAVF, ACOR,
     1                  WM, IWM, F, JAC, PSOL, VNLS)
      EXTERNAL F, JAC, PSOL, VNLS
      DOUBLE COMPLEX Y, YH, YH1, SAVF, ACOR, WM
      DOUBLE PRECISION EWT
      INTEGER LDYH, IWM
      DIMENSION Y(*), YH(LDYH,*), YH1(*), EWT(*), SAVF(*), 
     1   ACOR(*), WM(*), IWM(*)
      DOUBLE PRECISION ACNRM, CCMXJ, CONP, CRATE, DRC, EL,
     1     ETA, ETAMAX, H, HMIN, HMXI, HNEW, HRL1, HSCAL, PRL1,
     2     RC, RL1, SRUR, TAU, TQ, TN, UROUND
      INTEGER ICF, INIT, IPUP, JCUR, JSTART, JSV, KFLAG, KUTH,
     1        L, LMAX, LYH, LEWT, LACOR, LSAVF, LWM, LIWM,
     2        LOCJS, MAXORD, METH, MITER, MSBJ, MXHNIL, MXSTEP,
     3        N, NEWH, NEWQ, NHNIL, NQ, NQNYH, NQWAIT, NSLJ,
     4        NSLP, NYH
      DOUBLE PRECISION HU
      INTEGER NCFN, NETF, NFE, NJE, NLU, NNI, NQU, NST
      DOUBLE PRECISION ADDON, BIAS1,BIAS2,BIAS3, CNQUOT, DDN, DSM, DUP,
     1     ETACF, ETAMIN, ETAMX1, ETAMX2, ETAMX3, ETAMXF,
     2     ETAQ, ETAQM1, ETAQP1, FLOTL, ONE, ONEPSM,
     3     R, THRESH, TOLD, ZERO
      INTEGER I, I1, I2, IBACK, J, JB, KFC, KFH, MXNCF, NCF, NFLAG
      SAVE ADDON, BIAS1, BIAS2, BIAS3,
     1     ETACF, ETAMIN, ETAMX1, ETAMX2, ETAMX3, ETAMXF, ETAQ, ETAQM1,
     2     KFC, KFH, MXNCF, ONEPSM, THRESH, ONE, ZERO
      COMMON /ZVOD01/ ACNRM, CCMXJ, CONP, CRATE, DRC, EL(13), ETA,
     1                ETAMAX, H, HMIN, HMXI, HNEW, HRL1, HSCAL, PRL1,
     2                RC, RL1, SRUR, TAU(13), TQ(5), TN, UROUND,
     3                ICF, INIT, IPUP, JCUR, JSTART, JSV, KFLAG, KUTH,
     4                L, LMAX, LYH, LEWT, LACOR, LSAVF, LWM, LIWM,
     5                LOCJS, MAXORD, METH, MITER, MSBJ, MXHNIL, MXSTEP,
     6                N, NEWH, NEWQ, NHNIL, NQ, NQNYH, NQWAIT, NSLJ,
     7                NSLP, NYH
      COMMON /ZVOD02/ HU, NCFN, NETF, NFE, NJE, NLU, NNI, NQU, NST
      DATA KFC/-3/, KFH/-7/, MXNCF/10/
      DATA ADDON  /1.0D-6/,    BIAS1  /6.0D0/,     BIAS2  /6.0D0/,
     1     BIAS3  /10.0D0/,    ETACF  /0.25D0/,    ETAMIN /0.1D0/,
     2     ETAMXF /0.2D0/,     ETAMX1 /1.0D4/,     ETAMX2 /10.0D0/,
     3     ETAMX3 /10.0D0/,    ONEPSM /1.00001D0/, THRESH /1.5D0/
      DATA ONE/1.0D0/, ZERO/0.0D0/
      KFLAG = 0
      TOLD = TN
      NCF = 0
      JCUR = 0
      NFLAG = 0
      IF (JSTART .GT. 0) GO TO 20
      IF (JSTART .EQ. -1) GO TO 100
      LMAX = MAXORD + 1
      NQ = 1
      L = 2
      NQNYH = NQ*LDYH
      TAU(1) = H
      PRL1 = ONE
      RC = ZERO
      ETAMAX = ETAMX1
      NQWAIT = 2
      HSCAL = H
      GO TO 200
 20   CONTINUE
      IF (KUTH .EQ. 1) THEN
        ETA = MIN(ETA,H/HSCAL)
        NEWH = 1
        ENDIF
 50   IF (NEWH .EQ. 0) GO TO 200
      IF (NEWQ .EQ. NQ) GO TO 150
      IF (NEWQ .LT. NQ) THEN
        CALL ZVJUST (YH, LDYH, -1)
        NQ = NEWQ
        L = NQ + 1
        NQWAIT = L
        GO TO 150
        ENDIF
      IF (NEWQ .GT. NQ) THEN
        CALL ZVJUST (YH, LDYH, 1)
        NQ = NEWQ
        L = NQ + 1
        NQWAIT = L
        GO TO 150
      ENDIF
 100  CONTINUE
      LMAX = MAXORD + 1
      IF (N .EQ. LDYH) GO TO 120
      I1 = 1 + (NEWQ + 1)*LDYH
      I2 = (MAXORD + 1)*LDYH
      IF (I1 .GT. I2) GO TO 120
      DO 110 I = I1, I2
        YH1(I) = ZERO
 110  ENDDO
 120  IF (NEWQ .LE. MAXORD) GO TO 140
      FLOTL = REAL(LMAX)
      IF (MAXORD .LT. NQ-1) THEN
        DDN = ZVNORM (N, SAVF, EWT)/TQ(1)
        ETA = ONE/((BIAS1*DDN)**(ONE/FLOTL) + ADDON)
        ENDIF
      IF (MAXORD .EQ. NQ .AND. NEWQ .EQ. NQ+1) ETA = ETAQ
      IF (MAXORD .EQ. NQ-1 .AND. NEWQ .EQ. NQ+1) THEN
        ETA = ETAQM1
        CALL ZVJUST (YH, LDYH, -1)
        ENDIF
      IF (MAXORD .EQ. NQ-1 .AND. NEWQ .EQ. NQ) THEN
        DDN = ZVNORM (N, SAVF, EWT)/TQ(1)
        ETA = ONE/((BIAS1*DDN)**(ONE/FLOTL) + ADDON)
        CALL ZVJUST (YH, LDYH, -1)
        ENDIF
      ETA = MIN(ETA,ONE)
      NQ = MAXORD
      L = LMAX
 140  IF (KUTH .EQ. 1) ETA = MIN(ETA,ABS(H/HSCAL))
      IF (KUTH .EQ. 0) ETA = MAX(ETA,HMIN/ABS(HSCAL))
      ETA = ETA/MAX(ONE,ABS(HSCAL)*HMXI*ETA)
      NEWH = 1
      NQWAIT = L
      IF (NEWQ .LE. MAXORD) GO TO 50
 150  R = ONE
      DO 180 J = 2, L
        R = R*ETA
        CALL DZSCAL (N, R, YH(1,J), 1 )
 180    CONTINUE
      H = HSCAL*ETA
      HSCAL = H
      RC = RC*ETA
      NQNYH = NQ*LDYH
 200  TN = TN + H
      I1 = NQNYH + 1
      DO 220 JB = 1, NQ
        I1 = I1 - LDYH
        DO 210 I = I1, NQNYH
          YH1(I) = YH1(I) + YH1(I+LDYH)
 210    ENDDO
 220  ENDDO
      CALL ZVSET
      RL1 = ONE/EL(2)
      RC = RC*(RL1/PRL1)
      PRL1 = RL1
      CALL VNLS (Y, YH, LDYH, SAVF, EWT, ACOR, IWM, WM,
     1           F, JAC, PSOL, NFLAG)
      IF (NFLAG .EQ. 0) GO TO 450
        NCF = NCF + 1
        NCFN = NCFN + 1
        ETAMAX = ONE
        TN = TOLD
        I1 = NQNYH + 1
        DO 430 JB = 1, NQ
          I1 = I1 - LDYH
          DO 420 I = I1, NQNYH
            YH1(I) = YH1(I) - YH1(I+LDYH)
 420      ENDDO
 430    ENDDO
        IF (NFLAG .LT. -1) GO TO 680
        IF (ABS(H) .LE. HMIN*ONEPSM) GO TO 670
        IF (NCF .EQ. MXNCF) GO TO 670
        ETA = ETACF
        ETA = MAX(ETA,HMIN/ABS(H))
        NFLAG = -1
        GO TO 150
 450  CONTINUE
      DSM = ACNRM/TQ(2)
      IF (DSM .GT. ONE) GO TO 500
      KFLAG = 0
      NST = NST + 1
      HU = H
      NQU = NQ
      DO 470 IBACK = 1, NQ
        I = L - IBACK
        TAU(I+1) = TAU(I)
470   ENDDO
       TAU(1) = H
      DO 480 J = 1, L
        CALL DZAXPY (N, EL(J), ACOR, 1, YH(1,J), 1 )
 480    CONTINUE
      NQWAIT = NQWAIT - 1
      IF ((L .EQ. LMAX) .OR. (NQWAIT .NE. 1)) GO TO 490
      CALL ZCOPY (N, ACOR, 1, YH(1,LMAX), 1 )
      CONP = TQ(5)
 490  IF (ETAMAX .NE. ONE) GO TO 560
      IF (NQWAIT .LT. 2) NQWAIT = 2
      NEWQ = NQ
      NEWH = 0
      ETA = ONE
      HNEW = H
      GO TO 690
 500  KFLAG = KFLAG - 1
      NETF = NETF + 1
      NFLAG = -2
      TN = TOLD
      I1 = NQNYH + 1
      DO 520 JB = 1, NQ
        I1 = I1 - LDYH
        DO 510 I = I1, NQNYH
          YH1(I) = YH1(I) - YH1(I+LDYH)
 510    ENDDO
 520  ENDDO
      IF (ABS(H) .LE. HMIN*ONEPSM) GO TO 660
      ETAMAX = ONE
      IF (KFLAG .LE. KFC) GO TO 530
      FLOTL = REAL(L)
      ETA = ONE/((BIAS2*DSM)**(ONE/FLOTL) + ADDON)
      ETA = MAX(ETA,HMIN/ABS(H),ETAMIN)
      IF ((KFLAG .LE. -2) .AND. (ETA .GT. ETAMXF)) ETA = ETAMXF
      GO TO 150
 530  IF (KFLAG .EQ. KFH) GO TO 660
      IF (NQ .EQ. 1) GO TO 540
      ETA = MAX(ETAMIN,HMIN/ABS(H))
      CALL ZVJUST (YH, LDYH, -1)
      L = NQ
      NQ = NQ - 1
      NQWAIT = L
      GO TO 150
 540  ETA = MAX(ETAMIN,HMIN/ABS(H))
      H = H*ETA
      HSCAL = H
      TAU(1) = H
      CALL F (TN, Y, SAVF)
      NFE = NFE + 1
      DO 550 I = 1, N
        YH(I,2) = H*SAVF(I)
 550  ENDDO
      NQWAIT = 10
      GO TO 200
 560  FLOTL = REAL(L)
      ETAQ = ONE/((BIAS2*DSM)**(ONE/FLOTL) + ADDON)
      IF (NQWAIT .NE. 0) GO TO 600
      NQWAIT = 2
      ETAQM1 = ZERO
      IF (NQ .EQ. 1) GO TO 570
      DDN = ZVNORM (N, YH(1,L), EWT)/TQ(1)
      ETAQM1 = ONE/((BIAS1*DDN)**(ONE/(FLOTL - ONE)) + ADDON)
 570  ETAQP1 = ZERO
      IF (L .EQ. LMAX) GO TO 580
      CNQUOT = (TQ(5)/CONP)*(H/TAU(2))**L
      DO 575 I = 1, N
        SAVF(I) = ACOR(I) - CNQUOT*YH(I,LMAX)
 575  ENDDO
      DUP = ZVNORM (N, SAVF, EWT)/TQ(3)
      ETAQP1 = ONE/((BIAS3*DUP)**(ONE/(FLOTL + ONE)) + ADDON)
 580  IF (ETAQ .GE. ETAQP1) GO TO 590
      IF (ETAQP1 .GT. ETAQM1) GO TO 620
      GO TO 610
 590  IF (ETAQ .LT. ETAQM1) GO TO 610
 600  ETA = ETAQ
      NEWQ = NQ
      GO TO 630
 610  ETA = ETAQM1
      NEWQ = NQ - 1
      GO TO 630
 620  ETA = ETAQP1
      NEWQ = NQ + 1
      CALL ZCOPY (N, ACOR, 1, YH(1,LMAX), 1)
 630  IF (ETA .LT. THRESH .OR. ETAMAX .EQ. ONE) GO TO 640
      ETA = MIN(ETA,ETAMAX)
      ETA = ETA/MAX(ONE,ABS(H)*HMXI*ETA)
      NEWH = 1
      HNEW = H*ETA
      GO TO 690
 640  NEWQ = NQ
      NEWH = 0
      ETA = ONE
      HNEW = H
      GO TO 690
 660  KFLAG = -1
      GO TO 720
 670  KFLAG = -2
      GO TO 720
 680  IF (NFLAG .EQ. -2) KFLAG = -3
      IF (NFLAG .EQ. -3) KFLAG = -4
      GO TO 720
 690  ETAMAX = ETAMX3
      IF (NST .LE. 10) ETAMAX = ETAMX2
 700  R = ONE/TQ(2)
      CALL DZSCAL (N, R, ACOR, 1)
 720  JSTART = 1
      RETURN
      END SUBROUTINE ZVSTEP 
*DECK ZVSET
      SUBROUTINE ZVSET
      DOUBLE PRECISION ACNRM, CCMXJ, CONP, CRATE, DRC, EL,
     1     ETA, ETAMAX, H, HMIN, HMXI, HNEW, HRL1, HSCAL, PRL1,
     2     RC, RL1, SRUR, TAU, TQ, TN, UROUND
      INTEGER ICF, INIT, IPUP, JCUR, JSTART, JSV, KFLAG, KUTH,
     1        L, LMAX, LYH, LEWT, LACOR, LSAVF, LWM, LIWM,
     2        LOCJS, MAXORD, METH, MITER, MSBJ, MXHNIL, MXSTEP,
     3        N, NEWH, NEWQ, NHNIL, NQ, NQNYH, NQWAIT, NSLJ,
     4        NSLP, NYH
      DOUBLE PRECISION AHATN0, ALPH0, CNQM1, CORTES, CSUM, ELP, EM,
     1     EM0, FLOTI, FLOTL, FLOTNQ, HSUM, ONE, RXI, RXIS, S, SIX,
     2     T1, T2, T3, T4, T5, T6, TWO, XI, ZERO
      INTEGER I, IBACK, J, JP1, NQM1, NQM2
      DIMENSION EM(13)
      SAVE CORTES, ONE, SIX, TWO, ZERO
      COMMON /ZVOD01/ ACNRM, CCMXJ, CONP, CRATE, DRC, EL(13), ETA,
     1                ETAMAX, H, HMIN, HMXI, HNEW, HRL1, HSCAL, PRL1,
     2                RC, RL1, SRUR, TAU(13), TQ(5), TN, UROUND,
     3                ICF, INIT, IPUP, JCUR, JSTART, JSV, KFLAG, KUTH,
     4                L, LMAX, LYH, LEWT, LACOR, LSAVF, LWM, LIWM,
     5                LOCJS, MAXORD, METH, MITER, MSBJ, MXHNIL, MXSTEP,
     6                N, NEWH, NEWQ, NHNIL, NQ, NQNYH, NQWAIT, NSLJ,
     7                NSLP, NYH
      DATA CORTES /0.1D0/
      DATA ONE  /1.0D0/, SIX /6.0D0/, TWO /2.0D0/, ZERO /0.0D0/
      FLOTL = REAL(L)
      NQM1 = NQ - 1
      NQM2 = NQ - 2
      GO TO (100, 200), METH
 100  IF (NQ .NE. 1) GO TO 110
      EL(1) = ONE
      EL(2) = ONE
      TQ(1) = ONE
      TQ(2) = TWO
      TQ(3) = SIX*TQ(2)
      TQ(5) = ONE
      GO TO 300
 110  HSUM = H
      EM(1) = ONE
      FLOTNQ = FLOTL - ONE
      DO 115 I = 2, L
        EM(I) = ZERO
 115  ENDDO
      DO 150 J = 1, NQM1
        IF ((J .NE. NQM1) .OR. (NQWAIT .NE. 1)) GO TO 130
        S = ONE
        CSUM = ZERO
        DO 120 I = 1, NQM1
          CSUM = CSUM + S*EM(I)/REAL(I+1)
          S = -S
 120    ENDDO
        TQ(1) = EM(NQM1)/(FLOTNQ*CSUM)
 130    RXI = H/HSUM
        DO 140 IBACK = 1, J
          I = (J + 2) - IBACK
          EM(I) = EM(I) + EM(I-1)*RXI
 140    ENDDO
        HSUM = HSUM + TAU(J)
 150    CONTINUE
      S = ONE
      EM0 = ZERO
      CSUM = ZERO
      DO 160 I = 1, NQ
        FLOTI = REAL(I)
        EM0 = EM0 + S*EM(I)/FLOTI
        CSUM = CSUM + S*EM(I)/(FLOTI+ONE)
        S = -S
 160  ENDDO
      S = ONE/EM0
      EL(1) = ONE
      DO 170 I = 1, NQ
        EL(I+1) = S*EM(I)/REAL(I)
 170  ENDDO
      XI = HSUM/H
      TQ(2) = XI*EM0/CSUM
      TQ(5) = XI/EL(L)
      IF (NQWAIT .NE. 1) GO TO 300
      RXI = ONE/XI
      DO 180 IBACK = 1, NQ
        I = (L + 1) - IBACK
        EM(I) = EM(I) + EM(I-1)*RXI
 180  ENDDO
      S = ONE
      CSUM = ZERO
      DO 190 I = 1, L
        CSUM = CSUM + S*EM(I)/REAL(I+1)
        S = -S
 190  ENDDO
      TQ(3) = FLOTL*EM0/CSUM
      GO TO 300
 200  DO 210 I = 3, L
        EL(I) = ZERO
 210  ENDDO
      EL(1) = ONE
      EL(2) = ONE
      ALPH0 = -ONE
      AHATN0 = -ONE
      HSUM = H
      RXI = ONE
      RXIS = ONE
      IF (NQ .EQ. 1) GO TO 240
      DO 230 J = 1, NQM2
        HSUM = HSUM + TAU(J)
        RXI = H/HSUM
        JP1 = J + 1
        ALPH0 = ALPH0 - ONE/REAL(JP1)
        DO 220 IBACK = 1, JP1
          I = (J + 3) - IBACK
          EL(I) = EL(I) + EL(I-1)*RXI
 220    ENDDO
 230  ENDDO
      ALPH0 = ALPH0 - ONE/REAL(NQ)
      RXIS = -EL(2) - ALPH0
      HSUM = HSUM + TAU(NQM1)
      RXI = H/HSUM
      AHATN0 = -EL(2) - RXI
      DO 235 IBACK = 1, NQ
        I = (NQ + 2) - IBACK
        EL(I) = EL(I) + EL(I-1)*RXIS
 235  ENDDO
 240  T1 = ONE - AHATN0 + ALPH0
      T2 = ONE + REAL(NQ)*T1
      TQ(2) = ABS(ALPH0*T2/T1)
      TQ(5) = ABS(T2/(EL(L)*RXI/RXIS))
      IF (NQWAIT .NE. 1) GO TO 300
      CNQM1 = RXIS/EL(L)
      T3 = ALPH0 + ONE/REAL(NQ)
      T4 = AHATN0 + RXI
      ELP = T3/(ONE - T4 + T3)
      TQ(1) = ABS(ELP/CNQM1)
      HSUM = HSUM + TAU(NQ)
      RXI = H/HSUM
      T5 = ALPH0 - ONE/REAL(NQ+1)
      T6 = AHATN0 - RXI
      ELP = T2/(ONE - T6 + T5)
      TQ(3) = ABS(ELP*RXI*(FLOTL + ONE)*T5)
 300  TQ(4) = CORTES*TQ(2)
      RETURN
      END SUBROUTINE ZVSET
*DECK ZVJUST
      SUBROUTINE ZVJUST (YH, LDYH, IORD)
      DOUBLE COMPLEX YH
      INTEGER LDYH, IORD
      DIMENSION YH(LDYH,*)
      DOUBLE PRECISION ACNRM, CCMXJ, CONP, CRATE, DRC, EL,
     1     ETA, ETAMAX, H, HMIN, HMXI, HNEW, HRL1, HSCAL, PRL1,
     2     RC, RL1, SRUR, TAU, TQ, TN, UROUND
      INTEGER ICF, INIT, IPUP, JCUR, JSTART, JSV, KFLAG, KUTH,
     1        L, LMAX, LYH, LEWT, LACOR, LSAVF, LWM, LIWM,
     2        LOCJS, MAXORD, METH, MITER, MSBJ, MXHNIL, MXSTEP,
     3        N, NEWH, NEWQ, NHNIL, NQ, NQNYH, NQWAIT, NSLJ,
     4        NSLP, NYH
      DOUBLE PRECISION ALPH0, ALPH1, HSUM, ONE, PROD, T1, XI,XIOLD, ZERO
      INTEGER I, IBACK, J, JP1, LP1, NQM1, NQM2, NQP1
      SAVE ONE, ZERO
      COMMON /ZVOD01/ ACNRM, CCMXJ, CONP, CRATE, DRC, EL(13), ETA,
     1                ETAMAX, H, HMIN, HMXI, HNEW, HRL1, HSCAL, PRL1,
     2                RC, RL1, SRUR, TAU(13), TQ(5), TN, UROUND,
     3                ICF, INIT, IPUP, JCUR, JSTART, JSV, KFLAG, KUTH,
     4                L, LMAX, LYH, LEWT, LACOR, LSAVF, LWM, LIWM,
     5                LOCJS, MAXORD, METH, MITER, MSBJ, MXHNIL, MXSTEP,
     6                N, NEWH, NEWQ, NHNIL, NQ, NQNYH, NQWAIT, NSLJ,
     7                NSLP, NYH
      DATA ONE /1.0D0/, ZERO /0.0D0/
      IF ((NQ .EQ. 2) .AND. (IORD .NE. 1)) RETURN
      NQM1 = NQ - 1
      NQM2 = NQ - 2
      GO TO (100, 200), METH
 100  CONTINUE
      IF (IORD .EQ. 1) GO TO 180
      DO 110 J = 1, LMAX
        EL(J) = ZERO
 110  ENDDO
      EL(2) = ONE
      HSUM = ZERO
      DO 130 J = 1, NQM2
        HSUM = HSUM + TAU(J)
        XI = HSUM/HSCAL
        JP1 = J + 1
        DO 120 IBACK = 1, JP1
          I = (J + 3) - IBACK
          EL(I) = EL(I)*XI + EL(I-1)
 120     ENDDO
 130    ENDDO
      DO 140 J = 2, NQM1
        EL(J+1) = REAL(NQ)*EL(J)/REAL(J)
 140  ENDDO
      DO 170 J = 3, NQ
        DO 160 I = 1, N
          YH(I,J) = YH(I,J) - YH(I,L)*EL(J)
 160    ENDDO
 170  ENDDO
      RETURN
 180  CONTINUE
      LP1 = L + 1
      DO 190 I = 1, N
        YH(I,LP1) = ZERO
 190  ENDDO
      RETURN
 200  CONTINUE
      IF (IORD .EQ. 1) GO TO 300
      DO 210 J = 1, LMAX
        EL(J) = ZERO
 210  ENDDO
      EL(3) = ONE
      HSUM = ZERO
      DO 230 J = 1,NQM2
        HSUM = HSUM + TAU(J)
        XI = HSUM/HSCAL
        JP1 = J + 1
        DO 220 IBACK = 1, JP1
          I = (J + 4) - IBACK
          EL(I) = EL(I)*XI + EL(I-1)
 220    ENDDO
 230  ENDDO
      DO 250 J = 3,NQ
        DO 240 I = 1, N
          YH(I,J) = YH(I,J) - YH(I,L)*EL(J)
 240     ENDDO
 250  ENDDO
      RETURN
 300  DO 310 J = 1, LMAX
        EL(J) = ZERO
 310  ENDDO
      EL(3) = ONE
      ALPH0 = -ONE
      ALPH1 = ONE
      PROD = ONE
      XIOLD = ONE
      HSUM = HSCAL
      IF (NQ .EQ. 1) GO TO 340
      DO 330 J = 1, NQM1
        JP1 = J + 1
        HSUM = HSUM + TAU(JP1)
        XI = HSUM/HSCAL
        PROD = PROD*XI
        ALPH0 = ALPH0 - ONE/REAL(JP1)
        ALPH1 = ALPH1 + ONE/XI
        DO 320 IBACK = 1, JP1
          I = (J + 4) - IBACK
          EL(I) = EL(I)*XIOLD + EL(I-1)
 320    ENDDO
        XIOLD = XI
 330    CONTINUE
 340  CONTINUE
      T1 = (-ALPH0 - ALPH1)/PROD
      LP1 = L + 1
      DO 350 I = 1, N
         YH(I,LP1) = T1*YH(I,LMAX)
 350  ENDDO
      NQP1 = NQ + 1
      DO 370 J = 3, NQP1
        CALL DZAXPY (N, EL(J), YH(1,LP1), 1, YH(1,J), 1 )
 370  CONTINUE
      RETURN
      END SUBROUTINE ZVJUST 
*DECK ZVNLSD
      SUBROUTINE ZVNLSD (Y, YH, LDYH, SAVF, EWT, ACOR, IWM, WM,
     1                 F, JAC, PDUM, NFLAG)
      EXTERNAL F, JAC, PDUM
      DOUBLE COMPLEX Y, YH, SAVF, ACOR, WM
      DOUBLE PRECISION EWT
      INTEGER LDYH, IWM, NFLAG
      DIMENSION Y(*), YH(LDYH,*), SAVF(*), EWT(*), ACOR(*),
     1          IWM(*), WM(*)
      DOUBLE PRECISION ACNRM, CCMXJ, CONP, CRATE, DRC, EL,
     1     ETA, ETAMAX, H, HMIN, HMXI, HNEW, HRL1, HSCAL, PRL1,
     2     RC, RL1, SRUR, TAU, TQ, TN, UROUND
      INTEGER ICF, INIT, IPUP, JCUR, JSTART, JSV, KFLAG, KUTH,
     1        L, LMAX, LYH, LEWT, LACOR, LSAVF, LWM, LIWM,
     2        LOCJS, MAXORD, METH, MITER, MSBJ, MXHNIL, MXSTEP,
     3        N, NEWH, NEWQ, NHNIL, NQ, NQNYH, NQWAIT, NSLJ,
     4        NSLP, NYH
      DOUBLE PRECISION HU
      INTEGER NCFN, NETF, NFE, NJE, NLU, NNI, NQU, NST
      DOUBLE PRECISION CCMAX, CRDOWN, CSCALE, DCON, DEL, DELP, ONE,
     1     RDIV, TWO, ZERO
      INTEGER I, IERPJ, IERSL, M, MAXCOR, MSBP
      SAVE CCMAX, CRDOWN, MAXCOR, MSBP, RDIV, ONE, TWO, ZERO
      COMMON /ZVOD01/ ACNRM, CCMXJ, CONP, CRATE, DRC, EL(13), ETA,
     1                ETAMAX, H, HMIN, HMXI, HNEW, HRL1, HSCAL, PRL1,
     2                RC, RL1, SRUR, TAU(13), TQ(5), TN, UROUND,
     3                ICF, INIT, IPUP, JCUR, JSTART, JSV, KFLAG, KUTH,
     4                L, LMAX, LYH, LEWT, LACOR, LSAVF, LWM, LIWM,
     5                LOCJS, MAXORD, METH, MITER, MSBJ, MXHNIL, MXSTEP,
     6                N, NEWH, NEWQ, NHNIL, NQ, NQNYH, NQWAIT, NSLJ,
     7                NSLP, NYH
      COMMON /ZVOD02/ HU, NCFN, NETF, NFE, NJE, NLU, NNI, NQU, NST
      DATA CCMAX /0.3D0/, CRDOWN /0.3D0/, MAXCOR /3/, MSBP /20/,
     1     RDIV  /2.0D0/
      DATA ONE /1.0D0/, TWO /2.0D0/, ZERO /0.0D0/
      IF (JSTART .EQ. 0) NSLP = 0
      IF (NFLAG .EQ. 0) ICF = 0
      IF (NFLAG .EQ. -2) IPUP = MITER
      IF ( (JSTART .EQ. 0) .OR. (JSTART .EQ. -1) ) IPUP = MITER
      IF (MITER .EQ. 0) THEN
        CRATE = ONE
        GO TO 220
      ENDIF
      DRC = ABS(RC-ONE)
      IF (DRC .GT. CCMAX .OR. NST .GE. NSLP+MSBP) IPUP = MITER
 220  M = 0
      DELP = ZERO
      CALL ZCOPY (N, YH(1,1), 1, Y, 1 )
      CALL F (TN, Y, SAVF)
      NFE = NFE + 1
      IF (IPUP .LE. 0) GO TO 250
      CALL ZVJAC (Y, YH, LDYH, EWT, ACOR, SAVF, WM, IWM, F, JAC, IERPJ)
      IPUP = 0
      RC = ONE
      DRC = ZERO
      CRATE = ONE
      NSLP = NST
      IF (IERPJ .NE. 0) GO TO 430
 250  DO 260 I = 1,N
        ACOR(I) = ZERO
 260  ENDDO
 270  IF (MITER .NE. 0) GO TO 350
      DO 280 I = 1,N
        SAVF(I) = RL1*(H*SAVF(I) - YH(I,2))
 280  ENDDO
      DO 290 I = 1,N
        Y(I) = SAVF(I) - ACOR(I)
 290  ENDDO
      DEL = ZVNORM (N, Y, EWT)
      DO 300 I = 1,N
        Y(I) = YH(I,1) + SAVF(I)
 300  ENDDO
      CALL ZCOPY (N, SAVF, 1, ACOR, 1)
      GO TO 400
 350  DO 360 I = 1,N
        Y(I) = (RL1*H)*SAVF(I) - (RL1*YH(I,2) + ACOR(I))
 360  ENDDO
      CALL ZVSOL (WM, IWM, Y, IERSL)
      NNI = NNI + 1
      IF (IERSL .GT. 0) GO TO 410
      IF (METH .EQ. 2 .AND. RC .NE. ONE) THEN
        CSCALE = TWO/(ONE + RC)
        CALL DZSCAL (N, CSCALE, Y, 1)
      ENDIF
      DEL = ZVNORM (N, Y, EWT)
      CALL DZAXPY (N, ONE, Y, 1, ACOR, 1)
      DO 380 I = 1,N
        Y(I) = YH(I,1) + ACOR(I)
 380  ENDDO
 400  IF (M .NE. 0) CRATE = MAX(CRDOWN*CRATE,DEL/DELP)
      DCON = DEL*MIN(ONE,CRATE)/TQ(4)
      IF (DCON .LE. ONE) GO TO 450
      M = M + 1
      IF (M .EQ. MAXCOR) GO TO 410
      IF (M .GE. 2 .AND. DEL .GT. RDIV*DELP) GO TO 410
      DELP = DEL
      CALL F (TN, Y, SAVF)
      NFE = NFE + 1
      GO TO 270
 410  IF (MITER .EQ. 0 .OR. JCUR .EQ. 1) GO TO 430
      ICF = 1
      IPUP = MITER
      GO TO 220
 430  CONTINUE
      NFLAG = -1
      ICF = 2
      IPUP = MITER
      RETURN
 450  NFLAG = 0
      JCUR = 0
      ICF = 0
      IF (M .EQ. 0) ACNRM = DEL
      IF (M .GT. 0) ACNRM = ZVNORM (N, ACOR, EWT)
      RETURN
      END SUBROUTINE ZVNLSD 
*DECK ZVJAC
      SUBROUTINE ZVJAC (Y, YH, LDYH, EWT, FTEM, SAVF, WM, IWM, F, JAC,
     1                 IERPJ)
      EXTERNAL F, JAC
      DOUBLE COMPLEX Y, YH, FTEM, SAVF, WM
      DOUBLE PRECISION EWT
      INTEGER LDYH, IWM, IERPJ
      DIMENSION Y(*), YH(LDYH,*), EWT(*), FTEM(*), SAVF(*),
     1   WM(*), IWM(*)
      DOUBLE PRECISION ACNRM, CCMXJ, CONP, CRATE, DRC, EL,
     1     ETA, ETAMAX, H, HMIN, HMXI, HNEW, HRL1, HSCAL, PRL1,
     2     RC, RL1, SRUR, TAU, TQ, TN, UROUND
      INTEGER ICF, INIT, IPUP, JCUR, JSTART, JSV, KFLAG, KUTH,
     1        L, LMAX, LYH, LEWT, LACOR, LSAVF, LWM, LIWM,
     2        LOCJS, MAXORD, METH, MITER, MSBJ, MXHNIL, MXSTEP,
     3        N, NEWH, NEWQ, NHNIL, NQ, NQNYH, NQWAIT, NSLJ,
     4        NSLP, NYH
      DOUBLE PRECISION HU
      INTEGER NCFN, NETF, NFE, NJE, NLU, NNI, NQU, NST
      DOUBLE COMPLEX DI, R1, YI, YJ, YJJ
      DOUBLE PRECISION CON, FAC, ONE, PT1, R, R0, THOU, ZERO
      INTEGER I, I1, I2, IER, II, J, J1, JJ, JOK, LENP, MBA, MBAND,
     1        MEB1, MEBAND, ML, ML1, MU, NP1
      SAVE ONE, PT1, THOU, ZERO
      COMMON /ZVOD01/ ACNRM, CCMXJ, CONP, CRATE, DRC, EL(13), ETA,
     1                ETAMAX, H, HMIN, HMXI, HNEW, HRL1, HSCAL, PRL1,
     2                RC, RL1, SRUR, TAU(13), TQ(5), TN, UROUND,
     3                ICF, INIT, IPUP, JCUR, JSTART, JSV, KFLAG, KUTH,
     4                L, LMAX, LYH, LEWT, LACOR, LSAVF, LWM, LIWM,
     5                LOCJS, MAXORD, METH, MITER, MSBJ, MXHNIL, MXSTEP,
     6                N, NEWH, NEWQ, NHNIL, NQ, NQNYH, NQWAIT, NSLJ,
     7                NSLP, NYH
      COMMON /ZVOD02/ HU, NCFN, NETF, NFE, NJE, NLU, NNI, NQU, NST
      DATA ONE /1.0D0/, THOU /1000.0D0/, ZERO /0.0D0/, PT1 /0.1D0/
      IERPJ = 0
      HRL1 = H*RL1
      JOK = JSV
      IF (JSV .EQ. 1) THEN
        IF (NST .EQ. 0 .OR. NST .GT. NSLJ+MSBJ) JOK = -1
        IF (ICF .EQ. 1 .AND. DRC .LT. CCMXJ) JOK = -1
        IF (ICF .EQ. 2) JOK = -1
      ENDIF
      IF (JOK .EQ. -1 .AND. MITER .EQ. 1) THEN
      NJE = NJE + 1
      NSLJ = NST
      JCUR = 1
      LENP = N*N
      DO 110 I = 1,LENP
        WM(I) = ZERO
 110  ENDDO
      CALL JAC (N, TN, Y, 0, 0, WM, N)
      IF (JSV .EQ. 1) CALL ZCOPY (LENP, WM, 1, WM(LOCJS), 1)
      ENDIF
      IF (JOK .EQ. -1 .AND. MITER .EQ. 2) THEN
      NJE = NJE + 1
      NSLJ = NST
      JCUR = 1
      FAC = ZVNORM (N, SAVF, EWT)
      R0 = THOU*ABS(H)*UROUND*REAL(N)*FAC
      IF (R0 .EQ. ZERO) R0 = ONE
      J1 = 0
      DO 230 J = 1,N
        YJ = Y(J)
        R = MAX(SRUR*ABS(YJ),R0/EWT(J))
        Y(J) = Y(J) + R
        FAC = ONE/R
        CALL F (TN, Y, FTEM)
        DO 220 I = 1,N
          WM(I+J1) = (FTEM(I) - SAVF(I))*FAC
 220    ENDDO
        Y(J) = YJ
        J1 = J1 + N
 230    CONTINUE
      NFE = NFE + N
      LENP = N*N
      IF (JSV .EQ. 1) CALL ZCOPY (LENP, WM, 1, WM(LOCJS), 1)
      ENDIF
      IF (JOK .EQ. 1 .AND. (MITER .EQ. 1 .OR. MITER .EQ. 2)) THEN
      JCUR = 0
      LENP = N*N
      CALL ZCOPY (LENP, WM(LOCJS), 1, WM, 1)
      ENDIF
      IF (MITER .EQ. 1 .OR. MITER .EQ. 2) THEN
      CON = -HRL1
      CALL DZSCAL (LENP, CON, WM, 1)
      J = 1
      NP1 = N + 1
      DO 250 I = 1,N
        WM(J) = WM(J) + ONE
        J = J + NP1
 250  ENDDO
      NLU = NLU + 1
      CALL ZGEFA (WM, N, N, IWM(31), IER)
      IF (IER .NE. 0) IERPJ = 1
      RETURN
      ENDIF
      IF (MITER .EQ. 3) THEN
      NJE = NJE + 1
      JCUR = 1
      R = RL1*PT1
      DO 310 I = 1,N
        Y(I) = Y(I) + R*(H*SAVF(I) - YH(I,2))
 310  ENDDO
      CALL F (TN, Y, WM)
      NFE = NFE + 1
      DO 320 I = 1,N
        R1 = H*SAVF(I) - YH(I,2)
        DI = PT1*R1 - H*(WM(I) - SAVF(I))
        WM(I) = ONE
        IF (ABS(R1) .LT. UROUND/EWT(I)) GO TO 320
        IF (ABS(DI) .EQ. ZERO) GO TO 330
        WM(I) = PT1*R1/DI
 320    CONTINUE
      RETURN
 330  IERPJ = 1
      RETURN
      ENDIF
      ML = IWM(1)
      MU = IWM(2)
      ML1 = ML + 1
      MBAND = ML + MU + 1
      MEBAND = MBAND + ML
      LENP = MEBAND*N
      IF (JOK .EQ. -1 .AND. MITER .EQ. 4) THEN
      NJE = NJE + 1
      NSLJ = NST
      JCUR = 1
      DO 410 I = 1,LENP
        WM(I) = ZERO
 410  ENDDO
      CALL JAC (N, TN, Y, ML, MU, WM(ML1), MEBAND)
      IF (JSV .EQ. 1)
     1   CALL ZACOPY (MBAND, N, WM(ML1), MEBAND, WM(LOCJS), MBAND)
      ENDIF
      IF (JOK .EQ. -1 .AND. MITER .EQ. 5) THEN
      NJE = NJE + 1
      NSLJ = NST
      JCUR = 1
      MBA = MIN(MBAND,N)
      MEB1 = MEBAND - 1
      FAC = ZVNORM (N, SAVF, EWT)
      R0 = THOU*ABS(H)*UROUND*REAL(N)*FAC
      IF (R0 .EQ. ZERO) R0 = ONE
      DO 560 J = 1,MBA
        DO 530 I = J,N,MBAND
          YI = Y(I)
          R = MAX(SRUR*ABS(YI),R0/EWT(I))
          Y(I) = Y(I) + R
 530    ENDDO
        CALL F (TN, Y, FTEM)
        DO 550 JJ = J,N,MBAND
          Y(JJ) = YH(JJ,1)
          YJJ = Y(JJ)
          R = MAX(SRUR*ABS(YJJ),R0/EWT(JJ))
          FAC = ONE/R
          I1 = MAX(JJ-MU,1)
          I2 = MIN(JJ+ML,N)
          II = JJ*MEB1 - ML
          DO 540 I = I1,I2
            WM(II+I) = (FTEM(I) - SAVF(I))*FAC
 540      ENDDO
 550      CONTINUE
 560    CONTINUE
      NFE = NFE + MBA
      IF (JSV .EQ. 1)
     1   CALL ZACOPY (MBAND, N, WM(ML1), MEBAND, WM(LOCJS), MBAND)
      ENDIF
      IF (JOK .EQ. 1) THEN
      JCUR = 0
      CALL ZACOPY (MBAND, N, WM(LOCJS), MBAND, WM(ML1), MEBAND)
      ENDIF
      CON = -HRL1
      CALL DZSCAL (LENP, CON, WM, 1 )
      II = MBAND
      DO 580 I = 1,N
        WM(II) = WM(II) + ONE
        II = II + MEBAND
 580  ENDDO
      NLU = NLU + 1
      CALL ZGBFA (WM, MEBAND, N, ML, MU, IWM(31), IER)
      IF (IER .NE. 0) IERPJ = 1
      RETURN
      END SUBROUTINE ZVJAC 
*DECK ZACOPY
      SUBROUTINE ZACOPY (NROW, NCOL, A, NROWA, B, NROWB)
      DOUBLE COMPLEX A, B
      INTEGER NROW, NCOL, NROWA, NROWB
      DIMENSION A(NROWA,NCOL), B(NROWB,NCOL)
      INTEGER IC
      DO 20 IC = 1,NCOL
        CALL ZCOPY (NROW, A(1,IC), 1, B(1,IC), 1)
 20     CONTINUE
      RETURN
      END SUBROUTINE ZACOPY
*DECK ZVSOL
      SUBROUTINE ZVSOL (WM, IWM, X, IERSL)
      DOUBLE COMPLEX WM, X
      INTEGER IWM, IERSL
      DIMENSION WM(*), IWM(*), X(*)
      DOUBLE PRECISION ACNRM, CCMXJ, CONP, CRATE, DRC, EL,
     1     ETA, ETAMAX, H, HMIN, HMXI, HNEW, HRL1, HSCAL, PRL1,
     2     RC, RL1, SRUR, TAU, TQ, TN, UROUND
      INTEGER ICF, INIT, IPUP, JCUR, JSTART, JSV, KFLAG, KUTH,
     1        L, LMAX, LYH, LEWT, LACOR, LSAVF, LWM, LIWM,
     2        LOCJS, MAXORD, METH, MITER, MSBJ, MXHNIL, MXSTEP,
     3        N, NEWH, NEWQ, NHNIL, NQ, NQNYH, NQWAIT, NSLJ,
     4        NSLP, NYH
      DOUBLE COMPLEX DI
      DOUBLE PRECISION ONE, PHRL1, R, ZERO
      INTEGER I, MEBAND, ML, MU
      SAVE ONE, ZERO
      COMMON /ZVOD01/ ACNRM, CCMXJ, CONP, CRATE, DRC, EL(13), ETA,
     1                ETAMAX, H, HMIN, HMXI, HNEW, HRL1, HSCAL, PRL1,
     2                RC, RL1, SRUR, TAU(13), TQ(5), TN, UROUND,
     3                ICF, INIT, IPUP, JCUR, JSTART, JSV, KFLAG, KUTH,
     4                L, LMAX, LYH, LEWT, LACOR, LSAVF, LWM, LIWM,
     5                LOCJS, MAXORD, METH, MITER, MSBJ, MXHNIL, MXSTEP,
     6                N, NEWH, NEWQ, NHNIL, NQ, NQNYH, NQWAIT, NSLJ,
     7                NSLP, NYH
      DATA ONE /1.0D0/, ZERO /0.0D0/
      IERSL = 0
      GO TO (100, 100, 300, 400, 400), MITER
 100  CALL ZGESL (WM, N, N, IWM(31), X, 0)
      RETURN
 300  PHRL1 = HRL1
      HRL1 = H*RL1
      IF (HRL1 .EQ. PHRL1) GO TO 330
      R = HRL1/PHRL1
      DO 320 I = 1,N
        DI = ONE - R*(ONE - ONE/WM(I))
        IF (ABS(DI) .EQ. ZERO) GO TO 390
        WM(I) = ONE/DI
 320  ENDDO
 330  DO 340 I = 1,N
        X(I) = WM(I)*X(I)
 340  ENDDO
      RETURN
 390  IERSL = 1
      RETURN
 400  ML = IWM(1)
      MU = IWM(2)
      MEBAND = 2*ML + MU + 1
      CALL ZGBSL (WM, MEBAND, N, ML, MU, IWM(31), X, 0)
      RETURN
      END SUBROUTINE ZVSOL 
*DECK ZVSRCO
      SUBROUTINE ZVSRCO (RSAV, ISAV, JOB)
      DOUBLE PRECISION RSAV
      INTEGER ISAV, JOB
      DIMENSION RSAV(*), ISAV(*)
      DOUBLE PRECISION RVOD1, RVOD2
      INTEGER IVOD1, IVOD2
      INTEGER I, LENIV1, LENIV2, LENRV1, LENRV2
      SAVE LENRV1, LENIV1, LENRV2, LENIV2
      COMMON /ZVOD01/ RVOD1(50), IVOD1(33)
      COMMON /ZVOD02/ RVOD2(1), IVOD2(8)
      DATA LENRV1/50/, LENIV1/33/, LENRV2/1/, LENIV2/8/
      IF (JOB .EQ. 2) GO TO 100
      DO 10 I = 1,LENRV1
        RSAV(I) = RVOD1(I)
 10   ENDDO
      DO 15 I = 1,LENRV2
        RSAV(LENRV1+I) = RVOD2(I)
 15   ENDDO
      DO 20 I = 1,LENIV1
        ISAV(I) = IVOD1(I)
 20   ENDDO
      DO 25 I = 1,LENIV2
        ISAV(LENIV1+I) = IVOD2(I)
 25   ENDDO
      RETURN
 100  CONTINUE
      DO 110 I = 1,LENRV1
         RVOD1(I) = RSAV(I)
 110  ENDDO
      DO 115 I = 1,LENRV2
         RVOD2(I) = RSAV(LENRV1+I)
 115  ENDDO
      DO 120 I = 1,LENIV1
         IVOD1(I) = ISAV(I)
 120  ENDDO
      DO 125 I = 1,LENIV2
         IVOD2(I) = ISAV(LENIV1+I)
 125  ENDDO
      RETURN
      END subroutine zvsrco
*DECK ZEWSET
      SUBROUTINE ZEWSET (N, ITOL, RTOL, ATOL, YCUR, EWT)
      DOUBLE COMPLEX YCUR
      DOUBLE PRECISION RTOL, ATOL, EWT
      INTEGER N, ITOL
      INTEGER I
      DIMENSION RTOL(*), ATOL(*), YCUR(N), EWT(N)
      GO TO (10, 20, 30, 40), ITOL
 10   CONTINUE
      DO 15 I = 1,N
        EWT(I) = RTOL(1)*ABS(YCUR(I)) + ATOL(1)
 15   ENDDO
      RETURN
 20   CONTINUE
      DO 25 I = 1,N
        EWT(I) = RTOL(1)*ABS(YCUR(I)) + ATOL(I)
 25   ENDDO
      RETURN
 30   CONTINUE
      DO 35 I = 1,N
        EWT(I) = RTOL(I)*ABS(YCUR(I)) + ATOL(1)
 35   ENDDO
      RETURN
 40   CONTINUE
      DO 45 I = 1,N
        EWT(I) = RTOL(I)*ABS(YCUR(I)) + ATOL(I)
 45   ENDDO
      RETURN
      END subroutine zewset
*DECK ZVNORM
      DOUBLE PRECISION FUNCTION ZVNORM (N, V, W)
      DOUBLE COMPLEX V
      DOUBLE PRECISION W,   SUM
      INTEGER N,   I
      DIMENSION V(N), W(N)
      SUM = 0.0D0
      DO 10 I = 1,N
        SUM = SUM + ZABSSQ(V(I)) * W(I)**2
 10   ENDDO
      ZVNORM = SQRT(SUM/N)
      RETURN
      END function zvnorm
*DECK ZABSSQ
      DOUBLE PRECISION FUNCTION ZABSSQ(Z)
      DOUBLE COMPLEX Z
      ZABSSQ = DREAL(Z)**2 + DIMAG(Z)**2
      RETURN
      END function zabssq
*DECK DZSCAL
      SUBROUTINE DZSCAL(N, DA, ZX, INCX)
      DOUBLE COMPLEX ZX(*)
      DOUBLE PRECISION DA
      INTEGER I,INCX,IX,N
      IF( N.LE.0 .OR. INCX.LE.0 )RETURN
      IF(INCX.EQ.1)GO TO 20
      IX = 1
      DO 10 I = 1,N
        ZX(IX) = DA*ZX(IX)
        IX = IX + INCX
   10 CONTINUE
      RETURN
   20 DO 30 I = 1,N
        ZX(I) = DA*ZX(I)
   30 CONTINUE
      RETURN
      END subroutine dzscal
*DECK DZAXPY
      SUBROUTINE DZAXPY(N, DA, ZX, INCX, ZY, INCY)
      DOUBLE COMPLEX ZX(*),ZY(*)
      DOUBLE PRECISION DA
      INTEGER I,INCX,INCY,IX,IY,N
      IF(N.LE.0)RETURN
      IF (ABS(DA) .EQ. 0.0D0) RETURN
      IF (INCX.EQ.1.AND.INCY.EQ.1)GO TO 20
      IX = 1
      IY = 1
      IF(INCX.LT.0)IX = (-N+1)*INCX + 1
      IF(INCY.LT.0)IY = (-N+1)*INCY + 1
      DO 10 I = 1,N
        ZY(IY) = ZY(IY) + DA*ZX(IX)
        IX = IX + INCX
        IY = IY + INCY
   10 CONTINUE
      RETURN
   20 DO 30 I = 1,N
        ZY(I) = ZY(I) + DA*ZX(I)
   30 CONTINUE
      RETURN
      END subroutine dzaxpy
*DECK DUMACH
      DOUBLE PRECISION FUNCTION DUMACH ()
      DOUBLE PRECISION U, COMP
      U = 1.0D0
 10   U = U*0.5D0
      CALL DUMSUM(1.0D0, U, COMP)
      IF (COMP .NE. 1.0D0) GO TO 10
      DUMACH = U*2.0D0
      RETURN
      END function dumach

      SUBROUTINE DUMSUM(A,B,C)
      DOUBLE PRECISION A, B, C
      C = A + B
      RETURN
      END subroutine dumsum

*DECK XERRWD
      SUBROUTINE XERRWD (MSG, LEVEL, NI, I1, I2, NR, R1, R2)
      DOUBLE PRECISION R1, R2
      INTEGER LEVEL, NI, I1, I2, NR
      CHARACTER*(*) MSG
      INTEGER LUNIT, MESFLG
      LUNIT = IXSAV (1, 0, .FALSE.)
      MESFLG = IXSAV (2, 0, .FALSE.)
      IF (MESFLG .EQ. 0) GO TO 100
      WRITE (LUNIT,10)  MSG
 10   FORMAT(1X,A)
      IF (NI .EQ. 1) WRITE (LUNIT, 20) I1
 20   FORMAT(6X,'In above message,  I1 =',I10)
      IF (NI .EQ. 2) WRITE (LUNIT, 30) I1,I2
 30   FORMAT(6X,'In above message,  I1 =',I10,3X,'I2 =',I10)
      IF (NR .EQ. 1) WRITE (LUNIT, 40) R1
 40   FORMAT(6X,'In above message,  R1 =',D21.13)
      IF (NR .EQ. 2) WRITE (LUNIT, 50) R1,R2
 50   FORMAT(6X,'In above,  R1 =',D21.13,3X,'R2 =',D21.13)
 100  IF (LEVEL .NE. 2) RETURN
      STOP
      END subroutine XERRWD
*DECK XSETF
      SUBROUTINE XSETF (MFLAG)
      INTEGER MFLAG, JUNK
      IF (MFLAG .EQ. 0 .OR. MFLAG .EQ. 1) JUNK = IXSAV (2,MFLAG,.TRUE.)
      RETURN
      END subroutine xsetf
*DECK XSETUN
      SUBROUTINE XSETUN (LUN)
      INTEGER LUN, JUNK
      IF (LUN .GT. 0) JUNK = IXSAV (1,LUN,.TRUE.)
      RETURN
      END subroutine xsetun
*DECK IXSAV
      INTEGER FUNCTION IXSAV (IPAR, IVALUE, ISET)
      LOGICAL ISET
      INTEGER IPAR, IVALUE
      INTEGER LUNIT, MESFLG
      SAVE LUNIT, MESFLG
      DATA LUNIT/-1/, MESFLG/1/
      IF (IPAR .EQ. 1) THEN
        IF (LUNIT .EQ. -1) LUNIT = IUMACH()
        IXSAV = LUNIT
        IF (ISET) LUNIT = IVALUE
        ENDIF
      IF (IPAR .EQ. 2) THEN
        IXSAV = MESFLG
        IF (ISET) MESFLG = IVALUE
        ENDIF
      RETURN
      END function ixsav

*DECK IUMACH
      INTEGER FUNCTION IUMACH()
      IUMACH = 6
      RETURN
      END function iumach


*DECK ZGEFA
      subroutine zgefa(a,lda,n,ipvt,info)
      integer lda,n,ipvt(1),info
      complex*16 a(lda,1)
c
c     zgefa factors a complex*16 matrix by gaussian elimination.
c
c     zgefa is usually called by zgeco, but it can be called
c     directly with a saving in time if  rcond  is not needed.
c     (time for zgeco) = (1 + 9/n)*(time for zgefa) .
c
c     on entry
c
c        a       complex*16(lda, n)
c                the matrix to be factored.
c
c        lda     integer
c                the leading dimension of the array  a .
c
c        n       integer
c                the order of the matrix  a .
c
c     on return
c
c        a       an upper triangular matrix and the multipliers
c                which were used to obtain it.
c                the factorization can be written  a = l*u  where
c                l  is a product of permutation and unit lower
c                triangular matrices and  u  is upper triangular.
c
c        ipvt    integer(n)
c                an integer vector of pivot indices.
c
c        info    integer
c                = 0  normal value.
c                = k  if  u(k,k) .eq. 0.0 .  this is not an error
c                     condition for this subroutine, but it does
c                     indicate that zgesl or zgedi will divide by zero
c                     if called.  use  rcond  in zgeco for a reliable
c                     indication of singularity.
c
c     linpack. this version dated 08/14/78 .
c     cleve moler, university of new mexico, argonne national lab.
c
c     subroutines and functions
c
c     blas zaxpy,zscal,izamax
c     fortran dabs
c
c     internal variables
c
      complex*16 t
      integer izamax,j,k,kp1,l,nm1
c
      complex*16 zdum
      double precision cabs1
      double precision dreal,dimag
      complex*16 zdumr,zdumi
      dreal(zdumr) = zdumr
      dimag(zdumi) = (0.0d0,-1.0d0)*zdumi
      cabs1(zdum) = dabs(dreal(zdum)) + dabs(dimag(zdum))
c
c     gaussian elimination with partial pivoting
c
      info = 0
      nm1 = n - 1
      if (nm1 .lt. 1) go to 70
      do 60 k = 1, nm1
         kp1 = k + 1
c
c        find l = pivot index
c
         l = izamax(n-k+1,a(k,k),1) + k - 1
         ipvt(k) = l
c
c        zero pivot implies this column already triangularized
c
         if (cabs1(a(l,k)) .eq. 0.0d0) go to 40
c
c           interchange if necessary
c
            if (l .eq. k) go to 10
               t = a(l,k)
               a(l,k) = a(k,k)
               a(k,k) = t
   10       continue
c
c           compute multipliers
c
            t = -(1.0d0,0.0d0)/a(k,k)
            call zscal(n-k,t,a(k+1,k),1)
c
c           row elimination with column indexing
c
            do 30 j = kp1, n
               t = a(l,j)
               if (l .eq. k) go to 20
                  a(l,j) = a(k,j)
                  a(k,j) = t
   20          continue
               call zaxpy(n-k,t,a(k+1,k),1,a(k+1,j),1)
   30       continue
         go to 50
   40    continue
            info = k
   50    continue
   60 continue
   70 continue
      ipvt(n) = n
      if (cabs1(a(n,n)) .eq. 0.0d0) info = n
      return
      end subroutine zgefa


*DECK ZGESL
      subroutine zgesl(a,lda,n,ipvt,b,job)
      integer lda,n,ipvt(1),job
      complex*16 a(lda,1),b(1)
c
c     zgesl solves the complex*16 system
c     a * x = b  or  ctrans(a) * x = b
c     using the factors computed by zgeco or zgefa.
c
c     on entry
c
c        a       complex*16(lda, n)
c                the output from zgeco or zgefa.
c
c        lda     integer
c                the leading dimension of the array  a .
c
c        n       integer
c                the order of the matrix  a .
c
c        ipvt    integer(n)
c                the pivot vector from zgeco or zgefa.
c
c        b       complex*16(n)
c                the right hand side vector.
c
c        job     integer
c                = 0         to solve  a*x = b ,
c                = nonzero   to solve  ctrans(a)*x = b  where
c                            ctrans(a)  is the conjugate transpose.
c
c     on return
c
c        b       the solution vector  x .
c
c     error condition
c
c        a division by zero will occur if the input factor contains a
c        zero on the diagonal.  technically this indicates singularity
c        but it is often caused by improper arguments or improper
c        setting of lda .  it will not occur if the subroutines are
c        called correctly and if zgeco has set rcond .gt. 0.0
c        or zgefa has set info .eq. 0 .
c
c     to compute  inverse(a) * c  where  c  is a matrix
c     with  p  columns
c           call zgeco(a,lda,n,ipvt,rcond,z)
c           if (rcond is too small) go to ...
c           do 10 j = 1, p
c              call zgesl(a,lda,n,ipvt,c(1,j),0)
c        10 continue
c
c     linpack. this version dated 08/14/78 .
c     cleve moler, university of new mexico, argonne national lab.
c
c     subroutines and functions
c
c     blas zaxpy,zdotc
c     fortran dconjg
c
c     internal variables
c
      complex*16 zdotc,t
      integer k,kb,l,nm1
      complex*16 zdumr,zdumi
c      real dreal
c      real dimag 

      dreal(zdumr) = zdumr
      dimag(zdumi) = (0.0d0,-1.0d0)*zdumi
c
      nm1 = n - 1
      if (job .ne. 0) go to 50
c
c        job = 0 , solve  a * x = b
c        first solve  l*y = b
c
         if (nm1 .lt. 1) go to 30
         do 20 k = 1, nm1
            l = ipvt(k)
            t = b(l)
            if (l .eq. k) go to 10
               b(l) = b(k)
               b(k) = t
   10       continue
            call zaxpy(n-k,t,a(k+1,k),1,b(k+1),1)
   20    continue
   30    continue
c
c        now solve  u*x = y
c
         do 40 kb = 1, n
            k = n + 1 - kb
            b(k) = b(k)/a(k,k)
            t = -b(k)
            call zaxpy(k-1,t,a(1,k),1,b(1),1)
   40    continue
      go to 100
   50 continue
c
c        job = nonzero, solve  ctrans(a) * x = b
c        first solve  ctrans(u)*y = b
c
         do 60 k = 1, n
            t = zdotc(k-1,a(1,k),1,b(1),1)
            b(k) = (b(k) - t)/dconjg(a(k,k))
   60    continue
c
c        now solve ctrans(l)*x = y
c
         if (nm1 .lt. 1) go to 90
         do 80 kb = 1, nm1
            k = n - kb
            b(k) = b(k) + zdotc(n-k,a(k+1,k),1,b(k+1),1)
            l = ipvt(k)
            if (l .eq. k) go to 70
               t = b(l)
               b(l) = b(k)
               b(k) = t
   70       continue
   80    continue
   90    continue
  100 continue
      return
      end subroutine zgesl


*DECK ZGBFA
      subroutine zgbfa(abd,lda,n,ml,mu,ipvt,info)
      integer lda,n,ml,mu,ipvt(1),info
      complex*16 abd(lda,1)

!>     zgbfa factors a complex*16 band matrix by elimination.
!!
!!     zgbfa is usually called by zgbco, but it can be called
!!     directly with a saving in time if  rcond  is not needed.
c
c     on entry
c
c        abd     complex*16(lda, n)
c                contains the matrix in band storage.  the columns
c                of the matrix are stored in the columns of  abd  and
c                the diagonals of the matrix are stored in rows
c                ml+1 through 2*ml+mu+1 of  abd .
c                see the comments below for details.
c
c        lda     integer
c                the leading dimension of the array  abd .
c                lda must be .ge. 2*ml + mu + 1 .
c
c        n       integer
c                the order of the original matrix.
c
c        ml      integer
c                number of diagonals below the main diagonal.
c                0 .le. ml .lt. n .
c
c        mu      integer
c                number of diagonals above the main diagonal.
c                0 .le. mu .lt. n .
c                more efficient if  ml .le. mu .
c     on return
c
c        abd     an upper triangular matrix in band storage and
c                the multipliers which were used to obtain it.
c                the factorization can be written  a = l*u  where
c                l  is a product of permutation and unit lower
c                triangular matrices and  u  is upper triangular.
c
c        ipvt    integer(n)
c                an integer vector of pivot indices.
c
c        info    integer
c                = 0  normal value.
c                = k  if  u(k,k) .eq. 0.0 .  this is not an error
c                     condition for this subroutine, but it does
c                     indicate that zgbsl will divide by zero if
c                     called.  use  rcond  in zgbco for a reliable
c                     indication of singularity.
c
c     band storage
c
c           if  a  is a band matrix, the following program segment
c           will set up the input.
c
c                   ml = (band width below the diagonal)
c                   mu = (band width above the diagonal)
c                   m = ml + mu + 1
c                   do 20 j = 1, n
c                      i1 = max0(1, j-mu)
c                      i2 = min0(n, j+ml)
c                      do 10 i = i1, i2
c                         k = i - j + m
c                         abd(k,j) = a(i,j)
c                10    continue
c                20 continue
c
c           this uses rows  ml+1  through  2*ml+mu+1  of  abd .
c           in addition, the first  ml  rows in  abd  are used for
c           elements generated during the triangularization.
c           the total number of rows needed in  abd  is  2*ml+mu+1 .
c           the  ml+mu by ml+mu  upper left triangle and the
c           ml by ml  lower right triangle are not referenced.
c
c     linpack. this version dated 08/14/78 .
c     cleve moler, university of new mexico, argonne national lab.
c
c     subroutines and functions
c
c     blas zaxpy,zscal,izamax
c     fortran dabs,max0,min0
c
c     internal variables
c
      complex*16 t
      integer i,izamax,i0,j,ju,jz,j0,j1,k,kp1,l,lm,m,mm,nm1
c
      complex*16 zdum
      double precision cabs1
      double precision dreal,dimag
      complex*16 zdumr,zdumi
      dreal(zdumr) = zdumr
      dimag(zdumi) = (0.0d0,-1.0d0)*zdumi
      cabs1(zdum) = dabs(dreal(zdum)) + dabs(dimag(zdum))
c
      m = ml + mu + 1
      info = 0
c
c     zero initial fill-in columns
c
      j0 = mu + 2
      j1 = min0(n,m) - 1
      if (j1 .lt. j0) go to 30
      do 20 jz = j0, j1
         i0 = m + 1 - jz
         do 10 i = i0, ml
            abd(i,jz) = (0.0d0,0.0d0)
   10    continue
   20 continue
   30 continue
      jz = j1
      ju = 0
c
c     gaussian elimination with partial pivoting
c
      nm1 = n - 1
      if (nm1 .lt. 1) go to 130
      do 120 k = 1, nm1
         kp1 = k + 1
c
c        zero next fill-in column
c
         jz = jz + 1
         if (jz .gt. n) go to 50
         if (ml .lt. 1) go to 50
            do 40 i = 1, ml
               abd(i,jz) = (0.0d0,0.0d0)
   40       continue
   50    continue
c
c        find l = pivot index
c
         lm = min0(ml,n-k)
         l = izamax(lm+1,abd(m,k),1) + m - 1
         ipvt(k) = l + k - m
c
c        zero pivot implies this column already triangularized
c
         if (cabs1(abd(l,k)) .eq. 0.0d0) go to 100
c
c           interchange if necessary
c
            if (l .eq. m) go to 60
               t = abd(l,k)
               abd(l,k) = abd(m,k)
               abd(m,k) = t
   60       continue
c
c           compute multipliers
c
            t = -(1.0d0,0.0d0)/abd(m,k)
            call zscal(lm,t,abd(m+1,k),1)
c
c           row elimination with column indexing
c
            ju = min0(max0(ju,mu+ipvt(k)),n)
            mm = m
            if (ju .lt. kp1) go to 90
            do 80 j = kp1, ju
               l = l - 1
               mm = mm - 1
               t = abd(l,j)
               if (l .eq. mm) go to 70
                  abd(l,j) = abd(mm,j)
                  abd(mm,j) = t
   70          continue
               call zaxpy(lm,t,abd(m+1,k),1,abd(mm+1,j),1)
   80       continue
   90       continue
         go to 110
  100    continue
            info = k
  110    continue
  120 continue
  130 continue
      ipvt(n) = n
      if (cabs1(abd(m,n)) .eq. 0.0d0) info = n
      return
      end subroutine zgbfa



*DECK ZGBSL
      subroutine zgbsl(abd,lda,n,ml,mu,ipvt,b,job)
      integer lda,n,ml,mu,ipvt(1),job
      complex*16 abd(lda,1),b(1)

!>     zgbsl solves the complex*16 band system
!!     a * x = b  or  ctrans(a) * x = b
!!     using the factors computed by zgbco or zgbfa.
c
c     on entry
c
c        abd     complex*16(lda, n)
c                the output from zgbco or zgbfa.
c
c        lda     integer
c                the leading dimension of the array  abd .
c
c        n       integer
c                the order of the original matrix.
c
c        ml      integer
c                number of diagonals below the main diagonal.
c
c        mu      integer
c                number of diagonals above the main diagonal.
c
c        ipvt    integer(n)
c                the pivot vector from zgbco or zgbfa.
c
c        b       complex*16(n)
c                the right hand side vector.
c
c        job     integer
c                = 0         to solve  a*x = b ,
c                = nonzero   to solve  ctrans(a)*x = b , where
c                            ctrans(a)  is the conjugate transpose.
c
c     on return
c
c        b       the solution vector  x .
c
c     error condition
c
c        a division by zero will occur if the input factor contains a
c        zero on the diagonal.  technically this indicates singularity
c        but it is often caused by improper arguments or improper
c        setting of lda .  it will not occur if the subroutines are
c        called correctly and if zgbco has set rcond .gt. 0.0
c        or zgbfa has set info .eq. 0 .
c
c     to compute  inverse(a) * c  where  c  is a matrix
c     with  p  columns
c           call zgbco(abd,lda,n,ml,mu,ipvt,rcond,z)
c           if (rcond is too small) go to ...
c           do 10 j = 1, p
c              call zgbsl(abd,lda,n,ml,mu,ipvt,c(1,j),0)
c        10 continue
c
c     linpack. this version dated 08/14/78 .
c     cleve moler, university of new mexico, argonne national lab.
c
c     subroutines and functions
c
c     blas zaxpy,zdotc
c     fortran dconjg,min0
c
c     internal variables
c
      complex*16 zdotc,t
      integer k,kb,l,la,lb,lm,m,nm1
      complex*16 zdumr,zdumi
c      real dreal
c      real dimag

      dreal(zdumr) = zdumr
      dimag(zdumi) = (0.0d0,-1.0d0)*zdumi
c
      m = mu + ml + 1
      nm1 = n - 1
      if (job .ne. 0) go to 50
c
c        job = 0 , solve  a * x = b
c        first solve l*y = b
c
         if (ml .eq. 0) go to 30
         if (nm1 .lt. 1) go to 30
            do 20 k = 1, nm1
               lm = min0(ml,n-k)
               l = ipvt(k)
               t = b(l)
               if (l .eq. k) go to 10
                  b(l) = b(k)
                  b(k) = t
   10          continue
               call zaxpy(lm,t,abd(m+1,k),1,b(k+1),1)
   20       continue
   30    continue
c
c        now solve  u*x = y
c
         do 40 kb = 1, n
            k = n + 1 - kb
            b(k) = b(k)/abd(m,k)
            lm = min0(k,m) - 1
            la = m - lm
            lb = k - lm
            t = -b(k)
            call zaxpy(lm,t,abd(la,k),1,b(lb),1)
   40    continue
      go to 100
   50 continue
c
c        job = nonzero, solve  ctrans(a) * x = b
c        first solve  ctrans(u)*y = b
c
         do 60 k = 1, n
            lm = min0(k,m) - 1
            la = m - lm
            lb = k - lm
            t = zdotc(lm,abd(la,k),1,b(lb),1)
            b(k) = (b(k) - t)/dconjg(abd(m,k))
   60    continue
c
c        now solve ctrans(l)*x = y
c
         if (ml .eq. 0) go to 90
         if (nm1 .lt. 1) go to 90
            do 80 kb = 1, nm1
               k = n - kb
               lm = min0(ml,n-k)
               b(k) = b(k) + zdotc(lm,abd(m+1,k),1,b(k+1),1)
               l = ipvt(k)
               if (l .eq. k) go to 70
                  t = b(l)
                  b(l) = b(k)
                  b(k) = t
   70          continue
   80       continue
   90    continue
  100 continue
      return
      end subroutine zgbsl



      SUBROUTINE JEX (NEQ, T, W, ML, MU, PD, NRPD, RPAR, IPAR)
!> Dummy routine needed for ZVODE (provide the jacobian of the function to be integrated.
!> \todo Either remove or write something useful inside.   
      DOUBLE COMPLEX W(NEQ), PD(NRPD,NEQ), RPAR
      DOUBLE PRECISION T

      write(6,*) 'I am computing a non-existant Jacobian !'
      stop

      RETURN
      END SUBROUTINE JEX 

      END MODULE Stiff_Integrator
