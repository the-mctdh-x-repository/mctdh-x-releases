!>GPLv3
!    MCTDH-X: the multiconfigurational time-depENDent Hartree for 
!    indistinguishable particles software
!
!    Copyright (C) 2021  A. U. J. Lode, M. C. Tsatsos, 
!                        E. Fasshauer, S. E. Weiner, 
!                        R. Lin, L. Papariello, P. Molignini, 
!                        C. Lévêque, M. Büttner , J. Xiang, S. Dutta
!
!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <https://www.gnu.org/licenses/>.
!
!
!
!
!<GPLv3










MODULE Random
CONTAINS

  !> @ingroup random
  !! @brief PRNG for REAL*8 numbers
  !!
  !! Constructs a pseudo-random number \f$R \f$ from two consecutive pseudo-random numbers 
  !! (\f$N_1 \f$ and \f$N_2 \f$) by calculating \f$ R = \sqrt{-2\log(N_1)} \cdot \cos\Big( 2\pi N_2 \Big) \f$
  !!
  !! @param[inout] R : random number, REAL*8
  SUBROUTINE gnu_rand8_normal(R)
    IMPLICIT NONE
    REAL*8 :: R

    REAL*8 :: Num1,Num2
    REAL*8,parameter ::  pi=3.141592653589793D0

    CALL gnu_rand(Num1)
    CALL gnu_rand(Num2)

    R=sqrt(-2.d0 * log(Num1))*cos(2.d0*pi*Num2)

  END SUBROUTINE gnu_rand8_normal

  !> @ingroup random
  !! @brief Basic PRNG for REAL*4 numbers
  !!
  !! Re-seeds the PRNG and generates a random number with an intrinsic Fortran subroutine.
  !!
  !! @param R[inout] : random number, REAL*4
  SUBROUTINE gnu_rand4(R)
    IMPLICIT NONE
    REAL*4 :: R

    CALL gnu_init_random_seed()
    CALL RANDOM_NUMBER(R)
  END SUBROUTINE gnu_rand4

  !> @ingroup random
  !! @brief Basic PRNG for REAL*8 numbers
  !!
  !! Re-seeds the PRNG and generates a random number with an intrinsic Fortran subroutine.
  !!
  !! @param R[inout] : random number, REAL*8
  SUBROUTINE gnu_rand(R)
    IMPLICIT NONE
    REAL*8 :: R

    CALL gnu_init_random_seed()
    CALL RANDOM_NUMBER(R)
  END SUBROUTINE gnu_rand

  !> @ingroup random
  !! @brief Basic PRNG for COMLPEX*16 numbers
  !!
  !! Re-seeds the PRNG and generates a random number with an intrinsic Fortran subroutine.
  !! Once for the imaginary part of the complex number and once for the real part of the complex number.
  !!
  !! @param R[inout] : random number, COMLPEX*16
  SUBROUTINE gnu_rand_C16(C)
    IMPLICIT NONE
    COMPLEX*16 :: C
    REAL*8 :: R1,R2

    CALL gnu_init_random_seed()
    CALL RANDOM_NUMBER(R1)
    CALL gnu_init_random_seed()
    CALL RANDOM_NUMBER(R2)

    C=dcmplx(R1,R2)
  END SUBROUTINE gnu_rand_C16

  !> @ingroup random
  !! @brief Tries to obtain a truely random seed from the operating system or hardware.
  SUBROUTINE gnu_init_random_seed()
    USE iso_fortran_env, only: int64
    IMPLICIT NONE
    INTEGER, allocatable :: seed(:)
    INTEGER :: i, n, un, istat, dt(8), pid
    INTEGER(int64) :: t
    INTEGER :: getpid

    ! call to Fortran's intrinsic seed initializer for the pseudo-random number generator
    CALL random_seed(size = n)
    ALLOCATE(seed(n))
    !> First try if the OS provides a random number generator
    OPEN(newunit=un, file="/dev/urandom", access="stream", &
        form="unformatted", action="read", status="old", iostat=istat)
    IF (istat == 0) then
      read(un) seed
      close(un)
    ELSE
      !> Fallback to XOR:ing the current time and pid. The PID is
      !! useful in case one launches multiple instances of the same
      !! program in parallel.
      CALL system_clock(t)
      IF (t == 0) then
          CALL date_and_time(values=dt)
          t = (dt(1) - 1970) * 365_int64 * 24 * 60 * 60 * 1000 &
              + dt(2) * 31_int64 * 24 * 60 * 60 * 1000 &
              + dt(3) * 24_int64 * 60 * 60 * 1000 &
              + dt(5) * 60 * 60 * 1000 &
              + dt(6) * 60 * 1000 + dt(7) * 1000 &
              + dt(8)
      END IF
      pid = getpid()
      t = ieor(t, int(pid, kind(t)))
      do i = 1, n
          seed(i) = lcg(t)
      END do
    END IF
    CALL random_seed(put=seed)
  CONTAINS
    !> @ingroup random
    !! @brief Simple PRNG, called from within random::gnu_init_random_seed
    !!
    !! This simple linear congruential generator might not be good enough for real work, but is
    !! sufficient for seeding a better PRNG.
    FUNCTION lcg(s)
      INTEGER :: lcg
      INTEGER(int64) :: s
      IF (s == 0) then
        s = 104729
      ELSE
        s = mod(s, 4294967296_int64)
      END IF
      s = mod(s * 279470273_int64, 4294967291_int64)
      lcg = int(mod(s, int(huge(0), int64)), kind(0))
    END FUNCTION lcg
  END SUBROUTINE gnu_init_random_seed

  !> @ingroup random
  !! @brief Generates complex-valued 2D random unitary matrices
  !!
  !! This program produces complex-valued \f$ N \times N \f$ Random Unitary Matrices distributed according
  !! to the Haar measure, making use of the QR decomposition of the complex-valued \f$ N \times N \f$ Random (non 
  !! Hermitian) Matrices from the Ginibre Ensemble. The QR decomposition is performed
  !! by means of (N-1) Householder reflections. 
  !! 
  !! @param[in] n : for the dimensions of the complex-valued \f$ N \times N \f$ matrices
  !! @param[inout] QH : complex-valued random unitary matrix
  SUBROUTINE Haar_U_N(n,QH)

    INTEGER, INTENT(IN) :: n
    COMPLEX*16, DIMENSION(n,n), INTENT(INOUT) :: QH

    COMPLEX*16, DIMENSION(n,n) :: a   ! matrix from the Ginibre ensemble
    COMPLEX*16 :: phase,dk,sum_c,tau

    REAL*8 :: ck
    REAL*8 :: VARIANCE,sum_r
    INTEGER :: i,j,k

    VARIANCE=1.d0/SQRT(2.d0)

    CALL GinUE(n,VARIANCE,a)  ! the matrix A(0) has been produced

    Do i=1,n
        Do j=1,n
            QH(i,j)=(0.d0,0.d0)
        ENDDO
        QH(i,i)=(1.d0,0.d0)
    ENDDO                   ! the identity matrix QH(0) has been produced



    sum_r=0.

    do 30 k=1,n-1

      do 11 i=k,n
        sum_r=max(sum_r,abs(a(i,k)))     ! a(k:n,k) CONTAINS v
    11      CONTINUE

      IF(sum_r.ne.0.)then                 ! IF v =/= 0
        sum_r=0.

        do 13 i=k,n
          sum_r=sum_r+abs(a(i,k))**2
    13        CONTINUE                           ! sum_r = || v ||^2

    phase=sqrt(sum_r)*a(k,k)/abs(a(k,k)) ! phase = exp(i theta) * || v ||
        a(k,k)=a(k,k)+phase              ! a(k:n,k) now CONTAINS u
        ck=real(conjg(phase)*a(k,k))    ! ck now CONTAINS c_k = || u ||^2 / 2
                                        !  = || v ||*( || v || + | v_1 | )
        dk=-conjg(phase)/sqrt(sum_r)  ! dk = - exp(-i theta)

        do 16 j=k+1,n                        !           _N_    _
          sum_c=(0.d0,0.d0)                      !          \   |   u_l * a_{l,j}

          do 14 i=k,n                        !   tau =   |     ---------------  
            sum_c=sum_c+conjg(a(i,k))*a(i,j) !          /___|        c_k
    14          CONTINUE                           !           l=k

          tau=sum_c/ck                  

          do 15 i=k,n                      !
            a(i,j)=dk*(a(i,j)-tau*a(i,k))  !  a(k:n,j) -> M_k * a(k:n,j)
    15          CONTINUE                         !

    16        CONTINUE                           



        do 26 j=1,n                           !           _N_    _      H
          sum_c=(0.d0,0.d0)                       !          \   |   u_l * Q_{l,j}

          do 24 i=k,n                         !   tau =   |     ---------------  
            sum_c=sum_c+conjg(a(i,k))*QH(i,j) !          /___|        c_k
    24          CONTINUE                            !           l=k

          tau=sum_c/ck                  

          do 25 i=k,n                        !   H                  H
            QH(i,j)=dk*(QH(i,j)-tau*a(i,k))  !  Q (k:n,j) -> M_k * Q (k:n,j)
    25          CONTINUE                           !

    26        CONTINUE                           



      ENDIF

    30    CONTINUE



    dk=-conjg(a(n,n))/abs(a(n,n))   !

    Do i=1,n                         !  we multiply the last raw of QH(N-1) 
        QH(n,i)= -dk*QH(n,i)         !  by exp(-i theta)
    ENDDO                            !

    RETURN

  END SUBROUTINE HAAR_U_N


  !> @ingroup random
  !! @brief Generates matrix from Ginibre ensemble
  !!
  !! Generator of complex-valued \f$ N \times N \f$ non-Hermitian matrices drawn according to the Ginibre ensemble
  !! called \f$ <GinUE> \subset GL(N,C)\f$.
  !!
  !! @param[in] N : for the dimension of the \f$ N \times N \f$ non-Hermitian matrix
  !! @param[in] VARIANCE : 
  !! @param[inout] CMAT : complex-valued \f$ N \times N \f$ non-Hermitian matrix
  SUBROUTINE GinUE(N,VARIANCE,CMAT)

    !C     ROUTIN   : Name of the subroutine you are going to use for generating
    !C                the random numbers (suggested :: rgnf_lux.f)

    INTEGER, INTENT(IN) :: N
    REAL*8, INTENT(IN) :: VARIANCE
    INTEGER :: emme, mu
    COMPLEX*16 :: SW(0:1), CMAT(N,N)
    REAL*8 :: U(2), S, T, A, B, R1, R2, V, X, Y, Q, DEVIAT 
    SAVE  S, T, A, B, R1, R2, SW

    DATA  S, T, A, B / 0.449871d0, -0.386595d0, 0.19600d0, 0.25472d0/
    DATA  R1, R2 ,SW / 0.27597d0, 0.27846d0, (1.d0 , 0.d0), (0.d0 , 1.d0)/
    !C         generate pair of uniform deviates

    DO  emme = 1, N
        DO  mu = 2, 2*N+1
    IF(mu.LE.(N+1)) CMAT(emme,mu-1) = (0.d0,0.d0)
    50 CALL gnu_rand(U(1))
    CALL gnu_rand(U(2))

    V = 1.7156d0 * (U(2) - 0.5d0)
    X = U(1) - S
    Y = ABS(V) - T
    Q = X**2 + Y*(A*Y - B*X)
    !C           accept P IF inside inner ellipse
    IF (Q .LT. R1)  GO TO 100
    !C           reject P IF outside outer ellipse
    IF (Q .GT. R2)  GO TO 50
    !C           reject P IF outside acceptance region
    IF (V**2 .GT. -4.0d0 *LOG(U(1)) *U(1)**2)  GO TO 50
    !C           ratio of P's coordinates is normal deviate
    100 DEVIAT = V/U(1)*VARIANCE
    CMAT(emme,mu/2) = CMAT(emme,mu/2) + DEVIAT * SW(MOD(mu,2))

          ENDDO !mu
    ENDDO ! emme

    RETURN
  END SUBROUTINE GINUE

  !> @ingroup singleshots 
  !> @ingroup random
  !! @brief Full pseudo-random number generator.
  !!
  !! Checks whether routine is being called for the first time, potentially calls to initialize new seeds and saves current seeds.
  !! Uses the generator by L'Ecuyer and Burkhard that is implemented in function random::r8_uni to calculate the next number in the sequence.
  !!
  !! @param R : the next value in the sequence of pseudo-random numbers
  !! @param s1 : value used as the seed for the sequence. On first call, the user should initialize s1 to a value between 1 and 2147483562
  !! @param s2 : value used as the seed for the sequence. On first call, the user should initialize s2 to a value between 1 and 2147483398
  SUBROUTINE gnu_rand_Burkardt(R)
    IMPLICIT NONE
    REAL*8, INTENT(INOUT) :: R
    INTEGER*4, SAVE :: S1,S2
    LOGICAL, SAVE :: FIRST=.TRUE.

    IF (FIRST.eqv..TRUE.) THEN
      CALL gnu_init_random_seed()
      ! call to Fortran's intrinsic pseudo-random number generator
      ! which implements the xoshiro256** generator
      CALL RANDOM_NUMBER(R)
      ! for r8_uni: "On first call, the user should initialize s1 to a value between 1 and 2147483562"
      S1=FLOOR(2147483562*R)
      CALL RANDOM_NUMBER(R)
      ! for r8_uni: "On first call, the user should initialize s2 to a value between 1 and 2147483398"
      S2=FLOOR(2147483398*R)
      FIRST=.FALSE.
    ENDIF

    R=r8_uni(S1,S2)

  END SUBROUTINE gnu_rand_Burkardt

  !> @ingroup random
  !! @brief Returns a pseudorandom number between 0 and 1.
  !!
  !! This function generates uniformly distributed pseudorandom numbers
  !! between 0 and 1, using the 32-bit generator from figure 3 of
  !! the article by L'Ecuyer.
  !! The cycle length is claimed to be 2.30584E+18.
  !!
  !! @param[inout] s1 : value used as the seed for the sequence. On first call, the user should initialize s1 to a value between 1 and 2147483562
  !! @param[inout] s2 : value used as the seed for the sequence. On first call, the user should initialize s2 to a value between 1 and 2147483398
  !!
  !! @returns
  !! Output, real ( kind = 8 ) R8_UNI, the next value in the sequence.
  !!
  !! ***********
  !!
  !! <b> Licensing: </b>
  !! This code is distributed under the GNU LGPL license.
  !!
  !! <b> Modified: </b>
  !! 08 July 2008
  !!
  !! <b> Authors: </b>
  !! * Original Pascal original version by Pierre L'Ecuyer
  !! * FORTRAN90 version by John Burkardt
  !!
  !! <b> Reference: </b>
  !!
  !!    Pierre LEcuyer,
  !!    Efficient and Portable Combined Random Number Generators,
  !!    Communications of the ACM,
  !!    Volume 31, Number 6, June 1988, pages 742-751.
  FUNCTION r8_uni ( s1, s2 )

    IMPLICIT NONE

    INTEGER ( kind = 4 ) k
    REAL ( kind = 8 ) r8_uni
    INTEGER ( kind = 4 ) s1
    INTEGER ( kind = 4 ) s2
    INTEGER ( kind = 4 ) z

    k = s1 / 53668
    s1 = 40014 * ( s1 - k * 53668 ) - k * 12211
    IF ( s1 < 0 ) then
      s1 = s1 + 2147483563
    END IF

    k = s2 / 52774
    s2 = 40692 * ( s2 - k * 52774 ) - k * 3791
    IF ( s2 < 0 ) then
      s2 = s2 + 2147483399
    END IF

    z = s1 - s2
    IF ( z < 1 ) then
      z = z + 2147483562
    END IF

    r8_uni = REAL ( z, kind = 8 ) / 2147483563.0D+00

    RETURN
  END FUNCTION

END MODULE Random
