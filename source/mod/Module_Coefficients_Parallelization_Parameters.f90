!>GPLv3
!    MCTDH-X: the multiconfigurational time-dependent Hartree for 
!    indistinguishable particles software
!
!    Copyright (C) 2021  A. U. J. Lode, M. C. Tsatsos, 
!                        E. Fasshauer, S. E. Weiner, 
!                        R. Lin, L. Papariello, P. Molignini, 
!                        C. Lévêque, M. Büttner , J. Xiang, S. Dutta
!
!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <https://www.gnu.org/licenses/>.
!
!
!
!
!<GPLv3










!> Coefficients_Parallelization_Parameters collects the numbers necessary to
!! organize the parallelization of the orbital equations of motion.
         MODULE Coefficients_Parallelization_Parameters

         USE MPI

         IMPLICIT NONE
         SAVE

         INTEGER :: CI_World,CI_Group,CI_COMM 

         INTEGER, PARAMETER :: MaxProc = 50000 !< maximal number of
!! processes.
         INTEGER, DIMENSION(MaxProc) :: CI_Proc_Job !< Number of
!! operators handled by each process.
         INTEGER, DIMENSION(MaxProc) :: CI_Proc_From !< At which
!! operator each process is starting.
         INTEGER, DIMENSION(MaxProc) :: CI_Proc_Till !< At which
!! operator each process is stopping.

         INTEGER, ALLOCATABLE :: Operator_Lengths_2body(:) !< How many non-zero prefactors are there for each two-body operator
         INTEGER, ALLOCATABLE :: Operator_Lengths_1body(:) !< How many non-zero prefactors are there for each one-body operator

         INTEGER, DIMENSION(MaxProc) :: MYID_TRM !< To decide, whether
!! the process is working on one-body or two-body terms.
         INTEGER :: EXCEPTIONAL_ID !< ID of the process working on both,
!! one-body and two-body terms.
         INTEGER :: CI_ThreadNumber
         INTEGER :: Coefficient_BinaryUnitNumber
         
         integer(kind=MPI_OFFSET_KIND),Allocatable :: &
                                       CoefficientWrite_Offsets(:)
         Integer*8,Allocatable ::      Coefficient_Blocksize(:)


         END MODULE Coefficients_Parallelization_Parameters


