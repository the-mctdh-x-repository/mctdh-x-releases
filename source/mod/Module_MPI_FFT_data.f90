!>GPLv3
!    MCTDH-X: the multiconfigurational time-dependent Hartree for 
!    indistinguishable particles software
!
!    Copyright (C) 2021  A. U. J. Lode, M. C. Tsatsos, 
!                        E. Fasshauer, S. E. Weiner, 
!                        R. Lin, L. Papariello, P. Molignini, 
!                        C. Lévêque, M. Büttner , J. Xiang, S. Dutta
!
!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <https://www.gnu.org/licenses/>.
!
!
!
!
!<GPLv3










!> This module contains allocatables for the MPI FFTs as well as a function to initialize them.
MODULE MPI_FFT_Data

USE MPI
use, intrinsic :: iso_c_binding 
USE DVR_Parameters
USE Orbital_Parallelization_Parameters,ONLY:LocalPsiDim_Trans,&
                                            LocalPsiDim,&
                                            Local_Offset, &
                                            Local_Offset_Trans, &
                                            OrbitalWrite_Offsets, &
                                            Orbital_Blocksize_Inter, &
                                            LocalPsiDim_Inter, &
                                            Local_Offset_Inter, &
                                            LocalPsiDim_Dilated, &
                                            Local_Offset_dilated
USE Auxiliary_Routines
USE Global_Parameters, ONLY: Orbital_Integrator,DIM_MCTDH
USE Matrix_Elements, ONLY: RDIM,RDIM1


implicit none

#if MPIFFT

include 'fftw3-mpi.f03'

! FFTW plans
type(C_PTR)      :: plan_mpi_forward, plan_mpi_backward, cdata_transposed, cdata_untransposed
type(C_PTR)      :: plan_mpi_forward_analysis,cdata_untransposed_analysis
type(C_PTR)      :: plan_mpi_forward_dilated,cdata_untransposed_dilated

complex(C_DOUBLE_COMPLEX), pointer :: ct_tmp(:),cu_tmp(:) !Rank remapping target must be rank 1
complex(C_DOUBLE_COMPLEX), pointer :: cu_tmp_analysis(:) !Rank remapping target must be rank 1

COMPLEX(C_DOUBLE_COMPLEX), pointer :: Psi_pointer(:,:,:), PSI_transposed_pointer(:,:,:)
COMPLEX(C_DOUBLE_COMPLEX), pointer :: Psi_pointer_2D(:,:), PSI_transposed_pointer_2D(:,:)
COMPLEX(C_DOUBLE_COMPLEX), pointer :: fsl_pointer(:,:,:), fsl_transposed_pointer(:,:,:)
COMPLEX(C_DOUBLE_COMPLEX), pointer :: fsl_pointer_2D(:,:), fsl_transposed_pointer_2D(:,:)

COMPLEX(C_DOUBLE_COMPLEX), pointer :: Psi_pointer_aux(:,:,:), PSI_transposed_pointer_aux(:,:,:)
COMPLEX(C_DOUBLE_COMPLEX), pointer :: Psi_pointer_2D_aux(:,:), PSI_transposed_pointer_2D_aux(:,:)
COMPLEX(C_DOUBLE_COMPLEX), pointer :: Psi_pointer_3D_aux(:,:,:), PSI_transposed_pointer_3D_aux(:,:,:)


complex(C_DOUBLE_COMPLEX), pointer :: cu_tmp_dilated(:) !Rank remapping target must be rank 1
complex(C_DOUBLE_COMPLEX), pointer :: ct_tmp_dilated(:) !Rank remapping target must be rank 1

COMPLEX(C_DOUBLE_COMPLEX), pointer :: Psi_pointer_2D_dilated(:,:), PSI_transposed_pointer_2D_dilated(:,:)
COMPLEX(C_DOUBLE_COMPLEX), pointer :: Psi_pointer_3D_dilated(:,:,:), PSI_transposed_pointer_3D_dilated(:,:,:)

COMPLEX(C_DOUBLE_COMPLEX), pointer :: fsl_pointer_2D_dilated(:,:), fsl_transposed_pointer_2D_dilated(:,:)
COMPLEX(C_DOUBLE_COMPLEX), pointer :: fsl_pointer_3D_dilated(:,:,:), fsl_transposed_pointer_3D_dilated(:,:,:)

!> local sizes of domain decomposed orbitals for MCTDH-X 
integer(C_INTPTR_T)                :: alloc_local, local_Lz, local_z_offset
integer(C_INTPTR_T)                ::              local_Ly, local_y_offset
integer(C_INTPTR_T)                ::              local_Lx, local_x_offset


!> local sizes of domain decomposed orbitals for the analysis
integer(C_INTPTR_T)                :: alloc_local_analysis, local_Lz_analysis, local_z_offset_analysis
integer(C_INTPTR_T)                ::              local_Ly_analysis, local_y_offset_analysis
integer(C_INTPTR_T)                ::              local_Lx_analysis, local_x_offset_analysis

!> local sizes of domain decomposed orbitals for dilation
integer(C_INTPTR_T)                :: alloc_local_dilated, local_Lz_dilated, local_z_offset_dilated
integer(C_INTPTR_T)                ::              local_Ly_dilated, local_y_offset_dilated
integer(C_INTPTR_T)                ::              local_Lx_dilated, local_x_offset_dilated

!> local sizes of domain decomposed orbitals for interpolation
integer(C_INTPTR_T)                :: alloc_local_inter, local_Lz_inter, local_z_offset_inter
integer(C_INTPTR_T)                ::              local_Ly_inter, local_y_offset_inter
integer(C_INTPTR_T)                ::              local_Lx_inter, local_x_offset_inter

#endif


INTEGER*8   :: I,J,K,IND

Contains


Subroutine MPI_FFT_Data_Allocation_Dilated(Dimension,Dilation,MYID)

  IMPLICIT NONE

  Integer :: Dimension,Dilation
  Integer :: MYID,PROCS,Ierr
  INTEGER,Allocatable :: Displacements(:)
  INTEGER*8 :: I
  

Write(6,*) "TO ALLOC MPI DIL:::", Dimension, DIM_MCTDH
#if MPIFFT

CALL MPI_COMM_SIZE(MPI_COMM_WORLD,PROCS,ierr)

ALLOCATE(LocalPsiDim_Dilated(Procs))
ALLOCATE(Local_Offset_Dilated(Procs))
ALLOCATE(Displacements(Procs))


!> TODO: need to define transposed & dilated FFTs for dilated IMEST of Exl et al
IF (DIM_MCTDH.eq.3) THEN
  alloc_local_dilated = fftw_mpi_local_size_3d(INT8(NDVR_z*Dilation), &
                                                        INT8(NDVR_y*Dilation), &
                                                        INT8(NDVR_x*Dilation), & 
                                                        MPI_COMM_WORLD, &
                                                        local_Lz_dilated, local_z_offset_dilated)
  cdata_untransposed_dilated = fftw_alloc_complex(alloc_local_dilated)
  call c_f_pointer(cdata_untransposed_dilated, cu_tmp_dilated, [NDVR_x*Dilation*NDVR_y*Dilation*local_Lz_dilated])
  Psi_Pointer_3D_dilated(0:NDVR_X*Dilation-1, 0:NDVR_Y*Dilation-1, 0:Local_Lz-1) => cu_tmp_dilated
  LocalPsiDim_dilated(MYID+1)=Local_Lz_dilated*NDVR_X*NDVR_Y*Dilation*Dilation

!> Output information about domain decomposition
Write(6,*) "Local size for dilated orbitals", LocalPsiDim_dilated(MYID+1)


ELSEIF (DIM_MCTDH.eq.2) THEN
  alloc_local_dilated = fftw_mpi_local_size_2d( INT8(NDVR_y*Dilation), &
                                                INT8(NDVR_x*Dilation), &
                                                MPI_COMM_WORLD, &
                                                local_Ly_dilated, local_y_offset_dilated)

  cdata_untransposed_dilated = fftw_alloc_complex(alloc_local_dilated)
  call c_f_pointer(cdata_untransposed_dilated, cu_tmp_dilated, [NDVR_x*Dilation*local_Ly_dilated])
  Psi_Pointer_2D_dilated(0:NDVR_X*Dilation-1, 0:Local_Ly-1) => cu_tmp_dilated

  LocalPsiDim_dilated(MYID+1)=Local_Ly_dilated*NDVR_X*Dilation

!> Output information about domain decomposition
  Write(6,*) "Local size:", LocalPsiDim_dilated(MYID+1)

ENDIF

!Distribute LocalPsiDim and LocalPsi
Do I=1,Procs
   Displacements(I)=INT(I-1,4)
END DO
   
CALL MPI_ALLGATHER(LocalPsiDim_dilated(MYID+1),1,MPI_INTEGER8,LocalPsiDim_dilated,1,MPI_INTEGER8,MPI_COMM_WORLD,ierr)

Local_Offset_dilated(1)=0
DO K=2,Procs
  Local_Offset_dilated(K) = SUM(LocalPsiDim_dilated(1:K-1))
END DO


Write(6,*) "Finished MPI Orbital data allocation, dilated PSIDIM:", LocalPsiDim_dilated
Write(6,*) "Finished MPI Orbital data allocation, dilated Offsets:", Local_Offset_dilated

#endif

End Subroutine MPI_FFT_Data_Allocation_Dilated



Subroutine MPI_FFT_Data_Allocation(Dimension,MYID,Dilation)

Integer :: Dimension
Integer :: MYID
INTEGER, OPTIONAL,VALUE :: Dilation
INTEGER :: PROCS,Ierr
INTEGER,Allocatable :: Displacements(:)
INTEGER*8 :: I

IF(.NOT. PRESENT(Dilation)) Dilation = 1 ! default value for dilation
WRITE(*,'("DILATION IN MPI_FFT ALLOC is ", I0)') Dilation

CALL MPI_COMM_SIZE(MPI_COMM_WORLD,PROCS,ierr)


if (Dilation.ne.1) THEN
  Call MPI_FFT_Data_Allocation_Dilated(DIM_MCTDH,Dilation,0)
  write(6,*) "EXIT from MPI_FFT_Data_ALLOC_Dilated"
ENDIF


IF (((PROCS.eq.1).or.(Dimension.eq.5)).and.(MPI_ORBS.eqv..FALSE.)) THEN

ALLOCATE(LocalPsiDim(667))
ALLOCATE(LocalPsiDim_Trans(667))
ALLOCATE(Local_Offset(667))
ALLOCATE(Local_Offset_Trans(667))

LocalPsiDim=NDVR_X*NDVR_X*NDVR_Z
LocalPsiDim_Trans=NDVR_X*NDVR_X*NDVR_Z
Local_Offset=0
Local_Offset_Trans=0
RETURN
ELSE




#if MPIFFT

IF ((PROCS.eq.1).and.(MPI_ORBS.eqv..TRUE.)) THEN
  write(6,*) "MPI_ORBS is activated, but only 1 processor is in use."
ENDIF

CALL MPI_COMM_SIZE(MPI_COMM_WORLD,PROCS,ierr)

!PAUSE

ALLOCATE(LocalPsiDim(Procs))
ALLOCATE(LocalPsiDim_Trans(Procs))
ALLOCATE(Local_Offset(Procs))
ALLOCATE(Local_Offset_Trans(Procs))
ALLOCATE(Displacements(Procs))
ALLOCATE(OrbitalWrite_Offsets(Procs))



IF (Dimension.eq.1) THEN
  ! Determine size of local data
!  alloc_local = fftw_mpi_local_size_1d(NDVR_X, MPI_COMM_WORLD,FFTW_FORWARD,FFTW_MEASURE,&
!        local_Lx, local_x_offset, local_Lx_out, Local_x_offset_out)

!  LocalPsiDim_Trans(MYID) = NDVR_Z*NDVR_Y*Local_Lx   !!! Dimension of local portion of the transposed orbital

!  LocalPsiDim = 0 
!  Local_Offset = 0
!  LocalPsiDim_Trans = 0 
!  Local_Offset_Trans = 0

!  LocalPsiDim(MYID+1) = Local_Lx   !!! Dimension of local portion of the NON-transposed orbital
!  Local_Offset(MYID+1) = Local_x_Offset
!  LocalPsiDim_Trans(MYID+1) = Local_Lx_out  !!! 1D output sizes may differ from input sizes 
!  Local_Offset_Trans(MYID+1) = Local_x_Offset_out

  Write(6,*) "1D MPI FFTs not yet supported; exiting!!!"
  STOP

!  ! Allocate FFTW C-data
!  Psi_Pointer = fftw_alloc_complex( alloc_local); call c_f_pointer(Psi_Pointer, ct_tmp, [ local_Lx])

ELSEIF (Dimension.eq.3) THEN
  alloc_local = fftw_mpi_local_size_3d_transposed(INT8(NDVR_z), INT8(NDVR_y), INT8(NDVR_x), MPI_COMM_WORLD, &
                                                local_Lz, local_z_offset,               &
                                                local_Ly, local_y_offset)

  cdata_untransposed = fftw_alloc_complex(alloc_local)
  call c_f_pointer(cdata_untransposed, cu_tmp, [NDVR_x*NDVR_y*local_Lz])

  cdata_transposed = fftw_alloc_complex(alloc_local)
  call c_f_pointer(cdata_untransposed, ct_tmp, [NDVR_x*NDVR_Z*local_Ly])

  !   rank remapping or array dimension changing
  Psi_Pointer(0:NDVR_X-1, 0:NDVR_Y-1, 0:local_Lz-1) => cu_tmp
  Psi_transposed_Pointer(0:NDVR_X-1, 0:NDVR_Z-1, 0:local_Ly-1) => ct_tmp

  LocalPsiDim(MYID+1)=Local_Lz*NDVR_X*NDVR_Y
  Local_Offset(MYID+1) = Local_x_Offset
  LocalPsiDim_Trans(MYID+1)=Local_Ly*NDVR_X*NDVR_Z


  alloc_local_analysis = fftw_mpi_local_size_3d(INT8(NDVR_z), INT8(NDVR_y), INT8(NDVR_x), MPI_COMM_WORLD, &
                                                local_Lz_analysis, local_z_offset_analysis )
  cdata_untransposed_analysis = fftw_alloc_complex(alloc_local_analysis)
  call c_f_pointer(cdata_untransposed_analysis, cu_tmp_analysis, [NDVR_x*NDVR_y*local_Lz])


!> Output information about domain decomposition
Write(6,*) "Local size:", LocalPsiDim(MYID+1)
Write(6,*) "Local untransposed offset/size:", Local_z_offset,Local_Lz
Write(6,*) "Local transposed offset:", Local_y_offset,Local_Ly


ELSEIF (Dimension.eq.2) THEN
  alloc_local = fftw_mpi_local_size_2d_transposed( INT8(NDVR_y), &
                                                   INT8(NDVR_x), &
                                                   MPI_COMM_WORLD, &
                                                   local_Ly, &
                                                   local_y_offset, &
                                                   local_Lx, &
                                                   local_x_offset)

  cdata_untransposed = fftw_alloc_complex(alloc_local)
  call c_f_pointer(cdata_untransposed, cu_tmp, [NDVR_x*local_Ly])

  cdata_transposed = fftw_alloc_complex(alloc_local)
  call c_f_pointer(cdata_untransposed, ct_tmp, [NDVR_Y*local_Lx])


  !   rank remapping or array dimension changing
  Psi_Pointer_2D(0:NDVR_X-1, 0:Local_Ly-1) => cu_tmp
  Psi_transposed_Pointer_2D(0:NDVR_Y-1, 0:local_Lx-1) => ct_tmp

  LocalPsiDim(MYID+1)=Local_Ly*NDVR_X
  LocalPsiDim_Trans(MYID+1)=Local_Lx*NDVR_Y


  alloc_local_analysis = fftw_mpi_local_size_2d( INT8(NDVR_y), INT8(NDVR_x), MPI_COMM_WORLD, &
                                                local_Ly_analysis, local_y_offset_analysis )
  cdata_untransposed_analysis = fftw_alloc_complex(alloc_local_analysis)
  call c_f_pointer(cdata_untransposed_analysis, cu_tmp_analysis, [NDVR_x*local_Ly])


!> Output information about domain decomposition
  Write(6,*) "Local size:", LocalPsiDim(MYID+1)
  Write(6,*) "Local untransposed offset/size:", Local_y_offset,Local_Ly
  Write(6,*) "Local transposed offset:", Local_x_offset,Local_Lx

ENDIF


!Distribute LocalPsiDim and LocalPsi
Do I=1,Procs
   Displacements(I)=INT(I-1,4)
END DO
   
CALL MPI_ALLGATHER(LocalPsiDim(MYID+1),1,MPI_INTEGER8,LocalPsiDim,1,MPI_INTEGER8,MPI_COMM_WORLD,ierr)
CALL MPI_ALLGATHER(LocalPsiDim_Trans(MYID+1),1,MPI_INTEGER8,LocalPsiDim_Trans,1,MPI_INTEGER8,MPI_COMM_WORLD,ierr)

Local_Offset(1)=0
DO K=2,Procs
  Local_Offset(K) = SUM(LocalPsiDim(1:K-1))
  Local_Offset_Trans(K) = SUM(LocalPsiDim_Trans(1:K-1))
END DO


!Allocate(Weight(LocalPsiDim(MYID+1)))
! This is a repeated definition of weight
IF (DIM_MCTDH.eq.3) THEN
  Do I=1,Local_Lz
     Do J=1,NDVR_Y
        Do K=1,NDVR_X
           ind=k+(j-1)*NDVR_X+(i-1)*NDVR_X*NDVR_Y 
           weight(ind)=weight_X(K)*weight_Y(J)  &
                      *weight_Z(I+Local_z_Offset)
        EndDo
     EndDo
  EndDo
ELSEIF (DIM_MCTDH.eq.2) THEN
     Do J=1,Local_Ly
        Do K=1,NDVR_X
           ind=k+(j-1)*NDVR_X
           weight(ind)=weight_X(K)*weight_Y(J+Local_y_Offset)
        EndDo
     EndDo
ELSE
   Write(6,*) "Domain decomposition not implemented in 1D!"
   STOP
ENDIF

IF ((Interaction_Type == 4).or.(Interaction_Type == 5) &
    .or.(Interaction_Type == 7)) THEN

   IF (MPI_ORBS.eqv..TRUE.) THEN
     allocate(W3xx(LocalPsiDim(MYID+1)),stat=ierr)
     allocate(W3xxFFT_TRans(LocalPsiDim_Trans(MYID+1)),stat=ierr)
   ENDIF

   if(ierr /= 0) then
     write(*,*)"allocation error for MPI-W3xx"
     STOP
   endif
ENDIF

Write(6,*) "Finished MPI Orbital data allocation, Local PSIDIM:", LocalPsiDim
Write(6,*) "Finished MPI Orbital data allocation, Local Offsets:", Local_Offset

#endif
ENDIF

End Subroutine MPI_FFT_Data_Allocation

Subroutine MPI_FFT_Data_Interpol_Allocation(NDVR_X_inter,NDVR_Y_inter,NDVR_Z_inter)

INTEGER*8 :: NDVR_X_inter,NDVR_Y_inter,NDVR_Z_inter

#if MPIFFT
INTEGER*8  :: Idummy
REAL*8     :: Rdummy
COMPLEX*16 :: Cdummy 

Integer :: MYID,PROCS,Ierr
INTEGER,Allocatable :: Displacements(:)
INTEGER*8 :: I

IF (DIM_MCTDH.eq.1) THEN
   Write(6,*) "1D interpolation with MPI FFTS not implemented!!!!!"
   STOP
ENDIF

CALL MPI_COMM_SIZE(MPI_COMM_WORLD,PROCS,ierr)
CALL MPI_COMM_RANK(MPI_COMM_WORLD,MYID,ierr)

ALLOCATE(LocalPsiDim_Inter(Procs))
ALLOCATE(Local_Offset_Inter(Procs))
ALLOCATE(Displacements(Procs))


IF (DIM_MCTDH.eq.3) THEN
  alloc_local_inter = fftw_mpi_local_size_3d_transposed(INT8(NDVR_z_inter), INT8(NDVR_y_inter), INT8(NDVR_x_inter), & 
                                                        MPI_COMM_WORLD, &
                                                        local_Lz_inter, local_z_offset_inter,               &
                                                        local_Ly_inter, local_y_offset_inter)

  LocalPsiDim_inter(MYID+1)=Local_Lz_inter*NDVR_X*NDVR_Y

!> Output information about domain decomposition
Write(6,*) "Local size for reading orbitals to interpolate:", LocalPsiDim_inter(MYID+1)


ELSEIF (DIM_MCTDH.eq.2) THEN
  alloc_local = fftw_mpi_local_size_2d_transposed( INT8(NDVR_y_inter), INT8(NDVR_x_inter), MPI_COMM_WORLD, &
                                                local_Ly_inter, local_y_offset_inter,               &
                                                local_Lx_inter, local_x_offset_inter)

  LocalPsiDim_inter(MYID+1)=Local_Ly_inter*NDVR_X_inter

!> Output information about domain decomposition
  Write(6,*) "Local size:", LocalPsiDim_inter(MYID+1)

ENDIF

!Distribute LocalPsiDim and LocalPsi
Do I=1,Procs
   Displacements(I)=INT(I-1,4)
END DO
   
CALL MPI_ALLGATHER(LocalPsiDim_inter(MYID+1),1,MPI_INTEGER8,LocalPsiDim_inter,1,MPI_INTEGER8,MPI_COMM_WORLD,ierr)

Local_Offset_inter(1)=0
DO K=2,Procs
  Local_Offset_inter(K) = SUM(LocalPsiDim_inter(1:K-1))
END DO



Orbital_BlockSize_inter=(MORB+1)*NDVR_X_inter*NDVR_Y_inter*NDVR_Z_inter        &
                  *SIZEOF(CDummy)                      &
                  +2*SIZEOF(RDummy)+SIZEOF(IDummy)     &
                  +(Rdim+Rdim1)*SIZEOF(CDummy)             

Write(6,*) "Finished MPI Orbital data allocation, Interpolation PSIDIM:", LocalPsiDim_inter
Write(6,*) "Finished MPI Orbital data allocation, Interpolation Offsets:", Local_Offset_inter
Write(6,*) "Finished MPI Orbital data allocation, Interpolation Orbital_BlockSize:", Orbital_BlockSize_Inter


#endif
end subroutine MPI_FFT_Data_Interpol_Allocation



END MODULE MPI_FFT_Data


