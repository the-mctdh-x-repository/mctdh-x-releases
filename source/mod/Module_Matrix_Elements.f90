!>GPLv3
!    MCTDH-X: the multiconfigurational time-dependent Hartree for 
!    indistinguishable particles software
!
!    Copyright (C) 2021  A. U. J. Lode, M. C. Tsatsos, 
!                        E. Fasshauer, S. E. Weiner, 
!                        R. Lin, L. Papariello, P. Molignini, 
!                        C. Lévêque, M. Büttner , J. Xiang, S. Dutta
!
!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <https://www.gnu.org/licenses/>.
!
!
!
!
!<GPLv3










!> Matrix_Elements contains and the matrix elements and allocates the
!> necessary arrays.
!> \todo Check if all the variables here are truly necessary, for instace TERM_REQ* and TERM_INDEX* might be identical.
        MODULE Matrix_Elements

        use Global_Parameters
        use orbital_parallelization_parameters
        use Coefficients_Parameters,ONLY:Npar
        use Auxiliary_Routines

        IMPLICIT NONE
        SAVE 

        COMPLEX*16, ALLOCATABLE :: Rho1_Elements(:) !< Matrix Elements of the
!! reduced one-body density (rho_ij, compact storage).
        COMPLEX*16, ALLOCATABLE :: Rho1_Elements_Block(:,:) !< Matrix Elements of the
!! reduced one-body density for block davidson computations(rho_ij, compact storage).

        COMPLEX*16, ALLOCATABLE :: Rho1_Elements_Storage(:) !< Matrix Elements of the
!! reduced one-body density (rho_ij, compact storage, MPI work array).
        COMPLEX*16, ALLOCATABLE :: Full_Rho1_Elements(:,:)!< Matrix Elements of the
!! reduced one-body density (rho_ij, normal storage).
        COMPLEX*16, ALLOCATABLE :: Full_Rho1_Elements_Storage(:,:)!< Matrix Elements of the
!! reduced one-body density (rho_ij, normal storage,MPI work array).
        COMPLEX*16, ALLOCATABLE :: Inverted_Rho1_Elements(:,:) !< Inverse of the 
!! Matrix Elements of the one-body Hamiltonian h (normal form).
        COMPLEX*16, ALLOCATABLE :: BH_Offset(:) !< Onsite Energy Offset for BH Hamiltonians.


       COMPLEX*16, ALLOCATABLE :: Rho2_Elements(:)!< Matrix Elements of the
!! reduced two-body density (rho_ijkl, compact storage).
       COMPLEX*16, ALLOCATABLE :: Rho2_Elements_Block(:,:)!< Matrix Elements of the
!! reduced two-body density for block-Davidson computations (rho_ijkl, compact storage).
       COMPLEX*16, ALLOCATABLE :: Rho2_Elements_Storage(:)!< Matrix Elements of the
!! reduced two-body density (rho_ijkl, compact storage,MPI work array).


        COMPLEX*16, ALLOCATABLE :: Compact_1bodyHamiltonian_Elements(:) !< Matrix Elements of the
!! one-body Hamiltonian h (compact form).
        COMPLEX*16, ALLOCATABLE :: Full_1bodyHamiltonian_Elements(:,:) !< Matrix Elements of the
!! one-body Hamiltonian h (normal form).
        COMPLEX*16, ALLOCATABLE :: Interaction_Elements(:) !< Matrix Elements of the
!! two-body operators W.
        COMPLEX*16, ALLOCATABLE :: Interaction_Elements_Storage(:)!< Matrix Elements of the
!! two-body operators W (MPI work array).


        REAL*8, ALLOCATABLE :: Nocc(:) !< Natural Occupation Numbers.
        REAL*8, ALLOCATABLE :: Energy_Block(:) !< Natural Occupation Numbers.
        REAL*8, ALLOCATABLE :: Nocc_Block(:,:) !< Natural Occupation Numbers for Block Davidson computations.
        COMPLEX*16, ALLOCATABLE :: NatVec(:,:) !< Eigenvectors of the
!! reduced density matrix elements (to transform working to natural
!! orbitals).
        COMPLEX*16, ALLOCATABLE :: NatVec_Block(:,:,:) !< Eigenvectors of the
!! reduced density matrix elements for Block-Davidsond computations 
!! (to transform working to natural orbitals).
        COMPLEX*16, ALLOCATABLE :: NO_Expectation_x(:) !< Expectation of
!! x for the orbitals.

        INTEGER, ALLOCATABLE :: TERM_INDEX_1B(:) !< Indexing for the
!! reduced one-body matrix elements / one-body operators.
        INTEGER, ALLOCATABLE :: TERM_REQ_1B(:) !< Indexing for the
!! reduced one-body matrix elements / one-body operators.
        COMPLEX*16, ALLOCATABLE :: RESCALE_1B(:) !< Indexing for the
!! reduced one-body matrix elements / one-body operators.



        INTEGER, ALLOCATABLE :: TERM_INDEX_2B(:)  !< Indexing for the
!! reduced two-body matrix elements / two-body operators.
        INTEGER, ALLOCATABLE :: TERM_REQ_2B(:) !< Indexing for the
!! reduced two-body matrix elements / two-body operators.
        COMPLEX*16, ALLOCATABLE :: RESCALE_2B(:) !< Indexing for the
!! reduced two-body matrix elements / two-body operators.

        INTEGER, ALLOCATABLE :: TERM_INDEX_2B_Bosons(:)  !< Indexing for the
!! reduced two-body matrix elements / two-body operators.
        INTEGER, ALLOCATABLE :: TERM_REQ_2B_Bosons(:) !< Indexing for the
!! reduced two-body matrix elements / two-body operators.
        INTEGER, ALLOCATABLE :: TERM_INDEX_1B_Bosons(:) !< Indexing for the
!! reduced one-body matrix elements / one-body operators.
        INTEGER, ALLOCATABLE :: TERM_REQ_1B_Bosons(:) !< Indexing for the
!! reduced one-body matrix elements / one-body operators.

        LOGICAL :: ENRG_EVAL !< Energy expectation value evaluation.
        INTEGER :: MaxTrm1b !< Number of one-body operators. 
        INTEGER :: MaxTrm2b !< Number of two-body operators.
        COMPLEX*16, ALLOCATABLE :: ZMU(:,:)

        INTEGER, public :: Rdim !< Number of one-body matrix elements in
!! compact storage (== upper triangle).
        INTEGER, public :: Rdim1 !< Number of two-body matrix elements in
        INTEGER, Public :: KSLQTERMS
!! compact storage (== upper triangle).
        INTEGER, public :: Rdim_Bosons !< Number of one-body matrix elements in
!! compact storage (== upper triangle).
        INTEGER, public :: Rdim1_Bosons !< Number of two-body matrix elements in
!! compact storage (== upper triangle).
        INTEGER, ALLOCATABLE :: KSLQ_K(:),KSLQ_S(:),KSLQ_L(:), &
                                  KSLQ_Q(:) !< Index arrays for two-body operators
        INTEGER, ALLOCATABLE :: KSLQ_SL(:) !< Index array for local interaction potentials

        contains

!> Allocates all the arrays for the matrix elements.
!> Fills the indexing arrays TERM_INDEX_1B/2B, KSQL...  
        logical function Matrix_Elements_init(Job_Type,MYID)
        integer :: ierr,MYID
        character*3 :: Job_Type
        INTEGER :: FromN,TillN
        INTEGER*8 :: Dummy,L,S,K,SL,Q,KQ
        INTEGER :: KSLQ,P
        INTEGER ::  I,J,cK,cS,cQ,cL,i2,cI,cJ

        Rdim = Morb*(Morb+1)/2
        Rdim1=Rdim*(Rdim+1)/2
        Rdim_Bosons= (Morb-Npar+1)*(Morb-Npar+2)/2
        Rdim1_Bosons=Rdim*(Rdim+1)/2

        IF (Bose_Hubbard.eqv..TRUE.) THEN
          allocate(BH_Offset(Morb),stat=ierr)
          if(ierr /= 0)write(*,*)"allocation error in Matrix_Elements"
        ENDIF  
 
        allocate(Rho1_Elements(Rdim),stat=ierr)
        if(ierr /= 0)write(*,*)"allocation error in Matrix_Elements"
        allocate(Compact_1bodyHamiltonian_Elements(Rdim),stat=ierr)
        if(ierr /= 0)write(*,*)"allocation error in Matrix_Elements"
        allocate(Rho1_Elements_Storage(Rdim),stat=ierr)
        IF (trim(Coefficients_Integrator).eq.'BDV') THEN
          allocate(Rho1_Elements_Block(Rdim,BlockSize),stat=ierr)
          if(ierr /= 0)write(*,*)"allocation error in Matrix_Elements"
        ENDIF

        if(ierr /= 0)write(*,*)"allocation error in Matrix_Elements"
        allocate(Full_Rho1_Elements(Morb,Morb),stat=ierr)
        if(ierr /= 0)write(*,*)"allocation error in Matrix_Elements"
        allocate(Full_1bodyHamiltonian_Elements(Morb,Morb),stat=ierr)
        if(ierr /= 0)write(*,*)"allocation error in Matrix_Elements"
        allocate(Inverted_Rho1_Elements(Morb,Morb),stat=ierr)
        if(ierr /= 0)write(*,*)"allocation error in Matrix_Elements"
        allocate(Full_Rho1_Elements_Storage(Morb,Morb),stat=ierr)
        if(ierr /= 0)write(*,*)"allocation error in Matrix_Elements"

        allocate(Rho2_Elements(Rdim1),stat=ierr)
        if(ierr /= 0)write(*,*)"allocation error in Matrix_Elements"

        IF (Morb.eq.1) THEN
           Rho2_Elements=Npar*(Npar-1)
        ENDIF

        allocate(Interaction_Elements(Rdim1),stat=ierr)
        if(ierr /= 0)write(*,*)"allocation error in Matrix_Elements"
        allocate(Interaction_Elements_Storage(Rdim1),stat=ierr)
        if(ierr /= 0)write(*,*)"allocation error in Matrix_Elements"
        allocate(Rho2_Elements_Storage(Rdim1),stat=ierr)
        if(ierr /= 0)write(*,*)"allocation error in Matrix_Elements"

        IF (trim(Coefficients_Integrator).eq.'BDV') THEN
          allocate(Rho2_Elements_Block(Rdim1,BlockSize),stat=ierr)
          if(ierr /= 0)write(*,*)"allocation error in Matrix_Elements"
        ENDIF

        allocate(Nocc(Morb),stat=ierr)
        if(ierr /= 0)write(*,*)"allocation error in Matrix_Elements"

        IF (trim(Coefficients_Integrator).eq.'BDV') THEN
          allocate(Nocc_Block(Morb,BlockSize),stat=ierr)
          if(ierr /= 0)write(*,*)"allocation error in Matrix_Elements"
        ENDIF

        IF (trim(Coefficients_Integrator).eq.'BDV') THEN
          allocate(Energy_Block(BlockSize),stat=ierr)
          if(ierr /= 0)write(*,*)"allocation error in Matrix_Elements"
        ENDIF

        allocate(NatVec(Morb,Morb),stat=ierr)
        if(ierr /= 0)write(*,*)"allocation error in Matrix_Elements"

        IF (trim(Coefficients_Integrator).eq.'BDV') THEN
           allocate(NatVec_Block(Morb,Morb,BlockSize),stat=ierr)
           if(ierr /= 0)write(*,*)"allocation error in Matrix_Elements"
        ENDIF

        allocate(NO_Expectation_x(Morb),stat=ierr)
        if(ierr /= 0)write(*,*)"allocation error in Matrix_Elements"
        NO_Expectation_x=Zero

        allocate(TERM_INDEX_1B(Rdim),stat=ierr)
        if(ierr /= 0)write(*,*)"allocation error in Matrix_Elements"
        allocate(RESCALE_1B(Rdim),stat=ierr)
        if(ierr /= 0)write(*,*)"allocation error in Matrix_Elements"
        allocate(TERM_REQ_1B(Rdim),stat=ierr)
        if(ierr /= 0)write(*,*)"allocation error in Matrix_Elements"

        allocate(TERM_INDEX_2B(Rdim1),stat=ierr)
        if(ierr /= 0)write(*,*)"allocation error in Matrix_Elements"
        allocate(RESCALE_2B(Rdim1),stat=ierr)
        if(ierr /= 0)write(*,*)"allocation error in Matrix_Elements"
        allocate(TERM_REQ_2B(Rdim1),stat=ierr)
        if(ierr /= 0)write(*,*)"allocation error in Matrix_Elements"


        IF (Job_Type.eq.'FER') THEN
           allocate(TERM_INDEX_1B_Bosons(Rdim_Bosons),stat=ierr)
           if(ierr /= 0)write(*,*)"allocation error in Matrix_Elements"
           allocate(TERM_REQ_1B_Bosons(Rdim_Bosons),stat=ierr)
           if(ierr /= 0)write(*,*)"allocation error in Matrix_Elements"
 
           allocate(TERM_INDEX_2B_Bosons(Rdim1_Bosons),stat=ierr)
           if(ierr /= 0)write(*,*)"allocation error in Matrix_Elements"
           allocate(TERM_REQ_2B_Bosons(Rdim1_Bosons),stat=ierr)
           if(ierr /= 0)write(*,*)"allocation error in Matrix_Elements"
        ENDIF



        allocate(ZMU(Morb,Morb),stat=ierr)
        if(ierr /= 0)write(*,*)"allocation error in Matrix_Elements"

        if(ierr /= 0) then
           write(*,*)"allocation error for Matrix_Elements"
           Matrix_Elements_init=.false.
        else
           if(ierr == 0)write(*,*)"allocation ok for Matrix_Elements"
           Matrix_Elements_init=.true.
        endif


      IF ((Job_Type.eq.'BOS').or.(Job_Type.eq.'FCI')) THEN

       L=0
       Do cK= 1,Morb
          Do cJ=cK,Morb
             L=L+1
             P=cK+100*cJ
             TERM_INDEX_1B(L)=P
             TERM_REQ_1B(L)=L
             RESCALE_1B(L)= 1.0d0*ZONER+1.0d0*ZONEI
             IF(cJ.eq.cK)  RESCALE_1B(L)= 1.0d0*ZONER+0.0d0*ZONEI
          EndDo
       EndDo

       MaxTrm1b=Morb*(Morb+1)/2
       L=0
       Do cK= 1,Morb
          Do cL=cK,Morb
             i2=cL
             Do cI=cK,Morb
                Do cJ=i2,Morb
                   L=L+1
                   P=cK+100*cL+10000*cI+1000000*cJ
                   TERM_INDEX_2B(L)=P
                   TERM_REQ_2B(L)=L
                   RESCALE_2B(L)=0.5*ZONER+0.5*ZONEI
                   IF((cI.eq.cK).and.(cJ.eq.cL)) THEN 
                      RESCALE_2B(L)=0.5*ZONER+0.0*ZONEI
                   ENDIF
                EndDo
                  i2=cI+1
             EndDo
          EndDo
       EndDo


       MaxTrm2b=(Morb*(Morb+1)/2+1)*(Morb*(Morb+1)/2)/2

      END IF

       IF (Job_Type.eq.'FER') THEN
          L=0
          Do cK= 1,Morb
             Do cQ=cK,Morb
                L=L+1
                P=cK+100*cQ
                TERM_INDEX_1B(L)=P
                TERM_REQ_1B(L)=L
                RESCALE_1B(L)= 1.0d0*ZONER+1.0d0*ZONEI
                IF(cK.eq.cQ) THEN
                   RESCALE_1B(L)= 1.0d0*ZONER+0.0d0*ZONEI
                ENDIF
             EndDo
          EndDo

          MaxTrm1b=Morb*(Morb+1)/2

          L=0
          Do cL= 1,Morb
             Do cQ=cL,Morb
                i2=cQ
                Do cK=cL,Morb
                   Do cS=i2,Morb
                      L=L+1
                      P=cL+100*cQ+10000*cK+1000000*cS
                      TERM_INDEX_2B(L)=P
                      TERM_REQ_2B(L)=L
                      RESCALE_2B(L)=0.5*ZONER+0.5*ZONEI
                      IF((cK.eq.cL).or.(cS.eq.cQ)) THEN
                        RESCALE_2B(L)=+0.5*ZONER+0.5*ZONEI
                      ENDIF
                      IF((cK.eq.cL).and.(cS.eq.cQ)) THEN
                        RESCALE_2B(L)=+0.5*ZONER+0.0*ZONEI
                      ENDIF
                   EndDo
                     i2=cK+1
                EndDo
             EndDo
          EndDo

          MaxTrm2b=(Morb*(Morb+1)/2+1)*(Morb*(Morb+1)/2)/2

       ENDIF

        IF (MPI_ORBS.eqv..TRUE.) THEN
          KSLQTERMS=(Rdim)*Morb**2 
          FromN=1 
          TillN=RDim  
        ELSE
!          Write(6,*) "Proc_WSL_Starts",Proc_WSL_Starts(:MYID+1)
!          Write(6,*) "Proc_WSL_Stops",Proc_WSL_Finish(:MYID+1)
          FromN=Proc_WSL_Starts(MYID+1) 
          TillN=Proc_WSL_Finish(MYID+1) 
          KSLQTerms=(TillN-FROMN+1)*Morb**2
        ENDIF 

!        write(6,*) "KSLQTerms", KSLQTerms, FromN, TillN
        ALLOCATE(KSLQ_K(KSLQTerms))
        ALLOCATE(KSLQ_S(KSLQTerms))
        ALLOCATE(KSLQ_L(KSLQTerms))
        ALLOCATE(KSLQ_Q(KSLQTerms))
        ALLOCATE(KSLQ_SL(KSLQTerms))

        KSLQLoop: DO KSLQ=1,KSLQTERMS
          call get_ijk_from_m(INT((FromN-1)*Morb**2+KSLQ,8), &
                              INT(Morb**2,8),INT(RDim,8),KQ, &
                              SL,dummy)
          call get_ijk_from_m(INT(KQ,8),INT(Morb,8),INT(Morb,8), &
                              K,Q,dummy)
 
          P=TERM_INDEX_1B(SL)
          L= INT(P/100)
          S= P-L*100
     
          KSLQ_K(KSLQ)=K           
          KSLQ_S(KSLQ)=S
          KSLQ_L(KSLQ)=L
          KSLQ_Q(KSLQ)=Q
          KSLQ_SL(KSLQ)=SL
!          write(6,*) "KSLQ,SL", K,S,L,Q,SL

        END DO KSLQLoop



       IF(MYID.eq.0) then 
          write(6,*) "#######################################"
          write(6,*) "####    COEFFICIENTS INITIALIZED   ####"
          write(6,*) "#######################################"
       ENDIF

        end function
        END MODULE Matrix_Elements


