!>GPLv3
!    MCTDH-X: the multiconfigurational time-dependent Hartree for 
!    indistinguishable particles software
!
!    Copyright (C) 2021  A. U. J. Lode, M. C. Tsatsos, 
!                        E. Fasshauer, S. E. Weiner, 
!                        R. Lin, L. Papariello, P. Molignini, 
!                        C. Lévêque, M. Büttner , J. Xiang, S. Dutta
!
!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <https://www.gnu.org/licenses/>.
!
!
!
!
!<GPLv3










!> This module collects the routines necessary for the indexing of the 
!> one-body and two-body matrix elements as well as the coefficients 
!> and configurations.
      module addresses

      contains

!>    This SUBROUTINE computes the Number state Nvec
!>    corresponding to the index Ind0 of the CI-vector.
      SUBROUTINE Get_ConfigurationFromIndex(Ind0,N,M,Ivec,Nvec)

      USE Coefficients_Parameters

      IMPLICIT NONE

      INTEGER :: IND0
      INTEGER:: i,N,Ind,j,M,MaxI
      INTEGER, DIMENSION(MORB) :: Ivec
      INTEGER, DIMENSION(MORB) :: Nvec

      IND=Ind0
      Ivec=0 
      Nvec=0 
      MaxI=M+N-1 

      do i=1,M-1
        qq: Do j=M-i,MaxI
           Ivec(i)=j
           IF(Ind.le.MatrixOfBinomialCoefficients(j-1,M-i)) then 
             Ivec(i)=j-1
             goto 1
           endif
        End Do  qq
1       Ind=Ind-MatrixOfBinomialCoefficients(Ivec(i)-1,M-i)
        MaxI=Ivec(i)
      End do

2     Nvec(1)=N+M-1-Ivec(1)

      Do i=2,M-1
         Nvec(i)=Ivec(i-1)-Ivec(i)-1
      Enddo

      Nvec(M)=Ivec(M-1)-1
      
      end  SUBROUTINE  Get_ConfigurationFromIndex

!>    This SUBROUTINE computes the Number state Nvec
!>    corresponding to the index Ind0 of the CI-vector.
      SUBROUTINE Get_ConfigurationFromIndex_OLD(Ind0,N,M,Nvec)

         USE Coefficients_Parameters
   
         IMPLICIT NONE
   
         INTEGER :: IND0
         INTEGER:: i,N,Ind,j,M,MaxI
         INTEGER, DIMENSION(MORB) :: Ivec
         INTEGER, DIMENSION(MORB) :: Nvec
   
         IND=Ind0
         Ivec=0 
         Nvec=0 
         MaxI=M+N-1 
   
         do i=1,M-1
           qq: Do j=M-i,MaxI
              Ivec(i)=j
              IF(Ind.le.MatrixOfBinomialCoefficients(j-1,M-i)) then 
                Ivec(i)=j-1
                goto 1
              endif
           End Do  qq
   1       Ind=Ind-MatrixOfBinomialCoefficients(Ivec(i)-1,M-i)
           MaxI=Ivec(i)
         End do
   
   2     Nvec(1)=N+M-1-Ivec(1)
   
         Do i=2,M-1
            Nvec(i)=Ivec(i-1)-Ivec(i)-1
         Enddo
   
         Nvec(M)=Ivec(M-1)-1
         
         end  SUBROUTINE  Get_ConfigurationFromIndex_OLD

!>    This routine computes the index Ind of the CI-Vector 
!>    that corresponds to a given number state Nvec. It's basically
!>    an implementation of Eq.20 of the mapping paper, PRA 81 022124.
      SUBROUTINE Get_IndexFromConfiguration(N,M,Nvec,Ind)

      USE Coefficients_Parameters

      IMPLICIT NONE

      INTEGER:: i,N,M,TEMP,Ind
      INTEGER, DIMENSION(MORB)   :: Nvec
      INTEGER, DIMENSION(MORB)   :: Ivec

      Ivec(1)=N+M-1-Nvec(1)

      Do i=2,M-1
         Ivec(i)=Ivec(i-1)-Nvec(i)-1
      Enddo

      TEMP=1

      Do i=1,M-1
         TEMP=TEMP+MatrixOfBinomialCoefficients(Ivec(i)-1,M-i)
      EndDo
      IND=TEMP

      end SUBROUTINE Get_IndexFromConfiguration


!>    This function extracts a two-body matrix element 
!>    with indices K,S,Q,L from the array Rho2_Elements.
      COMPLEX*16 Function RhoAll(K,S,L,Q) 

      use Global_Parameters
      USE Matrix_Elements

      IMPLICIT NONE

      INTEGER,INTENT(IN) :: K,S,L,Q
      INTEGER:: K1,S1,L1,Q1,KSadr,LQadr,Nadr,DNSDim
      INTEGER :: SymmExp, Symm_Bas

      IF (JOB_TYPE.eq.'FER') THEN 
         Symm_Bas=-1
      ELSE 
         Symm_Bas=1
      ENDIF

      DNSDim=Morb*(Morb+1)/2
      SymmExp = 2

      K1=K
      S1=S
      L1=L
      Q1=Q

      IF(K-S.gt.0) THEN 
         K1=S
         S1=K 
         SymmExp = SymmExp + 1
      ENDIF

      KSadr=Morb*(K1-1)-(K1-1)*(K1-2)/2+(S1-K1)+1

      SymmExp = SymmExp + 1
      IF(L-Q.gt.0) THEN 
         Q1=L
         L1=Q
         SymmExp = SymmExp + 1
      ENDIF

      LQadr=Morb*(L1-1)-(L1-1)*(L1-2)/2+(Q1-L1)+1

      IF(KSadr-LQadr.le.0) THEN ! If upper triangle 
         Nadr=DNSDim*(KSadr-1)-(KSadr-1)*(KSadr-2)/2+(LQadr-KSadr)+1
         Rhoall=Rho2_Elements(Nadr)
      ELSE ! Else: lower triangle.
         Nadr=DNSDim*(LQadr-1)-(LQadr-1)*(LQadr-2)/2+(KSadr-LQadr)+1
         Rhoall=Conjg(Rho2_Elements(Nadr))
      ENDIF

      RhoAll = Symm_Bas**SymmExp * RhoAll

      END FUNCTION RhoAll


!>    This function computes the address of a matrix element in the 1d
!>    arrays used with the indices K, S, Q, L for Morb orbitals.     
      INTEGER Function Nadr(K,S,L,Q,Morb) 

      IMPLICIT NONE

      INTEGER*8,INTENT(IN) :: K,S,L,Q
      INTEGER,INTENT(IN) :: Morb 
      INTEGER :: K1,S1,Q1,L1,KSadr,QLadr,DNSDim

      DNSDim=Morb*(Morb+1)/2

      K1=K
      S1=S
      Q1=Q
      L1=L

      IF(K-S.gt.0) THEN 
         K1=S
         S1=K 
      ENDIF

      KSadr=Morb*(K1-1)-(K1-1)*(K1-2)/2+(S1-K1)+1

      IF(L-Q.gt.0) THEN 
        Q1=L
        L1=Q
      ENDIF

      QLadr=Morb*(L1-1)-(L1-1)*(L1-2)/2+(Q1-L1)+1

      IF(KSadr-QLadr.le.0) THEN 
         Nadr=DNSDim*(KSadr-1)-(KSadr-1)*(KSadr-2)/2+(QLadr-KSadr)+1
      ELSE
         Nadr=DNSDim*(QLadr-1)-(QLadr-1)*(QLadr-2)/2+(KSadr-QLadr)+1
         Nadr=-Nadr
      ENDIF


      END FUNCTION Nadr


!>    This function computes the address of a matrix element in the 1d
!>    arrays used with the indices K, S, Q, L for Morb orbitals.     
      INTEGER Function SymmExp(K,S,L,Q,Morb) 

      IMPLICIT NONE

      INTEGER*8,INTENT(IN) :: K,S,L,Q
      INTEGER,INTENT(IN) :: Morb 
      INTEGER :: DNSDim

      DNSDim=Morb*(Morb+1)/2
      SymmExp = 2

      IF(K-S.gt.0) THEN 
         SymmExp = SymmExp + 1
      ENDIF

      IF(Q-L.gt.0) THEN 
        SymmExp = SymmExp + 1
      ENDIF

      END FUNCTION SymmExp


!>    This function computes the address of a matrix element of the
!>    two-body interaction W with
!>    indices K,S,Q,L.
      INTEGER Function NadrW(K,S,Q,L,Morb) 

      IMPLICIT NONE

      INTEGER,INTENT(IN) :: K,S,Q,L,Morb 
      INTEGER :: K1,S1,Q1,L1,KSadr,QLadr,WDNSDim

      WDNSDim=Morb**2*(Morb**2+1)/2
      K1=K
      S1=S
      Q1=Q
      L1=L
      KSadr=Morb*(K1-1)+S1
      QLadr=Morb*(Q1-1)+L1
      IF(KSadr-QLadr.le.0) THEN 
         NadrW=WDNSDim*(KSadr-1)+QLadr
      ELSE
         NadrW=WDNSDim*(QLadr-1)+KSadr
         NadrW=-NadrW
      ENDIF

      END FUNCTION NadrW

      end module addresses
