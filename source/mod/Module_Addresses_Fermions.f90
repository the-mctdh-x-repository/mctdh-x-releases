!>GPLv3
!    MCTDH-X: the multiconfigurational time-dependent Hartree for 
!    indistinguishable particles software
!
!    Copyright (C) 2021  A. U. J. Lode, M. C. Tsatsos, 
!                        E. Fasshauer, S. E. Weiner, 
!                        R. Lin, L. Papariello, P. Molignini, 
!                        C. Lévêque, M. Büttner , J. Xiang, S. Dutta
!
!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <https://www.gnu.org/licenses/>.
!
!
!
!
!<GPLv3










! This is a mercurial test line
!> This module collects the routines necessary for the indexing of the 
!> one-body and two-body matrix elements as well as the coefficients 
!> and configurations of indistinguishable fermions.
      module addresses_fermions

      use addresses

      contains

     
!>    This subroutine computes the distance d^kq for an input occupation number 
!>    vector |n> for Fermions to determine the additional potential (-1) prefactors
!>    in the matrix elements of the reduced one- and two- body densities and other
!>    operators as prescribed in the appendixes of JCP 127, 154103 (2007).
      SUBROUTINE Get_Configuration_Distance(Distance,Nvec_Fermions,&
                                            Slot1,Slot2)

      USE Coefficients_Parameters

      IMPLICIT NONE


      INTEGER :: Distance,Slot1,Slot2
      INTEGER, DIMENSION(Morb) :: Nvec_Fermions
     
      Integer :: I
      
      Distance=2

      IF (Slot2 > Slot1) THEN
        DO I=Slot1,Slot2-1
           Distance=Distance+Nvec_Fermions(I)
        END DO
      END IF

      end SUBROUTINE Get_Configuration_Distance

!>    This subroutine computes the Number state Nvec
!>    corresponding to the index Ind0 of the fermionic CI-vector.
      SUBROUTINE Get_ConfigurationFromIndex_Fermions(Ind0,N0,M0,&
                                                     Nvec_Fermions)
      USE Coefficients_Parameters

      implicit NONE

      INTEGER :: IND0,N0,M0
      integer, DIMENSION(M0) :: Nvec_Fermions

      integer:: i,IND,M,N,j

      IND=IND0-1 !The index in the program starts at 1
      M=M0
      N=N0 

      Do While (M.gt.0)

        IF ((M.gt.N).and.(N.ge.0)) THEN
           J = MatrixOfBinomialCoefficients(M-1,N)
        ELSE
           J = 0
        ENDIF

        IF (IND.ge.J) THEN
           IND=IND-J
           Nvec_Fermions(M)=1
           N=N-1
        ELSE
           Nvec_Fermions(M)=0
        ENDIF 

        M=M-1

      end do
  
      end  subroutine  Get_ConfigurationFromIndex_Fermions

!>    This routine computes the index Ind of the CI-Vector 
!>    that corresponds to a given number state Nvec for Fermions. It's basically
!>    an implementation of Eq.6 of the mapping paper, PRA 81 022124.
      subroutine Get_IndexFromConfiguration_Fermions(N,M,Nvec,Ind)

      USE Coefficients_Parameters

      implicit NONE

      integer:: I,K,N,M,TEMP,Ind,L
      integer, DIMENSION(MORB)   :: Nvec

      I=0
      L=1
      Ind=1

      DO K=1,MORB
         IF (NVEC(K).eq.1) THEN 
            IF (K.gt.L) THEN
               IND=IND+MatrixOfBinomialCoefficients(K-1,L)
            ENDIF
            L=L+1
         ENDIF
      END DO

      end subroutine Get_IndexFromConfiguration_Fermions
 
      end module addresses_fermions 
