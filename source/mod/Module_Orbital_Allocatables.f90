!>GPLv3
!    MCTDH-X: the multiconfigurational time-dependent Hartree for 
!    indistinguishable particles software
!
!    Copyright (C) 2021  A. U. J. Lode, M. C. Tsatsos, 
!                        E. Fasshauer, S. E. Weiner, 
!                        R. Lin, L. Papariello, P. Molignini, 
!                        C. Lévêque, M. Büttner , J. Xiang, S. Dutta
!
!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <https://www.gnu.org/licenses/>.
!
!
!
!
!<GPLv3










!> Module which contains the allocatable orbital data for the Master
!> and slave processes.
      MODULE Orbital_Allocatables

      USE Orbital_Parallelization_Parameters, &
                    Only: LocalPsiDim,OrbitalWrite_Offsets,&
                          Orbital_Blocksize,Local_Offset
      Use Global_Parameters      
      USE CRAB_Parameters, ONLY: CRAB_First
      USE Coefficients_Parameters, ONLY: NConf
      USE Matrix_Elements, ONLY: Rdim,Rdim1  
 
      IMPLICIT NONE

!=============================================================
!=========== Allocatables needed for the integration (single level)
!=============================================================
      COMPLEX*16, ALLOCATABLE, TARGET ::  PSI(:,:)
      COMPLEX*16, ALLOCATABLE ::  FTPSI(:,:)
      COMPLEX*16, ALLOCATABLE, TARGET ::  PSI1(:,:)
      COMPLEX*16, ALLOCATABLE, TARGET ::  PSI2(:,:)
      COMPLEX*16, ALLOCATABLE, TARGET ::  OPSI(:,:)
 !     COMPLEX*16, ALLOCATABLE, TARGET ::  AuxPsi(:)
      COMPLEX*16, ALLOCATABLE ::  PSI_IN(:,:)
      COMPLEX*16, ALLOCATABLE ::  PSI_interpol(:,:)

!=============================================================
!=========== Allocatables needed for the integration (multilevel)
!=============================================================
      COMPLEX*16, ALLOCATABLE, TARGET ::  PSI_Nlevel(:,:,:)
      COMPLEX*16, ALLOCATABLE, TARGET ::  PSI1_Nlevel(:,:,:)
      COMPLEX*16, ALLOCATABLE, TARGET ::  PSI2_Nlevel(:,:,:)
      COMPLEX*16, ALLOCATABLE, TARGET ::  OPSI_Nlevel(:,:,:)
!      COMPLEX*16, ALLOCATABLE, TARGET ::  AuxPsi_Nlevel(:,:)
      COMPLEX*16, ALLOCATABLE ::  PSI_IN_Nlevel(:,:,:)

      REAL*8, ALLOCATABLE :: State_Populations(:)

      COMPLEX*16, ALLOCATABLE :: VTRAP_EXT(:) !< One-Body Potential.
      COMPLEX*16, ALLOCATABLE :: VTRAP_EXT_Nlevel(:,:) !< One-Body Potential for multi-level systems.

      COMPLEX*16, ALLOCATABLE,SAVE :: PSI_OCT(:,:)
      COMPLEX*16, ALLOCATABLE,SAVE :: VIN_OCT(:) 

      INTEGER*8  :: LocalSize

      CONTAINS

!> @ingroup init      
!> @brief Function to allocate everything in the Obital_Allocatables module
      Logical Function Allocate_Orbital_Arrays(MYID,NPROC)

      IMPLICIT NONE

      INTEGER*8  :: offset 
      INTEGER    :: ierr,K,MYID,NPROC

      INTEGER*8  :: Idummy
      REAL*8     :: Rdummy
      COMPLEX*16 :: Cdummy 



      
!======================================================
!=====ALLOCATION OF ORBITAL ARRAYS====================
!======================================================
       
        IF (MPI_ORBS.eqv..FALSE.) THEN
           LocalSize=NDVR_X*NDVR_Y*NDVR_Z
           LocalPsiDim=NDVR_X*NDVR_Y*NDVR_Z
        ELSEIF (MPI_ORBS.eqv..TRUE.) THEN

           allocate(LocalPsiDim(NPROC),stat=ierr)
           call Assign_LocalPsiDim_MPI(NPROC)
           offset=SUM(LocalPsiDim(1:MyID))-LocalPsiDim(1)
           LocalSize=LocalPsiDim(MYID+1)

           Orbital_BlockSize=(MORB+1)*NDVR_X*NDVR_Y*NDVR_Z        &
                             *SIZEOF(CDummy)                      &
                             +2*SIZEOF(RDummy)+SIZEOF(IDummy)     &
                             +(Rdim+Rdim1)*SIZEOF(CDummy)             
           Write(6,*) "Determined Orbital_BlockSize:",Orbital_BlockSize
        ENDIF

      IF (Multi_Level.eqv..FALSE.) THEN

        allocate(PSI(LocalSize,Morb),stat=ierr)
        allocate(FTPSI(LocalSize,Morb),stat=ierr)
        allocate(PSI1(LocalSize,Morb),stat=ierr)
        allocate(PSI2(LocalSize,Morb),stat=ierr)
        allocate(OPSI(LocalSize,Morb),stat=ierr)
!        allocate(AuxPsi(LocalSize*Morb*18),stat=ierr)
        allocate(PSI_IN(LocalSize,Morb),stat=ierr)


        if(ierr /= 0) write(*,*)"allocation error in PSI_*", &
             LocalSize,Morb,ierr


        allocate(VTRAP_EXT(LocalSize),stat=ierr)
        !IF (MPI_ORBS.eqv..FALSE.) THEN
        !   allocate(VTRAP_EXT(NDVR_X*NDVR_Y*NDVR_Z),stat=ierr)
        !ELSEIF (MPI_ORBS.eqv..TRUE.) THEN
        !   allocate(VTRAP_EXT(LocalPsiDim(MYID+1)),stat=ierr)
        !ENDIF

        if(ierr /= 0) write(*,*)"allocation error in Potenital"

          IF (DO_OCT.eqv..TRUE.) THEN
            IF (CRAB_First.eqv..TRUE.) THEN

              allocate(PSI_OCT(LocalSize,Morb),stat=ierr)
              if(ierr /= 0)write(*,*)"allocation error in PSI"
              ALLOCATE(VIN_OCT(Nconf))
            ENDIF
          ENDIF


      !!! Multilevel orbitals
      ELSE 
      !!! Multilevel orbitals

          !LocalPsiDim=NDVR_X*NDVR_Y*NDVR_Z
          !LocalSize=NDVR_X*NDVR_Y*NDVR_Z

          IF ( Conical_Intersection .eqv. .FALSE. ) THEN
           allocate(VTRAP_EXT_Nlevel(LocalSize,Nlevel)&
                                 ,stat=ierr)
          ELSE
           allocate(VTRAP_EXT_Nlevel(LocalSize,Nlevel+1)&
                                 ,stat=ierr)
          ENDIF

          if(ierr /= 0)write(*,*)"allocation error in DVR_Parameters"



        allocate(PSI_Nlevel(LocalSize,Morb,Nlevel),stat=ierr)
        if(ierr /= 0) write(*,*)"allocation error in PSI_Nlevel"
        allocate(PSI1_Nlevel(LocalSize,Morb,Nlevel),stat=ierr)
        if(ierr /= 0) write(*,*)"allocation error in PSI1_Nlevel"
        allocate(PSI2_Nlevel(LocalSize,Morb,Nlevel),stat=ierr)
        if(ierr /= 0) write(*,*)"allocation error in PSI2_Nlevel"
        allocate(OPSI_Nlevel(LocalSize,Morb,Nlevel),stat=ierr)
        if(ierr /= 0)write(*,*)"allocation error in OPSI_Nlevel"
!        allocate(AuxPsi_Nlevel(LocalSize*Morb*18,Nlevel),stat=ierr)
!        if(ierr /= 0)write(*,*)"allocation error in AuxPsi_Nlevel"
        allocate(PSI_IN_Nlevel(LocalSize,Morb,Nlevel),stat=ierr)
        if(ierr /= 0)write(*,*)"allocation error in PSI_IN_Nlevel"
        allocate(State_Populations(Nlevel),stat=ierr)
        if(ierr /= 0)write(*,*)"allocation error in State_Populations"

      ENDIF




!======================================================
!=====ALLOCATION OF ORBITAL ARRAYS====================
!=========================================================================
      write(6,*)"Orbital data allocation is finished"
!=========================================================================
      End Function Allocate_Orbital_Arrays

! The following subroutine assigns the number of grid for each processor in MPI
      Subroutine Assign_LocalPsiDim_MPI(NPROC)
      INTEGER    :: NPROC , iID, DIV, DomainSize

      If (DIM_MCTDH.eq.2) THEN
          DomainSize = NDVR_X
          DIV = NDVR_Y/NPROC
      ElseIf (DIM_MCTDH.eq.3) THEN
          DomainSize = NDVR_X*NDVR_Y
          DIV = NDVR_Z/NPROC
      EndIf

      If (NPROC.eq.1) THEN
         LocalPsiDim(1) = NDVR_X*NDVR_Y*NDVR_Z
      Else
      Do iID = 1,NPROC-1
          LocalPsiDim(iID) = DomainSize*DIV
      EndDo
          LocalPsiDim(NPROC) = DomainSize*(NDVR_X*NDVR_Y*NDVR_Z/DomainSize-DIV*(NPROC-1))
      EndIF

      write(6,*) "In MPI_ORBS, the grids are decomposed into domains along y (2D) or z (3D)  &
                  directions as:", LocalPsiDim/DomainSize
      END Subroutine

      END MODULE Orbital_Allocatables
