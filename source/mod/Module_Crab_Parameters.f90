!>GPLv3
!    MCTDH-X: the multiconfigurational time-dependent Hartree for 
!    indistinguishable particles software
!
!    Copyright (C) 2021  A. U. J. Lode, M. C. Tsatsos, 
!                        E. Fasshauer, S. E. Weiner, 
!                        R. Lin, L. Papariello, P. Molignini, 
!                        C. Lévêque, M. Büttner , J. Xiang, S. Dutta
!
!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <https://www.gnu.org/licenses/>.
!
!
!
!
!<GPLv3










!> This module collects the parameters necessary for the CRAB optimal control.
!> Details of CRAB are given in http://arxiv.org/pdf/1103.0855v2.pdf.
         MODULE CRAB_Parameters

         USE Global_Parameters, ONLY: DO_OCT,OCT_Beta,OCT_Alpha,&
                                     OCT_NGoals,OCT_NC,OCT_TimeSteps,&
                                     NDVR_X,NDVR_Y,NDVR_Z, &
                                     Time_Final,Time_Begin,PI,&
                                     OCT_Restart,OCT_Bounded_Bandwidth,&
                                     OCT_Bandwidth_UpperBound,&
                                     OCT_Bandwidth_LowerBound
         USE Random

         IMPLICIT NONE

         REAL*4, ALLOCATABLE,TARGET ::   OCT_As(:,:),OCT_Bs(:,:)

         REAL*8, ALLOCATABLE :: OCT_Infidelities(:),OCT_Gamma(:,:),&
                                OCT_CurrentGamma(:,:),OCT_Omega(:,:)
         REAL*8,ALLOCATABLE :: OCT_g(:,:)
     
         COMPLEX*16, ALLOCATABLE :: OCT_Goal(:,:),OCT_Density(:)

         REAL*4 :: LastFunctionalValue,OCT_Fret 
         REAL*8 :: OCT_Rand8    
         REAL*4 :: OCT_Rand4    
 
         INTEGER*4 :: OCT_Iterations
         LOGICAL :: CRAB_Continue
         LOGICAL :: CRAB_First
         LOGICAL :: CRAB_BREAK
         LOGICAL :: BREAK_CG
             
         contains
!> This function allocates the necessary variables for a CRAB treatment.
         Logical Function CRAB_Parameters_Init()

         INTEGER :: K,J

         ALLOCATE(OCT_Omega(OCT_NC,OCT_NGoals))
         ALLOCATE(OCT_As(OCT_NC+1,OCT_NGoals))
         ALLOCATE(OCT_Bs(OCT_NC,OCT_NGoals))
 
         ALLOCATE(OCT_Infidelities(OCT_NGoals))
         ALLOCATE(OCT_Goal(NDVR_X*NDVR_Y*NDVR_Z,OCT_NGoals))
         ALLOCATE(OCT_Density(NDVR_X*NDVR_Y*NDVR_Z))
   

         ALLOCATE(OCT_CurrentGamma(OCT_TimeSteps,OCT_Ngoals))

         ALLOCATE(OCT_Gamma(OCT_TimeSteps,OCT_Ngoals))
         ALLOCATE(OCT_g(OCT_TimeSteps,OCT_NGoals))

         IF (OCT_Restart.eqv..FALSE.) THEN 

           OCT_Gamma=1.d0
           OCT_CurrentGamma=1.d0
           
           DO K=1,OCT_NC
             DO J=1,OCT_NGoals    
           
               CALL GNU_Rand4(OCT_Rand4)
               OCT_As(K,J)=OCT_Rand4
           
               IF (OCT_Bounded_Bandwidth.eqv..FALSE.) THEN
                 CALL GNU_Rand(OCT_Rand8)
                    
                 OCT_Omega(K,J)=2.d0*K*PI*(1+(OCT_Rand8-0.5d0))  &
                               /(time_final-time_begin)    
               
               ELSEIF (OCT_Bounded_Bandwidth.eqv..TRUE.) THEN
                 
                 CALL GNU_Rand(OCT_Rand8)
                 OCT_Omega(K,J)=(1.d0*K+OCT_Rand8-0.5d0)  &
         *(OCT_Bandwidth_UpperBound-OCT_Bandwidth_LowerBound)/(OCT_NC+1)
               ENDIF

               CALL GNU_Rand4(OCT_Rand4)
               OCT_Bs(K,J)=OCT_Rand4
           
             END DO
           END DO 
  
           Write(6,*) "CRAB frequencies:", OCT_Omega
           Write(6,*) "CRAB As:", OCT_As
           Write(6,*) "CRAB Bs:", OCT_Bs

         ENDIF

         LastFunctionalValue=(10.0)**37
         CRAB_First=.TRUE.
         BREAK_CG=.FALSE.
         CRAB_BREAK=.FALSE.
         CRAB_Parameters_Init = .TRUE.
    
         End Function CRAB_Parameters_Init

         END MODULE CRAB_Parameters
