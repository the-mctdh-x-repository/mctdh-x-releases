!>GPLv3
!    MCTDH-X: the multiconfigurational time-dependent Hartree for 
!    indistinguishable particles software
!
!    Copyright (C) 2021  A. U. J. Lode, M. C. Tsatsos, 
!                        E. Fasshauer, S. E. Weiner, 
!                        R. Lin, L. Papariello, P. Molignini, 
!                        C. Lévêque, M. Büttner , J. Xiang, S. Dutta
!
!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <https://www.gnu.org/licenses/>.
!
!
!
!
!<GPLv3










!> DVR_Parameters contains and allocates the arrays necessary in
!> dealing with the DVR and the kinetic energy in the DVR. 
!> \todo check for which variables can be moved (from) here (to) from Module Interaction_Parameters.
        MODULE DVR_Parameters

        USE Function_Library
        use Global_Parameters
        USE Interaction_Parameters
#if MKLFFT
        USE MKL_DFTI
#endif

        IMPLICIT NONE

#if MKLFFT
       type(DFTI_DESCRIPTOR), POINTER ::  MKL_Master_Plan_3D,&
                                          MKL_Master_Plan_2D,&
                                          MKL_Master_Plan_1D_X,&
                                          MKL_Master_Plan_1D_Y,&
                                          MKL_Master_Plan_1D_Z
#endif


#if FFTW 
       INTEGER*8 ::          FFTW_Forward_Master_Plan_3D,&
                             FFTW_Backward_Master_Plan_3D,&
                             FFTW_Forward_Master_Plan_2D,&
                             FFTW_Backward_Master_Plan_2D,&
                             FFTW_Forward_Master_Plan_1D_X,&
                             FFTW_Backward_Master_Plan_1D_X,&
                             FFTW_Forward_Master_Plan_1D_Y,&
                             FFTW_Backward_Master_Plan_1D_Y,&
                             FFTW_Forward_Master_Plan_1D_Z,&
                             FFTW_Backward_Master_Plan_1D_Z
#endif

        REAL*8, ALLOCATABLE :: weight(:) !< Weights of the used DVR.
        REAL*8, ALLOCATABLE :: weight_X(:) !< Weight of DVR in X.
        REAL*8, ALLOCATABLE :: ort_X(:) !< Grid of DVR in X.
        REAL*8, ALLOCATABLE :: weight_Y(:) !< Weight of DVR in Y.
        REAL*8, ALLOCATABLE :: ort_Y(:) !< Grid of DVR in Y.
        REAL*8, ALLOCATABLE :: weight_Z(:) !< Weight of DVR in Z.
        REAL*8, ALLOCATABLE :: ort_Z(:) !< Grid of DVR in Z.
        REAL*8, ALLOCATABLE :: Op_X(:,:) !< Kinetic Energy DVR in X. 
        REAL*8, ALLOCATABLE :: Op_Y(:,:) !< Kinetic Energy DVR in Y.
        REAL*8, ALLOCATABLE :: Op_Z(:,:) !< Kinetic Energy DVR in Z.
!        COMPLEX*16, ALLOCATABLE :: VTRAP_EXT(:) !< One-Body Potential.
!        COMPLEX*16, ALLOCATABLE :: VTRAP_EXT_Nlevel(:,:) !< One-Body Potential for multi-level systems.

        COMPLEX*16, ALLOCATABLE :: Cavity_Field(:) !< Cavity Field .
        REAL*8, ALLOCATABLE :: Atom_CavityExpectation(:) !< Expectation value of Atom-Cavity Field .
        COMPLEX*8, ALLOCATABLE :: Atom_PumpExpectation(:) !< Expectation value of Atom-Cavity pumping Field .
        REAL*8, ALLOCATABLE :: BunchingParameter(:) !< Bunching parameter determines the strength of cavity-mediated long-range interactions     
        COMPLEX*8, ALLOCATABLE :: Custom_Cavity_Profile(:,:) !< Cavity and pump profiles for custom cavity 
        COMPLEX*8, ALLOCATABLE :: Custom_Pump_Profile(:,:)  !< Cavity and pump profiles for custom cavity 
        COMPLEX*8, ALLOCATABLE :: Custom_Interfere_Profile_Re(:,:)  !< Cavity and pump profiles for custom cavity 
        COMPLEX*8, ALLOCATABLE :: Custom_Interfere_Profile_Im(:,:)  !< Cavity and pump profiles for custom cavity 
               
        REAL*8, ALLOCATABLE :: ort_kx(:) !< Momentum grid of DVR in X.
        REAL*8, ALLOCATABLE :: ort_ky(:) !< Momentum grid of DVR in Y.
        REAL*8, ALLOCATABLE :: ort_kz(:) !< Momentum grid of DVR in Z.

        COMPLEX*16, ALLOCATABLE :: Vortex_Profile(:,:)  !< Array for the vortex profile to imprint in a relaxation.

        contains

!> This function allocates the arrays needed for the DVR and the kinetic
!> energy.
        logical function DVR_Parameters_init(NPROC,MYID)

        integer :: NPROC,MYID,ierr
        integer*8 :: LocalSize

        IF (MPI_ORBS.eqv..FALSE.) THEN
           allocate(weight(NDVR_X*NDVR_Y*NDVR_Z),stat=ierr)
           if(ierr /= 0)write(*,*)"allocation error in DVR_Parameters"
        ELSEIF (MPI_ORBS.eqv..TRUE.) THEN
           call Calculate_LocalPsiDim_MPI(NPROC,MYID,LocalSize)
           allocate(weight(LocalSize),stat=ierr)
           if(ierr /= 0)write(*,*)"allocation error in DVR_Parameters"
        ENDIF

        allocate(weight_X(NDVR_X),stat=ierr)
        if(ierr /= 0)write(*,*)"allocation error in DVR_Parameters"
        allocate(   ort_X(NDVR_X),stat=ierr)
        if(ierr /= 0)write(*,*)"allocation error in DVR_Parameters"
        allocate(weight_Y(NDVR_Y),stat=ierr)
        if(ierr /= 0)write(*,*)"allocation error in DVR_Parameters"
        allocate(   ort_Y(NDVR_Y),stat=ierr)
        if(ierr /= 0)write(*,*)"allocation error in DVR_Parameters"
        allocate(weight_Z(NDVR_Z),stat=ierr)
        if(ierr /= 0)write(*,*)"allocation error in DVR_Parameters"
        allocate(   ort_Z(NDVR_Z),stat=ierr)
        if(ierr /= 0)write(*,*)"allocation error in DVR_Parameters"

        IF(DVR_X==4) allocate(Op_X(1,1),stat=ierr)
        if(ierr /= 0)write(*,*)"allocation error in DVR_Parameters"
        IF(DVR_X/=4) allocate(Op_X(NDVR_X,NDVR_X),stat=ierr)
        if(ierr /= 0)write(*,*)"allocation error in DVR_Parameters"

        IF(DVR_Y==4) allocate(Op_Y(1,1),stat=ierr)
        if(ierr /= 0)write(*,*)"allocation error in DVR_Parameters"
        IF(DVR_Y/=4) allocate(Op_Y(NDVR_Y,NDVR_Y),stat=ierr)
        if(ierr /= 0)write(*,*)"allocation error in DVR_Parameters"

        IF(DVR_Z==4) allocate(Op_Z(1,1),stat=ierr)
        if(ierr /= 0)write(*,*)"allocation error in DVR_Parameters"
        IF(DVR_Z/=4) allocate(Op_Z(NDVR_Z,NDVR_Z),stat=ierr)
        if(ierr /= 0)write(*,*)"allocation error in DVR_Parameters"


        IF (Cavity_BEC.eqv..TRUE.) THEN
          allocate(Cavity_Field(NCavity_Modes),stat=ierr)
          if(ierr /= 0)write(*,*)"allocation error in DVR_Parameters"
          allocate(Atom_CavityExpectation(NCavity_Modes),stat=ierr)
          if(ierr /= 0)write(*,*)"allocation error in DVR_Parameters"
          allocate(Atom_PumpExpectation(NCavity_Modes),stat=ierr)
          if(ierr /= 0)write(*,*)"allocation error in DVR_Parameters"
          allocate(BunchingParameter(NCavity_Modes),stat=ierr)
          if(ierr /= 0)write(*,*)"allocation error in DVR_Parameters"
          IF (Custom_Cavity.eqv..TRUE.) THEN
            allocate(Custom_Cavity_Profile(NDVR_X*NDVR_Y*NDVR_Z,NCavity_Modes),stat=ierr)
            if(ierr /= 0)write(*,*)"allocation error in DVR_Parameters"
            allocate(Custom_Pump_Profile(NDVR_X*NDVR_Y*NDVR_Z,NCavity_Modes),stat=ierr)
            if(ierr /= 0)write(*,*)"allocation error in DVR_Parameters"
            allocate(Custom_Interfere_Profile_Re(NDVR_X*NDVR_Y*NDVR_Z,NCavity_Modes),stat=ierr)
            if(ierr /= 0)write(*,*)"allocation error in DVR_Parameters"
            allocate(Custom_Interfere_Profile_Im(NDVR_X*NDVR_Y*NDVR_Z,NCavity_Modes),stat=ierr)
            if(ierr /= 0)write(*,*)"allocation error in DVR_Parameters"
          ENDIF
        ENDIF      


        IF ((Vortex_Imprint.eqv..TRUE.)&
          .or.(Vortex_Seeding.eqv..TRUE.))  THEN
          Allocate(Vortex_Profile(NDVR_X*NDVR_Y*NDVR_Z,Morb),stat=ierr)
          if(ierr /= 0)write(*,*)"allocation error in DVR_Parameters"
        ENDIF       
 
        allocate(   ort_kx(NDVR_X),stat=ierr)
        if(ierr /= 0)write(*,*)"allocation error in DVR_Parameters"
        allocate(   ort_ky(NDVR_Y),stat=ierr)
        if(ierr /= 0)write(*,*)"allocation error in DVR_Parameters"
        allocate(   ort_kz(NDVR_Z),stat=ierr)
        if(ierr /= 0)write(*,*)"allocation error in DVR_Parameters"

        if(ierr /= 0) then
           write(*,*)"allocation error for DVR_Parameters"
           DVR_Parameters_init=.false.
        else
           if(ierr == 0)write(*,*)"allocation ok for DVR_Parameters"
           DVR_Parameters_init=.true.
        endif

        Call Get_Operators(MYID,NPROC,LocalSize)

        end function

!> Get the DVR representation of the Kinetic Energy Operator
      SUBROUTINE Get_Operators(MYID,NPROC,LocalSize)

      USE Global_Parameters
!      USE DVR_Parameters
      USE Interaction_Parameters
     
      IMPLICIT NONE

      INTEGER :: DVRMETHOD
      INTEGER*8 :: I,J,K,ind,m
      REAL*8 :: xi,xf
      EXTERNAL DCOPY
      INTEGER :: MYID,NPROC
      integer*8 :: LocalSize

      WRITE(1003,*) "########################################"
      WRITE(1003,*) "####OPERATOR DVR INITIALIZATION#########"
      WRITE(1003,*) "########################################"

      Op_X=0d0
      Op_Y=0d0
      Op_Z=0d0
      DVRMETHOD=DVR_X
      IF (DVRMETHOD.ne.6) THEN
        xi=X_initial
        xf=X_final
      ELSEIF (DVRMETHOD.eq.6) THEN
        xi=0.d0
        xf=1.d0*NDVR_X
      ENDIF

     ! Get the DVR in x direction
       CALL Init_KineticEnergy_DVR(xi,xf,NDVR_X,Op_X,weight_X,&
                                   ort_X,Ort_Kx,DVRMETHOD) 


      ort_Y=0.0d0
      ort_Z=0.0d0
      weight_Y=1.0d0
      weight_Z=1.0d0
      DVRMETHOD=DVR_Y
      IF (DVRMETHOD.ne.6) THEN
      xi=Y_initial
      xf=Y_final
      ELSEIF (DVRMETHOD.eq.6) THEN
        xi=0.d0
        xf=1.d0*NDVR_Y
      ENDIF

      IF(NDVR_Y.NE.1) THEN
         CALL Init_KineticEnergy_DVR(xi,xf,NDVR_Y,Op_Y,weight_Y,&
                                     ort_Y,Ort_Ky,DVRMETHOD)
      ENDIF

      DVRMETHOD=DVR_Z
      IF (DVRMETHOD.ne.6) THEN
        xi=Z_initial
        xf=Z_final
      ELSEIF (DVRMETHOD.eq.6) THEN
        xi=0.d0
        xf=1.d0*NDVR_Z
      ENDIF

      IF(NDVR_Z.NE.1) then 
         CALL Init_KineticEnergy_DVR(xi,xf,NDVR_Z,Op_Z,weight_Z,&
                                     ort_Z,Ort_Kz,DVRMETHOD)
      ENDIF

      write(1003,*)"Simulation is ",DIM_MCTDH,"dimensional"
      write(1003,*)"total number of DVR functions:", &
                NDVR_X*NDVR_Y*NDVR_Z
 
      ind=1
 
     ! Fill the array with the DVR weights 
      IF (MPI_ORBS.eqv..FALSE.) THEN
        Do K=1,NDVR_Z
           Do J=1,NDVR_Y
              Do I=1,NDVR_X
                 weight(ind)=weight_X(I)*weight_Y(J)*weight_Z(K)
                 ind=ind+1
              EndDo
           EndDo
        EndDo
      ELSEIF (MPI_ORBS.eqv..TRUE.) THEN
        Do ind=1,LocalSize
          m=ind
          call get_ijk_from_m_MPI(m,NDVR_X,NDVR_Y,i,j,k,MYID,NPROC)
          weight(ind)=weight_X(I)*weight_Y(J)*weight_Z(K)
        EndDo
      ENDIF

      END SUBROUTINE Get_Operators

!> Subroutine that initializes the DVR of the Kinetic Energy.
      subroutine Init_KineticEnergy_DVR    &
                (xi,xf,igdim,hdvr,weight,ort,ort_p,DVRMETHOD)


      USE   Interaction_Parameters

      Implicit NONE

      integer*8 j,i,ndim,igdim,kdim
      integer kmin
      integer hotyp,ierr
      real*8, ALLOCATABLE :: trafo(:,:),dif1mat(:,:),dif2mat(:,:)
      real*8 hofm,hoxeq,dx
      real*8  weight(igdim),ort(igdim),ort_p(igdim)
      real*8  rpbaspar(3), rsbaspar(5),PI1
      integer ::  ipbaspar1dummy(7)=0
      integer ::  ipbaspardummy(7)=0
      integer ::  isbaspardummy(7)=0

      real*8, ALLOCATABLE ::  workr(:)
      COMPLEX*16, ALLOCATABLE ::  psi(:,:)
      COMPLEX*16, ALLOCATABLE ::  exphin(:,:),exprueck(:,:)
      real*8 hdvr(igdim,igdim)
      integer DVRMETHOD,indimhalf
      real*8 xmassinp,xfreqinp,xeqlinp,xi,xf,xmass
      real*8 xall,wscl,xeql,xmass1,xfreq1,xfreq
      parameter(xmassinp=1.0d0,xfreqinp=1.0d0,xeqlinp=0.0d0)
      parameter        (PI1 = 3.141592653589793238462643d0)

      external schmidtortho

!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCc
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCc
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCc
      IF ((DVRMETHOD/=4).and.(DVRMETHOD/=6)) THEN ! IF NOT FFT or BH DVR 
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCc
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCc
        allocate(trafo(igdim,igdim),stat=ierr)

        if(ierr /= 0) then
          write(*,*)"allocation error in Init_KineticEnergy_DVR: trafo"
        endif

        allocate(dif1mat(igdim,igdim),stat=ierr)

        if(ierr /= 0) then
         write(*,*)"allocation error in Init_KineticEnergy_DVR: dif1mat"
        endif

        allocate(dif2mat(igdim,igdim),stat=ierr)

        if(ierr /= 0) then
         write(*,*)"allocation error in Init_KineticEnergy_DVR: dif2mat"
        endif

      ENDIF

      indimhalf=(igdim+1)/2
      ndim=igdim
 

!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCc
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCc
      IF(DVRMETHOD.EQ.1) THEN ! Harmonic Oscillator DVR
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCc
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCc
        allocate(psi(igdim,igdim),stat=ierr)

        if(ierr /= 0) then
          write(*,*)"allocation error in HO Init_KineticEnergy_DVR"
        endif

        wscl=1.0D0
        xmass=xmassinp/wscl
        xfreq=1.0D0*xfreqinp/wscl
        xeql=xeqlinp

        hofm=xmass*xfreq
        hoxeq=xeql
        dx=0.0D0
        dx=xf-xi
        hotyp=1
        call initho(trafo,ort,dif2mat,dif1mat,ndim &
                        ,hofm,hoxeq,dx,hotyp)

        rsbaspar(1)=hoxeq
        rsbaspar(2)=0.0d0
        rsbaspar(3)=dsqrt(2.0D0*xfreq*xmass)
        rsbaspar(4)=0.0D0
        rsbaspar(5)=xmass

        call gengauss(psi,ndim,ndim,weight,  &
               trafo,ort,1,                  &
               rpbaspar,ipbaspardummy,ipbaspar1dummy, &
               rsbaspar,isbaspardummy,ndim)

        do i=1,ndim
           weight(i)=weight(i)*DSQRT(DSQRT(PI)) 
        enddo

        if(xeql.eq.0.0d0) then
          do i=1,indimhalf
            ort(i)=-1.0D0*ort(ndim-i+1) 
            weight(i)=weight(ndim-i+1) 
          enddo
        endif 

        write(1003,'(a50,f20.16,a3,f20.16,a8,i10)')  &
        "DVR Method Harmonic Oscillator on interval  [", &
        ort(1),":",ort(ndim),"] DIM ",ndim

!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCc
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCc
      ELSEIF(DVRMETHOD.EQ.5) THEN ! Exonential DVR
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCc
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCc
        allocate(exphin(igdim,igdim),stat=ierr)

        if(ierr /= 0) then
          write(*,*)"allocation error in Exp Init_KineticEnergy_DVR"
        endif

        allocate(exprueck(igdim,igdim),stat=ierr)

        if(ierr /= 0) then
          write(*,*)"allocation error in Exp Init_KineticEnergy_DVR"
        endif

        xall=xf-xi
        xi=xi
        xf=xf-(xall)/(ndim)
        kmin=1
        kdim=1

        CALL initexp(trafo,ort,dif2mat,dif1mat,exphin,exprueck,ndim, &
                         xi,xf,kmin,kdim)

        rsbaspar(1)=0.d0
        rsbaspar(2)=0.D0
        rsbaspar(3)=0.d0
        rsbaspar(4)=0.D0
        rsbaspar(5)=0.d0

        CALL dvrweights(trafo,ort,ndim,ndim,weight,5,   &
                      rpbaspar,ipbaspardummy,ipbaspar1dummy, &
                      .false.,.false.)

        write(1003,'(a50,f20.16,a3,f20.16,a8,i10)')   &
        "DVR Method Exponential DVR     on interval  [", &
        ort(1),":",ort(ndim),") DIM ",ndim

!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCc
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCc
      ELSEIF(DVRMETHOD.EQ.3) THEN   ! Sine DVR
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCc
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCc
        allocate(workr(igdim*igdim),stat=ierr)

        if(ierr /= 0) then
          write(*,*)"allocation error in SIN Init_KineticEnergy_DVR"
        endif

        xall=xf-xi
        xi=xi+(xall)/(ndim+1)
        xf=xf-(xall)/(ndim+1)

        CALL initsin(trafo,ort,dif2mat,dif1mat,ndim,xi,xf)

        rsbaspar(1)=0.d0
        rsbaspar(2)=0.d0
        rsbaspar(3)=0.d0
        rsbaspar(4)=0.d0
        rsbaspar(5)=0.d0

        CALL dvrweights(trafo,ort,ndim,ndim,weight,3, &
                     rpbaspar,ipbaspardummy,ipbaspar1dummy, &
                     .false.,.false.)
        write(1003,'(a50,f20.16,a3,f20.16,a8,i10)')  &
        "DVR Method          SIN DVR    on interval  [", &
        ort(1),":",ort(ndim),"] DIM ",ndim

!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCc
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCc
      ELSEIF(DVRMETHOD.EQ.4) THEN ! FFT Colocation
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCc
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCc

        call getpos(xi,xf,igdim,ort)
        call getmom(xi,xf,igdim,ort_p)
 
        do i=1,ndim
          weight(i) = sqrt(ort(2)-ort(1))
        enddo
        write(1003,'(a50,f20.16,a3,f20.16,a8,i10)')  &
        "DVR Method          FFT DVR    on interval  [", &
        ort(1),":",ort(ndim),"] DIM ",ndim

        return
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCc
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCc
      ELSEIF(DVRMETHOD.EQ.6) THEN ! Bose Hubbard "DVR
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCc
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCc
          do i=1,igdim
             ort(i)=-(igdim/2)+(i-1)*1.d0
             weight(i)=1.d0
          end do
          return
      ENDIF

      xmass1=xmassinp
      xfreq1=xfreqinp
      xmass1=mass
      Do i=1,ndim
         Do j=1,ndim
            hdvr(i,j)=(-1.0D0/(2.0D0*xmass1))*dif2mat(i,j)
         enddo
      enddo 

      deallocate(trafo,dif1mat,dif2mat)

      IF(DVRMETHOD.EQ.1) deallocate(psi)
      IF(DVRMETHOD.EQ.3) deallocate(workr)
      IF(DVRMETHOD.EQ.5) deallocate(exphin,exprueck)

      return
      end subroutine Init_KineticEnergy_DVR 

!>   According to the selected DVR and its parameters -- get the weights.
      subroutine dvrweights(trafo,ort,gdim1,ggdim,weight,basis, &
                            rpbaspar,ipbaspar,ipbaspar1,lsq,lnw)
      implicit none

      integer i,blz,ibleg,g,basis,ipbaspar(*),ipbaspar1(*)
      integer*8 gdim1,ggdim
      real*8  trafo(ggdim,gdim1),ort(gdim1),hoxeq,hofreq,homass, &
              x0,breite,xx,fac,weight(gdim1),rpbaspar(*)
      logical lsq,lnw


      if( lnw .or. basis.le.0 .or. basis.eq.6 .or. basis.eq.8 ) then
         do g=1,gdim1
            weight(g) = 1.d0
         enddo
         return
      end if

      if( basis.ge.3 .and. basis.le.5 ) then
         do g=1,gdim1
            weight(g) = sqrt(ort(2)-ort(1))
         enddo
         goto 100
      end if


      if (basis .eq. 1 .or. basis .eq. 11) then
         hoxeq  = rpbaspar(1)
         hofreq = rpbaspar(2)
         homass = rpbaspar(3)
      else if (basis.eq.2 .or. basis.eq.12) then
         blz    = ipbaspar(1)
         ibleg  = ipbaspar(2)
      else if (basis.eq.7 ) then
         blz    = ipbaspar1(3)
         ibleg  = ipbaspar(1)
      else if (basis.eq.9 ) then
         blz    = ipbaspar(5)
         ibleg  = ipbaspar(1)
      else if (basis.ge.15 .and. basis.le.18) then
         x0     = rpbaspar(1)
         breite = rpbaspar(2)
      endif

      if ( basis.eq.2 .or. basis.eq.7 .or. &
           basis.eq.9 .or. basis.eq.12 ) then
         fac = 1.d0/dble(blz+1)
         do i = 1, blz+1
            fac = fac*2*i/dble(2*i-1)
         enddo
         fac = sqrt(fac)
      endif


      do g=1,gdim1
 
        if (basis.eq.1) then 
            weight(g) = trafo(1,g)*(homass*hofreq)**(-0.25d0) &
                 *exp(0.5d0*homass*hofreq*(ort(g)-hoxeq)**2)
         elseif (basis.eq.11) then
            weight(g) = trafo(1,g)*(homass*hofreq)**(-0.75d0) &
                 *exp(0.5d0*homass*hofreq*(ort(g)-hoxeq)**2) &
                  /(ort(g)-hoxeq)
 
         elseif (basis.eq.2 .or. basis.eq.7 .or. &
                 basis.eq.9 .or. basis.eq.12) then
            if (blz.eq.0) then
               weight(g) = trafo(1,g)*fac
            elseif (blz.gt.0) then
               weight(g) = trafo(1,g)*fac/sin(ort(g))**blz
            else
               write(6,*) ' DVRweights, wrong blz: ', blz
               stop
            endif
            if((ibleg.gt.0) .and. (mod(ibleg+blz,2).eq.1)) &
               weight(g) = weight(g)/(sqrt(dble(2*blz+3))*cos(ort(g)))

         elseif (basis.eq.15 ) then
            xx = (ort(g)-x0)/breite
            weight(g) = trafo(1,g)*exp(0.5d0*xx)*sqrt(breite/xx)

         elseif (basis.eq.16 ) then
            xx = (ort(g)-x0)/breite
            weight(g) = trafo(1,g)*exp(0.5d0*xx)*sqrt(2.d0*breite)/xx

         elseif (basis.eq.17 ) then
            xx = (ort(g)-x0)/breite
            weight(g)=trafo(1,g)*exp(0.5d0*xx)*sqrt(6.d0*breite/xx)/xx

         elseif (basis.eq.18 ) then 
            xx = (ort(g)-x0)/breite 
            weight(g) = trafo(1,g)*exp(0.5d0*xx)*sqrt(24.d0*breite) &
                                    /(xx*xx)


         else
            write(6,*) ' DVRweights: Cannot do basis no:', basis
            stop
         endif

      enddo

 100  if(lsq) then
         do g = 1, gdim1
            weight(g) = weight(g)**2
         enddo
       endif

      return
      end subroutine dvrweights


!> Initialize the harmonic oscillator DVR.
      subroutine initho(trafo,ort,dif2mat,dif1mat,gdim &
                         ,hofm,hoxeq,dx,hotyp)

      implicit none

      integer g,g1,g2,error,hotyp
      integer*8 gdim
      real*8 trafo(gdim,gdim),ort(gdim),dif2mat(gdim,gdim) &
           ,dif1mat(gdim,gdim),hofm,hoxeq,x,dx


      call zerovxd(ort,gdim)


      do g=1,gdim-1
         dif2mat(g,1)=dsqrt(g/(2.d0*hofm))
      enddo


      call DSTEQR('I',INT(gdim,4),ort,dif2mat,trafo,INT(gdim,4),&
                   dif1mat,error)
      if (error.ne.0) then
         write(6,*) 'Error in diagonalising HO-coordinate-matrix'
      endif

 
      if(hotyp .eq. 1) then
         hofm = dx/(ort(gdim)-ort(1))
         do  g = 1, gdim
            ort(g) = ort(g)*hofm
         end do
         hofm = hofm**(-2)
      end if

      do g=1,gdim
         if(trafo(1,g) .lt. 0.d0 ) then
            do g1=1,gdim
               trafo(g1,g) = -trafo(g1,g) 
            enddo
         end if
      enddo


      call zeromxd(dif2mat,gdim,gdim)


      do g=1,gdim
         x=(g-0.5d0)*hofm*2.d0


         do g1=1,gdim
            do g2=1,gdim
               dif2mat(g2,g1)=dif2mat(g2,g1)-trafo(g,g2)*x*trafo(g,g1)
            enddo
         enddo
      enddo


      do g=1,gdim
         dif2mat(g,g) = dif2mat(g,g) + (hofm*ort(g))*(hofm*ort(g))
      enddo
 


      call zeromxd(dif1mat,gdim,gdim)
      
      do g=1,gdim-1


         x = -dsqrt(hofm*g/2.d0)


         do g1=1,gdim
            do g2=1,gdim
               dif1mat(g2,g1)=dif1mat(g2,g1)+trafo(g+1,g2) &
                    *trafo(g,g1)*x-trafo(g,g2)*trafo(g+1,g1)*x
            enddo
         enddo
      enddo



      do g=1,gdim
         ort(g)=ort(g)+hoxeq
      enddo
      return
      end subroutine initho



!> Initialize the real harmonic oscillator DVR.
      subroutine initrho(trafo,ort,dif2mat,dif1mat,gdim &
                         ,hofm,hoxeq,xi,dx,hotyp,message)

      implicit none

      integer*8 gdim
      integer g,g1,g2,error,hotyp
      real*8 trafo(gdim,gdim),ort(gdim),dif2mat(gdim,gdim) &
           ,dif1mat(gdim,gdim),hofm,hoxeq,x,xi,dx
      character*(*) message



         do g=1,gdim-1
            dif2mat(g,1)=dsqrt(dble(2*g*(2*g+1)))/(2.d0*hofm)
         enddo


         do g=1,gdim
            ort(g) = (4*g-1)/(2.d0*hofm)
         enddo


      call DSTEQR('I',INT(gdim,4),ort,dif2mat,trafo,INT(gdim,4),&
                 dif1mat,error)

      if (error.ne.0) then
         message = 'Error in diagonalising HO-coordinate-matrix'
      endif

      
      do g=1,gdim
         ort(g) = dsqrt(ort(g))
      enddo

 
      if(hotyp .eq. 1) then
         hofm  = dx/(ort(gdim)-ort(1))
         do  g = 1, gdim
            ort(g) = ort(g)*hofm 
         end do
         hoxeq = xi - ort(1)
         hofm  = hofm**(-2)
      end if

      do g=1,gdim
         if(trafo(1,g) .lt. 0.d0 ) then
            do g1=1,gdim
               trafo(g1,g) = -trafo(g1,g) 
            enddo
         end if
      enddo


      call zeromxd(dif2mat,gdim,gdim)


      do g=1,gdim
         x=(2*g-0.5d0)*hofm*2.d0


         do g1=1,gdim
            do g2=1,gdim
               dif2mat(g2,g1)=dif2mat(g2,g1)-trafo(g,g2)*x*trafo(g,g1)
            enddo
         enddo
      enddo


      do g=1,gdim
         dif2mat(g,g) = dif2mat(g,g) + (hofm*ort(g))**2
      enddo
 


      call zeromxd(dif1mat,gdim,gdim)
      
      do g=1,gdim-1


         x = -dsqrt(dble(2*g*(2*g+1)))


         do g1=1,gdim
            do g2=1,gdim
               dif1mat(g2,g1)=dif1mat(g2,g1)+trafo(g+1,g2) &
                    *trafo(g,g1)*x-trafo(g,g2)*trafo(g+1,g1)*x
            enddo
         enddo
      enddo



      do g=1,gdim
         ort(g)=ort(g)+hoxeq
      enddo

      return
      end subroutine initrho



!> Initialize sine DVR.
      subroutine initsin(trafo,ort,dif2mat,dif1mat,gdim,xi,xf)

      implicit none

      integer*8 g1,g2,gdim
      real*8 trafo(gdim,gdim),ort(gdim),dif2mat(gdim,gdim) &
           ,dif1mat(gdim,gdim), &
           x,xi,xf,deltax,fac1,Pi

      Pi     = 4.d0*atan(1.d0)
      deltax = (xf-xi)/dble(gdim-1)
      x      = gdim+1.d0
      fac1   = Pi/x


      do g1=1,gdim
         ort(g1)=xi+(g1-1)*deltax
      enddo         


      do g1=1,gdim
         do g2=1,gdim
            trafo(g2,g1)= dsqrt(2d0/x)*sin(g2*g1*fac1)
         enddo
      enddo


      do g1=1,gdim
         do g2=1,g1-1
            dif2mat(g2,g1)=-(Pi/deltax)**2  &
                 *2d0*(-1d0)**(g2-g1)/x**2  &
                 *sin(g2*fac1)*sin(g1*fac1) &
                 /(cos(g2*fac1)-cos(g1*fac1))**2
            dif2mat(g1,g2)=dif2mat(g2,g1)
         enddo
         dif2mat(g1,g1)=-(Pi/deltax)**2 &
              *(1d0/3d0+1d0/(6d0*x**2) &
              -1d0/(2d0*x**2*sin(g1*fac1)**2))
      enddo        

         

      fac1 = 4.d0/((gdim+1)*deltax)
      do g1=1,gdim 
         do g2=1,g1-1
            if (mod(g2+g1,2).eq.0) then
               dif1mat(g2,g1) = 0.d0
            else
               dif1mat(g2,g1) = fac1*g2*g1/(g2**2-g1**2)
            endif
            dif1mat(g1,g2) = -dif1mat(g2,g1)
         enddo
         dif1mat(g1,g1) = 0.d0 
      enddo
      
      if(gdim.eq.2 .and. abs(xi+0.5d0).lt.1.0d-9 .and.  &
                         abs(xf-0.5d0).lt.1.0d-9 ) then
         dif1mat(1,1) = 0.d0
         dif1mat(1,2) = -0.5d0
         dif1mat(2,1) = 0.5d0
         dif1mat(2,2) = 0.d0
         dif2mat(1,1) = 0.d0
         dif2mat(1,2) = 0.5d0
         dif2mat(2,1) = 0.5d0
         dif2mat(2,2) = 0.d0
         PRINT * , 'SPIN Form'
      else
      endif

      return
      end subroutine initsin





      subroutine initexp(trafo,ort,dif2mat,dif1mat, &
                         exphin,exprueck,gdim,  &
                         xi,xf,kmin,kdim)

      implicit none

      integer g,g1,g2,m,kmin
      integer*8 gdim,kdim

      real*8 trafo(gdim,gdim),ort(gdim),dif2mat(gdim,gdim) &
            ,dif1mat(gdim,gdim),xi,xf,deltax,fac1,fac2,Pi
      complex*16 exphin(gdim,kdim),exprueck(kdim,gdim)


      if( mod(gdim,2) .eq. 0 )  then
         write(6,*) ' ERROR in init1.f : '
         write(6,*) 'Number of grid points must be odd for exp-DVR'
         stop
      end if

      Pi     = 4.d0*atan(1.d0)
      deltax = (xf-xi)/dble(gdim-1)
      fac1   = Pi/(gdim*deltax)
      fac2   = Pi/dble(gdim)



      do g=1,gdim
         ort(g)=xi+(g-1)*deltax
      enddo         


      call zeromxd(trafo,gdim,gdim)


      do g1=1,kdim
         m=g1-1+kmin
         do g2=1,gdim
            exphin(g2,g1) = exp(-dcmplx(0.d0,m*ort(g2)))/ &
                            dsqrt(dble(gdim)) 
            exprueck(g1,g2) = exp(dcmplx(0.d0,m*ort(g2)))/ &
                              dsqrt(dble(gdim))
         enddo
      enddo


      do g1=1,gdim
         do g2=1,g1-1
            g = g1-g2
            dif2mat(g1,g2) = -2.d0*fac1**2 *( (-1)**g ) &
                         * cos(fac2*g)/(sin(fac2*g))**2
            dif2mat(g2,g1) =  dif2mat(g1,g2)   
         enddo
         dif2mat(g1,g1) = -fac1**2 *(gdim**2-1)/3.d0
      enddo



      do g1=1,gdim
         do g2=1,g1-1
            g = g1-g2
            dif1mat(g1,g2) =  fac1*( (-1)**g ) /sin(fac2*g)
            dif1mat(g2,g1) =  -dif1mat(g1,g2)   
         enddo
         dif1mat(g1,g1) = 0.d0
      enddo

      return
      end subroutine initexp





!> Initialize the FFT collocation.
      subroutine initfft(ort,fftp,gdim,xi,xf)

      implicit none

      integer gdim,g
      real*8  xi,xf,deltax,length,deltap,pmax,Pi
      real*8  ort(0:gdim-1), fftp(0:gdim-1)

      Pi=4.d0*atan(1.d0)

      deltax = (xf-xi)/(gdim-1)
      do g=0,gdim-1
         ort(g)=xi+g*deltax
      enddo

      length = gdim*deltax
      deltap = 2d0*Pi/length
      pmax   = Pi/deltax 
      if (mod(gdim,2).eq.0) then
         do g=0,gdim/2-1
            fftp(g)=g*deltap
            fftp(g+gdim/2)=-pmax+g*deltap
         enddo
      else
         do g=0,gdim/2
            fftp(g)=g*deltap
         enddo
         do g=0,gdim/2-1
            fftp(g+gdim/2+1)=-pmax+(g+0.5d0)*deltap
         enddo
      endif      

      return
      end subroutine initfft        


!> Initialize spherical harmonics DVR.
      subroutine initsphfbr(gdim,kinsph,jzsph,jzzsph,jsph,msph, &
                            jmax,sphtyp,mmax,mincr,mmin,ilog)

      implicit none

      integer gdim,jsph(gdim),msph(gdim),jmax,sphtyp,ilog
      integer m,j0,j,g,xx,mmax,mincr,mmin
      real*8  kinsph(gdim),jzsph(gdim),jzzsph(gdim)


      g=0
      if (sphtyp.le.2)  then
         if (sphtyp.eq.1) then
            j0=0
         else
            j0=mod(jmax,2)
         endif

         do j=j0,jmax,sphtyp
            do m=-j,j,1
               g=g+1
               jsph(g)=j
               msph(g)=m
            enddo
         enddo

      else

         if (sphtyp.eq.3) then
           xx=2
           j0=mod(jmax,2)
         else
           xx=1
           j0=0
         endif

         do j=j0,jmax,xx
            do m=mmin,mmax,mincr
               if (iabs(m).le.min(mmax,j)) then
                  g=g+1
                  jsph(g)=j
                  msph(g)=m
               endif
            enddo
         enddo

      endif

      if (gdim.ne.g) then
         write(ilog,'(a,2i5)') '+++ problem in initsphfbr ',gdim,g
         stop
      endif


      do g=1,gdim
         kinsph(g)=jsph(g)*(jsph(g)+1)
         jzsph (g)=msph(g)
         jzzsph(g)=msph(g)**2
      enddo

      return
      end subroutine initsphfbr


!> Initialize the laguerre DVR.
      subroutine initlagu(trafo,ort,dif2mat,dif1mat,workr,gdim, &
                          x0,breite,xi,xf,typ,icut,message,basis)

      implicit none

      integer g,g1,gdim,typ,icut,error,basis,alpha
      real*8  trafo(gdim,gdim),ort(gdim),workr(gdim,gdim), &
              dif1mat(gdim,gdim),dif2mat(gdim,gdim), &
              x0,breite,xi,xf
      character*(*) message
      character*5  clgu


      if(basis.eq.15) then
         alpha = 1
         clgu  = 'Lagu1'
         do g=1,gdim
            trafo(g,g) = 0.25d0*(1-2*g)/breite**2
            do g1=1,g-1
               trafo(g,g1) = -0.5d0*dsqrt(dble(g1)/dble(g))*g1/breite**2
               trafo(g1,g) = trafo(g,g1)
            enddo
         enddo
      elseif(basis.eq.16) then
         alpha = 2
         clgu  = 'Lagu2'
         do g=1,gdim
            trafo(g,g) = dble(1-4*g)/(12.d0*breite**2)
            do g1=1,g-1
               trafo(g,g1) = -dsqrt(dble(g1*(g1+1))/dble(g*(g+1))) &
                   * dble(2*g1+1)/(6.d0*breite**2)
               trafo(g1,g) = trafo(g,g1)
            enddo
         enddo
      elseif(basis.eq.17) then
         alpha = 3
         clgu  = 'Lagu3'
         do g=1,gdim
            trafo(g,g) = -g/(4.d0*breite**2)
            do g1=1,g-1
               trafo(g,g1) = dsqrt(dble(g1*(g1+1)*(g1+2))/ &
                              dble(g*(g+1)*(g+2))) &
                            * dble(-g1-1)/(4.d0*breite**2)
               trafo(g1,g) = trafo(g,g1)
            enddo
         enddo
      elseif(basis.eq.18) then
         alpha = 4
         clgu  = 'Lagu4'
         do g=1,gdim
            trafo(g,g) = -dble(4*g+1)/(20.d0*breite**2)
            do g1=1,g-1
               trafo(g,g1) = dsqrt((dble(g1*(g1+1))/dble(g*(g+1)))* &
                          (dble((g1+2)*(g1+3))/dble((g+2)*(g+3)))) &
                   * dble(-2*g1-3)/(10.d0*breite**2)
               trafo(g1,g) = trafo(g,g1)
            enddo
         enddo
      endif

      g = gdim*gdim   
      call dsyev('V','U',gdim,trafo,gdim,ort,workr,g,error)

      if (error.ne.0) then
         message = 'Error in diagonalising dif2mat (Lagu)'
      endif

      write(2,'(2a,i3,3(a,e12.4))') clgu,': icut =',icut, &
              ', EV :',-ort(1), ', cut :', -ort(icut+1), &
              ', EV/cut :',ort(1)/ort(icut+1)
      do g = 1, icut
         ort(g) = ort(icut+1)
      enddo

      


      do g=1,gdim
         ort(g) = 2*g + alpha -1
      enddo


      do g=1,gdim-1
         workr(g,1) = -dsqrt(dble(g*(g+alpha)))
      enddo


      call DSTEQR('I',INT(gdim,4),ort,workr,trafo,INT(gdim,4),&
                 dif1mat,error)

      if (error.ne.0) then
         message = 'Error in diagonalising X-matrix (Lagu)'
      endif

      do g=1,gdim
         if(trafo(1,g) .lt. 0.d0 ) then
            do g1=1,gdim
               trafo(g1,g) = -trafo(g1,g)
            enddo
         end if
      enddo


      if(basis.eq.15) then
         do g=1,gdim
            dif2mat(g,g) = dif2mat(g,g) - 0.25d0/(ort(g)*breite)**2
         enddo
      elseif(basis.eq.17) then
         do g=1,gdim
            dif2mat(g,g) = dif2mat(g,g) + 0.75d0/(ort(g)*breite)**2
         enddo
      elseif(basis.eq.18) then
         do g=1,gdim
            dif2mat(g,g) = dif2mat(g,g) + 2.0d0/(ort(g)*breite)**2
         enddo
      endif

      if(basis.eq.15) then
         do g=1,gdim
            dif1mat(g,g) = 0.d0
            do g1=1,g-1
               dif1mat(g,g1) = 0.5d0*dsqrt(dble(g1)/dble(g))/breite
               dif1mat(g1,g) = -dif1mat(g,g1)
            enddo
         enddo
      elseif(basis.eq.16) then
         do g=1,gdim
            dif1mat(g,g) = 0.d0
            do g1=1,g-1
              dif1mat(g,g1) = 0.5d0*dsqrt(dble(g1*(g1+1)) &
                              /dble(g*(g+1)))  &
                              /breite
              dif1mat(g1,g) = -dif1mat(g,g1)
            enddo
         enddo
      elseif(basis.eq.17) then
         do g=1,gdim
            dif1mat(g,g) = 0.d0
            do g1=1,g-1
               dif1mat(g,g1) = 0.5d0*dsqrt(dble(g1*(g1+1)*(g1+2))/ &
                                      dble(g*(g+1)*(g+2)))/breite
               dif1mat(g1,g) = -dif1mat(g,g1)
            enddo
         enddo
      elseif(basis.eq.18) then
         do g=1,gdim
            dif1mat(g,g) = 0.d0
            do g1=1,g-1
              dif1mat(g,g1) =0.5d0*dsqrt((dble(g1*(g1+1)) &
                            /dble(g*(g+1))) &
                            * (dble((g1+2)*(g1+3))/ &
                            dble((g+2)*(g+3))))/breite
               dif1mat(g1,g) = -dif1mat(g,g1)
            enddo
         enddo
      endif


      if(typ.gt.0) then
         if(typ.eq.1) then
            breite = (xf-xi)/(ort(gdim)-ort(1))
            x0 = xi - ort(1)*breite
         elseif(typ.eq.2) then
            breite = (xf-x0)/ort(gdim)
         endif
         do g=1,gdim
            do g1=1,gdim
               dif1mat(g,g1) = dif1mat(g,g1)/breite
               dif2mat(g,g1) = dif2mat(g,g1)/breite**2
            enddo
         enddo
      endif

      do g=1,gdim
         ort(g) = ort(g)*breite + x0
      enddo

      xi = ort(1)
      xf = ort(gdim)

      return
      end subroutine initlagu

!> Generate gaussian in DVR.
      subroutine gengauss(psi,gdim,dim,weight,trafo,ort,basis,rpbaspar,&
                 ipbaspar,ipbaspar1,rsbaspar,isbaspar,ggdim)

      implicit none

      integer ione 
      parameter (ione=1)

      integer*8 gdim,ggdim,dim,dim1,ierr
      integer e,g,basis,next,ipbaspar(*),blz,ibleg, &
              ipbaspar1(*),typ,power,isbaspar(*)
      real*8  trafo(ggdim,gdim),ort(gdim),weight(gdim), &
              gxeq,gmom,norm,homass,hofreq,hoxeq,ortg,shift, &
              rpbaspar(*),rsbaspar(*)
      complex*16 psi(gdim,dim),gwidth

      if (basis .eq. 1 .or. basis .eq. 11) then
         hoxeq  = rpbaspar(1)
         hofreq = rpbaspar(2)
         homass = rpbaspar(3)
      else if (basis .eq. 2 .or. basis .eq. 12) then
         blz=ipbaspar(1)
         ibleg=ipbaspar(2)
      else if (basis .eq. 7) then
         blz=ipbaspar1(3)
         ibleg=ipbaspar(1)
      else if (basis .eq. 9) then
         blz=ipbaspar(5)
         ibleg=ipbaspar(1)
      endif

      call dvrweights(trafo,ort,gdim,ggdim,weight,basis, &
                      rpbaspar,ipbaspar,ipbaspar1,.false.,.false.)

      typ=isbaspar(1)

      gxeq   = rsbaspar(1)
      gmom   = rsbaspar(2)
      gwidth = dcmplx(rsbaspar(3),rsbaspar(4))
      shift  = 0.d0 
      if(isbaspar(5).eq.1) shift = ort(2) + ort(gdim) - 2.d0*ort(1)

      do g=1,gdim
         ortg = ort(g)-gxeq
         if( isbaspar(5) .eq. 1 ) then
            if(abs(ortg) .gt. abs(ortg-shift)) ortg = ortg-shift
            if(abs(ortg) .gt. abs(ortg+shift)) ortg = ortg+shift
            if(abs(ortg) .gt. abs(ortg-shift)) ortg = ortg-shift
         endif

         psi(g,1) = weight(g)*exp(-0.25d0*(ortg*gwidth)**2)

         if (typ .eq. 1) then
            psi(g,1)=ortg*psi(g,1)
         endif
      enddo

      call normvxz(psi,norm,gdim)
      call xvixdzo(norm,psi,gdim)

      next=2
      dim1=min(dim,gdim)
      power=1
      if (typ .gt. 0) power=2
      call genhigher(psi,gdim,dim1,next,ort,basis,ipbaspar,power, &
                      gxeq,shift)
      call schmidtortho(psi,gdim,dim1,ierr)
     
      if( ierr .gt. 0 ) then
         next = ierr+1
         do g=1,gdim
            psi(g,ierr)=(ort(g)-gxeq)**typ
         enddo
         call normvxz(psi(1,ierr),norm,gdim)
         call xvixdzo(norm,psi(1,ierr),gdim)
         call genhigher(psi,gdim,dim1,next,ort,basis,ipbaspar, &
                        power,gxeq,shift)
         call schmidtortho(psi,gdim,dim1,ierr)
         if( ierr .gt. 0 ) then
            write(6,'(/a,/a,i4,a,/a,i4,a,i4,a,i4)')  &
          '############# ERROR in gengauss (genphi1.F) #############',&
                   'Cannot Schmidt-orthogonalize the', &
                    ierr,'-th spf (build).', &
                   ' basis =',basis,',  gdim =', gdim,', dim =',dim 
 
            write(2,'(/a,/a,i4,a,/a,i4,a,i4,a,i4)') & 
          '############# ERROR in gengauss (genphi1.F) #############', &
                   'Cannot Schmidt-orthogonalize the', &
                    ierr,'-th spf (build).', &
                   ' basis =',basis,',  gdim =', gdim,', dim =',dim 
            stop 1
         end if
      end if



      do e=1,dim
         do g=1,gdim
            psi(g,e)=psi(g,e) &
                 *exp((0,1)*gmom*(ort(g)-gxeq))
         enddo
      enddo
      return
      end subroutine gengauss

!> Generate mode eigenfunctions.
      subroutine genhigher (psi,gdim,dim,next,ort,basis,ipbaspar, &
                            power,gxeq,shift)

      implicit none

      integer*8 dim,gdim 
      integer next,e,g,e1,basis,ibleg,ipbaspar(*),power
      complex*16 psi(gdim,dim),s
      real*8 ort(gdim),norm,x,gxeq,shift

      if (basis .eq. 2  .or. basis .eq. 12) then
         ibleg=ipbaspar(2)
         elseif(basis .eq. 7 .or. basis .eq. 9) then
         ibleg=ipbaspar(1)
      endif

      do e=next,dim
         if((basis.eq.2 .or. basis.eq.7 .or. basis.eq.9 &
              .or. basis.eq.12)      .and. ibleg.eq.0 ) then
            do g=1,gdim
               psi(g,e)=cos(ort(g))*psi(g,e-1)
            enddo
         else if((basis.eq.2 .or. basis.eq.7 .or. basis.eq.9 &
                  .or. basis.eq.12)      .and. ibleg.gt.0 ) then
            do g=1,gdim
               psi(g,e)=(cos(ort(g))**2)*psi(g,e-1)
            enddo
         else 
            do g=1,gdim
               x=ort(g)-gxeq
               if(shift .ne. 0.d0 ) then
                  if(abs(x) .gt. abs(x-shift)) x = x-shift
                  if(abs(x) .gt. abs(x+shift)) x = x+shift
                  if(abs(x) .gt. abs(x-shift)) x = x-shift
               endif
               x=x**power
               psi(g,e)=x*psi(g,e-1)
            enddo
         end if
         do e1=1,e-1
            call vvaxzz(psi(1,e1),psi(1,e),s,gdim)
            call xvxxzzs(s,psi(1,e1),psi(1,e),gdim)
         enddo
         call normvxz(psi(1,e),norm,gdim)
         if( norm .lt. 1.d-12 ) norm = 1.d20
         call xvixdzo(norm,psi(1,e),gdim)
      enddo

      return
      end subroutine genhigher

!> Position grid of an FFT collocation.
      subroutine getpos(xi,xf,gdim,pos)
      use Global_Parameters, ONLY : PI
       implicit none

       integer*8,intent(IN)              :: gdim
       double precision,intent(IN)     :: xi,xf
       double precision,intent(OUT)    :: pos(gdim)

       double precision                :: dx
       integer                         :: i

       dx             =     (xf-xi)/(1.d0*gdim)
       do i=1,gdim
         pos(i) = xi+(i-1)*dx
       enddo
       end subroutine getpos

!> Momentum grid of an FFT collocation.
      subroutine getmom(xi,xf,gdim,mom)
      use Global_Parameters, ONLY : PI
       implicit none

       integer*8,intent(IN)              :: gdim
       double precision,intent(IN)     :: xi,xf
       double precision,intent(OUT)    :: mom(gdim)

       double precision                :: dp
       integer                         :: i

       dp             =     2.d0*pi/(xf-xi)
       do i=1,gdim
         mom(i) = (i-1)*dp
       enddo

       do i=gdim/2+2,gdim !put momenta in FFT
         mom(i)=1.d0*(i-gdim-1)*dp
       end do

       end subroutine getmom

! The following subroutine calculates the number of grid for each processor in MPI
! This subroutine is also found in Module_Orbital_Allocatables.f90
! It is repeated here to avoid circular dependence
      Subroutine Calculate_LocalPsiDim_MPI(NPROC,MYID,LocalSize)
      INTEGER    :: NPROC , iID, DIV, MYID, DomainSize
      INTEGER*8  :: LocalSize

          If (DIM_MCTDH.eq.2) THEN
              DomainSize = NDVR_X
              DIV = NDVR_Y/NPROC
          ElseIf (DIM_MCTDH.eq.3) THEN
              DomainSize = NDVR_X*NDVR_Y
              DIV = NDVR_Z/NPROC
          EndIf

          If (NPROC.eq.1) Then
             LocalSize = NDVR_X*NDVR_Y*NDVR_Z
          Else
          !MYID starts from 0
          If (MYID.ne.NPROC-1) Then
              LocalSize = DomainSize*DIV
          ElseIf (MYID.eq.NPROC-1) Then
              LocalSize = DomainSize*(NDVR_X*NDVR_Y*NDVR_Z/DomainSize-DIV*(NPROC-1))
          EndIF
      EndIF
      END Subroutine

! This is a modified version of get_ijk_from_m from Auxiliary_Routines Module
subroutine get_ijk_from_m_MPI(m,gdimx,gdimy,i,j,k,myid,nproc)
implicit none
integer*8,intent(IN)      :: m,gdimx,gdimy
integer*8,intent(OUT)     :: i,j,k
integer*8               :: n,localsize
integer                   :: myid,nproc

  call Calculate_LocalPsiDim_MPI(NPROC,0,LocalSize)

  n = MYID*LocalSize+m
  k = (n-1)/(gdimx*gdimy) + 1
  n =  n-(k-1)*gdimx*gdimy
  j = (n-1)/gdimx + 1
  n =  n-(j-1)*gdimx
  i =  n

end subroutine

        END MODULE DVR_Parameters



