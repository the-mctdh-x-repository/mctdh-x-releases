!>GPLv3
!    MCTDH-X: the multiconfigurational time-dependent Hartree for 
!    indistinguishable particles software
!
!    Copyright (C) 2021  A. U. J. Lode, M. C. Tsatsos, 
!                        E. Fasshauer, S. E. Weiner, 
!                        R. Lin, L. Papariello, P. Molignini, 
!                        C. Lévêque, M. Büttner , J. Xiang, S. Dutta
!
!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <https://www.gnu.org/licenses/>.
!
!
!
!
!<GPLv3










!> Interaction_Parameters contains input variables related to the
!> interparticle interaction and DVR and organizes the necessary allocatable
!> arrays.
!> \todo make the distribution of variables between this module and the DVR_Parameters module meaningful: DVR_X, DVR_Y, etc must be moved.
       MODULE Interaction_Parameters

       use Global_Parameters
       use Orbital_Parallelization_Parameters,ONLY: &
              LocalPsiDim,LocalPsiDim_Trans

       IMPLICIT NONE
       SAVE 

       INTEGER                   :: DVR_X!< Specify the DVR
!! for the X direction. 1 means HO-DVR, 3 sine-DVR, 4 is FFT
!! collocation, 5 is exponential DVR, 6 means MCTDBH. 
       INTEGER                   ::DVR_Y!< Specify the DVR
!! for the X direction. 1 means HO-DVR, 3 sine-DVR, 4 is FFT
!! collocation, 5 is exponential DVR, 6 means MCTDBH. 
       INTEGER                   :: DVR_Z!< Specify the DVR
!! for the X direction. 1 means HO-DVR, 3 sine-DVR, 4 is FFT
!! collocation, 5 is exponential DVR, 6 means MCTDBH. 
       REAL*8                    ::  X_initial!< Starting point of the
!! DVR in X direction.
       REAL*8                    ::  X_final !< Stopping point of the
!! DVR in X direction.
       REAL*8                    ::  Y_initial!< Starting point of the
!! DVR in Y direction.
       REAL*8                    ::  Y_final!< Stopping point of the
!! DVR in Y direction.
       REAL*8                    ::  Z_initial!< Starting point of the
!! DVR in Z direction.
       REAL*8                    ::  Z_final!< Stopping point of the
!! DVR in Z direction.
       REAL*8, ALLOCATABLE       :: WOp_X(:,:)!< DVR representation of X
!! part of the interaction in case of a factorizable interaction.
       REAL*8, ALLOCATABLE       :: WOp_Y(:,:)!< DVR representation of Y
!! part of the interaction in case of a factorizable interaction.
       REAL*8, ALLOCATABLE       :: WOp_Z(:,:)!< DVR representation of Z
!! part of the interaction in case of a factorizable interaction.
       Complex*16, ALLOCATABLE   :: W2xx(:)!< Array for interparticle
!! distance dependent interaction.
       INTEGER, PUBLIC           :: NtVec !<Size of the full interaction
!! array in the case that it's needed.
       Complex*16, ALLOCATABLE   :: Wxx(:,:) !< Full interparticle
!!interaction.
       Complex*16, ALLOCATABLE   :: W3xx(:) !< Interparticle interaction
!! for IMEST.
       Complex*16, ALLOCATABLE   :: W3xxFFT(:)  !< Interparticle interaction
!! for IMEST (this one stores the "f_sl").
       Complex*16, ALLOCATABLE   :: W3xxFFT_trans(:)  !< Interparticle interaction
!! for IMEST (this one stores the "f_sl" in transposed order for MPI parallel FFTs).

       REAL*8                    ::  mass !< Mass of the particles
!! considered.

!! for interpolation between different grids    
       REAL*8                    ::  Xi_interpol 
       REAL*8                    ::  Xf_interpol
       REAL*8                    ::  Yi_interpol
       REAL*8                    ::  Yf_interpol
       REAL*8                    ::  Zi_interpol
       REAL*8                    ::  Zf_interpol

                 
       contains

!> @ingroup init
!>  Function called in the main program to initialize/allocate arrays related to the interparticle interaction.
       logical function Interaction_Parameters_init(MYID)

       integer :: ierr,NtVec,MYID

       IF(Interaction_Type == 1) THEN

         allocate(WOp_X(NDVR_X,NDVR_X),stat=ierr)

         if(ierr /= 0) then
            write(*,*)"allocation error for WOp_X"
            Interaction_Parameters_init=.false.
         else
           if(ierr == 0)write(*,*)"allocation ok for WOp_X"
           Interaction_Parameters_init=.true.
         endif
 
         allocate(WOp_Y(NDVR_Y,NDVR_Y),stat=ierr)

         if(ierr /= 0) then
           write(*,*)"allocation error for WOp_Y"
           Interaction_Parameters_init=.false.
         else
           if(ierr == 0)write(*,*)"allocation ok for WOp_Y"
           Interaction_Parameters_init=.true.
         endif

         allocate(WOp_Z(NDVR_Z,NDVR_Z),stat=ierr)

         if(ierr /= 0) then
           write(*,*)"allocation error for WOp_Z"
           Interaction_Parameters_init=.false.
         else
           if(ierr == 0)write(*,*)"allocation ok for WOp_Z"
           Interaction_Parameters_init=.true.
         endif
       ENDIF

       IF(Interaction_Type == 2) THEN 

         allocate(W2xx(NDVR_X*NDVR_Y*NDVR_Z*2-1),stat=ierr)

         if(ierr /= 0) then
           write(*,*)"allocation error for W2xx"
           Interaction_Parameters_init=.false.
         else
           if(ierr == 0)write(*,*)"allocation ok for W2xx"
           Interaction_Parameters_init=.true.
         endif
       ENDIF


       NtVec=NDVR_X

       IF(Interaction_Type == 3) THEN 

         IF(DIM_MCTDH == 1)  NtVec=NDVR_X
         IF(DIM_MCTDH == 2)  NtVec=NDVR_X*NDVR_Y

         allocate(Wxx(NtVec,NtVec),stat=ierr)

         if(ierr /= 0) then
           write(*,*)"allocation error for Wxx"
           Interaction_Parameters_init=.false.
         else
           if(ierr == 0)write(*,*)"allocation ok for Wxx"
           Interaction_Parameters_init=.true.
         endif
       ENDIF

       IF ((Interaction_Type == 4).or.(Interaction_Type == 5) &
           .or.(Interaction_Type == 7)) THEN

          
          IF (MPI_ORBS.eqv..FALSE.) THEN
            allocate(W3xx(NDVR_X*NDVR_Y*NDVR_Z),stat=ierr)
            allocate(W3xxFFT(NDVR_X*NDVR_Y*NDVR_Z),stat=ierr)

            if(ierr /= 0) then
              write(*,*)"allocation error for W3xx"
              Interaction_Parameters_init=.false.
            else
              if(ierr == 0) write(*,*)"allocation ok for W3xx"
              Interaction_Parameters_init=.true.
            endif
          ELSEIF (MPI_ORBS.eqv..TRUE.) THEN
              Interaction_Parameters_init=.true.
          ENDIF
        ENDIF
        end function

        END MODULE Interaction_Parameters

