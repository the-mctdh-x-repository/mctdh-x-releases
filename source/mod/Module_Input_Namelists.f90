!>GPLv3
!    MCTDH-X: the multiconfigurational time-dependent Hartree for 
!    indistinguishable particles software
!
!    Copyright (C) 2021  A. U. J. Lode, M. C. Tsatsos, 
!                        E. Fasshauer, S. E. Weiner, 
!                        R. Lin, L. Papariello, P. Molignini, 
!                        C. Lévêque, M. Büttner , J. Xiang, S. Dutta
!
!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <https://www.gnu.org/licenses/>.
!
!
!
!
!<GPLv3










MODULE Input_Namelists

USE Global_Parameters
use Coefficients_Parameters
use Orbital_Parallelization_Parameters
use Coefficients_Parallelization_Parameters
use Interaction_Parameters
use DVR_Parameters
use Matrix_Elements
USE CI_Production_Parameters 

IMPLICIT NONE

NAMELIST /System_Parameters/ &
            Job_type,& 
            Morb,  & 
            Npar, &
            Job_Prefactor,&
            xlambda_0,&
            mass,&
            GUESS,&                            
            save_input,&
            Act_Operator_Init,&
            NProjections,&
            Diagonalize_OneBodyh,&
            Binary_Start_Time,&
            Restart_State,&
            Restart_Orbital_FileName ,&
            Restart_Coefficients_FileName ,&
            TNT_data, &           
            NProjections,&
            NonInteracting,&
            Custom_Orbital_Initialization,&
            Which_Custom_Orbital_Initialization,&
            Custom_Orbital_Initial_Parameters,&
!!! Multicomponent systems
            Multi_Level,&
            Conical_Intersection,&
            Nlevel,&
            InterLevel_InterParticle,&
            Spinor,&
            SpinOrbit,& 
            Rashba_Prefactor,& 
            Dresselhaus_Prefactor,&
            SpinOrbit_Prefactor,& 
            xlambda1,& 
            xlambda2,& 
            xlambda12,&
            xlambda3,& 
            Lambda1,&
            Lambda2,&
            Bose_Hubbard,&
            Periodic_BH,&
            BH_J,&
            BH_U,&
            Calculate_Rho2,&
!!! Coupling to a cavity field
            Cavity_BEC,& 
            NCavity_Modes,&
            Cavity_PumpRate,&
            Cavity_LossRate,& 
            Cavity_Detuning,& 
            Cavity_K0,& 
            CavityAtom_Coupling,&
            Pump_Imbalance,&
            X_Cavity_Pump_Waist,&
            Cavity_Mode_Waist,&
            Pump_Switch,&
            RampupTime,&
            RampdownTime,&
            PlateauTime,&
            Pump_Oscillate,& 
            Pump_Amplitude,& 
            Pump_Period,&
            Pump_Switch_Mode,&
            Pump_SwitchValue,&
            Pump_SwitchTime,& 
            Custom_Cavity,&
            Which_Cavity,&
            Transverse_Pump,&
            Cavity_Multilevel_U,& 
            Cavity_AtomPotential_Detuning,&
            Detuning_Switch,&
            Detuning_Switch_Mode,&
            Detuning_SwitchValue,&
            Detuning_SwitchTime,&
            Cavity_BEC_Nlevel_model,&
            Cavity_Initial_Guess,&
            Cavity_Initial_Guess_Value,&
            Cavity_Initial_Guess_Time,&
            Cavity_Extra_Parameters,&
!!!!! OCT stuff
            DO_OCT,&
            OCT_Restart,&
            OCT_NC,&
            OCT_Alpha,& 
            OCT_NGoals,&
            OCT_Beta,&
            OCT_TimeSteps,&
            OCT_Randomize,&
            OCT_Sampling,&
            OCT_NRandomize,&
            OCT_Bounded,&
            OCT_Method,&
            OCT_OptOmega,&
            OCT_UpperBounds,&
            OCT_LowerBounds,&
            OCT_Func_Tolerance,& 
            OCT_CRAB_Tolerance,& 
            OCT_Bounded_Bandwidth,& 
            OCT_Bandwidth_LowerBound,&
            OCT_Bandwidth_UpperBound,&
            dCRAB,&
            DO_Fejer,&
!!!  Artificial gauge fields and angular momentum            
            Fixed_Lz, &
            OrbLz, &
            Vortex_Imprint, &
            Vortex_Seeding, &
            Profile,&
            magnetic_strength, &
            charge, &
            SyntheticMagnetic, &
            Magnetic_field_type, &
            a_vec, &
            b_vec 
!

NAMELIST /DVR/&           
            DIM_MCTDH,&   
            NDVR_X,&
            NDVR_Y,& 
            NDVR_Z,&      
            DVR_X,&
            DVR_Y,&
            DVR_Z,&
            X_initial,&
            X_final,&
            Y_initial,&
            Y_final,&
            Z_initial,&
            Z_final



NAMELIST /Integration/&
            Time_Begin,            &
            Time_Final,            &
            Time_MAX,              &
            Output_TimeStep,       &
            Output_Coefficients,   &
            Integration_StepSize,  &
            Error_Tolerance,       &
            Minimal_Krylov,        &
            Maximal_Krylov,        &
            Minimal_Occupation,    &
            Orbital_Integrator,            &
            Orbital_Integrator_Order,      &
            Orbital_Integrator_MaximalStep,&
            Cavity_Integrator_MinimalStep, &
            Cav_Integrator_Loop_Max, &
            MPI_ORBS,&
            Write_ASCII, &
!            Write_FT, &
            Error_Rescale_TD,  &
            Error_Rescale_Cavity,  &
            Cavity_Error_Max, &
            LZ,    &
            Magnetic_FFT, &
            OMEGAZ, &
            STATE, &
            Coefficients_Integrator,        &
            NProc_Max_CI,&
            BlockSize, &
            RLX_Emin, &
            RLX_Emax, &
            Olsen, &
            CI_V3,                          &
            NPROC_CI, &
            NPROC_OP1B, & 
            NPROC_OP2B                  



NAMELIST /Potential/ &
            whichpot,&
            parameter1,&
            parameter2,&
            parameter3,&
            parameter4,&  
            parameter5,&
            parameter6,&
            parameter7,&  
            parameter8,&
            parameter9,&
            parameter10,&  
            parameter11,&
            parameter12,&
            parameter13,&
            parameter14,&
            parameter15,&
            parameter16,&
            parameter17,&
            parameter18,&
            parameter19,&
            parameter20,&
            parameter21,&
            parameter22,&
            parameter23,&
            parameter24,&
            parameter25,&
            parameter26,&
            parameter27,&
            parameter28,&
            parameter29,&
            parameter30 





NAMELIST /Interaction/ Which_Interaction,&
           Interaction_parameter1, &
           Interaction_parameter2, &
           Interaction_parameter3, &
           Interaction_parameter4, &
           Interaction_parameter5, &
           Interaction_parameter6, &
           Interaction_parameter7, &
           Interaction_parameter8, &
           Interaction_parameter9, &
           Interaction_parameter10,&
           Interaction_Width,&
           Interaction_Type 

END MODULE Input_Namelists
