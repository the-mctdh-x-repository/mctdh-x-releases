!>GPLv3
!    MCTDH-X: the multiconfigurational time-dependent Hartree for 
!    indistinguishable particles software
!
!    Copyright (C) 2021  A. U. J. Lode, M. C. Tsatsos, 
!                        E. Fasshauer, S. E. Weiner, 
!                        R. Lin, L. Papariello, P. Molignini, 
!                        C. Lévêque, M. Büttner , J. Xiang, S. Dutta
!
!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <https://www.gnu.org/licenses/>.
!
!
!
!
!<GPLv3










!> Coefficient Parameters contains the numbers necessary for dealing
!! with the coefficients (their number Nconf, Binomial coefficients, and
!! the particle number).
         MODULE Coefficients_Parameters

         use function_library
         USE Global_Parameters

         IMPLICIT NONE
         SAVE
         INTEGER, PUBLIC  :: Npar !< Number of particles.
         INTEGER, PUBLIC  :: Nconf !< Number of
!! coefficients/configurations.
         INTEGER, PUBLIC  :: Nconf_Bosons !< Number of configurations for the 
!! bosonic mapping equivalent of the considered fermionic system.
         INTEGER,  ALLOCATABLE :: MatrixOfBinomialCoefficients(:,:) !<
!! Binomial coefficients array.

         contains

!>  Function initializes binomial coefficients arrays for the Coefficients
         logical function Coefficients_Parameters_init(Job_Type)

         character*3 :: Job_Type
!         IMPLICIT NONE
         
         integer :: ierr,I,J

         IF ((Job_Type.eq.'BOS').or.&
           (Job_Type.eq.'FCI'))  THEN
            I=Npar+Morb-1
            J=Morb-1
            Nconf=NINT(BinomialCoefficient(Npar+Morb-1,Npar))
         ENDIF


         IF (Job_Type.eq.'FER') THEN

            I=Npar+Morb-1
            J=Morb-1
            Nconf=NINT(BinomialCoefficient(Morb,Npar))
            Nconf_Bosons=NINT(BinomialCoefficient(Npar+Morb-1,Npar))
         ENDIF

         Write (6,*) "Initialization::: M over N and N+M-1 over N",&
                      BinomialCoefficient(Morb,Npar),&
                      BinomialCoefficient(Npar+Morb-1,Npar)   

         allocate(MatrixOfBinomialCoefficients(0:I,0:J),stat=ierr)

         if(ierr /= 0) then
            write(*,*)&
            "allocation error for MatrixOfBinomialCoefficients"
            write(6,*) ierr
            Coefficients_Parameters_init=.false.
         else
            Coefficients_Parameters_init=.true.
         endif

         end function


!> @ingroup init
!> Initialize variables and arrays needed in dealing with the
!! coefficients. Namely the number of configurations Nconf, array of
!! Binomial coefficients MatrixOfBinomialCoefficients, the arrays 
!! TERM_INDEX_1B and TERM_REQ_1B as well as TERM_INDEX_2B and
!! TERM_REQ_2B for the compact storage of the matrix elements of 
!! the reduced one and two-body densities, respectively.
      subroutine Initialize_Coefficient_Parameters(MYID)

      use Global_Parameters
      USE Function_Library
 
      implicit NONE
      INTEGER ::  I,J,L,cK,cS,cQ,cL,i2,P,cI,cJ
      INTEGER*8 ::  KQ,SL,S,Q,Dummy,KSLQ,K
      INTEGER :: MYID

      I=Npar+Morb-1
      J=Morb
!      Nconf=NINT(BinomialCoefficient(I,Npar))
      IF(MYID.eq.0) THEN
       IF ((Job_Type.eq.'BOS').or.                  &
          (Job_Type.eq.'FCI'))  THEN                 
         write(1001,*)"Number of Bosons:",Npar,     &
         ". Number of orbitals:", Morb,             &
         "Number of configurations:",               &
         Nconf,BinomialCoefficient(I,Npar)           
      ELSE IF (JOB_TYPE.eq.'FER') THEN               
         write(1001,*)"Number of Fermions:",Npar,   &
         ". Number of orbitals:", Morb,             &
         "Number of configurations:",               &
         Nconf,BinomialCoefficient(Morb,Npar)        
      ENDIF             
      ENDIF                             
                                                     
                                                     
       Do i=0,Morb+Npar-1                            
          Do j=0,Morb-1                              
             MatrixOfBinomialCoefficients(i,j)=     &
                        NINT(BinomialCoefficient(i,j))
           EndDo
        EndDo
       end subroutine Initialize_Coefficient_Parameters


         END MODULE Coefficients_Parameters

