!>GPLv3
!    MCTDH-X: the multiconfigurational time-dependent Hartree for 
!    indistinguishable particles software
!
!    Copyright (C) 2021  A. U. J. Lode, M. C. Tsatsos, 
!                        E. Fasshauer, S. E. Weiner, 
!                        R. Lin, L. Papariello, P. Molignini, 
!                        C. Lévêque, M. Büttner , J. Xiang, S. Dutta
!
!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <https://www.gnu.org/licenses/>.
!
!
!
!
!<GPLv3










Module Coefficients_Allocatables

Complex*16, Allocatable ::  VIN(:), VIN0(:), VOUT(:), HDiagonal(:)

Contains

!> @ingroup init      
!> @brief Function to allocate everything in the Coefficients_Allocatables module
Logical Function Allocate_Coefficients()

USE Global_Parameters, ONLY: Coefficients_Integrator,BlockSize
USE Coefficients_Parameters, ONLY: Nconf


IF (Trim(Coefficients_Integrator).eq.'BDV') THEN
   ALLOCATE(VIN(NConf*BlockSize))
   ALLOCATE(VIN0(NConf*BlockSize))
   ALLOCATE(VOUT(NConf*BlockSize))
ELSE
   ALLOCATE(VIN(NConf))
   ALLOCATE(VIN0(NConf))
   ALLOCATE(VOUT(NConf))
ENDIF

IF ((Trim(Coefficients_Integrator).eq.'BDV') &
   .or.(Trim(Coefficients_Integrator).eq.'DAV')) THEN
   ALLOCATE(HDIAGONAL(Nconf))
ENDIF

Allocate_Coefficients=.TRUE.

End Function Allocate_Coefficients

End Module Coefficients_Allocatables
