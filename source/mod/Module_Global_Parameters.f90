!>GPLv3
!    MCTDH-X: the multiconfigurational time-dependent Hartree for 
!    indistinguishable particles software
!
!    Copyright (C) 2021  A. U. J. Lode, M. C. Tsatsos, 
!                        E. Fasshauer, S. E. Weiner, 
!                        R. Lin, L. Papariello, P. Molignini, 
!                        C. Lévêque, M. Büttner , J. Xiang, S. Dutta
!
!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <https://www.gnu.org/licenses/>.
!
!
!
!
!<GPLv3










!> Global_Parameters collects some numbers which are useful in almost
!> all of the program's routines. In this module, almost all variables 
!> are determined from the input file.
!> \todo Create input data type to unify reading/writing from/to files.
      MODULE Global_Parameters

         IMPLICIT NONE
         SAVE 
         INTEGER, PUBLIC     :: DIM_MCTDH=1  !< Dimensionality of the Orbitals in the treated problem.
         INTEGER, PUBLIC     :: Morb=2  !< The number of orbitals. 
         COMPLEX*16, PUBLIC  :: Job_Prefactor = dcmplx(-1.d0,0.d0) !< Prefactor selecting relaxation or (backward) propagation. Relaxation for (-1.0,0.0), propagation for (0.0,-1.0), and backward propagation for (0.0,1.0).
         character*4, public :: GUESS = 'HAND' !< Specify initial state of the calculation. 'binr' starts from binary files, 'hand' starts calling the routines Get_Initial_Coefficients and Get_Initial_Orbitals, 'data' starts from ASCII files.
         character*3, public :: Job_type = 'BOS' !< Do full configuration interaction ('FCI') or MCTDHB ('BOS') or MCTDHF ('FER') calculation.
         logical, public     :: Act_Operator_Init = .FALSE.   !< Act operator from "Get_Init_Operator_Action" after initial state was read or constructed
         logical, public     :: NonInteracting = .FALSE. !< Treat a noninteracting system (2body Hamiltonian not evaluated at all)

         logical, public     :: save_input = .FALSE. !< Specify whether the input file should be copied to another local file.
         
         logical, public     :: ORB_Binr = .FALSE. !< Restart from binary files,yes or no?
         INTEGER, PUBLIC     :: NProjections = 2   !< Number of times to apply the projection operator in the orbital equations of motion in each timestep

!!!!!!!!!!   OCT parameters
!!!!!!!!!!   OCT parameters

         logical, public     :: DO_OCT = .FALSE.   !< Do a parameter optimization with the CRAB optimal control algorithm
         logical, public     :: OCT_Restart = .FALSE.   !< Restart a previous OCT computation
         INTEGER, public     :: OCT_NC = 0  !< How many fourier components to take for CRAB
         REAL*8, DIMENSION(10) :: OCT_Alpha = 0.8d0 !< Weight of first part of cost functional.
         INTEGER, public     :: OCT_NGoals = 0 !< Number of goals for optimization
         REAL*8, DIMENSION(10) :: OCT_Beta = 0.2d0 !< Weight for the penalty for every goal in the cost functional.
         INTEGER, PUBLIC     :: OCT_TimeSteps = 10000 !< Number of time points for the representation of the control parameters.            
         Logical, PUBLIC     :: OCT_Randomize = .FALSE. !< Force randomization of frequencies after a certain amount of steps?           
         INTEGER, PUBLIC     :: OCT_NRandomize = 100 !< Number of OCT-steps after which OCT frequencies are randomized.
         REAL*8,PUBLIC       :: OCT_Func_Tolerance=0.0001 !< Error threshold for the convergence of the Optimal Control Functional
         REAL*8,PUBLIC       :: OCT_CRAB_Tolerance=0.0001 !< Error threshold for the convergence of the Controls
         REAL*8, DIMENSION(20) :: OCT_UpperBounds = 0.d0 !< Upper bounds for each of the Fourier components of the CRAB controls
         REAL*8, DIMENSION(20) :: OCT_LowerBounds = 0.d0 !< Lower bounds for each of the Fourier components of the CRAB controls 
         Character*4, PUBLIC :: OCT_Method='SIMP'  ! String selecting method to minimize CRAB functional; options 'SIMP','BOUN','POWL','GENE','STAT'
         LOGICAL, PUBLIC     :: OCT_OptOmega = .FALSE.   ! Instead of randomizing the frequencies in the CRAB cycle, consider them as variables of the functional and optimize them.
         LOGICAL, PUBLIC     :: OCT_Sampling = .FALSE.   ! Do random sampling for initial guess in optimization method.
         LOGICAL, PUBLIC     :: OCT_Bounded = .FALSE.   ! Controls are bounded. 
         LOGICAL, PUBLIC     :: OCT_Bounded_Bandwidth = .FALSE.   ! Frequencies are bounded. 
         REAL*8, PUBLIC      :: OCT_Bandwidth_LowerBound=0.d0  ! Lower Bound for frequencies 
         REAL*8, PUBLIC      :: OCT_Bandwidth_UpperBound=+10.d0  ! Upper bound for frequencies
         LOGICAL, PUBLIC     :: dCRAB = .FALSE. !< instead of updating the controls multiplicatively, do it additively (then bounds are used to constrain Gamma)
         LOGICAL, PUBLIC     :: Do_Fejer = .FALSE. !< use Fejer series instead of Fourier series for CRAB  
         LOGICAL, PUBLIC     :: SIMANN = .FALSE. !< Toggle if simulated annealing is used to minimize the cost functional
         LOGICAL, PUBLIC     :: TruncatedNewton = .FALSE. !< Toggle if truncated Newton conjugate gradient method is used to minimize cost functional
         LOGICAL, PUBLIC     :: QuasiNewton = .FALSE. !< Toggle if Quasi-Newton method is used to minimize cost functional

!!!!!!!!!!   OCT parameters
!!!!!!!!!!   OCT parameters


!!!!!!!!!!   Multi-component parameters
!!!!!!!!!!   Multi-component parameters

         logical, public     :: Multi_level = .FALSE. !< Treat problem with multi-level orbitals
         INTEGER, PUBLIC     :: NLevel = 1   !< Number of levels if Multi_level computation
         logical, public     :: InterLevel_InterParticle = .FALSE. !< Interlevel interparticle interactions in a multilevel computation
         logical, public     :: Spinor = .FALSE. !< Interlevel interparticle interaction is realized as a spin algebra? 
         logical, public     :: SpinOrbit = .FALSE. !< Toggles inclusion of spin-orbit-interaction in the Hamiltonian 
         REAL*8              :: Rashba_Prefactor !< Magnitude of Rashba-spin-orbit-interaction
         REAL*8              :: Dresselhaus_Prefactor !< Magnitude of Dresselhaus-spin-orbit-interaction
         REAL*8              :: SpinOrbit_Prefactor !< Magnitude of total spin-orbit (== Rashba + Dresselhaus) term.
         logical, public     :: Conical_Intersection = .FALSE. !< If the different internal levels of the considered atoms are coupled through a conical intersection.
         REAL*8              :: xlambda1 !< Intra-level interaction strength for multilevel computations.
         REAL*8              :: xlambda2 !< Intra-level interaction strength for multilevel computations.
         REAL*8              :: xlambda12 !< Inter-level interaction strength for multilevel computations.
         REAL*8              :: xlambda3  !< Intra-level interaction strength for multilevel computations.  
         REAL*8,DIMENSION(10) :: Lambda1 !< array of interaction strengths used for the spin-independent part of the interparticle interaction for spinors 
         REAL*8,DIMENSION(10) :: Lambda2 !<  array of interaction strengths used for the spin-dependent part of the interparticle interaction for spinors 

!!!!!!!!!!   Multi-component parameters
!!!!!!!!!!   Multi-component parameters


!!!!!!!!! Bose Hubbard Parameters
!!!!!!!!! Bose Hubbard Parameters
         logical, public     :: Bose_Hubbard =.FALSE. !< if Full CI computation is done, select to use Bose-Hubbard Hamiltonian.
         logical, public     :: Periodic_BH =.FALSE. !< Bose-Hubbard Hamiltonian boundary conditions.
         logical, public     :: Calculate_Rho2 =.TRUE. !< Toggle calculation of the two-body density in Bose-Hubbard.
         COMPLEX*16          :: BH_J = dcmplx(0.d0,0.d0) !< Bose-Hubbard Hamiltonian coupling of nearest neighbors.
         REAL*8              :: BH_U = 0.d0 !< Bose-Hubbard Hamiltonian interparticle interaction.
!!!!!!!!! Bose Hubbard Parameters
!!!!!!!!! Bose Hubbard Parameters

!!!!!!!!! Cavity-BEC Parameters
!!!!!!!!! Cavity-BEC Parameters
         Logical :: Cavity_BEC = .FALSE. !< toggle to couple the MCTDHB equations to a cavity field
         Integer :: NCavity_Modes = 1 !< Number of cavity modes
         REAL*8, DIMENSION(10)  :: Cavity_PumpRate = 0.d0 !< Rate at which the cavity modes are pumped
         REAL*8, DIMENSION(10)  :: Cavity_LossRate = 0.d0 !< Rate at which the cavity modes lose photons
         REAL*8, DIMENSION(10)  :: Cavity_Detuning = 0.d0 !< Cavity mode detuning with respect to mode pump
         REAL*8, DIMENSION(10)  :: Cavity_K0  = 0.d0 !< Cavity resonance 
         REAL*8, DIMENSION(10)  :: CavityAtom_Coupling = 0.d0 !< Coupling strength of the atoms to the cavity modes
         REAL*8, DIMENSION(10)  :: Atom_Detuning = 0.d0 !< Detuning of the atoms to the pump of the mode
         REAL*8, DIMENSION(10)  :: Pump_Imbalance = 0.d0 !< Imbalance between the two pumps of different colors
         REAL*8, DIMENSION(10)  :: X_Cavity_Pump_Waist = 0.d0 !< For 2D systems: X- Waist of pump beams.
         REAL*8, DIMENSION(10)  :: Cavity_Mode_Waist = 0.d0 !< For 2D systems: Waist of cavity modes.
         LOGICAL, DIMENSION(10) :: Pump_Switch = .FALSE.  !< Smoothly switch on cavity pump lasers.
         REAL*8, DIMENSION(10)  :: RampupTime = 0.d0 !< Rampup time for exponential ramps.
         REAL*8, DIMENSION(10)  :: RampdownTime = 0.d0 !< Rampdown time for exponential ramps.
         REAL*8, DIMENSION(10)  :: PlateauTime = 0.d0 !< Plateau time in pump powers.
         LOGICAL, DIMENSION(10) :: Pump_Oscillate = .FALSE. !< Pump powers are oscillating when the plateau is reached?
         REAL*8, DIMENSION(10)  :: Pump_Amplitude = 0.d0 !< Amplitudes of the oscillations of the pumps' powers as a fraction of Cavity_PumpRates 
         REAL*8, DIMENSION(10)  :: Pump_Period = 0.d0 !< At what periods the powers are oscillating.
         Character*20 :: Pump_Switch_Mode = 'exp' !< Specify what kind of time-dependence the pumps have.
         REAL*8,DIMENSION(10) :: Pump_SwitchValue !< Parameters to determine time-dependence of the pump
         REAL*8,DIMENSION(10) :: Pump_SwitchTime !< Parameters to determine time-dependence of the pump
         LOGICAL :: Custom_Cavity = .FALSE. !< Use "Get_Cavity_Modes.f90 routine to specify cavity and pump modes
         Character*20 :: Which_Cavity = 'SinCos' !< Specify which cavity-profiles are used if Custom_Cavity=.True.
         LOGICAL :: Detuning_Switch = .FALSE.   !< Specify if detuning of the cavity modes is time-dependent
         Character*20 :: Detuning_Switch_Mode = 'linear' !< Specify what kind of time-dependence the detunings have.
         REAL*8,DIMENSION(10) :: Detuning_SwitchValue !< Parameters to determine time-dependence of the detuning
         REAL*8,DIMENSION(10) :: Detuning_SwitchTime !< Parameters to determine time-dependence of the detuning
         LOGICAL, DIMENSION(10) :: Transverse_Pump = .FALSE. !< In a one-dimensional setup: is the pump transversal or not?
         LOGICAL :: Cavity_Initial_Guess = .False. !< Do we manually impose an initial value for the cavity field?
         COMPLEX*16 :: Cavity_Initial_Guess_Value = dcmplx(0.d0,0.d0) !< If yes, what is the value of this initial value?
         REAL*8 :: Cavity_Initial_Guess_Time = 0.d0 !< If yes, this initial value lasts for how long?
         REAL*8,Dimension(10)  :: Cavity_Extra_Parameters = 0.d0 !< Some extra parameters for the cavity-BEC module
!!!!!!!!! Cavity-BEC Parameters
!!!!!!!!! Cavity-BEC Parameters
!!!!!!!!! Cavity-BEC Parameters for Multilevel Systems
!!!!!!!!! Cavity-BEC Parameters for Multilevel Systems
         REAL*8,Dimension(10)  :: Cavity_Multilevel_U = 0.d0   !< Cavity-mediated coupling between internal components
         REAL*8,Dimension(10)  :: Cavity_AtomPotential_Detuning = 0.d0 !< Cavity-induced shift of energies of the internal states of the atoms
         Character*20 :: Cavity_BEC_Nlevel_model = 'Piazza' !< Which multi-component cavity-BEC model to use?
!!!!!!!!! Cavity-BEC Parameters for Multilevel Systems
!!!!!!!!! Cavity-BEC Parameters for Multilevel Systems
         REAL*8  :: Binary_Start_Time = 0.d0 !< Starting time of binary files.
         Integer :: Restart_State = 1 !< From which state in a Block-diagonalization run to restart 
         character*18, public ::  Restart_Orbital_FileName = ' ' !< If Guess='data', this variable contains the filename of the ASCII file with the orbitals.
         character*18, public ::  Restart_Coefficients_FileName = ' ' !< If Guess='data', this variable contains the filename of the ASCII file with the coefficients.
         LOGICAL,      public ::  TNT_data !< If True use TNT-generated data (slightly different form than usual .dat files) for the guess.
         LOGICAL,      public ::  Coefficients_Restart
         LOGICAL,      public ::  Orbital_Restart
         LOGICAL,      public ::  Diagonalize_OneBodyh = .FALSE. !< Diagonalize one-body
         LOGICAL,      public ::  Custom_Orbital_Initialization = .FALSE. !< If Guess='hand', do we use a custom orbital initialization?
         character*18, public ::  Which_Custom_Orbital_Initialization = ' ' !< If Guess='hand', which custom orbital initialization do we use? 
         REAL*8,DIMENSION(10), public :: Custom_Orbital_Initial_Parameters =  0.d0 !< Parameters for the specifying the custom initial orbitals
!! Hamiltonian for initial orbitals.
         LOGICAL,      public ::  FFT_2D !< if ==true 2D FFTs are used
!!instead of 1d FFTs
         INTEGER*8, PUBLIC :: NDVR_X = 256 !< Number of DVR functions for X dimension.
         INTEGER*8, PUBLIC :: NDVR_Y = 1   !< Number of DVR functions for Y dimension.
         INTEGER*8, PUBLIC :: NDVR_Z = 1   !< Number of DVR functions for Z dimension.

!! for interpolation of orbitals  
         INTEGER*8, PUBLIC :: NDVR_X_inter = 256 
         INTEGER*8, PUBLIC :: NDVR_Y_inter = 1   
         INTEGER*8, PUBLIC :: NDVR_Z_inter = 1  

         REAL*8, public  :: Time_Begin = 0.d0 !< Starting time in the writing of binary
!! files. 
         REAL*8, public  :: Time_Final = 10.d0 !< Stopping time of the
!! computation.
         REAL*8, public  :: Time_Max = 1.d99 !< Maximal total time (main loop in
!! the Master Process runs up to this time).
         REAL*8, public  :: Output_TimeStep = 1.d0 !< When to write data.
         REAL*8, public  :: Output_Coefficients = 1 !< How often (in units of
!! Output_TimeStep) the coefficients are written.
         REAL*8, public  :: Integration_Stepsize = 0.05d0 !< Stepsize of the 7-step
!! integration scheme.
         REAL*8, public  :: Error_Tolerance = 1.d-9 !< Error tolerance in
!! the 7-step integration scheme.
         REAL*8, public  :: Minimal_Occupation = 1.d-12 !< Minimal allowed occupation in the inversion of the density matrix.
         INTEGER, public :: Minimal_Krylov = 4 !< Minimal number of Krylov vectors in coefficients integrator.
         INTEGER, public :: Maximal_Krylov = 10 !< Maximal number of Krylov vectors in coefficients integrator.
         character*18, public :: Orbital_Integrator = 'RK' !< Orbital equations integration routine. Can be ABM,OMPABM,BS,RK or STIFF.
         character*3, public :: Coefficients_Integrator = 'MCS' !< Coefficients equations integration routine. Can be MCS,DSL or DAV.
         INTEGER, PUBLIC :: NProc_Max_CI !< If Orbital_Integrator is set to MPIABM, then this input sets the number of CI processes.
         INTEGER, public :: BlockSize = 4 !< If Block Davidson is used then this selects the number of coefficient vectors in the block.
         REAL*8, public :: RLX_Emin = -1.d90 !< If Block Davidson is used then this selects the lower energy bound for the block.
         REAL*8, public :: RLX_Emax = 1.d90 !< If Block Davidson is used then this selects the upper energy bound for the block.
         Logical, public :: Olsen = .FALSE. !< If Block Davidson is used then this selects if the Olsen correction is applied.
         INTEGER, public :: Orbital_Integrator_Order = 5  !< Orbital Integration order for ABM<=8, for BS <=16, for RK 5 or 8, and for STIFF 1 or 2
         INTEGER :: OrbitalIntegrator_Steps   !< Total number of steps needed to integrate Orbital equations.
         REAL*8  :: Orbital_Integrator_MaximalStep = 0.01d0 !< Maximal integration step allowed. 
         REAL*8  :: Cavity_Integrator_MinimalStep = 1.d-10 !< Minimal integration step allowed for cavity ABM integrator.
         INTEGER        :: Cav_Integrator_Loop_Max = 30 !< maximal number of attempted loops during cavity integrator
         LOGICAL,      public ::  MPI_ORBS = .FALSE. !< Flag to tell the program if it shall use domain decomposed orbitals.
         LOGICAL,      public ::  MPI_ORBS_MEMORY = .FALSE. !< Flag to replace the role of MPI_ORBS in analysis, where MPI_ORBS is technically set to FALSE.
         LOGICAL,public ::  Write_ASCII = .FALSE. !< Toggle output of ASCII data.
!         LOGICAL,public ::  Write_FT = .FALSE. !< Toggle output of ASCII data.
         REAL*8  ::  Error_Rescale_TD = 1.d0 !< Error rescaling for time-dependent potentials.
         REAL*8  ::  Error_Rescale_Cavity = 1.d0 !< Error rescaling for Cavity computations.
         REAL*8  :: Cavity_Error_Max = 1.d0 !< Maximal error tolerance for cavity ABM integrator, greater errors are ignored.
         LOGICAL,PUBLIC :: CI_V3=.FALSE.
         INTEGER        :: NPROC_OP1B, NPROC_CI,NPROC_OP2B
         INTEGER        :: Cav_Integrator_Loop_Count


         LOGICAL, PUBLIC :: SyntheticMagnetic = .FALSE. !< Add an artificial gauge field to the Hamiltonian?
         INTEGER, Public :: Magnetic_field_type = 1     !< Type of artificial gauge field
         REAL*8, Public  :: magnetic_strength = 0.d0    !< strength of the artificial magnetic field
         REAL*8, Public  :: charge = 1.d0               !< "charge" of the considered particles
         REAL*8, Public  :: a_vec = 0.d0                !< gauge field component in x direction
         REAL*8, Public  :: b_vec = 0.d0                !< gauge field component in y direction


         LOGICAL, PUBLIC :: LZ = .FALSE. !< Angular Momentum part in the Hamiltonian
         LOGICAL, PUBLIC :: Magnetic_FFT = .FALSE. !< Use magnetic FFTs (see  J. Phys.: Cond. Mat. 28, 285201 ) in 2D with magnetic field 2 OMEGAZ
         LOGICAL, PUBLIC :: Fixed_Lz = .FALSE. !< If the orbital angular momenta are fixed to the values specified in OrbLz
         INTEGER, DIMENSION(10) :: OrbLz !< Values, at which to fix the orbital angular momenta -- if -666 is set, the respective orbital is not projected
         LOGICAL, PUBLIC :: Vortex_Imprint !< Apply vortex profile augmented projection operator in relaxations.
         LOGICAL, PUBLIC :: Vortex_Seeding !< Multiply first orbital of initial state with vortex profile
         Character*20 :: Profile !< Select what functional dependence the profile has.
         REAL*8, PUBLIC  :: OMEGAZ = 0.d0 !< Angular frequency, i.e. prefactor
!! of the L_z operator in the Hamiltonian (ONLY 2D SUPPORTED).
         INTEGER, public :: STATE = 1   !< Which state should be computed in
!! a relaxation. 
         character*20 :: whichpot             !< Variables for Get_1bodyPotential 
         REAL*8 :: parameter1           !< Parameter 1          
         REAL*8 :: parameter2           !< Parameter 2           
         REAL*8 :: parameter3           !< Parameter 3          
         REAL*8 :: parameter4           !< Parameter 4          
         REAL*8 :: parameter5           !< Parameter 5          
         REAL*8 :: parameter6           !< Parameter 6          
         REAL*8 :: parameter7           !< Parameter 7          
         REAL*8 :: parameter8           !< Parameter 8          
         REAL*8 :: parameter9           !< Parameter 8          
         REAL*8 :: parameter10           !< Parameter 10          
         REAL*8 :: parameter11           !< Parameter 11          
         REAL*8 :: parameter12           !< Parameter 12          
         REAL*8 :: parameter13           !< Parameter 13          
         REAL*8 :: parameter14           !< Parameter 14          
         REAL*8 :: parameter15           !< Parameter 15          
         REAL*8 :: parameter16           !< Parameter 16          
         REAL*8 :: parameter17           !< Parameter 17          
         REAL*8 :: parameter18           !< Parameter 18          
         REAL*8 :: parameter19           !< Parameter 19          
         REAL*8 :: parameter20           !< Parameter 20          
         REAL*8 :: parameter21           !< Parameter 21          
         REAL*8 :: parameter22           !< Parameter 22          
         REAL*8 :: parameter23           !< Parameter 23          
         REAL*8 :: parameter24           !< Parameter 24          
         REAL*8 :: parameter25           !< Parameter 25          
         REAL*8 :: parameter26           !< Parameter 26          
         REAL*8 :: parameter27           !< Parameter 27          
         REAL*8 :: parameter28           !< Parameter 28          
         REAL*8 :: parameter29           !< Parameter 29          
         REAL*8 :: parameter30           !< Parameter 30          


         CHARACTER*20   :: Which_Interaction !< Select predefined Interaction potential. 'gauss' is a normalized Gaussian, 'HIM' is the harmonic interaction (acronym is Harmonic Interaction Model).
         REAL*8 :: Interaction_parameter1           !<Interaction Parameter 1                  
         REAL*8 :: Interaction_parameter2           !<Interaction Parameter 2         
         REAL*8 :: Interaction_parameter3           !<Interaction Parameter 3         
         REAL*8 :: Interaction_parameter4           !<Interaction Parameter 4         
         REAL*8 :: Interaction_parameter5           !<Interaction Parameter 5         
         REAL*8 :: Interaction_parameter6           !<Interaction Parameter 6         
         REAL*8 :: Interaction_parameter7           !<Interaction Parameter 7         
         REAL*8 :: Interaction_parameter8           !<Interaction Parameter 8         
         REAL*8 :: Interaction_parameter9           !<Interaction Parameter 8         
         REAL*8 :: Interaction_parameter10          !<Interaction Parameter 10       
 
 
         REAL*8                    :: xlambda_0 !< Interaction strength
         REAL*8                    :: Interaction_Width !< Interaction Width in Gaussian interaction potential
!! (input variable).
         INTEGER                   :: Interaction_Type !< Type of the interaction
!! potential. Interaction_Type=0 means delta-contact interaction, 1 means the
!! interaction potential is factorizable into the different spatial
!! dimensions, 2 means the interaction depends only on the interparticle
!! distance, 3 means the full interaction is represented on the full
!! grid, 4 is like 2 but the IMEST (see Kaspars/Axels PhD) is used
!! (should be a lot faster than Interaction_Type=2).

         Character*3,Public :: Previous_Coefficients_Integrator  !< Global variable to handle IO from Block relaxations
         INTEGER, Public :: Previous_BlockSize !< Global variable to handle IO of Block diagonalizations
         REAL*8  ::  Energy !< Energy variable used throughout whole program !TODO: shift this somewhere else
         REAL*8  :: timeCPU(10),ABM_Time(10) !< some timing arrays 
         INTEGER, PUBLIC :: CI_SCF !< Slave processes work on
!! Coefficients (CI_SCF=0) or slave processes work on Orbitals
!! (CI_SCF=1).
         INTEGER, PUBLIC :: TASK_ID !< Variable to manage which integration step is to be dealt with by the processes in the team
         COMPLEX*16, PARAMETER :: ZERO=(0.0d0,0.0d0)
         COMPLEX*16, PARAMETER :: ZONER=(1.0d0,0.0d0)
         COMPLEX*16, PARAMETER :: ZONEI=(0.0d0,1.0d0)
         REAL*8    , PARAMETER :: PI=3.141592653589793238462643d0




         character*80,dimension(10),parameter :: banner0 = (/&
      "@@@@@@@@@@    @@@@@@@  @@@@@@@  @@@@@@@   &
      &  @@@  @@@             @@@  @@@" , &
      "@@@@@@@@@@@  @@@@@@@@  @@@@@@@  @@@@@@@@  &
      &  @@@  @@@             @@@  @@@" , &
      "@@! @@! @@!  !@@         @@!    @@!  @@@  &
      &  @@!  @@@             @@!  !@@" , &
      "!@! !@! !@!  !@!         !@!    !@!  @!@  &
      &  !@!  @!@             !@!  @!!" , &
      "@!! !!@ @!@  !@!         @!!    @!@  !@!  &
      &  @!@!@!@!  @!@!@!@!@   !@@!@! " , &
      "!@!   ! !@!  !!!         !!!    !@!  !!!  &
      &  !!!@!!!!  !!!@!@!!!    @!!!  " , &
      "!!:     !!:  :!!         !!:    !!:  !!!  &
      &  !!:  !!!              !: :!! " , &
      ":!:     :!:  :!:         :!:    :!:  !:!  &
      &  :!:  !:!             :!:  !:!" , &
      ":::     ::    ::: :::     ::     :::: ::  &
      &  ::   :::              ::  :::" , &
      " :      :     :: :: :     :     :: :  :   &
      &   :   : :              :   :: " /)


        character*80,dimension(20),parameter :: banner1 = (/&
      "MCTDH-X: the multiconfigurational time-dependent Hartree for             ",&
      "indistinguishable particles software                                     ",&
      "                                                                         ",&  
      "Copyright (C) 2014-2024  Axel U. J. Lode, M. C. Tsatsos,                 ",&
      "Elke Fasshauer, Storm E. Weiner, Rui Lin, Luca Papariello,               ",& 
      "Paolo Molignini, Camille Leveque, Miriam Buettner, Jiabing Xiang,        ",& 
      "Sunayana Dutta, Daniel Ortuno, Yuliya Bilinskaya                         ",&
      "                                                                         ",&              
      "This program is free software: you can redistribute it and/or modify     ",&
      "it under the terms of the GNU General Public License as published by     ",&
      "the Free Software Foundation, either version 3 of the License, or        ",&
      "(at your option) any later version.                                      ",&
      "                                                                         ",& 
      "This program is distributed in the hope that it will be useful,          ",&
      "but WITHOUT ANY WARRANTY; without even the implied warranty of           ",&
      "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the             ",&
      "GNU General Public License for more details.                             ",&
      "                                                                         ",&
      "You should have received a copy of the GNU General Public License        ",&
      "along with this program. If not, see  <https://www.gnu.org/licenses/>.   "/)


         END MODULE Global_Parameters

