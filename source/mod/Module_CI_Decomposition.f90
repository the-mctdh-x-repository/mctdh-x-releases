!>GPLv3
!    MCTDH-X: the multiconfigurational time-dependent Hartree for 
!    indistinguishable particles software
!
!    Copyright (C) 2021  A. U. J. Lode, M. C. Tsatsos, 
!                        E. Fasshauer, S. E. Weiner, 
!                        R. Lin, L. Papariello, P. Molignini, 
!                        C. Lévêque, M. Büttner , J. Xiang, S. Dutta
!
!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <https://www.gnu.org/licenses/>.
!
!
!
!
!<GPLv3










!> Module that collects the variables necessary for the new decomposition of the CI vector
        module CI_Decomposition
         INTEGER              :: CI_EL_START, CI_EL_STOP
         INTEGER              :: OP_START, OP_STOP, NOPS
                 INTEGER              :: COORD_CI,COORD_OP
         INTEGER,ALLOCATABLE  :: CI_MAP_STARTS(:,:)
         INTEGER,ALLOCATABLE  :: OP_MAP_STARTS(:,:)
         INTEGER,ALLOCATABLE  :: CI_MAP_STOPS(:,:)
         INTEGER,ALLOCATABLE  :: OP_MAP_STOPS(:,:)

         INTEGER,ALLOCATABLE  :: CI_MAP_STARTS1B(:,:)
         INTEGER,ALLOCATABLE  :: OP_MAP_STARTS1B(:,:)
         INTEGER,ALLOCATABLE  :: CI_MAP_STOPS1B(:,:)
         INTEGER,ALLOCATABLE  :: OP_MAP_STOPS1B(:,:)

         INTEGER,ALLOCATABLE  :: QQ(:),QQQ(:) ! LENGTHS OF INDEXTYPES
         integer,ALLOCATABLE  :: indextype(:),indextype_RECV(:)
         integer :: DEBUGtype(2),DEBUGtype_RECV(2)
        end module
