!>GPLv3
!    MCTDH-X: the multiconfigurational time-dependent Hartree for 
!    indistinguishable particles software
!
!    Copyright (C) 2021  A. U. J. Lode, M. C. Tsatsos, 
!                        E. Fasshauer, S. E. Weiner, 
!                        R. Lin, L. Papariello, P. Molignini, 
!                        C. Lévêque, M. Büttner , J. Xiang, S. Dutta
!
!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <https://www.gnu.org/licenses/>.
!
!
!
!
!<GPLv3










!> This module collects several routines to convert arrays (of orbitals) from one-dimensional
!> and vice versa. This is needed for MPI communication and the integration routines of spino
Module Array_Interfaces
contains

subroutine Get_1dFrom3d_Array(One_d_Array,Three_d_Array,Block_size)

USE Global_Parameters

IMPLICIT NONE
INTEGER :: Block_size
COMPLEX*16 :: One_d_Array(Block_size)
COMPLEX*16 :: Three_d_Array(NDVR_X*NDVR_Y*NDVR_Z,Morb,Nlevel)
INTEGER :: Block_Orbs

INTEGER*8 :: NDVR
INTEGER ::K,J

NDVR=NDVR_X*NDVR_Y*NDVR_Z

IF (Mod(Block_size,NDVR*Nlevel).ne.0) then
   Write(6,*) "Block size not multiple of Orbital size"
   STOP
ELSE 
  Block_Orbs=Block_size/(NDVR*NLevel)
ENDIF

DO K=1,Block_Orbs
  DO J=1,Nlevel
    One_d_Array((K-1)*NDVR*NLevel+(J-1)*NDVR+1:(K-1)*NDVR*NLevel+(J*NDVR)) = Three_d_Array(:,K,J)
  END DO 
END DO

end subroutine Get_1dFrom3d_Array


subroutine Get_3dFrom1d_Array(One_d_Array,Three_d_Array)
USE Global_Parameters

IMPLICIT NONE
COMPLEX*16 :: One_d_Array(NDVR_X*NDVR_Y*NDVR_Z*Morb*Nlevel)
COMPLEX*16 :: Three_d_Array(NDVR_X*NDVR_Y*NDVR_Z,Morb,Nlevel)

INTEGER*8 :: NDVR
INTEGER :: K,J 

NDVR=NDVR_X*NDVR_Y*NDVR_Z

DO K=1,Morb
   DO J=1,Nlevel
      Three_d_Array(:,K,J) = One_d_Array((K-1)*NDVR*NLevel+(J-1)*NDVR+1: &
                                   (K-1)*NDVR*NLevel+(J*NDVR)) 
   END DO
END DO

end subroutine Get_3dFrom1d_Array

end module Array_Interfaces


