!>GPLv3
!    MCTDH-X: the multiconfigurational time-dependent Hartree for 
!    indistinguishable particles software
!
!    Copyright (C) 2021  A. U. J. Lode, M. C. Tsatsos, 
!                        E. Fasshauer, S. E. Weiner, 
!                        R. Lin, L. Papariello, P. Molignini, 
!                        C. Lévêque, M. Büttner , J. Xiang, S. Dutta
!
!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <https://www.gnu.org/licenses/>.
!
!
!
!
!<GPLv3










!>  CI_Production_Parameters contains the arrays necessary to manage and 
!>  form the mapping and readdressing scheme in PRA 81, 022124.

        module CI_Production_Parameters 
        LOGICAL :: CI_PRD=.TRUE.  !< Trigger for the assembly of the
!! mapping.
        LOGICAL :: CI_Production !< True if mapping is already
!! allocated and produced.
        LOGICAL :: CI_Production_1b !< True if one-body mapping is already
!! allocated and produced.
        LOGICAL :: CI_Production_2b !< True if two-body mapping is already
!! allocated and produced.
        integer :: nXdim !< Number of operators per slave process.
        integer :: nYdim !< Number of Coefficients per slave process.

!< Define basic Mapping_Type
        TYPE Mapping_Type
            Integer Ind1, Ind2
            REAL*8 Prefactor
        END TYPE Mapping_Type
        TYPE Operator_Type !< Define Operator_Type, an array of Mapping_Types of the size of the number of non-zero prefactors
          TYPE(Mapping_Type),dimension(:),allocatable :: Operator_Array
        END TYPE

        TYPE(Operator_Type),dimension(:),ALLOCATABLE :: Mapping_1body !< One-body mapping.
        TYPE(Operator_Type),dimension(:),ALLOCATABLE :: Mapping_2body !< Two-body mapping.
        end module

