!>GPLv3
!    MCTDH-X: the multiconfigurational time-dependent Hartree for 
!    indistinguishable particles software
!
!    Copyright (C) 2021  A. U. J. Lode, M. C. Tsatsos, 
!                        E. Fasshauer, S. E. Weiner, 
!                        R. Lin, L. Papariello, P. Molignini, 
!                        C. Lévêque, M. Büttner , J. Xiang, S. Dutta
!
!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <https://www.gnu.org/licenses/>.
!
!
!
!
!<GPLv3










!> Orbital_Parallelization_Parameters collects the numbers necessary to
!! organize the parallelization of the orbital equations of motion.
         MODULE Orbital_Parallelization_Parameters

         USE MPI 

         IMPLICIT NONE

         SAVE

         INTEGER, PARAMETER :: MaxProc = 50000 !< maximal number of
!! processes.
         INTEGER, DIMENSION(MaxProc) :: Proc_Job !< Number of Orbitals
!! treated by one slave process.
         INTEGER, DIMENSION(MaxProc) :: Proc_Iorb_Starts !< At which
!! orbital the slave process starts.
         INTEGER, DIMENSION(MaxProc) :: Proc_Iorb_Finish !< At which
!! orbital the slave process stops.
         INTEGER, DIMENSION(MaxProc) :: Proc_WSL_Starts !< At which
!! local interaction element the process starts.
         INTEGER, DIMENSION(MaxProc) :: Proc_WSL_Finish !< At which
!! local interaction element the process stops.
         INTEGER, DIMENSION(MaxProc) :: Orb_Block !< Blocksize of the
!! Orbitals stored on each process.
         INTEGER, DIMENSION(MaxProc) :: Orb_Displ !< Starting point of
!! the Orbitals stored on each process.
         INTEGER :: Energy_Evaluation !< Trigger whether or not energy needs to
!! be evaluated.
         INTEGER :: Orbital_ThreadNumber

         INTEGER*8, Allocatable :: LocalPsiDim(:)
         INTEGER*8, Allocatable :: Local_Offset(:)
         INTEGER*8, Allocatable :: LocalPsiDim_Trans(:)
         INTEGER*8, Allocatable :: Local_Offset_Trans(:)
   
         INTEGER :: Orbital_BinaryUnitNumber
         
         integer(kind=MPI_OFFSET_KIND),Allocatable ::  &
                                             OrbitalWrite_Offsets(:)

         Integer*8 ::                        Orbital_Blocksize
         integer(kind=MPI_OFFSET_KIND) :: OrbitalBinarySize

         INTEGER*8, Allocatable :: LocalPsiDim_Inter(:)
         INTEGER*8, Allocatable :: Local_Offset_Inter(:)
         Integer*8 ::                        Orbital_Blocksize_Inter

        
         INTEGER*8, Allocatable :: LocalPsiDim_dilated(:)
         INTEGER*8, Allocatable :: Local_Offset_dilated(:)

         END MODULE Orbital_Parallelization_Parameters


