!>GPLv3
!    MCTDH-X: the multiconfigurational time-dependent Hartree for 
!    indistinguishable particles software
!
!    Copyright (C) 2021  A. U. J. Lode, M. C. Tsatsos, 
!                        E. Fasshauer, S. E. Weiner, 
!                        R. Lin, L. Papariello, P. Molignini, 
!                        C. Lévêque, M. Büttner , J. Xiang, S. Dutta
!
!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <https://www.gnu.org/licenses/>.
!
!
!
!
!<GPLv3





MODULE Helmholtz

CONTAINS

subroutine Solve_Helmholtz(NDVR_X,NDVR_Y,NDVR_Z,Rho_1body,Cavity_K0,Ort_X,Ort_Y,Ort_Z,Time,Custom_Cavity_Profile)

USE iso_c_binding

IMPLICIT NONE

interface

subroutine CSolveHelmholtz(NDVR_X,NDVR_Y,NDVR_Z,Rho_1body,Cavity_K0,Ort_X,Ort_Y,Ort_Z,Time,Custom_Cavity_Profile) &
           bind ( C, name="CSolveHelmholtz_" )


   use iso_c_binding

   integer ( c_int ), VALUE :: NDVR_X,NDVR_Y,NDVR_Z
   real ( c_double ), VALUE :: Cavity_K0,Time
   real ( c_double ), dimension(NDVR_X) :: Ort_X
   real ( c_double ), dimension(NDVR_Y) :: Ort_Y
   real ( c_double ), dimension(NDVR_Z) :: Ort_Z
   real ( c_double ), dimension(NDVR_X*NDVR_Y*NDVR_Z) :: Rho_1body
   real ( c_double ), dimension(NDVR_X*NDVR_Y*NDVR_Z) :: Custom_Cavity_Profile

end subroutine CSolveHelmholtz 

end interface

integer ( c_int ), VALUE :: NDVR_X,NDVR_Y,NDVR_Z
real ( c_double ), VALUE :: Cavity_K0,Time
real ( c_double ), dimension(NDVR_X) :: Ort_X
real ( c_double ), dimension(NDVR_Y) :: Ort_Y
real ( c_double ), dimension(NDVR_Z) :: Ort_Z
real ( c_double ), dimension(NDVR_X*NDVR_Y*NDVR_Z) :: Rho_1body
real ( c_double ), dimension(NDVR_X*NDVR_Y*NDVR_Z) :: Custom_Cavity_Profile

INTEGER :: K

CALL CSolveHelmholtz(NDVR_X,NDVR_Y,NDVR_Z,Rho_1body,Cavity_K0,Ort_X,Ort_Y,Ort_Z,Time,Custom_Cavity_Profile)

write(1009,*) (ORT_X(k),Custom_Cavity_Profile(k),k=1,NDVR_X)
end subroutine Solve_Helmholtz

END Module Helmholtz
