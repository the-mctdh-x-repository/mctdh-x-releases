#include "CPPSolveHelmholtz.cpp"

extern"C" {
    void CSolveHelmholtz_(int NDVR_X,int NDVR_Y,int NDVR_Z, double* Rho_1body, const double Cavity_K0, double* Ort_X, double* Ort_Y, double* Ort_Z, const double Time, double* Custom_Cavity_Profile ) 
 {
  
 CPPSolveHelmholtzint(NDVR_X,NDVR_Y,NDVR_Z,Rho_1body,Cavity_K0,Ort_X,Ort_Y,Ort_Z,Time,Custom_Cavity_Profile) ; 
 
 }
}
