#include <vector>
#include <iostream>
#include <stdlib.h> // for rand
#include <math.h>
#include <time.h>

using namespace std;

void CPPSolveHelmholtzint(const int NDVR_X, const int NDVR_Y, const int NDVR_Z, double* Rho_1body, const double Cavity_K0, double* Ort_X, double* Ort_Y, double* Ort_Z, const double Time, double* Custom_Cavity_Profile ) {
for (int i = 0; i < NDVR_X*NDVR_Y*NDVR_Z; ++i) {
     Custom_Cavity_Profile[i]=cos(Cavity_K0*Ort_X[i])*cos(Cavity_K0*Ort_X[i]);//*Rho_1body[i];
//     cout << "Custom Cavity Profile  " << Ort_X[i] <<" , " << Rho_1body[i] <<" , " << i << " , " <<  Custom_Cavity_Profile[i] << std::endl;
  }
}
