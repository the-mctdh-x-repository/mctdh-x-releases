!>GPLv3
!    MCTDH-X: the multiconfigurational time-dependent Hartree for 
!    indistinguishable particles software
!
!    Copyright (C) 2021  A. U. J. Lode, M. C. Tsatsos, 
!                        E. Fasshauer, S. E. Weiner, 
!                        R. Lin, L. Papariello, P. Molignini, 
!                        C. Lévêque, M. Büttner , J. Xiang, S. Dutta
!
!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <https://www.gnu.org/licenses/>.
!
!
!
!
!<GPLv3










       SUBROUTINE Get_VortexProfile(VortexProfile)

       USE   Global_Parameters
       USE   DVR_Parameters

       IMPLICIT NONE

       COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb)  ::
     &                                           VortexProfile

       REAL*8 :: x,y,z,d,r
       COMPLEX*16 :: Z1,ZDOTC
       REAL*8, dimension(100) :: aa
       REAL*8 rx,ry,rz
c===========================================================
       INTEGER :: I,J,K,DM,l
       INTEGER :: ind,ierr
c===========================================================
       INTEGER :: NOVO,FFORM,S1,S2,S3,S4
       INTEGER :: Morb_Imprint
       REAL*8  :: x1,x2,x3,x4,y1,y2,y3,y4,z01,vcore,sv
       REAL*8  :: vcorex,vcorey,vcorez
       COMPLEX*16 :: imag

       imag=dcmplx(0.d0,1.d0)
c===========================================================
c===========================================================

! If desired, project orbitals to specified angular momentum or phase profile
      IF (Fixed_Lz.eqv..TRUE.) THEN 

          DO K=1,Morb
             IF (OrbLz(K).ne.-666) THEN

                ind=1

                DO L=1,NDVR_Z
                  Do J=1,NDVR_Y
                     Do I=1,NDVR_X
               
                        VortexProfile(ind,K)=
     .                      exp(imag
     .                         *OrbLz(K)
     .                         *atan2(Ort_Y(J),Ort_X(I)))

                        ind=ind+1

                     END DO
                  END DO
                END DO
! Do nothing...
             ELSEIF (OrbLz(K).eq.-666) THEN
                VortexProfile(:,K)=dcmplx(1.d0,0.d0)
             ENDIF

         END DO

         RETURN
         Write(6,*) "WHAT?"
         
      ENDIF



       IF (trim(Profile).eq.'tanh') THEN
           FFORM=1
       ELSEIF (trim(Profile).eq.'poly') THEN
           FFORM=2
       ELSEIF (trim(Profile).eq.'phase') THEN
           FFORM=3
       ELSEIF (trim(Profile).eq.'phase-x') THEN
           FFORM=4
       ENDIF

        z01=parameter14

        NOVO=parameter15   !choose number of vortices NOVO<=3.
        vcorex=parameter16 ! scaling of the vortex core along x
        vcorey=parameter17 ! scaling of the vortex core along y
        vcore=parameter18

        S1=parameter19  !charge of vortex 1
        x1=parameter20 !x-position of vortex 1
        y1=parameter21 !y-position of vortex 1
        S2=parameter22 !charge of vortex 2
        x2=parameter23 !x-position of vortex 2
        y2=parameter24 !y-position of vortex 2
        S3=parameter25 !charge of vortex 3
        x3=parameter26 !x-position of vortex 3
        y3=parameter27 !y-position of vortex 3
        S4=parameter28 !charge of vortex 4
        x4=parameter29 !x-position of vortex 4
        y4=parameter30 !y-position of vortex 4


      Dim_1_2_3D: SELECT CASE (DIM_MCTDH)

      CASE (1) ! 1D  ! 1D  ! 1D  ! 1D  ! 1D  ! 1D  ! 1D  ! 1D  ! 1D 

        Write(6,*) "No Vortex imprinting in relaxations in 1D yet!" 
        STOP

      CASE (2) ! 2D  2D 2D 2D 2D 2D 2D 2D 2D 2D 2D 2D 2D 2D 2D 2D 2D

        ind=1

        Do J=1,NDVR_Y
         Do I=1,NDVR_X
          rx=ort_X(I)
          ry=ort_Y(J)


        IF (FFORM.eq.1)      THEN
! TANH

         if     (NOVO.eq.0)       then
! No vortex 
             write(6,*) "Vortex imprinting with 0 vortices?"
             STOP
         elseif (NOVO.eq.1)       then
! One vortex 
              VortexProfile(ind,:)=
     &     tanh(sqrt(((rx-x1)/vcorex)**2+((ry-y1)/vcorey)**2)**S1)*
     &     ((rx-x1)/vcorex+imag*((ry-y1)/vcorey))/
     &     (sqrt(((rx-x1)/vcorex)**2+((ry-y1)/vcorey)**2+0.0001d0))**S1
         elseif (NOVO.eq.2)   then
! two vortices
              VortexProfile(ind,1)=
     &     tanh(sqrt(((rx-x1)/vcorex)**2+((ry-y1)/vcorey)**2)**S1)*
     &     ((rx-x1)/vcorex+imag*((ry-y1)/vcorey))/
     &     (sqrt(((rx-x1)/vcorex)**2+((ry-y1)/vcorey)**2+0.0001d0))**S1*
     &     tanh(sqrt(((rx-x2)/vcorex)**2+((ry-y2)/vcorey)**2)**S2)*
     &     ((rx-x2)/vcorex+imag*((ry-y2)/vcorey))/
     &     (sqrt(((rx-x1)/vcorex)**2+((ry-y2)/vcorey)**2+0.0001d0))**S2
! three vortices
        elseif (NOVO.eq.3)   then
              VortexProfile(ind,:)=
     &     tanh(sqrt(((rx-x1)/vcorex)**2+((ry-y1)/vcorey)**2)**S1)*
     &     ((rx-x1)/vcorex+imag*((ry-y1)/vcorey))/
     &   (sqrt(((rx-x1)/vcorex)**2+((ry-y1)/vcorey)**2+0.000001d0))**S1*
     &     tanh(sqrt(((rx-x2)/vcorex)**2+((ry-y2)/vcorey)**2)**S2)*
     &     ((rx-x2)/vcorex+imag*((ry-y2)/vcorey))/
     &   (sqrt(((rx-x2)/vcorex)**2+((ry-y2)/vcorey)**2+0.000001d0))**S2*
     &     tanh(sqrt(((rx-x3)/vcorex)**2+((ry-y3)/vcorey)**2)**S3)*
     &     ((rx-x3)/vcorex+imag*(-1)**S3*((ry-y3)/vcorey))/
     &    (sqrt(((rx-x3)/vcorex)**2+((ry-y3)/vcorey)**2+0.000001d0))**S3
! four vortices
        elseif (NOVO.eq.4)   then
              VortexProfile(ind,:)=
     &     tanh(sqrt(((rx-x1)/vcorex)**2+((ry-y1)/vcorey)**2)**S1)*
     &     ((rx-x1)/vcorex+imag*((ry-y1)/vcorey))/
     &   (sqrt(((rx-x1)/vcorex)**2+((ry-y1)/vcorey)**2+0.000001d0))**S1*
     &     tanh(sqrt(((rx-x2)/vcorex)**2+((ry-y2)/vcorey)**2)**S2)*
     &      ((rx-x2)/vcorex+imag*((ry-y2)/vcorey))/
     &   (sqrt(((rx-x2)/vcorex)**2+((ry-y2)/vcorey)**2+0.000001d0))**S2*
     &     tanh(sqrt(((rx-x3)/vcorex)**2+((ry-y3)/vcorey)**2)**S3)*
     &     ((rx-x3)/vcorex+imag*(-1)**S3*((ry-y3)/vcorey))/
     &   (sqrt(((rx-x3)/vcorex)**2+((ry-y3)/vcorey)**2+0.000001d0))**S3*
     &     tanh(sqrt(((rx-x4)/vcorex)**2+((ry-y4)/vcorey)**2)**S4)*
     &     ((rx-x4)/vcorex+imag*(-1)**S3*((ry-y4)/vcorey))/
     &    (sqrt(((rx-x4)/vcorex)**2+((ry-y4)/vcorey)**2+0.000001d0))**S4
        endif


        ELSEIF (FFORM.eq.2)    THEN
! POLYNOMIAL
         if      (NOVO.eq.0)    then
! no vortex  
             write(6,*) "Vortex imprinting with 0 vortices?"
             STOP
         elseif  (NOVO.eq.1)    then
! one vortex
             VortexProfile(ind,:)=
     %          ((rx-x1)/vcorex+imag*((ry-y1)/vcorey))**S1/
     %         (sqrt(((rx-x1)/vcorex)**2+((ry-y1)/vcorey)**2+vcore))**S1
         elseif (NOVO.eq.2.) then
! two vortices
              VortexProfile(ind,:)=
     %          ((rx-x1)/vcorex+imag*((ry-y1)/vcorey))**S1/
     %          sqrt(((rx-x1)/vcorex)**2+((ry-y1)/vcorey)**2+vcore)*
     %          ((rx-x2)/vcorex+imag*((ry-y2)/vcorey))**S2/
     %          sqrt(((rx-x2)/vcorex)**2+((ry-y2)/vcorey)**2+vcore)
         elseif (NOVO.eq.3)  then
! three vortices
              VortexProfile(ind,:)=
     %          ((rx-x1)/vcorex+imag*((ry-y1)/vcorey))**S1/
     %          sqrt(((rx-x1)/vcorex)**2+((ry-y1)/vcorey)**2+vcore)*
     %          ((rx-x2)/vcorex+imag*((ry-y2)/vcorey))**S2/
     %          sqrt(((rx-x2)/vcorex)**2+((ry-y2)/vcorey)**2+vcore)*
     %          ((rx-x3)/vcorex+imag*((ry-y3)/vcorey))**S3/
     %          sqrt(((rx-x3)/vcorex)**2+((ry-y3)/vcorey)**2+vcore)
         elseif (NOVO.eq.4)  then
! four vortices
              VortexProfile(ind,:)=
     %          ((rx-x1)/vcorex+imag*((ry-y1)/vcorey))**S1/
     %          sqrt(((rx-x1)/vcorex)**2+((ry-y1)/vcorey)**2+vcore)*
     %          ((rx-x2)/vcorex+imag*((ry-y2)/vcorey))**S2/
     %          sqrt(((rx-x2)/vcorex)**2+((ry-y2)/vcorey)**2+vcore)*
     %          ((rx-x3)/vcorex+imag*((ry-y3)/vcorey))**S3/
     %          sqrt(((rx-x3)/vcorex)**2+((ry-y3)/vcorey)**2+vcore)*
     %          ((rx-x4)/vcorex+imag*((ry-y4)/vcorey))**S4/
     %          sqrt(((rx-x4)/vcorex)**2+((ry-y4)/vcorey)**2+vcore)
         endif

        ELSEIF (FFORM.eq.3) THEN
!Imprint just phase
         if     (NOVO.eq.0) then
             write(6,*) "Vortex imprinting with 0 vortices?"
             STOP
         elseif (NOVO.eq.1) then
               VortexProfile(ind,:)=
     %          exp(S1*dcmplx(0.d0,atan2(ry-y1,rx-x1)))
         elseif (NOVO.eq.2) then
               VortexProfile(ind,:)=
     %          exp(S1*dcmplx(0.d0,atan2(ry-y1,rx-x1)))*
     %          exp(S2*dcmplx(0.d0,atan2(ry-y2,rx-x2)))
         elseif (NOVO.eq.3) then
               VortexProfile(ind,:)=
     %          exp(S1*dcmplx(0.d0,atan2(ry-y1,rx-x1)))*
     %          exp(S2*dcmplx(0.d0,atan2(ry-y2,rx-x2)))*
     %          exp(S3*dcmplx(0.d0,atan2(ry-y3,rx-x3)))
         elseif (NOVO.eq.4) then
               VortexProfile(ind,:)=
     %          exp(S1*dcmplx(0.d0,atan2(ry-y1,rx-x1)))*
     %          exp(S2*dcmplx(0.d0,atan2(ry-y2,rx-x2)))*
     %          exp(S3*dcmplx(0.d0,atan2(ry-y3,rx-x3)))*
     %          exp(S4*dcmplx(0.d0,atan2(ry-y4,rx-x4)))
         endif
         ELSEIF (FFORM.eq.4) THEN
!Imprint just phase along x
         if     (NOVO.eq.0) then
             write(6,*) "Vortex imprinting with 0 vortices?"
             STOP
         elseif (NOVO.eq.1) then
               VortexProfile(ind,:)=
     %          tanh(4.d0*(rx-x1))
         elseif (NOVO.eq.2) then
               VortexProfile(ind,:)=
     %          exp(S1*dcmplx(0.d0,atan2(1.d0+0*ry,rx-x1)))*
     %          exp(S2*dcmplx(0.d0,atan2(2.d0+0*ry,rx-x2)))
         endif
         ENDIF ! FFORM
         ind = ind +1 
        end do
      end do

      

      CASE (3) ! 3D ! 3D ! 3D ! 3D ! 3D ! 3D ! 3D ! 3D ! 3D ! 3D ! 3D

        ind=1
        Do K=1,NDVR_Z
         Do J=1,NDVR_Y
          Do I=1,NDVR_X
          rx=ort_X(I)
          ry=ort_Y(J)
          rz=ort_Z(K)


        IF (FFORM.eq.1)      THEN
! TANH

         if     (NOVO.eq.0)       then
! No vortex 
             write(6,*) "Vortex imprinting with 0 vortices?"
             STOP
               VortexProfile(ind,:)=VortexProfile(ind,:)
         elseif (NOVO.eq.1)       then
! One vortex 
! Scale the vortex core with distinct x,y core sizes
            VortexProfile(ind,:)=
     &      tanh(sqrt(((rx-x1)/vcorex)**2+((ry-y1)/vcorey)**2)**S1)*
     &      (((rx-x1)/vcorex)+imag*((ry-y1)/vcorey))/
     &      (sqrt(((rx-x1)/vcorex)**2+((ry-y1)/vcorey)**2+0.0001d0))**S1
!! z-vortex
!     &      *tanh(sqrt(((rz-z01)/vcorez)**2+((ry-y1)/vcorey)**2)**S1)*
!     &      (((rz-z01)/vcorez)+imag*((ry-y1)/vcorey))/
!     &     (sqrt(((rz-z01)/vcorez)**2+((ry-y1)/vcorey)**2+0.0001d0))**S1
        elseif (NOVO.eq.2)   then
! two vortices
            VortexProfile(ind,:)=
     &                 tanh(sqrt((rx-x1)**2+(ry-y1)**2)**S1)*
     &                 ((rx-x1)+imag*(ry-y1))/
     &                 (sqrt((rx-x1)**2+(ry-y1)**2+0.0001d0))**S1*
     &                 tanh(sqrt((rx-x2)**2+(ry-y2)**2)**S2)*
     &                 ((rx-x2)+imag*(ry-y2))/
     &                 (sqrt((rx-x2)**2+(ry-y2)**2+0.0001d0))**S2
! three vortices
        elseif (NOVO.eq.3)   then
             VortexProfile(ind,:)=
     &                 tanh(sqrt((rx-x1)**2+(ry-y1)**2)**S1)*
     &                 ((rx-x1)+imag*(ry-y1))/
     &                 (sqrt((rx-x1)**2+(ry-y1)**2+0.000001d0))**S1*
     &                 tanh(sqrt((rx-x2)**2+(ry-y2)**2)**S2)*
     &                 ((rx-x2)+imag*(ry-y2))/
     &                 (sqrt((rx-x2)**2+(ry-y2)**2+0.000001d0))**S2*
     &                 tanh(sqrt((rx-x3)**2+(ry-y3)**2)**abs(S3))*
     &                 ((rx-x3)+imag*(-1)**S3*(ry-y3))/
     &                 (sqrt((rx-x3)**2+(ry-y3)**2+0.000001d0))**abs(S3)
        endif

        ELSEIF (FFORM.eq.2)    THEN
! POLYNOMIAL
         if      (NOVO.eq.0)    then
! no vortex  
             write(6,*) "Vortex imprinting with 0 vortices?"
             STOP
         elseif  (NOVO.eq.1)    then
! one vortex 
             VortexProfile(ind,:)=
     %                   ((rx-x1)+imag*(ry-y1))**S1/
     %                   (sqrt((rx-x1)**2+(ry-y1)**2+vcore))**S1
         elseif (NOVO.eq.2.) then
! two vortices
              VortexProfile(ind,:)=
     %                   ((rx-x1)+imag*(ry-y1))**S1/
     %                   sqrt((rx-x1)**2+(ry-y1)**2+vcore)*
     %                   ((rx-x2)+imag*(ry-y2))**S2/
     %                   sqrt((rx-x2)**2+(ry-y2)**2+vcore)
         elseif (NOVO.eq.3)  then
! three vortices
              VortexProfile(ind,:)=
     %                   ((rx-x1)+imag*(ry-y1))**S1/
     %                   sqrt((rx-x1)**2+(ry-y1)**2+vcore)*
     %                   ((rx-x2)+imag*(ry-y2))**S2/
     %                   sqrt((rx-x2)**2+(ry-y2)**2+vcore)*
     %                   ((rx-x3)+imag*(ry-y3))**S3/
     %                   sqrt((rx-x3)**2+(ry-y3)**2+vcore)
          endif
       ELSE            !VortexProfile(ind,1)=VortexProfile(ind,1)
      ENDIF
              

               ind=ind+1
             Enddo
            Enddo
          Enddo
      END SELECT Dim_1_2_3D

      DO K=1,Morb
         IF (OrbLz(K).eq.-666) THEN
            VortexProfile(:,K)=dcmplx(1.d0,0.d0)
         ENDIF
      END DO

      END SUBROUTINE Get_VortexProfile
