      !> * uses the input variables "whichpot" to select a one-body potential \f$ V (\vec{r}) \f$ from the list below
      !> * uses the input parameters parameter1, parameter2,... to specify the details of the potential
      !> **List of implemented potentials (selected via the whichpot input)**
      !>  * HO1D: One dimensional harmonic potential with frequency \f$ p_1\f$ : \f$ V (x) = \frac{1}{2} p_1 x^2  \f$ \image html HO1D.png
      !>  * HO2D: Two dimensional harmonic potential with frequency \f$ p_1, p_2\f$ : \f$ V (x,y) = \frac{1}{2} (p_1 x^2+p_2 y^2) \f$ \image html HO2D.png
      !>  * HO3D: Three dimensional harmonic potential with frequency \f$ p_1, p_2, p_3\f$ : V (x,y,z) = \frac{1}{2} (p_1 x^2+p_2 y^2+p_3 z^2) \f$
      !>  * custom1D: Define 1 dimensional potential by yourself. (ca. line 602).
      !>  * custom2D: Define 2 dimensional potential by yourself (ca. line 1842).
      !>  * custom3D: Define 3 dimensional potential by yourself (ca. line 2869).
      !>  * MQT_ini: Rectangular 1D box with barrier width \f$ p_1 \f$ : \f$ V (x)=\left\{\begin{align*}
      !>                                          0 ,&  x .lt. (20/\sqrt{2}-p_1)) & (x .gt. 0)\\
      !>                                          150 ,& x .le. 0\\
      !>                                          0.05 ,& x .ge. (20/\sqrt{2}-p_1)
      !>                                          \end{align*}\right  \f$ \image html MQT_ini.png  
      !>  * MQT_prop: Rectangular 1D box, barrier and open space accept  \f$ V(x)= \left{ \begin{align*}
      !>                                          150 ,& x.le. 0;\\
      !>                           0 ,& 0.gl.x.lt.\frac{20}{\sqrt{2}}-p_1 ;\\ 
      !>                          0.05 ,& \frac{20}{\sqrt{2}}-p_1 .le. x<\frac{20}{\sqrt{2}}; \\
      !>                          0 ,& x .ge. \frac{20}{\sqrt{2}} \end{align*} \right \f$ \image html MQT_prop.png
      !>  * thr: parameters:
      !>           \f$   A=1-\frac{1}{4}p_1\\
      !>               B=2.25p_1-9.5;
      !>               C=28-6p_1;
      !>               D=5p_1-24; \f$
      !>          \f$V(x)=\frac{1}{2}x^2 ,& x.le.2 ;
      !>                  Ax^3+Bx^2+Cx+D ,& 2.gt.x.le.4 ;
      !>                  p_1 ,& x.lt.4 \f$ \image html thr.png
      !>  * qpl: 1D lattice with 2 frequencies/amplitudes \f$ V(x)=p_1cos(p_2 x)+p_3cos(p_4 x) \f$ \image html qpl.png
      !>  * cradle_init:  \f$ V(x)=\frac{1}{2}x^2 \f$ \image html cradle_init.png
      !>  * cradle_prop: \f$  V(x)= 7.79784(cos(\frac{x}{3.55765}))^2  ,   &t<0.0400927 or 0.0897312.le. t<0.126642 ;
      !>                           0  ,    &otherwise \f$ \image html cradle_prop.png
      !>  * cradle_imprint: \f$ V(x)=\frac{1}{2}x^2 \f$ \image html cradle_imprint.png
      !>  * TAB:??? 
      !>  * TAB_init:???
      !>  * OCT_HO1D+gauss:???
      !>  * TIQDW: \f$   V(x)=f(x)-min(f(x))\\
      !>                 f(x)=p_1x^2+p_3\exp{-2p_2 x^2} \f$ \image html TIQDW.png
      !>  * squeeze2p: parameters:
      !>                   \f$ p_1=t_1, p_2=t_{max}, t_0=\frac{1}{2}(t_{max}-t1), p_3=V_1, p_4=V_{max}, p_5,p_6 \f$
      !>              \f$  V_{max}(t)=
      !>                 V_1*\frac{t}{t_0}, & t.le. t_0\text{ and }\abs{t_0}.ge.1*10^{-10};
      !>                 p_3+(p_4-p_3)*\frac{t-t_0-t_1}{t0}, & t_{max}-t_0<t .le. t_{max}\text{ and }\abs{t_0}.ge. 1*10^{-10};
      !>                 p_3, & t_0<t.le. t_{max}-t_0\text{ or }(t.le. t_0\text{ and }\abs{t_0}< 1*10^{-10}); 
      !>                 p_4, & otherwise  \f$
      !>              \f$  V(x)=\frac{1}{2}p_5x^2+V_{max}(t)\exp{-\frac{x^2}{2p_6^2}} \f$ 
      !>
      !>  * Lesanovsky1D: parameters:
      !>                  \f$ p_1=t_1,p_2=t_{max},p_3=V_1,p_4=V_{max},t_0=\frac{1}{2}(t_{max}-t_0) \f$
      !>                  For relaxtions: $\lambda_{RF}=p_4$\\
      !>                  For porpagations:                
      !>                  \f$ V_{max}(t)=  V_1*\frac{t}{t_0}, & t.le. t_0 and \abs{t_0}.ge.1*10^{-10};
      !>                                  p_3+(p_4-p_3)*\frac{t-t_0-t_1}{t0}, & t_{max}-t_0<t .le. t_{max} and \abs{t_0} .ge. 1*10^{-10};
      !>                                  p_3, & t_0<t.le. t_{max}-t_0 or (t.le. t_0\text and \abs{t_0}< 1*10^{-10});
      !>                                  p_4, & otherwise; \f$
      !>                  \f$ \lambda_{RF}=V_{max} \f$
      !>                  \f$ \Delta=1.26*10^{10}*\frac{2\pi\hbar}{bohr} f$\
      !>                  \f$ brf=\frac{1}{2}+\frac{1}{3}\lambda_{RF} f$\
      !>                  \f$ bs=\sqrt{(0.2x)^2+1} f$\
      !>                  \f$ f(x)=\Kappa\sqrt{(bs-\Delta)^2+(\frac{brf}{2bs})^2}*\frac{1*10^{-7}*bohr}{1.37\hbar} f$\
      !>                  Potential:
      !>                  \f$ V(x)=f(x)-MIN(\abs{f(x)})&, \abs{f(x)}\leq 5000 ;
      !>                          5000 &, \abs{f(x)}>5000   \f$               
      !>  * Lesanovsky1D_Linear:???
      !>  * OCT_Lesanovsky1D: ??? 
      !>  * bonitz:  \f$ V(x) =0 ; x = 1
      !>                     =4000 ; else  \f$
      !>  * UFPRL: \f$ V (x) =0 \f$
      !>  * DeltaStatic: parameters:
      !>                 \f$ $p_1,Div=\frac{NDVR\_X}{x_{final}-x_{initial}}$  \f$
      !>                 potentials:
      !>                 \f$ V(x)=\delta(x-x_0)*p_1*Div,\text{ $x_0$ is the center in x direction} \f$
      !>  * Delta: ??????
      !>  * HO1DEff:   \f$  &Bav=590.855,\Delta=-192.3,Boo=736.8 \f$
      !>               \f$ TD\_\lambda_0=xlambda\_0*\frac{(Bav-Boo)*(Bav-Boo-Delta+dB*sin(\omega t))}{(Bav-Boo-\Delta)*(Bav-Boo+dB*sin(\omega t))}\f$
      !>               \f$ s_{00}=\left\{\begin{align*}{cc} \f$
      !>               \f$ V(x)= \frac{0.5*TD\_\lambda_0(Npar-1)*p_1+1}{15.7496^{\frac{1}{4}}} &  t\leq TimeMod\f$
      !>               \f$       \frac{0.5*TD\_\lambda_0(Npar-1)*p_1+1}{15.7496^{\frac{1}{4}}} &  t> TimeMod  \f$
      !>  * OCT_1DDW:???
      !>  * OCT_1D:???
      !>  * OCT_CavityC: \f$ V(x)=\frac{1}{2}x^2 \f$
      !>  * OCT_CavityF : \f$ V(x)=\frac{1}{2}x^2 \f$
      !>  * prether: \f$V(x)=\frac{1}{2}p_1*(x-p_2)^2 \f$ \image html prether.png
      !>  * OL_1D: \f$ V(x)=p_1*(sin(p_2*\abs{x}))^2 \f$ \image html OL_1D.png
      !>  * OL_HW_1D: \f$ V(x)=p_1*sin(p_2*x)^2 &,p_3<x<p_4 \f$ 
      !>              \f$     1000 &,otherwise \f$ \image html OL_HW_1D.png
      !>  * Cavity_OL1D: \f$ V(x)=p_1*(sin(p_2*\abs{x}))^2+\frac{1}{2}p_3*x^2 \f$ \image html Cavity_OL1D.png
      !>  * DQD: \f$ V(x)=-p_1*\exp{-p_2(x+\frac{1}{2}p_3)^2}-p_4\exp{-p_5(x-\frac{1}{2}p_3)} \f$ \image html DQD.png
      !>  * DQDLASER: \f$ V(x)=-p_1*\exp{-p_2(x+\frac{1}{2}p_3)^2}-p_4\exp{-p_5(x-\frac{1}{2}p_3)}+V_{las}(x,t)\exp{-p_9(x-p_{10})^2 \f$ \image html Tiltinit.png
      !>  * Tiltinit: \f$ V(x)=-p_1 x+p_2sin(2x)^4+\left(\frac{x-\frac{p_3\pi}{2}}{0.7}\right)^{20} \f$
      !>  * Tilt: \f$ V(x)=-p_1 x+p_2sin(2x)^4+\left(\frac{x}{2.2}\right)^{20} \f$ \image html Tilt.png
      !>  * TDHIM: \f$ V(x,t)=\frac{1}{2}(1+sin(t)cos(2t)sin(\frac{1}{2}t)sin(0.4t))x^2 \f$
      !>  * tun: \f$ V(x)=\left\{\begin{align*}{cc} 
      !>                   \frac{1}{2}x^2 ,& x\leq 2  \\
      !>                   2.2662969\exp{-2(x-2.25)^2} ,&2<x<15\\
      !>                   0,&x\ge 15
      !>                   \end{align*}\right. \f$ \image html tun.png
      !>  * shake1DchopCosSin:
      !> If \f$\frac{p_3*t}{2\pi}<p_7 and (p_3*t mod 2\pi) <\frac{3\pi}{2} \f$:
      !>    \f$\begin{align*}
      !>       &Disp=sin(p_3*t)\\
      !>       &A=(p_1+p_2*Disp)\\
      !>       &B=(p_5+p_6*Disp)\\
      !>       &V(x)=B+\frac{1}{2}A^2(x-p_4*Disp)^2
      !>       \end{align*} \f$
      !> ElseIf \f$\frac{p_3*t}{2\pi}<p_7 and (p_3*t mod $2\pi) \ge\frac{3\pi}{2} \f$:
      !>       \f$ \begin{align*}
      !>       &Disp=sin(p_8*t)\\
      !>       &A=(p_9+p_{10}*Disp)\\
      !>       &B=(p_{11}+p_{12}*Disp)\\
      !>       &V(x)=B+\frac{1}{2}A^2(x-p_{13}*Disp)^2 
      !>       \end{align*}\f$
      !> Otherwise:
      !> \f$ V(x)=p_5+\frac{1}{2}x^2 \f$
      !>  * shake1Dchop:
      !> If \textbf{"Interaction_Type"}=5 or 6: f$p_3\f$=Interaction_Parameter2
      !> If \f$\frac{p_3*t}{2\pi}<p_7 and (p_3*t mod 2\pi) <\frac{3\pi}{2} \f$:
      !>    \f$\begin{align*}
      !>       &Disp=sin(p_3*t)\\
      !>       &A=(p_1+p_2*Disp)\\
      !>       &B=(p_5+p_6*Disp)\\
      !>       &V(x)=B+\frac{1}{2}A^2(x-p_4*Disp)^2
      !>       \end{align*} \f$
      !> ElseIf \f$\frac{p_3*t}{2\pi}<p_7 and (p_3*t mod $2\pi) \ge\frac{3\pi}{2} \f$:
      !>       \f$ \begin{align*}
      !>       &Disp=-sin(p_3*t)\\
      !>       &A=(p_1+p_2*Disp)\\
      !>       &B=(p_5+p_6*Disp)\\
      !>       &V(x)=B+\frac{1}{2}A^2(x-p_4*Disp)^2 
      !>       \end{align*}\f$
      !> Otherwise:
      !> \f$V(x)=p_5+\frac{1}{2}x^2 \f$
      !>  * shake1Dtheta:
      !> If \textbf{"Interaction_Type"}=5 or 6: f$p_3\f$=Interaction_Parameter2
      !> If \f$\frac{p_3*t}{2\pi}<p_7 and (p_3*t mod 2\pi) <\frac{3\pi}{2} \f$:
      !>    \f$\begin{align*}
      !>       &Disp=1\\
      !>       &A=(p_1+p_2*Disp)\\
      !>       &B=(p_5+p_6*Disp)\\
      !>       &V(x)=B+\frac{1}{2}A^2(x-p_4*Disp)^2
      !>       \end{align*} \f$
      !>ElseIf \f$\frac{p_3*t}{2\pi}<p_7 and (p_3*t mod $2\pi) \ge\frac{3\pi}{2} \f$:
      !>       \f$ \begin{align*}
      !>       &Disp=-1\\
      !>       &A=(p_1+p_2*Disp)\\
      !>       &B=(p_5+p_6*Disp)\\
      !>       &V(x)=B+\frac{1}{2}A^2(x-p_4*Disp)^2 
      !>       \end{align*}\f$
      !> Otherwise:
      !> \f$V(x)=p_5+\frac{1}{2}x^2 \f$
      !>  * shake1D:
      !> If \textbf{"Interaction\_Type"}=5 or 6: $p_3$=Interaction\_Parameter2\\
      !> \f$ \begin{align*}
      !>    &Disp=sin(p_3*t)\\
      !>    &A=(p_1+p_2*Disp)\\
      !>    &B=(p_5+p_6*Disp)\\   
      !>    \end{align*} \f$
      !> \f$ V(x)=\left\{\begin{align*}{cc}
      !>    B+\frac{1}{2}A^2(x-p_4*Disp)^2 ,&\frac{p_3*t}{2\pi}<p_7\\
      !>    p_5+\frac{1}{2}x^2 ,&\frac{p_3*t}{2\pi}\ge p_7\\
      !>    \end{align*}\right. \f$
      !>  * shake1Dpoly:
      !> If \f$\frac{p_1*t}{2\pi}<p_{13}: \f$
      !> \f$\begin{align*}
      !>  &A=p_2+p_7*sin(p_1*t)\\
      !>    &B=p_3+p_8*sin(p_1*t)\\
      !>    &C=p_4+p_9*sin(p_1*t)\\
      !>    &D=p_5+p_{10}*sin(p_1*t)\\
      !>    &E=p_6+p_{11}*sin(p_1*t)\\
      !>    &x_0=x-p_{12}*sin(p_1*t)\\
      !>    &V(x)=A+B*x_0+C^2*x_0^2+D*x_0^3+E*x_0^4
      !>  \end{align*} \f$
      !> ElseIf \f$\frac{p_1*t}{2\pi}\ge p_{13} \f$:
      !> \f$ V(x)=p_2+p_3*x_0+p_4^2*x_0^2+p_5*x_0^3+p_6*x_0^4 \f$
      !>  * HO1D_box: f$ V(x)=\frac{1}{2}p_1^2*x^2+\frac{1}{2}p_3*tanh\left(p_4(x-\frac{1}{2}p_2)+1\right)+\frac{1}{2}p_3*tanh\left(p_4(x+\frac{1}{2}p_2)-1\right) \f$ \image html HO1D_box.png
      !>  * HO1D_shakebox:
      !> \f$ \begin{align*}
      !>    &dt=nint\left(\frac{p_7}{2\pi*p_6}\right)*2\pi p_7\\
      !>    &V_{max}=\left\{\begin{align*}{cc}
      !>    p_3+p_5*sin(p_6*t) ,& t<dt  \\
      !>    p_3 ,&t\ge dt 
      !>    \end{align*}\right.
      !>    \end{align*} \f$
      !> \f$ V(x)=\frac{1}{2}p_1^2*x^2+\frac{1}{2}V_{max}*tanh\left(p_4(x-\frac{1}{2}p_2)+1\right)+\frac{1}{2}V_{max}*tanh\left(p_4(x+\frac{1}{2}p_2)-1\right) \f$
      !>  *HO1D_TDlinear:
      !> \f$ V(x)=\left\{\begin{align*}{cc}
      !>  \frac{1}{2}\left(p_1+\frac{(p_2-p_1)t}{p_3} \right)x^2 &, t\leq p_3  \\
      !>  \frac{1}{2}p_2 x^2 &, t>p_3
      !>  \end{align*}\right. \f$
      !>  * zero: \f$ V(x)=0 \f$
      !>  * randomU: \f$ V(x)=0 \f$
      !>  * h+d: \f$ V(x)=\frac{1}{2}(x-p_1)^2+p_2\exp{-\frac{(x-p_1)^2}{2p_3^2}} \f$
      !>  * HO1D+td_gauss: 
      !> \f$ \begin{align*}
      !>    &A(t)=\left\{\begin{align*}{cc}
      !>    \frac{p_3*t}{p_4} &,t\leq p_4  \\
      !>    p_3 &,t>p_4 
      !>  \end{align*}\right.\\
      !>  &V(x)=\frac{1}{2}p_1^2(x-p_2)^2+A(t)\exp{-\frac{(x-p_5)^2}{2p_6^2}}
      !>  \end{align*} \f$
      !>  * qpl: \f$ V(x)=p_1cos(p_2 x)+p_3cos(p_4 x) \f$ \image html qpl.png
      !>  * HO1D+cos: \f$ V(x)=\frac{1}{2}p_1*x^2+p_2cos(p_3*x+p_4) \g$ \image html HO1D+cos.png
      !>  * HO2D: \f$ V(x,y)=\frac{1}{2}(p_1^2x^2+p_2^2y^2) \f$ \image html HO2D.png
      !>  * Cavity_OL2D: \f$ V(x,y)=p_1\left(sin(p_2*\abs{x})^2+sin(p_2*\abs{y})^2\right)+\frac{1}{2}p_3*x^2+\frac{1}{2}p_4*y^2 \f$ \image html Cavity_OL2D.png
      !>  * Bernhard2D: ???
      !>  * TC2D: ???
      !>  * ellipse2D: 
      !>  \f$ V(x,y)=\left\{\begin{align*}{cc}
      !>  1000 &, \sqrt{\left(\frac{x}{p_1}\right)^2+\left(\frac{y}{p_2}\right)^2}>p_3 \\
      !>  0 &,otherwise 
      !>  \end{align*}\right. \f$ \image html ellipse2D.png
      !>  * OL_HW_2D_odd:
      !> \f$ V(x,y)=\left\{\begin{align*}{cc}
      !>    p_1(sin(p_2*\abs{x})^2+sin(p_2*\abs{y})^2) &, p_3<x<p_4\text{ and }p_5<y<p_6  \\
      !>    1000 &, otherwise 
      !>  \end{align*}\right. \f$
      !>  * OL_HW_2D_even:
      !> \f$ V(x,y)=\left\{\begin{align*}{cc}
      !>    p_1(cos(p_2*\abs{x})^2+cos(p_2*\abs{y})^2) &, p_3<x<p_4\text{ and }p_5<y<p_6  \\
      !>    1000 &, otherwise 
      !>    \end{align*}\right. \f$
      !>  * spherdynam1:
      !> \f$ \begin{align*}
      !>    &r=\sqrt{x^2+y^2}\\
      !>    &r_1=\sqrt{p_7^2+p_8^2}\\
      !>    &r_2=\sqrt{p_1^2+p_2^2}\\
      !>    &V(x,y)=\left\{\begin{align*}{cc}
      !>        p_5\exp{-\frac{(r-r_1)^4}{2p_3^2}} &, r\leq r_1\text{ and } p_{10}\leq 0 \\
      !>        p_5 &, r>r_1\text{ and } p_{10}\leq 0\\
      !>        p_5\exp{-\frac{(r-r_1)^4}{2p_3^2}}+p_6\exp{-\frac{(r-r_1)^4}{2p_4^2}}*\frac{t}{p_{10}} &, r\leq r_1\text{ and } p_{10}> 0 \\
      !>        p_5+p_6\exp{-\frac{(r-r_1)^4}{2p_4^2}}*\frac{t}{p_{10}} &, r>r_1\text{ and } p_{10}> 0\\        
      !>    \end{align*}\right.
      !> \end{align*} \f$
      !>  * test2D_static: \f$ $V(x,y)=\frac{1}{2}(x^2+y^2) \f$ \image html test2D_static.png
      !>  * test2D_dynamic:
      !>  \f$  \begin{align*}
      !>    &A=0.3846153846153846153846153846\\
      !>    &V(x,y)=\frac{1}{2}x^2+\frac{1}{2}y^2+\exp{A(y+0.5)^2}
      !>    \end{align*} \f$
      !>  * TAB:
      !>  * TAB_init:
      !>  * X2D:
      !> \f$ \begin{align*}
      !> & y\leq x\leq y+1.5 \text{ and }1\leq y\leq 2\\
      !> & 5.5-y\leq x\leq 7-y \text{ and }1\leq y\leq 2\\
      !> & y\leq x\leq 7-y \text{ and }2\leq y\leq 3\\
      !> & 6-y\leq x\leq y+1 \text{ and }3\leq y\leq 4\\
      !> & y-0.5\leq x\leq y+1 \text{ and }4\leq y\leq 5\\
      !> & 6-y\leq x\leq 7.5-y \text{ and }4\leq y\leq 5\\
      !> \end{align*} \f$
      !> \f$ V(x,y,t)=\left\{\begin{align*}{cc}
      !> -t &, Condition A\text{ and }t<50  \\
      !> -50 &,  Condition A\text{ and }t\ge 50
      !> \end{align*}\right. \f$
      !>  * R2D:
      !> \f$ \begin{align*}
      !> & -2\leq x\leq 0 \text{ and }-5\leq y\leq 5\\
      !> & 0\leq x\leq 6 \text{ and }3\leq y\leq 5\\
      !> & 4\leq x\leq 6 \text{ and }-1\leq y\leq 3\\
      !> & 0\leq x\leq 4 \text{ and }-1\leq y\leq 1\\
      !> & -y-1\leq x\leq -y+1 \text{ and }-5\leq y\leq -1\\
      !> \end{align*} \f$
      !> \f$ V(x,y,t)=\left\{\begin{align*}{cc}
      !> -t &, Condition A\text{ and }t<50  \\
      !> -50 &,  Condition A\text{ and }t\ge 50
      !> \end{align*}\right. \f$
      !>  * 2Dgramp:  \f$ V (x,y)=\frac{1}{2} p_4 (x^2+y^2)+ time exp(-((x-p_1)^2 +(y-p_2)^2 )/2) ; time .le. p_3
      !>                        =\frac{1}{2} p_4 (x^2+y^2)+ p_3 exp(-((x-p_1)^2 +(y-p_2)^2 )/2) ; else \f$                       
      !>  * stir2D: \f$ x_0 = p_2 cos(p_1 time), y_0 = p_2 sin (p_1 time)                  
      !>               V (x,y) =\frac{1}{2} (x^2+y^2) + p_3 exp(-((x-x_0)^2 +(y-y_0)^2 )/p_4) \f$ 
      !>  * rot2D: \f$ x_0 = x cos(p_1 time)+y sin(p_1 time), y_0 = y cos(p_1 time)-x sin(p_1 time)
      !>                eta = p_2/(p_3 time) ; time .le. p_3      
      !>                    = p_2; time .gt.p_3 & time .le. (p_3+p_4) 
      !>                    = p_2- p_2/p_3 (time-p_3+p_4); time .ge. (p_3+p_4) & time .le. (p_4+2 p_3)  
      !>                    = 0  ; else 
      !>                V (x,y) = \frac{1}{2} ((1-eta) y_0^2  + (1+eta) x_0^2) \f$
      !>  * h2D+d: \f$ V(x,y)=\frac{1}{2}(p_1^2x^2+p_2^2y^2)+p_3\exp{-\left(\frac{x^2}{2p_4^2}+\frac{y^2}{2p_5^2}\right)} \f$ \image html h2D+d.png
      !>  * HO2Diso: \f$V(x,y,t)=\frac{1}{2}p_1(x^2+y^2) \f$ \image html HO2Diso.png
      !>  * HO2D+td_gauss: 
      !> \f$ \begin{align*}
      !>    &A(t)=\left\{\begin{align*}{cc}
      !>    \frac{p_3*t}{p_4} &,t\leq p_4  \\
      !>    p_3 &,t>p_4 
      !>    \end{align*}\right.\\
      !>    &V(x,y)=\frac{1}{2}(p_1^2(x-p_2)^2+y^2)+A(t)\exp{-\frac{(x-p_5)^2}{2p_6^2}}
      !>    \end{align*} \f$
      !>  * zero: \f$V(x)=0 \f$
      !>  * QPC2D:
      !>  \f$ \begin{align*}
      !>  &grad=\left\{\begin{align*}{cc}
      !>    p_9*\frac{18.849555909675-t}{18.849555909675}*y &, t<18.849555909675  \\
      !>    0 &, t\ge 18.849555909675 
      !> \end{align*}\right.\\
      !> &rep=\left\{\begin{align*}{cc}
      !>    p_5 &, t<18.849555909675  \\
      !>    0 &, t\ge 18.849555909675 
      !> \end{align*}\right.\\
      !> &V(x,y,t)=\left\{\begin{align*}{cc}
      !> \frac{1}{2}*(p_1^2x^2+p_2^2y^2)+\frac{1}{2}(p_3\exp{-\frac{y^2}{p_4^2}})^2x^2\\+rep*\exp{-\frac{x^2+y^2}{p_6}}-p_7*\exp{-\frac{x^2+y^2}{p_8}}+grad &,DREAL(V)\leq 800  \\
      !>    800 &,  DREAL(V)> 800
      !> \end{align*}\right.
      !> \end{align*} \f$
      !>  * trans2D:
      !> \f$ V(x,y)=\left\{\begin{align*}{cc}
      !>   \frac{1}{2}*(p_1^2x^2+p_2^2y^2)+\frac{1}{2}(p_3\exp{-\frac{y^2}{p_4^2}})^2x^2\\
      !>   p_5*\exp{-\frac{x^2+y^2}{p_6}}-p_7*\exp{-\frac{x^2+y^2}{p_8}}+p_9*y &, DREAL(V)\leq 800  \\
      !>   800 &, DREAL(V)>800 
      !>   \end{align*}\right. \f$
      !>  * HO2D+cos: \f$V(x)=\frac{1}{2}p_1*(x^2+y^2)+p_2cos(p_3*(x+y)+p_4)\f$ \image html HO2D+cos.png
      !>  * HO3Diso: \f$ V(x,y,t)=\frac{1}{2}p_1(x^2+y^2+z^2) \f$         
      !>  * HO3D+td_gauss: 
      !>  \f$ \begin{align*}
      !>  &A(t)=\left\{\begin{align*}{cc}
      !>     \frac{p_3*t}{p_4} &,t\leq p_4  \\
      !>     p_3 &,t>p_4 
      !>  \end{align*}\right.\\
      !>  &V(x,x,y)=\frac{1}{2}(p_1^2(x-p_2)^2+y^2+z^2)+A(t)\exp{-\frac{(x-p_5)^2}{2p_6^2}}
      !>  \end{align*} \f$ 
      !> @param[in] Time : Real -- time at which the potential is evulated
      !> @param[in] PSI : Complex array -- the orbitals \f$\langle \Psi \vert  \f$, as generated in the main program
      !> @param[in] MYID : integer -- ID of the process executing this routine
      !> @return 
      !> VTRAP_EXT: Complex array -- contains the potential \f$ V(\vec{r},t)\f$ 


