#include <vector>
#include <iostream>
#include <stdlib.h> // for rand
#include <math.h>
#include <time.h>
#include "PerfectMatching.h"

using namespace std;

// planar code: open boundaries! L better be odd
// initialize: syndrome[i][j] = 1.0 for i=0,...,L-1 and j=0,...,L-2; Z = 1; abort = false

void adrian_tdmc(const double temperature, const double lambda, const int L, double **bosonNumbers, double **syndrome, int &Z, bool &abort, int seed) {
	// coordinates of qubit to probe: ii, jj

	int ii;
	int jj;
	// neighboring plaquettes: A&B
	int iA, jA, iB, jB, ind;
       
        srand(seed);
	
	do {
	        ii = rand() % (2 * L - 1);
        	jj = rand() % (L+1);
	} while (jj == L && ii >= L); 

        cout << "random qubit" << seed <<"," << ii <<"," << jj << std::endl;

	// coordinate of plaquette == coordinate of its uppermost qubit
	if (ii < L) {
		iA = ii;
		iB = ii;
		jA = jj; // jA == L-1 : plaquette does not exist
		jB = jj - 1; // jB == -1 : plaquette does not exist
	}
	else { // L <= i <= 2*L-2
		iA = ii - L;
		iB = iA + 1;
		jA = jj;
		jB = jj;
	}
	double energyCost = 0;
	if (jA < L-1) energyCost += 2 * lambda * syndrome[iA][jA] * bosonNumbers[iA][jA];
	if (jB >= 0) energyCost += 2 * lambda * syndrome[iB][jB] * bosonNumbers[iB][jB];
	if ((double) rand() / RAND_MAX < exp(- energyCost / temperature)) {
		if (jA < L-1) syndrome[iA][jA] *= -1;
		if (jB >= 0) syndrome[iB][jB] *= -1;

		if (ii < L && jj == 0) Z *= -1;

		vector<vector<int> > anyons;
		for (int i = 0; i < L; ++i)
			for (int j = 0; j < L - 1; ++j)
				if (syndrome[i][j] < 0) {
					vector<int> anyon;
					anyon.push_back(i);
					anyon.push_back(j);
					anyons.push_back(anyon);
				}

		int nAnyons = anyons.size();

		PerfectMatching *pm = new PerfectMatching(2*nAnyons, nAnyons * nAnyons);
		PerfectMatching::Options options;
		options.verbose = false;
		pm->options = options;

		for (int i = 0; i < nAnyons; ++i)
			for (int j = i + 1; j < nAnyons; ++j) {
				int xA = anyons[i][0];
				int yA = anyons[i][1];
				int xB = anyons[j][0];
				int yB = anyons[j][1];

				int weight = abs(xA - xB) + abs(yA - yB);
				pm->AddEdge(i, j, weight);
				pm->AddEdge(i+nAnyons, j+nAnyons, 0);
			}

		for (int i = 0; i < nAnyons; ++i) {
			int y = anyons[i][1];
			pm->AddEdge(i, i+nAnyons, y < L/2 ? y+1 : L-y-1);
		}

		pm->Solve();

		int Zcorr = 1;

		for (int i = 0; i < nAnyons; ++i) {
			int j = pm->GetMatch(i);
			if (j < nAnyons) continue;

			if (anyons[i][1] < L/2) Zcorr *= -1;
		}

		delete pm;

		if (Z != Zcorr) abort = true;
	}
}
