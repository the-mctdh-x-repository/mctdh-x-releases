!>GPLv3
!    MCTDH-X: the multiconfigurational time-dependent Hartree for 
!    indistinguishable particles software
!
!    Copyright (C) 2021  A. U. J. Lode, M. C. Tsatsos, 
!                        E. Fasshauer, S. E. Weiner, 
!                        R. Lin, L. Papariello, P. Molignini, 
!                        C. Lévêque, M. Büttner , J. Xiang, S. Dutta
!
!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <https://www.gnu.org/licenses/>.
!
!
!
!
!<GPLv3










!MODULE SYNDROMEARRAY
!IMPLICIT NONE
!REAL*8,ALLOCATABLE,SAVE :: Fsyndrome(:)
!END MODULE SYNDROMEARRAY

MODULE ANYONS

CONTAINS 


Subroutine Step_anyons_TDMC(temp,lambda,L,boson_density,syndrome,step,abort)

USE iso_c_binding
USE Global_Parameters
USE Auxiliary_Routines
USE DVR_Parameters 
!USE SYNDROMEARRAY

implicit none

interface

subroutine tdmc(temperature, lambda, L, &
                       bosonNumbers,syndrome,z,abort,seed) & 
                bind ( C, name="tdmc_" )

   use iso_c_binding

   integer ( c_int ), VALUE :: L
   real ( c_double ), VALUE :: temperature, lambda
   real ( c_double ), dimension(L*(L-1)) :: bosonNumbers, syndrome
   integer (c_int) :: Z,seed
   logical (c_bool) :: abort

end subroutine tdmc 

end interface

INTEGER step,L
INTEGER*8 M,I,J,K,IND

REAL*8,ALLOCATABLE,SAVE :: Fsyndrome(:)


REAL :: R
REAL( c_double ) temp, lambda

REAL( c_double ) :: boson_density(NDVR_X*NDVR_Y)

REAL( c_double ), DIMENSION(0:L**2-1-L) :: bosons
REAL( c_double ), DIMENSION(0:L**2-1-L) :: syndrome
integer ( c_int ) :: Length,seed
integer ( c_int ),save :: z
logical (c_bool) :: abort
logical (c_bool),save :: Fabort

Write(6,*) "In Step_Anyons"

IF (Step.eq.1) THEN

 syndrome=1.d0

 IF (ALLOCATED(FSYNDROME).eqv..FALSE.) THEN
    ALLOCATE(FSYNDROME(L**2-L))
 ENDIF

 Fsyndrome=syndrome
 z=1
 abort=.FALSE.
 Fabort=abort 
ELSE
 syndrome=Fsyndrome
 abort=Fabort
ENDIF

!Extract Part of density for toric code
ind=1
DO M=1,NDVR_X*NDVR_Y
   CALL get_ijk_from_m(m,NDVR_X,NDVR_Y,i,j,k)
   IF ((Ort_X(I).ge.(-L/2)).and.(Ort_X(I).le.L/2-1) &
       .and.(Ort_Y(J).ge.(-L/2)).and.(Ort_Y(J).le.L/2)) THEN
       bosons(ind)=boson_density(m)
       ind=ind+1
   END IF
END DO

Length=L*(L-1)
Step=Step+1

CALL init_random_seed() 
CALL RANDOM_NUMBER(R)
SEED=INT(100000000*R)
!write(6,*) "SEED",seed
!write(6,*) "synd", syndrome
!write(6,*) "bos", bosons(99)
!write(6,*)"temp,lambda,L,Z,abort",temp,Z,lambda,length,abort
call tdmc(temp,lambda,L,bosons,syndrome,z,abort,seed)
!write(6,*) "syndrome after call to tdmc", syndrome

Fsyndrome=syndrome
return
END SUBROUTINE Step_anyons_TDMC


subroutine init_random_seed()
  
end subroutine init_random_seed

END MODULE ANYONS


