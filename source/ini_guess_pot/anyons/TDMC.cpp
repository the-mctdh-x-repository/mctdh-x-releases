/*
 * main.cpp
 *
 *  Created on: Nov 13, 2014
 *      Author: adrian
 */
#include "adrian.cpp"
#include <iostream>


extern"C" {
//template <size_t rows, size_t cols>
    void tdmc_(const double temperature, const double lambda, int L, double* bosonNumbers, double* syndrome, int &Z, bool &abort, int &seed) 
 {
	double** boson = new double*[L];
	for (int i = 0; i < L; ++i) boson[i] = new double[L-1];
	double** synd = new double*[L];
	for (int i = 0; i < L; ++i) synd[i] = new double[L-1];
	int ind = 0;
	for (int i = 0; i < L-1; ++i)
	    for (int j = 0; j < L; ++j) {
		boson[j][i] = bosonNumbers[ind];
		synd[j][i] = syndrome[ind];
		++ind;
	}

	adrian_tdmc( temperature, lambda, L, boson, synd, Z, abort, seed);

	ind = 0;

	for (int i = 0; i < L-1; ++i)
	    for (int j = 0; j < L; ++j) {
		bosonNumbers[ind] = boson[j][i];
		syndrome[ind] = synd[j][i];
		++ind;
	}
 /*	for (int i = 0; i < L; ++i) delete[] boson[i];
	delete[] boson;
	for (int i = 0; i < L; ++i) delete[] synd[i];
	delete[] synd;
*/
  
 }
}
