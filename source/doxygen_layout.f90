!---------------------------------------------------------------
!-------------------------  MAIN PAGE  -------------------------
!---------------------------------------------------------------

!>  \mainpage                                                                      
!>  \image html MCTDH-X_blue_black_slurred.png 
!> 
!> MCTDH-X is a program package to solve the 
!> time-dependent many-body Schroedinger equation for indistinguishable and interacting particles.
!> The theory expands the field operator in time-dependent modes. 
!> Hence, MCTDH-X provides a way to systematically improve the predictive
!> quality of the numerical description beyond mean-field methods like
!> the discrete non-linear Schroedinger equation, the Gross-Pitaevskii equation, or Hartree-Fock approaches.
!> Theory and background information can be found in the following references:
!> * MCTDH-X website: http://ultracold.org
!> * Releases on gitlab: https://gitlab.com/the-mctdh-x-repository/mctdh-x-releases
!> * Code workflow and tutorial: https://iopscience.iop.org/article/10.1088/2058-9565/ab788b
!> * Review of the MCTDH-X family of methods: https://journals.aps.org/rmp/abstract/10.1103/RevModPhys.92.011001
!> * original derivation MCTDH-B: https://journals.aps.org/pra/abstract/10.1103/PhysRevA.77.033613
!> * original derivation MCTDH-X: https://aip.scitation.org/doi/10.1063/1.2771159
!> * implementation of MCTDH-F: https://journals.aps.org/pra/abstract/10.1103/PhysRevA.93.033635
!> * implementation of MCTDH-B with spin:  https://journals.aps.org/pra/abstract/10.1103/PhysRevA.93.063601

!---------------------------------------------------------------


!---------------------------------------------------------------
!--------------- GROUPING / DROPDOWN MENU ----------------------
!---------------------------------------------------------------

!> @defgroup main The 'main' Modules
!! @brief Computes the numerical solutions of the TISE or TDSE.
!! 
!! The MCTDH-X package contains a main program to perform the actual numerical task and an analysis 
!! program that is used to compute the desired quantities of analysis from the many-body wavefunction. 
!! To set up a numerical task, the user modifies and chooses the parameters via the text input file 
!! MCTDHX.inp. A detailed description of the available options is given in the manual of the MCTDH-X 
!! software. The subroutine get_1bodypotential is used to specify custom 
!! one-body potentials, while the subroutine ::get_interparticle_potential 	
!! allows for custom two-body potentials.
!! For custom (initial) states, the subroutine ::get_initial_coefficients
!! and ::get_initial_orbitals files can be used to specify the (initial) 
!! coefficients and orbitals, respectively.
!!
!! The workflow and structure of the MCTDH-X software follows naturally from its main objectives to
!! determine a numerical solution to the TISE or the TDSE and then to extract desired quantities of interest from
!! the solution. This workflow can be summarized in the following steps, 
!!
!! <img src="MCTDHX_workflow_simple.png"  width="600">
!!
!! where 1 and 3 are done by the main program.
!!
!! The main module of the main program is Program_MCTDHX_Main.F. It shall briefly be described here:
!!
!> This is the main MCTDH-X program. The main program runs the initialization of 
!> variables and arrays and starts the Master and Slave processes 
!> that drive the 7-step integration scheme (see, e.g., https://journals.aps.org/pra/abstract/10.1103/PhysRevA.77.033613). 
!> Technically, the MCTDH-X software is a set of programs for the numerical solution of the MCTDH-X equations (see below) as well as the analysis of the obtained results:
!> * Orbitals:  \f$ \qquad i \partial_t \vert \phi_j \rangle = \mathbf{\hat{P}} \left[ \hat{h} \vert \phi_j \rangle + \sum_{k,s,q,l=1}^{M} \lbrace \mathbf{\rho}(t) \rbrace_{jk}^{-1} \rho_{ksql} \hat{W}_{sl}\vert \phi_q \rangle \right] \f$
!> * Coefficients: \f$ \qquad i \partial_t C_{\vec{n}}(t)=\sum_{\vec{n'}} \langle \vec{n};t \vert \hat{H} \vert \vec{n'};t \rangle C_{\vec{n}'} \f$
!> ## Inputs
!> @param[in] defined via MCTDHX.inp and, if required, code in the subdirectory ./source/ini_guess_pot
!> ## Outputs
!> @return binary and ASCII data files. binary data can be postprocessed using the MCTDHX_analysis program.
!!
!! The main program visualized differently:
!!
!! <img src="7StepIntegrationRoutine.png"  width="600">
!! 
!! @{
!! @defgroup inout Input-Output
!! @brief Reading in of input in various forms (fortran namelists, binaries, ASCII) and writing of output.
!!
!! @defgroup init Initialization and Allocation of States
!! @brief Prior to running anything, space has to be allocated for potential related variables, coefficients and orbitals. Much has to be initialized.
!!
!! @defgroup Hamiltonian Setting up the potentials of the Hamiltonian
!! @brief Routines that are often altered to customize the physical model system.
!!
!! @defgroup sevenstepintegration Main calculation loop: 7 Step Integration
!! @brief This is were the solution of the Schroedinger equation is found iteratively.
!!
!! At the heart of MCTDH-X lie the two coupled equations of motion for the permanent (determinant) coefficients 
!! and the orbitals that constitute the permanents (determinants) themselves, the multiconfigurational-type linear combination of which
!! is the solution
!! to the time-dependent bosonic (fermionic) Schrödinger equation. \n
!! The derivation of these EOMs can be found in numerous publications on the subject. \n
!! The equation of motion for the coefficients
!!
!! \f$ \qquad i \partial_t C_{\vec{n}}(t)=\sum_{\vec{n}'} \langle \vec{n};t \vert \hat{H} \vert \vec{n} ';t \rangle C_{\vec{n}'} \f$
!! 
!! is a linear differential equation and can be solved e.g. with short iterative Lanczos (SIL).
!! 
!! The equation of motion for the orbitals
!!
!! \f$ \qquad i \partial_t \vert \varphi_j \rangle = \mathbf{\hat{P}} \left[ \hat{h} \vert \varphi_j \rangle + \sum_{k,s,q,l=1}^{M} \lbrace \mathbf{\rho}(t) 
!! \rbrace_{jk}^{-1} \rho_{ksql} \hat{W}_{sl}\vert \varphi_q \rangle \right] \f$ 
!!
!! is a nonlinear integrodifferential equation. It can be propagated by means of general variable-order integrators, such as the Adam-Bashforth-Moulton (ABM)
!!
!! The EOMs are coupled, since the orbital EOM contains the coefficient dependent \f$\rho_{kq} \f$ und \f$\rho_{ksql}\f$ while the coefficient EOM 
!! simultaneously contains the orbital depentent matrix elements of the Hamiltonian. A specific integration scheme was created to solve this problem es addiciently as possible.
!! This scheme is the <b> constant mean field (CMF) </b> integration scheme.
!!
!! To quote https://link.springer.com/article/10.1007/s004600050342:
!!
!! "The main idea underlying our integration scheme is motivated by the realization that the A-Hamiltonian matrix elements 
!! and the product of the inverse density and the mean-field matrices generally change much slower than the MCTDH-coefficients and the single-particle functions."
!!
!! This allows separate integration steps were these quantities are kept constant (careful: very handwaving statement). \n
!! The following schematic illustrates this scheme: \n
!! ![](./documentation/images/7stepintegration.png)
!!
!! The main routine for this integration scheme is master_slave_parallelization::master_mctdhbf. \n
!! It contains the integration loop that calls (among others) the following key subroutines:
!! * integration::integrator_ci
!! * integration_steps::do_step1_orbitals
!! * integration_steps::do_step3_orbitals
!! * integration_steps::do_step4and5_orbitals
!!
!! Note that while the illustration of the integration scheme contains 7 steps, only step 1, 3 and 4+5 have
!! their own subroutines. This is because the other steps perform the calculation for the (foreward)
!! propagation of the coefficients, and here the integrator for the coefficients integration::integrator_ci is called directly in
!! master_slave_parallelization::master_mctdhbf because it is easier to perform,
!! whereas the orbital integration steps have their own wrapper routines to manage the evaluation of the right
!! hand side of the orbitals EOM. 
!! What the wrapper routines do, varies for the different stps of the integration scheme and is explained in the respective routine 
!! descriptions.
!!
!! Some of the important subrputines here are:
!! * integration_steps::evaluate_rhs_orbitals
!! * orbital_equationofmotion::func or orbital_equationofmotion::func_unified2
!! * integration::integrator_orb or integration::integrator_orb_unified 
!!
!! As is explained in the respective routine descriptions, integration::integrator_orb and
!! integration::integrator_orb_unified then implement different integrator calls, and pass the func subroutine
!! to those integrator routines.
!!
!! @note Axel: the routine func and everything it calls should use local variabels
!!
!! @defgroup mapping Mechanism: Operator mapping in Fock space
!! @brief Obtains results of the action of operators without constructing theirs matrices.
!!
!!
!! For any procedure that requires the evaluation of the action of any operator on
!! a state of interest, the routine hamiltonianaction_coefficients::get_hamiltonianaction_ci_core is called.
!! Mostly what this subroutine does is chose an appropriate mapping functionality and then call 
!! one of the follwoing mapping routines:
!! * hamiltonianaction_coefficients::apply_mapping_1body
!! * hamiltonianaction_coefficients::apply_mapping_2body
!! * hamiltonianaction_coefficients::apply_mapping_1body_fermions
!! * hamiltonianaction_coefficients::apply_mapping_2body_fermions
!! * recursive_1body_mapconstruction::get_1body_mapping_recursive
!! * recursive_2body_mapconstruction::get_2body_mapping_recursive
!! * onebody_mapconstruction_fermions::get_1body_mapping_fermions
!! * twobody_mapconstruction_fermions::get_2body_mapping_fermions
!!
!! Go check the documentation in hamiltonianaction_coefficients::get_hamiltonianaction_ci_core for some
!! intel on how the above routines are chosen, i.e. in what order and in which case. 
!! 
!! A **custom data type** was created to save the results of applying the action of a group of creation and 
!! annihilation operators onto a given configuration in a shortended version. The data type is saved in specific modules:
!! * <b> fermionic case </b>: Module_CI_Production_Parameters_Fermions
!! * <b> bosonic case </b>: Module_CI_Production_Parameters
!! 
!!
!! We also added documentation on the subroutines of the modules addresses and addresses_fermions which are necessary for the indexing of the 
!> of matrix elements and configuration coefficients which is essential to our approach. These modules are distinct from from the ones mentioned before
!> because they do not actively compute any action of any operator yet. 
!> (see https://journals.aps.org/pra/abstract/10.1103/PhysRevA.81.022124
!!
!! The relevant subroutines are:
!! * addresses::get_configurationfromindex
!! * addresses::get_indexfromconfiguration
!! * addresses::rhoall
!! * addresses::nadr
!! * addresses::symmexp
!! * addresses::nadrw
!! * addresses_fermions::get_configuration_distance
!! * addresses_fermions::get_configurationfromindex_fermions
!! * addresses_fermions::get_indexfromconfiguration_fermions 
!!
!!
!! @defgroup cavity Optional: Cavity-BEC Calculations
!! @brief Simulates a BEC inside of a cavity with external fields.
!!
!! When performing a cavity-BEC calculation, the basic MCTDH-X approach of solving
!! the equations of motion 
!! 
!! \f$ \qquad i \partial_t \vert \phi_j \rangle = \mathbf{\hat{P}} \left[ \hat{h} \vert \phi_j \rangle + 
!! \sum_{k,s,q,l=1}^{M} \lbrace \mathbf{\rho}(t) \rbrace_{jk}^{-1} \rho_{ksql} \hat{W}_{sl}\vert \phi_q \rangle \right] \f$
!!
!! by using a constant mean-field integration scheme is employed in a slightly different way.
!! Like in any standard BEC calculation, program_mctdhx_main still goes through the process of initializing MPI, reading +
!! distributing input, initializing coefficients, initializing output files, allocating orbitals 
!! and the calling subroutine mctdhx::mctdhx_routine. 
!! Also like in the regular, non-cavity case, the subroutine then initializes the interparticle potential
!! and then manages the 7 step integration.
!! Only there then, does the cavity implementation come into play. 
!! All additions to the Hamiltonian that relate to the cavity are of one-body nature.
!! For example a Hamiltonian could take on the form
!!
!! \f$  \hat H = \int \mathrm{d}x \mathrm{d}y \mathrm{d}z \hat \Psi^\dagger \bigg( \frac{\mathbf{p}^2}{2m_\text{Rb}}+V_\text{trap} +
!! V_\text{opt} \bigg) \hat \Psi + \frac{1}{2}g_{3D} \int \mathrm{d}x \mathrm{d}y \mathrm{d}z \hat \Psi^\dagger \hat \Psi^\dagger \hat \Psi \hat \Psi   \f$
!!
!! with the trapping potenital 
!!
!! \f$ V_\text{trap} = \frac{1}{2}m_\text{Rb} (\omega_x^2 x^2 + \omega_y^2 + \omega_z^2 z^2) \f$ \n
!!
!! and the optical lattice potential 
!!
!! \f$ V_\text{opt} = V_\text{pump}+V_\text{slice}  + \sqrt{\hbar E_p |U_0|} (\alpha+\alpha^*) \cos(k_c x) \cos(k_c y)+ \hbar U_0 |\alpha|^2 \cos^2(k_c x) \f$
!! 
!! Thus, the cavity comes into play when evaluating the one-body terms.
!! When executing the integration in integration::integrator_orb_unified, 
!! orbital_equationofmotion::func_unified is called
!! which in turn calls kineticenergyaction::get_kineticenergyaction_allorbitals .
!! Here the one-body terms are evaluated. 
!!
!!
!! <b> The cavity-specific treatment then does the following: </b> \n
!! * initialize the atom cavity expectation value
!! * initialize the atom pump expectation value
!! * CALL cavitybec::get_atom_pump_cavityexpectations,
!! * CALL cavitybec::get_cavityaction, this returns the propagated cavity field and computes
!! the one-body potential that is then used further when computing the action on the states
!!
!! See current (May 2022) code excerpt from kineticenergyaction::get_kineticenergyaction_allorbitals 
!! for comparison (Atom_CavityExpectation is a global variable):
!!
!!      IF (Cavity_BEC.eqv..TRUE.) THEN
!!
!!         Atom_CavityExpectation=0.d0
!!         Atom_PumpExpectation=cmplx(0.d0,0.d0)
!!
!!         CALL Get_Atom_Pump_CavityExpectations(PSI_Cavity,
!!     .                                         local_time)
!!
!!         CALL Get_CavityAction(local_time,PSI_Cav%Cavity) 
!!
!!      ENDIF
!!
!! @}
!! @defgroup analysis The 'analysis' Modules
!! @brief Analyzes the solutions of the TISE or TDSE.
!! @{
!! @defgroup singleshots Single Shot Generation
!! @brief Simulates single shot measurements by sampling the N-body density.
!!
!! ********
!! Single shots are samples of the N-body density 
!! \f$|\Psi(\mathbf r_1,...,\mathbf r_N)|^2\f$. 
!! Rather than sampling from this directly, one
!! can utilize the possibility of expressing it as a product of 
!! conditional probabilities
!! 
!! \f$ |\Psi(\mathbf r_1,...,\mathbf r_N)|^2 = P(\mathbf r_1,..., \mathbf r_N) = 
!! P(\mathbf r_1)P(\mathbf r_2|\mathbf r_1)...P(\mathbf r_N|\mathbf r_{N-1}...\mathbf r_1) \f$
!!
!! and generate a single shot by sampling 
!! * the position of the first particle \f$ \mathbf r_1\f$ from \f$ P(\mathbf r_1) \f$, 
!! * the position of the second particle \f$ \mathbf r_2\f$ from \f$ P(\mathbf r_2 | \mathbf r_1) \f$, 
!! * the position of the thirs particle \f$ \mathbf r_3\f$ from \f$ P(\mathbf r_3 | \mathbf r_2, \mathbf r_1) \f$, 
!!
!! and so on.
!!
!! To do this, we make use of the relation
!!
!! \f$ P(A_n | A_1 \cap A_1 \cap ...A_{n-1}) = \frac{P( A_1 \cap A_1 \cap ...A_{n})}{P(A_1 \cap A_1 \cap ...A_{n-1})} \f$
!!
!! for the conditional probabilities, as well as the fact that we know the order in which they are sampled.
!!
!! With \f$ k \f$ as the number of particles already sampled, the problem can be approached in terms of reduced sub-systems and we consider
!!
!! \f$ P(\mathbf r_{k+1} | \mathbf r_k,..., \mathbf r_1) = \frac{P(\mathbf r_{k+1} , \mathbf r_k,..., \mathbf r_1)}{P( \mathbf r_k,..., \mathbf r_1)} \propto \rho_k(\mathbf r_{k+1}) \f$
!!
!! where the defined proprtionality is due to the density distribution of the k-particle subsystem, i.e. the denominator of the fraction, being a constant.
!!
!! Utilizing this, the conditional probability-dependent sampling above can now be performed by sampling
!! * the position of the first particle \f$ \mathbf r_1\f$ from \f$ P(\mathbf r_1)= \frac{\rho_0}{N} \f$, 
!! * the position of the second particle \f$ \mathbf r_2\f$ from \f$ \rho_{k=1}(\mathbf r_2) \f$, 
!! * the position of the thirs particle \f$ \mathbf r_3\f$ from \f$ \rho_{k=2}(\mathbf r_3) \f$, 
!!
!! and so on. 
!!
!! ********
!! <b>The single shot related routines are hence dealing with one of two tasks:</b>
!! 
!! 1. Calculate reduced 1-body densities \f$ \rho_{k} \f$
!! 2. Apply sampling algorithm to reduced 1-body densities \f$ \rho_{k} \f$
!!
!!
!! ********
!!
!! <b> 1. Calculate reduced 1-body densities \f$ \rho_{k} \f$ :</b> 
!!
!! * auxiliary_analysis_routines::get_reduced_coefficients
!! * auxiliary_analysis_routines::get_reduced1bodyelements
!!
!! The reduced p-body density is given as
!! 
!! \f$ \rho^{(p)}_k(\mathbf x_1,..., \mathbf x_p) = \langle \Psi^{(k)} | \hat \Psi^\dagger (\mathbf x_1) ...\hat \Psi^\dagger (\mathbf x_p) \hat \Psi (\mathbf x_1)...\hat \Psi (\mathbf x_p)| \Psi^{(k)} \rangle \f$
!!
!! with \f$ \Psi^{(k)}\f$ being the respective reduced wave function, defined as
!!
!! \f$ | \Psi^{(k)} \rangle =  \begin{cases} | \Psi \rangle, \quad &\text{if} \quad k=0 \\   \mathcal{N}_k \hat \Psi(\mathbf{r}_k) |\Psi^{(k-1)} \rangle, \quad &\text{if} \quad k=1,...,N-1    \end{cases}\f$
!!
!! The conditional density is given as
!!
!! \f$ \rho^{(j)}_\text{cond}(\mathbf x_j) = \langle \Psi^{(j)} | \hat \Psi^\dagger (\mathbf x_k) \hat \Psi (\mathbf x_k)| \Psi^{(j)} \rangle  \f$
!!
!! which is a property with which one can easily obtain the diagonal elements of the p-body density, by calculating:
!!
!! \f$ \rho^{(p)}_\text{num}(\mathbf x_1,..., \mathbf x_p) = \prod_{j=1} ^p \rho^{(j-1)}_\text{cond}(\mathbf x_j)  \f$
!!
!! ********
!!
!! <b> 2. Apply sampling algorithm to reduced 1-body densities \f$ \rho_{k} \f$:</b> 
!!
!! The individual particle positions for each of the single shot images are obtained in a double loop over the number of single shots (outer loop) and 
!! the number of particles (inner loop).
!! In the loop over all particles (implemented in analysis_routines_manybody::get_singleshot_core), the elements of the reduced densitiy matrix \f$ \rho_{jk} \f$ are obtained by calling auxiliary_analysis_routines::get_reduced_coefficients and 
!! auxiliary_analysis_routines::get_reduced1bodyelements, as explained above, after which auxiliary_analysis_routines::drawfromdensity is called.
!! DrawFromDensity is the core routine for the sampling implementation (refer to its call graph for an overview).
!! 
!! There, the density is first calculated using the elements of the reduced densitiy matrix \f$ \rho_{jk} \f$
!!
!! \f$ \rho(\mathbf x) = \sum_{jk} \rho_{jk} \phi_j^* (\mathbf x) \phi_k (\mathbf x)\f$
!! 
!! and then, depending on the dimensionality, one of three subroutines is called, all of which implement some sort of rejection sampling algorithm:
!!
!! * auxiliary_analysis_routines::draw_sample_1d
!! * auxiliary_analysis_routines::draw_sample_2d
!! * auxiliary_analysis_routines::draw_sample_3d
!!
!! Note: To obtain the sample from the uniform distribution within a the range of 0 to the maximum of the probability density function we want to sample from (here: 1),
!! random::gnu_rand_burkardt is called.
!!
!! @todo unify Draw_sample_1D, Draw_sample_2D and Draw_sample_3D
!! @}
!!
!! @defgroup lib The 'lib' Modules
!! @brief Collection of modules that implement central mathematical functionalities
!! @todo random::haar_u_n and random::ginue work together but are not really used anywhere. Obsolete?
!!
!! @{
!! @defgroup random Pseudo-Random Number Generators
!! @brief Collection of different Pseudo-Random Number Generators (PRNGs)
!!
!! Keeping in mind, that Fortran itself provides a PRNG, namely RANDOM_NUMBER (complete with its seed initializer RANDOM_SEED), this module offers improvements
!! of this functionality by either extending it only slightly, as is done in
!!
!! * random::gnu_rand
!! * random::gnu_rand4
!! * random::gnu_rand8_normal
!! * random::gnu_rand_c16
!! 
!! or by applying more elaborate extensions
!!
!! * random::ginue
!! * random::gnu_rand_burkardt along with random::r8_uni
!! * random::haar_u_n
!!
!! The seed initializer that is used is always random::gnu_init_random_seed
!!
!! @}

!---------------------------------------------------------------

