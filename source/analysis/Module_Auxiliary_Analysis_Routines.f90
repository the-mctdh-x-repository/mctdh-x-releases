!>GPLv3
!    MCTDH-X: the multiconfigurational time-dependent Hartree for 
!    indistinguishable particles software
!
!    Copyright (C) 2021  A. U. J. Lode, M. C. Tsatsos, 
!                        E. Fasshauer, S. E. Weiner, 
!                        R. Lin, L. Papariello, P. Molignini, 
!                        C. Lévêque, M. Büttner , J. Xiang, S. Dutta
!
!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <https://www.gnu.org/licenses/>.
!
!
!
!
!<GPLv3









!> This Module collects auxiliary routines that perform transforms on the
!> one-body and two-body matrix-elements or the coefficients, the orbitals, or 
!> Routines that get Filenames.
MODULE Auxiliary_Analysis_Routines
  USE   Global_Parameters
  USE   DVR_Parameters
  USE   Auxiliary_Routines

CONTAINS

!------------------------------------------------------------------------------
!> compute the determinant of a nxn matrix using a LU decomposition of the matrix and mutliply 
!> the diagonal elements of LU matrix obtained  
!>
!>\authors Camille Lévêque (2017)
!>
subroutine get_Determinant(mat,n,det) 

   !---{{{
     implicit none
     integer,          intent(in) :: n 
     COMPLEX*16,intent(in) :: mat(n,n)
   
     COMPLEX*16,intent(out) :: det
   
     integer :: i, ierr, ipiv(n)
     COMPLEX*16 :: Mtmp(n,n)
     COMPLEX*16 :: One_C = dcmplx(1.d0, 0.d0)
   
    
     Mtmp = mat
     !ipiv = 0
     !write(0,*) 'MTMP:', Mtmp
     call zgetrf(n,n,Mtmp,n,ipiv,ierr)
   
     if (ierr .ne. 0) then
        write(0,*) 'error in subrotuine Determinant when calling zgetrf '
        write(0,*) 'to perform LU decomposition '
        write(0,*) 'Error code:', ierr
        
        stop
     endif
   
     det = One_C
   
     do i = 1, n
        if(ipiv(i) .ne. i) then
           det = - det * Mtmp(i,i)
        else
           det = det * Mtmp(i,i)
        endif 
     enddo 
   
     return
   
   !---}}} 
   end subroutine get_Determinant
   


SUBROUTINE get_Permanent(mat,n,perm)
   implicit none
   integer,          intent(in) :: n 
   COMPLEX*16,  intent(in) :: mat(n,n)

   COMPLEX*16,  intent(out) :: perm

   integer         :: i, j, k, z, s, g(n)
   COMPLEX*16 :: p, t, sm, X(n)
  
   p = - ZONER 
   t = ZERO

   X(:) = ZERO
   g(:) = 0

   do i = 1, n

      sm = ZERO
      do j = 1, n
         sm = sm + mat(i,j)
      enddo    
 
      X(i) = mat(i,n) - 0.5d0 * sm 
      p    = p * X(i)

   enddo 
  
   s = -1
   
   do k = 2, 2**(n-1)
   
      if (mod(k,2) == 0) then
         j = 1
      else
         j = 2
         do while( g(j-1) == 0 )
            j = j + 1
         enddo
      endif  

      z    = 1 - 2 * g(j) 
      g(j) = 1 - g(j)
      s    = -s
      t    = real(s,kind=8) 
  
      do i = 1, n
         X(i) = X(i) + real(z,kind=8)* mat(i,j)
         t    = t * X(i)   
      enddo

      p = p + t 

   enddo   

   perm = 2d0 * (-1d0)**(n) * p

   return

end SUBROUTINE get_Permanent

SUBROUTINE OverlapMat(Psi1,Psi2,NDX,MOrb,OvMat)

   implicit none
   integer,         intent(in)  :: NDX, MOrb
   COMPLEX*16, intent(in)  :: Psi1(NDX,MOrb), Psi2(NDX,MOrb)
   COMPLEX*16, intent(out) :: OvMat(MOrb,MOrb)
    
   integer         :: i, j, k
   COMPLEX*16 :: normC


   do i = 1, MOrb
      do j = 1, MOrb
 
         normC = ZERO 
         do k = 1, NDX
            normC = normC + dconjg(Psi1(k,i))*Psi2(k,j)
         enddo

         OvMat(i,j) = normC 

      enddo
   enddo

end SUBROUTINE OverlapMat

SUBROUTINE get_m_0_from_xyz(m_0,xref,yref,zref)
use DVR_Parameters
  IMPLICIT NONE
  INTEGER*8, INTENT(OUT) :: M_0
  REAL*8, INTENT(IN)  :: xref,yref,zref
  real*8  :: dist_x,dist_y,dist_z
  integer*8            :: m,n,i_n,j_n,k_n,i_0,j_0,k_0,NOGRIDPOINTS
  
  dist_x=1.d99
  dist_y=1.d99
  dist_z=1.d99

  NOGRIDPOINTS=NDVR_X*NDVR_Y*NDVR_Z
  do n=1,NOGRIDPOINTS
     CALL get_ijk_from_m(n,NDVR_X,NDVR_Y,i_n,j_n,k_n)
     IF(sqrt((xref-ort_x(i_n))**2).lt.dist_x) THEN
        i_0=i_n
        dist_x=sqrt((xref-ort_x(i_n))**2)
        m_0=i_n
     ENDIF
  end do

  IF(DIM_MCTDH.ge.2) THEN
     CALL get_ijk_from_m(n,NDVR_X,NDVR_Y,i_n,j_n,k_n)
    do n=1,NOGRIDPOINTS
       IF(sqrt((yref-ort_y(j_n))**2).lt.dist_y) THEN
         j_0=j_n
         dist_y=sqrt((yref-ort_y(j_n))**2)
       ENDIF
    end do
    m_0=m_0+(j_0-1)*NDVR_X    
  ENDIF

  IF(DIM_MCTDH.ge.3) THEN
     CALL get_ijk_from_m(n,NDVR_X,NDVR_Y,i_n,j_n,k_n)
    do n=1,NOGRIDPOINTS
      IF(sqrt((zref-ort_z(k_n))**2).lt.dist_z) THEN
        k_0=k_n
        dist_z=sqrt((zref-ort_z(k_n))**2)
      ENDIF
    end do
    m_0=m_0+(k_0-1)*NDVR_X*NDVR_Y
  ENDIF
end SUBROUTINE get_m_0_from_xyz

!> @ingroup singleshots
SUBROUTINE Get_Reduced1bodyElements(VIN,Nconf,Reduced_OneBodyElements,Nparticles)

use Global_Parameters,only:Morb
USE Matrix_Elements
USE Coefficients_Parameters,ONLY:MatrixOfBinomialCoefficients
USE Function_Library
USE omp_lib


implicit NONE

INTEGER :: NParticles
COMPLEX*16 :: VIN(NConf)
COMPLEX*16, DIMENSION(RDIM) :: Reduced_OneBodyElements

integer::  P
integer::  i
integer::  cJ,cK
integer::  I_current_term,Nterms
COMPLEX*16, DIMENSION(100) :: sumvout1=Zoner
real*4 :: start
INTEGER ::  FromN,TillN,Operator_Counter

INTEGER, DIMENSION(MORB+1) :: Sh_m,Sh_p
INTEGER::Nconf


FromN=1
TillN=MaxTrm1b
Nterms=MaxTrm1b
! IF GP
! IF GP
IF (MORB.eq.1) THEN
! IF GP
! IF GP
    Rho1_Elements(1)=ZONER*Nparticles
    VIN(1)=ZONER*Npar*Compact_1bodyHamiltonian_Elements(1)
! IF NOT GP
! IF NOT GP
ELSE IF (MORB.gt.1) THEN
! IF NOT GP
! IF NOT GP

!======== Loop over one-body operators and allocate mapping data type
!======== Loop over one-body operators and allocate mapping data type
    DO I=FromN,TillN
!======== Loop over one-body operators and allocate mapping data type
!======== Loop over one-body operators and allocate mapping data type
 
         I_current_term= TERM_REQ_1B(I)
         P=TERM_INDEX_1B(I_current_term)
         Reduced_OneBodyElements(I_current_term)=0.d0
         cK= INT(P/100)
         cJ= P-cK*100
         Sh_m=0
         Sh_m(cK)=Sh_m(cK)+1
         Sh_p=0
         Sh_p(cJ)=Sh_p(cJ)+1
       
         CALL loopy_main_ReducedElements(Nparticles,Morb,Sh_m,Sh_p,&
                        CJ,CK,&
                        nconf,I_current_term,I,&
                        RDIM,VIN,Reduced_OneBodyElements)

!======== Loop over one-body operators and allocate mapping data type
!======== Loop over one-body operators and allocate mapping data type
    EndDO
!======== Loop over one-body operators and allocate mapping data type
!======== Loop over one-body operators and allocate mapping data type
ENDIF
         
end SUBROUTINE Get_Reduced1bodyElements

!> Routine to compress the mapping for one-body operators to only the non-zero contributions.
      SUBROUTINE loopy_main_ReducedElements(Npar,Morb,Sh_m,Sh_p, &
                           CJ,CK, &
                           nconf,I_current_term,I_term, &
                           RDIM,VIN,Reduced_OneBodyElements)

      USE Coefficients_Parameters,ONLY:MatrixOfBinomialCoefficients

      IMPLICIT NONE

      INTEGER:: NPar,Morb
      INTEGER :: CJ,CK,nconf,I_current_term,RDIM,Operator_Counter
      COMPLEX*16:: VIN(Nconf)

      integer lix(Morb-1), ix, I_Term,MYID
      INTEGER   , DIMENSION(MORB+1) :: Sh_m,Sh_p
      integer depth,FromN,ToN
       COMPLEX*16:: Reduced_OneBodyElements(RDIM)


      lix = 0
      ix=1
      depth=Morb-1
      FromN= Npar+Morb-1-Sh_m(1)
      ToN=Morb-1
      if(depth==1) ToN=Morb-1 +Sh_m(2)

      CALL loopy_core_ReducedElements(depth, FromN,ToN, lix, & 
                       ix, Sh_m,Sh_p,Morb, & 
                       NPAR,nconf, &
                       CJ,CK,I_current_term,I_Term, &
                       RDIM,VIN,Reduced_OneBodyElements)
         

      end SUBROUTINE loopy_main_ReducedElements

!> Recursive core of the compression of the mapping.
      recursive SUBROUTINE loopy_core_ReducedElements(depth,FromN,ToN,lix, &
                                                       ix,Sh_m,Sh_p,Morb, &
                                                       NPAR,nconf, &
                                                       CJ,CK,I_current_term,I_Term, &
                                                       RDIM,VIN,RIJ)

      USE Coefficients_Parameters,ONLY:MatrixOfBinomialCoefficients
      USE Function_Library
      USE Matrix_Elements, ONLY: MaxTrm1b

      IMPLICIT NONE

      integer   :: nconf, FromN, ix, ToN,jj,Morb,k,NPAR,IND1,IND2
      integer   :: RDIM

      COMPLEX*16:: RIJ(RDIM)
      COMPLEX*16:: VIN(NConf)
      integer :: CJ,CK,I_current_term
      integer, intent(inout)     :: depth
      integer   :: FN,TN,MYID,I_TERM
      integer, intent(inout)  :: lix(depth)
      INTEGER   , DIMENSION(Morb+1) :: Sh_m,Sh_p
      INTEGER , DIMENSION(DEPTH) :: JVEC
      INTEGER  , DIMENSION(Morb) ::nvecin,nvecout
      
      INTEGER*8 :: xbprefac

      FN=1
      TN=MaxTrm1b

      do jj =  FromN,ToN,-1
         lix(ix) = jj
         if (ix .eq. depth) then
            nvecin(1)=Npar+Morb-1-lix(1)
            do k=2,MORB-1
               nvecin(k)=lix(k-1)-lix(k)-1
            end do
            nvecin(MORB)=lix(MORB-1)-1
            JVEC(1)=NPAR+MORB-1-(nvecin(1)-Sh_m(1)+Sh_p(1))
            do k=2, MORB-1
               JVEC(k)=JVEC(k-1)-1-(nvecin(k)-Sh_m(k)+Sh_p(k))
            end do
            IND1=1
            IND2=1

            do k=1,MORB-1
               IND1=IND1+MatrixOfBinomialCoefficients(lix(k)-1,MORB-k)  
               IND2=IND2+MatrixOfBinomialCoefficients(JVEC(k)-1,MORB-k) 
            end do
!            Write(6,*) "IND1,IND2,NConf",IND1,IND2,NConf
            nvecout=nvecin
            xbprefac=1.d0
            xbprefac=xbprefac*nvecout(cK)
            nvecout(cK)=nvecout(cK)-1 
            nvecout(cJ)=nvecout(cJ)+1
            xbprefac=xbprefac*nvecout(cJ)

            RIJ(I_current_term)=RIJ(I_current_term) &
               +Conjg(VIN(IND2))*VIN(IND1)*SQRT(xbprefac*1.d0)
         else
            FromN= lix(ix)-1-Sh_m(ix+1)
            ToN=Morb-(ix+1)
         if(ix+1==depth) ToN=Morb-ix-1 +Sh_m(ix+2)        
            CALL loopy_core_ReducedElements(depth,FromN,ToN,lix, &
                       ix+1,Sh_m,Sh_p,Morb, &
                       NPAR,nconf, &
                       CJ,CK,I_current_term,I_TERM, &
                       RDIM,VIN,RIJ)
         end if
      end do
end SUBROUTINE loopy_core_ReducedElements

!> @ingroup singleshots
SUBROUTINE Get_Reduced1bodyElements_Fermions(VIN,Nconf,Reduced_OneBodyElements,Npart)

   use Global_Parameters,only:Morb
   USE Matrix_Elements
   USE Coefficients_Parameters,ONLY:MatrixOfBinomialCoefficients
   USE omp_lib
   use addresses_fermions

   implicit NONE

   integer :: i, j, k, distance, P, NPart, NConf
   integer :: I_term, cK, cQ  

   INTEGER :: IND1, IND2

   REAL*8 :: xbprefac

   COMPLEX*16 :: VIN(NCONF)
   COMPLEX*16 :: Reduced_OneBodyElements(RDIM)
   COMPLEX*16 :: zdir

   INTEGER ::  FromN, TillN
   INTEGER ::  Nvecin_Fermions(Morb)
   INTEGER ::  Nvecout_Fermions(Morb)

   FromN  = 1
   TillN  = MaxTrm1b

   Reduced_OneBodyElements = ZERO

!   Write(6,*) '#######################'

   DO I = FromN, TillN

     I_term = TERM_REQ_1B(I)
     P = TERM_INDEX_1B(I_term)
     Reduced_OneBodyElements(I_term) = 0.d0
     cQ = INT(P/100)
     cK = P-cQ*100

!     write(6,*) 'I=', I, 'cQ=', cQ, 'cK=', cK
 
     DO J = 1, NCONF

       CALL Get_ConfigurationFromIndex_Fermions(J,Npart,Morb,Nvecin_Fermions)

!       write(6,*) 'J=', J, 'conf ini =', Nvecin_Fermions

       xbprefac = 1.d0
       Nvecout_Fermions = Nvecin_Fermions

       IF (Nvecin_Fermions(CQ).eq.1) THEN
         CALL Get_Configuration_Distance(distance,Nvecout_Fermions,1,CQ)

         xbprefac = xbprefac*((-1.d0)**distance)
         Nvecout_Fermions(CQ) = Nvecout_Fermions(CQ)-1
       Else
         xbprefac = 0.d0
         IND2     = 2
       Endif

       IF (Nvecout_Fermions(CK).eq.0) THEN
         CALL Get_Configuration_Distance(distance,Nvecout_Fermions,1,CK)

         xbprefac = xbprefac*((-1.d0)**distance)
         Nvecout_Fermions(CK) = Nvecout_Fermions(CK)+1
       Else
         xbprefac = 0.d0
         IND2     = 2
       Endif

       IND1 = J
               
       IF (abs(xbprefac).ge.0.00000001d0) THEN
         CALL Get_IndexFromConfiguration_Fermions(Npart,Morb,Nvecout_Fermions,IND2)

         zdir = VIN(IND1)*xbprefac
         Reduced_OneBodyElements(I_term) = Reduced_OneBodyElements(I_term) &
                                         & + Conjg(VIN(IND2))*zdir
!       write(6,*) 'J=', J, 'conf end =', Nvecout_Fermions
       ENDIF

     ENDDO
   EndDO

!   Write(6,*) '#######################'
end SUBROUTINE Get_Reduced1bodyElements_Fermions




!> @ingroup singleshots
!> @brief Hanldes mapping of incoming coefficients of a state of N particles of to reduced coefficients
!> of a state of N-k particles.
!>
!> From \f$ C_{\vec n}^{(k-1)} (t) \f$ to \f$ C_{\vec n}^{(k)} (t) \f$
!>
!> @param[in] VIN : 
!> @param[in] PSI : sample of orbitals
!> @param[in] Npar :
!> @param[in] Nconf :
!> @param[in] NConf_Red : 
!> @param [out] VRED : reduced coefficients
SUBROUTINE Get_Reduced_Coefficients(VIN,VRED,PSI,Npar,Nconf,NConf_Red)

USE Function_Library
Use Global_Parameters, only: DIM_MCTDH,MOrb
USE Addresses

Implicit none
INTEGER, INTENT(IN) :: NPar,Nconf,NConf_Red
COMPLEX*16, INTENT(IN) :: PSI(MORB) ! sample of orbitals
COMPLEX*16, INTENT(IN) :: VIN(Nconf)
COMPLEX*16, INTENT(OUT) :: VRED(NConf_Red)

INTEGER :: I,J,IndJ, Ivec(Morb), Nvec(Morb), Jvec(Morb)

IF (Morb.eq.1) THEN
   VRED = Dcmplx(1.d0,0.d0)
   RETURN
ENDIF

VRED = dcmplx(0.d0,0.d0)

DO I = 1, NConf_Red

   !> define NVec to be the configuration vector of conf. I
   Call Get_ConfigurationFromIndex(I,NPar,Morb,Ivec,Nvec)
   Jvec=Nvec

   DO J = 1, Morb

      !> add a particle to orbital J of JVec
      Jvec(J)=Jvec(J)+1

      !> compute the index IndJ of configuration JVec
      CALL Get_IndexFromConfiguration(Npar+1,Morb,Jvec,IndJ)

      !> act field annihilation operator at position PSI(J) to obtain new coefficient vector
      VRED(I)=VRED(I)+PSI(J)*VIN(IndJ)*sqrt(dble(Jvec(J)))

      !> restore original 
      Jvec(J)=Jvec(J)-1

   ENDDO

ENDDO

end SUBROUTINE Get_Reduced_Coefficients


!> @ingroup singleshots
SUBROUTINE Get_Reduced_Coefficients_Fermions(VIN,VRED,PSI,Npar,Nconf,NConf_Red)

USE Function_Library
Use Global_Parameters, only: DIM_MCTDH,MOrb
use addresses_fermions

Implicit none
INTEGER, INTENT(IN) :: NPar,Nconf,NConf_Red
COMPLEX*16, INTENT(IN) :: PSI(MORB) ! sample of orbitals
COMPLEX*16, INTENT(IN) :: VIN(Nconf)
COMPLEX*16, INTENT(OUT) :: VRED(NConf_Red)

INTEGER :: I,J,IndJ, dist, Nvec(Morb), Jvec(Morb)


! write(6,*) 'IN Get_Reduced_Coefficients_Fermions' 
! write(6,*) size(VIN), size(VRED), size(PSI)
! write(6,*) Npar,Nconf,NConf_Red
! write(6,*) 'IN Get_Reduced_Coefficients_Fermions' 


IF (Morb.eq.1) THEN
   VRED = Dcmplx(1.d0,0.d0)
   RETURN
ENDIF

VRED = dcmplx(0.d0,0.d0)

DO I = 1, NConf_Red

!   write(*,*) 'I =', I
   Call Get_ConfigurationFromIndex_Fermions(I,NPar,Morb,Nvec)
   Jvec = Nvec
!   write(*,*) 'Jvec', Jvec,'Nvec', Nvec

   DO J = 1, Morb

      if (Jvec(J) .eq. 0) then

!      write(*,*) 'J=',J
         Jvec(J) = Jvec(J) + 1
!      write(*,*) 'Jvec(J)+1=',Jvec
         CALL Get_IndexFromConfiguration_Fermions(Npar+1,Morb,Jvec,IndJ)
!      write(*,*) 'index =',IndJ
         CALL Get_Configuration_Distance(dist,Jvec,1,J)
!      write(*,*) 'Coeff =', VIN(IndJ), 'fac =',(-1d0)**dist
!      write(*,*) 'Phi(rk) =', PSI(J)
         VRED(I) = VRED(I)+PSI(J)*VIN(IndJ)*(-1d0)**dist
!         VRED(I) = VRED(I)+VIN(IndJ)*(-1d0)**dist
         Jvec(J) = Jvec(J)-1

      endif

   ENDDO

ENDDO

!write(6,*) real(VRED)

end SUBROUTINE Get_Reduced_Coefficients_Fermions


!> @ingroup singleshots
SUBROUTINE Convolute_SingleShot(Shot,SampleIndexes)

USE Coefficients_Parameters,ONLY: NPar
USE Global_Parameters,ONLY:NDVR_X,NDVR_Y,NDVR_Z,DIM_MCTDH,PI
USE Analysis_Input_Variables, ONLY: Convolution_Width

IMPLICIT NONE

REAL*8  :: Shot(NDVR_X*NDVR_Y*NDVR_Z)
REAL*8  :: Shot2D(NDVR_X,NDVR_Y)
REAL*8  :: Shot3D(NDVR_X,NDVR_Y,NDVR_Z)
INTEGER*8 :: SampleIndexes(Npar)

INTEGER*8 :: I,J,K,L,i_s,j_s,k_s,sigma

shot=0.d0
IF (DIM_MCTDH.eq.2) THEN
  Shot2D=0.d0
ELSEIF (DIM_MCTDH.eq.3) THEN
  Shot3D=0.d0
ENDIF

sigma = Convolution_Width 

IF (DIM_MCTDH.eq.3) THEN

   DO L=1,NPar

     CALL get_ijk_from_m(SampleIndexes(L),NDVR_X,NDVR_Y,i_s,j_s,k_S)

     DO I=1,NDVR_X
       DO J=1,NDVR_Y
         DO K=1,NDVR_Z
         
            Shot3D(I,J,K)=Shot3D(I,J,K)+((DSQRT(2.d0*pi*sigma**2))**3)*  &
                          DEXP(-1.d0*((I-I_s)**2+(J-J_s)**2+(K-k_s)**2)/(2.d0*sigma**2))

         END DO
       END DO
     END DO
   END DO
   Shot=Reshape(Shot3d,(/NDVR_X*NDVR_Y*NDVR_Z/))

ELSEIF (DIM_MCTDH.eq.2) THEN
 
   DO L=1,NPar

     CALL get_ijk_from_m(SampleIndexes(L),NDVR_X,NDVR_Y,i_s,j_s,k_S)

     DO I=1,NDVR_X
       DO J=1,NDVR_Y
         
            Shot2D(I,J)=Shot2D(I,J)+((DSQRT(2.d0*pi*sigma**2))**2)* &
                        DEXP(-1.d0*((I-I_s)**2+(J-J_s)**2)/(2.d0*sigma**2))

       END DO
     END DO
   END DO  
   Shot=Reshape(Shot2d,(/NDVR_X*NDVR_Y/))

ELSEIF (DIM_MCTDH.eq.1) THEN
 
   DO L=1,NPar

     CALL get_ijk_from_m(SampleIndexes(L),NDVR_X,NDVR_Y,i_s,j_s,k_S)

     DO I=1,NDVR_X
         
            Shot(I)=Shot(I)+((DSQRT(2.d0*pi*sigma**2)))*      &
                    DEXP(-1.d0*((I-I_s)**2)/(2.d0*sigma**2))

     END DO
   END DO  

ENDIF

end SUBROUTINE Convolute_SingleShot

SUBROUTINE Get_Reduced_Density(PSI,rho_jk_compact,Density)

USE Global_Parameters
USE Matrix_Elements,only:MaxTrm1b,Term_Index_1B,RDIM
USE DVR_Parameters, only: ORT_X,ORT_Y,ORT_Z

IMPLICIT NONE

COMPLEX*16, intent(IN), DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb)  :: PSI
COMPLEX*16, intent(IN), DIMENSION(RDIM) :: Rho_jk_compact
complex*16 :: rho_jk(Morb,Morb)

REAL*8, INTENT(OUT) :: DENSITY(NDVR_X*NDVR_Y*NDVR_Z)

INTEGER :: I,J,K,M,NOGRIDPOINTS,CK,CJ,P,icntr,Jorb,Korb,Iorb

NOGRIDPOINTS=NDVR_X*NDVR_Y*NDVR_Z

Do I=1,MaxTrm1b
   P=TERM_INDEX_1B(I)
   cK= INT(P/100)
   cJ= P-cK*100
   Rho_JK(cJ,cK)=Rho_jk_compact(I) 
   IF(cK.ne.cJ) Rho_JK(cK,cJ)=Conjg(Rho_jk_compact(I))
EnDdo

icntr=1
DO Iorb=1,Morb
   DO Jorb=Iorb,Morb
      Rho_JK(Iorb,Jorb)=Rho_jk_compact(icntr)
      IF(Jorb.ne.Iorb) Rho_JK(Jorb,Iorb)=Conjg(Rho_jk_compact(icntr))
      icntr=icntr+1
   EndDO
EndDO

Density=0.d0
do Jorb = 1,Morb  
   do Korb=1,Morb

      Density(:) = Density(:)+rho_jk(Jorb,Korb) * DCONJG(PSI(:,Jorb))*PSI(:,Korb)

   end do
end do

end SUBROUTINE Get_Reduced_Density

!> @ingroup singleshots
!> @brief Samples particle grid points from a given density and stores it in SampleIndex.
!>
!> The routine first calculated the reduced one-body density for the given number of already drawn particles and calculates
!> \f$ \rho(\mathbf x) = \langle \Psi | \hat \Psi^\dagger (\mathbf x) \hat \Psi (\mathbf x| \Psi \rangle = \sum_{jk} \rho_{jk} \phi_j^* (\mathbf x) \phi_k (\mathbf x)\f$
!>
!>
!> @param[in] PSI :the orbitals
!> @param[in] rho_jk_compact :the matrix elements of the one-body density
!> @param[out] sample : grid point values
!> @param[in]  SampleIndex : saves sampled grid points for all the particles
!>
!> @return
!> Returns
SUBROUTINE DrawFromDensity(PSI,rho_jk_compact,sample,SampleIndex)

USE Global_Parameters
USE Random
USE Matrix_Elements,only: MaxTrm1b,RDIM
USE DVR_Parameters, only: ORT_X,ORT_Y,ORT_Z

IMPLICIT NONE

COMPLEX*16, intent(IN), DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb)  :: PSI

complex*16 :: rho_jk(Morb,Morb)
Complex*16 ::  G1n_n, CumulativeG1n_n
REAL*8  :: Norm_CumulativeG1n_n
COMPLEX*16, intent(IN) :: rho_jk_compact(Morb*(Morb+1)/2) ! storage format of density matrix: half triangular

REAL*8,INTENT(OUT) :: sample(DIM_MCTDH)
REAL*8 :: RandomU,delta1,delta2
REAL*8 Density(NDVR_X,NDVR_Y,NDVR_Z)

INTEGER*8,INTENT(OUT) :: SampleIndex
INTEGER*8 :: I,J,K,M,NOGRIDPOINTS,n,i_n,j_n,k_n
INTEGER :: CK,CJ,P,icntr,Korb,Jorb,Iorb

cumulativeG1n_n = (0.d0,0.d0)
NOGRIDPOINTS=NDVR_X*NDVR_Y*NDVR_Z

! convert matrix element from storage form to usual matrix format
icntr=1
DO Iorb=1,Morb
   DO Jorb=Iorb,Morb
      Rho_JK(Iorb,Jorb)=Rho_jk_compact(icntr)
      IF(Jorb.ne.Iorb) Rho_JK(Jorb,Iorb)=Conjg(Rho_jk_compact(icntr))
      icntr=icntr+1
   EndDO
EndDO

Density=0.d0

DO n=1,NOGRIDPOINTS 

  CALL get_ijk_from_m(n,NDVR_X,NDVR_Y,i,j,k)
  do Jorb = 1,Morb  
    do Korb=1,Morb

      Density(i,j,k)= Density(i,j,k) + rho_jk(Jorb,Korb) * DCONJG(PSI(n,Jorb))*PSI(n,Korb)

    end do
  end do
END DO

IF (DIM_MCTDH.eq.1) THEN

CALL Draw_sample_1D(sampleindex,Density(:,1,1),NDVR_X)

sample(1)=sampleindex

return

ELSEIF (DIM_MCTDH.eq.2) THEN

CALL Draw_sample_2D(sampleindex,Density(:,:,1),NDVR_X,NDVR_Y)
CALL get_ijk_from_m(sampleindex,NDVR_X,NDVR_Y,i,j,k)

sample(1)=i
sample(2)=j

return

ELSEIF (DIM_MCTDH.eq.3) THEN

CALL Draw_sample_3D(sampleindex,Density,NDVR_X,NDVR_Y,NDVR_Z)
CALL get_ijk_from_m(sampleindex,NDVR_X,NDVR_Y,i,j,k)

sample(1)=i
sample(2)=j
sample(3)=k

return

ENDIF

end SUBROUTINE DrawFromDensity

!> @ingroup singleshots
!>
!> @brief Draw a the position of an atom using the one-body density for the reduced system, i.e. N-k atoms
!>
!> Draw \f$\vec r_k\f$ from \f$P(\vec r_k | \vec r_{k-1},...,\vec r_1) \propto \rho_k(\vec r)\f$
!> by **first** generating a random number between 0 and 1 and **then** using inverse transform sampling 
!> to go from the uniform distribution to the desired distribution.
!>
!>
!> @param[in] NG : number of grid points (often: NDVR_X)
!> @param[in] Density : previously calculated one-body density for the reduced system
!>                      \f$\rho(x)=\sum_{i,j}\rho_{ij}*\phi_{i}^{*}(x)*\phi_{j}(x)\f$
!> @param[out] sample : grid point values
!> @return
!> Returns samples from the density distribution.
SUBROUTINE Draw_sample_1D(sample,Density,NG)

USE Random
IMPLICIT NONE

INTEGER*8, INTENT(IN) :: NG 
INTEGER*8, INTENT(OUT) :: sample
REAL*8, INTENT(IN) :: Density(NG)
REAL*8 :: ORT_X(NG)

REAL*8 cumulativeG1n_np,cumulativeG1n_n,Norm_cumulativeG1n_n
REAL*8 diff_n,diff_np

REAL*8 RandomU
INTEGER :: N

! returns a random number between 0 and 1
CALL gnu_rand_Burkardt(RandomU)

! compute the norm to later have a normed density (between zero and one)
Norm_cumulativeG1n_n = 0.d0
Norm_cumulativeG1n_n = Norm_cumulativeG1n_n + SUM(Density(:))

!compute G1n_n
cumulativeG1n_n  = (0.d0,0.d0)

! cumulative way of generating a random number for a given density distribution (inverse transform sampling)
do n = 1, NG
  
   cumulativeG1n_n = cumulativeG1n_n + Density(n)

   if (dble(cumulativeG1n_n)/Norm_cumulativeG1n_n .ge. RandomU) then

         sample=n

         exit

   endif

 END DO
END SUBROUTINE Draw_sample_1D


!> @ingroup singleshots
!>
!> @brief Draw a the position of an atom using the one-body density for the reduced system, i.e. N-k atoms
!>
!> Draw \f$\vec r_k\f$ from \f$P(\vec r_k | \vec r_{k-1},...,\vec r_1) \propto \rho_k(\vec r)\f$
!> by **first** generating a random number between 0 and 1 and **then** using inverse transform sampling 
!> to go from the uniform distribution to the desired distribution.
!>
!>
!> @param[in] NG_X : number of grid points in X direction (often: NDVR_X)
!> @param[in] NG_Y : number of grid points in Y direction (often: NDVR_Y)
!> @param[in] Density : previously calculated one-body density for the reduced system
!>                      \f$\rho(x,y)=\sum_{i,j}\rho_{ij}*\phi_{i}^{*}(x,y)*\phi_{j}(x,y)\f$
!> @param[out] sample : grid point values
!> @return
!> Returns samples from the density distribution.
SUBROUTINE Draw_sample_2D(sample,Density,NG_X,NG_Y)

USE Random

IMPLICIT NONE

INTEGER*8 :: NG_X,NG_Y,sample, sampleX,sampleY
REAL*8, INTENT(IN) :: Density(NG_X,NG_Y)
REAL*8 :: Conditional_Y2D(NG_Y)
REAL*8 :: ORT_X(NG_X), ORT_Y(NG_Y)

REAL*8 cumulativeG1n_np,cumulativeG1n_n,Norm_cumulativeG1n_n
REAL*8 diff_n,diff_np

REAL*8 RandomU
INTEGER :: N,NG


! returns a random number between 0 and 1
CALL gnu_rand_Burkardt( RandomU )

! compute the norm to later have a normed density (between zero and one)
Norm_cumulativeG1n_n = 0.d0
Norm_cumulativeG1n_n = Norm_cumulativeG1n_n + SUM(Density(:,:))

cumulativeG1n_n  = (0.d0,0.d0)
cumulativeG1n_np = (0.d0,0.d0)

! we create density 1D int_{y} rho_{x,y} dy -> rho_1D(x)

! draw X
DO n = 1, NG_X
! draw X
  
   cumulativeG1n_np = cumulativeG1n_n

   cumulativeG1n_n = cumulativeG1n_n + SUM(Density(n,:))

   IF (dble(cumulativeG1n_n)/Norm_cumulativeG1n_n .gt. RandomU) then

         sampleX=n

         exit

   ENDIF

END DO

CALL gnu_rand_Burkardt( RandomU )

cumulativeG1n_n = dcmplx(0.d0,0.d0)
conditional_y2d(:)=Density(SampleX,:)/SUM(Density(SampleX,:))

! draw Y
 do n=1,NG_Y
! draw Y
   cumulativeG1n_n = cumulativeG1n_n + conditional_y2d(n)

   IF (dble(cumulativeG1n_n).ge.RandomU) THEN

      SampleY=n
      Sample=sampleX+NG_X*(sampleY-1)         

      EXIT

   ENDIF

 END DO


End SUBROUTINE Draw_sample_2D



!> @ingroup singleshots
SUBROUTINE Draw_sample_3D(sample,Density,NG_X,NG_Y,NG_Z)

USE Random

IMPLICIT NONE

INTEGER*8 :: NG_X,NG_Y,NG_Z,sample, sampleX,sampleY,sampleZ
REAL*8 :: Density(NG_X,NG_Y,NG_Z), Conditional_Y3D(NG_Y,NG_Z), Conditional_Z3D(NG_Z)
!REAL*8 :: ORT_X(NG_X), ORT_Y(NG_Y), Ort_Z(NG_Z)

REAL*8 cumulativeG1n_np,cumulativeG1n_n,Norm_cumulativeG1n_n
REAL*8 diff_n,diff_np

REAL*8 RandomU
INTEGER :: N,NG

NG=NG_X*NG_Y

CALL gnu_rand_Burkardt( RandomU )

Norm_cumulativeG1n_n = 0.d0
Norm_cumulativeG1n_n = Norm_cumulativeG1n_n + SUM(Density(:,:,:))

cumulativeG1n_n  = (0.d0,0.d0)
cumulativeG1n_np = (0.d0,0.d0)

! draw X
do n = 1, NG_X
! draw X
  
   cumulativeG1n_np = cumulativeG1n_n

   cumulativeG1n_n = cumulativeG1n_n + SUM(Density(n,:,:))

   if (dble(cumulativeG1n_n)/Norm_cumulativeG1n_n .gt. RandomU) then

         sampleX=n

         exit

   endif

END DO

CALL gnu_rand_Burkardt( RandomU )

cumulativeG1n_n = dcmplx(0.d0,0.d0)
conditional_y3d(:,:)=Density(SampleX,:,:)/SUM(Density(SampleX,:,:))

! draw Y
do n=1,NG_Y
! draw Y
   cumulativeG1n_n = cumulativeG1n_n +  SUM(conditional_y3d(n,:))

   IF (dble(cumulativeG1n_n).ge.RandomU) THEN

      SampleY=n

      EXIT

   ENDIF

END DO

CALL gnu_rand_Burkardt( RandomU )

cumulativeG1n_n = dcmplx(0.d0,0.d0)
conditional_z3d(:)=Density(SampleX,SampleY,:)/SUM(Density(SampleX,SampleY,:))

! draw Z
 do n=1,NG_Z
! draw Z
   cumulativeG1n_n = cumulativeG1n_n +  conditional_z3d(n)

   IF (dble(cumulativeG1n_n).ge.RandomU) THEN

      SampleZ=n
      Sample=sampleX+NG_X*(sampleY-1)+NG_X*NG_Y*(sampleZ-1)         

      EXIT

   ENDIF

! draw Z
 END DO
! draw Z

End SUBROUTINE Draw_sample_3D








  SUBROUTINE Get_WeightOrMomentumGrid(mom_x,mom_y,mom_z,w,dilation,mode)

  Use Global_Parameters
  Use DVR_Parameters
  Use Auxiliary_Routines
  USE Interaction_Parameters

  IMPLICIT NONE

  INTEGER :: Dilation, Mode
  REAL*8 :: w,dk,mom_X(NDVR_X),mom_Y(NDVR_Y),mom_Z(NDVR_Z)
  real*8 :: xi,xf,yi,yf,zi,zf
  INTEGER :: NOGRIDPOINTS



   NOGRIDPOINTS=NDVR_X*NDVR_Y*NDVR_Z

  if(mode.eq.1) then

     xi=X_initial
     xf=X_final
     yi=Y_initial
     yf=Y_final
     zi=z_initial
     zf=z_final

     w=DSQRT((xf-xi)/NDVR_X)
     if (DIM_MCTDH.ge.2) then
       w=w*DSQRT((yf-yi)/NDVR_Y)
     else if (DIM_MCTDH.eq.3) then
       w=w*DSQRT((zf-zi)/NDVR_z)
     endif

  else if (mode.eq.2) then ! when Get_Sorted_FFT was used

     ! apply dilation to grid
     xi=X_initial*dilation
     xf=X_final*dilation
     yi=Y_initial*dilation
     yf=Y_final*dilation
     zi=z_initial*dilation
     zf=z_final*dilation


     CALL get_mom(xi,xf,NDVR_X,mom_X,dilation=1) !returns momentum grid in strictly ascending order
     dk     = (mom_X(2)-mom_X(1))!/dilation 
     
     if (DIM_MCTDH.ge.2) then
       CALL get_mom(yi,yf,NDVR_Y,mom_Y,dilation=1) !returns momentum grid in strictly ascending order
       dk     = (mom_Y(2)-mom_y(1))*dk
     endif

     if (DIM_MCTDH.ge.3) then
       CALL get_mom(zi,zf,NDVR_Z,mom_Z,dilation=1) !returns momentum grid in strictly ascending order
       dk     = (mom_Z(2)-mom_Z(1))*dk!/(dilation**2)
     endif

     w      = DSQRT( dk )

  else 

        write(*,*) "wrong mode"

  end if

  end SUBROUTINE Get_WeightOrMomentumGrid

!> This routine puts together output filenames from some system parameters.
  SUBROUTINE Assemble_OutputFileName(FullName,Time,NParticles,Morbitals,KorX,Suffix)

  Use Auxiliary_Routines

  IMPLICIT NONE

  CHARACTER(*) :: FullName
  CHARACTER(*) :: KorX,Suffix

  REAL*8 :: Time
  INTEGER :: NParticles,Morbitals

  character(len=10)  :: timeAsString
  character(len=10)  :: MorbAsString
 
  character(len=1)   :: NparAsString1
  character(len=2)   :: NparAsString2
  character(len=3)   :: NparAsString3
  character(len=4)   :: NparAsString4
  character(len=5)   :: NparAsString5
  character(len=6)   :: NparAsString6
  character(len=7)   :: NparAsString7
  character(len=8)   :: NparAsString8



  if (MORB.lt.10) then
     write (MorbAsString, '(I1)') Morb
  else if ((MORB.ge.10).and.(MORB.lt.100)) then
     write (MorbAsString, '(I2)') Morb
  else 
     write(*,*) 'Bigger orbital number than 100 not implemented!'
  endif

  CALL getDoubleAsString(time,timeAsString) 

   
  if ((NParticles.gt.0) .and. (NParticles.lt.10)) then 
    write (NParAsString1, '(I1)') NParticles
    fullName=timeasString//'N'//trim(NParAsString1)//'M'//trim(MorbAsString)//trim(KorX)//trim(Suffix)
  else  if ((NParticles.ge.10) .and. (NParticles.lt.100)) then                                      
    write (NParAsString2, '(I2)') NParticles                                                  
    fullName=timeasString//'N'//trim(NParAsString2)//'M'//trim(MorbAsString)//trim(KorX)//trim(Suffix)
  else if ((NParticles.ge.100) .and. (NParticles.lt.1000)) then                                     
    write (NParAsString3, '(I3)') NParticles                                                  
    fullName=timeasString//'N'//trim(NParAsString3)//'M'//trim(MorbAsString)//trim(KorX)//trim(Suffix)
  else if ((NParticles.ge.1000) .and. (NParticles.lt.10000)) then                                   
    write (NParAsString4, '(I4)') NParticles                                                  
    fullName=timeasString//'N'//trim(NParAsString4)//'M'//trim(MorbAsString)//trim(KorX)//trim(Suffix)
  else if ((NParticles.ge.10000) .and. (NParticles.lt.100000)) then                                 
    write (NParAsString5, '(I5)') NParticles                                                  
    fullName=timeasString//'N'//trim(NParAsString5)//'M'//trim(MorbAsString)//trim(KorX)//trim(Suffix)
  else if ((NParticles.ge.100000) .and. (NParticles.lt.1000000)) then                               
    write (NParAsString6, '(I6)') NParticles                                                  
    fullName=timeasString//'N'//trim(NParAsString6)//'M'//trim(MorbAsString)//trim(KorX)//trim(Suffix)
  else if ((NParticles.ge.1000000) .and. (NParticles.lt.10000000)) then                             
    write (NParAsString7, '(I7)') NParticles                                                  
    fullName=timeasString//'N'//trim(NParAsString7)//'M'//trim(MorbAsString)//trim(KorX)//trim(Suffix)
  else if ((NParticles.ge.10000000) .and. (NParticles.lt.100000000)) then                           
    write (NParAsString8, '(I8)') NParticles                                                  
    fullName=timeasString//'N'//trim(NParAsString8)//'M'//trim(MorbAsString)//trim(KorX)//trim(Suffix)
  else                                                                                  
    NParAsString4='XXXX'                                                                
    fullName=timeasString//'N'//trim(NParAsString4)//'M'//trim(MorbAsString)//trim(KorX)//trim(Suffix)
  end if


  end SUBROUTINE Assemble_OutputFileName



!> This routine computes the full one-body matrix elements from the compact upper triangular storage.
  SUBROUTINE Get_Full_Rij(Rho_JK)
  USE   Global_Parameters
  USE   Matrix_Elements
  USE   Coefficients_Parameters
  USE   Analysis_Input_Variables
  USE   Interaction_Parameters
  USE   DVR_Parameters
  USE   Orbital_Parallelization_Parameters
  integer :: i,P,cK,cJ,icntr,Iorb,Jorb
  complex*16 :: rho_jk(Morb,Morb)
!c================================================================
   Do I=1,MaxTrm1b
      P=TERM_INDEX_1B(I)
      cK= INT(P/100)
      cJ= P-cK*100
      Rho_JK(cJ,cK)=Rho1_Elements(I) 
      IF(cK.ne.cJ) Rho_JK(cK,cJ)=Conjg(Rho1_Elements(I))
   EnDdo
   
   icntr=1
   DO Iorb=1,Morb
      DO Jorb=Iorb,Morb
         Rho_JK(Iorb,Jorb)=Rho1_Elements(icntr)
         IF(Jorb.ne.Iorb) Rho_JK(Jorb,Iorb)=Conjg(Rho1_Elements(icntr))
         icntr=icntr+1
      EndDO
   EndDO
 end SUBROUTINE Get_Full_Rij

!c==================================================================
!> This routine computes the full two-body matrix elements from the compact upper triangular storage.
  SUBROUTINE Get_Full_Rijkl(Rho_ijkl)

  USE   Global_Parameters
  USE   Matrix_Elements
  USE   Coefficients_Parameters
  USE   Analysis_Input_Variables
  USE   Interaction_Parameters
  USE   DVR_Parameters
  USE   Orbital_Parallelization_Parameters

  integer :: i,P,cK,cS,cL,cQ
  complex*16 :: rho_ijkl(Morb,Morb,Morb,Morb)

!c================================================================

  SELECT CASE (Job_Type)    
    CASE ('BOS','FCI')
      Do I=1,MaxTrm2b
         P=TERM_INDEX_2B(I)
         cQ= INT(P/1000000)
         cL= INT((P-cQ*1000000)/10000)
         cS= INT((P-cQ*1000000-cL*10000)/100)
         cK= P-cQ*1000000-cL*10000-cS*100
         Rho_ijkl(cK,cS,cL,cQ)=Rho2_Elements(I) 
         Rho_ijkl(cK,cS,cQ,cL)=Rho2_Elements(I) 
         Rho_ijkl(cS,cK,cL,cQ)=Rho2_Elements(I) 
         Rho_ijkl(cS,cK,cQ,cL)=Rho2_Elements(I) 
    
         IF(cK.ne.cL) then 
           Rho_ijkl(cL,cQ,cK,cS)=Conjg(Rho2_Elements(I))
           Rho_ijkl(cL,cQ,cS,cK)=Conjg(Rho2_Elements(I))
           Rho_ijkl(cQ,cL,cK,cS)=Conjg(Rho2_Elements(I))
           Rho_ijkl(cQ,cL,cS,cK)=Conjg(Rho2_Elements(I))
         elseif(cS.ne.cQ) then 
           rho_ijkl(cL,cQ,cK,cS)=Conjg(Rho2_Elements(I))
           rho_ijkl(cL,cQ,cS,cK)=Conjg(Rho2_Elements(I))
           Rho_ijkl(cQ,cL,cK,cS)=Conjg(Rho2_Elements(I))
           Rho_ijkl(cQ,cL,cS,cK)=Conjg(Rho2_Elements(I))
         endif
      EndDo

    CASE ('FER')

      WRITE(6,*) 'Job_Type = ', Job_Type
      WRITE(6,*) 'I   K   S   L   Q   Rho2_Elements(I)'
   
      Do I=1,MaxTrm2b
         P=TERM_INDEX_2B(I)
         cQ= INT(P/1000000)
         cL= INT((P-cQ*1000000)/10000)
         cS= INT((P-cQ*1000000-cL*10000)/100)
         cK= P-cQ*1000000-cL*10000-cS*100

         IF ((ABS(Rho2_Elements(I))).ne.(0.0)) THEN 
           WRITE(6,*) I,cK,cS,cL,cQ,Rho2_Elements(I)
         ENDIF

         Rho_ijkl(cK,cS,cL,cQ)=Rho2_Elements(I) 
         Rho_ijkl(cK,cS,cQ,cL)= - Rho2_Elements(I) 
         Rho_ijkl(cS,cK,cL,cQ)= - Rho2_Elements(I) 
         Rho_ijkl(cS,cK,cQ,cL)=Rho2_Elements(I) 
    
         IF(cK.ne.cL) then 
           Rho_ijkl(cL,cQ,cK,cS)=Conjg(Rho2_Elements(I))
           Rho_ijkl(cL,cQ,cS,cK)= - Conjg(Rho2_Elements(I))
           Rho_ijkl(cQ,cL,cK,cS)= - Conjg(Rho2_Elements(I))
           Rho_ijkl(cQ,cL,cS,cK)=Conjg(Rho2_Elements(I))
         elseif(cS.ne.cQ) then 
           rho_ijkl(cL,cQ,cK,cS)=Conjg(Rho2_Elements(I))
           rho_ijkl(cL,cQ,cS,cK)= - Conjg(Rho2_Elements(I))
           Rho_ijkl(cQ,cL,cK,cS)= - Conjg(Rho2_Elements(I))
           Rho_ijkl(cQ,cL,cS,cK)=Conjg(Rho2_Elements(I))
         endif
      EndDo

    END SELECT

 end SUBROUTINE Get_Full_Rijkl


!c==================================================================
!> This routine computes the full two-body matrix elements from the compact upper triangular storage.
  SUBROUTINE Get_Full_Wijkl(W_ijkl)

  USE   Global_Parameters
  USE   Matrix_Elements
  USE   Coefficients_Parameters
  USE   Analysis_Input_Variables
  USE   Interaction_Parameters
  USE   DVR_Parameters
  USE   Orbital_Parallelization_Parameters

  integer :: i,P,cK,cJ,cL,cI,Term_Index
  complex*16 :: W_ijkl(Morb,Morb,Morb,Morb)

!c================================================================
  Do I=1,MaxTrm2b

     P=TERM_INDEX_2B(I)
     Term_Index=Term_Req_2b(I)

     cL= INT(P/1000000)
     cK= INT((P-cL*1000000)/10000)
     cJ= INT((P-cL*1000000-cK*10000)/100)
     cI= P-cL*1000000-cK*10000-cJ*100

     W_ijkl(cI,cJ,cK,cL)=DREAL(RESCALE_2B(Term_Index))*Interaction_Elements(TERM_REQ_2B(I)) 
     W_ijkl(cI,cJ,cL,cK)=DREAL(RESCALE_2B(Term_Index))*Interaction_Elements(TERM_REQ_2B(I))
     W_ijkl(cJ,cI,cK,cL)=DREAL(RESCALE_2B(Term_Index))*Interaction_Elements(TERM_REQ_2B(I))
     W_ijkl(cJ,cI,cL,cK)=DREAL(RESCALE_2B(Term_Index))*Interaction_Elements(TERM_REQ_2B(I))

     IF(cI.ne.cK) then 
       W_ijkl(cK,cL,cI,cJ)=DIMAG(RESCALE_2B(Term_Index))*Conjg(Interaction_Elements(TERM_REQ_2B(I)))
       W_ijkl(cK,cL,cJ,cI)=DIMAG(RESCALE_2B(Term_Index))*Conjg(Interaction_Elements(TERM_REQ_2B(I)))
       W_ijkl(cL,cK,cI,cJ)=DIMAG(RESCALE_2B(Term_Index))*Conjg(Interaction_Elements(TERM_REQ_2B(I)))
       W_ijkl(cL,cK,cJ,cI)=DIMAG(RESCALE_2B(Term_Index))*Conjg(Interaction_Elements(TERM_REQ_2B(I)))
     elseif(cJ.ne.cL) then     
       W_ijkl(cK,cL,cI,cJ)=DIMAG(RESCALE_2B(Term_Index))*Conjg(Interaction_Elements(TERM_REQ_2B(I)))
       W_ijkl(cK,cL,cJ,cI)=DIMAG(RESCALE_2B(Term_Index))*Conjg(Interaction_Elements(TERM_REQ_2B(I)))
       W_ijkl(cL,cK,cI,cJ)=DIMAG(RESCALE_2B(Term_Index))*Conjg(Interaction_Elements(TERM_REQ_2B(I)))
       W_ijkl(cL,cK,cJ,cI)=DIMAG(RESCALE_2B(Term_Index))*Conjg(Interaction_Elements(TERM_REQ_2B(I)))
     endif
 EndDo

 end SUBROUTINE Get_Full_Wijkl




!> This routine computes the eigenvalues and eigenvectors of the reduced 
!> one-body density matrix elements.
        SUBROUTINE Diag_DNS(ALLZ1,natural_occupations,natural_vectors)

        use Global_Parameters
        USE Coefficients_Parameters

        implicit NONE

       COMPLEX*16, ALLOCATABLE :: Full_Rho1_Elements_Storage(:,:)
       COMPLEX*16, ALLOCATABLE :: Full_Rho1_Elements(:,:)
       COMPLEX*16, ALLOCATABLE :: Full_Rho1_Elements2(:,:)
       COMPLEX*16, ALLOCATABLE :: Full_Rho1_Elements3(:,:),WORK(:)
       COMPLEX*16,DIMENSION(MORB*(MORB+1)/2) :: AllZ1
       REAL*8, DIMENSION(MORB)             :: natural_occupations
       COMPLEX*16, DIMENSION(MORB,MORB)        :: natural_vectors
       REAL*8, ALLOCATABLE :: RWORK(:)

       INTEGER,  ALLOCATABLE :: ipiv(:)

!c================================== MPI ======================================
       INTEGER ::  ierr
!c=============================================================================
       INTEGER :: icntr,Nadr
       INTEGER :: Iorb,Jorb,info
       EXTERNAL DASUM,Nadr,ZGETRF,ZGETRI
!c=============================================================================
       allocate(Full_Rho1_Elements_Storage(Morb,Morb),stat=ierr)
       if(ierr /= 0)write(*,*)"allocation error in Get_Rho_Rho_inv"
       allocate(Full_Rho1_Elements(Morb,Morb),stat=ierr)
       if(ierr /= 0)write(*,*)"allocation error in Get_Rho_Rho_inv"
       allocate(WORK(Morb*Morb),stat=ierr)
       if(ierr /= 0)write(*,*)"allocation error in Get_Rho_Rho_inv"

       allocate(RWORK(3*Morb-2),stat=ierr)
       if(ierr /= 0)write(*,*)"allocation error in Get_Rho_Rho_inv"
       allocate(ipiv(Morb),stat=ierr)
       if(ierr /= 0)write(*,*)"allocation error in Get_Rho_Rho_inv"

!c=============================================================================
        Full_Rho1_Elements=Zero
        icntr=1
        DO Iorb=1,Morb
           DO Jorb=Iorb,Morb
              Full_Rho1_Elements(Iorb,Jorb)=AllZ1(icntr)
              IF(Jorb.ne.Iorb) Full_Rho1_Elements(Jorb,Iorb)=Conjg(ALLZ1(icntr)) 
              icntr=icntr+1
           EndDO
        EndDO
!c================== Natural Occupations and Natural Vectors ==================
        Full_Rho1_Elements_Storage=Full_Rho1_Elements
       CALL ZHEEV('V','U',Morb,Full_Rho1_Elements_Storage,Morb,natural_occupations,WORK,Morb*Morb,&
                                         RWORK,INFO)
       Natural_Vectors=Full_Rho1_Elements_Storage

         deallocate(Full_Rho1_Elements_Storage)
         deallocate(WORK,RWORK,ipiv)
         END  SUBROUTINE Diag_DNS


!>      Writes a real number in 4 digit format
!>      for naming files.
SUBROUTINE getsliceAsString(d,x,doubleString)
IMPLICIT NONE
integer       :: d
integer,parameter         :: DBL=8
real(kind=DBL)            :: x
CHARACTER(len=7)         :: doubleString
CHARACTER(len=2)          :: slc

if (d.eq.1) then 
    write(slc,'(A2)') 'c1'
elseif (d.eq.2) then 
    write(slc,'(A2)') 'c2'
elseif (d.eq.3)  then 
    write(slc,'(A2)') 'c3'
elseif (d.eq.4) then 
    write(slc,'(A2)') 'c4'
else 
  write(*,*) 'This slice cannot be right!'
  stop
endif

IF (x.ge.0.d0) then
    if (x .LE. 999.999999d0) WRITE(doubleString,'(A1,F4.0,A2)') 'P',x,slc
    if (x .LE. 99.9999999d0) WRITE(doubleString,'(A1,F4.1,A2)') 'P',x,slc
    if (x .LE. 9.99999999d0) WRITE(doubleString,'(A1,F4.2,A2)') 'P',x,slc
ELSE IF ((x.lt.0.d0).and.(abs(x).lt.1000.d0)) then
    if (abs(x) .LE. 999.999999d0) WRITE(doubleString,'(A1,F4.0,A2)') 'M',abs(x),slc
    if (abs(x) .LE. 99.9999999d0) WRITE(doubleString,'(A1,F4.1,A2)') 'M',abs(x),slc
    if (abs(x) .LE. 9.99999999d0) WRITE(doubleString,'(A1,F4.2,A2)') 'M',abs(x),slc
ELSE
  WRITE(6,*)"get slice as string: x not implemented!"
  WRITE(doubleString,'(A7)') 'XXXXXXX' 
END IF

end SUBROUTINE getsliceasString


!> This routine computes the k-space interval which is optimal to represent the orbitals in k-space.
SUBROUTINE get_dilation(dilation,Psi,rho_jk)
use DVR_Parameters
  implicit none  
  integer,intent(inout)     :: dilation
  COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb)  :: PSI
  complex*16,DIMENSION(Morb,Morb),intent(in) :: rho_jk

  integer                   :: m,iLower,iUpper
  integer                   :: Jorb,Korb
  complex*16                :: cmax,G1m_m
  real*8                    :: G1max,THRESHOLD,G1(NDVR_X*NDVR_Y*NDVR_Z)
  real*8                    :: imin,imax


  THRESHOLD =       0.001d0       
  dilation  =       1
  G1max     =       0.d0

  CALL get_maximum(PSI,rho_jk,cmax)
  G1max = DBLE(cmax)


do m=1,NDVR_X*NDVR_Y*NDVR_Z 

   G1m_m = (0.d0,0.d0) 
   do Jorb = 1,Morb  
      do Korb=1,Morb

         G1m_m = G1m_m + rho_jk(Jorb,Korb) * DCONJG(PSI(m,Jorb))*PSI(m,Korb)

      end do
   end do

   G1(m)=DBLE(G1m_m)   

end do 

!----------------
!----find boundaries

do m=1,NDVR_X/2
  if(G1(m)/G1max .GT. THRESHOLD) THEN
    iLower = m  
    iUpper = NDVR_X-m+1
    exit
  end if
  if(G1(NDVR_X-m+1)/G1max .GT. THRESHOLD) THEN
    iLower = m  
    iUpper = NDVR_X-m+1
    exit
  end if  
end do 

imax     = 1.d0*(iUpper-(NDVR_X)/2 - 1)
imin     = 1.d0*(iLower-(NDVR_X)/2 - 1)
imax     = MAX(DABS(imax),DABS(imin))    
dilation = MAX(INT(NDVR_X/2.d0 /(2.d0*imax)),1)

end  SUBROUTINE get_dilation

!> Wrapper routine that evaluates the action of the one-body operators (kinetic energy and angular momentum)
!> for the chosen DVR.
SUBROUTINE Get_KineticEnergyAction_AllOrbitalsAnalysis(temp,pot,time)

USE OneBodyPotential
use KineticEnergyAction 
use Global_Parameters
USE DVR_Parameters
USE Orbital_Parallelization_Parameters
USE Orbital_Allocatables, ONLY: VTRAP_EXT
USE Matrix_Elements
USE Analysis_Input_Variables, ONLY: MPIFFT_Binary

USE MPI

implicit NONE

LOGICAL,Optional :: Pot
REAL*8, Optional :: Time


INTEGER ::  ierr
INTEGER ::  Iorb,IPRC,icntr
COMPLEX*16,  DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb) :: temp
COMPLEX*16, ALLOCATABLE :: Psi_Temp(:), PsiIN(:)
logical :: threeD


allocate(Psi_Temp(NDVR_X*NDVR_Y*NDVR_Z),stat=ierr)
if(ierr /= 0)  then 
   write(*,*) "allocation error in" 
endif
allocate(PsiIN(NDVR_X*NDVR_Y*NDVR_Z),stat=ierr)
if(ierr /= 0) then 
  write(*,*) "allocation error in" 
endif

icntr=1
I_ORBITALS: DO Iorb=1,Morb !Proc_Iorb_Starts(IPRC),Proc_Iorb_Finish(IPRC)
   PsiIN=temp(:,Iorb)
   threed=.TRUE.
   FFT_2D=.TRUE.
   Psi_Temp=PsiIN

!!! 2D and 3D FFT Exception
IF((DVR_X.eq.4).AND.(DVR_Y.eq.4).and.(threed.eqv..true.)&
  .AND.(DIM_MCTDH.eq.3).AND.(DVR_Z.eq.4)) THEN
 IF (MPIFFT_Binary.eqv..TRUE.) THEN
    CALL Get_KineticEnergyAction_FFT_MPI_3D(Psi_Temp,0)  !FFT 3D
 ELSE
    CALL Get_KineticEnergyAction_FFT_3D(Psi_Temp)  !FFT 3D
 ENDIF
 temp(:,icntr)=Psi_Temp
 goto 9879
Endif

IF(DVR_X.eq.4.AND.DVR_Y.eq.4&
   .AND.DIM_MCTDH.eq.2.AND.FFT_2D.eqv..TRUE.) THEN
    IF (MPIFFT_Binary.eqv..TRUE.) THEN
       CALL Get_KineticEnergyAction_FFT_MPI_2D(Psi_Temp,0)  !FFT 2D
    ELSE
       CALL Get_KineticEnergyAction_FFT_2D(Psi_Temp)  !FFT 2D
    ENDIF
    temp(:,icntr)=Psi_Temp
    goto 9879
ENDIF
!!! 2D and 3D FFT Exception

!!! Kinetic energy action in X direction
IF(DVR_X.eq.4) THEN
  CALL Get_KineticEnergyAction_FFT(Psi_Temp,1)  !FFT
ELSE
  CALL Get_KineticEnergyAction(Psi_Temp,Op_X,NDVR_X,1)
ENDIF
temp(:,icntr)=Psi_Temp
!!! Kinetic energy action in X direction


!!! Kinetic energy action in Y direction
IF(DIM_MCTDH.ge.2) then
    Psi_Temp=PsiIN

    IF(DVR_Y.eq.4) THEN
       CALL Get_KineticEnergyAction_FFT(Psi_Temp,2)  !FFT
    ELSE
       CALL Get_KineticEnergyAction(Psi_Temp,Op_Y,NDVR_Y,2)
    ENDIF

    temp(:,icntr)=temp(:,icntr)+Psi_Temp
endif
!!! Kinetic energy action in Y direction

!!! Kinetic energy action in Z direction
IF(DIM_MCTDH.ge.3) then
    Psi_Temp=PsiIN
   
    IF(DVR_Z.eq.4) THEN
       CALL Get_KineticEnergyAction_FFT(Psi_Temp,3)  !FFT
    ELSE
       CALL Get_KineticEnergyAction(Psi_Temp,Op_Z,NDVR_Z,3)
    ENDIF
   
    temp(:,icntr)=temp(:,icntr)+Psi_Temp
endif
!!! Kinetic energy action in Z direction
IF ((LZ.eqv..TRUE.)) then
  Write(6,*) "Applying LZ in energy analysis"
  Psi_Temp=PsiIN
  CALL Get_KineticEnergyAction_L_FFT(Psi_Temp)  !FFT
  IF ((DIM_MCTDH.eq.1).and.(trim(whichpot).eq.'DeltaStatic')) THEN
     temp(:,icntr) = temp(:,icntr)+OMEGAZ*Psi_Temp+0.5d0*OMEGAZ**2*PsiIN
  ELSE
    temp(:,icntr) = temp(:,icntr)+OMEGAZ*Psi_Temp
  ENDIF
endif



IF ((Present(Pot).eqv..TRUE.).and.(Pot.eqv..TRUE.)) THEN
  IF (Present(Time).eqv..False.) THEN
     Write(6,*) "Cannot compute potential without time as input"
     STOP "Kin. Energy + Potential in Analysis"
  ENDIF
  CALL Get_1bodyPotential(time)
  temp(:,icntr)=temp(:,icntr) + VTRAP_EXT*PsiIN
ENDIF


 9879 continue 
  icntr=icntr+1

END DO I_ORBITALS


return
end SUBROUTINE Get_KineticEnergyAction_AllOrbitalsAnalysis


!> Compute (unitary) transform of a multiconfigurational wavefunction
!> MODE decides how the transform is done:
!> MODE=1 Just takes the MxM matrix TRAFO and applies it to both coeffs and orbs
!> MODE=2 Constructs the transformation from PHIIN to PHI_TRANS and applies it to the coefficients
SUBROUTINE U_TRANS(TRAFO,VIN,V_TRANS,PHIIN,PHI_TRANS,NGRID,MODE)

USE Coefficients_Parameters
USE CI_Production_Parameters
USE Interaction_Parameters
USE DVR_Parameters
USE Matrix_Elements
USE Combinatorial_Algorithms
USE Addresses


IMPLICIT NONE
COMPLEX*16                          :: PHIIN(NGRID,MORB)
COMPLEX*16                          :: PHI_TRANS(NGRID,MORB)
COMPLEX*16, DIMENSION(NCONF)        :: VIN
COMPLEX*16, DIMENSION(NCONF)        :: V_TRANS
COMPLEX*16, DIMENSION(MORB,MORB)    :: TRAFO
COMPLEX*16                          :: ROWPROD,ROWSUM,RESCALE
COMPLEX*16, DIMENSION(NCONF)        :: PREFACS

REAL*8                              :: FACTORIAL_I
REAL*8                              :: FACTORIAL_J
REAL*8                              :: FACTORIAL_IJ

INTEGER                             :: MODE
INTEGER*8                           :: NGRID
INTEGER                             :: I,J,K,L,M
INTEGER, DIMENSION(MORB)            :: VEC_II,VEC_IO,COUNTER
INTEGER, DIMENSION(MORB)            :: M_COUNT,TMP1,TMP2,VEC_JI,VEC_JO
INTEGER, DIMENSION(MORB,MORB)       :: COMP_STORE
LOGICAL                             :: BOOL(MORB)      

INTEGER :: K1,K2,N2
REAL*8  :: prefactor


REAL*8                              :: DZNRM2
EXTERNAL DZNRM2



IF ((MODE.ne.1).and.(MODE.ne.2)) THEN
   WRITE(6,*) "WRONG MODE IN U_TRANS!!!!" 
   STOP
ENDIF

IF (MODE.eq.2) THEN
   CALL GET_TRAFO(PHIIN,PHI_TRANS,TRAFO,MORB,NGRID)
ENDIF


VEC_II=0
VEC_IO=VEC_II

VEC_JI=0
VEC_JO=VEC_JI

V_TRANS=0.d0

PREFACS=1.d0
RESCALE=DCMPLX(1.d0,0.d0)

TMP1=0
TMP2=0

WRITE(6,*) "NORMALIZATION OF VIN", DZNRM2(NCONF,VIN,1)
WRITE(6,*) "NORMALIZATION OF V_TRANS",DZNRM2(NCONF,V_TRANS,1)

 
DO I=1,NCONF
  M=0  
  CALL Get_ConfigurationFromIndex(I,NPAR,MORB,VEC_II,VEC_IO)

  FACTORIAL_I=1.d0

  DO K=1,MORB
     FACTORIAL_I=FACTORIAL_I*(sqrt(Factorial(VEC_IO(K))))
  END DO

  WRITE(6,*) "CONF1",I,"FACTORIAL_I",FACTORIAL_I

  M_COUNT=0  
  ROWSUM=DCMPLX(0.d0,0.d0)
  BOOL=.FALSE.
  COUNTER=0
  COMP_STORE=0
!!!!!!!!!!!!!!!!!!!!!!
 9999 CONTINUE
!!!!!!!!!!!!!!!!!!!!!!
!  COMP_STORE=0
  DO K=1,MORB
      VEC_II=COMP_STORE(K,:)

!      IF ((BOOL(K).eqv..TRUE.).OR.(COUNTER(K).eq.0)) THEN
!      WRITE (6,*) "IN",VEC_IO(K),MORB,VEC_II,BOOL(K),TMP1(K),TMP2(K)
         CALL COMP_NEXT(VEC_IO(K),MORB,VEC_II,BOOL(K),TMP1(K),TMP2(K))
!         COUNTER(K)=COUNTER(K)+1  
!      ENDIF
!      WRITE(6,*) "I",I, "Op", K,"CONF",VEC_II
      COMP_STORE(K,:)=VEC_II
!
!      CALL MULTINOMIAL_COEF1(MORB,VEC_II,COEF)
!      WRITE(6,*) "NEW-C", COEF,"CONF",VEC_II
!         
!      ROWPROD=ROWPROD*DCMPLX(1.d0*COEF,0.d0)*TRAFO **VEC_II(L)
!
!      DO L=1,MORB
!             WRITE(6,*) "K",K,"L",L,"TRAFO",TRAFO(K,L),"NVEC_II(L)",NVEC_II(L), COEF*((TRAFO(K,L))**NVEC_II(L))
!       ROWSUM=ROWSUM+(TRAFO(L,K)**VEC_II(L))
!      END DO
!
!
!      WRITE(6,*) "MULTINOMIAL",ROWSUM,"COMBINATIONS:",VEC_II,"COEFFTS",COEF
!      ROWSUM=ROWSUM+ROWPROD
! 
!      RESCALE=RESCALE*ROWSUM
      FACTORIAL_J=1.d0
      FACTORIAL_IJ=1.d0
      ROWPROD=1.d0


      DO L=1,MORB

         M_COUNT(L)=SUM(COMP_STORE(:,L))
         IF (M_COUNT(L).gt.NPAR) WRITE(6,*) "ERRRR", COMP_STORE(:,L),"CONF",I 
         FACTORIAL_J=FACTORIAL_J*sqrt(Factorial(M_COUNT(L)))

         DO M=1,MORB
            FACTORIAL_IJ=FACTORIAL_IJ/Factorial(COMP_STORE(L,M))
            ROWPROD=ROWPROD*TRAFO(M,L)**COMP_STORE(L,M)
         END DO

      END DO
      WRITE(6,*) "TO GETIND",M_COUnt,NPAR,MORB
      CALL Get_IndexFromConfiguration(Npar,Morb,M_Count,J)
      V_TRANS(J)=V_TRANS(J)+ROWPROD*FACTORIAL_I*FACTORIAL_J*FACTORIAL_IJ*VIN(I)

!      WRITE(6,*) "I-CONF",I,"J-CONF",M_COUNT,"NO",J
!      WRITE(6,*) "BOOL",BOOL
     
     
     

   END DO

!   DO K=1,MORB
!      M_COUNT(K)=SUM(COMP_STORE(:,K))
!   END DO
!!!!!!!!!!!!!!!!
   DO K=1,MORB
        IF (BOOL(K).EQV..TRUE.) GOTO 9999
   END DO
!!!!!!!!!!!!!!!!
!   PAUSE 
   WRITE(6,*) "FINAL PREFACS:",RESCALE

!   V_TRANS(I)=VIN(I)*RESCALE
!   RESCALE=DCMPLX(1.d0,0.d0)

END DO
IF (MODE.eq.1) THEN
!Apply transform to the orbitals
  PHI_TRANS=DCMPLX(0.d0,0.d0)
  DO I=1,MORB
     DO J=1,NGRID
        DO K=1,MORB
           PHI_TRANS(J,I)=TRAFO(I,K)*PHIIN(J,K)
        END DO
     END DO
  END DO
ENDIF
RETURN

end SUBROUTINE U_TRANS





SUBROUTINE get_maximum(PSI,rho_jk,maximum)
!---{{{
use DVR_Parameters

  implicit none
!ORG  integer,parameter  :: NOGRIDPOINTS=NDVR_X*NDVR_Y*NDVR_Z
  integer  :: NOGRIDPOINTS
!  complex*16,DIMENSION(NOGRIDPOINTS,Morb),intent(in) :: PSI 
    COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb)  :: PSI
  complex*16,DIMENSION(Morb,Morb),intent(in) :: rho_jk
  complex*16,intent(out)  :: maximum
  integer                 :: m,Jorb,Korb
  complex*16              :: G1

  NOGRIDPOINTS=NDVR_X*NDVR_Y*NDVR_Z

  maximum = (0.d0,0.d0)
  do m=1,NOGRIDPOINTS
 
 !compute G1
     G1 = (0.d0,0.d0)
     do Korb = 1,Morb  
        do Jorb=1,Morb

           G1 = G1 + rho_jk(Jorb,Korb) * DCONJG(PSI(m,Jorb))*PSI(m,Korb)

        end do
     end do

     if (REAL(CDABS(G1)) .gt. REAL(CDABS(maximum))) maximum = G1
 
  end do
!---}}}
end SUBROUTINE get_maximum



SUBROUTINE Get_TRAFO(PHIIN,PHI_TRANS,TRAFO,MORB,NGRID)
!> Compute the transformation matrix of PHIIN to PHITRANS
INTEGER*8                           :: I,K,L,NGRID
COMPLEX*16                          :: PHIIN(NGRID,MORB)
COMPLEX*16                          :: PHI_TRANS(NGRID,MORB)
COMPLEX*16, DIMENSION(MORB,MORB)    :: TRAFO
INTEGER                             :: MORB

TRAFO=DCMPLX(0.d0,0.d0)
DO I=1,MORB
   DO L=1,MORB
      DO K=1,NGRID
         TRAFO(I,L)=TRAFO(I,L)+DCONJG(PHIIN(K,L))*PHI_TRANS(K,I)
      END DO
   END DO
END DO

!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!    FOR A ROTATION OF 2 ORBS uncomment the following
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

!TRAFO=DCMPLX(0.d0,0.d0)
!THETA=0.0005d0
!TRAFO(1,1)=COS(THETA)
!TRAFO(1,2)=SIN(THETA)
!TRAFO(2,1)=-SIN(THETA)
!TRAFO(2,2)=COS(THETA)
!TRAFO(3,3)=1.d0
!TRAFO(4,4)=1.d0

WRITE (6,*) "COMPUTED TRANSFORM OF THE ORBS TRAFO="
DO l=1,MORB
 WRITE (6,*)   (TRAFO(i,l),i=1,MORB)
END DO
!WRITE(6,*) "UNITARITY CHECK:", MATMUL(TRANSPOSE(DCONJG(TRAFO)),TRAFO)

!DO I=1,MORB 
!   IF (ABS(TRAFO(I,I)).lt.0.1d0) THEN
!      TRAFO(I,I)=1.d0
!   ENDIF
!END DO

END SUBROUTINE Get_TRAFO




!> Compute the transformation of a single configuration (permanent).
recursive SUBROUTINE loopy_permanent(More,Depth,Configuration,&
                                     First,TMP1,TMP2,L,Trans,Coefficients,&
                                     Coefficients_Out)

Use Global_Parameters
Use Function_Library
Use Coefficients_Parameters
Use Combinatorial_Algorithms
Use Addresses

INTEGER :: Depth
INTEGER,INTENT(INOUT) :: L(Morb,Morb),Configuration(Morb)
      
INTEGER,INTENT(INOUT) :: TMP1(Morb),TMP2(Morb)

LOGICAL,INTENT(INOUT) :: More(Morb),First(Morb)

COMPLEX*16    ::Coefficients(Nconf),Trans(Morb,Morb)

COMPLEX*16,INTENT(INOUT) :: Coefficients_out(Nconf)

INTEGER :: I,K,Configuration_out(Morb),Index_in,Index_out
COMPLEX*16    :: Prefactor

If ((More(Depth).eqv..TRUE.).or.(FIRST(Depth).eqv..TRUE.)) then
  If (Depth.lt.Morb) then
     DO While ((More(Depth).eqv..TRUE.).or.(FIRST(Depth).eqv..TRUE.))
        Call Comp_Next(Configuration(Depth),Morb,L(Depth,:), &
                          More(Depth),TMP1(Depth),TMP2(Depth))
        
           Prefactor=1.d0
           DO I=1,MORB
              Configuration_out(I)=SUM(L(:,I))
                     
              Prefactor=Prefactor*exp(&
                     (dlgama(1.d0*Configuration(I)))/2 &
                    +(dlgama(1.d0*Configuration_out(I)))/2)
               DO K=1,Morb
                  IF (L(I,K).ne.0) THEN
                   Prefactor=Prefactor*exp(-(dlgama(1.d0*L(I,K))))
                   write(6,*) "L",L(I,K),dlgama(1.d0*L(I,K))
                  ENDIF
               END DO
            END DO

           DO I=1,MORB
              DO K=1,MORB
                  IF (L(I,K).ne.0) THEN
                     Prefactor=Prefactor*(Trans(I,K)**L(I,K))
                  ENDIF
              END DO
           END DO
           CALL Get_IndexFromConfiguration(Npar,Morb,&
                                           Configuration_out,Index_out)
           CALL Get_IndexFromConfiguration(Npar,Morb,&
                                           Configuration,Index_in)

           WRITE(6,*)  Prefactor
         

        FIRST(Depth)=.FALSE.
        
         
        CALL loopy_permanent(More,Depth+1,Configuration,&
                             First,TMP1,TMP2,L,Trans,Coefficients,&
                             Coefficients_out)

        write(6,*) "Depth",Depth,"L-Matrix:",L
!        write(6,*) "M:",(SUM(L(:,k)),k=1,Morb)
     End Do
  Elseif ((Depth.eq.Morb)) THEN
     Do While ((More(Depth).eqv..TRUE.).or.(FIRST(Depth).eqv..TRUE.))
        Call Comp_Next(Configuration(Depth),Morb,L(Depth,:), &
                       More(Depth),TMP1(Depth),TMP2(Depth))
           Prefactor=1.d0
           DO I=1,MORB
              Configuration_out(I)=SUM(L(:,I))
                     
              Prefactor=Prefactor*exp(&
                     (dlgama(1.d0*Configuration(I)))/2 &
                    +(dlgama(1.d0*Configuration_out(I)))/2)
               DO K=1,Morb
                  IF (L(I,K).ne.0) THEN
                   Prefactor=Prefactor*exp(-(dlgama(1.d0*L(I,K))))
                  ENDIF
               END DO
            END DO

           DO I=1,MORB
              DO K=1,MORB
                  Prefactor=Prefactor*(Trans(I,K)**L(I,K))
              END DO
           END DO
           CALL Get_IndexFromConfiguration(Npar,Morb,&
                                           Configuration_out,Index_out)
           CALL Get_IndexFromConfiguration(Npar,Morb,&
                                           Configuration,Index_in)

           WRITE(6,*) Coefficients(Index_in),Prefactor
           Coefficients_out(Index_out)=Coefficients_out(Index_out)+&
                                       Prefactor*Coefficients(Index_in)
         

        FIRST(Depth)=.FALSE.
        write(6,*) "D",Depth, "L:",L,"M:",(SUM(L(:,k)),k=1,Morb)
     End Do
  Endif
  IF(Depth.gt.1) THEN
    First(Depth)=.TRUE.
  ELSE
    FIRST(Depth)=.FALSE.
  ENDIF
Endif

end SUBROUTINE loopy_permanent


!> This routine computes a unitary transform of of the coefficients' vector.
   SUBROUTINE U_Transform (time,VIN)

   USE   Global_Parameters
   USE   Matrix_Elements
   USE   Coefficients_Parameters
   USE   Interaction_Parameters
   USE   DVR_Parameters
   USE   Function_Library
   USE   Addresses
   USE   Combinatorial_Algorithms


   COMPLEX*16, DIMENSION(Nconf) :: VIN,VOUT,VIN1,VOUT1
   COMPLEX*16 scl
   COMPLEX*16 scl1,scl2
   real*8 time,t_total,prefactor
   character*18 lname
   COMPLEX*16, DIMENSION(Morb,Morb) :: U ! NatVecInv Added for Unitary rotations NatVec is given
   COMPLEX*16, DIMENSION(Morb) :: U_Rowsum
   COMPLEX*16 :: Configuration_Product
   real*8  xnrm,DZNRM2
   integer :: NDIM,N2,K1,K2,N,NN1,N1,M,JJ,IND,CT,I,J,K
   integer :: J_VEC_II(Morb),J_VEC_IO(Morb),TMP1(Morb),TMP2(Morb),COMP_STORE(Morb,Morb)
   integer :: I_VEC_II(Morb),I_VEC_IO(Morb),M_Coef
   LOGICAL, DIMENSION(MORB) :: BOOL,FIRST
   external DZNRM2,ZGETRF,ZGETRI

   VIN1=VIN
   VIN1=VIN1/DZNRM2(Nconf,VIN1,1)
   U=NatVec

   VOUT=Zero
   VOUT1=Zero
   ndim=Nconf
   CT=0

   goto 13

   DO I=1,MORB
      U_Rowsum(I)=SUM(U(I,:))
   END DO

   DO I=1,NCONF
      BOOL=.FALSE.
      FIRST=.TRUE.
      Comp_Store=0
      write(6,*) "Coefficients",VIN
      CALL Get_ConfigurationFromIndex(I,Npar,Morb,I_VEC_II,I_VEC_IO) 
      Call Loopy_Permanent(BOOL,1,I_VEC_IO,FIRST,TMP1,TMP2,&
                           COMP_STORE,U,VIN,VOUT1)
!recursive SUBROUTINE loopy_permanent(More,Depth,Configuration,&
!                                     First,TMP1,TMP2,L,Trans,Coefficients,&
!                                     Coefficients_Out)


   END DO

    xnrm=DZNRM2(Nconf,VOUT1,1)

    write(6,*)"U-transformed CI Vec NORM :",xnrm

13 continue
   do n2=0,Npar
     Do k1=0,Npar-n2
       Do k2=0,n2

          prefactor = (dlgama( 1.0d0*(Npar-n2+1))/2&  ! sqrt(n1!)
            + dlgama ( 1.0d0*(n2     +1))/2&          ! sqrt(n2!)
            + dlgama ( 1.0d0*(k1+k2+1))/2&            ! sqrt((k1+k2)!)
            + dlgama ( 1.0d0*(Npar-k1-k2+1))/2&       ! sqrt((N-k1-k2)!)
            - dlgama ( 1.0d0*(k1     +1))&            ! 1/k1!
            - dlgama ( 1.0d0*(Npar-n2-k1+1))&         ! 1/(N-n2-k1)!
            - dlgama ( 1.0d0*(k2     +1))&            ! 1/k2!
            - dlgama ( 1.0d0*(n2-k2  +1)) )           ! 1/(n2-k2)!

          scl=    U(1,2)**((Npar-n2-k1))   ! <phi1|phi2>^(N-n2-k1)
          scl=scl*U(1,1)**(k1)             ! <phi1|phi1>^(k1)
          scl=scl*U(2,2)**((n2-k2))        ! <phi2|phi2>^(n2-k2)
          scl=scl*U(2,1)**(k2)             ! <phi2|phi1>^(k2)
          scl1=scl*exp(prefactor)          


!          Write(6,*) "k1,k2",k1,k2,"n1,n2",Npar-n2,n2
!          Write(6,*) "Target conf",Npar-(k1+k2)+1,"Source conf",n2+1,"pref",scl,prefactor


          VOUT(Npar-(k1+k2)+1)=VOUT(Npar-(k1+k2)+1)+scl1*VIN1(n2+1)
          CT=CT+1
       Enddo
     Enddo
   enddo

   WRITE(6,*) "I SUMMED",CT,"CONFIGURATIONS FOR THE COEFFICIENTS TRANSFORMATION"
   goto 12

11     continue

   N=Npar
   do NN1=0,N
      VOUT(N-NN1+1)=Zero
      do n1=0,N
      scl1=Zero
         do m=MAX(NN1+n1-N,0),MIN(NN1,n1)
            scl=    U(1,1)**(1.0d0*m)
            scl=scl*U(2,1)**(1.0d0*(NN1-m))
            scl=scl*U(1,2)**(1.0d0*(n1-m))
            scl=scl*U(2,2)**(1.0d0*(N-n1-NN1+m))

            scl2=U(2,1)**(1.0d0*NN1)*U(1,2)**(1.0d0*n1)
            scl2=scl2*U(2,2)**(1.0d0*(N-n1-NN1))
            scl2=scl2*&
                Exp(dlgama ( 1.0d0*(n1+1))/2+&
                    dlgama(1.0d0*(N-n1+1))/2)

            prefactor = Exp(dlgama ( 1.0d0*(n1+1))/2&
               + dlgama ( 1.0d0*(N-n1+1))/2&
               + dlgama ( 1.0d0*(NN1+1))/2&
               + dlgama(1.0d0*(N-NN1+1))/2&
               - dlgama ( 1.0d0*(m     +1))& 
               - dlgama ( 1.0d0*(NN1-m+1))&
               - dlgama ( 1.0d0*(n1-m     +1)) &
               - dlgama ( 1.0d0*(N-n1-NN1+m +1)) )

            scl=scl*prefactor
            scl1=scl1+scl
          Enddo
          VOUT(N-NN1+1)=VOUT(N-NN1+1)+scl1*VIN1(N-n1+1)
       Enddo
    Enddo

12      continue

    xnrm=DZNRM2(Nconf,VOUT,1)

    write(6,*)"U-transformed CI Vec NORM :",xnrm

    IF(ABS(1.0-xnrm).ge.0.0001d0) return
    t_total=time

    if(t_total.lt.99999.99999999D0) then
       Write( lname, '(F10.4,a8)') t_total,"coef.TRN"
    endif
    if(t_total.lt.9999.99999999D0) then
      Write( lname, '(F10.5,a8)') t_total,"coef.TRN"
    endif
    if(t_total.lt.999.99999999D0) then
      Write( lname, '(F10.6,a8)') t_total,"coef.TRN"
    endif
    if(t_total.lt. 99.99999999D0) then
      Write( lname, '(F10.7,a8)') t_total,"coef.TRN"
    endif
    if(t_total.lt.  9.99999999D0) then
      Write( lname, '(F10.8,a8)') t_total,"coef.TRN"
    endif

    open(unit=111,file=lname,form='formatted')
    write(111,*)"          "
    write(111,'(a1,a28,138(I26,a3))')"#","",(jj,"  ",jj=2,10)
   
    ndim=Nconf
    do ind=1,ndim
       write(111,2222) ind,"  "&
     ,DREAL(VOUT(ind)),"   ",DIMAG(VOUT(ind)),"   "&
     ,DREAL(VOUT1(ind)),"   ",DIMAG(VOUT1(ind)),"   "&
     ,t_total
    enddo

    close(111)
    return

2222    format((i26,a3,10(F26.16,a3)))

       end SUBROUTINE U_Transform

END MODULE  Auxiliary_Analysis_Routines
