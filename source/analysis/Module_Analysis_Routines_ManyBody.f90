!>GPLv3
!    MCTDH-X: the multiconfigurational time-dependent Hartree for 
!    indistinguishable particles software
!
!    Copyright (C) 2021  A. U. J. Lode, M. C. Tsatsos, 
!                        E. Fasshauer, S. E. Weiner, 
!                        R. Lin, L. Papariello, P. Molignini, 
!                        C. Lévêque, M. Büttner , J. Xiang, S. Dutta
!
!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <https://www.gnu.org/licenses/>.
!
!
!
!
!<GPLv3










!> This module collects analysis routines for multiconfigurational wavefunctions
!> that have a many-body character.
MODULE Analysis_Routines_ManyBody

USE Analysis_Input_Variables
USE Interaction_Parameters
USE Global_Parameters
USE Matrix_Elements
USE Function_Library
USE Auxiliary_Routines
USE Auxiliary_Analysis_Routines

IMPLICIT NONE
INTEGER*8, private       :: I,J,K,m,L,i_m,j_m,k_m,i_n,j_n,k_n
INTEGER, private         :: Iorb,Jorb,Korb,Lorb
CONTAINS


SUBROUTINE Get_Autocorrelation(time_1, time_2, time_p, Nlvl)

   USE Global_Parameters
   USE Coefficients_Parameters
   USE addresses
   USE addresses_fermions
   USE Analysis_Input_Variables
   USE Input_Output
   USE DVR_Parameters

   IMPLICIT NONE
   REAL(kind=8)    :: time_1
   REAL(kind=8)    :: time_2
   INTEGER         :: time_p, Nlvl
   COMPLEX(kind=8) :: VIN(NConf), VIN0(NConf)
   COMPLEX(kind=8) :: Psi(NDVR_X*NDVR_Y*NDVR_Z,MOrb)
   COMPLEX(kind=8) :: Psi0(NDVR_X*NDVR_Y*NDVR_Z,MOrb)
   COMPLEX(kind=8) :: Psi_Nlevel(NDVR_X*NDVR_Y*NDVR_Z,MOrb,Nlvl)
   COMPLEX(kind=8) :: Psi0_Nlevel(NDVR_X*NDVR_Y*NDVR_Z,MOrb,Nlvl)


   INTEGER         :: i, j, k, l, m, n, ind1, ind2, dimOrb, DVR_size, NOZ, EOF
   INTEGER         :: Conf1(MOrb), Conf2(MOrb), Conf3(Morb), Ivec(MOrb)
   REAL(kind=16)   :: facto1, facto2
   COMPLEX(kind=8) :: auto, val, Ov
   COMPLEX(kind=8) :: OvMat(MOrb,MOrb), Perm_Or_Det(NPar,NPar)
   COMPLEX*16, DIMENSION(Nconf) :: proj1, proj2
   COMPLEX(kind=16):: temp1, temp2
   REAL(kind=8)    :: time_running, time_step, NOZ_running,en1,time_test


   DVR_size       = NDVR_X * NDVR_Y * NDVR_Z
   dimOrb         = NDVR_X*NDVR_Y*NDVR_Z
   time_step      = (real(time_2, 8)-real(time_1, 8))/real(time_p, 8)
   NOZ_running    = time_step
   EOF            = (time_2-time_1)*time_p+1 
   NOZ            = 0
   time_test      = 32.1

   ! Read input at given time t and t_ref

   if (time_step .ge. 1) then
      NOZ = 0
   else
      Do while (NOZ_running .lt. 1)
         NOZ_running = NOZ_running * 10
         NOZ  = NOZ + 1
      enddo
   endif

   time_running = time_1
   Do while (NINT(time_2*10**NOZ-time_running*10**NOZ) .ge. 0)
  
   IF (Nlvl .eq. 1) THEN

       IF (time_running .eq. time_1) THEN

             rewind(777)
             rewind(778)
             call Read_binary(777,time_1,VIN,PSI,0,DVR_size,Block=.FALSE.)  !Coeff_PSI_bin
             call Read_binary(778,time_1,VIN,PSI,0,DVR_size,Block=.FALSE.)  !Orbs_PSI_bin
             rewind(777)
             rewind(778)
             call Read_binary(777,time_running,VIN0,PSI0,0,DVR_size,Block=.FALSE.)
             call Read_binary(778,time_running,VIN0,PSI0,0,DVR_size,Block=.FALSE.)
  
        ELSE
             
             call Read_binary(777,time_running,VIN0,PSI0,0,DVR_size,Block=.FALSE.)
             call Read_binary(778,time_running,VIN0,PSI0,0,DVR_size,Block=.FALSE.)
  
        ENDIF

        CALL OverlapMat(Psi0,Psi,dimOrb,MOrb,OvMat)

   ELSE
        IF (time_running .eq. time_1) THEN

             rewind(777)
             rewind(778)
             call Read_binary_Nlevel(777,time_1,VIN,PSI_Nlevel)  !Coeff_PSI_bin
             call Read_binary_Nlevel(778,time_1,VIN,PSI_Nlevel)  !Orbs_PSI_bin

             PSI0_Nlevel=PSI_Nlevel
             VIN0=VIN
                
        ELSE

             call Read_binary_Nlevel(777,time_running,VIN0,PSI0_Nlevel)
             call Read_binary_Nlevel(778,time_running,VIN0,PSI0_Nlevel)
                
        ENDIF

        do i=1,Morb
           do j=1,Morb
           Ovmat(i,j)=0
              do k=1,Nlvl
                 do l=1,DVR_size
                    OvMat(i,j)=OvMat(i,j)+PSI0_Nlevel(l,i,k)*dconjg(PSI_Nlevel(l,j,k))
                 end do
              end do
           end do
        end do

   ENDIF

   ! build overlap matrix for <phi_{i}(t0)|phi_{j}(t)>
   ! (matrix of overlap between *orbitals*) --> still need to construct
   ! permanents/determinants to get the full wave function.

   auto = dcmplx(0.d0, 0.d0)
   IF (Job_type .eq. 'BOS') THEN

      ! autocorrelation function for single orbital case for bosons

      IF (MOrb == 1) THEN
        auto   = 0.d0
        auto = conjg(VIN0(1))*VIN(1)*OvMat(1,1)**(NPar)
      ENDIF

   ELSEIF (Job_type .eq. 'FER') THEN

      IF (MOrb .eq. NPar) THEN
          auto = conjg(VIN0(1))*VIN(1)
          DO i = 1, MORB
              auto = auto*OvMat(i,i)
          END DO

      ENDIF
   ENDIF

   IF (((Job_type .eq. 'BOS') .and. MORB .gt. 1)  &
    .or.((Job_type .eq. 'FER') .and. MORB .gt. NPar)) THEN

      DO i = 1, NConf
      

      IF (Job_type .eq. 'BOS') THEN
        CALL Get_ConfigurationFromIndex_OLD(i,NPar,MOrb,Conf1)
      ELSEIF (Job_type .eq. 'FER') THEN
          CALL Get_ConfigurationFromIndex_Fermions(i,NPar,Morb,Conf1)
      ENDIF

      ! temporary variable to calculate and store \sum_{j} val*VIN(j) for the
      ! projection onto configurations:
      temp1 = 0.d0
      temp2 = 0.d0

      DO j = 1, NConf

      ! build permanent or determinant, i.e. overlap matrix between two configurations
         ! using results stored in OvMat

         ind1 = 0
         DO k = 1, MOrb
            DO l = 1, Conf1(k)
               ind1 = ind1 + 1
               ind2 = 0
               DO m = 1, MOrb
                IF (Job_type .eq. 'BOS') THEN
                  CALL Get_ConfigurationFromIndex_OLD(j,NPar,MOrb,Conf2)
                ELSEIF (Job_type .eq. 'FER') THEN
                  CALL Get_ConfigurationFromIndex_Fermions(j,NPar,Morb,Conf2)
                ENDIF

                  DO n = 1, Conf2(m)
                     !swrite(6,*) "assigning", ind1, ind2, OvMat(k,m)
                     ind2 = ind2 + 1
                     Perm_Or_Det(ind1,ind2) = OvMat(k,m)                     
                  ENDDO

               ENDDO
            ENDDO
         ENDDO

         IF (Job_type .eq. 'BOS') THEN
         ! evaluate permanent
            CALL get_Permanent(Perm_Or_Det,NPar,val)

         ! evaluate factorial coefficient
         facto1 = 1d0
         facto2 = 1d0

         DO k = 1, MOrb
              ! compute (n1! n2! ...) for normalizing the contribution of each configuration in the boson's overlap
            facto1 = facto1 * gamma(REAL(Conf1(k)+1,kind=16))
            facto2 = facto2 * gamma(REAL(Conf2(k)+1,kind=16))
         ENDDO

         val = val / ( sqrt(REAL(facto1,kind=8)) * sqrt(REAL(facto2,kind=8)) )

         ELSEIF (Job_type .eq. 'FER') THEN
            !write(6,*) "Perm or det", Perm_Or_Det
            CALL get_Determinant(Perm_Or_Det,NPar,val)
            facto1 = 1d0
            facto1 = facto1 * gamma( REAL( NPar, kind = 16 ) )
            val = val / REAL(facto1, kind = 8 )

         ENDIF

         auto = auto + val*conjg(VIN0(i))*VIN(j)

      ENDDO
   ENDDO

   ENDIF

   !> Normalize the autocorrelation function for Fermions
  IF ((NPar.gt.2) .and. (Job_Type .eq. 'FER')) THEN
    Do k=2, NPar-1
      auto=k*auto
    ENDDO
  ENDIF

  !> WRITE fidelity to fidelity.out .
  IF (abs(time_1-time_running).lt.0.1d-8) THEN
     OPEN(unit=806,file='Autocorrelation.out', form='formatted', STATUS= "UNKNOWN")
  ELSE
     OPEN(unit=806,file='Autocorrelation.out', form='formatted', ACCESS = "APPEND" , STATUS= "OLD")
  ENDIF

  WRITE(806,*) time_1, time_running, REAL(auto), aimag(auto), abs(auto), abs(auto)**2
  
  time_running=time_running+real(nint(time_step*10**NOZ),8)/10**NOZ

  END DO

  rewind(777)
  rewind(778)
  CLOSE(806)

  RETURN

END SUBROUTINE Get_Autocorrelation

!> @brief Compute overlap of a state (VIN, PSI) and a second state (VIN0, Psi0).
!>
!> @param[IN] t : time
!> @param[IN] VIN : coefficients of the first state
!> @param[IN] Psi : orbitals of the first state
!> @param[IN] VIN0 : coefficients of the second state
!> @param[IN] Psi0 : orbitals of the second state
SUBROUTINE Get_Fidelity(VIN,Psi,VIN0,Psi0,t)

   USE Global_Parameters,ONLY:NDVR_X,NDVR_Y,NDVR_Z,MOrb,Job_type
   USE Coefficients_Parameters, ONLY: NPar,NConf
   USE addresses
   USE addresses_fermions
   USE Analysis_Input_Variables


   IMPLICIT NONE
   REAL(kind=8),    INTENT(in) :: t
   COMPLEX(kind=8), INTENT(in) :: VIN(NConf), VIN0(NConf)
   COMPLEX(kind=8), INTENT(in) :: Psi(NDVR_X*NDVR_Y*NDVR_Z,MOrb)
   COMPLEX(kind=8), INTENT(in) :: Psi0(NDVR_X*NDVR_Y*NDVR_Z,MOrb)

   INTEGER         :: i, j, k, l, m, n, ind1, ind2, dimOrb !< 
   INTEGER         :: Conf1(MOrb), Conf2(MOrb), Conf3(Morb), Ivec(MOrb)
   REAL(kind=16)   :: facto1, facto2
   COMPLEX(kind=8) :: auto, val, Ov
   COMPLEX(kind=8) :: OvMat(MOrb,MOrb), Perm_Or_Det(NPar,NPar)
   COMPLEX*16, DIMENSION(Nconf) :: proj1, proj2
   COMPLEX(kind=16) :: temp1, temp2

   dimOrb = NDVR_X*NDVR_Y*NDVR_Z

   ! Opening files where to store the results:
   open(unit=804,file='fidelity.out',form='formatted')
   open(unit=805,file='lattice_projection.out',form='formatted')


   ! build overlap matrix for <phi_{i}(t0)|phi_{j}(t)>
   ! (matrix of overlap between *orbitals*) --> still need to construct
   ! permanents/determinants to get the full wave function.
   CALL OverlapMat(Psi0,Psi,dimOrb,MOrb,OvMat)
   auto = dcmplx(0.d0, 0.d0)

   IF (Job_type .eq. 'BOS') THEN

      ! autocorrelation function for single orbital case for bosons

      IF (MOrb == 1) THEN
        auto   = 0.d0
        auto = conjg(VIN0(1))*VIN(1)*OvMat(1,1)**(NPar) 
      ENDIF

   ELSEIF (Job_type .eq. 'FER') THEN

      IF (MOrb .eq. NPar) THEN
          auto = conjg(VIN0(1))*VIN(1)
          DO i = 1, MORB
              auto = auto*OvMat(i,i) 
          END DO
      
      ENDIF
   ENDIF


   IF (((Job_type .eq. 'BOS') .and. MORB .gt. 1)  &
    .or.((Job_type .eq. 'FER') .and. MORB .gt. NPar)) THEN
   ! loop over all conf to evaluate autoco
   !---{{{
!$OMP PARALLEL
!$OMP DO PRIVATE(i,j,ind1,k,l,ind2,m,n,Conf1,Conf2,Perm_Or_Det,val,facto1,facto2) REDUCTION(+:auto)  
   DO i = 1, NConf

      IF (Job_type .eq. 'BOS') THEN
        CALL Get_ConfigurationFromIndex_OLD(i,NPar,MOrb,Conf1)

      ELSEIF (Job_type .eq. 'FER') THEN
          CALL Get_ConfigurationFromIndex_Fermions(i,NPar,Morb,Conf1)
      ENDIF


      ! temporary variable to calculate and store \sum_{j} val*VIN(j) for the
      ! projection onto configurations:
      temp1 = 0.d0
      temp2 = 0.d0

      DO j = 1, NConf
         

         
         ! build permanent or determinant, i.e. overlap matrix between two configurations
         ! using results stored in OvMat

         ind1 = 0 
         DO k = 1, MOrb   
            DO l = 1, Conf1(k)              
               ind1 = ind1 + 1 
               ind2 = 0 
               DO m = 1, MOrb
                IF (Job_type .eq. 'BOS') THEN
                  CALL Get_ConfigurationFromIndex_OLD(j,NPar,MOrb,Conf2)
                ELSEIF (Job_type .eq. 'FER') THEN
                  CALL Get_ConfigurationFromIndex_Fermions(j,NPar,Morb,Conf2)
                ENDIF
                  DO n = 1, Conf2(m)
                     !swrite(6,*) "assigning", ind1, ind2, OvMat(k,m)
                     ind2 = ind2 + 1 
                     Perm_Or_Det(ind1,ind2) = OvMat(k,m)  
                  ENDDO   
               ENDDO   
            ENDDO   
         ENDDO   

         IF (Job_type .eq. 'BOS') THEN
         ! evaluate permanent
            CALL get_Permanent(Perm_Or_Det,NPar,val) 

         ! evaluate factorial coefficient
         facto1 = 1d0 
         facto2 = 1d0 
         
         DO k = 1, MOrb
              ! compute (n1! n2! ...) for normalizing the contribution of each configuration in the boson's overlap
            facto1 = facto1 * gamma(REAL(Conf1(k)+1,kind=16))  
            facto2 = facto2 * gamma(REAL(Conf2(k)+1,kind=16))
         ENDDO  

         val = val / ( sqrt(REAL(facto1,kind=8)) * sqrt(REAL(facto2,kind=8)) )   
  

         ELSEIF (Job_type .eq. 'FER') THEN
            !write(6,*) "Perm or det", Perm_Or_Det
            CALL get_Determinant(Perm_Or_Det,NPar,val)
            facto1 = 1d0 
            facto1 = facto1 * gamma( REAL( NPar, kind = 16 ) )
            val = val / REAL(facto1, kind = 8 )




         ENDIF
         
         auto = auto + val*conjg(VIN0(i))*VIN(j)

        !< Get only projector onto each orbital configuration
        !< VIN(j) is the MCTDH-X wavefunction coeff
        !< VIN0(i) is the TNT wavefunction coeff

        IF (overlap_projection .eqv. .TRUE.) THEN
          ! Add a flag / input to control the calculation of the proj. 
          temp1 = temp1 + val*VIN(j)
          temp2 = temp2 + val
          ! Axel's idea: set coeff=1 to compare the Hilbert spaces better.
          ! ->> output that in a different file + input/flag
        ENDIF


      ENDDO
      IF (overlap_projection .eqv. .TRUE.) THEN
        proj1(i) = temp1
        proj2(i) = temp2
      ENDIF
   ENDDO
   
!$OMP ENDDO 
!$OMP END PARALLEL
   !---}}}

   ENDIF

  !> Normalize the autocorrelation function for Fermions
  IF ((NPar.gt.2) .and. (Job_Type .eq. 'FER')) THEN
    Do k=2, NPar-1
      auto=k*auto
    ENDDO
  ENDIF

   !> WRITE fidelity to fidelity.out .
  IF (abs(t-Time_From).lt.0.1d-10) THEN
    OPEN(unit=804,file='fidelity.out', form='formatted', STATUS= "UNKNOWN")
  ELSE
    OPEN(unit=804,file='fidelity.out', form='formatted', ACCESS = "APPEND" , STATUS= "OLD")
  ENDIF

  WRITE(804,*) t, REAL(auto), aimag(auto), abs(auto), abs(auto)**2

  CLOSE(804)

  ! Create file for saving the projectors:
  IF (abs(t-Time_From).lt.0.1d-10) THEN
    OPEN(unit=805,file='lattice_projection.out', form='formatted', STATUS= "UNKNOWN")
  ELSE
    OPEN(unit=805,file='lattice_projection.out', form='formatted', ACCESS = "APPEND" , STATUS= "OLD")
  ENDIF


  ! Append values of the projectors to the file lattice_projection.out
  DO i=1, NConf 
    ! Get config from idx:
    CALL Get_ConfigurationFromIndex_OLD(i,NPar,MOrb,Conf3)
    WRITE(805,*) i, REAL(proj1(i)), aimag(proj1(i)), REAL(proj2(i)), aimag(proj2(i)), REAL(VIN0(i)), aimag(VIN0(i)), Conf3
  ENDDO
  CLOSE(805)


   RETURN        

END SUBROUTINE Get_Fidelity


!> This routine computes the reduced wave function after detecting K particles at fixed positions
SUBROUTINE Get_PartialDetection_Fix(VIN0,PSI,Time,Mode,c_ref_x,c_ref_y,c_ref_z,NDetect)

   USE Global_Parameters, ONLY:NDVR_X,NDVR_Y,NDVR_Z,Morb
   USE Coefficients_Parameters, ONLY:NPar,Nconf
   USE Auxiliary_Routines
   USE Input_Output, ONLY:Close_binary,Open_Binary_Reduced_WF,Write_Header, &
                          Write_Orbitals_Binary,Write_Coefficients_Binary

   IMPLICIT NONE

   COMPLEX*16, INTENT(in) :: VIN0(NConf)
   COMPLEX*16, INTENT(in) :: PSI(NDVR_X*NDVR_Y*NDVR_Z,Morb)

   REAL*8,     INTENT(in) :: Time
   REAL*8,     INTENT(in) :: c_ref_x(20),c_ref_y(20),c_ref_z(20)

   INTEGER,    INTENT(in) :: Mode, NDetect

   COMPLEX*16, ALLOCATABLE :: VIN(:), VRED(:)

   REAL*8  :: mom_x(NDVR_X), mom_y(NDVR_Y), mom_z(NDVR_Z)
   REAL*8  :: W, xnrm, DZNRM2

   INTEGER*8 :: M_0(20)
   INTEGER   :: NConf_current, Nconf_red, Nconf_red_next

   EXTERNAL DZNRM2

   CALL Get_WeightOrMomentumGrid(mom_x,mom_y,mom_z,w,1,Mode)

   IF ((Npar-1) .ge. 1) THEN
     ALLOCATE(VRED(nint(BinomialCoefficient(Npar+Morb-2,Npar-1))))
   ELSE
     ALLOCATE(VRED(1))
   ENDIF

   IF( .not. allocated(VIN)) ALLOCATE(VIN(size(VIN0)))

   VIN = VIN0

   DO k = 1, NDetect
     CALL Get_m_0_from_xyz(M_0(k),c_ref_x(k),c_ref_y(k),c_ref_z(k))
   ENDDO

   DO k = 1, NDetect

     WRITE(6,*) '--------------------------------------------'
     WRITE(6,*) 'Particle detected',k
     WRITE(6,*) '--------------------------------------------'

     IF (Morb .ne. 1) THEN
       Nconf_Current = nint(BinomialCoefficient(Npar+Morb-INT(k,4),Npar-INT(k,4)+1))
     ELSE
       NConf_Current = 1
     ENDIF

     IF ((Npar-k+1) .ge. 1) THEN
       Nconf_Red = nint(BinomialCoefficient(Npar+Morb-1-INT(k,4),Npar-INT(k,4)))
       IF ((Npar-k) .gt. 0) THEN
         Nconf_Red_Next = nint(BinomialCoefficient(Npar+Morb-INT(k,4)-2,Npar-INT(k,4)-1))
       ENDIF
     ENDIF

     IF( .not. allocated(VRED)) THEN
       ALLOCATE(VRED(Nconf_Red))
     ENDIF

     CALL Get_Reduced_Coefficients(VIN,VRED,PSI(M_0(k),:),Npar-INT(k,4),&
                                   Nconf_current,NConf_Red)

     DEALLOCATE(VIN)
     ALLOCATE(VIN(NConf_Red))

     VIN = VRED(:)

     IF(allocated(VRED)) DEALLOCATE(VRED)
     ALLOCATE(VRED(NConf_Red_Next))

   ENDDO

! -------------------------- writte out data ---------------------------------

   ! CALL Get_Reduced1bodyElements(VRED,Nconf_Red,Rho1_Elements,Npar-K)

   ! ASK AXEL HOW TO COMPUTE Rho2_Elements !!!!!!!
   ! CALL Get_Reduced2bodyElements(VRED,Nconf_Red,Rho2_Elements,Npar-K)

   ! ASK AXEL HOW TO COMPUTE Energy !!!!!!!
   ! CALL Get_Energy ...

!   CALL Open_binary_Reduced_WF()

!   CALL Write_Header(777,NPar-NDetect)     
!   CALL Write_Header(778,NPar-NDetect)     

   WRITE(6,*) 'CARREFULL : 1- and 2-body matrix elements and energy written in '
   WRITE(6,*) 'Binary are NOT the ones of the reduced wavefunction !!!        '
   WRITE(6,*) ''
   WRITE(6,*) 'CARREFULL : 1- and 2-body matrix elements and energy written in '
   WRITE(6,*) 'Binary are NOT the ones of the reduced wavefunction !!!        '
   WRITE(6,*) ''
   WRITE(6,*) 'CARREFULL : 1- and 2-body matrix elements and energy written in '
   WRITE(6,*) 'Binary are NOT the ones of the reduced wavefunction !!!        '
   WRITE(6,*) ''

!   CALL Write_Orbitals_Binary(time,1,PSI) 

   ! Normalized CI vector     
   xnrm = DZNRM2(NConf_Red,VIN,1)
   VIN  = VIN/xnrm

   CALL Write_Coefficients_Binary(0d0,int(1,8),VIN,NConf_Red,901)

   CALL Close_binary(0)

   IF(allocated(VIN)) DEALLOCATE(VIN)
   IF(allocated(VRED)) DEALLOCATE(VRED)

END SUBROUTINE Get_PartialDetection_Fix


!------------------------------------------------------------------

!> This routine computes the reduced wave function after detecting K particles
SUBROUTINE Get_PartialDetection(VIN0,PSI,Time,Mode,NDetect)

   USE Global_Parameters, ONLY:NDVR_X,NDVR_Y,NDVR_Z,Morb
   USE Coefficients_Parameters, ONLY:NPar,Nconf
   USE Auxiliary_Routines
   USE Input_Output, ONLY:Close_binary,Open_Binary_Reduced_WF,Write_Header, &
                          Write_Orbitals_Binary,Write_Coefficients_Binary

   IMPLICIT NONE

   COMPLEX*16, INTENT(in) :: VIN0(NConf)
   COMPLEX*16, INTENT(in) :: PSI(NDVR_X*NDVR_Y*NDVR_Z,Morb)

   REAL*8,     INTENT(in) :: Time

   INTEGER,    INTENT(in) :: Mode, NDetect

   COMPLEX*16, ALLOCATABLE :: VIN(:), VRED(:)
   REAL*8,     ALLOCATABLE :: Samples(:,:)
   INTEGER*8,  ALLOCATABLE :: Sample_Indexes(:)

   COMPLEX*16              :: Reduced_OneBodyElements(Rdim)

   REAL*8  :: mom_x(NDVR_X), mom_y(NDVR_Y), mom_z(NDVR_Z)
   REAL*8  :: W, xnrm, DZNRM2, SingleShot(NDVR_X*NDVR_Y*NDVR_Z)

   INTEGER*8 :: M_0(20)
   INTEGER   :: NConf_current, Nconf_red, Nconf_red_next

   external DZNRM2

   SingleShot = 0d0

    CALL Get_WeightOrMomentumGrid(mom_x,mom_y,mom_z,w,1,Mode)

   IF ((Npar-1) .ge. 1) THEN
     ALLOCATE(VRED(nint(BinomialCoefficient(Npar+Morb-2,Npar-1))))
   ELSE
     ALLOCATE(VRED(1))
   ENDIF

   IF( .not. allocated(VIN)) ALLOCATE(VIN(size(VIN0)))
   ALLOCATE(Samples(NDetect,DIM_MCTDH))
   ALLOCATE(Sample_Indexes(NDetect))

   VIN = VIN0

   WRITE(6,*) '--------------------------------------------'
   WRITE(6,*) 'Particle detected 1'
   WRITE(6,*) '--------------------------------------------'

   Reduced_OneBodyElements = dcmplx(0.d0,0.d0)
   CALL DrawFromDensity(PSI,Rho1_Elements,Samples(1,:), &
                        Sample_Indexes(1)) ! initialize rho_0

   DO k = 1, NDetect-1

     WRITE(6,*) '--------------------------------------------'
     WRITE(6,*) 'Particle detected', k + 1
     WRITE(6,*) '--------------------------------------------'

     IF (Morb .ne. 1) THEN
       Nconf_Current = nint(BinomialCoefficient(Npar+Morb-INT(k,4),Npar-INT(k,4)+1))
     ELSE
       NConf_Current = 1
     ENDIF

     IF ((Npar-k+1) .ge. 1) THEN
       Nconf_Red = nint(BinomialCoefficient(Npar+Morb-1-INT(k,4),Npar-INT(k,4)))
       IF ((Npar-k) .gt. 0) THEN
         Nconf_Red_Next = nint(BinomialCoefficient(Npar+Morb-INT(k,4)-2,Npar-INT(k,4)-1))
       ENDIF
     ENDIF

     IF( .not. allocated(VRED)) THEN
       ALLOCATE(VRED(Nconf_Red))
     ENDIF

     IF (Morb .ne. 1) THEN
       WRITE(6,*) Npar-k, Nconf_current, NConf_Red
       CALL Get_Reduced_Coefficients(VIN,VRED,PSI(Sample_Indexes(k),:),Npar-INT(k,4),& ! Npar-k+1
                                     Nconf_current,NConf_Red)

       ! Normalized reduced CI vector     
       xnrm = DZNRM2(NConf_Red,VRED,1)
       VRED = VRED/xnrm

       Reduced_OneBodyElements = dcmplx(0.d0,0.d0)
       CALL Get_Reduced1bodyElements(VRED,Nconf_Red,&
                                     Reduced_OneBodyElements,Npar-INT(k,4))

     ELSE
       Reduced_OneBodyElements=dcmplx(1.d0*(Npar-k),0.d0)
       VRED = Dcmplx(1.d0,0.d0)
     ENDIF

     CALL DrawFromDensity(PSI,Reduced_OneBodyElements,Samples(k+1,:), &
                          Sample_Indexes(k+1))
 
     DEALLOCATE(VIN)
     ALLOCATE(VIN(NConf_Red))

     VIN = VRED(:)

     IF(allocated(VRED)) DEALLOCATE(VRED)
     ALLOCATE(VRED(NConf_Red_Next))

   ENDDO

! -------------------------- writte out data ---------------------------------

   ! CALL Get_Reduced1bodyElements(VRED,Nconf_Red,Rho1_Elements,Npar-K)

   ! ASK AXEL HOW TO COMPUTE Rho2_Elements !!!!!!!
   ! CALL Get_Reduced2bodyElements(VRED,Nconf_Red,Rho2_Elements,Npar-K)

   ! ASK AXEL HOW TO COMPUTE Energy !!!!!!!
   ! CALL Get_Energy ...

!   CALL Open_binary_Reduced_WF()

!   CALL Write_Header(777,NPar-NDetect)     
!   CALL Write_Header(778,NPar-NDetect)     

   WRITE(6,*) 'CARREFULL : 1- and 2-body matrix elements and energy written in '
   WRITE(6,*) 'Binary are NOT the ones of the reduced wavefunction !!!        '
   WRITE(6,*) ''
   WRITE(6,*) 'CARREFULL : 1- and 2-body matrix elements and energy written in '
   WRITE(6,*) 'Binary are NOT the ones of the reduced wavefunction !!!        '
   WRITE(6,*) ''
   WRITE(6,*) 'CARREFULL : 1- and 2-body matrix elements and energy written in '
   WRITE(6,*) 'Binary are NOT the ones of the reduced wavefunction !!!        '
   WRITE(6,*) ''

!   CALL Write_Orbitals_Binary(time,1,PSI) 

   ! Normalized CI vector     
   xnrm = DZNRM2(NConf_Red,VIN,1)
   VIN  = VIN/xnrm

   CALL Write_Coefficients_Binary(0d0,int(1,8),VIN,NConf_Red,901)
   CALL Close_binary(0)

   DO i = 1, NDetect
     SingleShot(Sample_Indexes(i)) = SingleShot(Sample_Indexes(i))+1
   ENDDO

   IF (Mode .eq. 1) THEN
     OPEN(unit=903,file='Position_detected_particles.dat',form='formatted')
     DO m = 1, NDVR_X*NDVR_Y*NDVR_Z
       CALL get_ijk_from_m(m,NDVR_X,NDVR_Y,i,j,k)
       WRITE(903,6767) ort_X(i), ort_Y(j), ort_Z(k), SingleShot(m)
     ENDDO
   ELSEIF (Mode .eq. 2) THEN
     OPEN(unit=903,file='Momemtum_detected_particles.dat',form='formatted')
     DO m = 1, NDVR_X*NDVR_Y*NDVR_Z
       CALL get_ijk_from_m(m,NDVR_X,NDVR_Y,i,j,k)
       WRITE(903,6767) mom_X(i), mom_Y(j), mom_Z(k), SingleShot(m)
     ENDDO
   ENDIF
 
   CLOSE(903)

   IF(allocated(VIN)) DEALLOCATE(VIN)
   IF(allocated(VRED)) DEALLOCATE(VRED)

   6767 FORMAT(3(F8.3,1X),100000(E25.16))

END SUBROUTINE Get_PartialDetection
!------------------------------------------------------------------

!> This routine computes higher-order correlation functions
SUBROUTINE Get_AnyOrderCorrelations(VIN0,PSI,Time,Mode,c_ref_x,c_ref_y,c_ref_z,order,oned,twod,dilation)
USE Global_Parameters,ONLY:NDVR_X,NDVR_Y,NDVR_Z,Morb
USE Coefficients_Parameters, ONLY: NPar,Nconf
USE Auxiliary_Routines 

IMPLICIT NONE

COMPLEX*16, INTENT(IN) :: VIN0(NConf)
COMPLEX*16, INTENT(IN) :: PSI(NDVR_X*NDVR_Y*NDVR_Z,Morb)

REAL*8, INTENT(IN) :: Time
REAL*8, INTENT(IN) :: c_ref_x(20),c_ref_y(20),c_ref_z(20)

INTEGER, INTENT(IN) :: Mode,order,dilation

LOGICAL, INTENT(IN) :: OneD,TwoD

REAL*8 :: Densities(NDVR_X*NDVR_Y*NDVR_Z,order)
REAL*8 :: Densities_Last(NDVR_X*NDVR_Y*NDVR_Z,NDVR_X*NDVR_Y*NDVR_Z)
REAL*8 :: MOM_X(NDVR_X),MOM_Y(NDVR_Y),MOM_Z(NDVR_Z),W 
REAL*8 :: Dens_NORM(NDVR_X*NDVR_Y*NDVR_Z)

COMPLEX*16, ALLOCATABLE :: VIN(:), VRED(:), VRED_tmp(:)

COMPLEX*16 :: CorrelationFunctions1d(NDVR_X*NDVR_Y*NDVR_Z,order-1)
COMPLEX*16 :: CorrelationFunctions2d(NDVR_X*NDVR_Y*NDVR_Z,NDVR_X*NDVR_Y*NDVR_Z,order-1)
COMPLEX*16 :: Reduced_OneBodyElements(Rdim)

INTEGER   :: NConf_current,Nconf_red,Nconf_red_next
INTEGER*8 :: i,j,k,l,m,n,i_m,i_n,j_m,j_n,k_m,k_n,NOGRIDPOINTS
INTEGER*8 :: M_0(20)


REAL*8  :: NORM, tmp

CHARACTER*1   :: xork
CHARACTER*800 :: FileName,Suffix
Character*800 :: c_ref_x_str(20)

NOGRIDPOINTS=NDVR_X*NDVR_Y*NDVR_Z

CALL Get_WeightOrMomentumGrid(mom_x,mom_y,mom_z,w,dilation,mode)

IF (Mode .eq. 1) THEN
  WRITE(xork,'(A1)') 'x'
ELSEIF (Mode .eq. 2) THEN
  WRITE(xork,'(A1)') 'k'
ENDIF

IF ((Npar-1) .ge. 1) THEN
  ALLOCATE(VRED(nint(BinomialCoefficient(Npar+Morb-2,Npar-1))))
  ALLOCATE(VRED_tmp(nint(BinomialCoefficient(Npar+Morb-2,Npar-1))))
ELSE
  ALLOCATE(VRED(1))
  ALLOCATE(VRED_tmp(1))
ENDIF

IF( .not. allocated(VIN)) ALLOCATE(VIN(size(VIN0)))
VIN = VIN0

Reduced_OneBodyElements = dcmplx(0.d0,0.d0)

CALL Get_Reduced_Density(PSI,Rho1_Elements,Densities(:,1))
Densities(:,1) = Densities(:,1)/w**2

DO K = 1, order
  CALL Get_m_0_from_xyz(M_0(K),c_ref_x(K),c_ref_y(K),c_ref_z(K))
ENDDO

IF (order .ge. 2) THEN
  DO K = 2, order


     WRITE(6,*) '--------------------------------------------'
     WRITE(6,*) 'order K=',K 
     WRITE(6,*) '--------------------------------------------'

    IF (Morb .ne. 1) THEN
      Nconf_Current = nint(BinomialCoefficient(INT(Npar+Morb-1-K+2,4),INT(Npar-K+2,4)))
    ELSE
      NConf_Current = 1
    ENDIF

    IF ((Npar-K+1) .ge. 1) THEN
      Nconf_Red = NINT(BinomialCoefficient(INT(Npar+Morb-1-K+1,4),INT(Npar-K+1,4)))
      IF ((Npar-K) .gt. 0) THEN
        Nconf_Red_Next = NINT(BinomialCoefficient(INT(Npar+Morb-1-K,4),INT(Npar-K,4)))
      ENDIF
    ENDIF

    IF( .not. allocated(VRED)) THEN
      ALLOCATE(VRED(Nconf_Red))
      ALLOCATE(VRED_tmp(Nconf_Red))
    ENDIF

    IF (Morb .ne. 1) THEN

      CALL Get_Reduced_Coefficients(VIN,VRED,PSI(M_0(K-1),:),INT(Npar-K+1,4),&
                                    Nconf_current,NConf_Red)     

      Reduced_OneBodyElements = dcmplx(0.d0,0.d0)
      CALL Get_Reduced1bodyElements(VRED,Nconf_Red,&
                                   Reduced_OneBodyElements,INT(Npar-K+1,4))
      CALL Get_Reduced_Density(PSI,Reduced_OneBodyElements,Densities(:,K))

      Densities(:,K) = Densities(:,K)/w**2  

    ELSE
      WRITE(6,*) "For GP computations, computing higher order density matrices makes no sence, the 1body density is sufficient!!!"
      WRITE(6,*) "For GP computations, computing higher order density matrices makes no sence, the 1body density is sufficient!!!"
      WRITE(6,*) "For GP computations, computing higher order density matrices makes no sence, the 1body density is sufficient!!!"
    ENDIF

    IF (OneD .eqv. .TRUE.) THEN
   
      CorrelationFunctions1d(:,K-1) = 1.d0

      DO M = 1, NOGRIDPOINTS
        CorrelationFunctions1d(M,K-1) = CorrelationFunctions1d(M,K-1)*Densities(M,K)
      ENDDO

    ENDIF

    WRITE(6,*) 'Twod', Twod
    IF (Twod .eqv. .TRUE.) THEN

      DO M = 1,NOGRIDPOINTS
        CALL Get_Reduced_Coefficients(VIN,VRED_tmp,PSI(M,:),INT(Npar-K+1,4),&
                                      Nconf_current,NConf_Red)

        Reduced_OneBodyElements = dcmplx(0.d0,0.d0)

        CALL Get_Reduced1bodyElements(VRED_tmp,Nconf_Red,&
                                     Reduced_OneBodyElements,INT(Npar-K+1,4))

        CALL Get_Reduced_Density(PSI,Reduced_OneBodyElements,Densities_last(:,M))

        Densities_last(:,M) = Densities_last(:,M)/w**2

      ENDDO

      CorrelationFunctions2d(:,:,K-1)=1.d0

      DO M = 1, NOGRIDPOINTS
        DO N = 1, NOGRIDPOINTS
          CorrelationFunctions2d(M,N,K-1)=CorrelationFunctions2d(M,N,K-1)*Densities_last(M,N)!*Densities(N,K-1)
        ENDDO
      ENDDO

    ENDIF

    DEALLOCATE(VIN)
    ALLOCATE(VIN(NConf_Red))

    VIN = VRED(:)

    IF(ALLOCATED(VRED)) DEALLOCATE(VRED)
    IF(ALLOCATED(VRED_tmp)) DEALLOCATE(VRED_tmp)
    ALLOCATE(VRED(NConf_Red_Next))
    ALLOCATE(VRED_tmp(NConf_Red_Next))

! -------------------------- write out data ---------------------------------

    IF (OneD .eqv. .TRUE.) THEN

      IF (K.ge.10) THEN
         WRITE(suffix,6871) '-order-',K,'-Correlations1D'
      ELSEIF (K.lt.10) THEN
         WRITE(suffix,6870) '-order-',K,'-Correlations1D'
      ENDIF

      IF (K .gt. 1) THEN

        DO J = 1, K-1
          WRITE(c_ref_x_str(J),'(F8.3)') c_ref_x(J)
          suffix = trim(suffix)//'x'//trim(adjustl(inttostr(INT(J,4))))//'='//trim(adjustl(c_ref_x_str(J)))
        ENDDO

      ELSE

        IF (K.ge.10) THEN
           WRITE(suffix,6871) '-order-',K,'-Correlations1D'
        ELSEIF (K.lt.10) THEN
           WRITE(suffix,6870) '-order-',K,'-Correlations1D'
        ENDIF

      ENDIF

      suffix = trim(suffix)//'.dat'

      CALL Assemble_OutputFileName(FileName,Time,NPar,Morb,trim(xork),suffix)

      OPEN(unit=982,file=FileName,form='formatted')

      DO M = 1, NOGRIDPOINTS                                                                  
        CALL get_ijk_from_m(M,NOGRIDPOINTS,NDVR_Y,i_m,j_m,k_m)
     
        IF (mode .eq. 1) THEN
          WRITE(982,6767) ort_X(i_m),ort_Y(j_m),ort_Z(k_m),CorrelationFunctions1d(M,K-1),Densities(M,1),(Densities(M_0(J),1),J=1,K-1)
        ELSEIF (Mode .eq. 2) THEN                                                              
          WRITE(982,6767) mom_X(i_m),mom_Y(j_m),mom_Z(k_m),CorrelationFunctions1d(M,K-1),Densities(M,1),(Densities(M_0(J),1),J=1,K-1)
        ENDIF

      ENDDO

      CLOSE(982)

    ENDIF

    IF (TwoD.eqv..TRUE.) THEN

      IF (K.ge.10) THEN
         WRITE(suffix,6871) '-order-',K,'-Correlations2D'
      ELSEIF (K.lt.10) THEN
         WRITE(suffix,6870) '-order-',K,'-Correlations2D'
      ENDIF

      IF (K .gt. 2) THEN

        DO J = 1, K-2
          WRITE(c_ref_x_str(J),'(F8.3)') c_ref_x(J)
          suffix = trim(suffix)//'x'//trim(adjustl(inttostr(INT(J,4))))//'='//trim(adjustl(c_ref_x_str(J)))
        ENDDO

      ELSE

        IF (K.ge.10) THEN
           WRITE(suffix,6871) '-order-',K,'-Correlations2D'
        ELSEIF (K.lt.10) THEN
           WRITE(suffix,6870) '-order-',K,'-Correlations2D'
        ENDIF

      ENDIF

      suffix = trim(suffix)//'.dat'

      CALL Assemble_OutputFileName(FileName,Time,NPar,Morb,trim(xork),suffix)

      OPEN(unit=983,file=FileName,form='formatted')

      DO M = 1,NOGRIDPOINTS
        DO N = 1,NOGRIDPOINTS
          CALL get_ijk_from_m(M,NOGRIDPOINTS,NDVR_Y,i_m,j_m,k_m)
          CALL get_ijk_from_m(N,NOGRIDPOINTS,NDVR_Y,i_n,j_n,k_n)

          IF (mode.eq.1) THEN
            WRITE(983,6768) ort_X(i_m),ort_Y(j_m),ort_Z(k_m),ort_X(i_n),ort_Y(j_n),ort_Z(k_n),CorrelationFunctions2d(M,N,K-1),Densities(M,1),Densities(N,1),(Densities(M_0(J),1),J=1,K-2)
          ELSEIF(mode.eq.2) THEN
            WRITE(983,6768) MOM_X(i_m),MOM_Y(j_m),MOM_Z(k_m),MOM_X(i_n),MOM_Y(j_n),MOM_Z(k_n),CorrelationFunctions2d(M,N,K-1),Densities(M,1),Densities(N,1),(Densities(M_0(J),1),J=1,K-2)
          ENDIF

          IF (mod(n,NDVR_X).eq.0) WRITE(983,*) '                             '

        ENDDO
      ENDDO

      CLOSE(983)

    ENDIF

  ENDDO

ENDIF ! order-IF

IF(allocated(VIN)) DEALLOCATE(VIN)
IF(allocated(VRED)) DEALLOCATE(VRED)
IF(allocated(VRED_tmp)) DEALLOCATE(VRED_tmp)


6767  FORMAT(3(F8.3),100000(E25.16))
6768  FORMAT(6(F8.3),100000(E25.16))
6868  FORMAT(A7,I1,A15,20(A1,I1,A4))
6869  FORMAT(A7,I2,A15,20(A1,I2,A1,F8.3))
6870  FORMAT(A7,I1,A15)
6871  FORMAT(A7,I2,A15)

END SUBROUTINE Get_AnyOrderCorrelations


!> @ingroup singleshots
!> @brief MAIN single shot subroutine. This routine computes single shots, i.e., random deviates from the N-body density matrix.
!>
!> Reference: Single shot algorithm published in DOi:10.1038/nphys3631.
!>
!> **This subroutine**
!> * creates particle resolved single shots, i.e. the array Sample_Indexes that contains the sampled positions 
!> for all the particles in the different single shots
!> * converts that particle-resolved array into a grid-resolved single shot array that contains the number of particles
!> for the (grid points/) positions in the different single shots
!> * saves the single shot results in an output file
!> 
!> @param[in] VIN0 : the coefficients, as generated in the main run
!> @param[in] PSI : the orbitals, as generated in the main run
!> @param Time : time of propagation at which the wave function is analyzed, only used for 
!> output file generation
!> @param Mode : Mode=1 means REAL-space, Mode=2 means momentum space. 
!> @param CentreOfMass : Whether to compute samples of centre-of-mass operator.
!> @param ShotVariance : Whether to compute integrated variance of single shots.
!> @param Binning : Whether to compute even-odd imbalance histogram
!> @param BinSplit : Whether to use random number generator to fix problems at bin border
!> @param FDF : Whether to compute the full distribution function
!>
!> @return 
!> Does not return anything and instead generates one or many output files: 
!> * SingleShots.dat
!> * SingleShotAverage.dat
!> * SingleShotVariance.dat
!> * CentreOfMass.dat
!> * Binning.dat
!> Which files are generated can be specified in the input.
SUBROUTINE Get_SingleShot(VIN0,PSI,Time,Mode,CentreOfMass,ShotVariance,Binning,BinSplit,FDF)

USE Global_Parameters,ONLY:NDVR_X,NDVR_Y,NDVR_Z,Morb,DIM_MCTDH
USE Coefficients_Parameters, ONLY: NPar,Nconf
USE Auxiliary_Routines 
USE RanDOm

IMPLICIT NONE

COMPLEX*16, INTENT(IN) :: VIN0(NConf)
COMPLEX*16, INTENT(IN) :: PSI(NDVR_X*NDVR_Y*NDVR_Z,MOrb)

REAL*8 :: Time

INTEGER :: Mode
INTEGER :: NBins_Binning,ThisBin 

LOGICAL :: CentreOfMass,ShotVariance,Binning,BinSplit,FDF

INTEGER*8, ALLOCATABLE :: Binning_Histogram(:,:)
REAL*8, ALLOCATABLE :: Binning_BinStarts(:)
REAL*8, ALLOCATABLE :: Binning_BinStops(:)

REAL*8, ALLOCATABLE  :: Samples(:,:,:)
INTEGER*8, ALLOCATABLE :: Sample_Indexes(:,:)
INTEGER*8, ALLOCATABLE :: FDF_Histogram(:,:)
COMPLEX*16 :: Reduced_OneBodyElements(Rdim)
REAL*8, ALLOCATABLE :: SingleShot(:,:)
REAL*8 :: Average(NDVR_X*NDVR_Y*NDVR_Z),Variance
REAL*8,ALLOCATABLE     :: SingleShotVariance(:,:)

REAL*8 :: COM,NORM,W,MOM_X(NDVR_X),MOM_Y(NDVR_Y),MOM_Z(NDVR_Z),Delta1,Delta2
REAL*8 :: RanDOm_Bin
INTEGER*8 :: I,J,K,M,L
INTEGER :: NCONF_RED,NCONF_Current,Nconf_Red_Next,NShots_Or_NSamples
INTEGER*8 :: COM_index,iCOMx(Npar),iCOMy(NPar),iCOMz(NPar), COMx,COMy,COMz
INTEGER, ALLOCATABLE :: CentreOfMass_Histogram(:)
CHARACTER*1 :: xork
CHARACTER*80 :: FileName

ALLOCATE(SingleShot(NDVR_X*NDVR_Y*NDVR_Z,NShots))
SingleShot=0
ALLOCATE(Samples(Npar,DIM_MCTDH,NShots))
Samples=0
ALLOCATE(Sample_Indexes(Npar,NShots))
Sample_Indexes=0


IF (CentreOfMass .eqv. .TRUE.) THEN
  ALLOCATE(CentreOfMass_Histogram(NDVR_X*NDVR_Y*NDVR_Z))
  CentreOfMass_Histogram = 0
ENDIF 

IF (Binning .eqv. .TRUE.) THEN
  IF (DIM_MCTDH.gt.1) THEN
    WRITE(6,*) "Binning imbalance not implemented for D>1 dimension yet"
    RETURN
  ENDIF

  K = nint(abs(x_initial+Binning_Offset-x_final)/Binning_Period)
  NBins_Binning = K

  ALLOCATE(Binning_Histogram(K+1,NShots))
  ALLOCATE(Binning_BinStarts(K+1))
  ALLOCATE(Binning_BinStops(K+1))
  
  Binning_BinStarts(1) = x_initial+Binning_Offset
  Binning_BinStops(K)  = x_final

  I = 1

  DO K = 1, NDVR_X
    IF (Ort_X(K) .ge. x_initial+Binning_Offset+I*Binning_Period) THEN
      I = I + 1
      Binning_BinStarts(I)  = Ort_X(K)
      Binning_BinStops(I-1) = Ort_X(K-1)
    ENDIF
  ENDDO

  Binning_Histogram = 0

ENDIF

IF (FDF.eqv..TRUE.) THEN
  ALLOCATE(FDF_Histogram(Npar+1,NDVR_X*NDVR_Y*NDVR_Z))
ENDIF


!------------ SingleShot evaluation --------------
!$OMP PARALLEL 
!$OMP DO PRIVATE(I)
DO I = 1, NShots

  ! CentreOfMass_Histogram must be reduced at the END of OMP
   CALL Get_SingleShot_Core(VIN0,PSI,&
                          & Sample_Indexes(:,I),Samples(:,:,I))

ENDDO
!$OMP ENDDO
!$OMP END PARALLEL

!-------------------------------------------------

DO I = 1, NShots

  IF (CentreOfMass .eqv. .TRUE.) THEN

    DO L = 1, Npar
      CALL get_ijk_from_m(Sample_Indexes(L,I),NDVR_X,NDVR_Y,iCOMx(L),iCOMy(L),iCOMz(L))
    ENDDO

    COMx = nint(dble(sum(iCOMx(:))/dble(NPar)))
    COMy = nint(dble(sum(iCOMy(:))/dble(NPar)))
    COMz = nint(dble(sum(iCOMz(:))/dble(NPar)))

    COM_index = COMx + NDVR_X*(COMy-1) + NDVR_X*NDVR_Y*(COMz-1)
    CentreOfMass_Histogram(COM_index) = CentreOfMass_Histogram(COM_index) + 1

  ENDIF

  IF (Binning.eqv..TRUE.) THEN

    DO L=1,Npar
      CALL get_ijk_from_m(Sample_Indexes(L,I),NDVR_X,NDVR_Y,iCOMx(L),iCOMy(L),iCOMz(L))
    ENDDO

    DO J=1,NPar
       DO M=1,NBins_Binning
            
         IF (BinSplit.eqv..TRUE.) THEN
          
           IF (((Ort_X(iCOMx(J)).gt.Binning_BinStarts(M)).and. &
               (Ort_X(iCOMx(J)).le.Binning_BinStops(M))).or.   &
              ((Ort_X(iCOMx(J)).eq.Binning_BinStarts(M)).and.(M.eq.1))) THEN

               Binning_Histogram(M,I)=Binning_Histogram(M,I)+1
           ENDIF

          IF ((Ort_X(iCOMx(J)).eq.Binning_BinStarts(M)).and.(M.ne.1)) THEN

             CALL gnu_rand_Burkardt(RanDOm_Bin)

             IF (RanDOm_Bin.lt.0.5d0) THEN
                Binning_Histogram(M-1,I)=Binning_Histogram(M-1,I)+1
             ELSE
                Binning_Histogram(M,I)=Binning_Histogram(M,I)+1
             ENDIF

          ENDIF
 
         ELSEIF (BinSplit.eqv..FALSE.) THEN

           IF ((Ort_X(iCOMx(J)).ge.Binning_BinStarts(M)).and. &
               (Ort_X(iCOMx(J)).le.Binning_BinStops(M))) THEN 
 
              Binning_Histogram(M,I)=Binning_Histogram(M,I)+1
 
           ENDIF

         ENDIF


       ENDDO
    ENDDO 
   

  ENDIF
ENDDO

!
!---------- construct single shots out of Sample indexes array-------
DO I = 1, NShots 

  IF (SingleShot_Convolution .eqv. .TRUE.) THEN
    CALL Convolute_SingleShot(SingleShot(:,I),Sample_Indexes(:,I))
  ELSE

    DO L = 1, NPar
      SingleShot(Sample_Indexes(L,I),I) = SingleShot(Sample_Indexes(L,I),I) + 1
    ENDDO

  ENDIF
ENDDO

DO I = 1, NDVR_X*NDVR_Y*NDVR_Z
  Average(I)= sum(SingleShot(I,:))/NShots
ENDDO


IF (Mode .eq. 1) THEN
  WRITE(xork,'(A1)') 'x'
ELSEIF (Mode.eq.2) THEN
  WRITE(xork,'(A1)') 'k'
ENDIF

!---------- WRITE Single Shot average -------
CALL Assemble_OutputFileName(FileName,Time,NPar,Morb,trim(xork),'SingleShotAverage.dat')
OPEN(unit=982,file=FileName,form='formatted')
CALL Get_WeightOrMomentumGrid(mom_x,mom_y,mom_z,w,dilation,mode)

DO M = 1, NDVR_X*NDVR_Y*NDVR_Z

  CALL get_ijk_from_m(M,NDVR_X,NDVR_Y,i,j,k)

  IF (mode .eq. 1) THEN
     WRITE(982,6767) ort_X(i),ort_Y(j),ort_Z(k),Average(M)
  ELSEIF ((mode .eq. 2) .OR. (mode .eq. 3)) THEN
     WRITE(982,6767) mom_X(i),mom_Y(j),mom_Z(k),Average(M) 
  ENDIF

  IF (mod(I,NDVR_X).eq.0) WRITE(982,*) '                             '

ENDDO

CLOSE(982)

!---------- WRITE Single Shot -------
CALL Assemble_OutputFileName(FileName,Time,NPar,Morb,trim(xork),'SingleShots.dat')
OPEN(unit=985,file=FileName,form='formatted')

DO M = 1, NDVR_X*NDVR_Y*NDVR_Z
  CALL get_ijk_from_m(M,NDVR_X,NDVR_Y,i,j,k)

  IF (mode .eq. 1) THEN
    WRITE(985,6767) ort_X(i),ort_Y(j),ort_Z(k),(SingleShot(M,L),L=1,NShots)
  ELSEIF ((mode .eq. 2) .OR. (mode .eq. 3)) THEN
    WRITE(985,6767) mom_X(i),mom_Y(j),mom_Z(k),(SingleShot(M,L),L=1,NShots)
  ENDIF

  IF (mod(i,NDVR_X).eq.0) WRITE(985,*) '                             '
ENDDO

CLOSE(985)

!---------- WRITE Single Shot Variance -------
IF (ShotVariance .eqv. .TRUE.) THEN

  Variance = 0.d0
  DO I = 1, NDVR_X*NDVR_Y*NDVR_Z
    Variance = Variance + sum((SingleShot(I,:)-Average(I))**2)/NShots
  ENDDO

  CALL Assemble_OutputFileName(FileName,Time,NPar,Morb,trim(xork),'SingleShotVariance.dat')
  OPEN(unit=982,file=FileName,form='formatted')
  WRITE(982,*) Variance
  CLOSE(982)

ENDIF

!---------- WRITE Single Shot Center Of Mass -------
IF (CentreOfMass .eqv. .TRUE.) THEN
  CALL Assemble_OutputFileName(FileName,Time,NPar,Morb,trim(xork),'CentreOfMass.dat')
  OPEN(unit=985,file=FileName,form='formatted')

  DO M = 1, NDVR_X*NDVR_Y*NDVR_Z
    CALL get_ijk_from_m(M,NDVR_X,NDVR_Y,i,j,k)

    IF (mode .eq. 1) THEN
      WRITE(985,6767) ort_X(i),ort_Y(j),ort_Z(k),DBLE(CentreOfMass_Histogram(M))
    ELSEIF ((mode .eq. 2) .or. (mode .eq. 3)) THEN
      WRITE(985,6767) mom_X(i),mom_Y(j),mom_Z(k),DBLE(CentreOfMass_Histogram(M))
    ENDIF 

    IF (mod(i,NDVR_X).eq.0) WRITE(985,*) '                             '
  ENDDO

  CLOSE(985)
ENDIF

!---------- WRITE Single Shot Binning -------
IF (Binning .eqv. .TRUE.) THEN
  CALL Assemble_OutputFileName(FileName,Time,NPar,Morb,trim(xork),'Binning.dat')
  OPEN(unit=986,file=FileName,form='formatted', ACCESS = "APPEND" , STATUS= "REPLACE")

  DO M = 1, NBins_Binning
    WRITE(986,6161) Binning_BinStarts(M),Binning_BinStops(M),SUM(Binning_Histogram(M,:)),(Binning_Histogram(M,L),L=1,NShots)
  ENDDO

  CLOSE(986)
ENDIF

IF (FDF.eqv..TRUE.) THEN

  IF (SingleShot_Convolution .eqv. .TRUE.) THEN

    SingleShot=0

    DO I=1,NShots
       DO L=1,NPar
          SingleShot(Sample_Indexes(L,I),I)= SingleShot(Sample_Indexes(L,I),I)+1
       ENDDO
    ENDDO

  ENDIF

  FDF_Histogram=0

  DO M=1,NDVR_X*NDVR_Y*NDVR_Z
     DO J=1,NShots
        FDF_Histogram(nint(SingleShot(M,J))+1,M)=FDF_Histogram(nint(SingleShot(M,J))+1,M)+1
     ENDDO
  ENDDO


  
  CALL Assemble_OutputFileName(FileName,Time,NPar,Morb,trim(xork),'FullDist.dat')
  OPEN(unit=986,file=FileName,form='formatted', ACCESS = "APPEND" , STATUS= "REPLACE")

  DO M=1,NDVR_X*NDVR_Y*NDVR_Z
     CALL get_ijk_from_m(M,NDVR_X,NDVR_Y,i,l,k)

     DO J=1,NPar+1
        IF (mode.eq.1) THEN
           WRITE(986,6766) J-1, Ort_X(i), Ort_Y(l), Ort_Z(k), FDF_Histogram(J,M)
        ELSE
           WRITE(986,6766) J-1, Mom_X(i), Mom_Y(l), Mom_Z(k), FDF_Histogram(J,M)
        ENDIF
     ENDDO
     WRITE(986,*) "                       "
  ENDDO

   CLOSE(986)

ENDIF


IF(allocated(CentreOfMass_Histogram)) DEALLOCATE(CentreOfMass_Histogram)
DEALLOCATE (Sample_Indexes)
DEALLOCATE (Samples)

6161 FORMAT(2(F16.9,1X),100000(I10))
6766 FORMAT(I10,3(F16.9,1X),I10)
6767 FORMAT(3(F16.9,1X),100000(F16.9))

END SUBROUTINE Get_SingleShot

!> @ingroup singleshots
!> @brief Loops over the different particles for one given single shot and samples their position or momentum.
!> 
!> @param[in] VIN0 : the coefficients, as generated in the main run
!> @param[in] PSI : the orbitals, as generated in the main run
!> @param[out] Sample_Indexes : saves sampled grid points for all the particles
!> @param[out] Samples : grid point values
SUBROUTINE Get_SingleShot_Core(VIN0,PSI,&
                             & Sample_Indexes,Samples)

USE Global_Parameters,       ONLY: NDVR_X,NDVR_Y,NDVR_Z,Morb,Job_Type
USE Coefficients_Parameters, ONLY: NPar,Nconf
USE Auxiliary_Routines 

IMPLICIT NONE

COMPLEX*16, INTENT(in) :: VIN0(NConf)
COMPLEX*16, INTENT(in) :: PSI(NDVR_X*NDVR_Y*NDVR_Z,Morb)


INTEGER*8,    INTENT(out) :: Sample_Indexes(Npar) !(Npar,DimSamp3)
REAL*8,     INTENT(out) :: Samples(Npar,DIM_MCTDH) !(Npar,DIM_MCTDH,DimSamp3)

COMPLEX*16, ALLOCATABLE :: VIN(:), VRED(:)
COMPLEX*16 :: Reduced_OneBodyElements(Rdim)

REAL*8 :: NORM

INTEGER :: J, K, M, L, NCONF_RED, NCONF_Current, Nconf_Red_Next

!INTEGER :: myid, OMP_GET_THREAD_NUM

  ALLOCATE(VIN(size(VIN0)))
  VIN = VIN0
  Reduced_OneBodyElements=dcmplx(0.d0,0.d0)
  CALL DrawFromDensity(PSI,Rho1_Elements,Samples(1,:), &
                       Sample_Indexes(1)) ! initialize rho_0


  DO K = 1, Npar-1
    SELECT CASE (Job_Type)

      CASE ('BOS','FCI')

        IF (Morb .ne. 1) THEN
          Nconf_Current = nint(BinomialCoefficient(Npar+Morb-1-K+1,Npar-K+1))
        ELSE
          NConf_Current = 1
        ENDIF

        Nconf_Red      = nint(BinomialCoefficient(Npar+Morb-1-K,Npar-K))
        Nconf_Red_Next = nint(BinomialCoefficient(Npar+Morb-2-K,Npar-K-1))

      CASE ('FER')

        IF (Morb .ne. 1) THEN
          Nconf_Current = nint(BinomialCoefficient(Morb,Npar-K+1))
        ELSE
          NConf_Current = 1
        ENDIF

        Nconf_Red      = nint(BinomialCoefficient(Morb,Npar-K))
        Nconf_Red_Next = nint(BinomialCoefficient(Morb,Npar-K-1))
   
    END SELECT 
 
    IF (.not. allocated(VRED)) THEN
      ALLOCATE(VRED(Nconf_Red))
    ENDIF

    IF (Morb .ne. 1) THEN

      SELECT CASE (Job_Type)

        CASE ('BOS','FCI')
          CALL Get_Reduced_Coefficients(VIN,VRED,PSI(Sample_Indexes(K),:),Npar-K,&
                                        Nconf_current,NConf_Red)      

        CASE ('FER')
!          WRITE(6,*) '---------------------' 
!          WRITE(6,*) size(VIN), size(VRED), size(PSI(Sample_Indexes(K,I),:))
!          WRITE(6,*) Npar-K,Nconf_current,NConf_Red 
!          WRITE(6,*) '---------------------' 
           CALL Get_Reduced_Coefficients_Fermions(VIN,VRED,PSI(Sample_Indexes(K),:),Npar-K,&
                                                 Nconf_current,NConf_Red)      
  
!          WRITE(6,*) '---------------------' 
!          WRITE(6,*) VRED 
!          WRITE(6,*) '---------------------' 
      END SELECT
     
      NORM = dsqrt(dble(sum(dconjg(VRED(:))*VRED(:))))
      VRED = VRED/NORM
  
      Reduced_OneBodyElements = dcmplx(0.d0,0.d0)

      select case (Job_Type)
    
         case ('BOS','FCI')
            CALL Get_Reduced1bodyElements(VRED,Nconf_Red,&
                                          Reduced_OneBodyElements,Npar-K)
        case ('FER') 
            CALL Get_Reduced1bodyElements_Fermions(VRED,Nconf_Red,&
                                          Reduced_OneBodyElements,Npar-K)
!          WRITE(6,*) '---------------------' 
!          WRITE(6,*) VRED 
!          WRITE(6,*) '---------------------' 
!          WRITE(6,*) Reduced_OneBodyElements 
!          WRITE(6,*) '---------------------' 
        END select 
    ELSE
      Reduced_OneBodyElements=dcmplx(1.d0*(Npar-K),0.d0)
      VRED = Dcmplx(1.d0,0.d0)         
    ENDIF

    CALL DrawFromDensity(PSI,Reduced_OneBodyElements,Samples(K+1,:), &
                         Sample_Indexes(K+1))

    DEALLOCATE(VIN)
    ALLOCATE(VIN(NConf_Red))
   
    VIN = VRED
   
    IF(allocated(VRED)) DEALLOCATE(VRED)
    ALLOCATE(VRED(NConf_Red_Next))

  ENDDO   ! END loop K = 1, NPar-1

  IF(allocated(VIN)) DEALLOCATE(VIN)
  IF(allocated(VRED)) DEALLOCATE(VRED)

END SUBROUTINE Get_SingleShot_Core


SUBROUTINE WRITE_HamiltonianElementsAnalysis(PSI,Time,GetA)

USE Matrix_Elements
USE Addresses
USE LocalInteractionPotential

IMPLICIT NONE

LOGICAL, OPTIONAL :: GetA
INTEGER :: FromN,TillN
INTEGER ::  ierr,ind
Complex*16 :: Adiag(Morb), Apair((Morb+1)*(Morb-1)/2) 
REAL*8 :: tm
REAL*8 :: Time,WTime
COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb)  :: PSI,OneBody_PSI
COMPLEX*16, DIMENSION(Morb,Morb,Morb,Morb) :: TwoBody_Elements
INTEGER*8 :: K,S,L,Q
INTEGER  ::  Iorb,Jorb,icntr,ii,n11,SL,P,I,J,n1
COMPLEX*16,DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb*(Morb+1)/2) :: ALL_WSL
COMPLEX*16 :: Z,ZZ,ZDOTC,DZNRM2
Character*80 :: fname
Character*10 :: timeAsString

EXTERNAL ZDOTC,DZNRM2

OneBody_PSI=PSI
WTime=Time

FromN=1
TillN=Morb*(Morb+1)/2

SLloop: DO SL=FromN,TillN
  P=TERM_INDEX_1B(SL)
  L= INT(P/100)
  S= P-L*100
  CALL Get_Local_InteractionPotentials(ALL_WSL(:,SL-FromN+1),psi(:,S),psi(:,L),WTime)
ENDDO SLloop

Interaction_Elements=Zero
DO ii=1, NDVR_X*NDVR_Y*NDVR_Z
  SLloop2: DO SL=FromN,TillN
    P=TERM_INDEX_1B(SL)
    L= INT(P/100)
    S= P-L*100
    Qloop2: DO Q=1,Morb
       Kloop2:  DO K=1,Morb
          n1= ABS(Nadr(K,S,Q,L,Morb))
          n11= Nadr(K,S,Q,L,Morb)

          IF(n11.ge.0)  THEN 
             Interaction_Elements(n1)=Interaction_Elements(n1)&
                                     +DCONJG(PSI(ii,K))&
                                     *ALL_WSL(ii,SL-FromN+1)&
                                     *PSI(ii,Q)
          ENDIF

          IF(L.eq.S) cycle Kloop2

          n1= ABS(Nadr(K,L,Q,S,Morb))
          n11=    Nadr(K,L,Q,S,Morb)

          IF(n11.ge.0) THEN
               Interaction_Elements(n1)=Interaction_Elements(n1)&
                                       +DCONJG(PSI(ii,K))&
                                       *Conjg(ALL_WSL(ii,SL-FromN+1))&
                                       *PSI(ii,Q)
          ENDIF

      ENDDO Kloop2
    ENDDO Qloop2
  ENDDO SLloop2
ENDDO
Call Get_Full_Wijkl(TwoBody_Elements)

Call Get_KineticEnergyAction_AllOrbitalsAnalysis(OneBody_PSI,.TRUE.,Time)

icntr=1
DO Iorb=1,Morb
   DO Jorb=Iorb,Morb
       Compact_1bodyHamiltonian_Elements(icntr) = ZDOTC(NDVR_X*NDVR_Y*NDVR_Z,PSI(:,Iorb),1,OneBody_PSI(:,Jorb),1)
       Full_1bodyHamiltonian_Elements(Iorb,Jorb) = Compact_1bodyHamiltonian_Elements(icntr)
       Full_1bodyHamiltonian_Elements(Jorb,Iorb) = Conjg(Full_1bodyHamiltonian_Elements(Iorb,Jorb))
       icntr=icntr+1
   ENDDO
ENDDO

Call WRITE_MatrixElements(Full_1bodyHamiltonian_Elements,TwoBody_Elements,Time,.TRUE.)

IF (Present(GetA).and.(GetA.eqv..TRUE.)) THEN

 Adiag=dcmplx(0.d0,0.d0)
 Apair=dcmplx(0.d0,0.d0)

 ind=1

 DO I=1,Morb
    DO J=I+1,Morb

       Apair(ind) = TwoBody_Elements(I,I,J,J)

       ind = ind + 1

    ENDDO

    Adiag(I)= TwoBody_Elements(I,I,I,I)

 ENDDO
 CALL getDOubleAsString(time,timeAsString)
 fname=timeAsString//'Adiag.dat'

 OPEN(unit=12,file=trim(fname),form='formatted')
 WRITE(12,'(I4,2E25.16)') (I,Adiag(I),I=1,Morb)
 CLOSE(12)

 fname=timeAsString//'Apair.dat'

 OPEN(unit=12,file=trim(fname),form='formatted')

 ind=1
 DO I=1,Morb
    DO J=I+1,Morb
       WRITE(12,'(2I4,2E25.16)') I,J,Apair(ind)
       ind=ind+1
    ENDDO
 ENDDO

 CLOSE(12)

ENDIF

END SUBROUTINE WRITE_HamiltonianElementsAnalysis


SUBROUTINE WRITE_MatrixElements(Rho1,Rho2,time,HamiltonianElements)

LOGICAL, OPTIONAL :: HamiltonianElements

COMPLEX*16, DIMENSION(MORB,MORB)                 :: Rho1
COMPLEX*16, DIMENSION(MORB,MORB,MORB,MORB)       :: Rho2

REAL*8 :: time

Character*80 :: fname1,fname2
Character*10 :: timeAsString
INTEGER :: I,J,K,L

CALL getDOubleAsString(time,timeAsString)
IF (Present(HamiltonianElements).and.(HamiltonianElements.eqv..TRUE.)) THEN
  fname1=timeAsString//'OneBodyH-Elements.dat'
  fname2=timeAsString//'TwoBodyH-Elements.dat'
ELSE
  fname1=timeAsString//'Rho1-Elements.dat'
  fname2=timeAsString//'Rho2-Elements.dat'
ENDIF

OPEN(unit=12,file=trim(fname1),form='formatted')
OPEN(unit=13,file=trim(fname2),form='formatted')

DO I=1,Morb
 DO J=1,Morb

 WRITE(12,6969) I,J,Rho1(I,J)

  DO K=1,Morb
   DO L=1,Morb

    WRITE(13,6968) I,J,K,L,Rho2(I,J,K,L)

   ENDDO
  ENDDO
 ENDDO
ENDDO

6969 FORMAT(I4,I4,E25.16,E25.16)
6968 FORMAT(I4,I4,I4,I4,E25.16,E25.16)

CLOSE(12)
CLOSE(13)

END SUBROUTINE WRITE_MatrixElements

!> Compute various types of entropies. The density related entropies,
!> S_a= \int r(a) ln r(a) da ; where a = x,k, the occupations-related
!> entropy S_n= sum_i n_i ln n_i and the coefficients-entropy S_c
!> S_c= sum_{n} |C_{n}|^2 ln |C_{n}|^2 are computed and output.

SUBROUTINE Get_Entropy(VIN,FT_PSI,PSI,rho_jk,TIME,NatOccs,NBody_C_Entropy)
USE DVR_Parameters
USE Coefficients_Parameters
USE Coefficients_Parallelization_Parameters
USE Analysis_Input_Variables,ONLY:Time_From
USE Auxiliary_Routines

LOGICAL, OPTIONAL :: NBody_C_Entropy
COMPLEX*16, DIMENSION(NCONF)                     :: VIN
COMPLEX*16, DIMENSION(MORB,MORB)                 :: Rho_jk
COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,MORB) :: PSI
COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,MORB) :: FT_PSI
REAL*8                                           :: TIME
REAL*8, DIMENSION(MORB)                          :: NatOccs

COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z) :: Density,K_Density
REAL*8                                      :: Density_Entropy,K_Density_Entropy, C_Entropy_Full, C_Entropy,NatOccs_Entropy
REAL*8                                      :: Inverse_ParticipationRatio
INTEGER                                     :: NoGridPoints,I,J,K,N
REAL*8             :: w,mom_X(NDVR_X),mom_Y(NDVR_Y),mom_Z(NDVR_Z), dnorm, knorm


Call Get_WeightOrMomentumGrid(mom_x,mom_y,mom_z,w,1,2)

NOGRIDPOINTS=NDVR_X*NDVR_Y*NDVR_Z

DENSITY   = (0.d0,0.d0)
K_DENSITY   = (0.d0,0.d0)

!> Compute the density and momentum density
DO n=1,NOGRIDPOINTS
   DO j = 1,Morb
      DO k=1,MOrb
         DENSITY(n)   = DENSITY(n) + rho_jk(j,k) * DCONJG(PSI(n,j))*PSI(n,k)/Weight(n)**2
         K_DENSITY(n) = K_DENSITY(n) + rho_jk(j,k) * DCONJG(FT_PSI(n,j))*FT_PSI(n,k)/w**2
      ENDDO
   ENDDO
ENDDO


!> normalize density
dnorm = 0d0
knorm = 0d0
DO n = 1, NOGRIDPOINTS
   dnorm = dnorm + density(n)*weight(n)**2
   knorm = knorm + K_density(n)*w**2
ENDDO

Density_Entropy=0.d0
K_Density_Entropy=0.d0

!> (K-)Density related form of the entropy.
DO n=1,NOGRIDPOINTS

    IF (ABS(density(n)).gt.1.d-20) THEN
       Density_Entropy=Density_Entropy-(dble(DENSITY(n)/dnorm)*LOG(dble(Density(n)/dnorm)))*weight(n)**2
    ENDIF

    IF (ABS(K_density(n)).gt.1.d-20) THEN
       K_Density_Entropy=K_Density_Entropy-(dble(K_DENSITY(n)/knorm)*LOG(dble(K_Density(n)/knorm)))*w**2
    ENDIF

ENDDO

!> Many-Body entropy related to the coefficients.
C_Entropy=0.d0
Inverse_ParticipationRatio=0.d0

DO n=1,Nconf

   IF (ABS(VIN(n)).gt.1.d-20) THEN
      C_Entropy=C_Entropy-(dconjg(VIN(N))*VIN(N)*LOG(dconjg(VIN(N))*VIN(N)))
   ENDIF

   Inverse_ParticipationRatio=Inverse_ParticipationRatio+(dconjg(VIN(N))*VIN(N)*dconjg(VIN(N))*VIN(N))
ENDDO

Inverse_ParticipationRatio=1.d0/Inverse_ParticipationRatio
   
IF (Present(NBody_C_Entropy).and.(NBody_C_Entropy.eqv..TRUE.)) THEN
  DO n=1,Nconf
   DO K=1,Nconf

     IF (ABS(VIN(n)*VIN(K)).gt.1.d-20) THEN
       C_Entropy_Full=C_Entropy_Full &
                     - (sqrt(dconjg(VIN(K))*VIN(K)*dconjg(VIN(N))*VIN(N)) &
                     * LOG(sqrt(dconjg(VIN(K))*VIN(K)*dconjg(VIN(N))*VIN(N))))
     ENDIF

   ENDDO
  ENDDO
ENDIF
   
!> Many-Body entropy related to the natural occupations.
NatOccs_Entropy=0.d0
DO i=1,Morb
   IF (ABS(natoccs(i)).gt.1.d-20) THEN
     NatOccs_Entropy=NatOccs_Entropy-(NatOccs(i)/(1.d0*NPar)*(LOG(NatOccs(i)/(1.d0*NPar))))
   ENDIF
ENDDO
    
    
IF (Present(NBody_C_Entropy).and.(NBody_C_Entropy.eqv..TRUE.)) THEN
!> WRITE different entropies to a file.
 IF (abs(Time-Time_From).lt.0.1d-10) THEN
   OPEN(unit=541,file='Entropy.dat',form='FORMATted',STATUS='UNKNOWN')
   WRITE(541,6146) time,' ',Density_Entropy,' ',K_Density_Entropy,' ',C_Entropy,' ',NatOccs_Entropy,' ',Inverse_ParticipationRatio,' ', C_Entropy_Full
 ELSE
   OPEN(unit=541,file='Entropy.dat',form='FORMATted',STATUS='OLD',ACCESS='APPEND')
   WRITE(541,6146) time,' ',Density_Entropy,' ',K_Density_Entropy,' ',C_Entropy,' ',NatOccs_Entropy,' ',Inverse_ParticipationRatio,' ', C_Entropy_Full
 ENDIF

6146  FORMAT(7(E25.16,a1))
ELSE
!> WRITE different entropies to a file.
 IF (abs(Time-Time_From).lt.0.1d-10) THEN
   OPEN(unit=541,file='Entropy.dat',form='FORMATted',STATUS='UNKNOWN')
   WRITE(541,6145) time,' ',Density_Entropy,' ',K_Density_Entropy,' ',C_Entropy,' ',NatOccs_Entropy,' ',Inverse_ParticipationRatio
 ELSE
   OPEN(unit=541,file='Entropy.dat',form='FORMATted',STATUS='OLD',ACCESS='APPEND')
   WRITE(541,6145) time,' ',Density_Entropy,' ',K_Density_Entropy,' ',C_Entropy,' ',NatOccs_Entropy,' ',Inverse_ParticipationRatio
 ENDIF

6145  FORMAT(6(E25.16,a1))
ENDIF

CLOSE(541)

END SUBROUTINE Get_Entropy

!> Compute the local fragmentation of a wavefunction with respect to border
SUBROUTINE Get_LOC_FRAG(PSI,VIN,TIME)
USE DVR_Parameters
USE Coefficients_Parameters
USE Coefficients_Parallelization_Parameters
USE Analysis_Input_Variables
USE Auxiliary_Routines

COMPLEX*16, DIMENSION(NCONF)                    :: VIN
COMPLEX*16, DIMENSION(Morb,Morb)                :: trunc_rho_ij
COMPLEX*16, DIMENSION(Morb,Morb,Morb,Morb)      :: trunc_rho_ijkl
COMPLEX*16, ALLOCATABLE                         :: PSI_TRUNC(:,:)
COMPLEX*16                                      :: PSI(NDVR_X,MORB)
COMPLEX*16, ALLOCATABLE                         :: PSI_TRUNC_ORTHO(:,:)
COMPLEX*16, DIMENSION(MORB,MORB)                :: TRAFO
COMPLEX*16, DIMENSION(NCONF)                    :: VIN_TRANS

REAL*8, INTENT(IN)          :: TIME
REAL*8, DIMENSION(MOrb)     :: truncated_noccs
REAL*8,DIMENSION(3*MORB-2)  :: RWORK
REAL*8,DIMENSION(MORB*MORB) :: WORK

INTEGER*8     :: ierr, i, j, k, IBORDER
INTEGER       :: INFO

j=0
DO I=1,NDVR_X
   IF (ORT_X(I).le.border_lf) THEN
      j=j+1
   ENDIF
ENDDO

IBORDER=J

!WRITE(6,*) 'Truncation at x=',ORT_X(j)
ALLOCATE(PSI_TRUNC(IBORDER,MORB))
ALLOCATE(PSI_TRUNC_ORTHO(IBORDER,MORB))
DO I=1,IBORDER
   DO K=1,MORB
      PSI_TRUNC(I,K)=PSI(I,K)
   ENDDO
ENDDO

!PSI_TRUNC_ORTHO=PSI_TRUNC
!DO I=1,Morb
!   NORM(I)=1.d0/DSQRT(ABS(ZDOTC(IBORDER,PSI_TRUNC(:,I),1,PSI_TRUNC(:,I),1)))
!   WRITE(6,*)I,"IN norm before renormalization =",NORM(I)
!   CALL ZSCAL(IBORDER,NORM(I),PSI_TRUNC(:,I),1)
!   NORM(I)=1.d0/DSQRT(ABS(ZDOTC(IBORDER,PSI_TRUNC(:,I),1,PSI_TRUNC(:,I),1)))
!   WRITE(6,*)I,"IN norm AFTER renormatization =",NORM(I)
!ENDDO
!PSI_TRUNC_ORTHO=PSI_TRUNC
 
!DO I=1,IBORDER
!   WRITE(177,'(1I4,8E25.16)') I,PSI_TRUNC(I,1),PSI_TRUNC(I,2),PSI_TRUNC(I,3),PSI_TRUNC(I,4)
!ENDDO

!CALL schmidtortho(PSI_TRUNC_ORTHO,IBORDER,MORB,ierr)
!DO I=1,IBORDER
!WRITE(176,'(1I4,8E25.16)') I,PSI_TRUNC_ORTHO(I,1),PSI_TRUNC_ORTHO(I,2),PSI_TRUNC_ORTHO(I,3),PSI_TRUNC_ORTHO(I,4)
!ENDDO
!CALL schmidtortho(PSI_TRUNC_ORTHO,IBORDER,MORB,ierr)

CALL schmidtortho(PSI_TRUNC_ORTHO,IBORDER,INT(MORB,8),ierr)

WRITE(6,*) "NORMALIZATIONS OF ORBS:",((SUM(DCONJG(PSI_TRUNC_ORTHO(:,K))*PSI_TRUNC_ORTHO(:,K))),K=1,MORB)
!ADJOINTTRAFO=TRANSPOSE(DCONJG(TRAFO))

!WRITE(397,*) ((VIN(I)),I=1,NCONF)
CALL U_TRANS(TRAFO,VIN,VIN_TRANS,PSI_TRUNC,PSI_TRUNC_ORTHO,IBORDER,2)
!WRITE(398,*) (VIN_TRANS(I),I=1,NCONF)
!CALL U_TRANS(TRAFO,VIN_TRANS,VIN,PSI_TRUNC_ORTHO,PSI_TRUNC,IBORDER,2)
!WRITE(399,*) (VIN(I),I=1,NCONF)
CALL ZSCAL(NCONF,dcmplx(1.d0,0.d0),VIN_TRANS,1)
 
VIN=VIN_TRANS
!!!!!!!!!!! FROM LR
!!!!CALL Parallel_Init_CI_julian(numprocs)
!!!!!! FROM LR
!!!!CALL HPSI_LR(VIN,VIN_TRANS,NCONF)

CALL Get_Full_Rij(Trunc_Rho_ij)
CALL Get_Full_Rijkl(Trunc_Rho_ijkl)
!WRITE(6,*) "1B-MATRIX ELEMENTS FROM HPSI_LR SUM", SUM(Rho1_Elements)
!WRITE(6,*) "1B-MATRIX ELEMENTS Expanded SUM", SUM(Trunc_Rho_ij)

CALL get_corr_loc_frag(time,J,PSI_TRUNC_ORTHO,trunc_rho_ij,trunc_rho_ijkl)

CALL ZHEEV('V','U',Morb,trunc_rho_ij,Morb,truncated_noccs,WORK,Morb*Morb,&
                                         RWORK,INFO) 
!WRITE(6,*) "ZHEEV INFO;",INFO

OPEN(387,FILE="truncated_occupations.dat", ACCESS = "APPEND", STATUS = "UNKNOWN")

WRITE(387,'(99E25.16)') TIME,((truncated_noccs(k)),k=1,MORB)

CLOSE(387)
DEALLOCATE(PSI_TRUNC)
DEALLOCATE(PSI_TRUNC_ORTHO)

END SUBROUTINE Get_LOC_FRAG


SUBROUTINE Get_Autocorr(PSI0,PSI1,VIN,VIN0,time)
USE Coefficients_Parameters
USE CI_Production_Parameters
USE Interaction_Parameters
USE DVR_Parameters
USE Matrix_Elements

IMPLICIT NONE
COMPLEX*16      :: PSI0(NDVR_X*NDVR_Y*NDVR_Z,MORB)
COMPLEX*16      :: PSI1(NDVR_X*NDVR_Y*NDVR_Z,MORB)
COMPLEX*16      :: VIN(NCONF)
COMPLEX*16      :: VIN0(NCONF)
COMPLEX*16      :: V_TRANS(NCONF)
REAL*8          :: time 

COMPLEX*16      :: TRANS(MORB,MORB)

REAL*8          :: acorr_out
REAL*8          :: acorr_out1
REAL*8          :: acorr_out2

REAL*8          :: DZNRM2
EXTERNAL DZNRM2

CALL U_TRANS(TRANS,VIN,V_TRANS,PSI0,PSI1,NDVR_X*NDVR_Y*NDVR_Z,2)

WRITE(6,*) "autocorr:DZNRM2(V_TRANS)=",DZNRM2(Nconf,V_TRANS,1)
!V_TRANS=V_TRANS/DZNRM2(Nconf,V_TRANS,1)

acorr_out=ABS(SUM(DCONJG(VIN0)*V_TRANS))
acorr_out1=DZNRM2(Nconf,V_TRANS,1)
V_TRANS=V_TRANS/(DZNRM2(Nconf,V_TRANS,1))
acorr_out2=ABS(SUM(DCONJG(VIN0)*V_TRANS))

WRITE(7856,'(4E25.16)') time,acorr_out,acorr_out1,acorr_out2

END SUBROUTINE Get_Autocorr

!> computes the local correlation functions from a truncated wavefunction 
SUBROUTINE get_corr_LOC_FRAG(time,NOGRIDPOINTS,PSI,rho_jk,rho_ijkl) 
!--------------------------------------------------------------------------------------------
USE Coefficients_Parameters,ONLY:Npar
USE DVR_Parameters
  IMPLICIT NONE
!  INTEGER,parameter  :: NOGRIDPOINTS=NDVR_X*NDVR_Y*NDVR_Z
  INTEGER*8 :: NOGRIDPOINTS,N
  INTEGER   :: mode
  REAL*8    :: time
  REAL*8    :: w

  COMPLEX*16, DIMENSION(NOGRIDPOINTS,Morb)  :: PSI
  COMPLEX*16,INTENT(in)                     :: rho_ijkl(Morb,Morb,Morb,Morb)
  COMPLEX*16,INTENT(in) :: rho_jk(MOrb,MOrb)
  CHARACTER(len=2)      :: aString
  CHARACTER(len=200)    :: fullName
  CHARACTER(len=10)     :: timeAsString
  CHARACTER(len=10)     :: MorbAsString
  CHARACTER(len=1)      :: NparAsString1
  CHARACTER(len=2)      :: NparAsString2
  CHARACTER(len=3)      :: NparAsString3
  CHARACTER(len=4)      :: NparAsString4
  CHARACTER(len=5)      :: NparAsString5
  CHARACTER(len=6)      :: NparAsString6
  CHARACTER(len=7)      :: NparAsString7
  CHARACTER(len=8)      :: NparAsString8
  COMPLEX*16            :: G1m_m,G1m_n,G1n_n,G2m_n
                        
  COMPLEX*16            :: maximum,integ,integ2
  REAL*8,parameter      :: TOL=0.00001d0 
  REAL*8     :: threshold
  REAL*8     :: outG1m_m,outReG1m_n,outImG1m_n,outG1n_n,outG2m_n
  REAL*8             :: xi,xf

  integ=0.d0
  integ2=0.d0
  mode=1
  IF ((NDVR_Y.ne.1).or.(NDVR_Z.ne.1)) THEN
      WRITE(6,*) "LOCAL_FRAGMENTATION IS ONLY 1D at the MOMENT"
      STOP
  ENDIF
     
   xi=X_initial
   xf=X_final

  w=DSQRT((xf-xi)/NOGRIDPOINTS)
  
  IF (MORB.lt.10) THEN
     WRITE (MorbAsString, '(I1)') Morb
  ELSE IF ((MORB.ge.10).and.(MORB.lt.100)) THEN
     WRITE (MorbAsString, '(I2)') Morb
  ELSE 
     WRITE(*,*) 'Bigger orbital number than 100 not implemented!'
  ENDIF

  CALL getDOubleAsString(time,timeAsString) 

  IF (mode.eq.1) THEN
     aString = 'x-'
  ELSE IF (mode.eq.2) THEN 
     aString = 'k-'
  ELSE 
     WRITE(*,*)"ERROR in get_correlations"
     STOP
  ENDIF   

  IF ((NPar.gt.0) .and. (NPar.lt.10)) THEN 
    WRITE (NParAsString1, '(I1)') NPar
    fullName=timeasString//'N'//trim(NParAsString1)//'M'//trim(MorbAsString)//aString//'corr_local.dat'
  ELSE  IF ((NPar.ge.10) .and. (NPar.lt.100)) THEN 
    WRITE (NParAsString2, '(I2)') NPar
    fullName=timeasString//'N'//trim(NParAsString2)//'M'//trim(MorbAsString)//aString//'corr_local.dat'
  ELSE IF ((NPar.ge.100) .and. (NPar.lt.1000)) THEN 
    WRITE (NParAsString3, '(I3)') NPar
    fullName=timeasString//'N'//trim(NParAsString3)//'M'//trim(MorbAsString)//aString//'corr_local.dat'
  ELSE IF ((NPar.ge.1000) .and. (NPar.lt.10000)) THEN 
    WRITE (NParAsString4, '(I4)') NPar
    fullName=timeasString//'N'//trim(NParAsString4)//'M'//trim(MorbAsString)//aString//'corr_local.dat'
  ELSE  IF ((NPar.ge.10000) .and. (NPar.lt.100000)) THEN 
    WRITE (NParAsString5, '(I5)') NPar
    fullName=timeasString//'N'//trim(NParAsString5)//'M'//trim(MorbAsString)//aString//'corr_local.dat'
  ELSE IF ((NPar.ge.100000) .and. (NPar.lt.1000000)) THEN 
    WRITE (NParAsString6, '(I6)') NPar
    fullName=timeasString//'N'//trim(NParAsString6)//'M'//trim(MorbAsString)//aString//'corr_local.dat'
  ELSE IF ((NPar.ge.1000000) .and. (NPar.lt.10000000)) THEN 
    WRITE (NParAsString7, '(I7)') NPar
    fullName=timeasString//'N'//trim(NParAsString7)//'M'//trim(MorbAsString)//aString//'corr_local.dat'
  ELSE IF ((NPar.ge.10000000) .and. (NPar.lt.100000000)) THEN 
    WRITE (NParAsString8, '(I8)') NPar
    fullName=timeasString//'N'//trim(NParAsString8)//'M'//trim(MorbAsString)//aString//'corr_local.dat'
  ELSE 
    NParAsString4='XXXX'
    fullName=timeasString//'N'//trim(NParAsString4)//'M'//trim(MorbAsString)//aString//'corr_local.dat'
  END IF
 ! needs to be edited for other dvrs with nonconstant weights! 
  CALL get_maximum(PSI,rho_jk,maximum)
  threshold = 0.005 * DBLE(maximum)/w**2 ! set the minimal density to be plotted
  
  OPEN(unit=12,file=trim(fullName),form='FORMATted')
  DO n=1,NOGRIDPOINTS

!compute G1n_n
     G1n_n = (0.d0,0.d0)
     DO Jorb = 1,Morb  
        DO Korb=1,Morb

           G1n_n = G1n_n + rho_jk(Jorb,Korb) * DCONJG(PSI(n,Jorb))*PSI(n,Korb)

        ENDDO
     ENDDO
     G1n_n=G1n_n*1.d0/w**2 
     outG1n_n=DBLE(G1n_n)
!---------------
     DO m=1,NOGRIDPOINTS         

!compute G1m_m
        G1m_m = (0.d0,0.d0) 
        DO Jorb = 1,Morb  
           DO Korb=1,Morb

              G1m_m = G1m_m + rho_jk(Jorb,Korb) * DCONJG(PSI(m,Jorb))*PSI(m,Korb)

           ENDDO
        ENDDO
        G1m_m=G1m_m*1.d0/w**2 
        outG1m_m=DBLE(G1m_m)   
!----------------
        
!compute G1m_n
        G1m_n = (0.d0,0.d0) 
        DO Jorb = 1,Morb  
           DO Korb=1,Morb

              G1m_n = G1m_n + rho_jk(Jorb,Korb) * DCONJG(PSI(m,Jorb))*PSI(n,Korb)

           ENDDO
        ENDDO
        G1m_n=G1m_n*1.d0/(w**2) 
        outReG1m_n=DBLE(G1m_n)   
        outImG1m_n=DIMAG(G1m_n)
        IF (m.eq.n) integ=integ+G1m_n
!----------------


!compute G2m_n 
        G2m_n=(0.d0,0.d0)
        DO Lorb=1,Morb
           DO Korb=1,Morb
              DO Jorb=1,Morb
                 DO Iorb=1,Morb
                    G2m_n = G2m_n + rho_ijkl(Iorb,Jorb,Korb,Lorb)* &
                              DCONJG(PSI(m,Iorb))*DCONJG(PSI(n,Jorb))*PSI(m,Korb)*PSI(n,Lorb)
  
                 ENDDO
              ENDDO
           ENDDO
        ENDDO
        G2m_n=G2m_n*1.d0/(w*w*w*w) 
        integ2=integ2+G2m_n
        outG2m_n=DBLE(G2m_n)

        CALL get_ijk_from_m(m,NOGRIDPOINTS,NDVR_Y,i_m,j_m,k_m)
        CALL get_ijk_from_m(n,NOGRIDPOINTS,NDVR_Y,i_n,j_n,k_n)
        IF ((outG1m_m.ge.threshold).and.(outG1n_n.ge.threshold)) THEN
            WRITE(12,6666) ort_X(i_m)," ",ort_X(i_n)," ", &
                       outG1m_m," ",outReG1m_n," ",outImG1m_n," ",outG1n_n," ",outG2m_n
        ELSE
            WRITE(12,6666) ort_X(i_m)," ",ort_X(i_n)," ", &
                       outG1m_m," ",-600.d0," ",-600.d0," ",outG1n_n," ",-600.d0
        ENDIF 
        IF(MOD(m,NOGRIDPOINTS).eq.0) WRITE(12,*)"                                            " 

     ENDDO
                             
  ENDDO         
  CLOSE(12)
 
 
6666  FORMAT(2(F8.3,a1),6(F16.9,a1))
END SUBROUTINE get_corr_LOC_FRAG


END MODULE Analysis_Routines_ManyBody
