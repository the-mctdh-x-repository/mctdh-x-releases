!>GPLv3
!    MCTDH-X: the multiconfigurational time-dependent Hartree for 
!    indistinguishable particles software
!
!    Copyright (C) 2021  A. U. J. Lode, M. C. Tsatsos, 
!                        E. Fasshauer, S. E. Weiner, 
!                        R. Lin, L. Papariello, P. Molignini, 
!                        C. Lévêque, M. Büttner , J. Xiang, S. Dutta
!
!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <https://www.gnu.org/licenses/>.
!
!
!
!
!<GPLv3










!> This module collects the routines for the analysis of two-body quantities 
!> in R-MCTDHB simulations.
MODULE Analysis_Routines_TwoBody

use Analysis_Input_Variables
USE Interaction_Parameters
USE Matrix_Elements
USE Function_Library
USE Auxiliary_Routines
USE Auxiliary_FFTRoutines

IMPLICIT NONE
CONTAINS

!> This routine computes the skew diagonal of 1D correlation functions 
!> G1(x,x'=-x), and G2(x,x'=-x), 
!> and writes them to a file. The first four columns contain
!> x,x'. 

subroutine get_correlationsSkewDiagonal1D(time,PSI,mode,rho_jk,rho_ijkl,dilation) 
use Coefficients_Parameters,ONLY:Npar
use DVR_Parameters
  implicit none
  integer  :: NOGRIDPOINTS
  integer,intent(in) :: mode,dilation
  real*8,intent(in)  :: time
  real*8     :: w,dk,mom_X(NDVR_X),mom_Y(NDVR_Y),mom_Z(NDVR_Z)
   COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb)  :: PSI
  complex*16,intent(in) :: rho_ijkl(Morb,Morb,Morb,Morb)
  complex*16,intent(in) :: rho_jk(Morb,Morb)
  character(len=2)   :: aString
  character(len=200) :: fullName
  character(len=10)  :: timeAsString
  character(len=10)  :: MorbAsString
  character(len=1)   :: NparAsString1
  character(len=2)   :: NparAsString2
  character(len=3)   :: NparAsString3
  character(len=4)   :: NparAsString4
  character(len=5)   :: NparAsString5
  character(len=6)   :: NparAsString6
  character(len=7)   :: NparAsString7
  character(len=8)   :: NparAsString8
  integer*8          :: ind,m,n,i_m,j_m,k_m,i_n,j_n,k_n
  integer            :: Iorb,Jorb,Korb,Lorb
  complex*16         :: G1m_m,G1m_n,G1n_n,G2m_n

  complex*16         :: integ,integ2
  real*8,parameter   :: TOL=0.00001d0 
  real*8     :: threshold
  real*8     :: outG1m_m,outReG1m_n,outImG1m_n,outG1n_n,outG2m_n
  real*8             :: xi,xf

  integ=0.d0
  integ2=0.d0


  Call Get_WeightOrMomentumGrid(mom_x,mom_y,mom_z,w,dilation,mode)
  NOGRIDPOINTS=NDVR_X*NDVR_Y*NDVR_Z

  if (MORB.lt.10) then
     write (MorbAsString, '(I1)') Morb
  else if ((MORB.ge.10).and.(MORB.lt.100)) then
     write (MorbAsString, '(I2)') Morb
  else 
     write(*,*) 'Bigger orbital number than 100 not implemented!'
  endif

  call getDoubleAsString(time,timeAsString) 

  if (mode.eq.1) then
     aString = 'x-'
  else if (mode.eq.2) then 
     aString = 'k-'
  else 
     write(*,*)"ERROR in get_correlations"
     stop
  endif   

  call Assemble_OutputFileName(FullName,Time,NPar,Morb,astring,'SkewCorrelations.dat')
  open(unit=12,file=trim(FullName),form='formatted',Status='UNKNOWN')

  ind=0 

  open(unit=12,file=trim(fullName),form='formatted')

  do n=1,NOGRIDPOINTS

!---------------
     do m=1,NOGRIDPOINTS         

        call get_ijk_from_m(m,NDVR_X,NDVR_Y,i_m,j_m,k_m)
        call get_ijk_from_m(n,NDVR_X,NDVR_Y,i_n,j_n,k_n)

!!!! Exception for points on the skew-diagonal
!!!! Exception for points on the skew-diagonal
        if (((mode.eq.1).and.(abs((ort_X(i_m)+ort_X(i_n))).lt.1.d-9)).or.&
           ((mode.eq.2).and.((abs((mom_X(i_m)+mom_X(i_n))).lt.1.d-9)))) THEN
!!!! Exception for points on the skew-diagonal
!!!! Exception for points on the skew-diagonal

        ind=ind+1   

!compute G1n_n
        G1n_n = (0.d0,0.d0)
        do Jorb = 1,Morb  
           do Korb=1,Morb
   
              G1n_n = G1n_n + rho_jk(Jorb,Korb) * DCONJG(PSI(n,Jorb))*PSI(n,Korb)
   
           end do
        end do
        G1n_n=G1n_n*1.d0/w**2 
        outG1n_n=DBLE(G1n_n)

!compute G1m_m
        G1m_m = (0.d0,0.d0) 
        do Jorb = 1,Morb  
           do Korb=1,Morb

              G1m_m = G1m_m + rho_jk(Jorb,Korb) * DCONJG(PSI(m,Jorb))*PSI(m,Korb)

           end do
        end do
        G1m_m=G1m_m*1.d0/w**2 
        outG1m_m=DBLE(G1m_m)   
!----------------
        
!compute G1m_n
        G1m_n = (0.d0,0.d0) 
        do Jorb = 1,Morb  
           do Korb=1,Morb

              G1m_n = G1m_n + rho_jk(Jorb,Korb) * DCONJG(PSI(m,Jorb))*PSI(n,Korb)

           end do
        end do
        G1m_n=G1m_n*1.d0/(w**2) 
        outReG1m_n=DBLE(G1m_n)   
        outImG1m_n=DIMAG(G1m_n)
        if (m.eq.n) integ=integ+G1m_n
!----------------


!compute G2m_n 
        G2m_n=(0.d0,0.d0)
        do Lorb=1,Morb
           do Korb=1,Morb
              do Jorb=1,Morb
                 do Iorb=1,Morb
                    G2m_n = G2m_n + rho_ijkl(Iorb,Jorb,Korb,Lorb)* &
                              DCONJG(PSI(m,Iorb))*DCONJG(PSI(n,Jorb))*PSI(m,Korb)*PSI(n,Lorb)
  
                 end do
              end do
           end do
        end do
        G2m_n=G2m_n*1.d0/(w*w*w*w) 
        integ2=integ2+G2m_n
        outG2m_n=DBLE(G2m_n)

        if (mode.eq.1) then
           write(12,306) ort_X(i_m)," ", &
                       outG1m_m," ",outReG1m_n," ",outImG1m_n," ",outG1n_n," ",outG2m_n
        else if (mode.eq.2) then

           write(12,306) mom_X(i_m)," ", &
                       outG1m_m," ",outReG1m_n," ",outImG1m_n," ",outG1n_n," ",outG2m_n
        end if

        if(MOD(ind,NDVR_X-1).eq.0) write(12,*)"                                            " 

!!!! Exception for points on the skew-diagonal
!!!! Exception for points on the skew-diagonal
        end if
!!!! Exception for points on the skew-diagonal
!!!! Exception for points on the skew-diagonal

     end do
                             
  end do         
  close(12)

306  format(1(F10.5,a1),5(F40.9,a1))
end subroutine get_correlationsSkewDiagonal1D






!> Compute the entropy of the two-body density diagonal in real and in momentum space.
!> Compute the entropy of the geminal occupations.
subroutine Get_2BEntropy(FT_PSI,PSI,rho_ijkl,TIME)
USE DVR_Parameters
USE Coefficients_Parameters
USE Coefficients_Parallelization_Parameters
USE Analysis_Input_Variables,ONLY:Time_From
USE Auxiliary_Routines

COMPLEX*16, DIMENSION(MORB,MORB,MORB,MORB)       :: Rho_ijkl
COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,MORB) :: PSI
COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,MORB) :: FT_PSI
REAL*8                                      :: TIME
REAL*8  :: GemOccs(Morb*(Morb+1)/2)

COMPLEX*16 :: Density2B,K_Density2B
REAL*8     :: Density_Entropy2B,K_Density_Entropy2B, GemOccs_Entropy

REAL*8 :: w,MOM_X(NDVR_X),MOM_Y(NDVR_Y),MOM_Z(NDVR_Z)

INTEGER*8                                     :: NoGridPoints,I,J,K,N,M
integer                                       :: Iorb,Jorb,Korb,Lorb
INTEGER*8                                     :: i_m,j_m,k_m
INTEGER*8                                     :: i_n,j_n,k_n
Call Diag_Geminal(Time,GemOccs,PSI,1,.TRUE.)
Call Get_WeightOrMomentumGrid(mom_x,mom_y,mom_z,w,1,2)
!> Many-Body entropy related to the natural geminal occupations.
GemOccs_Entropy=0.d0
do i=1,Morb*(Morb+1)/2
    GemOccs_Entropy=GemOccs_Entropy-(GemOccs(i)/(1.d0*NPar)*(LOG(GemOccs(i)/(1.d0*NPar))))
end do

NOGRIDPOINTS=NDVR_X*NDVR_Y*NDVR_Z

Density_Entropy2B=dcmplx(0.d0,0.d0)
K_Density_Entropy2B=dcmplx(0.d0,0.d0)

do n=1,NOGRIDPOINTS
   do m=1,NOGRIDPOINTS
      call get_ijk_from_m(m,NDVR_X,NDVR_Y,i_m,j_m,k_m)
      call get_ijk_from_m(n,NDVR_X,NDVR_Y,i_n,j_n,k_n)
      Density2B=dcmplx(0.d0,0.d0)
      K_Density2B=dcmplx(0.d0,0.d0)
      do Lorb=1,Morb
         do Korb=1,Morb
            do Jorb=1,Morb
               do Iorb=1,Morb

                  Density2B = Density2B + rho_ijkl(Iorb,Jorb,Korb,Lorb)* &
                            DCONJG(PSI(m,Iorb))*DCONJG(PSI(n,Jorb))*PSI(m,Korb)*PSI(n,Lorb)/(weight(n)**2*weight(m)**2)

                  K_Density2B = K_Density2B + rho_ijkl(Iorb,Jorb,Korb,Lorb)* &
                            DCONJG(FT_PSI(m,Iorb))*DCONJG(FT_PSI(n,Jorb))*FT_PSI(m,Korb)*FT_PSI(n,Lorb)/(w*w*w*w)

               end do
            end do
         end do
      end do

      Density_Entropy2B=   Density_Entropy2B - Density2B*LOG(Density2B)*weight(n)**2*weight(m)**2
      K_Density_Entropy2B= K_Density_Entropy2B - K_Density2B*LOG(K_Density2B)*w*w*w*w
   end do
end do

!> Write different entropies to a file.
IF (abs(Time-Time_From).lt.0.1d-10) THEN
  open(unit=541,file='TwoBody_Entropy.dat',form='formatted',STATUS='UNKNOWN')
  write(541,6145) time,' ',Density_Entropy2B,' ',K_Density_Entropy2B,' ',GemOccs_Entropy
ELSE
  open(unit=541,file='TwoBody_Entropy.dat',form='formatted',STATUS='OLD',ACCESS='APPEND')
  write(541,6145) time,' ',Density_Entropy2B,' ',K_Density_Entropy2B,' ',GemOccs_Entropy
ENDIF

6145  format(4(F53.16,a1))

close(541)

end subroutine Get_2BEntropy



!>     Natural geminals' analysis:
!>     Diagonalize reduced two-body density matrix elements \rho_ijkl  
!>     and return eigenvalues (Natural Geminal Occupations).
SUBROUTINE Diag_Geminal(Time,NatGemOcc,PSI,jobtype,InEntropy)

use Global_Parameters
USE Analysis_Input_Variables, ONLY: Time_From
USE Matrix_Elements,ONLY:Rho2_Elements,TERM_INDEX_2B,TERM_INDEX_1B,TERM_REQ_1B
USE Coefficients_Parameters

implicit NONE

COMPLEX*16, ALLOCATABLE :: Full_Rho2_Elements_Storage(:,:)
COMPLEX*16, ALLOCATABLE :: WORK(:)
LOGICAL, Optional :: InEntropy

COMPLEX*16 rho_ijkl2(Morb,Morb,Morb,Morb),rho_ijkl(Morb,Morb,Morb,Morb),XX,G2M_N,v(Morb*Morb) !(Morb*(Morb+1)/2)
COMPLEX*16 PSI(NDVR_X*NDVR_Y*NDVR_Z,Morb),X,Y,Z

REAL*8, ALLOCATABLE :: RWORK(:)
REAL*8 NatGemOcc(Morb*(Morb+1)/2),Time

Character*80 FullName

!c=============================================================================
INTEGER ::  ierr
!c=============================================================================
INTEGER :: icntr
INTEGER :: Iorb,Jorb,iflag,info
INTEGER :: Mgem,Korb,Lorb
INTEGER*8 :: I1,I2,I3,J1,J2,J3,K1,K2,K3,L1,L2,L3,JJ,I_Current_Term,ind1,ind2
REAL*8 :: rfactor,w
INTEGER :: jobtype

integer :: P,cK,cJ,cL,cI
!c=============================================================================
Mgem=Morb*(Morb+1)/2
allocate(Full_Rho2_Elements_Storage(Mgem,Mgem),stat=ierr)
if(ierr /= 0)write(*,*)"allocation error in Diag_Geminal"

allocate(WORK(Mgem*Mgem),stat=ierr)
if(ierr /= 0)write(*,*)"allocation error in Diag_Geminal"

allocate(RWORK(3*Mgem-2),stat=ierr)
if(ierr /= 0)write(*,*)"allocation error in Diag_Geminal"

Full_Rho2_Elements_Storage=(0.d0,0d0)

icntr=1

DO Iorb=1,Mgem
   DO Jorb=Iorb,Mgem

      P=TERM_INDEX_2B(icntr)
!c================ Unpack cI cJ cK cL from P
      cL= INT(P/1000000)
      cK= INT((P-cL*1000000)/10000)
      cJ= INT((P-cL*1000000-cK*10000)/100)
      cI= P-cL*1000000-cK*10000-cJ*100

      rfactor=1.d0
      if (cI.ne.cJ) rfactor=rfactor*dsqrt(2.d0)
      if (cK.ne.cL) rfactor=rfactor*dsqrt(2.d0)

      Full_Rho2_Elements_Storage(Iorb,Jorb)=Rho2_Elements(icntr)*rfactor

!      Write(6,*) "Cs",CI,CJ,CK,CL,"and Iorb,Jorb", Iorb,Jorb
      P=TERM_INDEX_1B(Iorb)
      cJ= INT(P/100)
      cI= P-cJ*100
      P=TERM_INDEX_1B(Jorb)
      cL= INT(P/100)
      cK= P-cL*100
!      Write(6,*) "Reconstructed",CI,CJ,CK,CL


      icntr=icntr+1
   EndDO
EndDO

call Get_Full_Rijkl(Rho_IJKL)
!c================== Natural Occupations and Natural Vectors ==================

SELECT CASE (jobtype)

  CASE (1)

CALL ZHEEV('N','U',Mgem,Full_Rho2_Elements_Storage,Mgem,NatGemOcc,WORK,Mgem*Mgem,&
                              RWORK,INFO) ! Case 1: Evaluation
                                          ! of eigenvalues
                                          ! only.

  CASE (2)
CALL ZHEEV('V','U',Mgem,Full_Rho2_Elements_Storage,Mgem,NatGemOcc,WORK,Mgem*Mgem,&
                              RWORK,INFO) ! Case 2: Evaluation
                                          ! of eigenvalues
                                          ! and eigenvectors.

 call Assemble_OutputFileName(FullName,Time,NPar,Morb,'x-','geminals.dat')
 open(unit=12,file=trim(FullName),form='formatted',Status='UNKNOWN')

 ind1=1
 ind2=1
 do K1=1,NDVR_Z
  do J1=1,NDVR_Y
   do I1=1,NDVR_X

    do K2=1,NDVR_Z
     do J2=1,NDVR_Y
      do I2=1,NDVR_X

        icntr=1

        XX=Zero

        Do k3=1,MGem

         v(k3)=Zero

         Do l3=k3,MGem

           P=TERM_INDEX_1B(k3)
           cJ= INT(P/100)
           cI= P-cJ*100
           P=TERM_INDEX_1B(l3)
           cL= INT(P/100)
           cK= P-cL*100

!           X=(PSI(ind1,CI)*PSI(ind2,CJ)+PSI(ind1,CJ)*PSI(ind2,CI))
           X=(PSI(ind1,CI)*PSI(ind2,CJ)+PSI(ind2,CI)*PSI(ind1,CJ))
           Z=conjg(Full_Rho2_Elements_Storage(l3,k3))

           rfactor=1.d0

           if (cI.ne.cJ) THEN
              rfactor=rfactor/2.d0
              X=X+(PSI(ind1,CL)*PSI(ind2,CK)+PSI(ind1,CK)*PSI(ind2,CL))
           ENDIF

           if (cK.ne.cL) THEN
              rfactor=rfactor/2.d0
              X=X+(PSI(ind1,CL)*PSI(ind2,CK)+PSI(ind1,CK)*PSI(ind2,CL))
           ENDIF
           
           v(k3)=v(k3)+rfactor*X*Z 

         END DO
         XX=XX+NatGemOcc(k3)*Dconjg( v(k3) ) * v(k3)
       END DO

        G2m_n=Zero
        do Lorb=1,Morb
           do Korb=1,Morb
              do Jorb=1,Morb
                 do Iorb=1,Morb
                    G2m_n = G2m_n + rho_ijkl(Iorb,Jorb,Korb,Lorb)* &
                              DCONJG(PSI(ind1,Iorb))*DCONJG(PSI(ind2,Jorb))*PSI(ind1,Korb)*PSI(ind2,Lorb)
  
                 end do
              end do
           end do
        end do
       
        IF (Dim_MCTDH.ge.1) THEN                
          w=DSQRT((Ort_X(NDVR_X)-Ort_X(1))/NDVR_X)
        ENDIF              
        IF (Dim_MCTDH.ge.2) THEN                
         w=w*DSQRT((Ort_Y(NDVR_Y)-Ort_Y(1))/NDVR_Y) 
        ENDIF              
        IF (Dim_MCTDH.ge.3) THEN                
         w=w*DSQRT((Ort_Z(NDVR_Z)-Ort_Z(1))/NDVR_Z) 
        ENDIF              

        G2m_n=G2m_n/(w*w*w*w)

        write(12,306) ort_X(i1)," ",ort_Y(j1)," ",ort_Z(k1)," ",ort_X(i2)," ",ort_Y(j2)," ",ort_Z(k2)," ", &
                       DBLE(G2m_n)," ",XX," ",(v(JJ)," ", JJ=1,MGem)
        IF (mod(ind2,NDVR_X).eq.0) Write(12,*) " "

306  format(7(F23.9,a1),99(2F23.9,a1))

        

       ind2=ind2+1

       END DO
      END DO
     END DO

    ind1=ind1+1
    ind2=1

    END DO
   END DO
  END DO

  CLOSE(12)
  CASE DEFAULT
  write(6,*)" Something wrong in Diag_Geminal !!!!!!!!!"
END SELECT 

IF (Present(InEntropy).eqv..FALSE.) THEN
 IF (abs(Time-Time_From).lt.0.1d-10) THEN
    open(unit=10,file='GO_PR.out',form='formatted',Status='UNKNOWN')
 ELSE
    open(unit=10,file='GO_PR.out',form='formatted',Status='OLD',ACCESS='APPEND')
 ENDIF

 WRITE(10,305) Time,' ', (NatGemOcc(JJ),' ', JJ=1,MGem)
305  format(99(F23.9,a1))

 CLOSE(10)

ENDIF

deallocate(Full_Rho2_Elements_Storage,WORK,RWORK)
END  SUBROUTINE Diag_Geminal




!> This routine computes the expectation values for N=2 particle loss operators when partitioning 
!> the two particle Hilbert space at 'border'
subroutine lossops_two_bosons(time,PSI,rho_ijkl,border)
use Coefficients_Parameters,ONLY:NPar
use DVR_Parameters
USE Interaction_Parameters

IMPLICIT NONE

COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb)  :: PSI
complex*16,DIMENSION(Morb,Morb,Morb,Morb),intent(in) :: rho_ijkl
complex*16 :: G2m_n
real*8, intent(in) :: time,border
real*8 :: int_2in,int_2out,int_1in_1out,int_1out_1in

integer*8           :: NOGRIDPOINTS,m,n,i_m,i_n,j_m,j_n,k_m,k_n
INTEGER             :: LORB,KORB,JORB,IORB
character(len=10)   :: borderAsString
character(len=100)   :: fullName


int_2in=0.d0
int_2out=0.d0
int_1in_1out=0.d0
int_1out_1in=0.d0

if (NPar.ne.2) then 
   write(*,*) 'this is so far only implemented for N=2'
   return
endif

if (DIM_MCTDH.ne.1) then
   write(*,*) 'this is so far only implemented for 1D systems'
   return
endif
NOGRIDPOINTS = NDVR_X*NDVR_Y*NDVR_Z

  do n=1,NOGRIDPOINTS
     do m=1,NOGRIDPOINTS
        call get_ijk_from_m(m,NDVR_X,NDVR_Y,i_m,j_m,k_m)
        call get_ijk_from_m(n,NDVR_X,NDVR_Y,i_n,j_n,k_n)
        if (((ort_x(i_m).gt.border).and.(ort_x(i_n).gt.border)).eqv..FALSE.) then
        G2m_n=(0.d0,0.d0)
        do Lorb=1,Morb
           do Korb=1,Morb
              do Jorb=1,Morb
                 do Iorb=1,Morb
                    G2m_n = G2m_n + rho_ijkl(Iorb,Jorb,Korb,Lorb)* &
                              DCONJG(PSI(m,Iorb))*DCONJG(PSI(n,Jorb))*PSI(m,Korb)*PSI(n,Lorb)
  
                 end do
              end do
           end do
        end do


        if ((ort_x(i_m).lt.border).and.(ort_x(i_n).lt.border)) int_2in=int_2in+Dble(G2m_n)
        if ((ort_x(i_m).ge.border).and.(ort_x(i_n).lt.border)) int_1in_1out=int_1in_1out+2*Dble(G2m_n)
        
!        if ((ort_x(i_m).ge.border).and.(ort_x(i_n).ge.border)) int_2out=int_2out+Dble(G2m_n)
!        if ((ort_x(i_n).ge.border).and.(ort_x(i_m).lt.border)) int_1out_1in=int_1out_1in+Dble(G2m_n)
        endif
        
     end do
  end do
  int_2out=2-int_2in-int_1in_1out
  call getDoubleAsString(border,borderAsString) 
  fullName='lossops_N2_'//trim(borderAsString)//'.dat'
  
  open(unit=12,file=trim(fullName),form='formatted')

  write(12,8765) time,int_2in,int_1in_1out,int_2out
 
 8765 FORMAT(5E25.16)

return
  
end subroutine lossops_two_bosons

!> This routine computes the correlation functions on a restricted subspace 
!> from x(k)ini to x(k)fin with x(k)pts points
subroutine get_corr_one_restricted(time,PSI,mode,rho_jk,&
                                   xini,xfin,xpts,kxini,kxfin,kpts) 
use Coefficients_Parameters,ONLY:Npar
use DVR_Parameters
  implicit none
  integer  :: NOGRIDPOINTS,last_i_m,last_i_n
  integer,intent(in) :: mode,xpts,kpts
  real*8,intent(in)  :: time
  real*8     :: w,dk,mom_X(NDVR_X)
  COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb)  :: PSI
  complex*16,intent(in) :: rho_jk(Morb,Morb)
  character(len=2)   :: aString
  character(len=200) :: fullName

  integer*8            :: m,n,i_m,j_m,k_m,i_n,j_n,k_n
  integer            :: Jorb,Korb
  complex*16         :: G1m_m,G1m_n

  real*8,parameter   :: TOL=0.00001d0 
  real*8     :: outReG1m_n,outImG1m_n,increment
  real*8             :: xi,xf,yi,yf,zi,zf,xini,xfin,kxini,kxfin,onebdens(NDVR_X)

  NOGRIDPOINTS=NDVR_X*NDVR_Y*NDVR_Z
   xi=X_initial
   xf=X_final
   yi=Y_initial
   yf=Y_final
   zi=Z_initial
   zf=Z_final

  if (mode.eq.1) then
     increment= (xfin-xini)/(xpts*1.d0+2.d0)
  else if (mode.eq.2) then 
     increment=(kxfin-kxini)/(kpts*1.d0+2.d0)
  endif

! some exceptions
  if (DVR_X.eq.1) then
     write(*,*) 'The correlations in restricted space work only aequidistant grids...'
     return
  else if ((mode.eq.1).and.(increment.le.ort_x(2)-ort_x(1))) then
     write(*,*) 'CORR1-RESTRICTED:The number of points you asked implies a denser grid than the output has,&
          I will use the full number - I decrease the number of points accordingly. Mode',mode
       increment=0.d0
  else if ((mode.eq.2).and.(increment.le.abs(mom_x(2)-mom_x(1)))) then 
       write(*,*) 'CORR1-RESTRICTED:The number of points you asked implies a denser grid than the output has,&
           I will use the full number - I decrease the number of points accordingly. Mode',mode
       increment=0.d0
  elseif ((xi.gt.xini).or.(xfin.gt.xf)) then
       write(*,*) 'CORR1-RESTRICTED:The asked grid is bigger than the one in the output - i decrease the size accordingly!'
       if ((xi.gt.xini)) xini = xi
       if ((xfin.gt.xf)) xfin = xf
  endif
  if (mode.eq.1) then 
    IF (DVR_X.ne.3) then
      w= DSQRT((xf-xi)/(NDVR_X*1.d0))
    else if (DVR_X.eq.3) then
      w= DSQRT((xf-xi)/(NDVR_X*1.d0+1.d0))
    endif   
  else if (mode.eq.2) then
     call get_mom(xi,xf,NDVR_X,mom_X)
     dk     = 2.d0*PI/(xf-xi)
     w      = DSQRT( dk )
  else if ((mode.ne.1).and.(mode.ne.2)) then
        write(*,*) "wrong mode"
  end if

  if (mode.eq.1) then
     aString = 'x-'
  else if (mode.eq.2) then 
     aString = 'k-'
  else 
     write(*,*)"ERROR in get_correlations"
     stop
  endif   

  call Assemble_OutputFileName(FullName,Time,NPar,Morb,aString,'corr1restr.dat')

  open(unit=12,file=trim(fullName),form='formatted')

!compute onnebody density
        onebdens = (0.d0,0.d0) 
  do m=1,NDVR_X
        G1m_m = (0.d0,0.d0) 
        do Jorb = 1,Morb  
           do Korb=1,Morb
              G1m_m = G1m_m + rho_jk(Jorb,Korb) * DCONJG(PSI(m,Jorb))*PSI(m,Korb)
           end do
        end do
        onebdens(m)=dble(G1m_m)/w**2
  end do
   
!compute G1n_n
  last_i_m=NDVR_X/2
  last_i_n=NDVR_X/2
  i_n=1
  i_m=1
!---------------
  do n=1,NOGRIDPOINTS
    call get_ijk_from_m(n,NDVR_X,NDVR_Y,i_n,j_n,k_n)
    if ((((mode.eq.1).and.&
         abs(ort_x(last_i_n)-ort_x(i_n)).ge.increment).and.&
         (Ort_X(i_n).lt.xfin).and.(Ort_x(i_n).gt.xini)).or. &
         ((mode.eq.2).and.&
         (abs(mom_x(last_i_n)-mom_x(i_n)).ge.increment).and.&
         (mom_X(i_n).lt.kxfin).and.(mom_X(i_n).gt.kxini)))&
         then
     do m=1,NOGRIDPOINTS
        call get_ijk_from_m(m,NDVR_X,NDVR_Y,i_m,j_m,k_m)
        if (((mode.eq.1).and.&
           (Ort_X(i_m).lt.xfin).and.&
           (Ort_x(i_m).gt.xini).and.&
           (abs(Ort_X(i_m)-Ort_X(last_i_m)).ge.increment)).or.&
           ((mode.eq.2).and.&
           (mom_X(i_m).lt.kxfin).and.&
           (mom_X(i_m).gt.kxini).and.&
           (abs(mom_X(i_m)-mom_X(last_i_m)).ge.increment)))&
           then
!compute G1m_m
        
!compute G1m_n
        G1m_n = (0.d0,0.d0) 
        do Jorb = 1,Morb  
           do Korb=1,Morb
              G1m_n = G1m_n + rho_jk(Jorb,Korb) * DCONJG(PSI(m,Jorb))*PSI(n,Korb)
           end do
        end do
        
        G1m_n=G1m_n*1.d0/w**2 
        outReG1m_n=DBLE(G1m_n)   
        outImG1m_n=DIMAG(G1m_n)
!----------------
!#########################################################################################
!####### output only in restricted space 
!#########################################################################################
           if (mode.eq.1) then
                write(12,309) ort_X(i_m),&
                      ort_X(i_n),outREG1m_n,outIMG1m_n,&
                      onebdens(m),onebdens(n)
           else if (mode.eq.2) then
                write(12,309) mom_X(i_m),&
                      mom_X(i_n),outREG1m_n,outIMG1m_n,&
                      onebdens(m),onebdens(n)
           endif
      else !if conditions for first coordinate are not met
            goto 9877
      endif
      last_i_m=i_m
 9877 continue
      if(MOD(i_m,NDVR_X).eq.0) write(12,*)"                                          " 
     end do
     else !if conditions for second coordinate are not met 
       goto 9878
     endif
 9878 continue
  end do 
  close(12)
 
 
309  format(2(F8.3,1X),4(E25.16))
end subroutine get_corr_one_restricted

!> This routine computes the correlation functions 
!> G2(r,r',r',r), where r=(x/k) in a restricted subspace
!> from x(k)ini to x(k)fin with x(k)pts
subroutine get_corr_two_restricted(time,PSI,mode,rho_jk,rho_ijkl,&
              xini,xfin,xpts,kxini,kxfin,kpts)
use Coefficients_Parameters,ONLY:Npar
use DVR_Parameters
  implicit none
  integer  :: NOGRIDPOINTS,last_i_m,last_i_n
  integer,intent(in) :: mode,xpts,kpts
  real*8,intent(in)  :: time
  real*8     :: w,dk,mom_X(NDVR_X)
   COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb)  :: PSI
  complex*16,intent(in) :: rho_ijkl(Morb,Morb,Morb,Morb)
  complex*16,intent(in) :: rho_jk(Morb,Morb)
  character(len=2)   :: aString
  character(len=200) :: fullName

  integer*8          :: m,n,i_m,j_m,k_m,i_n,j_n,k_n
  integer            :: Iorb,Jorb,Korb,Lorb
  complex*16         :: G1m_m,G2m_n

  real*8         :: onebdens(NDVR_X)
  real*8,parameter   :: TOL=0.00001d0 
  real*8             :: xi,xf,yi,yf,zi,zf,xini,xfin,kxini,kxfin
  real*8     ::   increment


    IF (DVR_X.eq.1) then
       write(*,*) 'Please code the analysis 2-body correlations on restricted subspaces for HO DVR!!!'
       return
    endif

   

  NOGRIDPOINTS=NDVR_X*NDVR_Y*NDVR_Z
   xi=X_initial
   xf=X_final
   yi=Y_initial
   yf=Y_final
   zi=Z_initial
   zf=Z_final
  if (mode.eq.1) then 
    IF (DVR_X.ne.3) then
      w= DSQRT((xf-xi)/(NDVR_X*1.d0))
    else if (DVR_X.eq.3) then
      w= DSQRT((xf-xi)/(NDVR_X*1.d0+1.d0))
    endif   
  else if (mode.eq.2) then
     call get_mom(xi,xf,NDVR_X,mom_X)
     dk     = 2.d0*PI/(xf-xi)
     w      = DSQRT( dk )

  else if ((mode.ne.1).and.(mode.ne.2)) then
        write(*,*) "wrong mode"
  end if

  if (mode.eq.1) then
     increment= (xfin-xini)/(xpts*1.d0+2.d0)
  else if (mode.eq.2) then 
     increment=(kxfin-kxini)/(kpts*1.d0+2.d0)
  endif

! some exceptions
  if (DVR_X.eq.1) then
     write(*,*) 'The correlations in restricted space work only aequidistant grids...'
     return
  else if ((mode.eq.1).and.(increment.le.abs(ort_x(2)-ort_x(1)))) then
       write(*,*) 'CORR2-RESTRICTED:The number of points you asked implies a denser grid than the output has,&
           I will use the full number - I decrease the number of points accordingly. Mode',mode
       increment=0.d0
  else if ((mode.eq.2).and.(increment.le.abs(mom_x(2)-mom_x(1)))) then 
       write(*,*) 'CORR2-RESTRICTED:The number of points you asked implies a denser grid than the output has,&
           I will use the full number - I decrease the number of points accordingly. Mode',mode
       increment=0.d0
  elseif ((xi.gt.xini).or.(xfin.gt.xf)) then
       write(*,*) 'The asked grid is bigger than the one in the output - i decrease the size accordingly!'
       if ((xi.gt.xini)) xini = xi
       if ((xfin.gt.xf)) xfin = xf
  endif

  if (mode.eq.1) then
     aString = 'x-'
  else if (mode.eq.2) then 
     aString = 'k-'
  else 
     write(*,*)"ERROR in get_correlations"
     stop
  endif 

  call Assemble_OutputFileName(FullName,Time,NPar,Morb,aString,'corr2restr.dat')

  open(unit=12,file=trim(fullName),form='formatted')

!compute onnebody density
        onebdens = (0.d0,0.d0) 
  do m=1,NDVR_X
        G1m_m = (0.d0,0.d0) 
        do Jorb = 1,Morb  
           do Korb=1,Morb
              G1m_m = G1m_m + rho_jk(Jorb,Korb) * DCONJG(PSI(m,Jorb))*PSI(m,Korb)
           end do
         end do
        onebdens(m)=dble(G1m_m*1.d0/(w**2))
  end do
!  write(*,*) 'N=', SUM(onebdens*w**2),'in corr2 restr'
!  write(*,*) 'computed weight', w,'in corr2 restr'

  last_i_m=NDVR_X/2
  last_i_n=NDVR_X/2
  i_n=1
  i_m=1
  do n=1,NOGRIDPOINTS
    call get_ijk_from_m(n,NDVR_X,NDVR_Y,i_n,j_n,k_n)
    if ((((mode.eq.1).and.&
         abs(ort_x(last_i_n)-ort_x(i_n)).ge.increment).and.&
         (Ort_X(i_n).lt.xfin).and.(Ort_x(i_n).gt.xini)).or. &
         ((mode.eq.2).and.&
         (abs(mom_x(last_i_n)-mom_x(i_n)).ge.increment).and.&
         (mom_X(i_n).lt.kxfin).and.(mom_X(i_n).gt.kxini)))&
         then
     do m=1,NOGRIDPOINTS
        call get_ijk_from_m(m,NDVR_X,NDVR_Y,i_m,j_m,k_m)
        if (((mode.eq.1).and.&
           (Ort_X(i_m).lt.xfin).and.&
           (Ort_x(i_m).gt.xini).and.&
           (abs(Ort_X(i_m)-Ort_X(last_i_m)).ge.increment)).or.&
           ((mode.eq.2).and.&
           (mom_X(i_m).lt.kxfin).and.&
           (mom_X(i_m).gt.kxini).and.&
           (abs(mom_X(i_m)-mom_X(last_i_m)).ge.increment)))&
         then
!compute G2m_n 
        G2m_n=(0.d0,0.d0)
        do Lorb=1,Morb
           do Korb=1,Morb
              do Jorb=1,Morb
                 do Iorb=1,Morb
                    G2m_n = G2m_n + rho_ijkl(Iorb,Jorb,Korb,Lorb)* &
                              DCONJG(PSI(m,Iorb))*DCONJG(PSI(n,Jorb))*PSI(m,Korb)*PSI(n,Lorb)
                 end do
              end do
           end do
        end do
        G2m_n=G2m_n*(1.d0/w**4)
           if (mode.eq.1) then
                write(12,306) ort_X(i_m),&
                      ort_X(i_n),G2m_n,&
                      onebdens(m),onebdens(n)
           else if (mode.eq.2) then
                write(12,306) mom_X(i_m),&
                      mom_X(i_n),G2m_n,&
                      onebdens(m),onebdens(n)
           endif
        else !if conditions for first coordinate are not met
            goto 9879
        endif
!        if (mode.eq.2) then
!             write(*,*) 'two body restricted momentum correlation function not implemented so far'
!        end if
        last_i_m=i_m
 9879 continue
        if(MOD(i_m,NDVR_X).eq.0) write(12,*)"                                 " 
     end do
     else !if conditions for second coordinate are not met 
       goto 9880
     endif
     last_i_n=i_n                        
 9880 continue
  end do  

  close(12)
 
 
 306  format(2(F8.3,1X),4(E25.16))
end subroutine get_corr_two_restricted



subroutine get_StructureFactor(time,PSI,rho_jk,rho_ijkl,xref,yref,zref) 
USE Global_Parameters
use Coefficients_Parameters,ONLY:Npar
use DVR_Parameters
USE FFT_Laboratory
use Auxiliary_Analysis_Routines
  implicit none
  integer  :: NOGRIDPOINTS
  real*8,intent(in)  :: time,xref,yref,zref
  real*8     :: w,dk,mom_X(NDVR_X),mom_Y(NDVR_Y),mom_Z(NDVR_Z)
  COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb)  :: PSI
  complex*16,intent(in) :: rho_ijkl(Morb,Morb,Morb,Morb)
  complex*16,intent(in) :: rho_jk(Morb,Morb)
  character(len=2)   :: aString
  character(len=200) :: fullName
  integer*8          :: m,n,i_m,j_m,k_m,i_n,j_n,k_n,i_0,j_0,k_0,m_0
  integer            :: Iorb,Jorb,Korb,Lorb
  
  complex*16         :: G2m_m_0(NDVR_X*NDVR_Y*NDVR_Z),DSF(NDVR_X*NDVR_Y*NDVR_Z),FTDSF(NDVR_X*NDVR_Y*NDVR_Z)
  complex*16         :: G1m_m_0(NDVR_X*NDVR_Y*NDVR_Z)

  real*8,parameter   :: TOL=0.00001d0 
  real*8     :: outG1m_m,outReG1m_n,outImG1m_n,outG1n_n,outG2m_n
  real*8             :: xi,xf



  NOGRIDPOINTS=NDVR_X*NDVR_Y*NDVR_Z

  Call Get_WeightOrMomentumGrid(mom_x,mom_y,mom_z,w,1,2)
  Call Get_WeightOrMomentumGrid(mom_x,mom_y,mom_z,w,1,1)

  aString = 'x-'

  call Assemble_OutputFileName(FullName,Time,NPar,Morb,aString,'StructureFactor.dat')

  open(unit=12,file=trim(fullName),form='formatted')


  Call Get_m_0_from_xyz(m_0,xref,yref,zref)
  
  G2m_m_0=(0.d0,0.d0)
  G1m_m_0=(0.d0,0.d0)

  do n=1,NOGRIDPOINTS
!compute G2m_m_0 
     do Lorb=1,Morb
        do Korb=1,Morb
           do Jorb=1,Morb
              do Iorb=1,Morb
                 G2m_m_0(n) = G2m_m_0(n) + rho_ijkl(Iorb,Jorb,Korb,Lorb)* &
                           DCONJG(PSI(m_0,Iorb))*DCONJG(PSI(n,Jorb))*PSI(m_0,Korb)*PSI(n,Lorb)
  
              end do
           end do
        end do
     end do
  end do
  G2m_m_0=G2m_m_0/(w*w*w*w)


  do m=1,NOGRIDPOINTS
     do Korb=1,Morb
        do Jorb=1,Morb
           G1m_m_0(m) = G1m_m_0(m) + rho_jk(Jorb,Korb) * DCONJG(PSI(m,Jorb))*PSI(m_0,Korb)
        end do
     end do
  end do
  G1m_m_0=G1m_m_0/(w*w)

  DSF=G2m_m_0-1.d0
  CALL Get_FFT(DIM_MCTDH,DSF,0,1,MYID=30)
  IF (DIM_MCTDH.eq.1) THEN
    if (mod(NDVR_X,2).eq.0) then
  
      do m=(NDVR_X/2+1),NDVR_X
         FTDSF(m-(NDVR_X/2))=DSF(m)
         FTDSF(m)=DSF(m-(NDVR_X/2))
      enddo
  
    else
  
      do m=(NDVR_X/2+1),NDVR_X
         FTDSF(m-(NDVR_X/2))=FTDSF(m)
         FTDSF(m)=FTDSF(m-(NDVR_X/2))
      enddo
  
    endif
  ELSEIF(DIM_MCTDH.eq.2) THEN
    call sort_FFT_to_ascending_2d(DSF,NDVR_X,NDVR_Y)
    FTDSF=DSF
  ENDIF

  FTDSF=1.d0+Npar*FTDSF/(NDVR_X*NDVR_Y*NDVR_Z)

  Do n=1,NOGRIDPOINTS

     call get_ijk_from_m(n,NDVR_X,NDVR_Y,i_n,j_n,k_n)

     write(12,306) ort_X(i_n)," ",ort_Y(j_n)," ",ort_Z(k_n)," ", DBLE(G2m_m_0(n))," ", DIMAG(G2m_m_0(n))," ", DBLE(G1m_m_0(n))," ", DIMAG(G1m_m_0(n))," ", mom_X(i_n)," ",mom_Y(j_n)," ",mom_Z(k_n)," ", DBLE(FTDSF(n))," ",DIMAG(FTDSF(n)) 

     if(MOD(n,NDVR_X).eq.0) write(12,*)"                                            " 

  end do         
  close(12)

306  format(3(F8.3,a1),4(F23.9,a1),3(F8.3,a1),2(F23.9,a1))

end subroutine get_StructureFactor

!> This routine computes the correlation functions 
!> G1(r,r), G1(r,r'), G1(r',r') and G2(r,r',r',r), where r=(x,y,z), 
!> and writes them to a file. The first six columns contain
!> x,y,z,x',y',z'. 

subroutine get_correlations(time,PSI,mode,rho_jk,rho_ijkl,Dilation,CorrCoef) 
use Coefficients_Parameters,ONLY:Npar
use DVR_Parameters
use Auxiliary_Analysis_Routines
  implicit none
  integer  :: NOGRIDPOINTS
  integer,intent(in) :: mode,Dilation
  real*8,intent(in)  :: time
  logical, optional  :: CorrCoef
  real*8     :: w,dk,mom_X(NDVR_X),mom_Y(NDVR_Y),mom_Z(NDVR_Z)
   COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb)  :: PSI
  complex*16,intent(in) :: rho_ijkl(Morb,Morb,Morb,Morb)
  complex*16,intent(in) :: rho_jk(Morb,Morb)
  character(len=2)   :: aString
  character(len=200) :: fullName
  integer*8          :: m,n,i_m,j_m,k_m,i_n,j_n,k_n
  integer            :: Iorb,Jorb,Korb,Lorb
  complex*16         :: G1m_m,G1m_n,G1n_n,G2m_n,G1n_n1st,G1n_n2nd,G2r1r2,tau

  real*8,parameter   :: TOL=0.00001d0 
  real*8     :: threshold
  real*8     :: outG1m_m,outReG1m_n,outImG1m_n,outG1n_n,outG2m_n
  real*8             :: xi,xf



  NOGRIDPOINTS=NDVR_X*NDVR_Y*NDVR_Z

  Call Get_WeightOrMomentumGrid(mom_x,mom_y,mom_z,w,dilation,mode)

  if (mode.eq.1) then
     aString = 'x-'
  else if (mode.eq.2) then 
     aString = 'k-'
  else 
     write(*,*)"ERROR in get_correlations"
     stop
  endif 


if (Present(CorrCoef).and.(CorrCoef).eqv..TRUE.) THEN


  fullName='CorrelationCoefficient.dat'

  IF (abs(Time-Time_From).lt.0.1d-10) THEN
     open(unit=12,file=trim(fullName),form='formatted',STATUS='UNKNOWN')
  ELSE
     open(unit=12,file=trim(fullName),form='formatted',STATUS='OLD',ACCESS='APPEND')
  ENDIF

  G1n_n1st = (0.d0,0.d0)
  G1n_n2nd = (0.d0,0.d0)
  G2r1r2=(0.d0,0.d0)

  do n=1,NOGRIDPOINTS
    call get_ijk_from_m(n,NDVR_X,NDVR_Y,i_n,j_n,k_n)
!compute G1n_n
     do Jorb = 1,Morb  
        do Korb=1,Morb

           G1n_n1st = G1n_n1st + (ort_x(i_n)+ort_y(j_n)+ort_z(k_n))*rho_jk(Jorb,Korb) * DCONJG(PSI(n,Jorb))*PSI(n,Korb)
           G1n_n2nd = G1n_n2nd + (ort_x(i_n)**2+ort_y(j_n)**2+ort_z(k_n)**2)*rho_jk(Jorb,Korb) * DCONJG(PSI(n,Jorb))*PSI(n,Korb)

        end do
     end do

     do m=1,NOGRIDPOINTS         
        call get_ijk_from_m(m,NDVR_X,NDVR_Y,i_m,j_m,k_m)

        do Lorb=1,Morb
           do Korb=1,Morb
              do Jorb=1,Morb
                 do Iorb=1,Morb
                    G2r1r2 = G2r1r2 &
                           + (ort_x(i_n)*ort_x(i_m)+ort_y(j_n)*ort_y(j_m)+ort_z(k_n)*ort_z(k_m)) & 
                           *rho_ijkl(Iorb,Jorb,Korb,Lorb) &
                           *DCONJG(PSI(m,Iorb))*DCONJG(PSI(n,Jorb))*PSI(m,Korb)*PSI(n,Lorb)
  
                 end do
              end do
           end do
        end do
     end do
  end do

  tau=(G2r1r2-g1n_n1st**2)/(G1n_n2nd-G1n_n1st**2)
  write(12,9999) time," ",DBLE(tau)," ",DIMAG(tau)

9999  format(3(F16.9,a1))


ELSEIF ((present(CorrCoef).eqv..FALSE.).or.((present(CorrCoef).eqv..TRUE.).and.(CorrCoef.eqv..FALSE.))) THEN

  call Assemble_OutputFileName(FullName,Time,NPar,Morb,aString,'correlations.dat')

  open(unit=12,file=trim(fullName),form='formatted')

  do n=1,NOGRIDPOINTS

!compute G1n_n
     G1n_n = (0.d0,0.d0)
     do Jorb = 1,Morb  
        do Korb=1,Morb

           G1n_n = G1n_n + rho_jk(Jorb,Korb) * DCONJG(PSI(n,Jorb))*PSI(n,Korb)

        end do
     end do
     G1n_n=G1n_n*1.d0/w**2 
     outG1n_n=DBLE(G1n_n)
!---------------
     do m=1,NOGRIDPOINTS         

!compute G1m_m
        G1m_m = (0.d0,0.d0) 
        do Jorb = 1,Morb  
           do Korb=1,Morb

              G1m_m = G1m_m + rho_jk(Jorb,Korb) * DCONJG(PSI(m,Jorb))*PSI(m,Korb)

           end do
        end do
        G1m_m=G1m_m*1.d0/w**2 
        outG1m_m=DBLE(G1m_m)   
!----------------
        
!compute G1m_n
        G1m_n = (0.d0,0.d0) 
        do Jorb = 1,Morb  
           do Korb=1,Morb

              G1m_n = G1m_n + rho_jk(Jorb,Korb) * DCONJG(PSI(m,Jorb))*PSI(n,Korb)

           end do
        end do
        G1m_n=G1m_n*1.d0/(w**2) 
        outReG1m_n=DBLE(G1m_n)   
        outImG1m_n=DIMAG(G1m_n)
!----------------


!compute G2m_n 
        G2m_n=(0.d0,0.d0)
        do Lorb=1,Morb
           do Korb=1,Morb
              do Jorb=1,Morb
                 do Iorb=1,Morb
                    G2m_n = G2m_n + rho_ijkl(Iorb,Jorb,Korb,Lorb)* &
                              DCONJG(PSI(m,Iorb))*DCONJG(PSI(n,Jorb))*PSI(m,Korb)*PSI(n,Lorb)
  
                 end do
              end do
           end do
        end do
        G2m_n=G2m_n*1.d0/(w*w*w*w) 
        outG2m_n=DBLE(G2m_n)

        call get_ijk_from_m(m,NDVR_X,NDVR_Y,i_m,j_m,k_m)
        call get_ijk_from_m(n,NDVR_X,NDVR_Y,i_n,j_n,k_n)

        if (mode.eq.1) then
           write(12,306) ort_X(i_m)," ",ort_Y(j_m)," ",ort_Z(k_m)," ",ort_X(i_n)," ",ort_Y(j_n)," ",ort_Z(k_n)," ", &
                       outG1m_m," ",outReG1m_n," ",outImG1m_n," ",outG1n_n," ",outG2m_n
        else if (mode.eq.2) then

           write(12,306) mom_X(i_m)," ",mom_Y(j_m)," ",mom_Z(k_m)," ",mom_X(i_n)," ",mom_Y(j_n)," ",mom_Z(k_n)," ", &
                       outG1m_m," ",outReG1m_n," ",outImG1m_n," ",outG1n_n," ",outG2m_n
        end if

        if(MOD(m,NDVR_X).eq.0) write(12,*)"                                            " 

     end do
                             
  end do         
306  format(6(F8.3,a1),6(F16.9,a1))
ENDIF 

close(12)

end subroutine get_correlations


!> Subrotine to calculate the derivative of PSI in a specific direction.
!> PSI is a single orbital/complex valued function on NDVR_X*NDVR_Y*NDVR_Z grid points
!> direction=1/2/3 stands for d/dx, d/dy, d/dz

subroutine get_derivative(PSI,direction)
USE DVR_Parameters, ONLY:  NDVR_X, NDVR_Y, NDVR_Z, ort_kx, ort_ky, ort_kz
USE FFT_Laboratory, ONLY: Get_FFT

!> input variables
COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z), INTENT(INOUT) :: PSI
INTEGER :: direction
!> loop counters
INTEGER :: I,J, K,IND



IF (direction .eq. 1) THEN

 CALL Get_FFT(1,PSI,0,1,MYID=666)
!> apply ik_x to form derivative
   Do K=1,NDVR_X
    Do I=1,NDVR_Z
     Do J=1,NDVR_Y
   ind=k+(J-1)*NDVR_X+(I-1)*NDVR_X*NDVR_Y
   PSI(ind)=PSI(ind)*dcmplx(0.d0,1.d0)*ort_kx(K)/NDVR_X

     EndDo
    EndDo
   EndDo

!> backward FFT for derivative in x direction
 CALL Get_FFT(1,PSI,1,1,MYID=666)
ENDIF

IF (direction .eq. 2) THEN

 CALL Get_FFT(1,PSI,0,2,MYID=666)
!> apply ik_y to form derivative

   Do K=1,NDVR_X
    Do I=1,NDVR_Z
     Do J=1,NDVR_Y
   ind=k+(J-1)*NDVR_X+(I-1)*NDVR_X*NDVR_Y
   PSI(ind)=PSI(ind)*dcmplx(0.d0,1.d0)*ort_ky(J)/NDVR_Y

     EndDo
    EndDo
   EndDo

!> backward FFT for derivative in x direction
 CALL Get_FFT(1,PSI,1,2,MYID=666)

ENDIF
IF (direction .eq. 3) THEN
 CALL Get_FFT(1,PSI,0,3,MYID=666)

!> apply ik_z to form derivative
   Do K=1,NDVR_X
    Do I=1,NDVR_Z
     Do J=1,NDVR_Y
   ind=k+(J-1)*NDVR_X+(I-1)*NDVR_X*NDVR_Y
   PSI(ind)=PSI(ind)*dcmplx(0.d0,1.d0)*ort_kz(I)/NDVR_Z

     EndDo
    EndDo
   EndDo

!> backward FFT for derivative in x direction
 CALL Get_FFT(1,PSI,1,3,MYID=666)
ENDIF
end subroutine get_derivative


!> Subrotine to calculate expectation values and variances of one-body operators; x, y, z, Lz, px,py,pz
subroutine Get_Variance_observables(time,PSI,rho_ij,rho_ijkl,Vtrap_Ext,TotalEnergy) 
use Coefficients_Parameters,ONLY:Npar
use DVR_Parameters
use Auxiliary_Analysis_Routines, ONLY: Get_KineticEnergyAction_AllOrbitalsAnalysis
use FFT_Laboratory
use OMP_lib
use MPI
implicit none
!>       Inputs
  real*8, INTENT(IN) :: time
  COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb), INTENT(IN) :: PSI
  complex*16, INTENT(IN) :: rho_ij(Morb,Morb)
  complex*16, INTENT(IN) :: rho_ijkl(Morb,Morb,Morb,Morb)
  COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z), INTENT(IN) :: Vtrap_Ext
  real*8, INTENT(IN) :: TotalEnergy
!>
 integer ::  i,j,k,Iorb,Jorb,ind,icnt,Korb,Lorb,P,ierr
 complex*16 :: XSQ,YSQ,ZSQ,PXSQ,PYSQ,PZSQ
 complex*16 :: LZSQ
 REAL*8 :: x_exp, y_exp,z_exp,Lz_exp,Px_exp,Py_exp,Pz_exp
 REAL*8 :: xsq_exp,ysq_exp,zsq_exp,Lzsq_exp,Pxsq_exp,Pysq_exp,Pzsq_exp
 REAL*8 :: Var_x, Var_y, Var_z, Var_Px, Var_Py, Var_Pz, Var_Lz
 integer :: cK,cJ,cL,cI,L2B
 COMPLEX*16, DIMENSION(Morb,Morb) :: XMAT,YMAT,ZMAT 
 COMPLEX*16, DIMENSION(Morb,Morb) :: X2MAT,Y2MAT,Z2MAT 
 COMPLEX*16, DIMENSION(Morb,Morb) :: PXMAT,PYMAT, PZMAT
 COMPLEX*16, DIMENSION(Morb,Morb) :: PX2MAT,PY2MAT,PZ2MAT
 COMPLEX*16, DIMENSION(Morb,Morb) :: VMAT !< array for the matrix elements of the potential
 REAL*8 :: Xfin2, Yfin2, Zfin2, Lzfin2,Pxfin2, Pyfin2, Pzfin2
 COMPLEX*16, DIMENSION(Morb,Morb) :: LZMAT,LZ2MAT
 
 REAL*8, ALLOCATABLE, DIMENSION(:) :: X,Y,Z,X2,Y2,Z2
 COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z)  :: DPSI,D1PSI
 COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z)  :: DDPSI,DDDPSI
 COMPLEX*16 :: DPSI2,DDPSI2
 COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z)  :: DXPSI
 COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z)  :: D2XPSI
 COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z)  :: DYPSI
 COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z)  :: D2YPSI
 COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z)  :: DYDXPSI
 COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb)  :: PSI_KE !< array for orbitals after the kinetic energy has been applied
 COMPLEX*16, DIMENSION(Morb,Morb) :: Kinetic !< array for the matrix elements of the kinetic energy
 REAL*8 :: V, Venergy !< value of \f$E_{pot}\f$
 REAL*8 :: E,KE !< value of \f$E_{kin}\f$
 REAL*8 :: W, T !< value of \f$E_{W}\f$
 integer*8 :: n !< loop index 
 
  
 ALLOCATE(X(NDVR_X*NDVR_Y*NDVR_Z),stat=ierr) 
 if(ierr /= 0) write(*,*)"allocation error Get_Variance_observables"
 ALLOCATE(Y(NDVR_X*NDVR_Y*NDVR_Z),stat=ierr) 
 if(ierr /= 0) write(*,*)"allocation error Get_Variance_observables"
 ALLOCATE(Z(NDVR_X*NDVR_Y*NDVR_Z),stat=ierr) 
 if(ierr /= 0) write(*,*)"allocation error Get_Variance_observables"
 ALLOCATE(X2(NDVR_X*NDVR_Y*NDVR_Z),stat=ierr) 
 if(ierr /= 0) write(*,*)"allocation error Get_Variance_observables"
 ALLOCATE(Y2(NDVR_X*NDVR_Y*NDVR_Z),stat=ierr) 
 if(ierr /= 0) write(*,*)"allocation error Get_Variance_observable"
 ALLOCATE(Z2(NDVR_X*NDVR_Y*NDVR_Z),stat=ierr) 
 if(ierr /= 0) write(*,*)"allocation error Get_Variance_observable"
 
 ind=1
 Do K=1,NDVR_Z
    Do J=1,NDVR_Y
       Do I=1,NDVR_X
               X(ind)=ort_X(I)
               Y(ind)=ort_Y(J)
               Z(ind)=ort_Z(K)
               X2(ind)=ort_X(I)**2
               Y2(ind)=ort_Y(J)**2
               Z2(ind)=ort_Z(K)**2
              
               ind=ind+1
        Enddo
     Enddo
  Enddo
         
       XMAT=0.d0
       YMAT=0.d0
       ZMAT=0.d0
       X2MAT=0.d0
       Y2MAT=0.d0
       Z2MAT=0.d0
       PXMAT=0.d0
       PYMAT=0.d0
       PZMAT=0.d0
       PX2MAT=0.d0
       PY2MAT=0.d0
       PZ2MAT=0.d0
       LZMAT=0.d0
       LZ2MAT=0.d0
DO Iorb=1,Morb
   DO Jorb=Iorb,Morb
      !> compute matrix elements of X/Y/Z and store them in XMAT/YMAT/ZMAT 
      XMAT(Iorb,Jorb)=SUM(PSI(:,Jorb)*(X(:))*dconjg(PSI(:,Iorb)))
      YMAT(Iorb,Jorb)=SUM(PSI(:,Jorb)*(Y(:))*dconjg(PSI(:,Iorb)))   
      ZMAT(Iorb,Jorb)=SUM(PSI(:,Jorb)*(Z(:))*dconjg(PSI(:,Iorb)))
      !> compute matrix elements of X^2/Y^2/Z^2 and store them in X2MAT/Y2MAT/Z2MAT 
      X2MAT(Iorb,Jorb)=SUM(PSI(:,Jorb)*(X2(:))*dconjg(PSI(:,Iorb)))
      Y2MAT(Iorb,Jorb)=SUM(PSI(:,Jorb)*(Y2(:))*dconjg(PSI(:,Iorb)))
      Z2MAT(Iorb,Jorb)=SUM(PSI(:,Jorb)*(Z2(:))*dconjg(PSI(:,Iorb)))
      
      !> compute derivative of the Jorb orbital in x direction 
      DPSI=PSI(:,Jorb)
      CALL get_derivative(DPSI,1)

      !> compute matrix elements of p_x, < psi_i | p_x | psi_j>
      PXMAT(Iorb,Jorb)=dcmplx(0.d0,1.d0)*SUM(DPSI*dconjg(PSI(:,Iorb)))
      
      !> compute second derivative of the Jorb orbital in x direction 
      DDPSI=DPSI
      CALL get_derivative(DDPSI,1)

      !> compute matrix elements of p_x^2,  < psi_i | p_x^2 | psi_j> 
      PX2MAT(Iorb,Jorb)=-1.d0*SUM(DDPSI*dconjg(PSI(:,Iorb)))

      !write(6,*) "MATRIX ELEMENT ", Iorb,Jorb, "of px2: ", PX2MAT(Iorb,Jorb)
      IF (DIM_MCTDH.GT.1) THEN
      !> compute derivative of the Jorb orbital in y direction 
            DPSI=PSI(:,Jorb)         
            CALL get_derivative(DPSI,2)
      !> compute matrix elements of p_y, < psi_i | p_y | psi_j>
      PYMAT(Iorb,Jorb)=dcmplx(0.d0,1.d0)*SUM(DPSI*dconjg(PSI(:,Iorb)))
      
      !> compute second derivative of the Jorb orbital in y direction 
      DDPSI=DPSI
      CALL get_derivative(DDPSI,2)

      !> compute matrix elements of p_y^2,  < psi_i | p_y^2 | psi_j> 
      PY2MAT(Iorb,Jorb)=-1.d0*SUM(DDPSI*dconjg(PSI(:,Iorb)))  
          
      !> compute angular momentum  
      !> compute derivative of the Jorb orbital in x direction 
         
      DXPSI=PSI(:,Jorb)
      CALL get_derivative(DXPSI,1)
      !> compute derivative of the Jorb orbital in y direction          
      DYPSI=PSI(:,Jorb)
      CALL get_derivative(DYPSI,2)
      ! compute matrix elements of L_z, < psi_i | L_z | psi_j> 
      
      LZMAT(Iorb,Jorb)=DCMPLX(0.d0,1.d0)*SUM(DYPSI*(X(:))*dconjg(PSI(:,Iorb)))-DCMPLX(0.d0,1.d0)*SUM(DXPSI*(Y(:))*dconjg(PSI(:,Iorb)))
      
      !> compute second derivative of the Jorb orbital in x direction 
      D2XPSI=DXPSI
      CALL get_derivative(D2XPSI,1)
      !> compute second derivative of the Jorb orbital in y direction      
      D2YPSI=DYPSI
      CALL get_derivative(D2YPSI,2)
            
      DYDXPSI=DXPSI
     ! DYDXPSI=DYPSI
      CALL get_derivative(DYDXPSI,2) 
                 
      LZ2MAT(Iorb,Jorb)=-1.d0*SUM(D2YPSI*(X2(:))*dconjg(PSI(:,Iorb)))-1.d0*SUM(D2XPSI*(Y2(:))*dconjg(PSI(:,Iorb))) &
                        +2.d0*SUM(DYDXPSI*(Y(:))*(X(:))*dconjg(PSI(:,Iorb)))+1.d0*SUM(DXPSI*(X(:))*dconjg(PSI(:,Iorb))) &
                        +1.d0*SUM(DYPSI*(Y(:))*dconjg(PSI(:,Iorb)))
      
      ENDIF  
      
      IF (DIM_MCTDH.GT.2) THEN
      !> compute derivative of the Jorb orbital in z direction 
      DPSI=PSI(:,Jorb)
      CALL get_derivative(DPSI,3)

      ! compute matrix elements of p_z, < psi_i | p_z | psi_j>
      PZMAT(Iorb,Jorb)=dcmplx(0.d0,-1.d0)*SUM(DPSI*dconjg(PSI(:,Iorb)))
      
      !> compute second derivative of the Jorb orbital in x direction 
      DDPSI=DPSI
      CALL get_derivative(DDPSI,3)

      !> compute matrix elements of p_z^2,  < psi_i | p_z^2 | psi_j> 
      PZ2MAT(Iorb,Jorb)=-1.d0*SUM(DDPSI*dconjg(PSI(:,Iorb)))
      
               
      ENDIF  
            
            XMAT(Jorb,Iorb)=Conjg(XMAT(Iorb,Jorb))    
            YMAT(Jorb,Iorb)=Conjg(YMAT(Iorb,Jorb))    
            ZMAT(Jorb,Iorb)=Conjg(ZMAT(Iorb,Jorb))    
            X2MAT(Jorb,Iorb)=Conjg(X2MAT(Iorb,Jorb))    
            Y2MAT(Jorb,Iorb)=Conjg(Y2MAT(Iorb,Jorb))    
            Z2MAT(Jorb,Iorb)=Conjg(Z2MAT(Iorb,Jorb))      
            PXMAT(Jorb,Iorb)=Conjg(PXMAT(Iorb,Jorb))    
            PX2MAT(Jorb,Iorb)=Conjg(PX2MAT(Iorb,Jorb))         
            PYMAT(Jorb,Iorb)=Conjg(PYMAT(Iorb,Jorb))    
            PY2MAT(Jorb,Iorb)=Conjg(PY2MAT(Iorb,Jorb)) 
            PZMAT(Jorb,Iorb)=Conjg(PZMAT(Iorb,Jorb))
            PZ2MAT(Jorb,Iorb)=Conjg(PZ2MAT(Iorb,Jorb))
            LZMAT(Jorb,Iorb)=Conjg(LZMAT(Iorb,Jorb))    
            LZ2MAT(Jorb,Iorb)=Conjg(LZ2MAT(Iorb,Jorb)) 
      
  EndDo
EndDo 

 !> Two-body contribution TO X2/Y2/Z2/PX2/PY2/PZ2;  <a^2>_2Body=SUM_IJKL RHO(I,J,K,L)*a(I,K)*a(J,L)  
 XSQ=0.d0
 YSQ=0.d0
 ZSQ=0.d0
 PXSQ=0.d0
 PYSQ=0.d0
 PZSQ=0.d0
 LZSQ=0.d0
 
DO CI=1,Morb
   DO CJ=1,Morb
      DO CK=1,Morb
         DO CL=1,Morb
            XSQ=XSQ+rho_ijkl(CI,CJ,CK,CL)*XMAT(CI,CK)*XMAT(CJ,CL)
            YSQ=YSQ+rho_ijkl(CI,CJ,CK,CL)*YMAT(CI,CK)*YMAT(CJ,CL)
            ZSQ=ZSQ+rho_ijkl(CI,CJ,CK,CL)*ZMAT(CI,CK)*ZMAT(CJ,CL)
            PXSQ=PXSQ+rho_ijkl(CI,CJ,CK,CL)*PXMAT(CI,CK)*PXMAT(CJ,CL)
            PYSQ=PYSQ+Rho_ijkl(CI,CJ,CK,CL)*PYMAT(CI,CK)*PYMAT(CJ,CL)
            PZSQ=PZSQ+Rho_ijkl(CI,CJ,CK,CL)*PZMAT(CI,CK)*PZMAT(CJ,CL)
            LZSQ=LZSQ+rho_ijkl(CI,CJ,CK,CL)*LZMAT(CI,CK)*LZMAT(CJ,CL)
         END DO
      END DO
   END DO
END DO
!> Expectation values per particle   
 x_exp=SUM(RHO_IJ*XMAT)/DBLE(Npar) !<x>/N
 y_exp=SUM(RHO_IJ*YMAT)/DBLE(Npar) !<y>/N
 z_exp=SUM(RHO_IJ*ZMAT)/DBLE(Npar) !<z>/N
 Lz_exp=SUM(RHO_IJ*LZMAT)/DBLE(Npar) !<Lz>/N
 Px_exp=SUM(RHO_IJ*PXMAT)/DBLE(Npar) !<Px>/N
 Py_exp=SUM(RHO_IJ*PYMAT)/DBLE(Npar) !<Py>/N
 Pz_exp=SUM(RHO_IJ*PZMAT)/DBLE(Npar) !<Pz>/N
 !>Expextation value of  <x^2>
 Xfin2=SUM(RHO_IJ*X2MAT)
 Yfin2=SUM(RHO_IJ*Y2MAT)
 Zfin2=SUM(RHO_IJ*Z2MAT)
 Lzfin2=SUM(RHO_IJ*LZ2MAT)
 Pxfin2=SUM(RHO_IJ*PX2MAT)
 Pyfin2=SUM(RHO_IJ*PY2MAT)
 Pzfin2=SUM(RHO_IJ*PZ2MAT)
 
 !> Expectation of square of the operator
 xsq_exp=(Real(XSQ)+Real(Xfin2))/DBLE(Npar) !<x^2>/N
 ysq_exp=(Real(YSQ)+Real(Yfin2))/DBLE(Npar) !<Y^2>/N
 zsq_exp=(Real(ZSQ)+Real(Zfin2))/DBLE(Npar) !<z^2>/N
 Lzsq_exp=(Real(LZSQ)+Real(Lzfin2))/DBLE(Npar) !<x^2>/N
 Pxsq_exp=(Real(PXSQ)+Real(Pxfin2))/DBLE(Npar) !<x^2>/N
 Pysq_exp=(Real(PYSQ)+Real(Pyfin2))/DBLE(Npar) !<x^2>/N
 Pzsq_exp=(Real(PZSQ)+Real(Pzfin2))/DBLE(Npar) !<x^2>/N
 
!>Variance of Position x,y,z
 Var_x=xsq_exp-x_exp**2*DBLE(Npar) !Var(x)/N 
 Var_y=ysq_exp-y_exp**2*DBLE(Npar) !Var(y)/N
 Var_z=zsq_exp-z_exp**2*DBLE(Npar) !Var(z)/N
 !>Variance of momentum Px, Py, Pz
 Var_Px=Pxsq_exp-Px_exp**2*DBLE(Npar) !Var(Px)/N 
 Var_Py=Pysq_exp-Py_exp**2*DBLE(Npar) !Var(Py)/N
 Var_Pz=Pzsq_exp-Pz_exp**2*DBLE(Npar) !Var(Pz)/N
 !> Variance of angular momentum Lz
 Var_Lz=Lzsq_exp-Lz_exp**2*DBLE(Npar) !Var(Lz)/N
 !Write(6,*) "Angular Momentum Variance", Var_Lz 
 
 !>evaluate kinetic energy and potential energy matrix elements
 PSI_KE=PSI
 CALL Get_KineticEnergyAction_AllOrbitalsAnalysis(PSI_KE)

!> initialize the matrix elements of potential/kinetic energy
Kinetic=0.d0
VMAT=0.d0

do n=1,NDVR_X*NDVR_Y*NDVR_Z
   do Jorb = 1,Morb  
      do Korb=1,Morb
         VMAT(Jorb,Korb) = VMAT(Jorb,Korb) + DCONJG(PSI(n,Jorb))*Vtrap_Ext(n)*PSI(n,Korb)
         Kinetic(Jorb,Korb) = Kinetic(Jorb,Korb) +DCONJG(PSI(n,Jorb))*PSI_KE(n,Korb)
      end do
   end do
end do
V = 0.d0
KE = 0.d0

call Get_Full_Rij(Rho_IJ)
do Jorb = 1,Morb  
   do Korb=1,Morb
       V = V + rho_ij(jorb,korb)*VMAT(jorb,korb)
       KE = KE + rho_ij(jorb,korb)*Kinetic(jorb,korb)
   end do
end do

W=TotalEnergy-(V+KE)

!> Write different Expectations and Variances to a file.
IF (abs(Time-Time_From).lt.0.1d-10) THEN
 ! open(unit=541,file='Variances.dat',form='formatted',STATUS='UNKNOWN')
 open(unit=541,file='Variances.dat',form='formatted',STATUS='REPLACE')
ELSE
  open(unit=541,file='Variances.dat',form='formatted',STATUS='OLD',ACCESS='APPEND')
ENDIF
 
 write(541,6145) time, ' ', x_exp, ' ', y_exp, ' ', Var_x, ' ', Var_y, ' ', Px_exp, ' ', Py_exp, ' ', Lz_exp, ' ',Var_Px, ' ', Var_Py, ' ', Var_Lz, ' '
! insert whatever you want to print
 6145  format(99(F53.16,a1))
 
 close(541)     

 DEALLOCATE(X,Y,Z,X2,Y2,Z2)
 
end subroutine Get_Variance_observables
!>
!> This routine computes the correlation functions 
!> G1(r,r), G1(r,r'), G1(r',r') and G2(r,r',r',r), where r=(x,y,z), 
!> and writes them to a file. The first six columns contain
!> x,y,z,x',y',z'. 

subroutine get_correlations_Nlevel(time,PSI,mode,rho_jk,rho_ijkl,dilation) 
use Coefficients_Parameters,ONLY:Npar
use DVR_Parameters
  implicit none
  integer  :: NOGRIDPOINTS
  integer  :: dilation
  integer,intent(in) :: mode
  real*8,intent(in)  :: time
  real*8     :: w,dk,mom_X(NDVR_X),mom_Y(NDVR_Y),mom_Z(NDVR_Z)
   COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb,Nlevel)  :: PSI
  complex*16,intent(in) :: rho_ijkl(Morb,Morb,Morb,Morb)
  complex*16,intent(in) :: rho_jk(Morb,Morb)
  character(len=2)   :: aString
  character(len=200) :: fullName
  integer*8          :: I,m,n,i_m,j_m,k_m,i_n,j_n,k_n
  integer            :: Iorb,Jorb,Korb,Lorb
  complex*16,DIMENSION(Nlevel)         :: G1m_m,G1m_n,G1n_n,G2m_n

  real*8,parameter   :: TOL=0.00001d0 
  real*8     :: threshold
  real*8     :: outG1m_m(Nlevel),outReG1m_n(Nlevel),outImG1m_n(Nlevel),outG1n_n(Nlevel),outG2m_n(Nlevel)
  real*8             :: xi,xf,yi,yf,zi,zf



  call Get_WeightOrMomentumGrid(mom_x,mom_y,mom_z,w,dilation,mode)

  if (mode.eq.1) then
     aString = 'x-'
  else if (mode.eq.2) then 
     aString = 'k-'
  else 
     write(*,*)"ERROR in get_correlations"
     stop
  endif 

  call Assemble_OutputFileName(FullName,Time,NPar,Morb,aString,'correlations.dat')

  open(unit=12,file=trim(fullName),form='formatted')

  NOGRIDPOINTS=NDVR_X*NDVR_Y*NDVR_Z

  do n=1,NOGRIDPOINTS

!compute G1n_n
     G1n_n = (0.d0,0.d0)
     do Jorb = 1,Morb  
        do Korb=1,Morb
           DO I=1,NLevel
              G1n_n(I) = G1n_n(I) + rho_jk(Jorb,Korb) * DCONJG(PSI(n,Jorb,I))*PSI(n,Korb,I)
           end do
        end do
     end do
     G1n_n=G1n_n*1.d0/w**2 
     outG1n_n=DBLE(G1n_n)
!---------------
     do m=1,NOGRIDPOINTS         

!compute G1m_m
        G1m_m = (0.d0,0.d0) 
        do Jorb = 1,Morb  
           do Korb=1,Morb
             DO I=1,NLevel

              G1m_m(I) = G1m_m(I) + rho_jk(Jorb,Korb) * DCONJG(PSI(m,Jorb,I))*PSI(m,Korb,I)

             END DO
           end do
        end do
        G1m_m=G1m_m*1.d0/w**2 
        outG1m_m=DBLE(G1m_m)   
!----------------
        
!compute G1m_n
        G1m_n = (0.d0,0.d0) 
        do Jorb = 1,Morb  
           do Korb=1,Morb
             DO I=1,NLevel

              G1m_n(I) = G1m_n(I) + rho_jk(Jorb,Korb) * DCONJG(PSI(m,Jorb,I))*PSI(n,Korb,I)

             END DO
           end do
        end do
        G1m_n=G1m_n*1.d0/(w**2) 
        outReG1m_n=DBLE(G1m_n)   
        outImG1m_n=DIMAG(G1m_n)
!----------------


!compute G2m_n 
        G2m_n=(0.d0,0.d0)
        do Lorb=1,Morb
           do Korb=1,Morb
              do Jorb=1,Morb
                 do Iorb=1,Morb
                   DO I=1,NLevel
                   
                    G2m_n(I) = G2m_n(I) + rho_ijkl(Iorb,Jorb,Korb,Lorb)* &
                              DCONJG(PSI(m,Iorb,I))*DCONJG(PSI(n,Jorb,I))*PSI(m,Korb,I)*PSI(n,Lorb,I)
  
                   END DO
                 end do
              end do
           end do
        end do
        G2m_n=G2m_n*1.d0/(w*w*w*w) 
        outG2m_n=DBLE(G2m_n)

        call get_ijk_from_m(m,NDVR_X,NDVR_Y,i_m,j_m,k_m)
        call get_ijk_from_m(n,NDVR_X,NDVR_Y,i_n,j_n,k_n)

        if (mode.eq.1) then
           write(12,306) ort_X(i_m)," ",ort_Y(j_m)," ",ort_Z(k_m)," ",ort_X(i_n)," ",ort_Y(j_n)," ",ort_Z(k_n)," ", &
                       (outG1m_m(I)," ",outReG1m_n(I)," ",outImG1m_n(I)," ",outG1n_n(I)," ",outG2m_n(I)," ",I=1,Nlevel)
        else if (mode.eq.2) then

           write(12,306) mom_X(i_m)," ",mom_Y(j_m)," ",mom_Z(k_m)," ",mom_X(i_n)," ",mom_Y(j_n)," ",mom_Z(k_n)," ", &
                       (outG1m_m(I)," ",outReG1m_n(I)," ",outImG1m_n(I)," ",outG1n_n(I)," ",outG2m_n(I)," ",I=1,Nlevel)
        end if

        if(MOD(m,NDVR_X).eq.0) write(12,*)"                                            " 

     end do
                             
  end do         
  close(12)
 
 
306  format(6(F8.3,a1),100(F16.9,a1))
end subroutine get_correlations_Nlevel

END MODULE Analysis_Routines_TwoBody
