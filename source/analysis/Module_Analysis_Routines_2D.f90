!>GPLv3
!    MCTDH-X: the multiconfigurational time-dependent Hartree for 
!    indistinguishable particles software
!
!    Copyright (C) 2021  A. U. J. Lode, M. C. Tsatsos, 
!                        E. Fasshauer, S. E. Weiner, 
!                        R. Lin, L. Papariello, P. Molignini, 
!                        C. Lévêque, M. Büttner , J. Xiang, S. Dutta
!
!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <https://www.gnu.org/licenses/>.
!
!
!
!
!<GPLv3










!> This module collects the analysis routines dedicated to two-dimensional R-MCTDHB simulations.
MODULE Analysis_Routines_2D

use Analysis_Input_Variables
USE Interaction_Parameters
use Global_Parameters
USE Matrix_Elements
USE Function_Library
USE Auxiliary_Routines
USE Auxiliary_Analysis_Routines

IMPLICIT NONE
CONTAINS

!> This routine computes the skew diagonal of 2D correlation functions 
!> G1(r,r'=-r), and G2(r,r'=-r), where r=(x,y), 
!> and writes them to a file. The first four columns contain
!> x,y,x',y'. 

subroutine get_correlationsSkewDiagonal2D(time,PSI,mode,rho_jk,rho_ijkl,dilation) 
use Coefficients_Parameters,ONLY:Npar
use DVR_Parameters
  implicit none
  integer  :: NOGRIDPOINTS
  integer  :: DIL 
  integer, optional :: dilation
  integer,intent(in) :: mode
  real*8,intent(in)  :: time
  real*8     :: w,dk,mom_X(NDVR_X),mom_Y(NDVR_Y),mom_Z(NDVR_Z)
   COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb)  :: PSI
  complex*16,intent(in) :: rho_ijkl(Morb,Morb,Morb,Morb)
  complex*16,intent(in) :: rho_jk(Morb,Morb)
  character(len=2)   :: aString
  character(len=200) :: fullName
  character(len=10)  :: timeAsString
  character(len=10)  :: MorbAsString
  character(len=1)   :: NparAsString1
  character(len=2)   :: NparAsString2
  character(len=3)   :: NparAsString3
  character(len=4)   :: NparAsString4
  character(len=5)   :: NparAsString5
  character(len=6)   :: NparAsString6
  character(len=7)   :: NparAsString7
  character(len=8)   :: NparAsString8
  integer            :: Iorb,Jorb,Korb,Lorb
  integer*8          :: ind,m,n,i_m,j_m,k_m,i_n,j_n,k_n
  complex*16         :: G1m_m,G1m_n,G1n_n,G2m_n,G2m_m(NDVR_X*NDVR_Y*NDVR_Z)

  complex*16         :: maximum,integ,integ2
  real*8,parameter   :: TOL=0.00001d0 
  real*8     :: threshold
  real*8     :: outG1m_m,outReG1m_n,outImG1m_n,outG1n_n,outG2m_n,outG2m_m
  real*8             :: xi,xf,yi,yf,zi,zf

  integ=0.d0
  integ2=0.d0

  if (present(dilation).eqv..FALSE.) THEN
     dil=1
  ELSE
     dil=dilation
  ENDIF

  NOGRIDPOINTS=NDVR_X*NDVR_Y*NDVR_Z
   xi=X_initial
   xf=X_final
  IF (DIM_MCTDH.ge.2) THEN
   yi=Y_initial
   yf=Y_final
  ENDIF

  IF (DIM_MCTDH.ge.3) THEN
   zi=Z_initial
   zf=Z_final
  ENDIF

  if(mode.eq.1) then
     w=DSQRT((xf-xi)/NDVR_X)
     IF (DIM_MCTDH.ge.2) THEN
        w=w*DSQRT((yf-yi)/NDVR_Y)
     ENDIF
     IF (DIM_MCTDH.ge.3) THEN
        w=w*DSQRT((zf-zi)/NDVR_Z)
     ENDIF
  else if (mode.eq.2) then
     call get_mom(xi,xf,NDVR_X,mom_X,dil)
     dk     = mom_X(2)-mom_X(1) 

     IF (DIM_MCTDH.ge.2) THEN
        call get_mom(yi,yf,NDVR_Y,mom_Y,dil)
        dk     = dk*(mom_Y(2)-mom_Y(1))
     ENDIF
     IF (DIM_MCTDH.ge.3) THEN
        call get_mom(zi,zf,NDVR_Z,mom_Z,dil)
        dk     = dk*(mom_Z(2)-mom_Z(1))
     ENDIF
     w      = DSQRT( dk )
  else 
        write(*,*) "wrong mode"
  end if
  
  if (MORB.lt.10) then
     write (MorbAsString, '(I1)') Morb
  else if ((MORB.ge.10).and.(MORB.lt.100)) then
     write (MorbAsString, '(I2)') Morb
  else 
     write(*,*) 'Bigger orbital number than 100 not implemented!'
  endif

  call getDoubleAsString(time,timeAsString) 

  if (mode.eq.1) then
     aString = 'x-'
  else if (mode.eq.2) then 
     aString = 'k-'
  else 
     write(*,*)"ERROR in get_correlations"
     stop
  endif   

  if ((NPar.gt.0) .and. (NPar.lt.10)) then 
    write (NParAsString1, '(I1)') NPar
    fullName=timeasString//'N'//trim(NParAsString1)//'M'//trim(MorbAsString)//aString//'SkewCorrelations.dat'
  else  if ((NPar.ge.10) .and. (NPar.lt.100)) then 
    write (NParAsString2, '(I2)') NPar
    fullName=timeasString//'N'//trim(NParAsString2)//'M'//trim(MorbAsString)//aString//'SkewCorrelations.dat'
  else if ((NPar.ge.100) .and. (NPar.lt.1000)) then 
    write (NParAsString3, '(I3)') NPar
    fullName=timeasString//'N'//trim(NParAsString3)//'M'//trim(MorbAsString)//aString//'SkewCorrelations.dat'
  else if ((NPar.ge.1000) .and. (NPar.lt.10000)) then 
    write (NParAsString4, '(I4)') NPar
    fullName=timeasString//'N'//trim(NParAsString4)//'M'//trim(MorbAsString)//aString//'SkewCorrelations.dat'
  else  if ((NPar.ge.10000) .and. (NPar.lt.100000)) then 
    write (NParAsString5, '(I5)') NPar
    fullName=timeasString//'N'//trim(NParAsString5)//'M'//trim(MorbAsString)//aString//'SkewCorrelations.dat'
  else if ((NPar.ge.100000) .and. (NPar.lt.1000000)) then 
    write (NParAsString6, '(I6)') NPar
    fullName=timeasString//'N'//trim(NParAsString6)//'M'//trim(MorbAsString)//aString//'SkewCorrelations.dat'
  else if ((NPar.ge.1000000) .and. (NPar.lt.10000000)) then 
    write (NParAsString7, '(I7)') NPar
    fullName=timeasString//'N'//trim(NParAsString7)//'M'//trim(MorbAsString)//aString//'SkewCorrelations.dat'
  else if ((NPar.ge.10000000) .and. (NPar.lt.100000000)) then 
    write (NParAsString8, '(I8)') NPar
    fullName=timeasString//'N'//trim(NParAsString8)//'M'//trim(MorbAsString)//aString//'SkewCorrelations.dat'
  else 
    NParAsString4='XXXX'
    fullName=timeasString//'N'//trim(NParAsString4)//'M'//trim(MorbAsString)//aString//'SkewCorrelations.dat'
  end if
 ! needs to be edited for other dvrs with nonconstant weights! 
  call get_maximum(PSI,rho_jk,maximum)
  threshold = Kdip * DBLE(maximum)/w**2 ! set the minimal density to be plotted

  ind=0 

  open(unit=12,file=trim(fullName),form='formatted')

  G2m_m=(0.d0,0.d0)

  do m=1,NOGRIDPOINTS
        call get_ijk_from_m(m,NDVR_X,NDVR_Y,i_m,j_m,k_m)
        do Lorb=1,Morb
           do Korb=1,Morb
              do Jorb=1,Morb
                 do Iorb=1,Morb
                    G2m_m(m) = G2m_m(m) + rho_ijkl(Iorb,Jorb,Korb,Lorb)* &
                              DCONJG(PSI(m,Iorb))*DCONJG(PSI(m,Jorb))*PSI(m,Korb)*PSI(m,Lorb)
     
                 end do
              end do
           end do
        end do
        G2m_m(m)=G2m_m(m)/w**4
  end do 


  do n=1,NOGRIDPOINTS

!---------------
     do m=1,NOGRIDPOINTS         

        call get_ijk_from_m(m,NDVR_X,NDVR_Y,i_m,j_m,k_m)
        call get_ijk_from_m(n,NDVR_X,NDVR_Y,i_n,j_n,k_n)

!!!! Exception for points on the skew-diagonal
!!!! Exception for points on the skew-diagonal
        if (((mode.eq.1).and.((abs((ort_X(i_m)+ort_X(i_n))).lt.1.d-9).and.&
                              (abs((ort_Y(j_m)+ort_Y(j_n))).lt.1.d-9))).or.&
           ((mode.eq.2).and.((abs((mom_X(i_m)+mom_X(i_n))).lt.1.d-9).and.&
                            ((abs(mom_Y(j_m)+mom_Y(j_n))).lt.1.d-9)))) THEN
!!!! Exception for points on the skew-diagonal
!!!! Exception for points on the skew-diagonal

        ind=ind+1   

!compute G1n_n
        G1n_n = (0.d0,0.d0)
        do Jorb = 1,Morb  
           do Korb=1,Morb
   
              G1n_n = G1n_n + rho_jk(Jorb,Korb) * DCONJG(PSI(n,Jorb))*PSI(n,Korb)
   
           end do
        end do
        G1n_n=G1n_n*1.d0/w**2 
        outG1n_n=DBLE(G1n_n)




!compute G1m_m
        G1m_m = (0.d0,0.d0) 
        do Jorb = 1,Morb  
           do Korb=1,Morb

              G1m_m = G1m_m + rho_jk(Jorb,Korb) * DCONJG(PSI(m,Jorb))*PSI(m,Korb)

           end do
        end do
        G1m_m=G1m_m*1.d0/w**2 
        outG1m_m=DBLE(G1m_m)   
!----------------
        
!compute G1m_n
        G1m_n = (0.d0,0.d0) 
        do Jorb = 1,Morb  
           do Korb=1,Morb

              G1m_n = G1m_n + rho_jk(Jorb,Korb) * DCONJG(PSI(m,Jorb))*PSI(n,Korb)

           end do
        end do
        G1m_n=G1m_n*1.d0/(w**2) 
        outReG1m_n=DBLE(G1m_n)   
        outImG1m_n=DIMAG(G1m_n)
        if (m.eq.n) integ=integ+G1m_n
!----------------


!compute G2m_n 
        G2m_n=(0.d0,0.d0)
        do Lorb=1,Morb
           do Korb=1,Morb
              do Jorb=1,Morb
                 do Iorb=1,Morb
                    G2m_n = G2m_n + rho_ijkl(Iorb,Jorb,Korb,Lorb)* &
                              DCONJG(PSI(m,Iorb))*DCONJG(PSI(n,Jorb))*PSI(m,Korb)*PSI(n,Lorb)
  
                 end do
              end do
           end do
        end do
        G2m_n=G2m_n*1.d0/(w*w*w*w) 
        integ2=integ2+G2m_n
        outG2m_n=DBLE(G2m_n)
        outG2m_m=DBLE(G2m_m(m))

        if (mode.eq.1) then
           write(12,6666) ort_X(i_m)," ",ort_Y(j_m)," ", &
                       outG1m_m," ",outReG1m_n," ",outImG1m_n," ",outG1n_n," ",outG2m_n," ",outG2m_m
        else if (mode.eq.2) then

           write(12,6666) mom_X(i_m)," ",mom_Y(j_m)," ", &
                       outG1m_m," ",outReG1m_n," ",outImG1m_n," ",outG1n_n," ",outG2m_n," ",outG2m_m
        end if

        if(MOD(ind,NDVR_X-1).eq.0) write(12,*)"                                            " 

!!!! Exception for points on the skew-diagonal
!!!! Exception for points on the skew-diagonal
        end if
!!!! Exception for points on the skew-diagonal
!!!! Exception for points on the skew-diagonal

     end do
                             
  end do         
  close(12)

6666  format(2(E25.16,a1),6(E25.16,a1))
end subroutine get_correlationsSkewDiagonal2D




!> This routine computes cuts of the correlation functions in two spatial dimensions
!> G1(r,r), G1(r,r'), G1(r',r') and G2(r,r',r',r), where r=(x,y), 
!> and writes them to a file.  
!> The user has to choose two of the coordinates to stay constant, to obtain a 3D plot.
subroutine get_corr_slice(time,PSI,mode,rho_jk,rho_ijkl,&
                       x1const,x1slice,y1const,y1slice,x2const,x2slice,y2const,y2slice,dilation) 
use Coefficients_Parameters,ONLY:Npar
use DVR_Parameters
  implicit none
  integer  :: NOGRIDPOINTS
  integer,intent(in) :: mode
  integer,intent(in) :: dilation
  real*8,intent(in)  :: time
  real*8,intent(in)  :: x1slice,x2slice,y1slice,y2slice
  logical,intent(in)  :: x1const,y1const,x2const,y2const
  logical  :: x1done,y1done,x2done,y2done

  real*8     :: w,dk,mom_X(NDVR_X),mom_Y(NDVR_Y)
   COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb)  :: PSI
  complex*16,intent(in) :: rho_ijkl(Morb,Morb,Morb,Morb)
  complex*16,intent(in) :: rho_jk(Morb,Morb)
  character(len=2)   :: aString
  character(len=200) :: fullName   
  character(len=10)  :: timeAsString
  character(len=7)  :: slice1AsString
  character(len=7)  :: slice2AsString
  real*8            :: slice1,slice2
  integer           :: slice1pt,slice2pt
  character(len=10)  :: MorbAsString
  character(len=1)   :: NparAsString1
  character(len=2)   :: NparAsString2
  character(len=3)   :: NparAsString3
  character(len=4)   :: NparAsString4
  character(len=5)   :: NparAsString5
  character(len=6)   :: NparAsString6
  character(len=7)   :: NparAsString7
  character(len=8)   :: NparAsString8
  integer*8          :: m,n,i_m,j_m,k_m,i_n,j_n,k_n
  integer            :: Iorb,Jorb,Korb,Lorb
  complex*16         :: G1m_n,G1n_n,G2m_n
  Real*8             :: onebdens(NDVR_X*NDVR_Y)

  complex*16         :: maximum,integ,integ2
  real*8,parameter   :: TOL=0.00001d0 
  real*8     :: threshold
  real*8     :: outReG1m_n,outImG1m_n,outG2m_n
  real*8             :: xi,xf,yi,yf

  if (NDVR_Y.eq.1) then
     write(*,*) 'The sliced correlations are implemented for >2D systems only!!'
     return
  endif

 
  integ=0.d0
  integ2=0.d0


  NOGRIDPOINTS=NDVR_X*NDVR_Y*NDVR_Z
  xi=X_initial
  xf=X_final
  yi=X_initial
  yf=X_final
  if(mode.eq.1) then
      w=weight(1)
  end if
  if (mode.eq.2) then
     call get_mom(xi,xf,NDVR_X,mom_X)
     call get_mom(yi,yf,NDVR_Y,mom_Y)
     mom_X=mom_X/dilation
     mom_Y=mom_Y/dilation
     dk  = 2.d0*PI/((xf-xi)*dilation)*2.d0*PI/((yf-yi)*dilation)
     w   = sqrt( dk )


  end if
 

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! some exceptions 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  if (mode.eq.1) then

  if ((x1const.eqv..true.).and.((x1slice.lt.xi).or.(x1slice.gt.xf))) then
     write(*,*) 'The wanted x1 slice of the correlations is outside the grid!!!'
     return 
  endif
  if ((x2const.eqv..true.).and.((x2slice.lt.xi).or.(x2slice.gt.xf))) then
     write(*,*) 'The wanted x1 slice of the correlations is outside the grid!!!'
     return 
  endif
  if ((y1const.eqv..true.).and.((y1slice.lt.yi).or.(y1slice.gt.yf))) then
     write(*,*) 'The wanted x1 slice of the correlations is outside the grid!!!'
     return 
  endif
  if ((y2const.eqv..true.).and.((y2slice.lt.xi).or.(y2slice.gt.xf))) then
     write(*,*) 'The wanted x1 slice of the correlations is outside the grid!!!'
     return 
  endif

  else if (mode.eq.2) then

  if ((x1const.eqv..true.).and.((x1slice.gt.mom_x(NDVR_X)).or.(x1slice.lt.mom_x(1)))) then
     write(*,*) 'The wanted x1 slice of the correlations is outside the grid!!!'
     return 
  endif
  if ((x2const.eqv..true.).and.((x2slice.gt.mom_x(NDVR_X)).or.(x2slice.lt.mom_x(1)))) then
     write(*,*) 'The wanted x1 slice of the correlations is outside the grid!!!'
     return 
  endif
  if ((y1const.eqv..true.).and.((y1slice.gt.mom_y(NDVR_X)).or.(y1slice.lt.mom_y(1)))) then
     write(*,*) 'The wanted x1 slice of the correlations is outside the grid!!!'
     return 
  endif
  if ((y2const.eqv..true.).and.((y2slice.gt.mom_y(NDVR_X)).or.(y2slice.lt.mom_y(1)))) then
     write(*,*) 'The wanted x1 slice of the correlations is outside the grid!!!'
     return 
  endif

  endif 
  m=0


  if (x1const.eqv..true.) then
      m=m+1
      call getsliceasstring(1,x1slice,slice1AsString)
      slice1=x1slice
  endif
  if (y1const.eqv..true.) then
      m=m+1
      if (m.eq.1) then
         call getsliceasstring(2,y1slice,slice1AsString)
         slice1=y1slice
      endif
      if (m.eq.2) then
         call getsliceasstring(2,y1slice,slice2AsString)
         slice2=y1slice
      endif
  endif
  if (x2const.eqv..true.) then 
      m=m+1
      if (m.eq.1) then 
         call getsliceasstring(3,x2slice,slice1AsString)
         slice1=x2slice
      endif
      if (m.eq.2) then 
        call getsliceasstring(3,x2slice,slice2AsString)
        slice2=x2slice
      endif
  endif
  if (y2const.eqv..true.) then 
     m=m+1
     if (m.eq.1) then
        call getsliceasstring(4,y2slice,slice1AsString)
        slice1=y2slice
      endif
     if (m.eq.2) then
        call getsliceasstring(4,y2slice,slice2AsString)
        slice2=y2slice
      endif
  endif

  if (m.ne.2) then
      write(*,*) 'You must keep two of the four coordinates of the correlations constant!!!'
      return
  endif

 
  if (MORB.lt.10) then
     write (MorbAsString, '(I1)') Morb
  else if ((MORB.ge.10).and.(MORB.lt.100)) then
     write (MorbAsString, '(I2)') Morb
  else 
     write(*,*) 'Bigger orbital number than 100 not implemented!'
  endif

  call getDoubleAsString(time,timeAsString) 

  if (mode.eq.1) then
     aString = 'x-'
  else if (mode.eq.2) then 
     aString = 'k-'
  else 
     write(*,*)"ERROR in get_correlations"
     stop
  endif   
!   assemble filename
  if ((NPar.gt.0) .and. (NPar.lt.10)) then 
    write (NParAsString1, '(I1)') NPar
    fullName=timeasString//'N'//trim(NParAsString1)//'M'//trim(MorbAsString)//aString//slice1Asstring//'-'//slice2Asstring//'-'//'correlations.dat'
  else  if ((NPar.ge.10) .and. (NPar.lt.100)) then 
    write (NParAsString2, '(I2)') NPar
    fullName=timeasString//'N'//trim(NParAsString2)//'M'//trim(MorbAsString)//aString//slice1Asstring//'-'//slice2Asstring//'-'//'correlations.dat'
  else if ((NPar.ge.100) .and. (NPar.lt.1000)) then 
    write (NParAsString3, '(I3)') NPar
    fullName=timeasString//'N'//trim(NParAsString3)//'M'//trim(MorbAsString)//aString//slice1Asstring//'-'//slice2Asstring//'-'//'correlations.dat'
  else if ((NPar.ge.1000) .and. (NPar.lt.10000)) then 
    write (NParAsString4, '(I4)') NPar
    fullName=timeasString//'N'//trim(NParAsString4)//'M'//trim(MorbAsString)//aString//slice1Asstring//'-'//slice2Asstring//'-'//'correlations.dat'
  else if ((NPar.ge.10000) .and. (NPar.lt.100000)) then 
    write (NParAsString5, '(I5)') NPar
    fullName=timeasString//'N'//trim(NParAsString5)//'M'//trim(MorbAsString)//aString//slice1Asstring//'-'//slice2Asstring//'-'//'correlations.dat'

  else if ((NPar.ge.100000) .and. (NPar.lt.1000000)) then 
    write (NParAsString6, '(I6)') NPar
    fullName=timeasString//'N'//trim(NParAsString6)//'M'//trim(MorbAsString)//aString//slice1Asstring//'-'//slice2Asstring//'-'//'correlations.dat'

  else if ((NPar.ge.1000000) .and. (NPar.lt.10000000)) then 
    write (NParAsString7, '(I7)') NPar
    fullName=timeasString//'N'//trim(NParAsString7)//'M'//trim(MorbAsString)//aString//slice1Asstring//'-'//slice2Asstring//'-'//'correlations.dat'

  else if ((NPar.ge.10000000) .and. (NPar.lt.100000000)) then 
    write (NParAsString8, '(I8)') NPar
    fullName=timeasString//'N'//trim(NParAsString8)//'M'//trim(MorbAsString)//aString//slice1Asstring//'-'//slice2Asstring//'-'//'correlations.dat'

  else 
    NParAsString4='XXXX'
    fullName=timeasString//'N'//trim(NParAsString4)//'M'//trim(MorbAsString)//aString//slice1Asstring//'-'//slice2Asstring//'-'//'correlations.dat'
  end if
  call get_maximum(PSI,rho_jk,maximum)
  threshold = Kdip * DBLE(maximum)/w**2 ! set the minimal density to be plotted
  
  open(unit=12,file=trim(fullName),form='formatted')
  do n=1,NOGRIDPOINTS
!compute one body density onebdens 
     G1n_n = (0.d0,0.d0)
     do Jorb = 1,Morb  
        do Korb=1,Morb
           G1n_n = G1n_n + rho_jk(Jorb,Korb) * DCONJG(PSI(n,Jorb))*PSI(n,Korb)
        end do
     end do
     G1n_n=G1n_n*1.d0/w**2 
     onebdens(n)=DBLE(G1n_n)
!---------------
   end do

! determine which points are to be plotted    
   x1done=.FALSE.
   y1done=.FALSE.
   x2done=.FALSE.
   y2done=.FALSE.


   do n=1,NOGRIDPOINTS
      call get_ijk_from_m(n,NDVR_X,NDVR_Y,i_n,j_n,k_n)

 IF (x1done.eqv..FALSE.) then
!     x1slice
      if ((mode.eq.1).and.((abs(ort_X(i_n)-x1slice).lt.ort_X(2)-ort_X(1)).and.(x1const.eqv..true.))&
     .or.((mode.eq.2).and.(abs(mom_x(i_n)-x1slice).lt.mom_X(2)-mom_X(1)).and.(x1const.eqv..true.))) then
          call get_ijk_from_m(n+1,NDVR_X,NDVR_Y,i_m,j_m,k_m) 
          if ((abs(ort_X(i_n)-x1slice)).lt.(abs(ort_X(i_m)-x1slice))) then
             slice1pt=i_n
                write(*,*) 'I assigned 1 slice1',i_n
             x1done=.TRUE.
          else
             slice1pt=i_m
                write(*,*) 'I assigned 2 slice1',i_m
             x1done=.TRUE.
          endif
      endif
 ENDIF


 IF (y1done.eqv..FALSE.) then
!     y1slice

      if (((mode.eq.1).and.(abs(ort_Y(j_n)-y1slice).lt.ort_Y(2)-ort_Y(1)).and.(y1const.eqv..true.)).or.&
      ((mode.eq.2).and.(abs(mom_Y(j_n)-y1slice).lt.mom_Y(2)-mom_Y(1)).and.(y1const.eqv..true.))) then
          call get_ijk_from_m(n+1,NDVR_X,NDVR_Y,i_m,j_m,k_m) 
          if ((abs(ort_Y(j_n)-y1slice)).lt.(abs(ort_Y(j_m)-y1slice))) then
             if (x1const.eqv..false.)  then
                slice1pt=j_n
                write(*,*) 'I assigned 3 slice1',j_n
                y1done=.TRUE.
             endif
          else
             if (x1const.eqv..false.) slice1pt=j_m
             if  (x1const.eqv..true.) then 
                slice2pt=j_m
                write(*,*) 'I assigned 4 slice2',j_m
                y1done=.TRUE.
                goto 8855
             endif
          endif
      endif
 ENDIF
 IF (x2done.eqv..FALSE.) then
!     x2slice
      if (((mode.eq.1).and.(abs(ort_X(i_n)-x2slice).lt.ort_x(2)-ort_x(1)).and.(x2const.eqv..true.)).or.&
       ((mode.eq.2).and.(abs(mom_X(i_n)-x2slice).lt.mom_x(2)-mom_x(1)).and.(x2const.eqv..true.))) then
          call get_ijk_from_m(n+1,NDVR_X,NDVR_Y,i_m,j_m,k_m) 
          if ((abs(ort_x(i_n)-x2slice)).lt.(abs(ort_X(i_m)-x2slice))) then
             if ((x1const.eqv..false.).and.(y1const.eqv..false.)) then
                    slice1pt=i_n
                    write(*,*) 'I assigned 5 slice2=',i_n
             endif
             if ((x1const.eqv..true.).or.(y1const.eqv..true.)) then
                 slice2pt=i_n
                 write(*,*) 'I assigned 6 slice2=',i_n
                 x2done=.TRUE.
                 goto 8855
             endif
          else
             if ((x1const.eqv..false.).and.(y1const.eqv..false.)) then 
                 slice1pt=i_m
                 write(*,*) 'I assigned 7 slice2=',i_m
             endif
             if  ((x1const.eqv..true.).or.(y1const.eqv..true.)) then 
                 slice2pt=i_m
                 write(*,*) 'I assigned 8 slice2=',i_m
                 x2done=.TRUE.
                goto 8855
             endif
          endif
      endif
 ENDIF


 IF (y2done.eqv..FALSE.) then
!     y2slice
      if (((mode.eq.1).and.(abs(ort_Y(j_n)-y2slice).lt.ort_Y(2)-ort_Y(1)).and.(y2const.eqv..true.)).or.&
      ((mode.eq.2).and.(abs(mom_Y(j_n)-y2slice).lt.mom_Y(2)-mom_Y(1)).and.(y2const.eqv..true.))) then 
          call get_ijk_from_m(n+1,NDVR_X,NDVR_Y,i_m,j_m,k_m) 
          if ((abs(ort_Y(j_n)-y2slice)).lt.(abs(ort_Y(j_m)-y2slice))) then
             if ((x1const.eqv..true.).or.(y1const.eqv..true.).or.((x2const.eqv..true.))) then
                 slice2pt=j_n
                 y2done=.TRUE.
                 write(*,*) 'I assigned 9 slice2=',j_n
                 goto 8855
             else 
                 write(*,*) 'Something went wrong with the assignment of the slices!!!'
                 stop
             endif
          else
             if ((x1const.eqv..true.).or.(y1const.eqv..true.).or.((x2const.eqv..true.))) then
                slice2pt=j_m
                 write(*,*) 'I assigned 10 slice2=',j_m
                y2done=.TRUE.
                goto 8855
             else 
                 write(*,*) 'Something went wrong with the assignment of the slices!!!'
                 stop
             endif
          endif
       endif
 ENDIF
 8855 continue
   end do
! 8855 continue
 do n=1,NOGRIDPOINTS
   call get_ijk_from_m(n,NDVR_X,NDVR_Y,i_n,j_n,k_n)
!  x1const and y1const
   if (((x1const.eqv..true.).and.(i_n.eq.slice1pt)).and.&
      ((y1const.eqv..true.).and.(j_n.eq.slice2pt))) then
   do m=1,NOGRIDPOINTS
      call get_ijk_from_m(m,NDVR_X,NDVR_Y,i_m,j_m,k_m)

!compute G1m_n
        G1m_n = (0.d0,0.d0) 
        do Jorb = 1,Morb  
           do Korb=1,Morb
              G1m_n = G1m_n + rho_jk(Jorb,Korb) * DCONJG(PSI(m,Jorb))*PSI(n,Korb)
           end do
        end do
        G1m_n=G1m_n*1.d0/(w**2) 
        outReG1m_n=DBLE(G1m_n)   
        outImG1m_n=DIMAG(G1m_n)
        if (m.eq.n) integ=integ+G1m_n
!----------------


!compute G2m_n 
        G2m_n=(0.d0,0.d0)
        do Lorb=1,Morb
           do Korb=1,Morb
              do Jorb=1,Morb
                 do Iorb=1,Morb
                    G2m_n = G2m_n + rho_ijkl(Iorb,Jorb,Korb,Lorb)* &
                              DCONJG(PSI(m,Iorb))*DCONJG(PSI(n,Jorb))*PSI(m,Korb)*PSI(n,Lorb)
  
                 end do
              end do
           end do
        end do
        G2m_n=G2m_n*1.d0/(w*w*w*w) 
        integ2=integ2+G2m_n
        outG2m_n=DBLE(G2m_n)
!----------------

        if (mode.eq.1) then
           write(12,6666) ort_X(i_m),ort_Y(j_m),ort_X(i_n),ort_Y(j_n), &
                       onebdens(m),onebdens(n),outReG1m_n,outImG1m_n,outG2m_n
        else if (mode.eq.2) then
           write(12,6666) mom_X(i_m),mom_Y(j_m),mom_X(i_n),mom_Y(j_n), &
                       onebdens(m),onebdens(n),outReG1m_n,outImG1m_n,outG2m_n
        end if
     if (mod(m,NDVR_X).eq.0)     write(12,*)"                                                       " 

     end do
! if x1const and x2const or y2const
     else if ((x1const.eqv..true.).and.((x2const.eqv..true.).or.(y2const.eqv..true.))&
              .and.(i_n.eq.slice1pt)) then 
     do m=1,NOGRIDPOINTS

        call get_ijk_from_m(m,NDVR_X,NDVR_Y,i_m,j_m,k_m)
! if the point is in the slice
        if (((x2const.eqv..true.).and.(i_m.eq.slice2pt))&
         .or.((y2const.eqv..true.).and.(j_m.eq.slice2pt))) then
!compute G1m_n
        G1m_n = (0.d0,0.d0) 
        do Jorb = 1,Morb  
           do Korb=1,Morb
              G1m_n = G1m_n + rho_jk(Jorb,Korb) * DCONJG(PSI(m,Jorb))*PSI(n,Korb)
           end do
        end do
        G1m_n=G1m_n*1.d0/(w**2) 
        outReG1m_n=DBLE(G1m_n)   
        outImG1m_n=DIMAG(G1m_n)
        if (m.eq.n) integ=integ+G1m_n
!----------------


!compute G2m_n 
        G2m_n=(0.d0,0.d0)
        do Lorb=1,Morb
           do Korb=1,Morb
              do Jorb=1,Morb
                 do Iorb=1,Morb
                    G2m_n = G2m_n + rho_ijkl(Iorb,Jorb,Korb,Lorb)* &
                              DCONJG(PSI(m,Iorb))*DCONJG(PSI(n,Jorb))*PSI(m,Korb)*PSI(n,Lorb)
  
                 end do
              end do
           end do
        end do
        G2m_n=G2m_n*1.d0/(w*w*w*w) 
        integ2=integ2+G2m_n
        outG2m_n=DBLE(G2m_n)
!----------------

        if (mode.eq.1) then
           write(12,6666) ort_X(i_m),ort_Y(j_m),ort_X(i_n),ort_Y(j_n), &
                       onebdens(m),onebdens(n),outReG1m_n,outImG1m_n,outG2m_n
        else if (mode.eq.2) then
           write(12,6666) mom_X(i_m),mom_Y(j_m),mom_X(i_n),mom_Y(j_n), &
                       onebdens(m),onebdens(n),outReG1m_n,outImG1m_n,outG2m_n
        end if

        endif ! loops only executed if point is in the slice
     enddo
     write(12,*) "                                                " 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!  if x1const false, y1const true and x2/y2const true
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     elseif ((x1const.eqv..false.).and.(y1const.eqv..true.).and.(j_n.eq.slice1pt)) then

     do m=1,NOGRIDPOINTS
        call get_ijk_from_m(m,NDVR_X,NDVR_Y,i_m,j_m,k_m)
! if the point is in the slice
        if (((x2const.eqv..true.).and.(i_m.eq.slice2pt))&
         .or.((y2const.eqv..true.).and.(j_m.eq.slice2pt))) then
!compute G1m_n
        G1m_n = (0.d0,0.d0) 
        do Jorb = 1,Morb  
           do Korb=1,Morb
              G1m_n = G1m_n + rho_jk(Jorb,Korb) * DCONJG(PSI(m,Jorb))*PSI(n,Korb)
           end do
        end do
        G1m_n=G1m_n*1.d0/(w**2) 
        outReG1m_n=DBLE(G1m_n)   
        outImG1m_n=DIMAG(G1m_n)
        if (m.eq.n) integ=integ+G1m_n
!----------------


!compute G2m_n 
        G2m_n=(0.d0,0.d0)
        do Lorb=1,Morb
           do Korb=1,Morb
              do Jorb=1,Morb
                 do Iorb=1,Morb
                    G2m_n = G2m_n + rho_ijkl(Iorb,Jorb,Korb,Lorb)* &
                              DCONJG(PSI(m,Iorb))*DCONJG(PSI(n,Jorb))*PSI(m,Korb)*PSI(n,Lorb)
  
                 end do
              end do
           end do
        end do
        G2m_n=G2m_n*1.d0/(w*w*w*w) 
        integ2=integ2+G2m_n
        outG2m_n=DBLE(G2m_n)
!----------------

        if (mode.eq.1) then
           write(12,6666) ort_X(i_m),ort_Y(j_m),ort_X(i_n),ort_Y(j_n), &
                       onebdens(m),onebdens(n),outReG1m_n,outImG1m_n,outG2m_n
        else if (mode.eq.2) then
           write(12,6666) mom_X(i_m),mom_Y(j_m),mom_X(i_n),mom_Y(j_n), &
                       onebdens(m),onebdens(n),outReG1m_n,outImG1m_n,outG2m_n
        end if

        endif ! loops only executed if point is in the slice
     enddo
      write(12,*)"                                                 " 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!  if x1const false, y1const false and x2const and y2const true
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     elseif ((x1const.eqv..false.).and.(y1const.eqv..false.)) then
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     do m=1,NOGRIDPOINTS
        call get_ijk_from_m(m,NDVR_X,NDVR_Y,i_m,j_m,k_m)
! if the point is in the slice
        if (((x2const.eqv..true.).and.(i_m.eq.slice1pt))&
         .and.((y2const.eqv..true.).and.(j_m.eq.slice2pt))) then
!compute G1m_n
        G1m_n = (0.d0,0.d0) 
        do Jorb = 1,Morb  
           do Korb=1,Morb
              G1m_n = G1m_n + rho_jk(Jorb,Korb) * DCONJG(PSI(m,Jorb))*PSI(n,Korb)
           end do
        end do
        G1m_n=G1m_n*1.d0/(w**2) 
        outReG1m_n=DBLE(G1m_n)   
        outImG1m_n=DIMAG(G1m_n)
        if (m.eq.n) integ=integ+G1m_n
!----------------


!compute G2m_n 
        G2m_n=(0.d0,0.d0)
        do Lorb=1,Morb
           do Korb=1,Morb
              do Jorb=1,Morb
                 do Iorb=1,Morb
                    G2m_n = G2m_n + rho_ijkl(Iorb,Jorb,Korb,Lorb)* &
                              DCONJG(PSI(m,Iorb))*DCONJG(PSI(n,Jorb))*PSI(m,Korb)*PSI(n,Lorb)
  
                 end do
              end do
           end do
        end do
        G2m_n=G2m_n*1.d0/(w*w*w*w) 
        integ2=integ2+G2m_n
        outG2m_n=DBLE(G2m_n)
!----------------
        if (mode.eq.1) then
           write(12,6666) ort_X(i_m),ort_Y(j_m),ort_X(i_n),ort_Y(j_n), &
                       onebdens(m),onebdens(n),outReG1m_n,outImG1m_n,outG2m_n
           if (mod(i_n,NDVR_X).eq.0) then 
              write(12,*) "                                                                   "
           endif
        else if (mode.eq.2) then

           write(12,6666) mom_X(i_m),mom_Y(j_m),mom_X(i_n),mom_Y(j_n), &
                       onebdens(m),onebdens(n),outReG1m_n,outImG1m_n,outG2m_n
           if (mod(j_n,NDVR_Y).eq.0) then 
              write(12,*) "                                                                   "
           endif


        end if

        endif ! loops only executed if point is in the slice
     enddo
     endif !loop m only if n is in the slice
  end do         

 6666 FORMAT(9(E20.10))

  close(12)
 
 
end subroutine get_corr_slice

!> This routine calculates a projected potential where the second spatial dimension
!> is integrated weighted with the 1-body-density.
SUBROUTINE Get_VtrapProjection(PSI,Rho_IJ,time,dir)
  use Global_Parameters
  USE Coefficients_Parameters
  USE DVR_Parameters
  USE Interaction_Parameters
  USE Orbital_Allocatables, ONLY: VTRAP_Ext

  IMPLICIT NONE


  COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb)       :: PSI
  COMPLEX*16, ALLOCATABLE ::  Vtrap_NormX(:,:,:)
  COMPLEX*16, ALLOCATABLE ::  Vtrap_NormY(:,:,:)
  COMPLEX*16, DIMENSION(MOrb,MOrb) ::  Rho_IJ
  COMPLEX*16, ALLOCATABLE ::  VX(:,:,:)
  REAL*8, ALLOCATABLE  ::  VX_EFF(:)
  COMPLEX*16, ALLOCATABLE ::  VY(:,:,:)
  REAL*8, ALLOCATABLE  ::  VY_EFF(:)
  REAL*8  :: time 
  INTEGER :: I,J,K,L,M,N,O,ierr


  character(len=200) :: fullName
  character(len=10)  :: timeAsString
  character(len=10)  :: MorbAsString
  character(len=1)   :: dir 
  character(len=1)   :: NparAsString1
  character(len=2)   :: NparAsString2
  character(len=3)   :: NparAsString3
  character(len=4)   :: NparAsString4
  character(len=5)   :: NparAsString5
  character(len=6)   :: NparAsString6
  character(len=7)   :: NparAsString7
  character(len=8)   :: NparAsString8

  IF (DIR.eq.'B') then
     N=1
     O=2
  ELSE IF (DIR.eq.'X') then
     N=1
     O=1
  ELSE IF (DIR.eq.'Y') then
     N=2
     O=2
  ENDIF

  do M=N,O 

    IF (M.eq.1) DIR='X'

    IF (M.eq.2) DIR='Y'

 
    IF (DIR.eq.'X') then
 
          allocate(VX_EFF(NDVR_X),stat=ierr)
          if(ierr /= 0) then
             write(*,*)"allocation error for VX_EFF"
          endif
          allocate(Vtrap_NormX(NDVR_X,MORB,MORB),stat=ierr)
          if(ierr /= 0) then
             write(*,*)"allocation error for Vtrap_norm"
          endif
          allocate(VX(NDVR_X,MORB,MORB),stat=ierr)
          if(ierr /= 0) then
             write(*,*)"allocation error for VX"
          endif
  
    ELSEIF (DIR.eq.'Y') then 
 
          allocate(VY_EFF(NDVR_Y),stat=ierr)
          if(ierr /= 0) then
             write(*,*)"allocation error for VX_EFF"
          endif
 
          allocate(Vtrap_NormY(NDVR_Y,MORB,MORB),stat=ierr)
          if(ierr /= 0) then
             write(*,*)"allocation error for Vtrap_norm"
          endif
          allocate(VY(NDVR_Y,MORB,MORB),stat=ierr)
          if(ierr /= 0) then
             write(*,*)"allocation error for VX"
          endif
    ENDIF
 
    write(6,*) "Vtrap projections "
    write(6,*) "V_ij(x)= int(psi*(x,y) V(x,y) psi(x,y))dy for 2D Only" 
    if(DIM_MCTDH.ne.2) then
      write(6,*)"It does not work in 1D and 3D "
      return         
    endif
 
    VX=(0d0,0d0)
    Vtrap_NormX=(0.d0,0.d0)
    VX_EFF=(0.d0,0.d0)
    VY=(0d0,0d0)
    Vtrap_NormY=(0.d0,0.d0)
    VY_EFF=(0.d0,0.d0)
    if (dir.eq.'X') then
  
    DO L=1,MORB
      DO K=1,MORB
 
        DO J=1,NDVR_Y
          DO I=1,NDVR_X
 
             VX(I,L,K)=VX(I,L,K)+Rho_IJ(L,K)*&
                                 Conjg(PSI(I+NDVR_X*(J-1),L))*VTRAP_EXT(I+NDVR_X*(J-1))&
                                 *PSI(I+NDVR_X*(J-1),K)&
                                 /weight_Y(J)/weight_Y(J)!/weight(I+NDVR_X*(J-1))/weight(I+NDVR_X*(J-1))
             Vtrap_NormX(I,L,K)=Vtrap_NormX(I,L,K)+Conjg(PSI(I+NDVR_X*(J-1),L))&
                                                *PSI(I+NDVR_X*(J-1),K)/weight_Y(J)/weight_Y(J)

          ENDDO
 
        ENDDO
 
      ENDDO
    ENDDO
 
       
         Do I=1,NDVR_X
         Do L=1,Morb
         Do K=1,Morb
         if(ABS(Vtrap_NormX(I,L,K)).ge.1.0d-16) then
            VX(I,L,K)=VX(I,L,K)/Vtrap_NormX(I,L,K)
         else
            VX(I,L,K)=VX(I,L,K)/1.0d-16
         endif
         ENDDO
         ENDDO
         ENDDO

     DO L=1,MORB
      DO K=1,MORB
 
        DO I=1,NDVR_X
    
          VX_EFF(I)=VX_EFF(I)+ABS(VX(I,L,K))
        ENDDO
 
      ENDDO
    ENDDO
 
 
    else if (dir.eq.'Y') then
    DO L=1,MORB
      DO K=1,MORB
 
         DO J=1,NDVR_Y
         DO I=1,NDVR_X

             VY(J,L,K)=VY(J,L,K)+Rho_IJ(L,K)*&
                                 Conjg(PSI(I+NDVR_X*(J-1),L))*VTRAP_EXT(I+NDVR_X*(J-1))&
                                 *PSI(I+NDVR_X*(J-1),K)&
                                 /weight_X(I)/weight_X(I)!/weight(I+NDVR_X*(J-1))/weight(I+NDVR_X*(J-1))
             Vtrap_NormY(J,L,K)=Vtrap_NormY(J,L,K)+Conjg(PSI(I+NDVR_X*(J-1),L))&
                                                *PSI(I+NDVR_X*(J-1),K)/weight_X(I)/weight_X(I)
          ENDDO
 
        ENDDO
 
      ENDDO
    ENDDO
         

!    VY=VY/Vtrap_NormY
           Do I=1,NDVR_Y
           Do L=1,Morb
           Do K=1,Morb
           if(ABS(Vtrap_NormY(I,L,K)).ge.1.0d-16) then
              VY(I,L,K)=VY(I,L,K)/Vtrap_NormY(I,L,K)
           else
              VY(I,L,K)=1.0d+16
           endif
           ENDDO
           ENDDO
           ENDDO

     DO L=1,MORB
      DO K=1,MORB
 
        DO I=1,NDVR_Y
    
          VY_EFF(I)=VY_EFF(I)+ABS(VY(I,L,K))
        ENDDO
 
      ENDDO
    ENDDO
 
    endif
 
    if (MORB.lt.10) then
       write (MorbAsString, '(I1)') Morb
    else if ((MORB.ge.10).and.(MORB.lt.100)) then
       write (MorbAsString, '(I2)') Morb
    else 
       write(*,*) 'Bigger orbital number than 100 not implemented!'
    endif
    call getDoubleAsString(time,timeAsString) 
 
    if ((NPar.gt.0) .and. (NPar.lt.10)) then 
      write (NParAsString1, '(I1)') NPar
      fullName=timeasString//'N'//trim(NParAsString1)//'M'//trim(MorbAsString)//'Vtrap_proj_'//trim(dir)//'.dat'
    else  if ((NPar.ge.10) .and. (NPar.lt.100)) then 
      write (NParAsString2, '(I2)') NPar
      fullName=timeasString//'N'//trim(NParAsString2)//'M'//trim(MorbAsString)//'Vtrap_proj_'//trim(dir)//'.dat'
    else if ((NPar.ge.100) .and. (NPar.lt.1000)) then 
      write (NParAsString3, '(I3)') NPar
      fullName=timeasString//'N'//trim(NParAsString3)//'M'//trim(MorbAsString)//'Vtrap_proj_'//trim(dir)//'.dat'
    else if ((NPar.ge.1000) .and. (NPar.lt.10000)) then 
      write (NParAsString4, '(I4)') NPar
      fullName=timeasString//'N'//trim(NParAsString4)//'M'//trim(MorbAsString)//'Vtrap_proj_'//trim(dir)//'.dat'
    else if ((NPar.ge.10000) .and. (NPar.lt.100000)) then 
      write (NParAsString5, '(I5)') NPar
      fullName=timeasString//'N'//trim(NParAsString5)//'M'//trim(MorbAsString)//'Vtrap_proj_'//trim(dir)//'.dat'
    else if ((NPar.ge.100000) .and. (NPar.lt.1000000)) then 
      write (NParAsString6, '(I6)') NPar
      fullName=timeasString//'N'//trim(NParAsString6)//'M'//trim(MorbAsString)//'Vtrap_proj_'//trim(dir)//'.dat'
    else if ((NPar.ge.1000000) .and. (NPar.lt.10000000)) then 
      write (NParAsString7, '(I7)') NPar
      fullName=timeasString//'N'//trim(NParAsString7)//'M'//trim(MorbAsString)//'Vtrap_proj_'//trim(dir)//'.dat'
    else if ((NPar.ge.10000000) .and. (NPar.lt.100000000)) then 
      write (NParAsString8, '(I8)') NPar
      fullName=timeasString//'N'//trim(NParAsString8)//'M'//trim(MorbAsString)//'Vtrap_proj_'//trim(dir)//'.dat'
    else 
      NParAsString4='XXXX'
      fullName=timeasString//'N'//trim(NParAsString4)//'M'//trim(MorbAsString)//'Vtrap_proj_'//trim(dir)//'.dat'
    end if
 
 
    open(unit=120,file=trim(fullName),form='formatted')
    if (DIR.eq.'X') then 
    DO I=1,NDVR_X
      write(120,'(99F22.16)') ort_x(i),ABS(VX_EFF(i)),((ABS(VX(i,K,L)),K=1,MORB),L=1,MORB)
    ENDDO
    elseif (DIR.eq.'Y') then 
    DO I=1,NDVR_Y
      write(120,'(99F22.16)') ort_Y(i),ABS(VY_EFF(i)),((ABS(VY(i,K,L)),K=1,MORB),L=1,MORB)
    ENDDO
    endif
    close(120)
  end do

    IF(ALLOCATED(VX_EFF)) deallocate(VX_EFF)
    IF(ALLOCATED(VY_EFF)) deallocate(VY_EFF)
    IF(ALLOCATED(VX)) deallocate(VX)
    IF(ALLOCATED(VY)) deallocate(VY)
    IF(ALLOCATED(Vtrap_normX)) deallocate(Vtrap_normX)
    IF(ALLOCATED(Vtrap_normY)) deallocate(Vtrap_normY)
     
    return         
END  SUBROUTINE Get_VtrapProjection


!> This routine Calculates the angular momentum expectation and matrix elements of a given 2-dimensional wavefunction
SUBROUTINE Get_LZ(PSI,NOcc,NatVec,time)
  use Global_Parameters
  USE DVR_Parameters
  USE Interaction_Parameters
  USE KineticEnergyAction
  USE Analysis_Input_Variables,ONLY:Time_From,Time_To

  REAL*8,INTENT(IN) :: time 
  REAL*8 :: norm
  COMPLEX*16 :: Total_L_z,Orbital_L_z(MOrb,MOrb)
  COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb), INTENT(IN)       :: PSI
  COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb)                   :: PSIsave
  REAL*8, DIMENSION(MOrb),             INTENT(IN)       :: NOcc
  COMPLEX*16, DIMENSION(MOrb,MOrb),        INTENT(IN)       :: NatVec
  COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb)       :: PSI2
  INTEGER :: K,I
CALL GET_NOs(PSI,PSI2,NatVec)
PSIsave=PSI2
DO I=1,MOrb
   CALL Get_KineticEnergyAction_L_FFT(psi2(:,I))
   call normvxz(psi2(:,I),norm,NDVR_X*NDVR_Y)
   do K=1,MORB  
     Orbital_L_z(I,K)=sum(dconjg(Psisave(:,K))*Psi2(:,I))
   end do
END DO

Total_L_z=(0.d0,0.d0)
DO I=1,MOrb
   Total_L_z=Total_L_z+(Nocc(I))*Orbital_L_z(I,I)
END DO

IF (abs(Time-Time_From).le.1.d-9) then
   open(unit=122,file='L_z_expectation.dat')
ELSE 
   open(unit=122,file='L_z_expectation.dat',ACCESS='APPEND')
ENDIF

WRITE(122,'(399F22.16)') time,Total_L_z,(((Orbital_L_z(I,K)),I=1,MORB),K=1,MORB)

close(122)

END SUBROUTINE Get_LZ


END MODULE Analysis_Routines_2D
