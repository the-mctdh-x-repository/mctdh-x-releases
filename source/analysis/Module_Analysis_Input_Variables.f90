!>GPLv3
!    MCTDH-X: the multiconfigurational time-dependent Hartree for 
!    indistinguishable particles software
!
!    Copyright (C) 2021  A. U. J. Lode, M. C. Tsatsos, 
!                        E. Fasshauer, S. E. Weiner, 
!                        R. Lin, L. Papariello, P. Molignini, 
!                        C. Lévêque, M. Büttner , J. Xiang, S. Dutta
!
!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <https://www.gnu.org/licenses/>.
!
!
!
!
!<GPLv3










!> This module contains the variables that constitute the input of the analysis program.
!! The input variables are grouped in different namelists.
       MODULE Analysis_Input_Variables

       IMPLICIT NONE

       SAVE 

       REAL*8  ::  time_psi_MAX, time_cic_MAX  
       LOGICAL,      public :: save_anal_input = .FALSE. !< If set to true, it saves the current analysis.inp into a separate file labelled with a timestamp.
       LOGICAL,      public ::  Total_energy = .FALSE.
       LOGICAL,      public ::  AutoDilation =.FALSE.  !< If set to true, Kdip is used to evaluate optimal dilation for FFTs
       LOGICAL,      public ::  MPIFFT_binary = .FALSE. !< If set to true, binary data structure is as from computations with domain decomposed orbitals 
       INTEGER,      public ::  Dilation   = 1         !< controls the factor by which the momentum space resolution is increased by dilation of real space grid.
       REAL*8 ,      public ::  Kdip       = 0.00001d0 !< Controls the cutoff to calculate the dilation if AutoDilation is True.
       LOGICAL,      public ::  Orbitals_Output           = .FALSE. !< Toggle the output of the orbitals in ASCII files.
       LOGICAL,      public ::  FTOrbitals_Output         = .FALSE. !< Toggle the output of the Fourier transformed orbitals in ASCII files.
       LOGICAL,      public ::  Coefficients_Output       = .FALSE. !< Toggle the output of the coefficients in ASCII files.
       LOGICAL,      public ::  MatrixElements_Output      = .FALSE. !< Toggle output of one-body and two-body reduced density matrix elements rho_kq and rho_kqsl.
       LOGICAL,      public ::  HamiltonianElements_Output = .FALSE. !< Toggle output of one-body and two-body Hamiltonian matrix elements h_kq and W_kqsl.
       LOGICAL,      public ::  GetA = .FALSE. !< Toggle separate output of Apair=W_kkjj with k\=j and Adiag=W_jjjj.
       REAL*8  ::  Time_From     = -666.d0 !< Time to start analysis from.
       REAL*8  ::  Time_To       = -666.d0 !< Time to stop analysis at.
       integer ::  Time_points     = 1  !< Number of desired time slices.

       NAMELIST /ZERO_body/& 
       Total_energy,&
       Kdip,&
       Dilation,&
       AutoDilation,&
       MPIFFT_binary,&
       Orbitals_Output,&
       Coefficients_Output,&
       FTOrbitals_Output,&         
       MatrixElements_Output,&     
       HamiltonianElements_Output,&
       GetA,&
       Time_From,&
       Time_To,&
       Time_points,&
       save_anal_input 

       LOGICAL,      public :: density_x  = .FALSE. !< Toggle output of real-space density.
       LOGICAL,      public :: density_k  = .FALSE. !< Toggle output of momentum-space density.
       LOGICAL,      public :: Pnot   = .FALSE. !< Toggle calculation of nonescape probability (density-related).
       REAL*8,       public :: xstart = 0.d0    !< Start of P_not integration.
       REAL*8,       public :: xend   = 0.d0    !< Stop of P_not integration.
       REAL*8,       public :: ystart = 0.d0    !< Start of P_not integration.
       REAL*8,       public :: yend   = 0.d0    !< Stop of P_not integration.
       REAL*8,       public :: zstart = 0.d0    !< Start of P_not integration.
       REAL*8,       public :: zend   = 0.d0    !< Stop of P_not integration.
       LOGICAL,      public :: Phase   = .FALSE. !< Toggle the output of the Phase and Averaged Phase.
       LOGICAL,      public :: Gradient = .FALSE. !< Toggle the output of the Phases' Gradients.
       LOGICAL,      public :: Vorticity = .FALSE. !< Toggle the output of the Vorticity.
       LOGICAL,      public :: CavityOrder = .FALSE. !< Toggle the output of order parameters Theta and B for cavity treatment (compare Nature 464, 1301 (2010)).
       LOGICAL,      public :: CavityLatticeOrder = .FALSE. !<Toggle the output of order parameters imbalance (Theta) for cavity with optical lattice treatment.
       REAL*8,       public :: k_op = -1.d0 !< Wavelength in the imbalance operator, -1.d0 means the same as the pump wavelength, -2.d0 means the same as the lattice wavelength.
       REAL*8,       public :: phi_op=0.d0 !< Phase shift in the imbalance operator, -2.d0 means the same as the lattice phase shift.
       LOGICAL,      public :: Variance=.FALSE. !<Toggle the output of variance <N^2>-<N>^2.
       LOGICAL,      public :: SpinDensity=.FALSE. !<Toggle the expectation values of S0, Sx, Sy, Sz in position space for multi-(two-)component gas.

       NAMELIST /ONE_body/ density_x, & 
                           density_k, &
                           Pnot, & 
                           xstart,&
                           xend,&
                           ystart,&
                           yend,&
                           zstart,&
                           zend,&
                           Phase,&
                           Gradient,&
                           Vorticity,&
                           CavityOrder,&
                           CavityLatticeOrder,&
                           k_op,&
                           phi_op,&
                           Variance,&
                           SpinDensity

       LOGICAL,      public :: Correlations_X= .FALSE. !< Toggle output of full Reduced one-body and two-body densities in real-space.
       LOGICAL,      public :: Correlations_K= .FALSE. !< Toggle output of full Reduced one-body and two-body densities in k-space.
       LOGICAL,      public :: StructureFactor=.FALSE. !< Toggle output of nonlocal correlations (rho2(r,r_ref,r,r_ref)) and dynamic structure factor (FT of nonlocal correlations minus 1)
       REAL*8,       public :: xref= 0.d0 !< x value of reference point for structure factor and nonlocal correlations 
       REAL*8,       public :: yref= 0.d0 !< y value of reference point for structure factor and nonlocal correlations
       REAL*8,       public :: zref= 0.d0 !< z value of reference point for structure factor and nonlocal correlations
       LOGICAL,      public :: Correlation_Coefficient !< toggle output of correlation coefficient.
       LOGICAL,      public :: SkewCorr1D_X= .FALSE. !< Toggle output of skew Reduced one-body and two-body densities in real-space in one-dimensional analysis.
       LOGICAL,      public :: SkewCorr1D_K= .FALSE. !< Toggle output of skew Reduced one-body and two-body densities in k-space in one-dimensional analysis.
       LOGICAL,      public :: Geminals = .FALSE. !< Toggle output of natural geminal occupations
       LOGICAL,      public :: FullGeminals = .FALSE. !< Toggle output of natural geminals and natural geminal occupations
       LOGICAL,      public :: corr1restr= .FALSE. !< Toggle output of restricted Reduced one-body density in real-space.
       LOGICAL,      public :: corr2restr= .FALSE.  !< Toggle output of restricted Reduced two-body density in real-space.
       LOGICAL,      public :: corr1restrmom= .FALSE. !< Toggle output of restricted Reduced one-body density in k-space.
       LOGICAL,      public :: corr2restrmom= .FALSE. !< Toggle output of restricted Reduced two-body density in k-space.
       REAL*8,       public :: xini1= 0.d0 !< Start of restricted one-body density.
       REAL*8,       public :: xfin1= 0.d0 !< Stop of restricted one-body density.
       REAL*8,       public :: xini2= 0.d0 !< Start of restricted two-body density.
       REAL*8,       public :: xfin2= 0.d0 !< Stop of restricted two-body density.
       REAL*8,       public :: kxini1= 0.d0 !< Start of restricted one-body k-space density.
       REAL*8,       public :: kxfin1= 0.d0 !< Stop of restricted one-body k-space density.
       REAL*8,       public :: kxini2= 0.d0  !< Start of restricted two-body k-space density.
       REAL*8,       public :: kxfin2= 0.d0 !< Stop of restricted two-body k-space density.
       integer,      public :: xpts2=0 !< Number of point for restricted two-body density.
       integer,      public :: kpts2=0!< Number of point for restricted two-body k-space density.
       integer,      public :: xpts1=0!< Number of point for restricted one-body density.
       integer,      public :: kpts1=0!< Number of point for restricted one-body k-space density.
       LOGICAL,      public :: Variance_observables=.FALSE. !< Toggle the output of variance and expectation values of position, angular momentum
       
       NAMELIST /TWO_body/ Correlations_X,&
                           Correlations_K,&
                           StructureFactor,&
                           xref,&
                           yref,&
                           zref,&
                           Correlation_Coefficient,&
                           SkewCorr1D_X,&
                           SkewCorr1D_K,&
                           Geminals,&
                           FullGeminals,&
                           corr1restr,&
                           xini1,&
                           xfin1,&
                           xpts1,&
                           corr1restrmom,&
                           kxini1,&
                           kxfin1,&
                           kpts1,&
                           corr2restr,&
                           xini2,&
                           xfin2,&
                           xpts2,&
                           corr2restrmom,&
                           kxini2,&
                           kxfin2,&
                           kpts2, &
                           Variance_observables

       REAL*8,       public :: x1slice= 0.d0 !< Specify cut through 2D correlation functions.
       REAL*8,       public :: y1slice= 0.d0!< Specify cut through 2D correlation functions.
       REAL*8,       public :: x2slice= 0.d0!< Specify cut through 2D correlation functions.
       REAL*8,       public :: y2slice= 0.d0 !< Specify cut through 2D correlation functions.       
       LOGICAL,      public :: x1const= .FALSE.!< Specify cut axis through 2D correlation functions.
       LOGICAL,      public :: y1const = .FALSE.!< Specify cut axis through 2D correlation functions.
       LOGICAL,      public :: x2const = .FALSE.!< Specify cut axis through 2D correlation functions.
       LOGICAL,      public :: y2const = .FALSE.!< Specify cut axis through 2D correlation functions.
       LOGICAL,      public :: REALSPACE2D= .FALSE. !< Toggle output of 2D correlation functions in real space.
       LOGICAL,      public :: MOMSPACE2D  = .FALSE. !< Toggle output of 2D correlation function in k-space.
       LOGICAL,      public :: REALSKEW2D  = .FALSE. !< Toggle output of 2D skew diagonal correlation function.
       LOGICAL,      public :: MOMSKEW2D  = .FALSE. !< Toggle output of 2D skew diagonal correlation function in k-space.
       LOGICAL,      public :: ZeroPadding2D  = .FALSE. !< Zero-Padding for the output of 2D-k-space-quantities (increases resolution).
       INTEGER,      public :: Dilation2D     = 1 !< By which factor to increase the resolution of 2D-k-space.
       LOGICAL,      public :: PROJ_X = .FALSE. !< Calculate an effective potential from a 2D-density.
       Character(len=1) , public :: DIR='X' !< Specify coordinate which is traced for the effective potential.
       LOGICAL,      public :: L_Z = .FALSE. !< Toggle calculation of the matrix elements of the angular momentum in z direction.

       NAMELIST /TWO_D/MOMSPACE2D,&
                       REALSPACE2D,&
                       x1const,&
                       x1slice,&
                       y1const,&
                       y1slice,&
                       x2const,&
                       x2slice,&
                       y2const,&
                       y2slice,&
                       REALSKEW2D,&
                       MOMSKEW2D,& 
                       ZeroPadding2D,&
                       Dilation2D,&
                       PROJ_X,&
                       DIR,&
                       L_Z

       LOGICAL,      public :: Entropy = .FALSE. !< Toggle the output of different entropy measures.
       LOGICAL,      public :: NBody_C_Entropy = .FALSE. !< Toggle the Computation of different full N-body entropy.
       LOGICAL,      public :: TwoBody_Entropy = .FALSE. !< Toggle output of shannon entropy of two-body (momentum and real-space) density. 
       LOGICAL,      public :: SingleShot_Analysis = .FALSE. !< Toggle the output of random deviates of the N-body density matrix.
       LOGICAL,      public :: SingleShot_FTAnalysis = .FALSE. !< Toggle the output of random deviates of the N-body momentum density matrix.
       INTEGER,      public :: NShots = 10 !< Number of single shots (random deviates of the N-body density) to compute.
       LOGICAL,      public :: SingleShot_Convolution = .TRUE. !< Convolute single shots with point-spread function?
       REAL*8,       public :: Convolution_Width = 3 !< Width of point spread function to convolute the single shots with in gridpoints.
       LOGICAL,      public :: CentreOfMass = .FALSE. !< Toggle sampling of centre-of-mass operator
       LOGICAL,      public :: CentreOfMomentum = .FALSE. !< Toggle sampling of centre-of-momentum operator
       INTEGER,      public :: NSamples_COM = 10000 !< Number of samples to make from the centre-of-mass operator  
       LOGICAL,      public :: ShotVariance = .FALSE. !< Compute the shot-to-shot variance for NSamples shots
       INTEGER,      public :: NSamples_VAR = 10000 !< Number of samples to take to compute the variance from 
       LOGICAL,      public :: Binning = .FALSE.         !< Compute Even-Odd imbalance histogram from a set of single-shots
       REAL*8 ,      public :: Binning_Period = 3.1415926536d0   !< Periodicity of Even-Odd imbalance sampling
       INTEGER,      public :: Binning_Samples = 10000  !< Number of samples to use for the Even-Odd imbalance sampling
       REAL*8,       public :: Binning_Offset = 0.d0    !< Offset for bins to form the binning histogram with respect to the beginning of the grid
       LOGICAL,      public :: BinSplit =.FALSE.    !< Distribute counts at the edge of the bins 50/50 or not 
       LOGICAL,      public :: lossops= .FALSE. !< Calculate expectation values of Loss-Operators (!!! Currently only for N=2 in 1D !!!)
       REAL*8 ,      public :: border =0.d0 !< Where to split the Hilbert space in the calculation of the exp. val. of the loss operators.
       LOGICAL,      public :: loc_frag= .FALSE. !< Calculate local fragmentation (!!! experimental !!!).
       REAL*8 ,      public :: border_lf =0.d0 !< Where the Hilbert space is split for the local fragmentation analysis.
       LOGICAL,      public :: autocorr = .FALSE. !< Calculate autocorrelation function (!!! experimental !!!)
       REAL*8 ,      public :: tautocorr=0.d0 !< Reference time for the autocorrelation function.
       LOGICAL,      public :: NO_Coeffs = .FALSE. !< Calculate coefficients in the natural orbital basis 
       LOGICAL,      public :: anyordercorrelations_X = .FALSE. !< toggle output of higher order correlations rho(c_ref_1,...,c_ref_order-3,r_order-1,r_order-2)  
       LOGICAL,      public :: anyordercorrelations_K = .FALSE. !< toggle output of higher order momentum correlations rho(c_ref_1,...,c_ref_order-3,r_order-1,r_order-2) 
       INTEGER,      public :: order = 10 !< order up to which correlation functions are output
       LOGICAL,      public :: oneD = .TRUE. !< toggle computation of higher order correlation functions as a function of a single variable     
       LOGICAL,      public :: twoD = .FALSE. !< toggle computation of higher order correlation functions as a function of two variables
       REAL*8,       public :: c_ref_x(20) = 0.d0 !< x-value of reference points of correlation functions 
       REAL*8,       public :: c_ref_y(20) = 0.d0 !< y-value of reference points of correlation functions 
       REAL*8,       public :: c_ref_z(20) = 0.d0 !< z-value of reference points of correlation functions
       LOGICAL,      public :: anyorderSKEWcorrelations_X = .FALSE. !< toggle output of higher order correlations rho(c_ref_1,...,c_ref_order-3,r_order-1,r_order-2)  
       LOGICAL,      public :: anyorderSKEWcorrelations_K = .FALSE. !< toggle output of higher order momentum correlations rho(c_ref_1,...,c_ref_order-3,r_order-1,r_order-2) 
       INTEGER,      public :: SkewOrder = 10 !< order up to which correlation functions are output
       LOGICAL,      public :: SkewOneD = .TRUE. !< toggle computation of higher order correlation functions as a function of a single variable     
       LOGICAL,      public :: SkewTwoD = .FALSE. !< toggle computation of higher order correlation functions as a function of two variables
       REAL*8,       public :: cskew_ref_x(20) = 0.d0 !< x-value of reference points of correlation functions 
       REAL*8,       public :: cskew_ref_y(20) = 0.d0 !< y-value of reference points of correlation functions 
       REAL*8,       public :: cskew_ref_z(20) = 0.d0 !< z-value of reference points of correlation functions
       LOGICAL,      public :: fidelity   = .FALSE. !< Calculate autocorrelation or fidelity function
       LOGICAL,      public :: single_ref = .FALSE. !< False: fidelity, i.e., |<Psi1(t)|Psi2(t)>|**2, TRUE: autocorrelation |<Psi(0)|Psi(t)>|**2
       LOGICAL,      public :: overlap_projection = .TRUE. !< True: calculates the projection of the overlap onto the configurations
       REAL*8 ,      public :: t_ref      = 0.d0 !< Reference time for the autocorrelation function.
       CHARACTER*80,  public :: CI_Reference = 'CIc_bin' !< filename for the coefficients of the reference wavefunction for autocorrelation
       CHARACTER*80,  public :: PSI_Reference = 'PSI_bin' !< filename for the orbitals of the reference wavefunction for autocorrelation
       CHARACTER*80,  public :: Header_Reference = 'Header' !< filename for the header of the reference wavefunction for autocorrelation
       LOGICAL,      public :: Autocorrelation = .False.
       REAL*8,       public :: Time_start = 0.d0
       REAL*8,       public :: Time_end = 0.d0
       LOGICAL,      public :: FDF_X = .FALSE. !< Compute histogram of counts in position space from a number of single shots given by FDF_Samples 
       LOGICAL,      public :: FDF_K = .FALSE. !< Compute histogram of counts in momentum space from a number of single shots given by FDF_Samples
       LOGICAL,      public :: Reduced_WF_X = .FALSE. !< Calculate reduced wave functio
       LOGICAL,      public :: Reduced_WF_K = .FALSE. !< Calculate reduced wave functio
       REAL*8 ,      public :: t_detect     = 0.d0 !< time to detect the NDetect atoms 
       INTEGER,      public :: NDetect      = 0.d0 !< Number of atoms to detect
       LOGICAL,      public :: Fixed_position = .FALSE. !< FALSE -> Partial detection using random deviates of the N-body density matrix (single shot)
                                                        !< TRUE  -> Position of the detected atoms fixed using c_ref_x, c_ref_y, c_ref_z

       LOGICAL,      public :: get_LR !< Toggle LR-MCTDHB analysis.
       INTEGER,      public :: LR_maxsil !< Maximum number of Krylov basis vectors for LR-MCTDHB analysis.

       NAMELIST /MANY_BODY/&
                           Entropy,&
                           NBody_C_Entropy,&
                           TwoBody_Entropy,&
                           SingleShot_Analysis,&
                           SingleShot_FTAnalysis,&
                           NShots,&
                           SingleShot_Convolution,&
                           Convolution_Width,&
                           CentreOfMass,&
                           CentreOfMomentum,&
                           NSamples_COM,&
                           ShotVariance,&
                           NSamples_VAR,&
                           Binning,&
                           Binning_Period,&
                           Binning_Samples,&
                           Binning_Offset,&
                           BinSplit,&
                           lossops,&
                           border,&
                           loc_frag,&
                           border_lf,&
                           autocorr,&
                           tautocorr,&
                           NO_Coeffs,&
                           anyordercorrelations_X,&
                           anyordercorrelations_K,&
                           order,&
                           oneD,&
                           twoD,&
                           c_ref_x,&
                           c_ref_y,&
                           c_ref_z,&  
                           anyorderSKEWcorrelations_X,&
                           anyorderSKEWcorrelations_K,&
                           SkewOrder,&
                           SkewOneD,& 
                           SkewTwoD,& 
                           cskew_ref_x,&
                           cskew_ref_y,&
                           cskew_ref_z,&
                           fidelity,&
                           single_ref,&   
                           overlap_projection,&   
                           t_ref,&
                           CI_Reference,&
                           PSI_Reference,&
                           Header_Reference,&
                           Autocorrelation,&
                           Time_start,&
                           Time_end,&
                           FDF_X,& 
                           FDF_K,&  
                           Reduced_WF_X,&
                           Reduced_WF_K,&
                           t_detect,&
                           NDetect,&
                           Fixed_position 
  
       END MODULE  Analysis_Input_Variables
