!>GPLv3
!    MCTDH-X: the multiconfigurational time-dependent Hartree for 
!    indistinguishable particles software
!
!    Copyright (C) 2021  A. U. J. Lode, M. C. Tsatsos, 
!                        E. Fasshauer, S. E. Weiner, 
!                        R. Lin, L. Papariello, P. Molignini, 
!                        C. Lévêque, M. Büttner , J. Xiang, S. Dutta
!
!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <https://www.gnu.org/licenses/>.
!
!
!
!
!<GPLv3










!> This module contains all routines using FFT to calculate colocations, k-space representations, derivatives or gradients. 
MODULE Auxiliary_FFTRoutines

use Analysis_Input_Variables
USE Interaction_Parameters
use Global_Parameters
USE Matrix_Elements
USE Function_Library
USE Auxiliary_Analysis_Routines
USE Auxiliary_Routines

IMPLICIT NONE
CONTAINS



subroutine Get_Sorted_FFT(PSI,FTPSI,Dilation) 
use Global_Parameters, ONLY : DIM_MCTDH,NDVR_X,NDVR_Y,NDVR_Z,Morb
use DVR_Parameters
use Interaction_Parameters
USE FFT_Laboratory


  implicit none
  integer                  :: Iorb,m,K

  complex*16,DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb),intent(in)  :: PSI 
  complex*16,DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb),intent(out) :: FTPSI
  INTEGER, Optional :: Dilation
  complex*16,allocatable :: ORBTMP(:)
 
  real*8                        :: norm

  NORM=1.d0
     
!##########################################################  
!##########################################################  
!     1D -CASE
    if (DIM_MCTDH.eq.1) then
!##########################################################  
!##########################################################  
   IF ((Present(Dilation).eqv..FALSE.).or.(Dilation.eq.1)) THEN

      ALLOCATE(ORBTMP(NDVR_X*NDVR_Y*NDVR_Z))

      FTPSI=PSI
         
!     do the fft voodoo
      
      do Iorb=1,Morb
            ORBTMP=(FTPSI(:,IORB))

            CALL Get_FFT(1,ORBTMP,0,1,MYID=0)

            if (mod(NDVR_X,2).eq.0) then

              do m=(NDVR_X/2+1),NDVR_X
                 FTPSI(m-(NDVR_X/2),IORB)=ORBTMP(m)
                 FTPSI(m,IORB)=ORBTMP(m-(NDVR_X/2))
              enddo

            else

              do m=(NDVR_X/2+1),NDVR_X
                 FTPSI(m-(NDVR_X/2),IORB)=ORBTMP(m)
                 FTPSI(m,IORB)=ORBTMP(m-(NDVR_X/2))
              enddo

            endif
      
        call normvxz(FTPSI(:,Iorb),norm,NDVR_X)
        call xvixdzo(norm,FTPSI(:,Iorb),NDVR_X)
      
      end do
   ELSE IF ((Present(Dilation).eqv..TRUE.).and.(Dilation.gt.1)) THEN
      ALLOCATE(ORBTMP(NDVR_X*NDVR_Y*NDVR_Z*Dilation))
      FTPSI=PSI
      do Iorb=1,Morb
            ORBTMP=dcmplx(0.d0,0.d0)
            ORBTMP((DILATION-1)*NDVR_X/2+1:(DILATION+1)*(NDVR_X)/2)=FTPSI(:,IORB)
            CALL Get_Dilated_FFT(1,ORBTMP,0,1,Dilation)

            if (mod(NDVR_X,2).eq.0) then
              do m=(NDVR_X/2+1),NDVR_X
                 FTPSI(m-(NDVR_X/2),IORB)=ORBTMP(NDVR_X*Dilation-NDVR_X+m)
                 FTPSI(m,IORB)=ORBTMP(m-(NDVR_X/2))
              enddo
            else
              do m=1,NDVR_X/2
                 FTPSI(m,IORB)=ORBTMP(NDVR_X*Dilation-NDVR_X/2+m)
              end do
              do m=1,NDVR_X/2+1
                 FTPSI(m+NDVR_X/2,IORB)=ORBTMP(m)
              enddo
            endif
      
        call normvxz(FTPSI(:,Iorb),norm,NDVR_X)
        call xvixdzo(norm,FTPSI(:,Iorb),NDVR_X)
      
      end do
   ENDIF
  
!##########################################################  
!##########################################################  
!     1D -CASE
    endif 
!##########################################################  
!##########################################################  

!##########################################################  
!##########################################################  
!     2D -CASE
    if (DIM_MCTDH.eq.2) then
!##########################################################  
!##########################################################  
     ALLOCATE(ORBTMP(NDVR_X*NDVR_Y*NDVR_Z))
     FTPSI=PSI

     IF (((Present(Dilation).eqv..FALSE.).or.(Dilation.eq.1)).and.  &
                                   (MPI_ORBS_MEMORY.eqv..FALSE.)) THEN
        do Iorb=1,Morb
   
            ORBTMP=(FTPSI(:,IORB))
            CALL Get_FFT(2,OrbTMP,0,666,MYID=0,Transposed=.FALSE.)
       
            call normvxz(ORBTMP,norm,NDVR_X*NDVR_Y)
            call xvixdzo(norm,ORBTMP,NDVR_X*NDVR_Y)
            
            call sort_FFT_to_ascending_2d(ORBTMP,NDVR_X,NDVR_Y)
   
            FTPSI(:,IORB)=ORBTMP
        end do

     ELSE IF (((Present(Dilation).eqv..TRUE.).and.(Dilation.gt.1))  &
                             .or.(MPI_ORBS_MEMORY.eqv..TRUE.)) THEN

         CALL twoD_zero_padding(PSI,FTPSI,dilation)
      
     ENDIF
!##########################################################  
!##########################################################  
!     2D -CASE
    endif
!##########################################################  
!##########################################################  

!##########################################################  
!##########################################################  
!     3D -CASE
    if (DIM_MCTDH.eq.3) then
!##########################################################  
!##########################################################  
     ALLOCATE(ORBTMP(NDVR_X*NDVR_Y*NDVR_Z))
     FTPSI=PSI


     IF ((Present(Dilation).eqv..FALSE.).or.(Dilation.eq.1)) THEN

        do Iorb=1,Morb
   
            ORBTMP=(FTPSI(:,IORB))
            IF (MPIFFT_Binary.eqv..FALSE.) THEN
              CALL Get_FFT(3,OrbTMP,0,666,MYID=0)
            ELSE
              CALL Get_FFT(3,OrbTMP,0,666,MYID=0,Transposed=.FALSE.)
            ENDIF
       
            call normvxz(ORBTMP,norm,NDVR_X*NDVR_Y*NDVR_Z)
            call xvixdzo(norm,ORBTMP,NDVR_X*NDVR_Y*NDVR_Z)
            
            call sort_FFT_to_ascending_3d(ORBTMP,NDVR_X,NDVR_Y,NDVR_Z)
   
            FTPSI(:,IORB)=ORBTMP
        end do

     ELSE IF ((Present(Dilation).eqv..TRUE.).and.(Dilation.gt.1)) THEN

         CALL threeD_zero_padding(PSI,FTPSI,dilation)
      
     ENDIF
!##########################################################  
!##########################################################  
!     3D -CASE
    endif
!##########################################################  
!##########################################################  

DEALLOCATE(ORBTMP)
end subroutine Get_Sorted_FFT

subroutine sort_FFT_to_ascending_3d(FFT,NDVR_X,NDVR_Y,NDVR_Z)
!> sort an array representing a 2D function given in FFT order, compact storage 
!> to be in strictly ascending order (needed for gnuplots pm3d), compact storage

IMPLICIT NONE
integer*8 NDVR_X,NDVR_Y,NDVR_Z,i,j,k
COMPLEX*16 FFT(NDVR_X*NDVR_Y*NDVR_Z),ASC(NDVR_X*NDVR_Y*NDVR_Z)

         ASC=FFT
 
! copy the first half of each vector in X direction to the corresponding last half

   IF (MOD(NDVR_X,2).eq.0) THEN
     Write(6,*) "inverting X"
      DO j=1,NDVR_Z
         do k=1,NDVR_Y
            do i=1,NDVR_X
             if (i.le.((NDVR_X)/2)) then
                FFT((j-1)*NDVR_X*NDVR_Y+(k-1)*NDVR_X+i)=ASC((j-1)*NDVR_X*NDVR_Y+(k-1)*NDVR_X+i+(NDVR_X)/2)
             else if (i.gt.((NDVR_X)/2)) then
                FFT((j-1)*NDVR_X*NDVR_Y+(k-1)*NDVR_X+i)=ASC((j-1)*NDVR_X*NDVR_Y+(k-1)*NDVR_X+i-NDVR_X/2)
             endif
            end do
         end do 
      end do 
   ENDIF

         ASC=FFT

! copy the first half of each vector in X direction to the corresponding last half
   IF (MOD(NDVR_X,2).ne.0) then
      STOP "Uneven number of grid points not implemented for 3d ffts"
   ENDIF

   IF (MOD(NDVR_Y,2).eq.0) then
! copy y direction from 0 -> kmax to last half

     Write(6,*) "inverting Y"
      DO j=1,NDVR_Z
         do k=1,NDVR_Y
            do i=1,NDVR_X
              if (k.le.((NDVR_Y)/2)) then
                FFT((j-1)*NDVR_X*NDVR_Y+(k-1)*NDVR_X+i)=ASC((j-1)*NDVR_X*NDVR_Y+(k+NDVR_Y/2-1)*NDVR_X+i)
             else if (k.gt.((NDVR_Y)/2)) then
                FFT((j-1)*NDVR_X*NDVR_Y+(k-1)*NDVR_X+i)=ASC((j-1)*NDVR_X*NDVR_Y+(k-NDVR_Y/2-1)*NDVR_X+i)
!                if ((i.eq.1).and.(j.eq.1)) then
!                   write(6,*) "reassigning",(j-1)*NDVR_X*NDVR_Y+(k-NDVR_Y/2-1)*NDVR_X+i, "to",(j-1)*NDVR_X*NDVR_Y+(k-1)*NDVR_X+i
!                endif
             endif
            end do
         end do 
      end do 
   ENDIF

   IF (MOD(NDVR_Y,2).ne.0) then
      STOP "Uneven number of grid points not implemented for 3d ffts"
! copy y direction from 0 -> kmax to last half
   ENDIF

         ASC=FFT
   IF (MOD(NDVR_Z,2).eq.0) then
! copy z direction from 0 -> kmax to last half
     Write(6,*) "inverting Z"
      DO j=1,NDVR_Z
         do k=1,NDVR_Y
            do i=1,NDVR_X
              if (j.le.((NDVR_Z)/2)) then
                FFT((j-1)*NDVR_X*NDVR_Y+(k-1)*NDVR_X+i)=ASC((j-1+NDVR_Z/2)*NDVR_X*NDVR_Y+(k-1)*NDVR_X+i)
!                if ((i.eq.1).and.(k.eq.1)) then
!                   write(6,*) "reassigning",(j-1+NDVR_Z/2)*NDVR_X*NDVR_Y+(k-1)*NDVR_X+i, "to",(j-1)*NDVR_X*NDVR_Y+(k-1)*NDVR_X+i 
!                endif

             else if (j.gt.((NDVR_Z)/2)) then
               FFT((j-1)*NDVR_X*NDVR_Y+(k-1)*NDVR_X+i)=ASC((j-NDVR_Z/2-1)*NDVR_X*NDVR_Y+(k-1)*NDVR_X+i)
!                if ((i.eq.1).and.(k.eq.1)) then
!                   write(6,*) "reassigning",(j-NDVR_Z/2-1)*NDVR_X*NDVR_Y+(k-1)*NDVR_X+i, "to",(j-1)*NDVR_X*NDVR_Y+(k-1)*NDVR_X+i 
!                endif
             endif
            end do
         end do 
      end do 

   ENDIF

   IF (MOD(NDVR_Z,2).ne.0) then
      STOP "Uneven number of grid points not implemented for 3d ffts"
   ENDIF
!   FFT=ASC
end subroutine sort_FFT_to_ascending_3d



subroutine sort_FFT_to_ascending_2d(FFT,NDVR_X,NDVR_Y)
!> sort an array representing a 2D function given in FFT order, compact storage 
!> to be in strictly ascending order (needed for gnuplots pm3d), compact storage

IMPLICIT NONE
integer*8 NDVR_X,NDVR_Y,i,k
COMPLEX*16 FFT(NDVR_X*NDVR_Y),ASC(NDVR_X*NDVR_Y)

         ASC=FFT
 
! copy the first half of each vector in X direction to the corresponding last half

   IF (MOD(NDVR_X,2).eq.0) THEN
         do k=1,NDVR_Y
            do i=1,NDVR_X
             if (i.le.((NDVR_X)/2)) then
                FFT((k-1)*NDVR_X+i)=ASC((k-1)*NDVR_X+i+(NDVR_X)/2)
             else if (i.gt.((NDVR_X)/2)) then
                FFT((k-1)*NDVR_X+i)=ASC((k-1)*NDVR_X+i-((NDVR_X)/2))
             endif
            end do
         end do 
   ENDIF


! copy the first half of each vector in X direction to the corresponding last half
   IF (MOD(NDVR_X,2).ne.0) then
         do k=1,NDVR_Y
            do i=1,NDVR_X
             if (i.le.((NDVR_X+1)/2-1)) then
                FFT((k-1)*NDVR_X+i)=ASC((k-1)*NDVR_X+i+(NDVR_X+1)/2)
             else if (i.gt.((NDVR_X+1)/2)-1) then
                FFT((k-1)*NDVR_X+i)=ASC((k-1)*NDVR_X+i-((NDVR_X+1)/2-1))
             endif
            end do
         end do 
   ENDIF

   IF (MOD(NDVR_Y,2).eq.0) then
! copy y direction from 0 -> kmax to last half

         do k=1,NDVR_X*NDVR_Y
            if (k.le.(NDVR_X*(NDVR_Y/2))) then
               ASC(k)=FFT(NDVR_X*(NDVR_Y/2)+k)
            elseif (k.gt.NDVR_X*((NDVR_Y)/2)) then
               ASC(k)=FFT(k-NDVR_X*(NDVR_Y/2))
            endif
         end do
   ENDIF
   IF (MOD(NDVR_Y,2).ne.0) then
! copy y direction from 0 -> kmax to last half
         do k=1,NDVR_X*NDVR_Y
            if (k.le.(NDVR_X*(NDVR_Y/2))) then
               ASC(k)=FFT(NDVR_X*(NDVR_Y/2+1)+k)
            elseif (k.gt.NDVR_X*(NDVR_Y/2)) then
               ASC(k)=FFT(k-NDVR_X*((NDVR_Y/2)))
            endif
         end do
   ENDIF
   FFT=ASC
end subroutine sort_FFT_to_ascending_2d

!> This subroutine calculates the derivative of a real field in x/y direction if the Direction=1/2
subroutine Get_Derivative_2D(Field,Derivative,Direction)
USE FFT_Laboratory
use Global_Parameters
USE DVR_Parameters
USE Interaction_Parameters

IMPLICIT NONE


Integer :: Direction
REAL*8,   DIMENSION(NDVR_X*NDVR_Y*NDVR_Z) :: Field
REAL*8,   DIMENSION(NDVR_X) :: grid_kx
REAL*8,   DIMENSION(NDVR_Y) :: grid_ky
REAL*8,   DIMENSION(NDVR_X*NDVR_Y*NDVR_Z) :: Derivative
COMPLEX*16,   DIMENSION(NDVR_X*NDVR_Y*NDVR_Z) :: Field_Complex,Field_Complex_Tmp
INTEGER*8 :: I,J,K,L,M,IND


IF (Direction.eq.1) THEN

   call get_mom(x_initial,x_final,NDVR_X,grid_kX)
   call get_mom(x_initial,x_final,NDVR_X,grid_ky)

!> Uncomment the loops in the routine in order to write the
!> field to fort.400, its FFT to fort.401, and its derivative to fort.402

!   DO M=1,NDVR_X*NDVR_Y   
!      
!      call get_ijk_from_m(m,NDVR_X,NDVR_Y,i,j,k)
!      Field(M)=exp(-ort_y(j)*ort_y(j)-ort_x(i)*ort_x(i))*ort_x(i)*ort_x(i)*ort_x(i)
!      write(400,'(3E25.16)') Ort_x(I),Ort_y(j),Field(M)
!      if (mod(m,NDVR_X).eq.0) write(400,*) " "
!
!   END DO

   Field_Complex=0.d0
   Field_Complex=Field

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!  Derivative in X-direction
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!   CALL Init_FFT(1,0,1)
   CALL Get_FFT(1,Field_Complex,0,1,MYID=0)
!   CALL Destroy_FFT(1,0,1)
   Field_Complex_TMP=Field_Complex
   DO J=1,NDVR_Y
         if (mod(NDVR_X,2).eq.0) then
           do m=(J-1)*NDVR_X+(NDVR_X/2+1),J*NDVR_X
              Field_Complex(m-(NDVR_X/2))=Field_Complex_TMP(m)
              Field_Complex(m)=           Field_Complex_TMP(m-(NDVR_X/2))
           enddo
         else
           do m=(J-1)*NDVR_X+(NDVR_X/2+1),J*NDVR_X
              Field_Complex(m-(NDVR_X/2))=Field_Complex_Tmp(m)
              Field_Complex(m)=           Field_Complex_Tmp(m-(NDVR_X/2))
           enddo
         endif
   END DO

!   DO M=1,NDVR_X*NDVR_Y   
!      
!      call get_ijk_from_m(m,NDVR_X,NDVR_Y,i,j,k)
!      write(401,'(4E25.16)') Ort_kx(I),Ort_ky(j),Field_Complex(M)*(-1.d0)**(i-1)
!      if (mod(m,NDVR_X).eq.0) write(401,*) " "
!
!   END DO

   Do M=1,NDVR_X*NDVR_Y*NDVR_Z
           call get_ijk_from_m(m,NDVR_X,NDVR_Y,i,j,k)

           Field_Complex(M)=Field_Complex(M)* &
                    dcmplx(0.d0,1.d0)*grid_kx(I)/NDVR_X
   END DO

!   CALL Init_FFT(1,1,1)
   CALL Get_FFT(1,Field_Complex,1,1,MYID=0)
!   CALL Destroy_FFT(1,1,1)

    DO M=1,NDVR_X*NDVR_Y   
      call get_ijk_from_m(m,NDVR_X,NDVR_Y,i,j,k)
      Field_Complex(M)=Field_Complex(M)*(-1.d0)**(i-1)
    end do


!    DO M=1,NDVR_X*NDVR_Y   
!      
!      call get_ijk_from_m(m,NDVR_X,NDVR_Y,i,j,k)
!      write(402,'(4E25.16)') Ort_x(I),Ort_y(j),Field_Complex(M)
!      if (mod(m,NDVR_X).eq.0) write(402,*) " "
!
!   END DO
!   stop
 

    Derivative=real(Field_Complex)
ELSE IF (Direction.eq.2) THEN

   call get_mom(y_initial,y_final,NDVR_y,grid_ky)
   call get_mom(x_initial,x_final,NDVR_x,grid_kx)


   Field_Complex=0.d0
   Field_Complex=Field

!   CALL Init_FFT(1,0,2)
   CALL Get_FFT(1,Field_Complex,0,2,MYID=0)
!   CALL Destroy_FFT(1,0,2)

   Field_Complex_TMP=Field_Complex

   IF (MOD(NDVR_Y,2).eq.0) then
! copy y direction from 0 -> kmax to last half

         do k=1,NDVR_X*NDVR_Y
            if (k.le.(NDVR_X*(NDVR_Y/2))) then
               Field_Complex(k)=Field_Complex_Tmp(NDVR_X*(NDVR_Y/2)+k)
            elseif (k.gt.NDVR_X*((NDVR_Y)/2)) then
               Field_Complex(k)=Field_Complex_Tmp(k-NDVR_X*(NDVR_Y/2))
            endif
         end do
   ENDIF
   IF (MOD(NDVR_Y,2).ne.0) then
! copy y direction from 0 -> kmax to last half
         do k=1,NDVR_X*NDVR_Y
            if (k.le.(NDVR_X*(NDVR_Y/2))) then
               Field_Complex(k)=Field_Complex_TMP(NDVR_X*(NDVR_Y/2+1)+k)
            elseif (k.gt.NDVR_X*(NDVR_Y/2)) then
               Field_Complex(k)=Field_Complex_TMP(k-NDVR_X*((NDVR_Y/2)))
            endif
         end do
   ENDIF

   Do M=1,NDVR_X*NDVR_Y*NDVR_Z
           call get_ijk_from_m(m,NDVR_X,NDVR_Y,i,j,k)

           Field_Complex(M)=Field_Complex(M)* &
                    dcmplx(0.d0,1.d0)*grid_ky(j)/NDVR_Y
   END DO

!   CALL Init_FFT(1,1,2)
   CALL Get_FFT(1,Field_Complex,1,2,MYID=0)
!   CALL Destroy_FFT(1,1,2)

    DO M=1,NDVR_X*NDVR_Y   
      call get_ijk_from_m(m,NDVR_X,NDVR_Y,i,j,k)
      Field_Complex(M)=Field_Complex(M)*(-1.d0)**(j-1)
    end do
 
   Derivative=Real(Field_Complex)

ELSE 
      Write(6,*) 'Derivative of Direction',Direction,'!?'
      stop
ENDIF
end subroutine Get_Derivative_2D


subroutine Get_Gradient_2D(Field,Gradient_X,Gradient_Y)
!> \todo use the appropriate real to complex and complex to real FFTs.

use Global_Parameters
USE DVR_Parameters
USE Interaction_Parameters

IMPLICIT NONE

REAL*8,   DIMENSION(NDVR_X*NDVR_Y*NDVR_Z) :: Field
REAL*8,   DIMENSION(NDVR_X*NDVR_Y*NDVR_Z) :: Gradient_X,Gradient_Y

CALL Get_Derivative_2D(Field,Gradient_X,1)

CALL Get_Derivative_2D(Field,Gradient_Y,2)

end subroutine Get_Gradient_2D

subroutine Get_Curl_2D(Field_X,Field_Y,Curl)
!> \todo use the appropriate real to complex and complex to real FFTs.
use Global_Parameters
USE DVR_Parameters
USE Interaction_Parameters

IMPLICIT NONE


REAL*8,   DIMENSION(NDVR_X*NDVR_Y*NDVR_Z) :: Curl,Curl_X,Curl_Y
REAL*8,   DIMENSION(NDVR_X*NDVR_Y*NDVR_Z) :: Field_X,Field_Y

INTEGER :: I

Curl=0.d0

CALL Get_Derivative_2D(Field_X,Curl_Y,2) ! Get d_y field_x

CALL Get_Derivative_2D(Field_Y,Curl_X,1) ! Get d_x field_y

Curl=Curl_X-Curl_Y

end subroutine Get_Curl_2D

subroutine twoD_zero_padding(PSI,FTPSI,dilation)
!> This routine calculates the 2D FT of an array of M Orbitals, increasing the resolution of 
!> the FT by adding zeroes to the end of the grid in realspace.

use Global_Parameters, ONLY : DIM_MCTDH
use DVR_Parameters
use Interaction_Parameters
USE FFT_Laboratory
integer, intent(in) :: dilation
complex*16,DIMENSION(NDVR_X*NDVR_Y,Morb),intent(in)  :: PSI
complex*16,DIMENSION(NDVR_X*NDVR_Y*dilation*dilation)    :: dilatedOrbital
complex*16,DIMENSION(NDVR_X,NDVR_Y)    :: orbtmp2D 
complex*16,DIMENSION(NDVR_X*dilation,NDVR_Y*dilation)    :: dilatedOrbital2D 
complex*16,DIMENSION(NDVR_X*NDVR_Y) :: ORBTMP 

complex*16,DIMENSION(NDVR_X*NDVR_Y,Morb),intent(OUT)  :: FTPSI
real*8   :: norm

Integer :: IORB

     if (DIM_MCTDH.ne.2) then
        write(6,*) "0-padding: Only for 2D so far!!!"
        return
     endif


     if (DIM_MCTDH.eq.2) then
!##########################################################  
!##########################################################  
     FTPSI=PSI

     do Iorb=1,Morb

         ORBTMP=(PSI(:,IORB))
         ORBTMP2D=RESHAPE(ORBTMP,(/NDVR_X,NDVR_Y/))
         dilatedOrbital2D =dcmplx(0.d0,0.d0)

         dilatedOrbital2D((DILATION-1)*NDVR_X/2+1:(DILATION-1)*NDVR_X/2+NDVR_X,&
                          (DILATION-1)*NDVR_Y/2+1:(DILATION-1)*NDVR_Y/2+NDVR_Y)=ORBTMP2D
         dilatedOrbital=RESHAPE(dilatedOrbital2D,(/NDVR_X*NDVR_Y*Dilation*Dilation/))
        
         write(6,*) "TO DIL FFT", SIZE(dilatedOrbital)
         CALL Get_Dilated_FFT(2,dilatedOrbital,0,666,Dilation)
          
         call normvxz(dilatedOrbital,norm,NDVR_X*NDVR_Y*dilation*dilation)
         call xvixdzo(norm,dilatedOrbital,NDVR_X*NDVR_Y*dilation*dilation)
         
         call sort_FFT_to_ascending_2d(dilatedOrbital,dilation*NDVR_X,dilation*NDVR_Y)
         DilatedOrbital2D=RESHAPE(dilatedorbital,(/Dilation*NDVR_X,Dilation*NDVR_Y/))

         FTPSI(:,IORB)=RESHAPE(dilatedOrbital2D((DILATION-1)*NDVR_X/2+1:(DILATION-1)*NDVR_X/2+NDVR_X,&
                          (DILATION-1)*NDVR_Y/2+1:(DILATION-1)*NDVR_Y/2+NDVR_Y),(/NDVR_X*NDVR_Y/))

         call normvxz(FTPSI(:,IORB),norm,NDVR_X*NDVR_Y)
         call xvixdzo(norm,FTPSI(:,IORB),NDVR_X*NDVR_Y)

     end do
!##########################################################  
!##########################################################  
!     2D -CASE
    endif
end subroutine twoD_zero_padding

subroutine threeD_zero_padding(PSI,FTPSI,dilation)
!> This routine calculates the 3D FT of an array of M Orbitals, increasing the resolution of 
!> the FT by adding zeroes to the end of the grid in realspace.

use Global_Parameters, ONLY : DIM_MCTDH
use DVR_Parameters
use Interaction_Parameters
USE FFT_Laboratory

integer, intent(in) :: dilation

complex*16,DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb),intent(in)  :: PSI
complex*16,DIMENSION(NDVR_X*NDVR_Y*NDVR_Z*dilation*dilation*dilation)    :: dilatedOrbital
complex*16,DIMENSION(NDVR_X,NDVR_Y,NDVR_Z)    :: orbtmp3D 
complex*16,DIMENSION(NDVR_X*dilation,NDVR_Y*dilation,dilation*NDVR_Z)    :: dilatedOrbital3D 
complex*16,DIMENSION(NDVR_X*NDVR_Y*NDVR_Z) :: ORBTMP 

complex*16,DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb),intent(OUT)  :: FTPSI

real*8   :: norm

Integer :: IORB

     if (DIM_MCTDH.ne.3) then
        write(6,*) "Wrong routine...."
        return
     endif


     if (DIM_MCTDH.eq.3) then
!##########################################################  
!##########################################################  
     FTPSI=PSI

     do Iorb=1,Morb

         ORBTMP=(PSI(:,IORB))
         ORBTMP3D=RESHAPE(ORBTMP,(/NDVR_X,NDVR_Y,NDVR_Z/))
         dilatedOrbital3D =dcmplx(0.d0,0.d0)

         dilatedOrbital3D((DILATION-1)*NDVR_X/2+1:(DILATION-1)*NDVR_X/2+NDVR_X,&
                          (DILATION-1)*NDVR_Y/2+1:(DILATION-1)*NDVR_Y/2+NDVR_Y,&
                          (DILATION-1)*NDVR_Z/2+1:(DILATION-1)*NDVR_Z/2+NDVR_Z)=ORBTMP3D

         dilatedOrbital=RESHAPE(dilatedOrbital3D,(/NDVR_X*NDVR_Y*NDVR_Z*dilation*Dilation*Dilation/))
         write(6,*) "TO GET_DILATED_FFT", size(dilatedOrbital)
         CALL Get_Dilated_FFT(3,dilatedOrbital,0,666,Dilation)
 
         call normvxz(dilatedOrbital,norm,NDVR_X*NDVR_Y*NDVR_Z*dilation*dilation*dilation)
         call xvixdzo(norm,dilatedOrbital,NDVR_X*NDVR_Y*NDVR_Z*dilation*dilation*dilation)
         
         call sort_FFT_to_ascending_3d(dilatedOrbital,dilation*NDVR_X,dilation*NDVR_Y,dilation*NDVR_Z)
         DilatedOrbital3D=RESHAPE(dilatedorbital,(/Dilation*NDVR_X,Dilation*NDVR_Y,Dilation*NDVR_Z/))

         FTPSI(:,IORB)=RESHAPE(dilatedOrbital3D((DILATION-1)*NDVR_X/2+1:(DILATION-1)*NDVR_X/2+NDVR_X,&
                                                (DILATION-1)*NDVR_Y/2+1:(DILATION-1)*NDVR_Y/2+NDVR_Y,&
                                                (DILATION-1)*NDVR_Z/2+1:(DILATION-1)*NDVR_Z/2+NDVR_Z),(/NDVR_X*NDVR_Y*NDVR_Z/))

         call normvxz(FTPSI(:,IORB),norm,NDVR_X*NDVR_Y*NDVR_Z)
         call xvixdzo(norm,FTPSI(:,IORB),NDVR_X*NDVR_Y*NDVR_Z)

     end do
!##########################################################  
!##########################################################  
!     2D -CASE
    endif
end subroutine threeD_zero_padding

END MODULE Auxiliary_FFTRoutines
