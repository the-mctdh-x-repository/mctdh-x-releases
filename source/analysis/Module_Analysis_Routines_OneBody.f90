!>GPLv3
!    MCTDH-X: the multiconfigurational time-dependent Hartree for 
!    indistinguishable particles software
!
!    Copyright (C) 2021  A. U. J. Lode, M. C. Tsatsos, 
!                        E. Fasshauer, S. E. Weiner, 
!                        R. Lin, L. Papariello, P. Molignini, 
!                        C. Lévêque, M. Büttner , J. Xiang, S. Dutta
!
!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <https://www.gnu.org/licenses/>.
!
!
!
!
!<GPLv3










!> This module contains the analysis routines controlled by the ONE_body namelist 
!> in the input. They compute one-body quantities.

MODULE Analysis_Routines_OneBody

use Analysis_Input_Variables
USE Interaction_Parameters
USE Matrix_Elements
USE Function_Library
USE Auxiliary_Routines
USE Auxiliary_Analysis_Routines
USE Auxiliary_FFTRoutines
USE Orbital_Allocatables, ONLY: VTRAP_EXT


IMPLICIT NONE
CONTAINS


!> Compute cavity order parameter Theta=<Psi|phi_c phi_p|Psi> and bunching parameter B=<Psi|phi^2_c|Psi>

subroutine Get_CavityOrderParameter(time,PSI,rho_jk) 

USE CavityBEC
USE DVR_Parameters
USE Global_Parameters, ONLY:NDVR_X,NDVR_Y,NDVR_Z,Cavity_K0,X_Cavity_Pump_Waist,Cavity_Mode_Waist,NCavity_Modes,Morb
use Coefficients_Parameters,ONLY:Npar

IMPLICIT NONE

COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb)  :: PSI
complex*16,DIMENSION(Morb,Morb),intent(in) :: rho_jk
real*8 :: time,Current_Cavity_PumpRate(NCavity_Modes)

Complex*16 :: B(Ncavity_Modes), Theta(Ncavity_Modes)

COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,NCavity_Modes):: Pump_Profile,Cavity_Profile

INTEGER*8 :: I,J,M,L,K

CALL Get_Current_Cavity_PumpRate(Current_Cavity_PumpRate,Time)

B=dcmplx(0.d0,0.d0)
Theta=dcmplx(0.d0,0.d0)

IF (DIM_MCTDH.eq.2) THEN
 DO L=1,NCavity_Modes
   DO M=1,NDVR_X*NDVR_Y*NDVR_Z
      call get_ijk_from_m(M,NDVR_X,NDVR_Y,i,j,k)

      Pump_Profile(M,L)=cos((Cavity_K0(L))*Ort_Y(J))&
                      *exp(-(Ort_X(I)/X_Cavity_Pump_Waist(L))**2)

      Cavity_Profile(M,L)=cos((Cavity_K0(L))*Ort_X(I))&
                    *exp(-(Ort_Y(I)/Cavity_Mode_Waist(L))**2)

   END DO
 END DO

ELSE IF(DIM_MCTDH.eq.1) THEN

 DO L=1,NCavity_Modes
   DO M=1,NDVR_X
       Pump_Profile(M,L)=COS(Cavity_K0(L)*Ort_X(M))
       Cavity_Profile(M,L)=COS(Cavity_K0(L)*Ort_X(M))
   END DO
 END DO

ENDIF


DO I=1,Morb
   DO J=1,Morb
      DO L=1,NCavity_Modes
         DO M=1,NDVR_X*NDVR_Y*NDVR_Z

               B(L)=B(L)+rho_jk(I,J)*dconjg(PSI(M,I))*Cavity_Profile(M,L)**2*PSI(M,J)/Npar
           Theta(L)=Theta(L)+rho_jk(I,J)*dconjg(PSI(M,I))*Cavity_Profile(M,L)*Pump_Profile(M,L)*PSI(M,J)/Npar
           
         END DO
      END DO
   END DO
END DO

IF (Time-Time_From.lt.1.d-10) THEN
   open(unit=985,file="CavityOrder.dat",form='formatted',STATUS='REPLACE',ACCESS='APPEND')
ELSE
   open(unit=985,file='CavityOrder.dat',form='formatted',ACCESS='APPEND')
ENDIF

Write(985,'(F21.16,99(E25.16))') Time,(dble(Theta(L)),L=1,NCavity_Modes),(dble(B(L)),L=1,NCavity_Modes)

close(985)

end subroutine Get_CavityOrderParameter



!> Compute cavity order parameter in a lattice Theta=<Psi|cos k_op x|Psi>

subroutine Get_CavityLatticeOrderParameter(time,PSI,rho_jk,k_op,phi_op) 

USE CavityBEC
USE DVR_Parameters
USE Global_Parameters, ONLY:NDVR_X,NDVR_Y,NDVR_Z,Cavity_K0,X_Cavity_Pump_Waist,Cavity_Mode_Waist,NCavity_Modes,Morb,parameter3,parameter4
use Coefficients_Parameters,ONLY:Npar

IMPLICIT NONE

COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb)  :: PSI
complex*16,DIMENSION(Morb,Morb),intent(in) :: rho_jk
real*8 :: time
real*8 :: k_op,phi_op
Complex*16 ::Theta,check
Complex*16, Dimension(NDVR_X*NDVR_Y*NDVR_Z):: Lattice_Profile

INTEGER*8 :: I,J,M,L,K

Theta=dcmplx(0.d0,0.d0)

IF (k_op.eq.-2.d0) THEN
  k_op=Parameter3
  write(*,*) "k_op=-2.d0 means it takes the lattice wavelength parameter3"
END IF

IF (phi_op.eq.-2.d0) then
   phi_op=parameter4
  write(*,*) "phi_op=-2.d0 means it takes the lattice phase shift parameter4"
END IF


IF (DIM_MCTDH.eq.2) THEN
  DO M=1,NDVR_X*NDVR_Y*NDVR_Z
      call get_ijk_from_m(M,NDVR_X,NDVR_Y,i,j,k)
      Lattice_Profile(M)=cos(k_op*(Ort_X(I)+Ort_Y(J))+phi_op) 
  END DO

ELSE IF(DIM_MCTDH.eq.1) THEN

  DO M=1,NDVR_X
       Lattice_Profile(M)=COS(k_op*Ort_X(M))
  END DO

ENDIF


DO I=1,Morb
   DO J=1,Morb
         DO M=1,NDVR_X*NDVR_Y*NDVR_Z

           Theta=Theta+rho_jk(I,J)*dconjg(PSI(M,I))*Lattice_Profile(M)*PSI(M,J)/Npar

         END DO
   END DO
END DO


IF (Time-Time_From.lt.1.d-10) THEN
   open(unit=985,file="CavityLatticeOrder.dat",form='formatted',STATUS='REPLACE',ACCESS='APPEND')
ELSE
   open(unit=985,file='CavityLatticeOrder.dat',form='formatted',ACCESS='APPEND')
ENDIF

Write(985,'(F21.16,99(E25.16))') Time,dble(Theta)

close(985)

end subroutine Get_CavityLatticeOrderParameter


subroutine Get_Variance(time,PSI,mode,rho_jk,rho_ijkl,Dilation) 
use Coefficients_Parameters,ONLY:Npar
use DVR_Parameters
use Auxiliary_Analysis_Routines
  implicit none
  integer  :: NOGRIDPOINTS
  integer,intent(in) :: mode,Dilation
  real*8,intent(in)  :: time
  real*8     :: w,dk,mom_X(NDVR_X),mom_Y(NDVR_Y),mom_Z(NDVR_Z)
  COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb)  :: PSI
  complex*16,intent(in) :: rho_ijkl(Morb,Morb,Morb,Morb)
  complex*16,intent(in) :: rho_jk(Morb,Morb)
  character(len=2)   :: aString
  character(len=200) :: fullName
  integer*8            :: m,n,i_m,j_m,k_m,i_n,j_n,k_n
  INTEGER            :: Iorb,Jorb,Korb,Lorb
  complex*16         :: G1m_m,G1m_n,G1n_n,G2m_n,G1n_n1st,G1n_n2nd,G2r1r2,tau
  real*8     :: outG1m_m,outReG1m_n,outImG1m_n,outG1n_n,outG2m_n,variance1



  NOGRIDPOINTS=NDVR_X*NDVR_Y*NDVR_Z


  Call Get_WeightOrMomentumGrid(mom_x,mom_y,mom_z,w,dilation,mode)
  

  if (mode.eq.1) then
     aString = 'x-'
  else if (mode.eq.2) then 
     aString = 'k-'
  else 
     write(*,*)"ERROR in get_correlations"
     stop
  endif 

  call Assemble_OutputFileName(FullName,Time,Npar,Morb,aString,'variance.dat')

  open(unit=12,file=trim(fullName),form='formatted')


  do n=1,NOGRIDPOINTS

!compute G1n_n
     G1n_n = (0.d0,0.d0)
     do Jorb = 1,Morb  
        do Korb=1,Morb

           G1n_n = G1n_n + rho_jk(Jorb,Korb) * DCONJG(PSI(n,Jorb))*PSI(n,Korb)

        end do
     end do
     G1n_n=G1n_n*1.d0/w**2 
     outG1n_n=DBLE(G1n_n)
!---------------

!compute G2m_n 
        G2m_n=(0.d0,0.d0)
        do Lorb=1,Morb
           do Korb=1,Morb
              do Jorb=1,Morb
                 do Iorb=1,Morb
                    G2m_n = G2m_n + rho_ijkl(Iorb,Jorb,Korb,Lorb)* &
                              DCONJG(PSI(n,Iorb))*DCONJG(PSI(n,Jorb))*PSI(n,Korb)*PSI(n,Lorb)
  
                 end do
              end do
           end do
        end do
        G2m_n=G2m_n*1.d0/(w*w*w*w) 
        outG2m_n=DBLE(G2m_n)

      variance1=outG2m_n+outG1n_n-outG1n_n**2

        call get_ijk_from_m(n,NDVR_X,NDVR_Y,i_n,j_n,k_n)

        write(12,6666) ort_X(i_n)," ",ort_Y(j_n)," ",ort_Z(k_n)," ",outG1n_n," ",outG2m_n," ",variance1

  end do         
6666  format(3(F8.3,a1),4(F16.9,a1))

close(12)

end subroutine Get_Variance


!---------------------------------------------------------------------------  
!> @brief
!> ## DESCRIPTION: 
!> This subroutine uses the total energy \f$E=\langle \Psi \vert H \vert \Psi \rangle\f$, 
!> to compute 
!>
!> * kinetic energy \f$E_{kin}=\langle \Psi \vert T \vert \Psi \rangle\f$, 
!> * potential energy \f$E_{pot}=\langle \Psi \vert V \vert \Psi \rangle\f$, and 
!> * interaction energy \f$E_{int}=\langle \Psi \vert W \vert \Psi \rangle\f$
!> for an MCTDH-X state \f$ \vert \Psi \rangle \f$.
!> ## Inputs
!> @param[in] time --> the time
!> @param[in] PSI  --> the orbitals
!> @param[in] rho_jk --> the matrix elements of the one-body density
!> @param[in] TotalEnergy --> the total energy
!> @return Energies.dat --> file with time, \f$E\f$, \f$E_{kin}\f$, \f$E_{pot}\f$, \f$E_W\f$
!---------------------------------------------------------------------------  
subroutine get_TotalEnergy(time,PSI,rho_jk,TotalEnergy) 

! imports
use Auxiliary_Analysis_Routines, ONLY: Get_KineticEnergyAction_AllOrbitalsAnalysis ! routine to evaluate \f$E_{kin}\f$
use Coefficients_Parameters,ONLY:NPar ! the number of particles
use Global_Parameters, ONLY: NDVR_X,NDVR_Y,NDVR_Z,Morb ! parameters of the DVR 

! no default variable declarations
implicit none

! ------------------ INPUTS ---------------------

COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb), INTENT(IN)  :: PSI !< array of orbitals 
complex*16,DIMENSION(Morb,Morb),intent(in) :: rho_jk     !< one-body density's matrix elements
real*8, INTENT(IN) :: time    !< time 
real*8, INTENT(IN) :: TotalEnergy !< total energy (this is read from binary, so we don't need the coefficients for E_{kin},E_{W},...)

!* ------------------ LOCAL VARIABLES ------------< /b>


COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb)  :: PSI_KE !< array for orbitals after the kinetic energy has been applied
complex*16,DIMENSION(Morb,Morb) :: VElements  !< array for the matrix elements of the potential
complex*16,DIMENSION(Morb,Morb) :: KEElements !< array for the matrix elements of the kinetic energy
REAL*8 :: VEnergy !< value of \f$E_{pot}\f$
REAL*8 :: KEnergy !< value of \f$E_{kin}\f$
REAL*8 :: WEnergy !< value of \f$E_{W}\f$
integer*8 :: n !< loop index 
INTEGER   :: JORB,KORB !< loop indices

! copy the input orbitals and apply the kinetic energy to them
PSI_KE=PSI
CALL Get_KineticEnergyAction_AllOrbitalsAnalysis(PSI_KE)

! initialize the matrix elements of potential/kinetic energy
KEElements=0.d0
VElements=0.d0

! evaluate kinetic energy and potential energy matrix elements
do n=1,NDVR_X*NDVR_Y*NDVR_Z
   do Jorb = 1,Morb  
      do Korb=1,Morb
         VElements(Jorb,Korb) = VElements(Jorb,Korb) + DCONJG(PSI(n,Jorb))*Vtrap_Ext(n)*PSI(n,Korb)
         KEElements(Jorb,Korb) = KEElements(Jorb,Korb) +DCONJG(PSI(n,Jorb))*PSI_KE(n,Korb)
      end do
   end do
end do

! initialize and evaluate kinetic and potential energy
Venergy = 0.d0
KEnergy = 0.d0
do Jorb = 1,Morb  
   do Korb=1,Morb
       Venergy = Venergy + rho_jk(jorb,korb)*VElements(jorb,korb)
       KEnergy = KEnergy + rho_jk(jorb,korb)*KEElements(jorb,korb)
   end do
end do

! get interaction energy from \f$E_{W}=E - E_{pot} - E_{kin} \f$
WEnergy=TotalEnergy-(Venergy+Kenergy)

! open the output file
! if the analysis task starts: create/replace the output file Energies.dat
! otherwise: append to the output file 
if (abs(time-Time_From).lt.1.d-9) THEN
   open(unit=12,file='Energies.dat',form='formatted',STATUS='REPLACE')
ELSE
   open(unit=12,file='Energies.dat',form='formatted',STATUS='OLD',ACCESS='APPEND')
ENDIF

! *write energies and time to file 
write(12,6677) time, TotalEnergy,KEnergy,VEnergy,WEnergy
6677  format(5(F16.9))

! *close the output file*
close(12)

end subroutine get_TotalEnergy

!> computes the density rho^1(x,x) if PSI is provided or 
!> or rho^1(k,k) if FT(PSI) is provided and
!> writes it to a file. 
!> time          : current time 
!> PSI           : orbital array 
!> rho_jk        : the 1-body density matrix elements where rho_jk(j,k)=rho_jk 
!> mode          : mode=1: G1(x,x) 
!>                 mode=2: G1(k,k) is computed
!> dilation      : the integer dilation gives the factor by which the momentum intervall should
!!                 be shrunk or equivalently the factor by which the sampling rate is increased.
!!                 The momentum distribution at the outer ends is padded with zeros then. 
!!                 Normally dilation is computed by get_dilation. If unsure put
!!                 dilation = 1, but the momentum grid will then usually have a bad
!!                 resolution. 
subroutine get_1BodyDiag(time,PSI,rho_jk,mode,dilation) 
use Coefficients_Parameters,ONLY:NPar
use DVR_Parameters
USE Interaction_Parameters

implicit none
integer  :: NOGRIDPOINTS
integer,intent(in) :: mode,dilation
real*8             :: time,w,dk,mom_X(NDVR_X),mom_Y(NDVR_Y),mom_Z(NDVR_Z)
COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb)  :: PSI
COMPLEX*16, ALLOCATABLE :: Psi_Nlevel(:,:,:)
complex*16,DIMENSION(Morb,Morb),intent(in) :: rho_jk
character(len=2)   :: aString
character(len=200) :: fullName
integer*8          :: n,i_n,j_n,k_n
INTEGER            :: JORB,KORB
complex*16         :: G1n_n
real*8, parameter  :: TOL=0.00001d0 

real*8             :: outG1n_n
real*8             :: xi,xf,yi,yf,zi,zf

Call Get_WeightOrMomentumGrid(mom_x,mom_y,mom_z,w,dilation,mode)


if (mode.eq.1) then
   aString = 'x-'
else if ((mode.eq.2).or.(mode.eq.3)) then 
   aString = 'k-'
else 
   write(*,*)"ERROR in get_1bodyDiag"
   stop
endif

call Assemble_OutputFileName(FullName,Time,NPar,Morb,aString,'density.dat')
open(unit=12,file=trim(fullName),form='formatted')

NOGRIDPOINTS=NDVR_X*NDVR_Y*NDVR_Z

do n=1,NOGRIDPOINTS
!compute G1n_n
   G1n_n = (0.d0,0.d0)

   do Jorb = 1,Morb  
      do Korb=1,Morb

         G1n_n = G1n_n + rho_jk(Jorb,Korb) * DCONJG(PSI(n,Jorb))*PSI(n,Korb)

      end do
   end do

   G1n_n=G1n_n*1.d0/w**(2)
   outG1n_n=DBLE(G1n_n)

   call get_ijk_from_m(n,NDVR_X,NDVR_Y,i_n,j_n,k_n)


   if (mode.eq.1) then

       write(12,6667) ort_X(i_n),ort_Y(j_n),ort_Z(k_n),outG1n_n

   else if ((mode.eq.2).or.(mode.eq.3)) then
  
       write(12,6667) mom_X(i_n),mom_Y(j_n),mom_Z(k_n),outG1n_n
   end if
   if (mod(i_n,NDVR_X).eq.0) write(12,*) '                             '
end do
                           
close(12)

6667  format(3(F8.3,1X),2(F16.9))
end subroutine get_1BodyDiag



!> computes the density rho^1(x,x) if PSI is provided or 
!> or rho^1(k,k) if FT(PSI) is provided and
!> writes it to a file. 
!> time          : current time 
!> PSI           : orbital array 
!> rho_jk        : the 1-body density matrix elements where rho_jk(j,k)=rho_jk 
!> mode          : mode=1: G1(x,x) 
!>                 mode=2: G1(k,k) is computed
!> dilation      : the integer dilation gives the factor by which the momentum intervall should
!!                 be shrunk or equivalently the factor by which the sampling rate is increased.
!!                 The momentum distribution at the outer ends is padded with zeros then. 
!!                 Normally dilation is computed by get_dilation. If unsure put
!!                 dilation = 1, but the momentum grid will then usually have a bad
!!                 resolution. 
subroutine get_1BodyDiag_Nlevel(time,PSI,rho_jk,mode,dilation) 

use Coefficients_Parameters,ONLY:NPar
use DVR_Parameters
USE Interaction_Parameters

implicit none

integer  :: NOGRIDPOINTS
integer,intent(in) :: mode,dilation
REAL*8 :: w,dk,mom_X(NDVR_X),mom_Y(NDVR_Y),mom_Z(NDVR_Z)
real*8             :: time
COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb,Nlevel)  :: PSI
complex*16,DIMENSION(Morb,Morb),intent(in) :: rho_jk
character(len=2)   :: aString
character(len=200) :: fullName
integer*8          :: I,n,i_n,j_n,k_n
INTEGER            :: JORB,KORB
complex*16         :: G1n_n(NLevel)
real*8, parameter  :: TOL=0.00001d0 

real*8             :: outG1n_n(Nlevel)
 
Call Get_WeightOrMomentumGrid(mom_x,mom_y,mom_z,w,dilation,mode)

if (mode.eq.1) then
   aString = 'x-'
else if ((mode.eq.2).or.(mode.eq.3)) then 
   aString = 'k-'
else 
   write(*,*)"ERROR in get_1bodyDiag_Nlevel"
   stop
endif

call Assemble_OutputFileName(FullName,Time,NPar,Morb,aString,'density.dat')
write(6,*) "Filename:",FullName

open(unit=12,file=trim(fullName),form='formatted')

NOGRIDPOINTS=NDVR_X*NDVR_Y*NDVR_Z

do n=1,NOGRIDPOINTS
!compute G1n_n
   G1n_n = (0.d0,0.d0)
  
   do Jorb = 1,Morb  
      do Korb=1,Morb
         DO I=1,Nlevel
            G1n_n(I) = G1n_n(I) + rho_jk(Jorb,Korb) * DCONJG(PSI(n,Jorb,I))*PSI(n,Korb,I)
         END DO
      end do
   end do

   G1n_n=G1n_n*1.d0/w**(2)
   outG1n_n=DBLE(G1n_n)

   call get_ijk_from_m(n,NDVR_X,NDVR_Y,i_n,j_n,k_n)
!    write(*,*) i_n,j_n
   if (mode.eq.1) then

       write(12,6667) ort_X(i_n),ort_Y(j_n),ort_Z(k_n),(outG1n_n(I),I=1,Nlevel)

   else if ((mode.eq.2).or.(mode.eq.3)) then
  
       write(12,6667) mom_X(i_n),mom_Y(j_n),mom_Z(k_n),(outG1n_n(I),I=1,Nlevel)

   end if
   if (mod(i_n,NDVR_X).eq.0) write(12,*) '                             '
end do
                           
close(12)

6667  format(3(F8.3,1X),99(F16.9))
end subroutine get_1BodyDiag_Nlevel

!> This routine computes the one-body density for two-dimensional computations 
!> on a dilated/zero-padded momentum grid.
subroutine get_2D_1BodyDiag_dilated_Nlevel(time,rho_jk,PSI_Nlevel,dilation)

  use Coefficients_Parameters,ONLY:NPar
  use DVR_Parameters
  USE Interaction_Parameters

  INTEGER,INTENT(IN) ::  dilation
  real*8,INTENT(IN)  ::  time
  COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb,Nlevel),INTENT(IN)  :: PSI_Nlevel
  complex*16,DIMENSION(Morb,Morb),intent(in) :: rho_jk
  COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb,Nlevel)             :: FTPSI_Nlevel
  character(len=2)   :: aString
  character(len=200) :: fullName
  real*8             :: dilmom_Z(NDVR_Z),dilmom_X(NDVR_X),dilmom_Y(NDVR_Y),dkdil,wdil,outG1n_n(Nlevel)
  real*8             :: xi,xf,yi,yf

  complex*16         :: G1n_n(Nlevel)
  INTEGER*8          :: i_n,j_n,k_n,n,L
  INTEGER            :: JORB,KORB

  aString = 'k-'

  call Assemble_OutputFileName(FullName,Time,NPar,Morb,aString,'density.dat')
  open(unit=12,file=trim(fullName),form='formatted')

  Call Get_WeightOrMomentumGrid(dilmom_x,dilmom_y,dilmom_z,wdil,dilation,2)
  dkdil     =     wdil**2 

  DO L=1,NLevel
    CALL twoD_zero_padding(PSI_Nlevel(:,:,L),FTPSI_Nlevel(:,:,L),dilation)
  END DO

  do n=1,NDVR_X*NDVR_Y
!compute G1n_n
     G1n_n = (0.d0,0.d0)

       do Jorb = 1,Morb  
          do Korb=1,Morb
             DO L=1,Nlevel
               G1n_n(L) = G1n_n(L) + rho_jk(Jorb,Korb) * DCONJG(FTPSI_Nlevel(n,Jorb,L))*FTPSI_Nlevel(n,Korb,L)
             END DO
          end do
       end do

       G1n_n=G1n_n*1.d0/wdil**2
       outG1n_n=DBLE(G1n_n)
       call get_ijk_from_m(n,NDVR_X,NDVR_Y,i_n,j_n,k_n)
       write(12,'(99E25.16)') dilmom_X(i_n),dilmom_Y(j_n),0.d0,(outG1n_n(L),L=1,Nlevel)
       if (mod(i_n,(NDVR_X)).eq.0) write(12,*) '                             '
       
    end do   
    close(12)
end subroutine get_2D_1BodyDiag_dilated_Nlevel


!> This routine computes the one-body density for two-dimensional computations 
!> on a dilated/zero-padded momentum grid.
subroutine get_2D_1BodyDiag_dilated(time,rho_jk,PSI,dil)

  use Coefficients_Parameters,ONLY:NPar
  use DVR_Parameters
  USE Interaction_Parameters

  INTEGER,INTENT(IN) ::  dil
  real*8,INTENT(IN)  ::  time
  COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb),INTENT(IN)  :: PSI
  complex*16,DIMENSION(Morb,Morb),intent(in) :: rho_jk
  COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb)             :: FTPSI
  character(len=2)   :: aString
  character(len=200) :: fullName
  real*8             :: dilmom_X(NDVR_X),dilmom_Y(NDVR_Y),dilmom_Z(NDVR_Z),dkdil,wdil,outG1n_n
  real*8             :: xi,xf,yi,yf

  complex*16         :: G1n_n
  INTEGER*8          :: i_n,j_n,k_n,n
  INTEGER            :: JORB,KORB

  aString = 'k-'
  call Assemble_OutputFileName(FullName,Time,NPar,Morb,aString,'density.dat')
  open(unit=12,file=trim(fullName),form='formatted')


  Call Get_WeightOrMomentumGrid(dilmom_x,dilmom_y,dilmom_z,wdil,dil,2)
  dkdil     =     wdil**2 

  CALL twoD_zero_padding(PSI,FTPSI,dil)

!  Write(6,*) "Computed dilated Fourier Transform, Dil-Density-2D"

  do n=1,NDVR_X*NDVR_Y
!compute G1n_n
     G1n_n = (0.d0,0.d0)
       do Jorb = 1,Morb  
          do Korb=1,Morb

             G1n_n = G1n_n + rho_jk(Jorb,Korb) * DCONJG(FTPSI(n,Jorb))*FTPSI(n,Korb)
  
          end do
       end do
       G1n_n=G1n_n*1.d0/wdil**2
       outG1n_n=DBLE(G1n_n)
       call get_ijk_from_m(n,NDVR_X,NDVR_Y,i_n,j_n,k_n)
       write(12,'(E25.16,E25.16,E25.16,E25.16)') dilmom_X(i_n),dilmom_Y(j_n),0.d0,outG1n_n
       if (mod(i_n,(NDVR_X)).eq.0) write(12,*) '                             '
       
    end do   
!       return
    close(12)
end subroutine get_2D_1BodyDiag_dilated

!> This routine computes the one-body density for three-dimensional computations 
!> on a dilated/zero-padded momentum grid.
subroutine get_3D_1BodyDiag_dilated(time,rho_jk,PSI,dil)

  use Coefficients_Parameters,ONLY:NPar
  use DVR_Parameters
  USE Interaction_Parameters

  INTEGER,INTENT(IN) ::  dil
  real*8,INTENT(IN)  ::  time
  COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb),INTENT(IN)  :: PSI
  complex*16,DIMENSION(Morb,Morb),intent(in) :: rho_jk
  COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb)             :: FTPSI
  character(len=2)   :: aString
  character(len=200) :: fullName
  real*8             :: dilmom_X(NDVR_X),dilmom_Y(NDVR_Y),dilmom_Z(NDVR_Z),dkdil,wdil,outG1n_n
  real*8             :: xi,xf,yi,yf

  complex*16         :: G1n_n
  INTEGER*8          :: i_n,j_n,k_n,n
  INTEGER            :: JORB,KORB

  aString = 'k-'
  call Assemble_OutputFileName(FullName,Time,NPar,Morb,aString,'density.dat')
  open(unit=12,file=trim(fullName),form='formatted')


  Call Get_WeightOrMomentumGrid(dilmom_x,dilmom_y,dilmom_z,wdil,dil,2)
  dkdil     =     wdil**2 

  CALL threeD_zero_padding(PSI,FTPSI,dil)

!  Write(6,*) "Computed dilated Fourier Transform, Dil-Density-2D"

  do n=1,NDVR_X*NDVR_Y*NDVR_Z
!compute G1n_n
     G1n_n = (0.d0,0.d0)
       do Jorb = 1,Morb  
          do Korb=1,Morb

             G1n_n = G1n_n + rho_jk(Jorb,Korb) * DCONJG(FTPSI(n,Jorb))*FTPSI(n,Korb)
  
          end do
       end do
       G1n_n=G1n_n*1.d0/wdil**2
       outG1n_n=DBLE(G1n_n)
       call get_ijk_from_m(n,NDVR_X,NDVR_Y,i_n,j_n,k_n)
       write(12,'(E25.16,E25.16,E25.16,E25.16)') dilmom_X(i_n),dilmom_Y(j_n),dilmom_Z(k_n),outG1n_n
       if (mod(i_n,(NDVR_X)).eq.0) write(12,*) '                             '
       
    end do   
!       return
    close(12)
end subroutine get_3D_1BodyDiag_dilated



!###############################################################
!>         This routine integrates the one body density 
!>    from xi to xf and writes the output to the file nonescape.
!>    Currently only one-dimensional problems are supported.
!###############################################################
subroutine density_nonescape(xi,xf,yi,yf,zi,zf,rho_jk,time,PSI)

use Coefficients_Parameters,ONLY:NPar
use DVR_Parameters
USE Interaction_Parameters

  IMPLICIT NONE

  COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb)  :: PSI
  complex*16,DIMENSION(Morb,Morb),intent(in) :: rho_jk
  real*8, intent(in) :: xi,xf,yi,yf,zi,zf
  real*8 :: nonescape,time,w
  
  complex*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z)  :: DENSITY    

  integer*8 :: i,j,k,n,NOGRIDPOINTS

  w=DSQRT((xf-xi)/NDVR_X)


  NOGRIDPOINTS=NDVR_X*NDVR_Y*NDVR_Z
! get the density from the one-body-matrix elements 
  DENSITY   = (0.d0,0.d0)
  do n=1,NOGRIDPOINTS
     do j = 1,Morb  
        do k=1,MOrb
           DENSITY(n) = DENSITY(n) + rho_jk(j,k) * DCONJG(PSI(n,j))*PSI(n,k)
        end do
     end do
  end do

! integrate from DENSITY from xi to xf
  
  if(DIM_MCTDH.eq.1) then
    nonescape=0.d0
    do n=1,NOGRIDPOINTS
       call get_ijk_from_m(n,NDVR_X,NDVR_Y,i,j,k)
       if ((Ort_X(i).lt.xf).and.(Ort_X(i).gt.xi)) then
          nonescape=nonescape +dble(DENSITY(n))
       endif
    end do
  else if(DIM_MCTDH.eq.2) then
    nonescape=0.d0
    do n=1,NOGRIDPOINTS
       call get_ijk_from_m(n,NDVR_X,NDVR_Y,i,j,k)
       if (((Ort_X(i).lt.xf).and.(Ort_X(i).gt.xi)).and.&
          ((Ort_Y(j).lt.yf).and.(Ort_Y(j).gt.yi))) then
          nonescape=nonescape +dble(DENSITY(n))
       endif
    end do
  else if(DIM_MCTDH.eq.3) then
    nonescape=0.d0
    do n=1,NOGRIDPOINTS
       call get_ijk_from_m(n,NDVR_X,NDVR_Y,i,j,k)
       if (((Ort_X(i).lt.xf).and.(Ort_X(i).gt.xi)).and.&
          ((Ort_Y(j).lt.yf).and.(Ort_Y(j).gt.yi)).and.&
          ((Ort_Z(k).lt.zf).and.(Ort_Z(k).gt.zi))) then
          nonescape=nonescape +dble(DENSITY(n))
       endif
    end do
   endif

! open/write/close output
  open(unit=542,file='nonescape',form='formatted',ACCESS='APPEND')

  write(542,6545) time,' ',nonescape 

6545  format(2(F21.16,a1))

  close(542)
end subroutine density_nonescape




!> This routine computes the Phase,phase gradient and vorticity in 2D analysis,
subroutine Get_Phase(Psi,Natvecs,Noccs,time,Gradient,Vorticity)

USE Global_Parameters
USE DVR_Parameters
USE Coefficients_Parameters

IMPLICIT NONE

LOGICAL :: Gradient,Vorticity
REAL*8, DIMENSION(MORB) :: Noccs
REAL*8 :: Time,Rho_Phase
COMPLEX*16, DIMENSION(MORB,MORB) :: Natvecs
COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb)  :: PSI,NOs
REAL*8, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb)  :: Phase
REAL*8, ALLOCATABLE :: Phase_Gradient_X(:,:)
REAL*8, ALLOCATABLE :: Phase_Gradient_Y(:,:)
REAL*8, ALLOCATABLE :: Vorticity_Field(:,:)
REAL*8, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z)  :: Averaged_Phase


Integer*8 :: I,J,K,L,M,NCOLS

character(len=200) :: fullName
character(len=10)  :: timeAsString
character(len=10)  :: MorbAsString
character(len=1)   :: NparAsString1
character(len=2)   :: NparAsString2
character(len=3)   :: NparAsString3
character(len=4)   :: NparAsString4
character(len=5)   :: NparAsString5
character(len=6)   :: NparAsString6
character(len=7)   :: NparAsString7
character(len=8)   :: NparAsString8

CALL GET_NOs(PSI,NOs,Natvecs)

IF (Gradient.eqv..TRUE.) THEN
   ALLOCATE(Phase_Gradient_X(NDVR_X*NDVR_Y*NDVR_Z,Morb+1))
   ALLOCATE(Phase_Gradient_Y(NDVR_X*NDVR_Y*NDVR_Z,Morb+1))
   IF (Vorticity.eqv..TRUE.) THEN
      ALLOCATE(Vorticity_Field(NDVR_X*NDVR_Y*NDVR_Z,Morb+1))
   ENDIF 
ENDIF

DO I=1,NDVR_X*NDVR_Y*NDVR_Z
   DO J=1,MORB
      Phase(I,J)=atan2(aimag(NOs(I,J)),real(NOs(I,J)))
   END DO
END DO


Averaged_Phase=0.d0
DO J=1,MORB
   DO I=1,NDVR_X*NDVR_Y*NDVR_Z
      Averaged_Phase(I)=Averaged_Phase(I)+(Phase(I,J)*Noccs(J))
   END DO
END DO

if (MORB.lt.10) then
   write (MorbAsString, '(I1)') Morb
else if ((MORB.ge.10).and.(MORB.lt.100)) then
   write (MorbAsString, '(I2)') Morb
else 
   write(*,*) 'Bigger orbital number than 100 not implemented!'
endif


call getDoubleAsString(time,timeAsString) 

if ((NPar.gt.0) .and. (NPar.lt.10)) then 
  write (NParAsString1, '(I1)') NPar
  fullName=timeasString//'N'//trim(NParAsString1)//'M'//trim(MorbAsString)//'phase.dat'
else  if ((NPar.ge.10) .and. (NPar.lt.100)) then 
  write (NParAsString2, '(I2)') NPar
  fullName=timeasString//'N'//trim(NParAsString2)//'M'//trim(MorbAsString)//'phase.dat'
else if ((NPar.ge.100) .and. (NPar.lt.1000)) then 
  write (NParAsString3, '(I3)') NPar
  fullName=timeasString//'N'//trim(NParAsString3)//'M'//trim(MorbAsString)//'phase.dat'
else if ((NPar.ge.1000) .and. (NPar.lt.10000)) then 
  write (NParAsString4, '(I4)') NPar
  fullName=timeasString//'N'//trim(NParAsString4)//'M'//trim(MorbAsString)//'phase.dat'
else  if ((NPar.ge.10000) .and. (NPar.lt.100000)) then 
  write (NParAsString5, '(I5)') NPar
  fullName=timeasString//'N'//trim(NParAsString5)//'M'//trim(MorbAsString)//'phase.dat'
else if ((NPar.ge.100000) .and. (NPar.lt.1000000)) then 
  write (NParAsString6, '(I6)') NPar
  fullName=timeasString//'N'//trim(NParAsString6)//'M'//trim(MorbAsString)//'phase.dat'
else if ((NPar.ge.1000000) .and. (NPar.lt.10000000)) then 
  write (NParAsString7, '(I7)') NPar
  fullName=timeasString//'N'//trim(NParAsString7)//'M'//trim(MorbAsString)//'phase.dat'
else if ((NPar.ge.10000000) .and. (NPar.lt.100000000)) then 
  write (NParAsString8, '(I8)') NPar
  fullName=timeasString//'N'//trim(NParAsString8)//'M'//trim(MorbAsString)//'phase.dat'
else 
  NParAsString4='XXXX'
  fullName=timeasString//'N'//trim(NParAsString4)//'M'//trim(MorbAsString)//'correlations.dat'
end if

open(unit=669,file=trim(fullName),form='formatted')
write(669,*)"          "
write(669,*)"          "
 

IF (Gradient.eqv..FALSE.) THEN 
   DO M=1,NDVR_X*NDVR_Y*NDVR_Z

       call get_ijk_from_m(m,NDVR_X,NDVR_Y,i,j,k)

       WRITE(669,6556) Ort_x(I), Ort_y(j), Ort_z(k),&
             Averaged_Phase(M),(Phase(M,L),L=1,MORB)
        
       if(MOD(m,NDVR_X).eq.0) write(669,*) "                                            " 

   END DO
6556  format(99(E25.16,1X))

   close(669)


ELSE IF (Gradient.eqv..TRUE.) THEN

  
   DO K=1,MORB
      CALL Get_Gradient_2D(Phase(:,K),&
                           Phase_Gradient_X(:,K),&
                           Phase_Gradient_Y(:,K))
   END DO

   CALL Get_Gradient_2D(Averaged_Phase,&
                        Phase_Gradient_X(:,MORB+1),&
                        Phase_Gradient_Y(:,MORB+1))

   IF (Vorticity.eqv..FALSE.) THEN
   

      DO M=1,NDVR_X*NDVR_Y*NDVR_Z

         call get_ijk_from_m(m,NDVR_X,NDVR_Y,i,j,k)

         WRITE(669,6556) Ort_x(I), Ort_y(j), Ort_z(k),&
                         Averaged_Phase(M),(Phase(M,L),L=1,MORB),&
                         Phase_Gradient_X(M,MORB+1),&
                         Phase_Gradient_Y(M,MORB+1),&
                  (Phase_Gradient_X(M,L),Phase_Gradient_Y(M,L),L=1,MORB)
         if(MOD(m,NDVR_X).eq.0) write(669,*) "                                            " 
      END DO

      close(669)


   ELSE IF (Vorticity.eqv..TRUE.) THEN

     DO L=1,MORB+1
       DO M=1,NDVR_X*NDVR_Y*NDVR_Z

         call get_ijk_from_m(m,NDVR_X,NDVR_Y,i,j,k)
         Phase_Gradient_X(M,L)=ort_y(J)*ort_y(j)+ort_x(I)*ort_x(i)
         Phase_Gradient_Y(M,L)=ort_x(I)*ort_y(J)

       END DO
     END DO

      DO L=1,MORB+1
         WRITE(6,*) " I AM DOING THE VORTICITY ANALYSIS"
         CALL Get_Derivative_2D(Phase_Gradient_Y(:,L),&
                          Vorticity_Field(:,L),&
                          1)
      END DO
      DO M=1,NDVR_X*NDVR_Y*NDVR_Z

         call get_ijk_from_m(m,NDVR_X,NDVR_Y,i,j,k)
         WRITE(669,6556) Ort_x(I), Ort_y(j), Ort_z(k),&
                         Averaged_Phase(M),(Phase(M,L),L=1,MORB),&
                         Phase_Gradient_X(M,MORB+1),&
                         Phase_Gradient_Y(M,MORB+1),&
                 (Phase_Gradient_X(M,L),Phase_Gradient_Y(M,L),L=1,MORB),&
                         Vorticity_Field(M,MORB+1),&
                         (Vorticity_Field(M,L),L=1,MORB) 

         if(MOD(m,NDVR_X).eq.0) write(669,*) "                                            " 
      END DO

      close(669)

   ENDIF

ENDIF


end subroutine Get_Phase

subroutine Get_SpinDensity_Nlevel(time,PSI,rho_jk)

use Coefficients_Parameters,ONLY:NPar
use DVR_Parameters
USE Interaction_Parameters

implicit none 

integer  :: NOGRIDPOINTS
REAL*8 :: w,dk,mom_X(NDVR_X),mom_Y(NDVR_Y),mom_Z(NDVR_Z)
real*8             :: time 
COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb,Nlevel)  :: PSI  
complex*16,DIMENSION(Morb,Morb),intent(in) :: rho_jk
character(len=2)   :: aString
character(len=200) :: fullName
integer*8            :: I,n,Jorb,Korb,i_n,j_n,k_n
complex*16         :: G1n_n(NLevel)
real*8, parameter  :: TOL=0.00001d0 

real*8             :: outG1n_n(Nlevel)
real*8             :: outS0, outSx, outSy, outSz
complex*16         :: S_0, S_x, S_y, S_z  

Call Get_WeightOrMomentumGrid(mom_x,mom_y,mom_z,w,1,1)

aString='x-'
call Assemble_OutputFileName(FullName,Time,NPar,Morb,aString,'spin_density.dat')
write(6,*) "Filename:",FullName

open(unit=12,file=trim(fullName),form='formatted')

NOGRIDPOINTS=NDVR_X*NDVR_Y*NDVR_Z

do n=1,NOGRIDPOINTS

   S_0 = (0.d0,0.d0)
   S_x = (0.d0,0.d0)
   S_y = (0.d0,0.d0)
   S_z = (0.d0,0.d0)

   do Jorb = 1,Morb  
      do Korb=1,Morb

         S_0 = S_0 + rho_jk(Jorb,Korb) * (DCONJG(PSI(n,Jorb,1))*PSI(n,Korb,1) &
                                        + DCONJG(PSI(n,Jorb,2))*PSI(n,Korb,2))

         S_x = S_x + rho_jk(Jorb,Korb) * (DCONJG(PSI(n,Jorb,1))*PSI(n,Korb,2) &
                                        + DCONJG(PSI(n,Jorb,2))*PSI(n,Korb,1))

         S_y = S_y + rho_jk(Jorb,Korb) * (DCONJG(PSI(n,Jorb,1))*PSI(n,Korb,2) &
                                        - DCONJG(PSI(n,Jorb,2))*PSI(n,Korb,1))

         S_z = S_z + rho_jk(Jorb,Korb) * (DCONJG(PSI(n,Jorb,1))*PSI(n,Korb,1) &
                                        - DCONJG(PSI(n,Jorb,2))*PSI(n,Korb,2))

      end do
   end do

   S_0 = S_0*1.d0/w**(2)
   S_x = S_x*1.d0/w**(2)
   S_y = S_y*(0.d0,1.d0)/w**(2)
   S_z = S_z*1.d0/w**(2)
   outS0 = DREAL(S_0)
   outSx = DREAL(S_x)
   outSy = DREAL(S_y)
   outSz = DREAL(S_z)


   call get_ijk_from_m(n,NDVR_X,NDVR_Y,i_n,j_n,k_n)

       write(12,6667) ort_X(i_n),ort_Y(j_n),ort_Z(k_n),outS0,outSx,outSy,outSz


   if (mod(i_n,NDVR_X).eq.0) write(12,*) '                             '
end do

close(12)

6667  format(3(F8.3,1X),99(F40.20))
end subroutine Get_SpinDensity_Nlevel

END MODULE Analysis_Routines_OneBody
