!>GPLv3
!    MCTDH-X: the multiconfigurational time-dependent Hartree for 
!    indistinguishable particles software
!
!    Copyright (C) 2021  A. U. J. Lode, M. C. Tsatsos, 
!                        E. Fasshauer, S. E. Weiner, 
!                        R. Lin, L. Papariello, P. Molignini, 
!                        C. Lévêque, M. Büttner , J. Xiang, S. Dutta
!
!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <https://www.gnu.org/licenses/>.
!
!
!
!
!<GPLv3











!-----------------------------------------------------------------------
MODULE nrtype
    INTEGER, PARAMETER :: I4B = SELECTED_INT_KIND(9)
    INTEGER, PARAMETER :: I2B = SELECTED_INT_KIND(4)
    INTEGER, PARAMETER :: I1B = SELECTED_INT_KIND(2)
    INTEGER, PARAMETER :: SP = KIND(1.0)
    INTEGER, PARAMETER :: DP = KIND(1.0D0)
    INTEGER, PARAMETER :: SPC = KIND((1.0,1.0))
    INTEGER, PARAMETER :: DPC = KIND((1.0D0,1.0D0))
    INTEGER, PARAMETER :: LGT = KIND(.true.)
    REAL(SP), PARAMETER :: PI=3.141592653589793238462643383279502884197_sp
    REAL(SP), PARAMETER :: PIO2=1.57079632679489661923132169163975144209858_sp
    REAL(SP), PARAMETER :: TWOPI=6.283185307179586476925286766559005768394_sp
    REAL(SP), PARAMETER :: SQRT2=1.41421356237309504880168872420969807856967_sp
    REAL(SP), PARAMETER :: EULER=0.5772156649015328606065120900824024310422_sp
    REAL(DP), PARAMETER :: PI_D=3.141592653589793238462643383279502884197_dp
    REAL(DP), PARAMETER :: PIO2_D=1.57079632679489661923132169163975144209858_dp
    REAL(DP), PARAMETER :: TWOPI_D=6.283185307179586476925286766559005768394_dp
    TYPE sprs2_sp
        INTEGER(I4B) :: n,len
        REAL(SP), DIMENSION(:), POINTER :: val
        INTEGER(I4B), DIMENSION(:), POINTER :: irow
        INTEGER(I4B), DIMENSION(:), POINTER :: jcol
    END TYPE sprs2_sp
    TYPE sprs2_dp
        INTEGER(I4B) :: n,len
        REAL(DP), DIMENSION(:), POINTER :: val
        INTEGER(I4B), DIMENSION(:), POINTER :: irow
        INTEGER(I4B), DIMENSION(:), POINTER :: jcol
    END TYPE sprs2_dp
END MODULE nrtype
!-----------------------------------------------------------------------
MODULE nrutil
    USE nrtype
    IMPLICIT NONE
    INTEGER(I4B), PARAMETER :: NPAR_ARTH=16,NPAR2_ARTH=8
    INTEGER(I4B), PARAMETER :: NPAR_GEOP=4,NPAR2_GEOP=2
    INTEGER(I4B), PARAMETER :: NPAR_CUMSUM=16
    INTEGER(I4B), PARAMETER :: NPAR_CUMPROD=8
    INTEGER(I4B), PARAMETER :: NPAR_POLY=8
    INTEGER(I4B), PARAMETER :: NPAR_POLYTERM=8
    INTERFACE swap
        MODULE PROCEDURE swap_i,swap_r,swap_rv,swap_c, &
            swap_cv,swap_cm,swap_z,swap_zv,swap_zm, &
            masked_swap_rs,masked_swap_rv,masked_swap_rm
    END INTERFACE
    INTERFACE assert
        MODULE PROCEDURE assert1,assert2,assert3,assert4,assert_v
    END INTERFACE
    INTERFACE assert_eq
        MODULE PROCEDURE assert_eq2,assert_eq3,assert_eq4,assert_eqn
    END INTERFACE
    INTERFACE arth
        MODULE PROCEDURE arth_r, arth_d, arth_i
    END INTERFACE
    INTERFACE geop
        MODULE PROCEDURE geop_r, geop_d, geop_i, geop_c, geop_dv
    END INTERFACE
    INTERFACE cumsum
        MODULE PROCEDURE cumsum_r,cumsum_i
    END INTERFACE
    INTERFACE poly
        MODULE PROCEDURE poly_rr,poly_rrv,poly_dd,poly_ddv,&
            poly_rc,poly_cc,poly_msk_rrv,poly_msk_ddv
    END INTERFACE
    INTERFACE poly_term
        MODULE PROCEDURE poly_term_rr,poly_term_cc
    END INTERFACE
    INTERFACE outerprod
        MODULE PROCEDURE outerprod_r,outerprod_d
    END INTERFACE
    INTERFACE outerdiff
        MODULE PROCEDURE outerdiff_r,outerdiff_d,outerdiff_i
    END INTERFACE
    INTERFACE scatter_add
        MODULE PROCEDURE scatter_add_r,scatter_add_d
    END INTERFACE
    INTERFACE scatter_max
        MODULE PROCEDURE scatter_max_r,scatter_max_d
    END INTERFACE
    INTERFACE diagadd
        MODULE PROCEDURE diagadd_rv,diagadd_r
    END INTERFACE
    INTERFACE diagmult
        MODULE PROCEDURE diagmult_rv,diagmult_r
    END INTERFACE
    INTERFACE get_diag
        MODULE PROCEDURE get_diag_rv, get_diag_dv
    END INTERFACE
    INTERFACE put_diag
        MODULE PROCEDURE put_diag_rv, put_diag_r
    END INTERFACE
CONTAINS
!BL
    SUBROUTINE swap_i(a,b)
    INTEGER(I4B), INTENT(INOUT) :: a,b
    INTEGER(I4B) :: dum
    dum=a
    a=b
    b=dum
    END SUBROUTINE swap_i
!BL
    SUBROUTINE swap_r(a,b)
    REAL(SP), INTENT(INOUT) :: a,b
    REAL(SP) :: dum
    dum=a
    a=b
    b=dum
    END SUBROUTINE swap_r
!BL
    SUBROUTINE swap_rv(a,b)
    REAL(SP), DIMENSION(:), INTENT(INOUT) :: a,b
    REAL(SP), DIMENSION(SIZE(a)) :: dum
    dum=a
    a=b
    b=dum
    END SUBROUTINE swap_rv
!BL
    SUBROUTINE swap_c(a,b)
    COMPLEX(SPC), INTENT(INOUT) :: a,b
    COMPLEX(SPC) :: dum
    dum=a
    a=b
    b=dum
    END SUBROUTINE swap_c
!BL
    SUBROUTINE swap_cv(a,b)
    COMPLEX(SPC), DIMENSION(:), INTENT(INOUT) :: a,b
    COMPLEX(SPC), DIMENSION(SIZE(a)) :: dum
    dum=a
    a=b
    b=dum
    END SUBROUTINE swap_cv
!BL
    SUBROUTINE swap_cm(a,b)
    COMPLEX(SPC), DIMENSION(:,:), INTENT(INOUT) :: a,b
    COMPLEX(SPC), DIMENSION(size(a,1),size(a,2)) :: dum
    dum=a
    a=b
    b=dum
    END SUBROUTINE swap_cm
!BL
    SUBROUTINE swap_z(a,b)
    COMPLEX(DPC), INTENT(INOUT) :: a,b
    COMPLEX(DPC) :: dum
    dum=a
    a=b
    b=dum
    END SUBROUTINE swap_z
!BL
    SUBROUTINE swap_zv(a,b)
    COMPLEX(DPC), DIMENSION(:), INTENT(INOUT) :: a,b
    COMPLEX(DPC), DIMENSION(SIZE(a)) :: dum
    dum=a
    a=b
    b=dum
    END SUBROUTINE swap_zv
!BL
    SUBROUTINE swap_zm(a,b)
    COMPLEX(DPC), DIMENSION(:,:), INTENT(INOUT) :: a,b
    COMPLEX(DPC), DIMENSION(size(a,1),size(a,2)) :: dum
    dum=a
    a=b
    b=dum
    END SUBROUTINE swap_zm
!BL
    SUBROUTINE masked_swap_rs(a,b,mask)
    REAL(SP), INTENT(INOUT) :: a,b
    LOGICAL(LGT), INTENT(IN) :: mask
    REAL(SP) :: swp
    if (mask) then
        swp=a
        a=b
        b=swp
    end if
    END SUBROUTINE masked_swap_rs
!BL
    SUBROUTINE masked_swap_rv(a,b,mask)
    REAL(SP), DIMENSION(:), INTENT(INOUT) :: a,b
    LOGICAL(LGT), DIMENSION(:), INTENT(IN) :: mask
    REAL(SP), DIMENSION(size(a)) :: swp
    where (mask)
        swp=a
        a=b
        b=swp
    end where
    END SUBROUTINE masked_swap_rv
!BL
    SUBROUTINE masked_swap_rm(a,b,mask)
    REAL(SP), DIMENSION(:,:), INTENT(INOUT) :: a,b
    LOGICAL(LGT), DIMENSION(:,:), INTENT(IN) :: mask
    REAL(SP), DIMENSION(size(a,1),size(a,2)) :: swp
    where (mask)
        swp=a
        a=b
        b=swp
    end where
    END SUBROUTINE masked_swap_rm
!BL

    SUBROUTINE assert1(n1,string)
    CHARACTER(LEN=*), INTENT(IN) :: string
    LOGICAL, INTENT(IN) :: n1
    if (.not. n1) then
       write (6,*) 'Terminating conjugate gradient loop, errorcode',string,'. Continuing with CRAB-update'
       RETURN
    end if
    END SUBROUTINE assert1
!BL
    SUBROUTINE assert2(n1,n2,string)
    CHARACTER(LEN=*), INTENT(IN) :: string
    LOGICAL, INTENT(IN) :: n1,n2
    if (.not. (n1 .and. n2)) then
       write (6,*) 'Terminating conjugate gradient loop, errorcode',string,'. Continuing with CRAB-update'
       RETURN
    end if
    END SUBROUTINE assert2
!BL
    SUBROUTINE assert3(n1,n2,n3,string)
    CHARACTER(LEN=*), INTENT(IN) :: string
    LOGICAL, INTENT(IN) :: n1,n2,n3
    if (.not. (n1 .and. n2 .and. n3)) then
       write (6,*) 'Terminating conjugate gradient loop, errorcode',string,'. Continuing with CRAB-update'
       RETURN
    end if
    END SUBROUTINE assert3
!BL
    SUBROUTINE assert4(n1,n2,n3,n4,string)
    CHARACTER(LEN=*), INTENT(IN) :: string
    LOGICAL, INTENT(IN) :: n1,n2,n3,n4
    if (.not. (n1 .and. n2 .and. n3 .and. n4)) then
       write (6,*) 'Terminating conjugate gradient loop, errorcode',string,'. Continuing with CRAB-update'
       RETURN
    end if
    END SUBROUTINE assert4
!BL
    SUBROUTINE assert_v(n,string)
    CHARACTER(LEN=*), INTENT(IN) :: string
    LOGICAL, DIMENSION(:), INTENT(IN) :: n
    if (.not. all(n)) then
       write (6,*) 'Terminating conjugate gradient loop, errorcode',string,'. Continuing with CRAB-update'
       RETURN
    end if
    END SUBROUTINE assert_v
!BL
    FUNCTION assert_eq2(n1,n2,string)
    CHARACTER(LEN=*), INTENT(IN) :: string
    INTEGER, INTENT(IN) :: n1,n2
    INTEGER :: assert_eq2
    if (n1 == n2) then
        assert_eq2=n1
    else
       write (6,*) 'Terminating conjugate gradient loop, errorcode',string,'. Continuing with CRAB-update'
       RETURN
    end if
    END FUNCTION assert_eq2
!BL
    FUNCTION assert_eq3(n1,n2,n3,string)
    CHARACTER(LEN=*), INTENT(IN) :: string
    INTEGER, INTENT(IN) :: n1,n2,n3
    INTEGER :: assert_eq3
    if (n1 == n2 .and. n2 == n3) then
        assert_eq3=n1
    else
       write (6,*) 'Terminating conjugate gradient loop, errorcode',string,'. Continuing with CRAB-update'
       RETURN
    end if
    END FUNCTION assert_eq3
!BL
    FUNCTION assert_eq4(n1,n2,n3,n4,string)
    CHARACTER(LEN=*), INTENT(IN) :: string
    INTEGER, INTENT(IN) :: n1,n2,n3,n4
    INTEGER :: assert_eq4
    if (n1 == n2 .and. n2 == n3 .and. n3 == n4) then
        assert_eq4=n1
    else
        write (6,*) 'Terminating conjugate gradient loop, errorcode',string,'. Continuing with CRAB-update'
       RETURN
    end if
    END FUNCTION assert_eq4
!BL
    FUNCTION assert_eqn(nn,string)
    CHARACTER(LEN=*), INTENT(IN) :: string
    INTEGER, DIMENSION(:), INTENT(IN) :: nn
    INTEGER :: assert_eqn
    if (all(nn(2:) == nn(1))) then
        assert_eqn=nn(1)
    else
        write (6,*) 'Terminating conjugate gradient loop, errorcode',string,'. Continuing with CRAB-update'
       RETURN
    end if
    END FUNCTION assert_eqn
!BL
    SUBROUTINE nrerror(string)
    CHARACTER(LEN=*), INTENT(IN) :: string
    write (6,*) 'Terminating conjugate gradient loop, errorcode',string,'. Continuing with CRAB-update'
       RETURN
    END SUBROUTINE nrerror
!BL
    FUNCTION arth_r(first,increment,n)
    REAL(SP), INTENT(IN) :: first,increment
    INTEGER(I4B), INTENT(IN) :: n
    REAL(SP), DIMENSION(n) :: arth_r
    INTEGER(I4B) :: k,k2
    REAL(SP) :: temp
    if (n > 0) arth_r(1)=first
    if (n <= NPAR_ARTH) then
        do k=2,n
            arth_r(k)=arth_r(k-1)+increment
        end do
    else
        do k=2,NPAR2_ARTH
            arth_r(k)=arth_r(k-1)+increment
        end do
        temp=increment*NPAR2_ARTH
        k=NPAR2_ARTH
        do
            if (k >= n) exit
            k2=k+k
            arth_r(k+1:min(k2,n))=temp+arth_r(1:min(k,n-k))
            temp=temp+temp
            k=k2
        end do
    end if
    END FUNCTION arth_r
!BL
    FUNCTION arth_d(first,increment,n)
    REAL(DP), INTENT(IN) :: first,increment
    INTEGER(I4B), INTENT(IN) :: n
    REAL(DP), DIMENSION(n) :: arth_d
    INTEGER(I4B) :: k,k2
    REAL(DP) :: temp
    if (n > 0) arth_d(1)=first
    if (n <= NPAR_ARTH) then
        do k=2,n
            arth_d(k)=arth_d(k-1)+increment
        end do
    else
        do k=2,NPAR2_ARTH
            arth_d(k)=arth_d(k-1)+increment
        end do
        temp=increment*NPAR2_ARTH
        k=NPAR2_ARTH
        do
            if (k >= n) exit
            k2=k+k
            arth_d(k+1:min(k2,n))=temp+arth_d(1:min(k,n-k))
            temp=temp+temp
            k=k2
        end do
    end if
    END FUNCTION arth_d
!BL
    FUNCTION arth_i(first,increment,n)
    INTEGER(I4B), INTENT(IN) :: first,increment,n
    INTEGER(I4B), DIMENSION(n) :: arth_i
    INTEGER(I4B) :: k,k2,temp
    if (n > 0) arth_i(1)=first
    if (n <= NPAR_ARTH) then
        do k=2,n
            arth_i(k)=arth_i(k-1)+increment
        end do
    else
        do k=2,NPAR2_ARTH
            arth_i(k)=arth_i(k-1)+increment
        end do
        temp=increment*NPAR2_ARTH
        k=NPAR2_ARTH
        do
            if (k >= n) exit
            k2=k+k
            arth_i(k+1:min(k2,n))=temp+arth_i(1:min(k,n-k))
            temp=temp+temp
            k=k2
        end do
    end if
    END FUNCTION arth_i
!BL
!BL
    FUNCTION geop_r(first,factor,n)
    REAL(SP), INTENT(IN) :: first,factor
    INTEGER(I4B), INTENT(IN) :: n
    REAL(SP), DIMENSION(n) :: geop_r
    INTEGER(I4B) :: k,k2
    REAL(SP) :: temp
    if (n > 0) geop_r(1)=first
    if (n <= NPAR_GEOP) then
        do k=2,n
            geop_r(k)=geop_r(k-1)*factor
        end do
    else
        do k=2,NPAR2_GEOP
            geop_r(k)=geop_r(k-1)*factor
        end do
        temp=factor**NPAR2_GEOP
        k=NPAR2_GEOP
        do
            if (k >= n) exit
            k2=k+k
            geop_r(k+1:min(k2,n))=temp*geop_r(1:min(k,n-k))
            temp=temp*temp
            k=k2
        end do
    end if
    END FUNCTION geop_r
!BL
    FUNCTION geop_d(first,factor,n)
    REAL(DP), INTENT(IN) :: first,factor
    INTEGER(I4B), INTENT(IN) :: n
    REAL(DP), DIMENSION(n) :: geop_d
    INTEGER(I4B) :: k,k2
    REAL(DP) :: temp
    if (n > 0) geop_d(1)=first
    if (n <= NPAR_GEOP) then
        do k=2,n
            geop_d(k)=geop_d(k-1)*factor
        end do
    else
        do k=2,NPAR2_GEOP
            geop_d(k)=geop_d(k-1)*factor
        end do
        temp=factor**NPAR2_GEOP
        k=NPAR2_GEOP
        do
            if (k >= n) exit
            k2=k+k
            geop_d(k+1:min(k2,n))=temp*geop_d(1:min(k,n-k))
            temp=temp*temp
            k=k2
        end do
    end if
    END FUNCTION geop_d
!BL
    FUNCTION geop_i(first,factor,n)
    INTEGER(I4B), INTENT(IN) :: first,factor,n
    INTEGER(I4B), DIMENSION(n) :: geop_i
    INTEGER(I4B) :: k,k2,temp
    if (n > 0) geop_i(1)=first
    if (n <= NPAR_GEOP) then
        do k=2,n
            geop_i(k)=geop_i(k-1)*factor
        end do
    else
        do k=2,NPAR2_GEOP
            geop_i(k)=geop_i(k-1)*factor
        end do
        temp=factor**NPAR2_GEOP
        k=NPAR2_GEOP
        do
            if (k >= n) exit
            k2=k+k
            geop_i(k+1:min(k2,n))=temp*geop_i(1:min(k,n-k))
            temp=temp*temp
            k=k2
        end do
    end if
    END FUNCTION geop_i
!BL
    FUNCTION geop_c(first,factor,n)
    COMPLEX(SP), INTENT(IN) :: first,factor
    INTEGER(I4B), INTENT(IN) :: n
    COMPLEX(SP), DIMENSION(n) :: geop_c
    INTEGER(I4B) :: k,k2
    COMPLEX(SP) :: temp
    if (n > 0) geop_c(1)=first
    if (n <= NPAR_GEOP) then
        do k=2,n
            geop_c(k)=geop_c(k-1)*factor
        end do
    else
        do k=2,NPAR2_GEOP
            geop_c(k)=geop_c(k-1)*factor
        end do
        temp=factor**NPAR2_GEOP
        k=NPAR2_GEOP
        do
            if (k >= n) exit
            k2=k+k
            geop_c(k+1:min(k2,n))=temp*geop_c(1:min(k,n-k))
            temp=temp*temp
            k=k2
        end do
    end if
    END FUNCTION geop_c
!BL
    FUNCTION geop_dv(first,factor,n)
    REAL(DP), DIMENSION(:), INTENT(IN) :: first,factor
    INTEGER(I4B), INTENT(IN) :: n
    REAL(DP), DIMENSION(size(first),n) :: geop_dv
    INTEGER(I4B) :: k,k2
    REAL(DP), DIMENSION(size(first)) :: temp
    if (n > 0) geop_dv(:,1)=first(:)
    if (n <= NPAR_GEOP) then
        do k=2,n
            geop_dv(:,k)=geop_dv(:,k-1)*factor(:)
        end do
    else
        do k=2,NPAR2_GEOP
            geop_dv(:,k)=geop_dv(:,k-1)*factor(:)
        end do
        temp=factor**NPAR2_GEOP
        k=NPAR2_GEOP
        do
            if (k >= n) exit
            k2=k+k
            geop_dv(:,k+1:min(k2,n))=geop_dv(:,1:min(k,n-k))*&
                spread(temp,2,size(geop_dv(:,1:min(k,n-k)),2))
            temp=temp*temp
            k=k2
        end do
    end if
    END FUNCTION geop_dv
!BL
!BL
    RECURSIVE FUNCTION cumsum_r(arr,seed) RESULT(ans)
    REAL(SP), DIMENSION(:), INTENT(IN) :: arr
    REAL(SP), OPTIONAL, INTENT(IN) :: seed
    REAL(SP), DIMENSION(size(arr)) :: ans
    INTEGER(I4B) :: n,j
    REAL(SP) :: sd
    n=size(arr)
    if (n == 0_i4b) RETURN
    sd=0.0_sp
    if (present(seed)) sd=seed
    ans(1)=arr(1)+sd
    if (n < NPAR_CUMSUM) then
        do j=2,n
            ans(j)=ans(j-1)+arr(j)
        end do
    else
        ans(2:n:2)=cumsum_r(arr(2:n:2)+arr(1:n-1:2),sd)
        ans(3:n:2)=ans(2:n-1:2)+arr(3:n:2)
    end if
    END FUNCTION cumsum_r
!BL
    RECURSIVE FUNCTION cumsum_i(arr,seed) RESULT(ans)
    INTEGER(I4B), DIMENSION(:), INTENT(IN) :: arr
    INTEGER(I4B), OPTIONAL, INTENT(IN) :: seed
    INTEGER(I4B), DIMENSION(size(arr)) :: ans
    INTEGER(I4B) :: n,j,sd
    n=size(arr)
    if (n == 0_i4b) RETURN
    sd=0_i4b
    if (present(seed)) sd=seed
    ans(1)=arr(1)+sd
    if (n < NPAR_CUMSUM) then
        do j=2,n
            ans(j)=ans(j-1)+arr(j)
        end do
    else
        ans(2:n:2)=cumsum_i(arr(2:n:2)+arr(1:n-1:2),sd)
        ans(3:n:2)=ans(2:n-1:2)+arr(3:n:2)
    end if
    END FUNCTION cumsum_i
!BL
!BL
    RECURSIVE FUNCTION cumprod(arr,seed) RESULT(ans)
    REAL(SP), DIMENSION(:), INTENT(IN) :: arr
    REAL(SP), OPTIONAL, INTENT(IN) :: seed
    REAL(SP), DIMENSION(size(arr)) :: ans
    INTEGER(I4B) :: n,j
    REAL(SP) :: sd
    n=size(arr)
    if (n == 0_i4b) RETURN
    sd=1.0_sp
    if (present(seed)) sd=seed
    ans(1)=arr(1)*sd
    if (n < NPAR_CUMPROD) then
        do j=2,n
            ans(j)=ans(j-1)*arr(j)
        end do
    else
        ans(2:n:2)=cumprod(arr(2:n:2)*arr(1:n-1:2),sd)
        ans(3:n:2)=ans(2:n-1:2)*arr(3:n:2)
    end if
    END FUNCTION cumprod
!BL
!BL
    FUNCTION poly_rr(x,coeffs)
    REAL(SP), INTENT(IN) :: x
    REAL(SP), DIMENSION(:), INTENT(IN) :: coeffs
    REAL(SP) :: poly_rr
    REAL(SP) :: pow
    REAL(SP), DIMENSION(:), ALLOCATABLE :: vec
    INTEGER(I4B) :: i,n,nn
    n=size(coeffs)
    if (n <= 0) then
        poly_rr=0.0_sp
    else if (n < NPAR_POLY) then
        poly_rr=coeffs(n)
        do i=n-1,1,-1
            poly_rr=x*poly_rr+coeffs(i)
        end do
    else
        allocate(vec(n+1))
        pow=x
        vec(1:n)=coeffs
        do
            vec(n+1)=0.0_sp
            nn=ishft(n+1,-1)
            vec(1:nn)=vec(1:n:2)+pow*vec(2:n+1:2)
            if (nn == 1) exit
            pow=pow*pow
            n=nn
        end do
        poly_rr=vec(1)
        deallocate(vec)
    end if
    END FUNCTION poly_rr
!BL
    FUNCTION poly_dd(x,coeffs)
    REAL(DP), INTENT(IN) :: x
    REAL(DP), DIMENSION(:), INTENT(IN) :: coeffs
    REAL(DP) :: poly_dd
    REAL(DP) :: pow
    REAL(DP), DIMENSION(:), ALLOCATABLE :: vec
    INTEGER(I4B) :: i,n,nn
    n=size(coeffs)
    if (n <= 0) then
        poly_dd=0.0_dp
    else if (n < NPAR_POLY) then
        poly_dd=coeffs(n)
        do i=n-1,1,-1
            poly_dd=x*poly_dd+coeffs(i)
        end do
    else
        allocate(vec(n+1))
        pow=x
        vec(1:n)=coeffs
        do
            vec(n+1)=0.0_dp
            nn=ishft(n+1,-1)
            vec(1:nn)=vec(1:n:2)+pow*vec(2:n+1:2)
            if (nn == 1) exit
            pow=pow*pow
            n=nn
        end do
        poly_dd=vec(1)
        deallocate(vec)
    end if
    END FUNCTION poly_dd
!BL
    FUNCTION poly_rc(x,coeffs)
    COMPLEX(SPC), INTENT(IN) :: x
    REAL(SP), DIMENSION(:), INTENT(IN) :: coeffs
    COMPLEX(SPC) :: poly_rc
    COMPLEX(SPC) :: pow
    COMPLEX(SPC), DIMENSION(:), ALLOCATABLE :: vec
    INTEGER(I4B) :: i,n,nn
    n=size(coeffs)
    if (n <= 0) then
        poly_rc=0.0_sp
    else if (n < NPAR_POLY) then
        poly_rc=coeffs(n)
        do i=n-1,1,-1
            poly_rc=x*poly_rc+coeffs(i)
        end do
    else
        allocate(vec(n+1))
        pow=x
        vec(1:n)=coeffs
        do
            vec(n+1)=0.0_sp
            nn=ishft(n+1,-1)
            vec(1:nn)=vec(1:n:2)+pow*vec(2:n+1:2)
            if (nn == 1) exit
            pow=pow*pow
            n=nn
        end do
        poly_rc=vec(1)
        deallocate(vec)
    end if
    END FUNCTION poly_rc
!BL
    FUNCTION poly_cc(x,coeffs)
    COMPLEX(SPC), INTENT(IN) :: x
    COMPLEX(SPC), DIMENSION(:), INTENT(IN) :: coeffs
    COMPLEX(SPC) :: poly_cc
    COMPLEX(SPC) :: pow
    COMPLEX(SPC), DIMENSION(:), ALLOCATABLE :: vec
    INTEGER(I4B) :: i,n,nn
    n=size(coeffs)
    if (n <= 0) then
        poly_cc=0.0_sp
    else if (n < NPAR_POLY) then
        poly_cc=coeffs(n)
        do i=n-1,1,-1
            poly_cc=x*poly_cc+coeffs(i)
        end do
    else
        allocate(vec(n+1))
        pow=x
        vec(1:n)=coeffs
        do
            vec(n+1)=0.0_sp
            nn=ishft(n+1,-1)
            vec(1:nn)=vec(1:n:2)+pow*vec(2:n+1:2)
            if (nn == 1) exit
            pow=pow*pow
            n=nn
        end do
        poly_cc=vec(1)
        deallocate(vec)
    end if
    END FUNCTION poly_cc
!BL
    FUNCTION poly_rrv(x,coeffs)
    REAL(SP), DIMENSION(:), INTENT(IN) :: coeffs,x
    REAL(SP), DIMENSION(size(x)) :: poly_rrv
    INTEGER(I4B) :: i,n,m
    m=size(coeffs)
    n=size(x)
    if (m <= 0) then
        poly_rrv=0.0_sp
    else if (m < n .or. m < NPAR_POLY) then
        poly_rrv=coeffs(m)
        do i=m-1,1,-1
            poly_rrv=x*poly_rrv+coeffs(i)
        end do
    else
        do i=1,n
            poly_rrv(i)=poly_rr(x(i),coeffs)
        end do
    end if
    END FUNCTION poly_rrv
!BL
    FUNCTION poly_ddv(x,coeffs)
    REAL(DP), DIMENSION(:), INTENT(IN) :: coeffs,x
    REAL(DP), DIMENSION(size(x)) :: poly_ddv
    INTEGER(I4B) :: i,n,m
    m=size(coeffs)
    n=size(x)
    if (m <= 0) then
        poly_ddv=0.0_dp
    else if (m < n .or. m < NPAR_POLY) then
        poly_ddv=coeffs(m)
        do i=m-1,1,-1
            poly_ddv=x*poly_ddv+coeffs(i)
        end do
    else
        do i=1,n
            poly_ddv(i)=poly_dd(x(i),coeffs)
        end do
    end if
    END FUNCTION poly_ddv
!BL
    FUNCTION poly_msk_rrv(x,coeffs,mask)
    REAL(SP), DIMENSION(:), INTENT(IN) :: coeffs,x
    LOGICAL(LGT), DIMENSION(:), INTENT(IN) :: mask
    REAL(SP), DIMENSION(size(x)) :: poly_msk_rrv
    poly_msk_rrv=unpack(poly_rrv(pack(x,mask),coeffs),mask,0.0_sp)
    END FUNCTION poly_msk_rrv
!BL
    FUNCTION poly_msk_ddv(x,coeffs,mask)
    REAL(DP), DIMENSION(:), INTENT(IN) :: coeffs,x
    LOGICAL(LGT), DIMENSION(:), INTENT(IN) :: mask
    REAL(DP), DIMENSION(size(x)) :: poly_msk_ddv
    poly_msk_ddv=unpack(poly_ddv(pack(x,mask),coeffs),mask,0.0_dp)
    END FUNCTION poly_msk_ddv
!BL
!BL
    RECURSIVE FUNCTION poly_term_rr(a,b) RESULT(u)
    REAL(SP), DIMENSION(:), INTENT(IN) :: a
    REAL(SP), INTENT(IN) :: b
    REAL(SP), DIMENSION(size(a)) :: u
    INTEGER(I4B) :: n,j
    n=size(a)
    if (n <= 0) RETURN
    u(1)=a(1)
    if (n < NPAR_POLYTERM) then
        do j=2,n
            u(j)=a(j)+b*u(j-1)
        end do
    else
        u(2:n:2)=poly_term_rr(a(2:n:2)+a(1:n-1:2)*b,b*b)
        u(3:n:2)=a(3:n:2)+b*u(2:n-1:2)
    end if
    END FUNCTION poly_term_rr
!BL
    RECURSIVE FUNCTION poly_term_cc(a,b) RESULT(u)
    COMPLEX(SPC), DIMENSION(:), INTENT(IN) :: a
    COMPLEX(SPC), INTENT(IN) :: b
    COMPLEX(SPC), DIMENSION(size(a)) :: u
    INTEGER(I4B) :: n,j
    n=size(a)
    if (n <= 0) RETURN
    u(1)=a(1)
    if (n < NPAR_POLYTERM) then
        do j=2,n
            u(j)=a(j)+b*u(j-1)
        end do
    else
        u(2:n:2)=poly_term_cc(a(2:n:2)+a(1:n-1:2)*b,b*b)
        u(3:n:2)=a(3:n:2)+b*u(2:n-1:2)
    end if
    END FUNCTION poly_term_cc
!BL
!BL
    FUNCTION zroots_unity(n,nn)
    INTEGER(I4B), INTENT(IN) :: n,nn
    COMPLEX(SPC), DIMENSION(nn) :: zroots_unity
    INTEGER(I4B) :: k
    REAL(SP) :: theta
    zroots_unity(1)=1.0
    theta=TWOPI/n
    k=1
    do
        if (k >= nn) exit
        zroots_unity(k+1)=cmplx(cos(k*theta),sin(k*theta),SPC)
        zroots_unity(k+2:min(2*k,nn))=zroots_unity(k+1)*&
            zroots_unity(2:min(k,nn-k))
        k=2*k
    end do
    END FUNCTION zroots_unity
!BL
    FUNCTION outerprod_r(a,b)
    REAL(SP), DIMENSION(:), INTENT(IN) :: a,b
    REAL(SP), DIMENSION(size(a),size(b)) :: outerprod_r
    outerprod_r = spread(a,dim=2,ncopies=size(b)) * &
        spread(b,dim=1,ncopies=size(a))
    END FUNCTION outerprod_r
!BL
    FUNCTION outerprod_d(a,b)
    REAL(DP), DIMENSION(:), INTENT(IN) :: a,b
    REAL(DP), DIMENSION(size(a),size(b)) :: outerprod_d
    outerprod_d = spread(a,dim=2,ncopies=size(b)) * &
        spread(b,dim=1,ncopies=size(a))
    END FUNCTION outerprod_d
!BL
    FUNCTION outerdiv(a,b)
    REAL(SP), DIMENSION(:), INTENT(IN) :: a,b
    REAL(SP), DIMENSION(size(a),size(b)) :: outerdiv
    outerdiv = spread(a,dim=2,ncopies=size(b)) / &
        spread(b,dim=1,ncopies=size(a))
    END FUNCTION outerdiv
!BL
    FUNCTION outersum(a,b)
    REAL(SP), DIMENSION(:), INTENT(IN) :: a,b
    REAL(SP), DIMENSION(size(a),size(b)) :: outersum
    outersum = spread(a,dim=2,ncopies=size(b)) + &
        spread(b,dim=1,ncopies=size(a))
    END FUNCTION outersum
!BL
    FUNCTION outerdiff_r(a,b)
    REAL(SP), DIMENSION(:), INTENT(IN) :: a,b
    REAL(SP), DIMENSION(size(a),size(b)) :: outerdiff_r
    outerdiff_r = spread(a,dim=2,ncopies=size(b)) - &
        spread(b,dim=1,ncopies=size(a))
    END FUNCTION outerdiff_r
!BL
    FUNCTION outerdiff_d(a,b)
    REAL(DP), DIMENSION(:), INTENT(IN) :: a,b
    REAL(DP), DIMENSION(size(a),size(b)) :: outerdiff_d
    outerdiff_d = spread(a,dim=2,ncopies=size(b)) - &
        spread(b,dim=1,ncopies=size(a))
    END FUNCTION outerdiff_d
!BL
    FUNCTION outerdiff_i(a,b)
    INTEGER(I4B), DIMENSION(:), INTENT(IN) :: a,b
    INTEGER(I4B), DIMENSION(size(a),size(b)) :: outerdiff_i
    outerdiff_i = spread(a,dim=2,ncopies=size(b)) - &
        spread(b,dim=1,ncopies=size(a))
    END FUNCTION outerdiff_i
!BL
    FUNCTION outerand(a,b)
    LOGICAL(LGT), DIMENSION(:), INTENT(IN) :: a,b
    LOGICAL(LGT), DIMENSION(size(a),size(b)) :: outerand
    outerand = spread(a,dim=2,ncopies=size(b)) .and. &
        spread(b,dim=1,ncopies=size(a))
    END FUNCTION outerand
!BL
    SUBROUTINE scatter_add_r(dest,source,dest_index)
    REAL(SP), DIMENSION(:), INTENT(OUT) :: dest
    REAL(SP), DIMENSION(:), INTENT(IN) :: source
    INTEGER(I4B), DIMENSION(:), INTENT(IN) :: dest_index
    INTEGER(I4B) :: m,n,j,i
    n=assert_eq2(size(source),size(dest_index),'scatter_add_r')
    m=size(dest)
    do j=1,n
        i=dest_index(j)
        if (i > 0 .and. i <= m) dest(i)=dest(i)+source(j)
    end do
    END SUBROUTINE scatter_add_r
    SUBROUTINE scatter_add_d(dest,source,dest_index)
    REAL(DP), DIMENSION(:), INTENT(OUT) :: dest
    REAL(DP), DIMENSION(:), INTENT(IN) :: source
    INTEGER(I4B), DIMENSION(:), INTENT(IN) :: dest_index
    INTEGER(I4B) :: m,n,j,i
    n=assert_eq2(size(source),size(dest_index),'scatter_add_d')
    m=size(dest)
    do j=1,n
        i=dest_index(j)
        if (i > 0 .and. i <= m) dest(i)=dest(i)+source(j)
    end do
    END SUBROUTINE scatter_add_d
    SUBROUTINE scatter_max_r(dest,source,dest_index)
    REAL(SP), DIMENSION(:), INTENT(OUT) :: dest
    REAL(SP), DIMENSION(:), INTENT(IN) :: source
    INTEGER(I4B), DIMENSION(:), INTENT(IN) :: dest_index
    INTEGER(I4B) :: m,n,j,i
    n=assert_eq2(size(source),size(dest_index),'scatter_max_r')
    m=size(dest)
    do j=1,n
        i=dest_index(j)
        if (i > 0 .and. i <= m) dest(i)=max(dest(i),source(j))
    end do
    END SUBROUTINE scatter_max_r
    SUBROUTINE scatter_max_d(dest,source,dest_index)
    REAL(DP), DIMENSION(:), INTENT(OUT) :: dest
    REAL(DP), DIMENSION(:), INTENT(IN) :: source
    INTEGER(I4B), DIMENSION(:), INTENT(IN) :: dest_index
    INTEGER(I4B) :: m,n,j,i
    n=assert_eq2(size(source),size(dest_index),'scatter_max_d')
    m=size(dest)
    do j=1,n
        i=dest_index(j)
        if (i > 0 .and. i <= m) dest(i)=max(dest(i),source(j))
    end do
    END SUBROUTINE scatter_max_d
!BL
    SUBROUTINE diagadd_rv(mat,diag)
    REAL(SP), DIMENSION(:,:), INTENT(INOUT) :: mat
    REAL(SP), DIMENSION(:), INTENT(IN) :: diag
    INTEGER(I4B) :: j,n
    n = assert_eq2(size(diag),min(size(mat,1),size(mat,2)),'diagadd_rv')
    do j=1,n
        mat(j,j)=mat(j,j)+diag(j)
    end do
    END SUBROUTINE diagadd_rv
!BL
    SUBROUTINE diagadd_r(mat,diag)
    REAL(SP), DIMENSION(:,:), INTENT(INOUT) :: mat
    REAL(SP), INTENT(IN) :: diag
    INTEGER(I4B) :: j,n
    n = min(size(mat,1),size(mat,2))
    do j=1,n
        mat(j,j)=mat(j,j)+diag
    end do
    END SUBROUTINE diagadd_r
!BL
    SUBROUTINE diagmult_rv(mat,diag)
    REAL(SP), DIMENSION(:,:), INTENT(INOUT) :: mat
    REAL(SP), DIMENSION(:), INTENT(IN) :: diag
    INTEGER(I4B) :: j,n
    n = assert_eq2(size(diag),min(size(mat,1),size(mat,2)),'diagmult_rv')
    do j=1,n
        mat(j,j)=mat(j,j)*diag(j)
    end do
    END SUBROUTINE diagmult_rv
!BL
    SUBROUTINE diagmult_r(mat,diag)
    REAL(SP), DIMENSION(:,:), INTENT(INOUT) :: mat
    REAL(SP), INTENT(IN) :: diag
    INTEGER(I4B) :: j,n
    n = min(size(mat,1),size(mat,2))
    do j=1,n
        mat(j,j)=mat(j,j)*diag
    end do
    END SUBROUTINE diagmult_r
!BL
    FUNCTION get_diag_rv(mat)
    REAL(SP), DIMENSION(:,:), INTENT(IN) :: mat
    REAL(SP), DIMENSION(size(mat,1)) :: get_diag_rv
    INTEGER(I4B) :: j
    j=assert_eq2(size(mat,1),size(mat,2),'get_diag_rv')
    do j=1,size(mat,1)
        get_diag_rv(j)=mat(j,j)
    end do
    END FUNCTION get_diag_rv
!BL
    FUNCTION get_diag_dv(mat)
    REAL(DP), DIMENSION(:,:), INTENT(IN) :: mat
    REAL(DP), DIMENSION(size(mat,1)) :: get_diag_dv
    INTEGER(I4B) :: j
    j=assert_eq2(size(mat,1),size(mat,2),'get_diag_dv')
    do j=1,size(mat,1)
        get_diag_dv(j)=mat(j,j)
    end do
    END FUNCTION get_diag_dv
!BL
    SUBROUTINE put_diag_rv(diagv,mat)
    REAL(SP), DIMENSION(:), INTENT(IN) :: diagv
    REAL(SP), DIMENSION(:,:), INTENT(INOUT) :: mat
    INTEGER(I4B) :: j,n
    n=assert_eq2(size(diagv),min(size(mat,1),size(mat,2)),'put_diag_rv')
    do j=1,n
        mat(j,j)=diagv(j)
    end do
    END SUBROUTINE put_diag_rv
!BL
    SUBROUTINE put_diag_r(scal,mat)
    REAL(SP), INTENT(IN) :: scal
    REAL(SP), DIMENSION(:,:), INTENT(INOUT) :: mat
    INTEGER(I4B) :: j,n
    n = min(size(mat,1),size(mat,2))
    do j=1,n
        mat(j,j)=scal
    end do
    END SUBROUTINE put_diag_r
!BL
    SUBROUTINE unit_matrix(mat)
    REAL(SP), DIMENSION(:,:), INTENT(OUT) :: mat
    INTEGER(I4B) :: i,n
    n=min(size(mat,1),size(mat,2))
    mat(:,:)=0.0_sp
    do i=1,n
        mat(i,i)=1.0_sp
    end do
    END SUBROUTINE unit_matrix
!BL
    FUNCTION vabs(v)
    REAL(SP), DIMENSION(:), INTENT(IN) :: v
    REAL(SP) :: vabs
    vabs=sqrt(dot_product(v,v))
    END FUNCTION vabs

END MODULE nrutil

MODULE NewGammaCRAB
CONTAINS
!>This routine computes the set of updated control parameters
!>If the optional argument Update=.T. is given, then Gamma and g are updated. 
!>If the optional argument Update=.T. is not given, then only g is updated. 
    SUBROUTINE Get_NewGammaCRAB(Update)

    USE CRAB_Parameters
    USE Global_Parameters, ONLY: Time_Begin,Time_Final,OCT_CRAB_Tolerance,PI,OCT_TimeSteps,OCT_NGoals,OCT_NC, &
                                 OCT_UpperBounds, OCT_LowerBounds,dCRAB,do_fejer 
    USE Random

    IMPLICIT NONE

    LOGICAL, Optional :: Update

    REAL*8 :: dt_OCT
    REAL*8 :: OCT_NewGamma(OCT_TimeSteps,OCT_Ngoals)

    REAL*8 :: Mask,Mask_norm,Mask_type

    INTEGER :: I,J,K,SEED

    dt_OCT=(Time_Final-Time_Begin)/OCT_TimeSteps

    Write(6,*) "Global Pars", Time_Final,Time_Begin,OCT_TimeSteps,OCT_NGoals,OCT_NC
    Write(6,*) "As,Bs,ws",OCT_As,OCT_Bs,OCT_Omega,dt_OCT

    
    DO K=1,OCT_NGoals
       DO J=1,OCT_NC

       END DO
    END DO

    IF (CRAB_CONTINUE.eqv..TRUE.) THEN
     IF (DO_Fejer.eqv..TRUE.) THEN
!>----Masked Ferje Series-------------------------------------------------------------
      do i=1,OCT_TimeSteps
         DO K=1,OCT_NGoals
           Mask_norm=1.d0
           Mask_type=1.d0

           Mask=Mask_norm*tanh(Mask_Type*((i-1)*dt_OCT-Time_Begin))&
                         *tanh(Mask_Type*(Time_Final-(i-1)*dt_OCT))&
                         /tanh(Mask_Type*(0.5d0*(Time_Final-Time_Begin)-Time_Begin))&
                         /tanh(Mask_Type*(Time_Final-0.5d0*(Time_Final-Time_Begin)))
             OCT_g(I,K)=1.d0
           do j=1,OCT_NC

             OCT_g(I,K)=OCT_g(I,K)+(OCT_As(j,K)*Sin(OCT_Omega(j,K)*(i-1)*dt_OCT) &
                        +OCT_Bs(j,K)*Cos(OCT_Omega(j,K)*(i-1)*dt_OCT)) & 
                        *Mask*(OCT_NC+1.d0-j)
           enddo
         enddo
      end do
     ELSE
!>----Masked Fourier Series-----------------------------------------------------------

      do i=1,OCT_TimeSteps
  
         DO K=1,OCT_NGoals
           Mask_norm=1.d0
           Mask_type=1.d0
  
           Mask=Mask_norm*tanh(Mask_Type*((i-1)*dt_OCT-Time_Begin))&
                         *tanh(Mask_Type*(Time_Final-(i-1)*dt_OCT))&
                         /tanh(Mask_Type*(0.5d0*(Time_Final-Time_Begin)-Time_Begin))&
                         /tanh(Mask_Type*(Time_Final-0.5d0*(Time_Final-Time_Begin)))
  
           OCT_g(I,K)=((1.d0+SUM(OCT_As(:,K)*Sin(OCT_Omega(:,K)*(i-1)*dt_OCT) &
                         +OCT_Bs(:,K)*Cos(OCT_Omega(:,K)*(i-1)*dt_OCT))) & 
                         *Mask)

         enddo

 
      end do
     ENDIF

      if (dCRAB.eqv..FALSE.) THEN
        DO I=1,OCT_TimeSteps
           DO K=1,OCT_NGoals
             OCT_CurrentGamma(I,K)=OCT_Gamma(I,K)*Oct_g(I,K)
           enddo
!           Write(1007,*) i,OCT_g(I,1),OCT_Gamma(I,1),OCT_CurrentGamma(I,1)
        end do
      Elseif (dCRAB.eqv..TRUE.) THEN
        DO I=1,OCT_TimeSteps

           DO K=1,OCT_NGoals
             OCT_CurrentGamma(I,K)=OCT_Gamma(I,K)+Oct_g(I,K)

             IF (OCT_CurrentGamma(I,K).ge.OCT_UpperBounds(K)) THEN
               OCT_CurrentGamma(I,K)=OCT_UpperBounds(K)
             ENDIF   

             IF (OCT_CurrentGamma(I,K).le.OCT_LowerBounds(K)) THEN
               OCT_CurrentGamma(I,K)=OCT_LowerBounds(K)
             ENDIF   

           END DO

        end do
      ENDIF
    ENDIF

    IF (Present(Update).and.Update.eqv..TRUE.) THEN

      if (dCRAB.eqv..FALSE.) THEN
        do i=1,OCT_TimeSteps
           DO K=1,OCT_NGoals
              OCT_NewGamma(I,K)=OCT_Gamma(I,K)*OCT_g(I,K)
           enddo
        end do
      Elseif (dCRAB.eqv..TRUE.) THEN
        do i=1,OCT_TimeSteps
           DO K=1,OCT_NGoals

             OCT_NewGamma(I,K)=OCT_Gamma(I,K)+OCT_g(I,K)

             IF (OCT_NewGamma(I,K).ge.OCT_UpperBounds(K)) THEN
               OCT_NewGamma(I,K)=OCT_UpperBounds(K)
             ENDIF   
             IF (OCT_CurrentGamma(I,K).le.OCT_LowerBounds(K)) THEN
               OCT_NewGamma(I,K)=OCT_LowerBounds(K)
             ENDIF   

           enddo
        end do
      ENDIF
  
      IF (SUM(ABS(OCT_Gamma(:,:)-OCT_NewGamma(:,:))).lt.OCT_CRAB_Tolerance) THEN
         BREAK_CG=.TRUE.
         OCT_Gamma=OCT_CurrentGamma
         RETURN
      ENDIF

      IF (CRAB_CONTINUE.eqv..TRUE.) THEN
        OCT_Gamma=OCT_NewGamma
        DO K=1,OCT_NC
          DO J=1,OCT_NGoals    
 
!          CALL GNU_Rand4(OCT_Rand4)
!          OCT_As(K,J)=OCT_Rand4


          CALL GNU_Rand(OCT_Rand8)

          OCT_Omega(K,J)=2.d0*K*PI*(1+(OCT_Rand8-0.5d0))  &
                         /(time_final-time_begin)    

!          CALL GNU_Rand4(OCT_Rand4)
!          OCT_Bs(K,J)=OCT_Rand4
 
          END DO
        END DO 
      ELSEIF(CRAB_CONTINUE.eqv..FALSE.) THEN 
         OCT_Gamma=OCT_CurrentGamma
      ENDIF

    ENDIF

    End Subroutine Get_NewGammaCRAB

END MODULE NewGammaCRAB




!-----------------------------------------------------------------------
MODULE nr
    INTERFACE
        FUNCTION brent(ax,bx,cx,func_OCT,tol,xmin)
        USE nrtype
        USE NewGammaCRAB
        
        REAL(SP), INTENT(IN) :: ax,bx,cx,tol
        REAL(SP), INTENT(OUT) :: xmin
        REAL(SP) :: brent
        INTERFACE
            FUNCTION func_OCT(x)
            USE nrtype
            REAL(SP), INTENT(IN) :: x
            REAL(SP) :: func_OCT
            END FUNCTION func_OCT
        END INTERFACE
        END FUNCTION brent
    END INTERFACE
    INTERFACE
        FUNCTION dbrent(ax,bx,cx,func_OCT,dbrent_dfunc,tol,xmin)
        USE nrtype
        USE NewGammaCRAB
        REAL(SP), INTENT(IN) :: ax,bx,cx,tol
        REAL(SP), INTENT(OUT) :: xmin
        REAL(SP) :: dbrent
        INTERFACE
            FUNCTION func_OCT(x)
            USE nrtype
            REAL(SP), INTENT(IN) :: x
            REAL(SP) :: func_OCT
            END FUNCTION func_OCT
!BL
            FUNCTION dbrent_dfunc(x)
            USE nrtype
            REAL(SP), INTENT(IN) :: x
            REAL(SP) :: dbrent_dfunc
            END FUNCTION dbrent_dfunc
        END INTERFACE
        END FUNCTION dbrent
    END INTERFACE
    INTERFACE
        SUBROUTINE dfpmin(p,gtol,iter,fret,func_OCT,dfunc)
        USE nrtype
        USE NewGammaCRAB
        INTEGER(I4B), INTENT(OUT) :: iter
        REAL(SP), INTENT(IN) :: gtol
        REAL(SP), INTENT(OUT) :: fret
        REAL(SP), DIMENSION(:), INTENT(INOUT) :: p
        INTERFACE
            FUNCTION func_OCT(p)
            USE nrtype
            USE Global_Parameters,ONLY:OCT_NGoals,OCT_NC
            REAL(SP), DIMENSION(2*OCT_NGoals*OCT_NC), INTENT(IN) :: p
            REAL(SP) :: func_OCT
            END FUNCTION func_OCT
!BL
            FUNCTION dfunc(p)
            USE nrtype
            USE Global_Parameters,ONLY:OCT_NGoals,OCT_NC
            REAL(SP), DIMENSION(2*OCT_NGoals*OCT_NC), INTENT(IN) :: p
            REAL(SP), DIMENSION(size(p)) :: dfunc
            END FUNCTION dfunc
        END INTERFACE
        END SUBROUTINE dfpmin
    END INTERFACE
    INTERFACE
        SUBROUTINE Functional_FCRAB_Minimization(OCT_As,OCT_Bs,ftol,iter,fret)
        USE nrtype
        USE Global_Parameters,ONLY:OCT_NGoals,OCT_NC
        INTEGER(I4B), INTENT(OUT) :: iter
        REAL(SP), INTENT(IN) :: ftol
        REAL(SP), INTENT(OUT) :: fret
        REAL, DIMENSION(OCT_NC,OCT_NGoals):: OCT_As,OCT_Bs
        END SUBROUTINE Functional_FCRAB_Minimization 
    END INTERFACE
    INTERFACE
        SUBROUTINE linmin(p,xi,fret)
        USE nrtype
        REAL(SP), INTENT(OUT) :: fret
        REAL(SP), DIMENSION(:), TARGET, INTENT(INOUT) :: p,xi
        END SUBROUTINE linmin
    END INTERFACE
    INTERFACE
        SUBROUTINE mnbrak(ax,bx,cx,fa,fb,fc,func_OCT)
        USE nrtype
        USE NewGammaCRAB
        REAL(SP), INTENT(INOUT) :: ax,bx
        REAL(SP), INTENT(OUT) :: cx,fa,fb,fc
        INTERFACE
            FUNCTION func_OCT(x)
            USE nrtype
            REAL(SP), INTENT(IN) :: x
            REAL(SP) :: func_OCT
            END FUNCTION func_OCT
        END INTERFACE
        END SUBROUTINE mnbrak
    END INTERFACE
    INTERFACE
        SUBROUTINE lnsrch(xold,fold,g,p,x,f,stpmax,check,func)
        USE nrtype
        REAL(SP), DIMENSION(:), INTENT(IN) :: xold,g
        REAL(SP), DIMENSION(:), INTENT(INOUT) :: p
        REAL(SP), INTENT(IN) :: fold,stpmax
        REAL(SP), DIMENSION(:), INTENT(OUT) :: x
        REAL(SP), INTENT(OUT) :: f
        LOGICAL(LGT), INTENT(OUT) :: check
        INTERFACE
            FUNCTION func(x)
            USE nrtype
            USE Global_Parameters,ONLY:OCT_NGoals,OCT_NC
            REAL(SP) :: func
            REAL(SP), DIMENSION(2*OCT_NC*OCT_NGoals), INTENT(IN) :: x
            END FUNCTION func
        END INTERFACE
        END SUBROUTINE lnsrch
END INTERFACE

END MODULE nr


    FUNCTION func_OCT(x)
    USE NewGammaCRAB
    USE NRTYPE
    USE MCTDHX  
    USE CRAB_Parameters  
    USE Global_Parameters,ONLY:OCT_NGoals,OCT_NC,OCT_TimeSteps,Time_Final,Time_Begin
    USE Input_Output

    IMPLICIT NONE
    REAL(SP) :: func_OCT
    REAL(SP), DIMENSION(2*OCT_NGoals*OCT_NC), INTENT(IN) :: x
    INTEGER(I4B) :: i

    INTEGER :: J,K,L
    INTEGER,SAVE :: Count_OCT

    REAL*8 :: dt_OCT

    IF (CRAB_First.eqv..TRUE.) THEN
       COUNT_OCT=1
    ELSE
      COUNT_OCT=COUNT_OCT+1
    ENDIF


    Write(6,*) "In Func_OCT X:", X
    Write(6,*) "In Func_OCT X:", X
    Write(6,*) "In Func_OCT X:", X

    L=1
    DO K=1,OCT_NC
      DO J=1,OCT_NGoals
         OCT_As(K,J)=x(L)
         OCT_Bs(K,J)=x(L+OCT_NGoals*OCT_NC)
         L=L+1
      END DO
    END DO

    Write(6,*) "Parameters in Func_OCT:", x, OCT_As,OCT_Bs

    Call Get_NewGammaCRAB
 
    Call MCTDHX_Routine
!Define here the functional

    func_OCT=0.d0

    dt_OCT=(Time_Final-Time_Begin)/OCT_TimeSteps

    DO K=1,OCT_NGoals
       func_OCT=func_OCT+OCT_Alpha(K)*OCT_Infidelities(K) 
       DO J=1,OCT_TimeSteps-1
          func_OCT=func_OCT+OCT_Beta(K)*0.5d0*dt_OCT*(sqrt(OCT_CurrentGamma(J+1,K)**2)+sqrt(OCT_CurrentGamma(J,K)**2))
       END DO
    END DO

    CALL Write_OCT_PARAMETERS(COUNT_OCT,.FALSE.,REAL(func_OCT,8))

    END FUNCTION func_OCT

!-----------------------------------------------------------------------
    FUNCTION dfunc(x)
    USE nrtype
    USE nr
    USE Global_Parameters,ONLY: OCT_NGoals,OCT_NC
    IMPLICIT NONE
    INTEGER(I4B) :: i,j
    REAL(SP), PARAMETER :: rh=1.0e-6_sp
    REAL(SP), INTENT(IN) :: x(2*OCT_NGoals*OCT_NC)
    REAL(SP), DIMENSION(size(x)) :: xhp
    REAL(SP), DIMENSION(size(x)) :: xhm
    REAL(SP), DIMENSION(size(x)) :: dfunc
    REAL(SP) :: func_OCT_X
        INTERFACE
        FUNCTION func_OCT(p)
        USE nrtype
        USE Global_Parameters,ONLY: OCT_NGoals,OCT_NC
        IMPLICIT NONE
        REAL(SP), INTENT(IN) :: p(2*OCT_NGoals*OCT_NC)
        REAL(SP) :: func_OCT
        END FUNCTION func_OCT
        end interface


    do i=1,OCT_NC*OCT_NGoals*2 !! Is this really the correct loop? 

     do j=1,OCT_NC*OCT_NGoals*2

      if (i.eq.j) Then 
         xhp(j)=x(j)+rh
         xhm(j)=x(j)-rh
      else
         xhp(j)=x(j)
         xhm(j)=x(j)
      end if

     enddo
! Central difference (for backward see older version)

     dfunc(i)=(func_OCT(xhp)-func_OCT(xhm))/(2_sp*rh)

    enddo

    END FUNCTION dfunc




!-----------------------------------------------------------------------
    FUNCTION brent(ax,bx,cx,func_OCT,tol,xmin)
    USE nrtype; USE nrutil, ONLY : nrerror
    IMPLICIT NONE
    REAL(SP), INTENT(IN) :: ax,bx,cx,tol
    REAL(SP), INTENT(OUT) :: xmin
    REAL(SP) :: brent
    REAL(SP) :: func_OCT
    INTEGER(I4B), PARAMETER :: ITMAX=11
    REAL(SP), PARAMETER :: CGOLD=0.3819660_sp,ZEPS=1.0e-3_sp*epsilon(ax)
    INTEGER(I4B) :: iter
    REAL(SP) :: a,b,d,e,etemp,fu,fv,fw,fx,p,q,r,tol1,tol2,u,v,w,x,xm
    a=min(ax,cx)
    b=max(ax,cx)
    v=bx
    w=v
    x=v
    e=0.0
    fx=func_OCT(x)
    fv=fx
    fw=fx
    do iter=1,ITMAX
        xm=0.5_sp*(a+b)
        tol1=tol*abs(x)+ZEPS
        tol2=2.0_sp*tol1
        if (abs(x-xm) <= (tol2-0.5_sp*(b-a))) then
            xmin=x
            brent=fx
            RETURN
        end if
        if (abs(e) > tol1) then
            r=(x-w)*(fx-fv)
            q=(x-v)*(fx-fw)
            p=(x-v)*q-(x-w)*r
            q=2.0_sp*(q-r)
            if (q > 0.0) p=-p
            q=abs(q)
            etemp=e
            e=d
            if (abs(p) >= abs(0.5_sp*q*etemp) .or. &
                p <= q*(a-x) .or. p >= q*(b-x)) then
                e=merge(a-x,b-x, x >= xm )
                d=CGOLD*e
            else
                d=p/q
                u=x+d
                if (u-a < tol2 .or. b-u < tol2) d=sign(tol1,xm-x)
            end if
        else
            e=merge(a-x,b-x, x >= xm )
            d=CGOLD*e
        end if
        u=merge(x+d,x+sign(tol1,d), abs(d) >= tol1 )
        fu=func_OCT(u)
        if (fu <= fx) then
            if (u >= x) then
                a=x
            else
                b=x
            end if
            call shft(v,w,x,u)
            call shft(fv,fw,fx,fu)
        else
            if (u < x) then
                a=u
            else
                b=u
            end if
            if (fu <= fw .or. w == x) then
                v=w
                fv=fw
                w=u
                fw=fu
            else if (fu <= fv .or. v == x .or. v == w) then
                v=u
                fv=fu
            end if
        end if
    end do
    call nrerror('brent: exceed maximum iterations')
    CONTAINS
!BL
    SUBROUTINE shft(a,b,c,d)
    REAL(SP), INTENT(OUT) :: a
    REAL(SP), INTENT(INOUT) :: b,c
    REAL(SP), INTENT(IN) :: d
    a=b
    b=c
    c=d
    END SUBROUTINE shft
    END FUNCTION brent
!-----------------------------------------------------------------------
    SUBROUTINE mnbrak(ax,bx,cx,fa,fb,fc,func_OCT)
    USE nrtype; USE nrutil, ONLY : swap
    IMPLICIT NONE
    REAL(SP), INTENT(INOUT) :: ax,bx
    REAL(SP), INTENT(OUT) :: cx,fa,fb,fc
    REAL(SP) :: func_OCT
    REAL(SP), PARAMETER :: GOLD=1.618034_sp,GLIMIT=100.0_sp,TINY=1.0e-20_sp
    REAL(SP) :: fu,q,r,u,ulim
    fa=func_OCT(ax)
    fb=func_OCT(bx)
    if (fb > fa) then
        call swap(ax,bx)
        call swap(fa,fb)
    end if
    cx=bx+GOLD*(bx-ax)
    fc=func_OCT(cx)
    do
        if (fb < fc) RETURN
        r=(bx-ax)*(fb-fc)
        q=(bx-cx)*(fb-fa)
        u=bx-((bx-cx)*q-(bx-ax)*r)/(2.0_sp*sign(max(abs(q-r),TINY),q-r))
        ulim=bx+GLIMIT*(cx-bx)
        if ((bx-u)*(u-cx) > 0.0) then
            fu=func_OCT(u)
            if (fu < fc) then
                ax=bx
                fa=fb
                bx=u
                fb=fu
                RETURN
            else if (fu > fb) then
                cx=u
                fc=fu
                RETURN
            end if
            u=cx+GOLD*(cx-bx)
            fu=func_OCT(u)
        else if ((cx-u)*(u-ulim) > 0.0) then
            fu=func_OCT(u)
            if (fu < fc) then
                bx=cx
                cx=u
                u=cx+GOLD*(cx-bx)
                call shft(fb,fc,fu,func_OCT(u))
            end if
        else if ((u-ulim)*(ulim-cx) >= 0.0) then
            u=ulim
            fu=func_OCT(u)
        else
            u=cx+GOLD*(cx-bx)
            fu=func_OCT(u)
        end if
        call shft(ax,bx,cx,u)
        call shft(fa,fb,fc,fu)
    end do
    CONTAINS
!BL
    SUBROUTINE shft(a,b,c,d)
    REAL(SP), INTENT(OUT) :: a
    REAL(SP), INTENT(INOUT) :: b,c
    REAL(SP), INTENT(IN) :: d
    a=b
    b=c
    c=d
    END SUBROUTINE shft
    END SUBROUTINE mnbrak
!-----------------------------------------------------------------------
MODULE f1dim_mod
    USE nrtype
    INTEGER(I4B) :: ncom
    REAL(SP), DIMENSION(:), POINTER :: pcom,xicom
CONTAINS
!BL
    FUNCTION f1dim(x)
!    USE NewGammaCRAB
    USE Global_Parameters,ONLY: OCT_NGoals,OCT_NC
    IMPLICIT NONE
    REAL(SP), INTENT(IN) :: x
    REAL(SP) :: f1dim
    INTERFACE
        FUNCTION func_OCT(x)
        USE nrtype
        USE Global_Parameters,ONLY: OCT_NGoals,OCT_NC
        REAL(SP), INTENT(IN) :: x(2*OCT_NC*OCT_NGoals)
        REAL(SP) :: func_OCT
        END FUNCTION func_OCT
    END INTERFACE
    REAL(SP), DIMENSION(:), ALLOCATABLE :: xt
    allocate(xt(ncom))
    xt(:)=pcom(:)+x*xicom(:)
    f1dim=func_OCT(xt)
    deallocate(xt)
    END FUNCTION f1dim
END MODULE f1dim_mod

    SUBROUTINE linmin(p,xi,fret)
    USE nrtype; USE nrutil, ONLY : assert_eq
    USE nr, ONLY : mnbrak,brent
    USE f1dim_mod

    IMPLICIT NONE

    REAL(SP), INTENT(OUT) :: fret
    REAL(SP), DIMENSION(:), TARGET, INTENT(INOUT) :: p,xi
    REAL(SP), PARAMETER :: TOL=1.0e-2_sp
    REAL(SP) :: ax,bx,fa,fb,fx,xmin,xx

    ncom=assert_eq(size(p),size(xi),'linmin')
    pcom=>p
    xicom=>xi
    ax=0.0
    xx=1.0
    call mnbrak(ax,xx,bx,fa,fx,fb,f1dim)
    fret=brent(ax,xx,bx,f1dim,TOL,xmin)
    xi=xmin*xi
    p=p+xi
    END SUBROUTINE linmin

SUBROUTINE lnsrch(xold,fold,g,p,x,f,stpmax,check,func_OCT)

USE nrtype; USE nrutil, ONLY : assert_eq,nrerror,vabs

IMPLICIT NONE

REAL(SP), DIMENSION(:), INTENT(IN) :: xold,g
REAL(SP), DIMENSION(:), INTENT(INOUT) :: p
REAL(SP), INTENT(IN) :: fold,stpmax
REAL(SP), DIMENSION(:), INTENT(OUT) :: x
REAL(SP), INTENT(OUT) :: f
LOGICAL(LGT), INTENT(OUT) :: check

INTERFACE
  FUNCTION func_OCT(p)
  USE nrtype
  USE Global_Parameters, ONLY: OCT_NC,OCT_NGoals
  IMPLICIT NONE
  REAL(SP), DIMENSION(2*OCT_NC*OCT_NGoals), INTENT(IN) :: p
  REAL(SP) :: func_OCT
  END FUNCTION func_OCT
END INTERFACE

REAL(SP), PARAMETER :: ALF=1.0e-4_sp,TOLX=epsilon(x)
INTEGER(I4B) :: ndum
REAL(SP) :: a,alam,alam2,alamin,b,disc,f2,pabs,rhs1,rhs2,slope,tmplam

ndum=assert_eq(size(g),size(p),size(x),size(xold),'lnsrch')
check=.false.
pabs=vabs(p(:))

if (pabs > stpmax) p(:)=p(:)*stpmax/pabs

slope=dot_product(g,p)

if (slope >= 0.0) call nrerror('roundoff problem in lnsrch')

alamin=TOLX/maxval(abs(p(:))/max(abs(xold(:)),1.0_sp))
alam=1.0

do
   x(:)=xold(:)+alam*p(:)
   f=func_OCT(x)
  
   if (alam < alamin) then
      x(:)=xold(:)
      check=.true.
      RETURN
   else if (f <= fold+ALF*alam*slope) then
      RETURN
   else

     if (alam == 1.0) then
        tmplam=-slope/(2.0_sp*(f-fold-slope))
     else
        rhs1=f-fold-alam*slope
        rhs2=f2-fold-alam2*slope
        a=(rhs1/alam**2-rhs2/alam2**2)/(alam-alam2)
        b=(-alam2*rhs1/alam**2+alam*rhs2/alam2**2)/&
        (alam-alam2)

        if (a == 0.0) then
           tmplam=-slope/(2.0_sp*b)
        else
           disc=b*b-3.0_sp*a*slope

           if (disc < 0.0) then
              tmplam=0.5_sp*alam
           else if (b <= 0.0) then
              tmplam=(-b+sqrt(disc))/(3.0_sp*a)
           else
              tmplam=-slope/(b+sqrt(disc))
           end if

        end if

        if (tmplam > 0.5_sp*alam) tmplam=0.5_sp*alam

     end if

   end if
   alam2=alam
   f2=f
   alam=max(tmplam,0.1_sp*alam)
end do

END SUBROUTINE lnsrch

!-----------------------------------------------------------------------    
!>This routine minimizes the CRAB functional func_OCT(P_OCT=(OCT_As,OCT_Bs)) 
!>This is the function to minimize f(P_OCT) with P_OCT=(OCT_As,OCT_Bs)
    SUBROUTINE  Functional_FCRAB_Minimization(OCT_As,OCT_Bs,ftol,iter,fret)

    USE Random

    USE nrtype; USE nrutil, ONLY : nrerror,outerprod,unit_matrix,vabs
    USE nr, ONLY : linmin,lnsrch
    USE NewGammaCRAB
    USE CRAB_Parameters, ONLY:  LastFunctionalValue,CRAB_CONTINUE,CRAB_FIRST,BREAK_CG,LastFunctionalValue 

    USE Global_Parameters, ONLY: OCT_Func_Tolerance,OCT_NC,OCT_NGoals,SIMANN,TruncatedNewton,&
                                 OCT_UpperBounds,OCT_LowerBounds,QuasiNewton

    IMPLICIT NONE

    REAL, DIMENSION(OCT_NC,OCT_NGoals) :: OCT_As, OCT_Bs
    INTEGER(I4B), INTENT(OUT) :: iter
    REAL(SP), INTENT(IN) :: ftol
    REAL(SP), INTENT(OUT) :: fret


    INTEGER :: I,J,K,S1,S2,Naccept,Nevals,Nout,errorflag,LW,IPIVOT(2*OCT_NC*OCT_NGoals)
    INTEGER :: MSGLVL, MAXIT, MAXFUN,STEPMX,L
    REAL*8 :: W(700),ETA,F

    REAL(SP), DIMENSION(2*OCT_NC*OCT_NGoals) :: p
    REAL(SP), DIMENSION(2*OCT_NC*OCT_NGoals) :: uppbounds,lowpbounds,stepsizes,stepsizes2
    INTEGER :: NACP(2*OCT_NC*OCT_NGoals)
    REAL :: XP(2*OCT_NC*OCT_NGoals),FSTAR(4)
    REAL(SP), DIMENSION(2*OCT_NC*OCT_NGoals) :: poptimal
    REAL(SP) :: FuncOpt 

    INTEGER(I4B), PARAMETER :: ITMAX=200
    REAL(SP), PARAMETER :: STPMX=100.0_sp,EPS=epsilon(p),TOLX=4.0_sp*EPS
    INTEGER(I4B) :: its
    REAL(SP) :: dgg,fp,gam,gg,SEED
    REAL(SP), DIMENSION(size(p)) :: g,h,xi
    REAL*8, DIMENSION(size(p)) :: g8,p8,uppbounds8,lowpbounds8

    REAL(SP) :: den,fac,fad,fae,stpmax,sumdg,sumxi
    REAL(SP), DIMENSION(size(p)) :: dg,hdg,pnew
    REAL(SP), DIMENSION(size(p),size(p)) :: hessin

    LOGICAL :: check
    

    INTERFACE                            
        FUNCTION func_OCT(p)
        USE nrtype
        USE Global_Parameters, ONLY: OCT_NC,OCT_NGoals
        IMPLICIT NONE
        REAL(SP), DIMENSION(2*OCT_NC*OCT_NGoals), INTENT(IN) :: p
        REAL(SP) :: func_OCT
        END FUNCTION func_OCT
!BL
        FUNCTION dfunc(p)
        USE nrtype
        USE Global_Parameters, ONLY: OCT_NC,OCT_NGoals
        IMPLICIT NONE
        REAL(SP), DIMENSION(2*OCT_NC*OCT_NGoals), INTENT(IN) :: p
        REAL(SP), DIMENSION(size(p)) :: dfunc
        END FUNCTION dfunc
    END INTERFACE
   
    EXTERNAL TruncatedNewton_Func_Dfunc 

    I=1
    DO K=1,OCT_NC
      DO J=1,OCT_NGoals
         p(I)=OCT_As(K,J)
         p(I+OCT_NGoals*OCT_NC)=OCT_Bs(K,J)
         I=I+1
      END DO
    END DO


    IF ((SIMANN.eqv..FALSE.).and.(TruncatedNewton.eqv..FALSE.).and.(QuasiNewton.eqv..FALSE.)) THEN
      fp=func_OCT(p)
      LastFunctionalValue=fp
     
      xi=dfunc(p)
      
      g=-xi
      h=g
      xi=h

      do its=1,ITMAX
          write(6,*) "Iteration inside FCRAB:", its
          write(6,*) "Iteration inside FCRAB:", its
          write(6,*) "Iteration inside FCRAB:", its
          iter=its
          call linmin(p,xi,fret)
          if (2.0_sp*abs(fret-fp) <= ftol*(abs(fret)+abs(fp)+EPS)) THEN
             L=1
             DO K=1,OCT_NC
               DO J=1,OCT_NGoals
                  OCT_As(K,J)=p(L)
                  OCT_Bs(K,J)=p(L+OCT_NGoals*OCT_NC)
                  L=L+1
               END DO
             END DO
             RETURN
          ENDIF
          fp=fret
          xi=dfunc(p)
          gg=dot_product(g,g)
!         dgg=dot_product(xi,xi)
          dgg=dot_product(xi+g,xi)
          if (gg == 0.0) RETURN
          gam=dgg/gg
          g=-xi
          h=g+gam*h
          xi=h
      end do
      call nrerror('frprmn: maximum iterations exceeded')
 
    ELSEIF ((SIMANN.eqv..FALSE.).and.(TruncatedNewton.eqv..FALSE.).and.(QuasiNewton.eqv..TRUE.)) THEN

    fp=func_OCT(p)
    g=dfunc(p)

    call unit_matrix(hessin)
    xi=-g
    stpmax=STPMX*max(vabs(p),real(size(p),sp))

    do its=1,ITMAX
       iter=its

       call lnsrch(p,fp,g,xi,pnew,fret,stpmax,check,func_OCT)

       fp=fret
       xi=pnew-p
       p=pnew

       if (maxval(abs(xi)/max(abs(p),1.0_sp)) < TOLX) RETURN

       dg=g
       g=dfunc(p)
       den=max(fret,1.0_sp)

       if (maxval(abs(g)*max(abs(p),1.0_sp)/den) < ftol) RETURN

       dg=g-dg
       hdg=matmul(hessin,dg)
       fac=dot_product(dg,xi)
       fae=dot_product(dg,hdg)
       sumdg=dot_product(dg,dg)
       sumxi=dot_product(xi,xi)

       if (fac > sqrt(EPS*sumdg*sumxi)) then
          fac=1.0_sp/fac
          fad=1.0_sp/fae
          dg=fac*xi-fad*hdg
          hessin=hessin+fac*outerprod(xi,xi)-&
          fad*outerprod(hdg,hdg)+fae*outerprod(dg,dg)
       end if

       xi=-matmul(hessin,g)
    end do


    ELSEIF ((SIMANN.eqv..TRUE.).and.(TruncatedNewton.eqv..FALSE.).and.(QuasiNewton.eqv..FALSE.)) THEN
       Write(6,*) "SIMANN IS PRESENT!" 

       uppbounds8(1:OCT_NC)=OCT_UpperBounds(1:OCT_NC)
       uppbounds8(OCT_NC+1:2*OCT_NC)=OCT_UpperBounds(1:OCT_NC)
       lowpbounds8=OCT_LowerBounds(1:OCT_NC)
       lowpbounds8(OCT_NC+1:2*OCT_NC)=OCT_LowerBounds(1:OCT_NC)

       Write(6,*) "Bounds:",uppbounds8,lowpbounds8
       
       stepsizes=2.d0
       stepsizes2=0.05d0

       CALL SA(2*OCT_NC*OCT_NGoals,REAL(P(1:2*OCT_NGoals*OCT_NC),8),.FALSE.,0.8d0,OCT_Func_Tolerance, &
               10,50,4,400,lowpbounds8, &
               uppbounds8,REAL(stepsizes(1:2*OCT_NGoals*OCT_NC),8),3,100.d0,&
               REAL(stepsizes2(1:2*OCT_NGoals*OCT_NC),8),REAL(poptimal(1:2*OCT_NGoals*OCT_NC),8),REAL(funcopt,8), &
               naccept,nevals,nout,errorflag, REAL(FSTAR,8),REAL(XP,8),NACP)

      IF (ERRORFLAG .EQ. 0) THEN

         CRAB_CONTINUE=.FALSE.

         WRITE(6,*) "Constrained simulated annealing converged, breaking CRAB loop!!!"
         WRITE(6,*) "Constrained simulated annealing converged, breaking CRAB loop!!!"
         WRITE(6,*) "Constrained simulated annealing converged, breaking CRAB loop!!!"

         L=1

         DO K=1,OCT_NC
           DO J=1,OCT_NGoals
              OCT_As(K,J)=REAL(poptimal(L),4)
              OCT_Bs(K,J)=Real(poptimal(L+OCT_NGoals*OCT_NC),4)
              L=L+1
           END DO
         END DO

      ENDIF


    ELSE 
       Write(6,*) "Your selection of minimization for the cost functional is not supported, check MCTDHX.inp"
       STOP
    ENDIF
         
    END SUBROUTINE Functional_FCRAB_Minimization

    SUBROUTINE TruncatedNewton_Func_Dfunc(N,X,F,G)

    DOUBLE PRECISION  X(N), G(N), F
    REAL XF(N),GF(N)

    INTERFACE
        FUNCTION func_OCT(p)
        USE nrtype
        USE Global_Parameters, ONLY: OCT_NC,OCT_NGoals
        IMPLICIT NONE
        REAL(SP), DIMENSION(2*OCT_NC*OCT_NGoals), INTENT(IN) :: p
        REAL(SP) :: func_OCT
        END FUNCTION func_OCT
!BL
        FUNCTION dfunc(p)
        USE nrtype
        USE Global_Parameters, ONLY: OCT_NC,OCT_NGoals
        IMPLICIT NONE
        REAL(SP), DIMENSION(2*OCT_NC*OCT_NGoals), INTENT(IN) :: p
        REAL(SP), DIMENSION(size(p)) :: dfunc
        END FUNCTION dfunc
    END INTERFACE

    XF=REAL(X,8)
    F=REAL(Func_OCT(XF),8)

    GF=dfunc(XF)
    G=REAL(GF,8)

    END SUBROUTINE TruncatedNewton_Func_Dfunc


      SUBROUTINE SA(N,X,MAX,RT,EPS,NS,NT,NEPS,MAXEVL,LB,UB,C,IPRINT, &
                    T,VM,XOPT,FOPT,NACC,NFCNEV,NOBDS,IER, &
                    FSTAR,XP,NACP)
      USE Random
!C For modifications to the algorithm and many details on its use,
!C (particularly for econometric applications) see Goffe, Ferrier
!C and Rogers, "Global Optimization of Statistical Functions with
!C Simulated Annealing," Journal of Econometrics, vol. 60, no. 1/2, 
!C Jan./Feb. 1994, pp. 65-100.
!C For more information, contact 
!C             Bill Goffe
!C             Department of Economics and International Business
!C             University of Southern Mississippi 
!C             Hattiesburg, MS  39506-5072 
!C             (601) 266-4484 (office)
!C             (601) 266-4920 (fax)
!C             bgoffe@whale.st.usm.edu (Internet)
!C
!C As far as possible, the parameters here have the same name as in
!C the description of the algorithm on pp. 266-8 of Corana et al.
!C
!C In this description, SP is single precision, DP is double precision,
!C INT is integer, L is logical and (N) denotes an array of length n.
!C Thus, DP(N) denotes a double precision array of length n.
!C
!C Input Parameters:
!C   Note: The suggested values generally come from Corana et al. To
!C         drastically reduce runtime, see Goffe et al., pp. 90-1 for
!C         suggestions on choosing the appropriate RT and NT.
!C   N - Number of variables in the function to be optimized. (INT)
!C    X - The starting values for the variables of the function to be
!C        optimized. (DP(N))
!C    MAX - Denotes whether the function should be maximized or
!C          minimized. A true value denotes maximization while a false
!C          value denotes minimization. Intermediate output (see IPRINT)
!C          takes this into account. (L)
!C    RT - The temperature reduction factor. The value suggested by
!C         Corana et al. is .85. See Goffe et al. for more advice. (DP)
!C    EPS - Error tolerance for termination. If the final function
!C          values from the last neps temperatures differ from the
!C          corresponding value at the current temperature by less than
!C          EPS and the final function value at the current temperature
!C          differs from the current optimal function value by less than
!C          EPS, execution terminates and IER = 0 is returned. (EP)
!C    NS - Number of cycles. After NS*N function evaluations, each
!C         element of VM is adjusted so that approximately half of
!C         all function evaluations are accepted. The suggested value
!C         is 20. (INT)
!C    NT - Number of iterations before temperature reduction. After
!C         NT*NS*N function evaluations, temperature (T) is changed
!C         by the factor RT. Value suggested by Corana et al. is
!C         MAX(100, 5*N). See Goffe et al. for further advice. (INT)
!C    NEPS - Number of final function values used to decide upon termi-
!C           nation. See EPS. Suggested value is 4. (INT)
!C    MAXEVL - The maximum number of function evaluations. If it is
!C             exceeded, IER = 1. (INT)
!C    LB - The lower bound for the allowable solution variables. (DP(N))
!C    UB - The upper bound for the allowable solution variables. (DP(N))
!C         If the algorithm chooses X(I) .LT. LB(I) or X(I) .GT. UB(I),
!C         I = 1, N, a point is from inside is randomly selected. This
!C         This focuses the algorithm on the region inside UB and LB.
!C         Unless the user wishes to concentrate the search to a par-
!C         ticular region, UB and LB should be set to very large positive
!C         and negative values, respectively. Note that the starting
!C         vector X should be inside this region. Also note that LB and
!C         UB are fixed in position, while VM is centered on the last
!C         accepted trial set of variables that optimizes the function.
!C    C - Vector that controls the step length adjustment. The suggested
!C        value for all elements is 2.0. (DP(N))
!C    IPRINT - controls printing inside SA. (INT)
!C             Values: 0 - Nothing printed.
!C                     1 - Function value for the starting value and
!C                         summary results before each temperature
!C                         reduction. This includes the optimal
!C                         function value found so far, the total
!C                         number of moves (broken up into uphill,
!C                         downhill, accepted and rejected), the
!C                         number of out of bounds trials, the
!C                         number of new optima found at this
!C                         temperature, the current optimal X and
!C                         the step length VM. Note that there are
!C                         N*NS*NT function evalutations before each
!C                         temperature reduction. Finally, notice is
!C                         is also given upon achieveing the termination
!C                         criteria.
!C                     2 - Each new step length (VM), the current optimal
!C                         X (XOPT) and the current trial X (X). This
!C                         gives the user some idea about how far X
!C                         strays from XOPT as well as how VM is adapting
!C                         to the function.
!C                     3 - Each function evaluation, its acceptance or
!C                         rejection and new optima. For many problems,
!C                         this option will likely require a small tree
!C                         if hard copy is used. This option is best
!C                         used to learn about the algorithm. A small
!C                         value for MAXEVL is thus recommended when
!C                         using IPRINT = 3.
!C             Suggested value: 1
!C             Note: For a given value of IPRINT, the lower valued
!C                   options (other than 0) are utilized.
!C  Input/Output Parameters:
!C    T - On input, the initial temperature. See Goffe et al. for advice.
!C        On output, the final temperature. (DP)
!C    VM - The step length vector. On input it should encompass the
!C         region of interest given the starting value X. For point
!C         X(I), the next trial point is selected is from X(I) - VM(I)
!C         to  X(I) + VM(I). Since VM is adjusted so that about half
!C         of all points are accepted, the input value is not very
!C         important (i.e. is the value is off, SA adjusts VM to the
!C         correct value). (DP(N))
!C
!C  Output Parameters:
!C    XOPT - The variables that optimize the function. (DP(N))
!C    FOPT - The optimal value of the function. (DP)
!C    NACC - The number of accepted function evaluations. (INT)
!C    NFCNEV - The total number of function evaluations. In a minor
!C             point, note that the first evaluation is not used in the
!C             core of the algorithm; it simply initializes the
!C             algorithm. (INT).
!C    NOBDS - The total number of trial function evaluations that
!C            would have been out of bounds of LB and UB. Note that
!C            a trial point is randomly selected between LB and UB.
!C            (INT)
!C    IER - The error return number. (INT)
!C          Values: 0 - Normal return; termination criteria achieved.
!C                  1 - Number of function evaluations (NFCNEV) is
!C                      greater than the maximum number (MAXEVL).
!C                  2 - The starting value (X) is not inside the
!C                      bounds (LB and UB).
!C                  3 - The initial temperature is not positive.
!C                  99 - Should not be seen; only used internally.
!C
!C  Work arrays that must be dimensioned in the calling routine:
!C       RWK1 (DP(NEPS))  (FSTAR in SA)
!C       RWK2 (DP(N))     (XP    "  " )
!C       IWK  (INT(N))    (NACP  "  " )
!C
!C  Required Functions (included):
!C    EXPREP - Replaces the function EXP to avoid under- and overflows.
!C             It may have to be modified for non IBM-type main-
!C             frames. (DP)
!C  Required Subroutines (included):
!C    PRTVEC - Prints vectors.
!C    PRT1 ... PRT10 - Prints intermediate output.
!C    FCN - Function to be optimized. The form is
!C            SUBROUTINE FCN(N,X,F)
!C            INTEGER N
!C            DOUBLE PRECISION  X(N), F
!C            ...
!C            function code with F = F(X)
!C            ...
!C            RETURN
!C            END
!C          Note: This is the same form used in the multivariable
!C          minimization algorithms in the IMSL edition 10 library.
!C
!C  Machine Specific Features:
!C    1. EXPREP may have to be modified if used on non-IBM type main-
!C       frames. Watch for under- and overflows in EXPREP.
!C    2. Some FORMAT statements use G25.18; this may be excessive for
!C       some machines.

!C  Type all external variables.
      DOUBLE PRECISION  X(*), LB(*), UB(*), C(*), VM(*), FSTAR(*), &
                        XOPT(*), XP(*), T, EPS, RT, FOPT
      INTEGER  NACP(*), N, NS, NT, NEPS, NACC, MAXEVL, IPRINT, &
               NOBDS, IER, NFCNEV, ISEED1, ISEED2
      LOGICAL  MAX

!C  Type all internal variables.
      DOUBLE PRECISION  F, FP, P, PP, RATIO
      INTEGER  NUP, NDOWN, NREJ, NNEW, LNOBDS, H, I, J, M
      LOGICAL  QUIT

!C  Type all functions.
      DOUBLE PRECISION  EXPREP
      REAL*4 :: RandomU


!C  Set initial values.
      NACC = 0
      NOBDS = 0
      NFCNEV = 0
      IER = 99

      DO 10, I = 1, N
         XOPT(I) = X(I)
         NACP(I) = 0
10    CONTINUE

      DO 20, I = 1, NEPS
         FSTAR(I) = 1.0D+20
20    CONTINUE 

!C  If the initial temperature is not positive, notify the user and 
!C  return to the calling routine.  
      IF (T .LE. 0.0) THEN
         WRITE(*,'(/,''  THE INITIAL TEMPERATURE IS NOT POSITIVE. '' &
                   /,''  RESET THE VARIABLE T. ''/)')
         IER = 3
         RETURN
      END IF

!C  If the initial value is out of bounds, notify the user and return
!C  to the calling routine.
      DO 30, I = 1, N
         IF ((X(I) .GT. UB(I)) .OR. (X(I) .LT. LB(I))) THEN
            CALL PRT1
            IER = 2
            RETURN
         END IF
30    CONTINUE

!C  Evaluate the function with input X and return value as F.
      F=Func_OCT(REAL(X(1:N),4))
!      CALL FCN(N,X,F)

!C  If the function is to be minimized, switch the sign of the function.
!C  Note that all intermediate and final output switches the sign back
!C  to eliminate any possible confusion for the user.
      IF(.NOT. MAX) F = -F
      NFCNEV = NFCNEV + 1
      FOPT = F
      FSTAR(1) = F
      IF(IPRINT .GE. 1) CALL PRT2(MAX,N,X,F)

!C  Start the main loop. Note that it terminates if (i) the algorithm
!C  succesfully optimizes the function or (ii) there are too many
!C  function evaluations (more than MAXEVL).
100   NUP = 0
      NREJ = 0
      NNEW = 0
      NDOWN = 0
      LNOBDS = 0

      DO 400, M = 1, NT
         DO 300, J = 1, NS
            DO 200, H = 1, N

!C  Generate XP, the trial value of X. Note use of VM to choose XP.
               DO 110, I = 1, N
                  IF (I .EQ. H) THEN
                     CALL GNU_Rand4(RandomU)
                     XP(I) = X(I) + (RandomU*2.- 1.) * VM(I)
                  ELSE
                     XP(I) = X(I)
                  END IF

!C  If XP is out of bounds, select a point in bounds for the trial.
                  IF((XP(I) .LT. LB(I)) .OR. (XP(I) .GT. UB(I))) THEN
                     CALL GNU_Rand4(RandomU)
                    XP(I) = LB(I) + (UB(I) - LB(I))*RandomU
                    LNOBDS = LNOBDS + 1
                    NOBDS = NOBDS + 1
                    IF(IPRINT .GE. 3) CALL PRT3(MAX,N,XP,X,FP,F)
                  END IF
110            CONTINUE

!C  Evaluate the function with the trial point XP and return as FP.
!               CALL FCN(N,XP,FP)
               FP=Func_OCT(REAL(XP(1:N),4))
               IF(.NOT. MAX) FP = -FP
               NFCNEV = NFCNEV + 1
               IF(IPRINT .GE. 3) CALL PRT4(MAX,N,XP,X,FP,F)

!C  If too many function evaluations occur, terminate the algorithm.
               IF(NFCNEV .GE. MAXEVL) THEN
                  CALL PRT5
                  IF (.NOT. MAX) FOPT = -FOPT
                  IER = 1
                  RETURN
               END IF

!C  Accept the new point if the function value increases.
               IF(FP .GE. F) THEN
                  IF(IPRINT .GE. 3) THEN
                     WRITE(*,'(''  POINT ACCEPTED'')')
                  END IF
                  DO 120, I = 1, N
                     X(I) = XP(I)
120               CONTINUE
                  F = FP
                  NACC = NACC + 1
                  NACP(H) = NACP(H) + 1
                  NUP = NUP + 1

!C  If greater than any other point, record as new optimum.
                  IF (FP .GT. FOPT) THEN
                  IF(IPRINT .GE. 3) THEN
                     WRITE(*,'(''  NEW OPTIMUM'')')
                  END IF
                     DO 130, I = 1, N
                        XOPT(I) = XP(I)
130                  CONTINUE
                     FOPT = FP
                     NNEW = NNEW + 1
                  END IF

!C  If the point is lower, use the Metropolis criteria to decide on
!C  acceptance or rejection.
               ELSE
                  P = EXPREP((FP - F)/T)
                  CALL GNU_Rand4(RandomU)
                  PP = RandomU
                  IF (PP .LT. P) THEN
                     IF(IPRINT .GE. 3) CALL PRT6(MAX)
                     DO 140, I = 1, N
                        X(I) = XP(I)
140                  CONTINUE
                     F = FP
                     NACC = NACC + 1
                     NACP(H) = NACP(H) + 1
                     NDOWN = NDOWN + 1
                  ELSE
                     NREJ = NREJ + 1
                     IF(IPRINT .GE. 3) CALL PRT7(MAX)
                  END IF
               END IF

200         CONTINUE
300      CONTINUE

!C  Adjust VM so that approximately half of all evaluations are accepted.
         DO 310, I = 1, N
            RATIO = DFLOAT(NACP(I)) /DFLOAT(NS)
            IF (RATIO .GT. .6) THEN
               VM(I) = VM(I)*(1. + C(I)*(RATIO - .6)/.4)
            ELSE IF (RATIO .LT. .4) THEN
               VM(I) = VM(I)/(1. + C(I)*((.4 - RATIO)/.4))
            END IF
            IF (VM(I) .GT. (UB(I)-LB(I))) THEN
               VM(I) = UB(I) - LB(I)
            END IF
310      CONTINUE

         IF(IPRINT .GE. 2) THEN
            CALL PRT8(N,VM,XOPT,X)
         END IF

         DO 320, I = 1, N
            NACP(I) = 0
320      CONTINUE

400   CONTINUE

      IF(IPRINT .GE. 1) THEN
         CALL PRT9(MAX,N,T,XOPT,VM,FOPT,NUP,NDOWN,NREJ,LNOBDS,NNEW)
      END IF

!C  Check termination criteria.
      QUIT = .FALSE.
      FSTAR(1) = F
      IF ((FOPT - FSTAR(1)) .LE. EPS) QUIT = .TRUE.
      DO 410, I = 1, NEPS
         IF (ABS(F - FSTAR(I)) .GT. EPS) QUIT = .FALSE.
410   CONTINUE

!C  Terminate SA if appropriate.
      IF (QUIT) THEN
         DO 420, I = 1, N
            X(I) = XOPT(I)
420      CONTINUE
         IER = 0
         IF (.NOT. MAX) FOPT = -FOPT
         IF(IPRINT .GE. 1) CALL PRT10
         RETURN
      END IF

!C  If termination criteria is not met, prepare for another loop.
      T = RT*T
      DO 430, I = NEPS, 2, -1
         FSTAR(I) = FSTAR(I-1)
430   CONTINUE
      F = FOPT
      DO 440, I = 1, N
         X(I) = XOPT(I)
440   CONTINUE

!C  Loop again.
      GO TO 100

      END

      FUNCTION  EXPREP(RDUM)
!C  This function replaces exp to avoid under- and overflows and is
!C  designed for IBM 370 type machines. It may be necessary to modify
!C  it for other machines. Note that the maximum and minimum values of
!C  EXPREP are such that they has no effect on the algorithm.

      DOUBLE PRECISION  RDUM, EXPREP

      IF (RDUM .GT. 174.) THEN
         EXPREP = 3.69D+75
      ELSE IF (RDUM .LT. -180.) THEN
         EXPREP = 0.0
      ELSE
         EXPREP = EXP(RDUM)
      END IF

      RETURN
      END

      SUBROUTINE PRT1
!C  This subroutine prints intermediate output, as does PRT2 through
!C  PRT10. Note that if SA is minimizing the function, the sign of the
!C  function value and the directions (up/down) are reversed in all
!C  output to correspond with the actual function optimization. This
!C  correction is because SA was written to maximize functions and
!C  it minimizes by maximizing the negative a function.

      WRITE(*,'(/,''  THE STARTING VALUE (X) IS OUTSIDE THE BOUNDS '' &
               /,''  (LB AND UB). EXECUTION TERMINATED WITHOUT ANY'' &
               /,''  OPTIMIZATION. RESPECIFY X, UB OR LB SO THAT  '' &
               /,''  LB(I) .LT. X(I) .LT. UB(I), I = 1, N. ''/)')

      RETURN
      END

      SUBROUTINE PRT2(MAX,N,X,F)

      DOUBLE PRECISION  X(*), F
      INTEGER  N
      LOGICAL  MAX

      WRITE(*,'(''  '')')
      CALL PRTVEC(X,N,'INITIAL X')
      IF (MAX) THEN
         WRITE(*,'(''  INITIAL F: '',/, G25.18)') F
      ELSE
         WRITE(*,'(''  INITIAL F: '',/, G25.18)') -F
      END IF

      RETURN
      END

      SUBROUTINE PRT3(MAX,N,XP,X,FP,F)

      DOUBLE PRECISION  XP(*), X(*), FP, F
      INTEGER  N
      LOGICAL  MAX

      WRITE(*,'(''  '')')
      CALL PRTVEC(X,N,'CURRENT X')
      IF (MAX) THEN
         WRITE(*,'(''  CURRENT F: '',G25.18)') F
      ELSE
         WRITE(*,'(''  CURRENT F: '',G25.18)') -F
      END IF
      CALL PRTVEC(XP,N,'TRIAL X')
      WRITE(*,'(''  POINT REJECTED SINCE OUT OF BOUNDS'')')

      RETURN
      END

      SUBROUTINE PRT4(MAX,N,XP,X,FP,F)

      DOUBLE PRECISION  XP(*), X(*), FP, F
      INTEGER  N
      LOGICAL  MAX

      WRITE(*,'(''  '')')
      CALL PRTVEC(X,N,'CURRENT X')
      IF (MAX) THEN
         WRITE(*,'(''  CURRENT F: '',G25.18)') F
         CALL PRTVEC(XP,N,'TRIAL X')
         WRITE(*,'(''  RESULTING F: '',G25.18)') FP
      ELSE
         WRITE(*,'(''  CURRENT F: '',G25.18)') -F
         CALL PRTVEC(XP,N,'TRIAL X')
         WRITE(*,'(''  RESULTING F: '',G25.18)') -FP
      END IF

      RETURN
      END

      SUBROUTINE PRT5

      WRITE(*,'(/,''  TOO MANY FUNCTION EVALUATIONS; CONSIDER '' &
                /,''  INCREASING MAXEVL OR EPS, OR DECREASING '' &
                /,''  NT OR RT. THESE RESULTS ARE LIKELY TO BE '' &
                /,''  POOR.'',/)')

      RETURN
      END

      SUBROUTINE PRT6(MAX)

      LOGICAL  MAX

      IF (MAX) THEN
         WRITE(*,'(''  THOUGH LOWER, POINT ACCEPTED'')')
      ELSE
         WRITE(*,'(''  THOUGH HIGHER, POINT ACCEPTED'')')
      END IF

      RETURN
      END

      SUBROUTINE PRT7(MAX)

      LOGICAL  MAX

      IF (MAX) THEN
         WRITE(*,'(''  LOWER POINT REJECTED'')')
      ELSE
         WRITE(*,'(''  HIGHER POINT REJECTED'')')
      END IF

      RETURN
      END

      SUBROUTINE PRT8(N,VM,XOPT,X)

      DOUBLE PRECISION  VM(*), XOPT(*), X(*)
      INTEGER  N

      WRITE(*,'(/, &
        '' INTERMEDIATE RESULTS AFTER STEP LENGTH ADJUSTMENT'',/)')
      CALL PRTVEC(VM,N,'NEW STEP LENGTH (VM)')
      CALL PRTVEC(XOPT,N,'CURRENT OPTIMAL X')
      CALL PRTVEC(X,N,'CURRENT X')
      WRITE(*,'('' '')')

      RETURN
      END

      SUBROUTINE PRT9(MAX,N,T,XOPT,VM,FOPT,NUP,NDOWN,NREJ,LNOBDS,NNEW)

      DOUBLE PRECISION  XOPT(*), VM(*), T, FOPT
      INTEGER  N, NUP, NDOWN, NREJ, LNOBDS, NNEW, TOTMOV
      LOGICAL  MAX

      TOTMOV = NUP + NDOWN + NREJ

      WRITE(*,'(/, &
       '' INTERMEDIATE RESULTS BEFORE NEXT TEMPERATURE REDUCTION'',/)')
      WRITE(*,'(''  CURRENT TEMPERATURE:            '',G12.5)') T
      IF (MAX) THEN
         WRITE(*,'(''  MAX FUNCTION VALUE SO FAR:  '',G25.18)') FOPT
         WRITE(*,'(''  TOTAL MOVES:                '',I8)') TOTMOV
         WRITE(*,'(''     UPHILL:                  '',I8)') NUP
         WRITE(*,'(''     ACCEPTED DOWNHILL:       '',I8)') NDOWN
         WRITE(*,'(''     REJECTED DOWNHILL:       '',I8)') NREJ
         WRITE(*,'(''  OUT OF BOUNDS TRIALS:       '',I8)') LNOBDS
         WRITE(*,'(''  NEW MAXIMA THIS TEMPERATURE:'',I8)') NNEW
      ELSE
         WRITE(*,'(''  MIN FUNCTION VALUE SO FAR:  '',G25.18)') -FOPT
         WRITE(*,'(''  TOTAL MOVES:                '',I8)') TOTMOV
         WRITE(*,'(''     DOWNHILL:                '',I8)')  NUP
         WRITE(*,'(''     ACCEPTED UPHILL:         '',I8)')  NDOWN
         WRITE(*,'(''     REJECTED UPHILL:         '',I8)')  NREJ
         WRITE(*,'(''  TRIALS OUT OF BOUNDS:       '',I8)')  LNOBDS
         WRITE(*,'(''  NEW MINIMA THIS TEMPERATURE:'',I8)')  NNEW
      END IF
      CALL PRTVEC(XOPT,N,'CURRENT OPTIMAL X')
      CALL PRTVEC(VM,N,'STEP LENGTH (VM)')
      WRITE(*,'('' '')')

      RETURN
      END

      SUBROUTINE PRT10

      WRITE(*,'(/,''  SA ACHIEVED TERMINATION CRITERIA. IER = 0. '',/)')

      RETURN
      END

      SUBROUTINE PRTVEC(VECTOR,NCOLS,NAME)
!C  This subroutine prints the double precision vector named VECTOR.
!C  Elements 1 thru NCOLS will be printed. NAME is a character variable
!C  that describes VECTOR. Note that if NAME is given in the call to
!C  PRTVEC, it must be enclosed in quotes. If there are more than 10
!C  elements in VECTOR, 10 elements will be printed on each line.

      INTEGER NCOLS
      DOUBLE PRECISION VECTOR(NCOLS)
      CHARACTER *(*) NAME

      WRITE(*,1001) NAME

      IF (NCOLS .GT. 10) THEN
         LINES = INT(NCOLS/10.)

         DO 100, I = 1, LINES
            LL = 10*(I - 1)
            WRITE(*,1000) (VECTOR(J),J = 1+LL, 10+LL)
  100    CONTINUE

         WRITE(*,1000) (VECTOR(J),J = 11+LL, NCOLS)
      ELSE
         WRITE(*,1000) (VECTOR(J),J = 1, NCOLS)
      END IF

 1000 FORMAT( 10(G12.5,1X))
 1001 FORMAT(/,25X,A)

      RETURN
      END


