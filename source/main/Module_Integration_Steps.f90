!>GPLv3
!    MCTDH-X: the multiconfigurational time-dependent Hartree for 
!    indistinguishable particles software
!
!    Copyright (C) 2021  A. U. J. Lode, M. C. Tsatsos, 
!                        E. Fasshauer, S. E. Weiner, 
!                        R. Lin, L. Papariello, P. Molignini, 
!                        C. Lévêque, M. Büttner , J. Xiang, S. Dutta
!
!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <https://www.gnu.org/licenses/>.
!
!
!
!
!<GPLv3










!> This module contains wrapper routines to drive the integration
!> of the orbital equations of motion by the master/slave processes.

Module Integration_Steps

IMPLICIT NONE

CONTAINS



!> @ingroup sevenstepintegration
!! @brief The 1st step of the seven-step integration scheme.
!!
!! It evaluates the coefficients on the right hand side by calling Evaluate_RHS_Orbitals.
!!
!!
!! ### Inputs and Outputs:
!! @param    time          : Current time
!! @param    MYID          : ID for MPI
!! @param    CISCF         : Label for coefficients? 
!! @param    time_rec      : CPU time recording
!!
!! PM & RL, August 2021
!> @todo this routine actually also modifies the coefficients, so: renaming / separate the tasks
SUBROUTINE Do_Step1_Orbitals(time,MYID,CISCF,time_rec)

!> Modules used:
USE Global_Parameters, ONLY: Multi_Level,Orbital_Integrator
USE Orbital_Allocatables
USE Matrix_Elements
USE Coefficients_Allocatables

IMPLICIT NONE

! Initialization of the variables:
Integer :: CISCF
Integer :: MYID
REAL*8  :: Time_Rec(12),Time
INTEGER :: IERR

CALL Evaluate_RHS_Orbitals(time,MYID,CISCF)

!===================================================================
time_rec(2)=MPI_WTIME()
!===================================================================

IF (Multi_Level.eqv..true.) THEN 
  PSI_IN_Nlevel=PSI_Nlevel    ! needed for step rejection
ELSE
  PSI_IN=PSI                  ! needed for step rejection
ENDIF

VIN0=VIN ! Coefficients
END SUBROUTINE Do_Step1_Orbitals
!--------------------------------------------------------------------------



!---------------------------------------------------------------------------
!--------------------------------------------------------------------------
!> @ingroup sevenstepintegration
!! @brief The 3rd step of the seven-step integration scheme. 
!!
!! ### Inputs and Outputs:
!! @param    time          : Current time
!! @param    t0            : Integration Step ?
!! @param    tau           : Integration Step ?
!! @param    tau_new       : Integration Step ? 
!! @param    time_rec      : CPU time recording
!! @param    zerr          : Error tolerance (only used when M=1, = Error_Tolerance)
!! @param    zerrci        : Error tolerance for the coefficients (only used when M=1, = 0)
!! @param    MYID          : ID for MPI
!!
!! PM & RL, August 2021
SUBROUTINE Do_Step3_Orbitals(time,t0,tau,tau_new,time_rec,&
                             zerr,zerrci,MYID)  

! Modules used:
USE Matrix_Elements
USE Global_Parameters, ONLY: Multi_Level,Orbital_Integrator,Error_Tolerance,Job_Prefactor,Morb
USE Integration
USE Orbital_Allocatables
USE Coefficients_Allocatables
USE Orbital_EquationOfMotion
USE Integration
USE Distribution_Organization
USE Orbital_Allocatables, ONLY: LocalSize
USE MPI

IMPLICIT NONE

! Initialization of the variables:
LOGICAL NewStart
COMPLEX*16 :: zerr,zerrci
REAL*8     :: ABMError
REAL*8     :: Time_Rec(12),time,t0,tau,tau_new
INTEGER    :: MYID,IERR
COMPLEX*16, POINTER :: PSI1_Pointer(:,:,:), OPSI_Pointer(:,:,:)!, AuxPsi_Pointer(:)



 IF (Multi_Level.eqv..FALSE.) THEN
    PSI1=PSI
    PSI1_Pointer(1:LocalSize,1:MOrb,1:1) => PSI1(:,:)
 ELSE
    PSI1_Nlevel=PSI_Nlevel
    PSI1_Pointer(1:LocalSize,1:MOrb,1:NLevel) => PSI1_Nlevel(:,:,:)
 ENDIF
!===================================================================
 time_rec(4)=MPI_WTIME()
!==================================================================
 t0=tau/2
 IF(MORB.eq.1) THEN !! No time-step adaption for TDGP
    t0=tau
 ENDIF

!===================================================================

 IF (Multi_Level.eqv..FALSE.) THEN
   OPSI =ZERO
   OPSI_Pointer(1:LocalSize,1:MOrb,1:1) => OPSI(:,:)
 ELSE
   OPSI_Nlevel =ZERO
   OPSI_Pointer(1:LocalSize,1:MOrb,1:NLevel) => OPSI_Nlevel(:,:,:)
 ENDIF

 NewStart=.True.
! distribute the matrix elements at tau/2
 call Distribute_Rho_Rhoinv(Rho1_Elements,Rho2_Elements,&
                            Inverted_Rho1_Elements,&
                            Full_Rho1_Elements)

 ABMError=Error_Tolerance/1.0d0
 ENRG_EVAL=.FALSE.

 ! Call to the function implementing the Heisenberg equations of motions
 ! for all possible cases (standard, multilevel, mpi):
 CALL FUNC_Unified(time,PSI1_Pointer,OPSI_Pointer,MYID)

 ENRG_EVAL=.FALSE.


 ! Call to the integrator for the orbital equations of motion
 ! for all possible cases (standard, multilevel, mpi):
 CALL Integrator_Orb_Unified(PSI1_Pointer,  &
                            OPSI_Pointer,  &
                            time,         &
                            t0,      &
                            NewStart,      &
                            ABMError,        &
                            MYID)

!==================================================================
!==================================================================
 Energy_Evaluation=999
 CALL MPI_BCAST(Energy_Evaluation,1, &
                MPI_INTEGER,0,MPI_COMM_WORLD,ierr)
!==================================================================
 time_rec(5)=MPI_WTIME()
!===================================================================
! Output of the values for the orbitals (why only called for MORB=1?)
IF(MORB.eq.1) THEN
    IF (Multi_Level.eqv..FALSE.) THEN
       PSI=PSI1
    ELSE
       PSI_Nlevel=PSI1_Nlevel
    ENDIF

! Parameter reset for the next step (why only called for MORB=1?)
    tau_new=tau
    zerr=Error_Tolerance
    zerrci=Zero
    time=time+tau/2
    VOUT=VIN0
ENDIF

!===================================================================
!=======FOR RELAXATION====================================
! Reset of parameters for the next step:
IF(Job_Prefactor.eq.(-1.0d0,0d0) &
    .or.Job_Prefactor.eq.(1.0d0,0d0)) THEN
    IF (Multi_Level.eqv..FALSE.) THEN
       PSI=PSI1
    ELSE
       PSI_Nlevel=PSI1_Nlevel
    ENDIF
    VOUT=VIN0
    tau_new=tau
ENDIF

END SUBROUTINE Do_Step3_Orbitals
!--------------------------------------------------------------------------   







!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
!> @ingroup sevenstepintegration
!!
!! @brief The 4th and 5th steps of the seven-step integration scheme.
!!
!! ### Inputs and Outputs:
!! @param    time          : Current time
!! @param    t0            : Integration Step ?
!! @param    tau           : Integration Step ?
!! @param    tau_new       : Integration Step ? 
!! @param    time_rec      : CPU time recording
!! @param    zerr          : Error tolerance (only used when M=1, = Error_Tolerance)
!! @param    zerrci        : Error tolerance for the coefficients (only used when M=1, = 0)
!! @param    MYID          : ID for MPI
!!
SUBROUTINE Do_Step4and5_Orbitals(time,t0,tau,tau_new,time_rec,&
                                 zerr,zerrci,MYID)   

! Modules used:
USE Matrix_Elements,ONLY: Rho1_Elements,Rho2_Elements,&
                          Inverted_Rho1_Elements,Full_Rho1_Elements,ENRG_EVAL,&
                          Energy_Evaluation
USE Global_Parameters, ONLY: Multi_Level,Orbital_Integrator,Error_Tolerance
USE Integration
USE Orbital_Allocatables
USE Coefficients_Allocatables
USE Orbital_EquationOfMotion
USE Distribution_Organization
USE Integration
USE Matrix_Elements
USE MPI

IMPLICIT NONE

! Variable initialization:
LOGICAL NewStart
REAL*8    :: ABMError
REAL*8    :: Time_Rec(12),time,t0,tau,tau_new
COMPLEX*16 :: zerr, zerrci
INTEGER   :: MYID,IERR
COMPLEX*16, POINTER :: PSI_Pointer(:,:,:), OPSI_Pointer(:,:,:)!, AuxPsi_Pointer(:)

!--------------Fourth integration step-----------------

! Use pointers to change the shape of the input and output orbitals
! (extended dimension if needed)
! standard case: 2->3
! multilevel case: 3->3
 IF (Multi_Level.eqv..FALSE.) THEN
   OPSI=ZERO
   OPSI_Pointer(1:LocalSize,1:MOrb,1:1) => OPSI(:,:)
   PSI_Pointer(1:LocalSize,1:MOrb,1:1) => PSI(:,:)
 ELSE
   OPSI_Nlevel=ZERO
   OPSI_Pointer(1:LocalSize,1:MOrb,1:NLevel) => OPSI_Nlevel(:,:,:)
   PSI_Pointer(1:LocalSize,1:MOrb,1:NLevel) => PSI_Nlevel(:,:,:)
 ENDIF

NewStart=.True.

! Calculate rho and rho inverse:
call Distribute_Rho_Rhoinv(Rho1_Elements,Rho2_Elements,&
                           Inverted_Rho1_Elements,&
                           Full_Rho1_Elements)

ABMError=Error_Tolerance/1.0d0
ENRG_EVAL=.FALSE.

! Call to the function implementing the Heisenberg equations of motions
! for all possible cases (standard, multilevel, mpi):
CALL FUNC_Unified(time,PSI_Pointer,OPSI_Pointer,MYID)

ENRG_EVAL=.FALSE.

!========================================================================
!=======  PSI2 orbitals at  tau/2   with rij(tau/2) Rijkl(tau/2)
!=======  PSI2 orbitals at  tau/2   with rij(tau/2) Rijkl(tau/2)

! Call to the integrator for the orbital equations of motion
! for all possible cases (standard, multilevel, mpi):
call Integrator_Orb_Unified(PSI_Pointer,  &
                            OPSI_Pointer,  &
                            time,         &
                            t0,      &
                            NewStart,      &
                            ABMError,        &
                            MYID)


IF (Multi_Level.eqv..FALSE.) THEN
   PSI2=PSI
ELSE
   PSI2_Nlevel=PSI_Nlevel
ENDIF

!=======  PSI2 orbitals at  tau/2   with rij(tau/2) Rijkl(tau/2)
!=======  PSI2 orbitals at  tau/2   with rij(tau/2) Rijkl(tau/2)
!========================================================================

!=========================================================================
time_rec(8)=MPI_WTIME()
!========================================================================

!--------------Fifth integration step---------------
!========================================================================
!====================== FIFTH INTEGRATION STEP ==========================
!====================== Orbitals from t=t+tau/2 to t=t+tau =============
!====================== using rho_ij(t+tau/2) and rho_ijkl(t+tau/2) ====
!========================================================================
time=time+tau/2
!========================================================================
NewStart=.False.
ENRG_EVAL=.FALSE.


CALL FUNC_Unified(time,PSI_Pointer,OPSI_Pointer,MYID)
ENRG_EVAL=.FALSE.
!========================================================================
!=======  PSI orbitals at   tau   with rij(tau/2) Rijkl(tau/2)
!=======  PSI orbitals at   tau   with rij(tau/2) Rijkl(tau/2)
 call Integrator_Orb_Unified(PSI_Pointer,  &
                            OPSI_Pointer,  &
                            time,         &
                            t0,      &
                            NewStart,      &
                            ABMError,        &
                            MYID)


!=======  PSI orbitals at   tau   with rij(tau/2) Rijkl(tau/2) 
!=======  PSI orbitals at   tau   with rij(tau/2) Rijkl(tau/2) 
!========================================================================

Energy_Evaluation=999
call MPI_BCAST(Energy_Evaluation,1, &
               MPI_INTEGER,0,MPI_COMM_WORLD,ierr)

IF (MPI_ORBS.eqv..FALSE.) THEN
    ! CI_SCF=1 -> SCF PART --> needed for evaluation of RHS.
    ! At this point it should already be CI_SCF=1.
    ! tell the processors in the team sharing the workload
    ! that now the orbital integration is done and they should
    ! start await data and task assignments for solving the 
    ! coeff eoms.
    call MPI_BCAST(CI_SCF,1,MPI_INTEGER,0,MPI_COMM_WORLD,ierr)
ENDIF

!  h(tau) Interaction_Elements(tau)
!  Wrapper routine for calling Func / Func_nlevel / Func_MPI to evaluate the RHS of the
!  orbital EOMs
CALL Evaluate_RHS_Orbitals(time,MYID,CI_SCF) 
!  check if here CI_SCF is already set to 0 for the coefficient part in step 6/7
!  h(tau) Interaction_Elements(tau)

End Subroutine Do_Step4and5_Orbitals
!--------------------------------------------------------------------------   



!--------------------------------------------------------------------------
!--------------------------------------------------------------------------   
!> @ingroup init 
!! @brief This subroutine initializes the Orbital and Coefficients arrays,
!! opens the binary output files and writes their headers.
!! It fills orbital and coefficient arrays with data, either read from files or
!! with predefined guess (in Get_Initial_Orbitals and Get_initial_Coefficient routines)
!! (e.g. Gaussian shape for the orbitals, coherent initial state in coeff. array etc.
!! default is random initialization)
!! To quickly understand what this routine is doing to initialize orbitals and coefficient
!! arrays, the following code block is very telling:
!!
!!       Code in Fortran:
!!
!!            IF (Multi_level.eqv..true.) THEN
!!              CALL Get_Initial_NlevelOrbitals(PSI_Nlevel)
!!            ELSE
!!            IF (MPI_ORBS.eqv..FALSE.) THEN
!!              CALL Get_Initial_Orbitals(PSI)
!!            ELSE
!!              CALL Get_Initial_Orbitals_MPI(PSI,MYID)
!!            ENDIF
!!            ENDIF
!!            IF (trim(Coefficients_Integrator).ne.'BDV') THEN
!!              CALL Get_Initial_Coefficients(VIN)
!!            ELSE
!!              CALL Get_Initial_Coefficients_BDAV(VIN)
!!            ENDIF
!!
!! @param OrbBinr : boolean, True if orbital information should be read from binary
!! @param MYID : MPI related
Subroutine Get_Initial_Value(OrbBinr,MYID)

USE Initial_Orbitals
USE Initial_Coefficients
USE Orbital_Allocatables
USE Coefficients_Allocatables
USE Coefficients_parameters, only : NPar 
USE Input_Output

USE Global_Parameters, ONLY: Binary_Start_Time,Multi_Level,Orbital_Integrator,DO_OCT
USE Crab_Parameters
USE DVR_Parameters, ONLY: Vortex_Profile,Ort_X
USE Interaction_Parameters

USE Orbital_Parallelization_Parameters, ONLY: LocalPsiDim, LocalPsiDim_Inter,Local_Offset_Inter 
USE Distribution_Organization
USE Interpolation

USE MPI

IMPLICIT NONE

INTEGER :: MYID,Nprocs
LOGICAL, INTENT(IN) :: ORBBinr

INTEGER :: ierr, K, DVR_size

COMPLEX*16, ALLOCATABLE :: Psi_Interpol_full(:,:),Psi_full(:,:)

IF(ORB_Binr) then
  !c========== Read Initial State from Binary files
  !  Write(6,*) "MYID", MYID,"Before Open Binary"
  !  CALL MPI_BARRIER(MPI_COMM_WORLD,IERR)
  CALL Open_binary(MYID)
  !  Write(6,*) "MYID", MYID,"OPENED BINARY"
  CALL Read_Header(779,'scan')
  !  Write(6,*) "MYID", MYID,"READ HEADER"

  IF (Multi_level.eqv..true.) THEN
    CALL READ_BINARY_Nlevel(778, Binary_Start_Time,VIN,PSI_Nlevel)
    CALL READ_BINARY_Nlevel(777, Binary_Start_Time,VIN,PSI_Nlevel)
  ELSE

    IF (MPI_ORBS.eqv..FALSE.) THEN 
      DVR_size = NDVR_X * NDVR_Y * NDVR_Z
    ELSE
      DVR_Size = LocalPsiDim(MYID+1)
    ENDIF

    IF (DO_OCT.eqv..TRUE.) THEN
      IF (CRAB_First.eqv..TRUE.) THEN

        allocate(PSI_OCT(NDVR_X*NDVR_Y*NDVR_Z,Morb),stat=ierr)
        if(ierr /= 0) write(*,*)"allocation error in PSI"
        ALLOCATE(VIN_OCT(Nconf))


        IF (OCT_Restart.eqv..FALSE.) THEN
          CALL READ_BINARY(778, Binary_Start_Time,VIN,PSI,MYID,DVR_size,.FALSE.)
          CALL READ_BINARY(777, Binary_Start_Time,VIN,PSI,MYID,DVR_size,.FALSE.)
        ELSEIF (OCT_Restart.eqv..TRUE.) THEN
          CALL READ_BINARY(778, Time_Begin,VIN,PSI,MYID,DVR_size,.FALSE.)
          CALL READ_BINARY(777, Time_Begin,VIN,PSI,MYID,DVR_size,.FALSE.)
        ENDIF

        VIN_OCT=VIN
        PSI_OCT=PSI

        OCT_Goal(:,1)=Dconjg(PSI_OCT(:,2))*PSI_OCT(:,2)
              
        Open(unit=135,file='OCT_Goal')
        Write(135,'(3E25.16)') (Ort_X(K),OCT_Goal(K,1),K=1,NDVR_X)
        Close(135)

      ELSEIF (CRAB_First.eqv..FALSE.) THEN
                             
        VIN=VIN_OCT
        PSI=PSI_OCT
        CRAB_BREAK=.FALSE.

      ENDIF ! First or not OCT
         
      CRAB_FIRST=.FALSE.

    ELSEIF (DO_OCT.eqv..FALSE.) THEN  ! No OCT

      ! Interpolation of the orbitals if different grids
      if( (NDVR_X .ne. NDVR_X_inter) .or. (NDVR_Y .ne. NDVR_Y_inter) &
         .or.  (NDVR_Z .ne. NDVR_Z_inter) ) then

        IF (MPI_ORBS.eqv..FALSE.) THEN 

          DVR_size = NDVR_X_inter*NDVR_Y_inter*NDVR_Z_inter
          allocate(PSI_interpol(NDVR_X_inter*NDVR_Y_inter*NDVR_Z_inter,MOrb))

        ELSEIF (MPI_ORBS.eqv..TRUE.) THEN

          IF (ALLOCATED(LocalPsiDim_Inter).eqv..FALSE.) THEN
             STOP "Allocation error LocalPsiDim_Inter"
          ENDIF

          DVR_size=LocalPsiDim_inter(MYID+1)
          allocate(PSI_interpol(DVR_Size,MOrb))
          allocate(PSI_interpol_full(SUM(LocalPsiDim_inter(:)),MOrb))
          allocate(PSI_full(NDVR_X*NDVR_Y*NDVR_Z,MOrb))

        ENDIF

        if ((trim(Previous_Coefficients_Integrator).eq.'BDV').and.    &
            (trim(Coefficients_Integrator).ne.'BDV')) then
           
          call READ_BINARY(778, Binary_Start_Time,VIN,PSI_interpol,MYID,DVR_size,.TRUE.) ! orbs
          call READ_BINARY(777, Binary_Start_Time,VIN,PSI_interpol,MYID,DVR_size,.TRUE.)
          
        elseif ((trim(Previous_Coefficients_Integrator).eq.'BDV')    &
                .and.(trim(Coefficients_Integrator).eq.'BDV')) then 
              
          call READ_BINARY_Block(778, Binary_Start_Time,VIN,PSI_interpol) ! orbs
          call READ_BINARY_Block(777, Binary_Start_Time,VIN,PSI)
        
        else
  
          call READ_BINARY(778, Binary_Start_Time,VIN,PSI_interpol,MYID,DVR_size,.FALSE.) ! orbs
          call READ_BINARY(777, Binary_Start_Time,VIN,PSI,MYID,DVR_size,.FALSE.)

        endif

        write(0,*) '##################################################################'
        write(0,*) ' The size of the DVR has changed with respect to the previous run'   
        write(0,*) ' in CIc_bin and PSI_bin, -> Interpolation will be used'   
        write(0,*) ' Previous grid parameters :  ', NDVR_X_inter, NDVR_Y_inter, NDVR_Z_inter
        write(0,*) ' New grid parameters :       ', NDVR_X, NDVR_Y, NDVR_Z
        write(0,*) '##################################################################'

        IF (MPI_ORBS.eqv..FALSE.) THEN 
          call interpol_orbs(PSI_interpol,PSI)
          deallocate(PSI_interpol)
        ELSEIF (MPI_ORBS.eqv..TRUE.) THEN
 
          CALL MPI_COMM_SIZE(MPI_COMM_WORLD,Nprocs,IERR)

          CALL AllGather_MPI_Orbitals(Psi_interpol,Psi_interpol_full,MYID,Nprocs,&
                                      INT(NDVR_X_inter,4),INT(NDVR_Y_inter,4),INT(NDVR_Z_inter,4),&
                                      INT(LocalPsiDim_Inter,4),INT(Local_Offset_Inter,4))

          !           Write(6,*) "MYID", MYID,"HAS NORM",& 
          !                       SUM(PSI_interpol_full(:,2)*dconjg(PSI_interpol_full(:,2)))
          !           Write(ELSE6,*) "MYID", MYID,"HAS local NORM",& 
          !                       SUM(PSI_interpol(:,2)*dconjg(PSI_interpol(:,2)))

          call interpol_orbs(PSI_interpol_full,PSI_full)

          CALL Restore_MPI_Orbitals(PSI,PSI_full,MYID,Nprocs,LocalPsiDim,Local_Offset)

          deallocate(PSI_interpol)
          deallocate(PSI_full)
          deallocate(Psi_interpol_full)

          DEALLOCATE(LocalPsiDim_Inter)
          DEALLOCATE(Local_Offset_Inter)
        ENDIF

    else 

      IF ((trim(Previous_Coefficients_Integrator).eq.'BDV').and.    &
          (trim(Coefficients_Integrator).ne.'BDV')) THEN

        CALL READ_BINARY(778, Binary_Start_Time,VIN,PSI,MYID,DVR_size,.TRUE.)
        CALL READ_BINARY(777, Binary_Start_Time,VIN,PSI,MYID,DVR_size,.TRUE.)

      ELSEIF ((trim(Previous_Coefficients_Integrator).eq.'BDV')    &
              .and.(trim(Coefficients_Integrator).eq.'BDV')) THEN        

        CALL READ_BINARY_Block(778, Binary_Start_Time,VIN,PSI)
        CALL READ_BINARY_Block(777, Binary_Start_Time,VIN,PSI) 

      ELSE

        CALL READ_BINARY(778, Binary_Start_Time,VIN,PSI,MYID,DVR_size,.FALSE.)
        CALL READ_BINARY(777, Binary_Start_Time,VIN,PSI,MYID,DVR_size,.FALSE.)

      ENDIF

      endif ! No interpolation 
   
    ENDIF ! No OCT

  ENDIF !single vs multilevel

  CALL Close_binary(MYID)
 
!c========== Read Initial State from Binary files
ELSE
!c========== Construct Initial State manually
!MPI_MULTI_LEVEL VERSION NEEDED ! DONE 
        IF (Multi_level.eqv..true.) THEN
          IF (MPI_ORBS.eqv..FALSE.) THEN
            CALL Get_Initial_NlevelOrbitals(PSI_Nlevel)
          ELSE
            CALL Get_Initial_NlevelOrbitals_MPI(PSI_Nlevel,MYID) ! Function already written
          ENDIF
        ELSE
          IF (MPI_ORBS.eqv..FALSE.) THEN
            CALL Get_Initial_Orbitals(PSI)
          ELSE
            CALL Get_Initial_Orbitals_MPI(PSI,MYID)
          ENDIF
        ENDIF
        IF (trim(Coefficients_Integrator).ne.'BDV') THEN
          CALL Get_Initial_Coefficients(VIN)
        ELSE
          CALL Get_Initial_Coefficients_BDAV(VIN)
        ENDIF
!c========== Construct Initial State manually 
ENDIF

!!!!   Vortex Seeding (after initial guess has been read)
       IF (Vortex_Seeding.eqv..TRUE.) THEN
          Write(6,*) "Multiplying initial orbital with Vortex Profile.."
          DO K=1,MORB
            IF (OrbLz(K).ne.-666) THEN
               PSI(:,K)=PSI(:,K)*Vortex_Profile(:,K)          
               write(6,*) "Vortex seeded in orbital",K    
            ENDIF
          END DO
       ENDIF
!!!!   Vortex Seeding (after initial guess has been read)
!c================================================================
!c==== Open and initialize binary files
    CALL Open_binary(MYID)

!    Write(6,*) "MYID",MYID,"opened binary"

   IF (MYID.eq.0) THEN
      CALL Write_Header(779,NPar)
      FLUSH(779)
   ENDIF
END Subroutine Get_Initial_Value

!> @ingroup sevenstepintegration
!! @brief Obtain matrix elements h_ij and W_ijkl
!!
!! This subroutine calls a variation of Get_HamiltonianAction_Orbitals
!! which then calls the routine FUNC or FUNC_* which computes 
!! the right hand side of the orbital equations of motion and
!! the matrix elements of the one-body and two-body parts of the 
!! Hamiltonian, \f$ h_{ij} \f$  and \f$ W_{ijkl} \f$, respectively.
!!
!! ## Inputs and Outputs:
!! @param    time          : Current time
!! @param    MYID          : ID for MPI
!! @param    CISCF         : Label for coefficients? 
!!
!> @TODO: separate tasks of this routine: one task is to compute the matrix elements, 
!> and another is to evaluate the eoms for the orbitals.
SUBROUTINE Evaluate_RHS_Orbitals(time,MYID,CISCF)

USE Matrix_Elements

USE Global_Parameters, ONLY: Multi_Level,Orbital_Integrator

USE Orbital_Allocatables
USE Orbital_EquationOfMotion

Real*8  :: Time
Integer :: MYID     ! MPI process ID
Integer :: CISCF    ! MPI-related: if CI_SCF=1, the slave process was assigned to 
                    ! work on the orbital EOM, if CI_SCF=0 on the coeff EOM


IF (CISCF.ne.1) THEN
   Write(6,*) "Wrong case in Evaluate_RHS_Orbitals CISCF", CISCF
   STOP
ENDIF



IF (Multi_Level.eqv..FALSE.) THEN 
  IF (MPI_ORBS.eqv..FALSE.) THEN
    !> TODO: These different Get_Hamiltonian_ routines should be unified into one.
   !CALL Get_HamiltonianAction_Orbitals &
   CALL Get_HamiltonianAction_Orbitals_No_MPI &
         (PSI,time,Rho1_Elements,Rho2_Elements,&
          Inverted_Rho1_Elements,&
          Full_Rho1_Elements)


  ELSEIF (MPI_ORBS.eqv..TRUE.) THEN
   CALL Get_HamiltonianAction_Orbitals_MPI&
         (PSI,time,Rho1_Elements,Rho2_Elements,&
          Inverted_Rho1_Elements,&
          Full_Rho1_Elements,MYID)

  ENDIF
ELSEIF (Multi_Level.eqv..TRUE.) THEN
  IF (MPI_ORBS.eqv..FALSE.) THEN
   !CALL Get_HamiltonianAction_NlevelOrbitals&
   CALL Get_HamiltonianAction_Orbitals_No_MPI &
         (PSI_Nlevel,time,Rho1_Elements,Rho2_Elements,&
          Inverted_Rho1_Elements,&
          Full_Rho1_Elements)
  ELSEIF (MPI_ORBS.eqv..TRUE.) THEN

   CALL Get_HamiltonianAction_Orbitals_MPI&
         (PSI_Nlevel,time,Rho1_Elements,Rho2_Elements,&
          Inverted_Rho1_Elements,&
          Full_Rho1_Elements,MYID)
   ENDIF

ENDIF

END SUBROUTINE Evaluate_RHS_Orbitals


SUBROUTINE Write_Output(time,step,ici,MYID) 

USE Global_Parameters, ONLY: Multi_Level,Orbital_Integrator,Write_ASCII

USE Orbital_Allocatables
USE Coefficients_Allocatables
USE Auxiliary_FFTRoutines

USE Input_Output

IMPLICIT NONE

Integer :: ici,MYID
Real*8 :: time
Integer*8 :: step
LOGICAL, SAVE :: FIRST=.TRUE.


IF (FIRST.eqv..TRUE.) THEN
  ici=Output_Coefficients
  FIRST=.FALSE.
ENDIF

! write orbital binary
IF (Multi_Level.eqv..true.) THEN
   call Write_NlevelOrbitals_Binary(time,step,PSI_Nlevel)
ELSE
   call Write_Orbitals_Binary(time,step,PSI,778,MYID)
ENDIF

Call flush(778)

! write orbitals to ASCII file
IF(Write_ASCII.eqv..TRUE.) THEN 
  IF (Multi_Level.eqv..False.) THEN
     call Write_Orbitals(time,PSI,FT=.FALSE.)

!     IF (Write_FT.eqv..TRUE.) THEN
!      call Get_Sorted_FFT(Psi,FTPsi) 
!      call Write_Orbitals(time,FTPSI,FT=.TRUE.)
!     ENDIF

  ELSE 
     call Write_NlevelOrbitals(time,PSI_Nlevel,FT=.FALSE.)
  ENDIF
ENDIF

!write coefficients if desired
if (ici.eq.Output_Coefficients) then
  ! --- to ASCIII
  IF (Write_ASCII.eqv..TRUE.) THEN
    IF (Coefficients_Integrator.ne.'BDV') THEN
       call Write_Coefficients(time,VIN)
    ELSE 
       call Write_Coefficients_BDAV(time,VIN)
    ENDIF
  ENDIF
  ! --- to binary
  IF (Coefficients_Integrator.ne.'BDV') THEN
    If (MYID.eq.0) Then
    call Write_Coefficients_Binary(time,step,VIN,NConf,777)
    EndIf
  ELSE 
    call Write_Coefficients_Block_Binary(time,step,VIN)
  ENDIF

  If (MYID.eq.0) Then
  Call flush(777)
  EndIf
  ! we just wrote out the coefficients, reset counter
  ici=0

endif

ici=ici+1 ! increment counter  
 
End Subroutine Write_Output

!> @ingroup sevenstepintegration
Subroutine Evaluate_Error(zerr,zerrci,xerr,xerr_td,tau_new,tau,Energy_Old,Energ,MYID)

USE Global_Parameters, ONLY: Multi_Level, Orbital_Integrator, Morb, Error_Rescale_TD,Error_Tolerance
USE Matrix_Elements, ONLY: Full_Rho1_Elements
USE Orbital_Parallelization_Parameters

USE Orbital_Allocatables
USE Coefficients_Allocatables

IMPLICIT NONE

COMPLEX*16 :: zerr,zerrci
REAL*8     :: xerr,xerr_td,tau_new,tau,Energy_Old,Energ


INTEGER    :: MYID,IERR
Integer*8  :: ii,jj,L,DM
COMPLEX*16 :: zerr_new,ZDOTC,zerr_new_storage

EXTERNAL ZDOTC

!include 'mpif.h'

IF (MYID.eq.0) THEN

  VIN=VIN0-VIN
  zerr=ZDOTC(INT(Nconf,8),VIN,1,VIN,1)

ENDIF

IF (MPI_ORBS.eqv..TRUE.) THEN
  call MPI_BCAST(ZERR,1,MPI_DOUBLE_COMPLEX,0,MPI_COMM_WORLD,ierr)
ENDIF

zerrci=zerr

xerr=0d0

If (Multi_Level.eqv..False.) THEN
   PSI2=PSI2-PSI1
Else
   Psi2_Nlevel=Psi2_Nlevel-Psi1_Nlevel
endif

IF (MPI_ORBS.eqv..FALSE.) THEN
   DM=NDVR_X*NDVR_Y*NDVR_Z
ELSEIF (MPI_ORBS.eqv..TRUE.) THEN
   DM=LocalPsiDim(MYID+1)
ENDIF

zerr_new=Zero

If (Multi_Level.eqv..False.) THEN

  Do ii=1,Morb
    Do jj=1,Morb

      zerr_new=zerr_new &
              +ZDOTC(DM,PSI2(:,II),1,PSI2(:,JJ),1) &
              *Full_Rho1_Elements(jj,ii)

    EndDo
  EndDo
ELSE

  Do ii=1,Morb
    Do jj=1,Morb
      DO L=1,Nlevel
         zerr_new=zerr_new &
               +ZDOTC(DM,PSI2_Nlevel(:,II,L),1, &
                         PSI2_Nlevel(:,JJ,L),1) &
               *Full_Rho1_Elements(jj,ii)
      EndDo
    EndDo
  EndDo

ENDIF

IF (MPI_ORBS.eqv..TRUE.) THEN
     CALL MPI_ALLREDUCE(zerr_new,zerr_new_storage,1, &
               MPI_DOUBLE_COMPLEX,MPI_SUM,MPI_COMM_WORLD,ierr)
ENDIF

zerr_new=zerr_new_storage
!=======================================================================
xerr=ABS(zerr_new)
zerr=(ABS(zerr_new)+ABS(zerrci))*Zoner
!! New Error coming from TD potential NOT EXACT!!!!!  1-Exp(dE*dT)
xerr_td=1.0d0-Exp((ABS(Energ-Energy_old))*ABS(tau))
!! New Error coming from TD potential NOT EXACT!!!!!  1-Exp(dE*dT)
tau_new=Error_Rescale_TD*tau*(Error_Tolerance/REAL(ABS(zerr)))**0.25d0

!Write(6,*) "MYID error results", tau_new,zerr,xerr,zerr_new

END Subroutine Evaluate_Error

Subroutine Write_Timing(Time,time_rec,step)

IMPLICIT NONE

REAL*8 Time_rec(12),Time,time_ci1,time_ci2,time_ci3,time_psi1,time_psi2,time_psi3,exc_tm
INTEGER*8 step

!================ CI TIMING =============================================
time_ci1=time_rec(3)-time_rec(2) !first CI propagation
time_ci2=time_rec(10)-time_rec(9)!second CI (back)propagation
time_ci3=time_rec(4)-time_rec(3)&!inversion of rho
        +time_rec(7)-time_rec(6)&!inversion of rho
        +time_rec(12)-time_rec(11)!inversion of rho

!================ ORB TIMING =============================================
time_psi1=time_rec(5)-time_rec(4) !first orbital integration
time_psi2=time_rec(8)-time_rec(7) !second orbital integration
time_psi3=time_rec(9)-time_rec(8) !third orbital integration

!=========================================================================
exc_tm=exc_tm+time_rec(12)-time_rec(1) !time of whole integration

WRITE(1001,'(I4,7E25.16)') step,time,&
        exc_tm,&                        ! total runtime
        time_rec(12)-time_rec(1),&      ! time of the current integration step
        time_ci1+time_ci2+time_ci3,&    ! total time spent for ci part
        time_ci3,&                      ! time spent for inverting rho
        time_ci1+time_ci2,&             ! time for CI-EOM propagation
        time_psi1+time_psi2+time_psi3   ! total time spent for orbital part

End Subroutine Write_Timing



Subroutine Evaluate_State_Populations
USE MPI
USE Global_Parameters,Only: Multi_Level,Nlevel,Morb,Coefficients_Integrator
USE Matrix_Elements,Only: Nocc,Nocc_Block,Full_Rho1_Elements,Energy_Block
USE Orbital_Allocatables
INTEGER*8 :: I,K,J,Q
INTEGER :: MYID,i_error

State_Populations=0.d0                                                                                        

DO I=1,Morb
  DO J=1,Morb
    DO K=1,Nlevel
      State_Populations(K)=State_Populations(K)+  &                                                            
                Full_Rho1_Elements(I,J)*          &
                sum(dconjg(PSI_Nlevel(:,I,K))     &                                                            
                          *PSI_Nlevel(:,J,K))                                                                 
    ENDDO  
  ENDDO
ENDDO      
CALL MPI_ALLREDUCE(MPI_IN_PLACE,State_Populations,Nlevel, &
                   MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD, &
                   i_error)                                                                                             
End Subroutine Evaluate_State_Populations

Subroutine Write_Occupations(Time)

USE Global_Parameters,Only: Multi_Level,Cavity_BEC,Energy,Nlevel,Morb, &
                            NCavity_Modes,Energy,Coefficients_Integrator
USE DVR_Parameters, Only: Cavity_Field
USE Orbital_Allocatables
USE Matrix_Elements,Only: Nocc,Nocc_Block,Full_Rho1_Elements,Energy_Block
USE Coefficients_Parameters,ONLY:Npar

IMPLICIT NONE

Real*8    :: Time
INTEGER*8 :: I,K,J,Q

!=========================================================================
IF (Cavity_BEC.eqv..TRUE.) THEN
!=========================================================================
   write(20,2222)time,                 &
        "   ",(REAL(Cavity_Field(i)),  &
        "   ",DIMAG(Cavity_Field(i)),  &
        "   ",i=1,NCavity_Modes)
ENDIF


!=========================================================================
IF (Multi_Level.eqv..TRUE.) THEN
!=========================================================================
! MPI MULTI_LEVEL NEEDED 
! When MPI_ORBS=T, This only retrieves the state population from MYID=0
! This is now realized in the subroutine Evaluate_State_Populations
!State_Populations=0.d0                                                                                        

!DO I=1,Morb
!  DO J=1,Morb
!    DO K=1,Nlevel
!      State_Populations(K)=State_Populations(K)+  &                                                            
!                Full_Rho1_Elements(I,J)*          &
!                sum(dconjg(PSI_Nlevel(:,I,K))     &                                                            
!                          *PSI_Nlevel(:,J,K))                                                                 
!    ENDDO                                                                                                     
!  ENDDO                                                                                                       
!ENDDO                                                                                                         

write(10,2222)time,"  ",(Nocc(i)/Npar,"  ",i=1,Morb),Energy,"  ", &                                             
(State_Populations(I),"  ",i=1,Nlevel)


!=========================================================================
ELSE
!=========================================================================
  IF (trim(Coefficients_Integrator).ne.'BDV') THEN
    write(10,2222)time,"  ",(Nocc(i)/Npar,"  ",i=1,Morb),Energy                                               
  ELSE
   DO Q=1,BlockSize 
    write(9+Q,2222) time,"  ",(Nocc_Block(i,Q)/Npar, &                                                         
                     "  ",i=1,Morb),Energy_Block(Q)                                                            
   END DO                                                                                                     
  ENDIF                                                                                                       
!=========================================================================
ENDIF
!=========================================================================
2222  format((999(G26.16,a3)))

End Subroutine Write_Occupations

END Module Integration_Steps
