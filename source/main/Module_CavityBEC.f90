!>GPLv3
!    MCTDH-X: the multiconfigurational time-dependent Hartree for 
!    indistinguishable particles software
!
!    Copyright (C) 2021  A. U. J. Lode, M. C. Tsatsos, 
!                        E. Fasshauer, S. E. Weiner, 
!                        R. Lin, L. Papariello, P. Molignini, 
!                        C. Lévêque, M. Büttner , J. Xiang, S. Dutta
!
!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <https://www.gnu.org/licenses/>.
!
!
!
!
!<GPLv3










Module CavityBEC

USE Cavity_Functions 
IMPLICIT NONE

!COMPLEX*16,ALLOCATABLE,PUBLIC :: Custom_Cavity_Profile(:,:), Custom_Pump_Profile(:,:)
!COMPLEX*16,ALLOCATABLE,PUBLIC :: Custom_Interfere_Profile_Re(:,:), Custom_Interfere_Profile_Im(:,:)
REAL*8,ALLOCATABLE,PUBLIC :: Pump_Profile_1D(:,:)


Contains

!> @ingroup cavity
!> @brief This routine evaluates the current pump laser power.
!!
!! This routine loops over the number of cavity modes and if the Pump_Switch for that mode is
!! set to true, meaning if we choose to smoothly switch on cavity pump lasers for that mode, then
!! the routine goes through a control sequence that chooses the correct ramping function for the pump
!! (the choices are exponential, linear and s-shaped) and calculates the current pumprate based on that
!! function, the time step and the current value of the function. \n
!! If Pump_Switch is not set to true, this routine simply reads out the cavity pump
!! strength that is specified in the input.
!!
!! @param[inout] Current_Cavity_PumpRate : current cavity pump rate
!! @param[in] Time : current time of the rampup procedure of the pump
subroutine Get_Current_Cavity_PumpRate(Current_Cavity_PumpRate,Time)

USE Global_Parameters
USE Crab_Parameters, ONLY: OCT_CurrentGamma

REAL*8 :: Current_Cavity_PumpRate(NCavity_Modes)
REAL*8 :: Time 
REAL*8 :: dt_OCT
Integer :: N_Steps, ITurn, L

DO L=1,NCavity_Modes
 If (Pump_Switch(L).eqv..TRUE.) then 
  If (trim(Pump_Switch_Mode).eq.'exp') Then 
   If (Time.lt.RampupTime(L)) THEN 
      IF ((trim(whichpot).ne.'OCT_CavityC').and.(trim(whichpot).ne.'OCT_CavityF')) THEN 
         Current_Cavity_PumpRate(L)= Cavity_Pumprate(L)*( exp(Time/(RampupTime(L)/log(2.d0)))-1 )
      ELSE 
         dt_OCT=(Time_Final-Time_Begin)/OCT_TimeSteps
         N_steps=NInt(time/dt_OCT)+1
         IF (N_Steps.gt.OCT_TimeSteps) N_Steps=OCT_TimeSteps
         Current_Cavity_PumpRate(L)= Cavity_Pumprate(L)*( exp(Time/(RampupTime(L)/log(2.d0)))-1 )+OCT_CurrentGamma(N_Steps,1)
      ENDIF
   Elseif ((Time.ge.RampupTime(L)).and.(Time.le.RampupTime(L)+PlateauTime(L))) then 
      If (Pump_Oscillate(L).eqv..FALSE.) THEN 
         Current_Cavity_PumpRate(L) = Cavity_Pumprate(L)
      Elseif (Pump_Oscillate(L).eqv..TRUE.) THEN 
         Current_Cavity_PumpRate(L) = Cavity_PumpRate(L) + Pump_Amplitude(L)*sin(2.d0*PI/Pump_Period(L)*(Time-RampupTime(L)))
      Endif
   Elseif (Time.gt.RampupTime(L)+PlateauTime(L)) then 
      Current_Cavity_PumpRate(L)= Cavity_Pumprate(L)*(exp((-Time+(RampupTime(L)+PlateauTime(L)))/((-RampdownTime(L))/log(0.01))))
   Endif
   
   Elseif (trim(Pump_Switch_Mode).eq.'linear') Then 
       If((Pump_SwitchTime(1).ne.0.d0).or.(Pump_SwitchTime(2).eq.0.d0)) then 
          Write(6,*) "The first entry of Pump_SwitchTime should be 0.d0, but the second entry should NOT be 0.d0 "
          Stop 
       Endif
 

       Do ITurn = 2,Size(Pump_SwitchTime)

          If(Time.lt.Pump_SwitchTime(ITurn)) then 

              Current_Cavity_PumpRate(L) = Pump_SwitchValue(iTurn-1)+(Pump_SwitchValue(iTurn)-Pump_SwitchValue(iTurn-1))&
              *(time-Pump_SwitchTime(iTurn-1))/(Pump_SwitchTime(iTurn)-Pump_SwitchTime(iTurn-1))

            Exit 

          Endif

       Enddo


   Elseif (trim(Pump_Switch_Mode).eq.'Vp_linear') Then 
      If((Pump_SwitchTime(1).ne.0.d0).or.(Pump_SwitchTime(2).eq.0.d0)) then 
         Write(6,*) "The first entry of Pump_SwitchTime should be 0.d0, but the second entry should NOT be 0.d0 "
         Stop 
      Endif
   
  
         Do ITurn = 2,Size(Pump_SwitchTime)
  
            If(Time.lt.Pump_SwitchTime(ITurn)) then 
               
               ! The funciton sign(1.0, CavityAtom_Coupling(L)) gives +1 or -1 dependeing of the sign og the CavityAtom_Coupling U0
               !  sign(1.0d0, CavityAtom_Coupling(L))*
               Current_Cavity_PumpRate(L) = sqrt(Pump_SwitchValue(iTurn-1)**2&
               +(Pump_SwitchValue(iTurn)**2-Pump_SwitchValue(iTurn-1)**2)&
              *(time-Pump_SwitchTime(iTurn-1))/(Pump_SwitchTime(iTurn)-Pump_SwitchTime(iTurn-1)))

              Exit 
  
            Endif
  
         Enddo


   Elseif (trim(Pump_Switch_Mode).eq.'s_shape') Then 
      If (Time.lt.Pump_SwitchTime(1)) then 
            Current_Cavity_PumpRate(L) = Pump_SwitchValue(1)*sqrt(3*(time/Pump_SwitchTime(1))**2-2*(time/Pump_SwitchTime(1))**3)
      Else 
            Current_Cavity_PumpRate(L) = Pump_SwitchValue(1)
      EndIf

    Else 
       Write(6,*) "The Pump_Switch_Mode ",Pump_Switch_Mode," has not been implemented yet."
    Endif
    

 else 
   Current_Cavity_PumpRate(L)=Cavity_PumpRate(L)
 endif
End DO

end subroutine Get_Current_Cavity_PumpRate




!> @ingroup cavity
!> @brief This routine evaluates the current cavity detuning.
!!
!! If the cavity detuning is not somehow dynamically ramped up, this just returns 
!! the user defined value of the input. If the detuning is however ramped up, the 
!! respective change is calculated here for an input-specified ramp-shape at the 
!! given time.
!!
!! @param[in] time : current time
!! @param[inout] Current_Cavity_Detuning : cavity detuning at the beginning and then updated here
subroutine Get_Current_Cavity_Detuning(Current_Cavity_Detuning,Time)

USE Global_Parameters
USE Crab_Parameters, ONLY: OCT_CurrentGamma

REAL*8 :: Current_Cavity_Detuning(NCavity_Modes), Time
REAL*8, DIMENSION(10) :: SwitchValue, SwitchTime
REAL*8 :: dt_OCT
Integer :: N_Steps, ITurn, L

If (Detuning_Switch.eqv..TRUE.) then

    If (Detuning_Switch_Mode.eq.'linear') then
       If((Detuning_SwitchTime(1).ne.0.d0).or.(Detuning_SwitchTime(2).eq.0.d0)) then
          Write(6,*) "The first entry of Cavity_Detuning_SwitchTime should be 0.d0, but the second entry should NOT be 0.d0 "
          Stop
       Endif
 
       SwitchValue = Detuning_SwitchValue
       SwitchTime  = Detuning_SwitchTime

       Do ITurn = 2,Size(SwitchTime)

          If(Time.lt.SwitchTime(ITurn)) then

            DO L=1,NCavity_Modes
              Current_Cavity_Detuning(L) = SwitchValue(iTurn-1)+(SwitchValue(iTurn)-SwitchValue(iTurn-1))&
              *(time-SwitchTime(iTurn-1))/(SwitchTime(iTurn)-SwitchTime(iTurn-1))
            Enddo

            Exit

          Endif

       Enddo

    Else
       Write(6,*) "The Detuning_Switch_Mode ",Detuning_Switch_Mode," has not been implemented yet."
    Endif
Else
   DO L=1,NCavity_Modes
     Current_Cavity_Detuning(L)=Cavity_Detuning(L)
   END DO
Endif


end subroutine Get_Current_Cavity_Detuning


!> @ingroup cavity
!> @brief  This routine evaluates the action of the cavity field on the atomic sample
!> through the dipole force. 
!> The cavity field is updated using the ABM integrator and the one-body potential
!> of the atoms is updated using the cavity mode and pump laser intensity profiles
!> defined through the input.
Subroutine Get_CavityAction(Time,OneBodyPotential,CavityField)

USE Global_Parameters
USE DVR_Parameters
USE Auxiliary_Routines
Use Coefficients_Parameters,Only:NPar

COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z),INTENT(INOUT) :: OneBodyPotential
COMPLEX*16, DIMENSION(NCavity_Modes),INTENT(INOUT) :: CavityField
COMPLEX*16, DIMENSION(NCavity_Modes) :: CavityField_In,CavityField_Out
COMPLEX*16 :: Pump_Profile,Cavity_Profile
REAL*8,INTENT(IN) :: Time
REAL*8, DIMENSION(NCavity_Modes) :: Current_Cavity_PumpRate,U0,V0,T0

INTEGER*8 :: I,J,K,L,M


IF ((DIM_MCTDH.eq.1).and.(Interaction_Type.eq.8)) THEN
   CavityField_In=CavityField
   Call Propagate_CavityField(Time,Integration_Stepsize,CavityField_In,CavityField_Out)
   CavityField=CavityField_Out
   RETURN
ENDIF

call Get_Current_Cavity_PumpRate(Current_Cavity_PumpRate,Time)

DO L=1,NCavity_Modes
  V0(L)=Current_Cavity_PumpRate(L) ! Current_Cavity_PumpRate(L)*CavityAtom_Coupling(L)/Atom_Detuning(L)!/sqrt(Npar*1.d0)
  U0(L)=CavityAtom_Coupling(L)
  T0(L)=Current_Cavity_PumpRate(L)**2/CavityAtom_Coupling(L)
END DO

IF (abs(Time-Time_Begin).lt.1.d-14) THEN
   Cavity_Field=0.d0
   OPEN(UNIT=16, FILE='CavityField.dat')
   OPEN(UNIT=17, FILE='AtomCavityExpectation.dat')
   OPEN(UNIT=18, FILE='AtomPumpExpectation.dat')
ENDIF

WRITE(16,*) Time, REALPART(CavityField), IMAGPART(CavityField)
WRITE(17,*) Time, Atom_CavityExpectation
WRITE(18,*) Time, REALPART(Atom_PumpExpectation), IMAGPART(Atom_PumpExpectation)

CavityField_In=CavityField
Call Propagate_CavityField(Time,Integration_Stepsize,CavityField_In,CavityField_Out)
CavityField=CavityField_Out

IF (abs(Time-Time_Final) .lt. 1.d-14) THEN
   WRITE(16,*) Time, REALPART(CavityField), IMAGPART(CavityField)
   WRITE(17,*) Time, Atom_CavityExpectation
   WRITE(18,*) Time, REALPART(Atom_PumpExpectation), IMAGPART(Atom_PumpExpectation)
ENDIF

IF (Custom_Cavity.eqv..FALSE.) THEN
   IF (Interaction_Type.ne.8) THEN
      IF (DIM_MCTDH.eq.1) THEN
        DO L=1,NCavity_Modes
            DO M=1,NDVR_X*NDVR_Y*NDVR_Z

               call get_ijk_from_m(M,NDVR_X,NDVR_Y,i,j,k)

               OneBodyPotential(M)=OnebodyPotential(M)&
                        +CONJG(CavityField(L))*CavityField(L)&
                        *U0(L) &
                        *(cos((Cavity_K0(L))*Ort_X(I)))**2&
                        +(conjg(CavityField(L))+CavityField(L))&
                        *V0(L)&
                        *cos((Cavity_K0(L))*Ort_X(I))
 
            END DO
         END DO
 
      ELSE IF (DIM_MCTDH.eq.2) THEN
        DO L=1,NCavity_Modes
            DO M=1,NDVR_X*NDVR_Y*NDVR_Z
               call get_ijk_from_m(M,NDVR_X,NDVR_Y,i,j,k)
           
               Pump_Profile=cos((Cavity_K0(L))*Ort_Y(J))&
                      *exp(-(Ort_X(I)/X_Cavity_Pump_Waist(L))**2)
 
               Cavity_Profile=cos((Cavity_K0(L))*Ort_X(I))&
                      *exp(-(Ort_Y(I)/Cavity_Mode_Waist(L))**2)
  
               OneBodyPotential(M)=OnebodyPotential(M)&
                        +CONJG(CavityField(L))*CavityField(L)&
                        *U0(L) &
                        *Cavity_Profile**2&
                        +(conjg(CavityField(L))+CavityField(L))&
                        *V0(L)&
                        *Cavity_Profile*Pump_Profile&
                        +T0(L)*Pump_Profile**2
 
            END DO
         END DO

      ENDIF

    ELSEIF (Interaction_Type.eq.8) THEN

       IF (DIM_MCTDH.eq.1) THEN

          DO L=1,NCavity_Modes

             IF (Transverse_Pump(L).eqv..FALSE.) THEN

               DO I=1,NDVR_X
 
                  OneBodyPotential(I)=OnebodyPotential(I)&
                                     +T0(L)*Pump_Profile_1D(I,L)**2

               END DO

             ENDIF

          END DO

       ELSE IF (DIM_MCTDH.eq.2) THEN

          DO L=1,NCavity_Modes
             DO M=1,NDVR_X*NDVR_Y*NDVR_Z
                call get_ijk_from_m(M,NDVR_X,NDVR_Y,i,j,k)

                Pump_Profile=cos((Cavity_K0(L))*Ort_Y(J))&
                    *exp(-(Ort_X(I)/X_Cavity_Pump_Waist(L))**2)

                Cavity_Profile=cos((Cavity_K0(L))*Ort_X(I))&
                    *exp(-(Ort_Y(I)/Cavity_Mode_Waist(L))**2)

                OneBodyPotential(M)=OnebodyPotential(M)&
                                   +T0(L)*Pump_Profile**2

             END DO
          END DO

       ENDIF
    ENDIF

ELSEIF (Custom_Cavity.eqv..TRUE.) THEN
   IF (Interaction_Type.ne.8) THEN
   ! So far, all the cavity-atom implementations satisfy the following pattern,
   ! but there is no guarantee that this is also true for future cases
      IF (DIM_MCTDH.eq.1) THEN

         DO L=1,NCavity_Modes
            DO M=1,NDVR_X*NDVR_Y*NDVR_Z

  
               OneBodyPotential(M)=OnebodyPotential(M)&
                        +CONJG(CavityField(L))*CavityField(L)&
                        *U0(L) &
                        *(Custom_Cavity_Profile(M,L))**2&
                        +(conjg(CavityField(L))+CavityField(L))&
                        *V0(L)&
                        *Custom_Pump_Profile(M,L)
         
            END DO
         END DO
      ELSE IF (DIM_MCTDH.eq.2) THEN

         DO L=1,NCavity_Modes
            DO M=1,NDVR_X*NDVR_Y*NDVR_Z
           
               OneBodyPotential(M)=OnebodyPotential(M)&
                        +CONJG(CavityField(L))*CavityField(L)&
                        *U0(L) &
                        *Custom_Cavity_Profile(M,L)**2&
                        +(conjg(CavityField(L))+CavityField(L))&
                        *V0(L)&
                        *Custom_Interfere_Profile_Re(M,L)&
!                        *Custom_Cavity_Profile(M,L)*Custom_Pump_Profile(M,L)&
                        +(conjg(CavityField(L))-CavityField(L))&
                        *V0(L)&
!                        *Custom_Cavity_Profile(M,L)*Custom_Pump_Profile_Im(M,L)&
                        *Custom_Interfere_Profile_Im(M,L)&
                        +T0(L)*Custom_Pump_Profile(M,L)**2
 
            END DO
         END DO
      ENDIF

   ELSEIF (Interaction_Type.eq.8) THEN
      IF (DIM_MCTDH.eq.1) THEN
          !This case should've been covered before leading to a direct 
          !evaluation of the cavity. Just in case: 
          Write(6,*) "Something wrong in CavityAction!!!"
          STOP


      ELSE IF (DIM_MCTDH.eq.2) THEN

         DO L=1,NCavity_Modes
            DO M=1,NDVR_X*NDVR_Y*NDVR_Z
               call get_ijk_from_m(M,NDVR_X,NDVR_Y,i,j,k)
          
               OneBodyPotential(M)=OnebodyPotential(M)&
                        +CONJG(CavityField(L))*CavityField(L)&
                        *U0(L) &
                        *Custom_Cavity_Profile(M,L)**2&
                        +(conjg(CavityField(L))+CavityField(L))&
                        *V0(L)&
                        *Custom_Cavity_Profile(M,L)*Custom_Pump_Profile(M,L)&
                        +T0(L)*Custom_Pump_Profile(M,L)**2
 
            END DO
         END DO
      ENDIF
   ENDIF
ENDIF
end subroutine Get_CavityAction





!> @ingroup cavity
Subroutine Get_CavityAction_MPI_Part1(Time,CavityField)

USE Global_Parameters
USE DVR_Parameters
USE Auxiliary_Routines
Use Coefficients_Parameters,Only:NPar

!COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z),INTENT(INOUT) :: OneBodyPotential
COMPLEX*16, DIMENSION(NCavity_Modes),INTENT(INOUT) :: CavityField
COMPLEX*16, DIMENSION(NCavity_Modes) :: CavityField_In,CavityField_Out
COMPLEX*16 :: Pump_Profile,Cavity_Profile
REAL*8,INTENT(IN) :: Time
REAL*8, DIMENSION(NCavity_Modes) :: Current_Cavity_PumpRate,U0,V0,T0

INTEGER*8 :: I,J,K,L,M


IF ((DIM_MCTDH.eq.1).and.(Interaction_Type.eq.8)) THEN
   CavityField_In=CavityField
   Call Propagate_CavityField(Time,Integration_Stepsize,CavityField_In,CavityField_Out)
   CavityField=CavityField_Out
   RETURN
ENDIF

call Get_Current_Cavity_PumpRate(Current_Cavity_PumpRate,Time)

DO L=1,NCavity_Modes
  V0(L)=Current_Cavity_PumpRate(L) ! Current_Cavity_PumpRate(L)*CavityAtom_Coupling(L)/Atom_Detuning(L)!/sqrt(Npar*1.d0)
  U0(L)=CavityAtom_Coupling(L)
  T0(L)=Current_Cavity_PumpRate(L)**2/CavityAtom_Coupling(L)
END DO

IF (abs(Time-Time_Begin).lt.1.d-14) THEN
   Cavity_Field=0.d0
   OPEN(UNIT=16, FILE='CavityField.dat')
   OPEN(UNIT=17, FILE='AtomCavityExpectation.dat')
   OPEN(UNIT=18, FILE='AtomPumpExpectation.dat')
ENDIF

WRITE(16,*) Time, REALPART(CavityField), IMAGPART(CavityField)
WRITE(17,*) Time, Atom_CavityExpectation
WRITE(18,*) Time, REALPART(Atom_PumpExpectation), IMAGPART(Atom_PumpExpectation)

CavityField_In=CavityField
Call Propagate_CavityField(Time,Integration_Stepsize,CavityField_In,CavityField_Out)
CavityField=CavityField_Out

IF (abs(Time-Time_Final) .lt. 1.d-14) THEN
   WRITE(16,*) Time, REALPART(CavityField), IMAGPART(CavityField)
   WRITE(17,*) Time, Atom_CavityExpectation
   WRITE(18,*) Time, REALPART(Atom_PumpExpectation), IMAGPART(Atom_PumpExpectation)
ENDIF

end subroutine Get_CavityAction_MPI_Part1


!> @ingroup cavity
Subroutine Get_CavityAction_MPI_Part2(Time,OneBodyPotential,CavityField)

USE Global_Parameters
USE DVR_Parameters
USE Auxiliary_Routines
Use Coefficients_Parameters,Only:NPar

COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z),INTENT(INOUT) :: OneBodyPotential
COMPLEX*16, DIMENSION(NCavity_Modes),INTENT(INOUT) :: CavityField
COMPLEX*16, DIMENSION(NCavity_Modes) :: CavityField_In,CavityField_Out
COMPLEX*16 :: Pump_Profile,Cavity_Profile
REAL*8,INTENT(IN) :: Time
REAL*8, DIMENSION(NCavity_Modes) :: Current_Cavity_PumpRate,U0,V0,T0

INTEGER*8 :: I,J,K,L,M



call Get_Current_Cavity_PumpRate(Current_Cavity_PumpRate,Time)

DO L=1,NCavity_Modes
  V0(L)=Current_Cavity_PumpRate(L) ! Current_Cavity_PumpRate(L)*CavityAtom_Coupling(L)/Atom_Detuning(L)!/sqrt(Npar*1.d0)
  U0(L)=CavityAtom_Coupling(L)
  T0(L)=Current_Cavity_PumpRate(L)**2/CavityAtom_Coupling(L)
END DO

IF (Custom_Cavity.eqv..FALSE.) THEN
   IF (Interaction_Type.ne.8) THEN
      IF (DIM_MCTDH.eq.1) THEN
        DO L=1,NCavity_Modes
            DO M=1,NDVR_X*NDVR_Y*NDVR_Z

               call get_ijk_from_m(M,NDVR_X,NDVR_Y,i,j,k)

               OneBodyPotential(M)=OnebodyPotential(M)&
                        +CONJG(CavityField(L))*CavityField(L)&
                        *U0(L) &
                        *(cos((Cavity_K0(L))*Ort_X(I)))**2&
                        +(conjg(CavityField(L))+CavityField(L))&
                        *V0(L)&
                        *cos((Cavity_K0(L))*Ort_X(I))
 
            END DO
         END DO
 
      ELSE IF (DIM_MCTDH.eq.2) THEN
        DO L=1,NCavity_Modes
            DO M=1,NDVR_X*NDVR_Y*NDVR_Z
               call get_ijk_from_m(M,NDVR_X,NDVR_Y,i,j,k)
           
               Pump_Profile=cos((Cavity_K0(L))*Ort_Y(J))&
                      *exp(-(Ort_X(I)/X_Cavity_Pump_Waist(L))**2)
 
               Cavity_Profile=cos((Cavity_K0(L))*Ort_X(I))&
                      *exp(-(Ort_Y(I)/Cavity_Mode_Waist(L))**2)
  
               OneBodyPotential(M)=OnebodyPotential(M)&
                        +CONJG(CavityField(L))*CavityField(L)&
                        *U0(L) &
                        *Cavity_Profile**2&
                        +(conjg(CavityField(L))+CavityField(L))&
                        *V0(L)&
                        *Cavity_Profile*Pump_Profile&
                        +T0(L)*Pump_Profile**2
 
            END DO
         END DO

      ENDIF

    ELSEIF (Interaction_Type.eq.8) THEN

       IF (DIM_MCTDH.eq.1) THEN

          DO L=1,NCavity_Modes

             IF (Transverse_Pump(L).eqv..FALSE.) THEN

               DO I=1,NDVR_X
 
                  OneBodyPotential(I)=OnebodyPotential(I)&
                                     +T0(L)*Pump_Profile_1D(I,L)**2

               END DO

             ENDIF

          END DO

       ELSE IF (DIM_MCTDH.eq.2) THEN

          DO L=1,NCavity_Modes
             DO M=1,NDVR_X*NDVR_Y*NDVR_Z
                call get_ijk_from_m(M,NDVR_X,NDVR_Y,i,j,k)

                Pump_Profile=cos((Cavity_K0(L))*Ort_Y(J))&
                    *exp(-(Ort_X(I)/X_Cavity_Pump_Waist(L))**2)

                Cavity_Profile=cos((Cavity_K0(L))*Ort_X(I))&
                    *exp(-(Ort_Y(I)/Cavity_Mode_Waist(L))**2)

                OneBodyPotential(M)=OnebodyPotential(M)&
                                   +T0(L)*Pump_Profile**2

             END DO
          END DO

       ENDIF
    ENDIF

ELSEIF (Custom_Cavity.eqv..TRUE.) THEN
   IF (Interaction_Type.ne.8) THEN
   ! So far, all the cavity-atom implementations satisfy the following pattern,
   ! but there is no guarantee that this is also true for future cases
      IF (DIM_MCTDH.eq.1) THEN

         DO L=1,NCavity_Modes
            DO M=1,NDVR_X*NDVR_Y*NDVR_Z

  
               OneBodyPotential(M)=OnebodyPotential(M)&
                        +CONJG(CavityField(L))*CavityField(L)&
                        *U0(L) &
                        *(Custom_Cavity_Profile(M,L))**2&
                        +(conjg(CavityField(L))+CavityField(L))&
                        *V0(L)&
                        *Custom_Pump_Profile(M,L)
         
            END DO
         END DO
      ELSE IF (DIM_MCTDH.eq.2) THEN

         DO L=1,NCavity_Modes
            DO M=1,NDVR_X*NDVR_Y*NDVR_Z
           
               OneBodyPotential(M)=OnebodyPotential(M)&
                        +CONJG(CavityField(L))*CavityField(L)&
                        *U0(L) &
                        *Custom_Cavity_Profile(M,L)**2&
                        +(conjg(CavityField(L))+CavityField(L))&
                        *V0(L)&
                        *Custom_Interfere_Profile_Re(M,L)&
!                        *Custom_Cavity_Profile(M,L)*Custom_Pump_Profile(M,L)&
                        +(conjg(CavityField(L))-CavityField(L))&
                        *V0(L)&
!                        *Custom_Cavity_Profile(M,L)*Custom_Pump_Profile_Im(M,L)&
                        *Custom_Interfere_Profile_Im(M,L)&
                        +T0(L)*Custom_Pump_Profile(M,L)**2
 
            END DO
         END DO
      ENDIF

   ELSEIF (Interaction_Type.eq.8) THEN
      IF (DIM_MCTDH.eq.1) THEN
          !This case should've been covered before leading to a direct 
          !evaluation of the cavity. Just in case: 
          Write(6,*) "Something wrong in CavityAction!!!"
          STOP


      ELSE IF (DIM_MCTDH.eq.2) THEN

         DO L=1,NCavity_Modes
            DO M=1,NDVR_X*NDVR_Y*NDVR_Z
               call get_ijk_from_m(M,NDVR_X,NDVR_Y,i,j,k)
          
               OneBodyPotential(M)=OnebodyPotential(M)&
                        +CONJG(CavityField(L))*CavityField(L)&
                        *U0(L) &
                        *Custom_Cavity_Profile(M,L)**2&
                        +(conjg(CavityField(L))+CavityField(L))&
                        *V0(L)&
                        *Custom_Cavity_Profile(M,L)*Custom_Pump_Profile(M,L)&
                        +T0(L)*Custom_Pump_Profile(M,L)**2
 
            END DO
         END DO
      ENDIF
   ENDIF
ENDIF
end subroutine Get_CavityAction_MPI_Part2







!> @ingroup cavity
Subroutine Get_CavityAction_Multilevel(Time,OneBodyPotential,CavityField)
USE Global_Parameters
USE DVR_Parameters
USE Auxiliary_Routines
Use Coefficients_Parameters,Only:NPar

COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,NLevel),INTENT(INOUT) :: OneBodyPotential
COMPLEX*16, DIMENSION(NCavity_Modes),INTENT(INOUT) :: CavityField
COMPLEX*16, DIMENSION(NCavity_Modes) :: CavityField_In,CavityField_Out
COMPLEX*16 :: Pump_Profile,Cavity_Profile
REAL*8,INTENT(IN) :: Time
REAL*8 :: Current_Cavity_PumpRate(NCavity_Modes)
REAL*8 :: U0(2),V0

INTEGER*8 :: I,J,K,L,M,N


call Get_Current_Cavity_PumpRate(Current_Cavity_PumpRate,Time)

!eta
V0=Current_Cavity_PumpRate(1)   !Current_Cavity_PumpRate*CavityAtom_Coupling/CavityAtom_Detuning!/sqrt(Npar*1.d0)
!eta

!Us
U0(1:2)=Cavity_Multilevel_U(1:2) !CavityAtom_Coupling**2/CavityAtom_Detuning
!Us

!T0=Current_Cavity_PumpRate**2/CavityAtom_Detuning

IF (abs(Time-Time_Begin).lt.1.d-14) THEN
   Cavity_Field=0.d0
ENDIF

CavityField_In=CavityField
Call Propagate_CavityField(Time,Integration_Stepsize,CavityField_In,CavityField_Out)
CavityField=CavityField_Out

IF (DIM_MCTDH.eq.1) THEN

! Diagonal contribution: modification of potential
! Diagonal contribution: modification of potential

 IF (trim(Cavity_BEC_Nlevel_model).eq.'Piazza') THEN
  DO L=1,NCavity_Modes
    DO M=1,NDVR_X*NDVR_Y*NDVR_Z

      call get_ijk_from_m(M,NDVR_X,NDVR_Y,i,j,k)

      OneBodyPotential(M,1)=OnebodyPotential(M,1)&
                           +(conjg(CavityField(L))*CavityField(L))&
                           *U0(1)&
                           *(cos((L*Cavity_K0(L))*Ort_X(I)))**2&
                           +Cavity_AtomPotential_Detuning(1)


      OneBodyPotential(M,2)=OnebodyPotential(M,2)&
                           +(conjg(CavityField(L))*CavityField(L))&
                           *U0(2)&
                           *(cos((L*Cavity_K0(L))*Ort_X(I)))**2&
                           +Cavity_AtomPotential_Detuning(2)



    END DO
  END DO

 ELSE

    write(6,*) 'Cavity_BEC_Nlevel_model = ',Cavity_BEC_Nlevel_model,' is not implemented for 1D.'
    STOP

 ENDIF

ELSE IF ((DIM_MCTDH.eq.2).and.(NLevel.eq.2)) THEN

 IF (trim(Cavity_BEC_Nlevel_model).eq.'RosaMedina') THEN
   !For this case we should do nothing here.
 ELSE
   write(6,*) 'Cavity_BEC_Nlevel_model = ',Cavity_BEC_Nlevel_model,' is not implemented for 2D.'
   STOP
 ENDIF



ELSE IF ((DIM_MCTDH.ge.3).or.(NLevel.ne.2)) THEN

   Write(6,*) "Cavity has not been implemented for D>2 multilevel systems!!!"
   Write(6,*) "Cavity has not been implemented for Nlevel>2 multilevel systems!!!"
   STOP

ENDIF

end subroutine Get_CavityAction_Multilevel

!> @ingroup cavity
subroutine Get_CavityAction_ComponentCoupling_Multilevel(PSI,PSIIN,Time)

USE Global_Parameters
USE DVR_Parameters
USE Auxiliary_Routines

IMPLICIT NONE

COMPLEX*16 :: PSI(NDVR_X*NDVR_Y*NDVR_Z,NLevel)
COMPLEX*16 :: PSIIN(NDVR_X*NDVR_Y*NDVR_Z,NLevel)
REAL*8 :: Time

INTEGER*8 :: I,J,K,L,M

REAL*8 :: Current_Cavity_PumpRate(NCavity_Modes)
REAL*8 :: U0(2),V0
REAL*8 :: Phi

call Get_Current_Cavity_PumpRate(Current_Cavity_PumpRate,Time)

!eta
V0=Current_Cavity_PumpRate(1)   !Current_Cavity_PumpRate*CavityAtom_Coupling/CavityAtom_Detuning!/sqrt(Npar*1.d0)
!eta

! Off-diagonal Contribution: coupling different components of the orbitals
! Off-diagonal Contribution: coupling different components of the orbitals
IF ((DIM_MCTDH.eq.1).and.(NLevel.eq.2)) THEN

IF (trim(Cavity_BEC_Nlevel_model).eq.'Piazza') THEN

DO L=1,NCavity_Modes
   DO M=1,NDVR_X*NDVR_Y*NDVR_Z

      call get_ijk_from_m(M,NDVR_X,NDVR_Y,i,j,k)

      PSI(M,1)=PSI(M,1) &
              +PSIIN(M,2) &
              *(CONJG(Cavity_Field(L))+Cavity_Field(L))&
              *V0 &
              *(cos((L*Cavity_K0(L))*Ort_X(I)))

      PSI(M,2)=PSI(M,2) &
              +PSIIN(M,1) &
              *(CONJG(Cavity_Field(L))+Cavity_Field(L))&
              *V0 &
              *(cos((L*Cavity_K0(L))*Ort_X(I)))

   END DO
END DO

ELSE

    write(6,*) 'Cavity_BEC_Nlevel_model = ',Cavity_BEC_Nlevel_model,' is not implemented for 1D.'
    STOP
ENDIF

ELSE IF ((DIM_MCTDH.eq.2).and.(NLevel.eq.2)) THEN

 IF ((trim(Cavity_BEC_Nlevel_model).eq.'RosaMedina').and.(NCavity_Modes.eq.1)) THEN
   Phi = Cavity_Extra_Parameters(1)

   DO M=1,NDVR_X*NDVR_Y*NDVR_Z

      call get_ijk_from_m(M,NDVR_X,NDVR_Y,i,j,k)

      PSI(M,1)=PSI(M,1) &
              +PSIIN(M,2) &
              *(Cavity_Field(1)+CONJG(Cavity_Field(1)))&
              *V0 &
              *(cos(Cavity_K0(1)*Ort_X(I))) &
              *(cos(Cavity_K0(1)*Ort_Y(J)+Phi)) &
              +PSIIN(M,2) &
              *(CONJG(Cavity_Field(1))-Cavity_Field(1))&
              *V0 &
              *(cos(Cavity_K0(1)*Ort_X(I))) &
              *(sin(Cavity_K0(1)*Ort_Y(J)+Phi))


      PSI(M,2)=PSI(M,2) &
              +PSIIN(M,1) &
              *(Cavity_Field(1)+CONJG(Cavity_Field(1)))&
              *V0 &
              *(cos(Cavity_K0(1)*Ort_X(I))) &
              *(cos(Cavity_K0(1)*Ort_Y(J)+Phi)) &
              -PSIIN(M,1) &
              *(CONJG(Cavity_Field(1))-Cavity_Field(1))&
              *V0 &
              *(cos(Cavity_K0(1)*Ort_X(I))) &
              *(sin(Cavity_K0(1)*Ort_Y(J)+Phi))


   END DO

ELSE
   write(6,*) 'Cavity_BEC_Nlevel_model = ',Cavity_BEC_Nlevel_model,' is not implemented for 2D.'
   write(6,*) 'NCavity_Modes greater than 1 is not implemented for 2D.'
   STOP
 ENDIF

ELSE IF ((DIM_MCTDH.gt.2).or.(NLevel.ne.2)) THEN


    Write(6,*) "Cavity has not been implemented for D>2 multilevel systems!!!"
    Write(6,*) "Cavity has not been implemented for Nlevel>2 multilevel systems!!!"
    STOP


ENDIF



end subroutine Get_CavityAction_ComponentCoupling_Multilevel
 

!> @ingroup cavity
!! @brief This routine evaluates the right-hand side of the cavity 
!! mode equation of motion.
!!
!! @note The current implementation does not use the actual cavity field \alpha but 
!! instead its expectation value.
!!
!! The cavity mode equation of motion can take different form depending on the system
!!
!! <b> Example: </b>
!!
!! \f$ \partial_t \alpha = \bigg[\mathrm{i} (\Delta_c-N_{3D}U_0B)
!! -\kappa\bigg] \alpha - \mathrm{i}\sqrt{\frac{E_p |U_0|}{\hbar}} \theta \f$
!!
!! or 
!!
!! \f$ \begin{split}
!! \mathrm{i} \partial_t \alpha &= \bigg[ - \Delta_c+ \overbrace{\sum_{kq}^M \rho(t)U_{kq}(t)}^{\text{atom cavity expectation value}} \\
!!  &\qquad - \mathrm{i} \kappa \bigg] \alpha(t) + \underbrace{\sum_{kq}^M \rho(t)\eta_{kq}(t)}_{\text{atom pump expectation value}}
!! \end{split} \f$ 
!!
!! which in Code translates to
!!
!!      CavityField_Out(L)=Job_Prefactor*((CavityField_In(L))*((-1.d0*Current_Cavity_Detuning(L))&
!!                                         +Atom_CavityExpectation(L) &
!!                                         -cmplx(0.d0,1.d0)*Cavity_LossRate(L))&
!!                                         +Atom_PumpExpectation(L))
!!
!! @param[in] Time : time
!! @param[in] CavityField_In : 
!! @param[inout] CavityField_Out : 
!! @param[in] static :
Subroutine Func_Cavity(Time,CavityField_In,CavityField_Out,static)

USE Global_Parameters
USE Matrix_Elements,ONLY:Full_Rho1_Elements
use Coefficients_Parameters,ONLY:Npar
USE DVR_Parameters

Logical, Optional :: Static
REAL*8  :: Time
COMPLEX*16, DIMENSION(NCavity_Modes) :: CavityField,CavityField_In,CavityField_Out
INTEGER :: I,N,O,M,J,K,L

REAL*8, DIMENSION(NCavity_Modes) :: Current_Cavity_Detuning


CALL Get_Current_Cavity_Detuning(Current_Cavity_Detuning,Time)

IF ((Interaction_Type.eq.8).and.(Job_Prefactor.eq.dcmplx(-1.d0,0.d0))) THEN

     !Calculate the caviy field from other parameters as in the Mottl thesis 
     DO L=1,NCavity_Modes
           CavityField_Out(L)=Atom_PumpExpectation(L)&
                         /(Current_Cavity_Detuning(L)-BunchingParameter(L)&
                           +dcmplx(0.d0,1.d0)*Cavity_LossRate(L))
     END DO
 
     RETURN
 
END IF


If (Present(Static).eqv..TRUE.) THEN
  DO L=1,NCavity_Modes
      CavityField_In(L)=(-Atom_PumpExpectation(L))/&
                      ((-1.d0*Current_Cavity_Detuning(L)) &
                       +Atom_CavityExpectation(L) &
                       -dcmplx(0.d0,1.d0)*Cavity_LossRate(L))
  END DO
END IF

DO L=1,NCavity_Modes
      CavityField_Out(L)=Job_Prefactor*((CavityField_In(L))*((-1.d0*Current_Cavity_Detuning(L))&
                                         +Atom_CavityExpectation(L) &
                                         -cmplx(0.d0,1.d0)*Cavity_LossRate(L))&
                                         +Atom_PumpExpectation(L))
END DO
end subroutine Func_Cavity


!> @ingroup cavity
!> @brief This routine calls the ABM integrator to solve the cavity mode equation 
!! of motion.
!!
!! This subroutine mostly calls cavitybec::func_cavity but has some control structures to
!! choose the right input parameters for different possible system scenarios.
!!
!! @param[in] Time                  :
!! @param[in] IntegrationPeriod     :
!! @param[inout] CavityField_In     :
!! @param[inout] CavityField_Out    : 
!!
!! @return
Subroutine Propagate_CavityField(Time,IntegrationPeriod,CavityField_In,CavityField_Out)

USE Global_Parameters
USE ABM_Integrators
USE DVR_Parameters
use Coefficients_Parameters,ONLY:Npar

IMPLICIT NONE

Real*8 :: IntegrationPeriod,Step
Real*8 :: Time, ABMTime

Real*8 :: TolError
COMPLEX*16  :: AuxPsi(NCavity_Modes,9) 
COMPLEX*16, DIMENSION(NCavity_Modes) :: CavityField_In,CavityField_Out

INTEGER ::IntOrder,NSteps,NRepeatedSteps,iErrorCode,Loop

Logical :: NewRestart
INTEGER :: L

Loop=1
TolError=Error_Tolerance
IntOrder=Orbital_Integrator_order
Step=IntegrationPeriod
ABMTime=Time

IF ((Interaction_Type.eq.8).and.(Job_Prefactor.eq.dcmplx(-1.d0,0.d0))) THEN
   CavityField_In=dcmplx(0.d0,0.d0)
   CavityField_Out=dcmplx(0.d0,0.d0)
   CALL  Func_Cavity(Time,CavityField_In,CavityField_Out)
   RETURN
ENDIF


IF ((Cavity_Initial_Guess.eqv..False.).AND. &
      (abs(Time-Time_Begin).lt.1.d-14)) THEN
   NewRestart=.TRUE.
   IF (Job_Prefactor.eq.dcmplx(0.d0,-1.d0)) THEN
     CavityField_In=dcmplx(0.d0,0.d0)
     CavityField_Out=dcmplx(0.d0,0.d0)
     Call Func_Cavity(Time,CavityField_In,CavityField_Out,.TRUE.)
     Write(6,*) "First ABM call for Cavity Field:", Time,CavityField_In,CavityField_Out
   ENDIF
ELSE                      
   NewRestart=.FALSE.
ENDIF

6666 Continue

IF ((Cavity_Initial_Guess.eqv..True.).and.(abs(Time-Time_Begin).lt.Cavity_Initial_Guess_Time)) THEN
   CavityField_In = Cavity_Initial_Guess_Value
   CavityField_Out = Cavity_Initial_Guess_Value
ELSE !Cavity_Initial_Guess.eqv..FALSE.

   NSteps=0
   NRepeatedSteps=10
   iErrorCode=0


   DO L=1,Loop
     IF (Job_Prefactor.eq.dcmplx(-1.d0,0.d0)) THEN
        Call Func_Cavity(Time,CavityField_In,CavityField_Out)
        call Complex_rk4vec ( Time, NCavity_Modes, CavityField_In, Step, Func_Cavity, CavityField_Out)
        CavityField_In=CavityField_Out
     ENDIF

     IF (Job_Prefactor.ne.dcmplx(-1.d0,0.d0)) THEN
        Cav_Integrator_Loop_Count = 0
        Call Func_Cavity(ABMTime,CavityField_In,CavityField_Out)
        call ABM(CavityField_In,CavityField_Out,NCavity_Modes,IntegrationPeriod,&
                  ABMTime,IntOrder, &
                  Step,TolError*Error_Rescale_Cavity,NewRestart,NSteps,NRepeatedSteps,&
                  iErrorCode,AuxPsi,Func_Cavity,AbsABMError)
     ENDIF     

     IF (iErrorCode.ne.0) THEN
        Exit
     ELSE
        ABMTime=ABMTime+IntegrationPeriod
     ENDIF
   END DO


if (iErrorCode.ne.0) THen
  IF (Job_Prefactor.ne.dcmplx(-1.d0,0.d0)) THEN
    ! Check if the IntegrationPeriod is below the minimal step, in case it becomes smaller than the machine precision
    ! If IntegrationPeriod has reached its minimal allowed value, the Error tolerace is increased instead
     If (IntegrationPeriod.gt.Cavity_Integrator_MinimalStep) Then
       Write(6,*) "Cavity-ABM Errorcode:", iErrorCode, "reducing time-step to:", IntegrationPeriod
       Step=Step/2.d0
       IntegrationPeriod=IntegrationPeriod/2.d0
       Loop=Loop*2
     ElseIf (Error_Rescale_Cavity.lt.Cavity_Error_Max) Then
        Error_Rescale_Cavity = Error_Rescale_Cavity*10
        write(6,*) "Cavity-ABM Errorcode:", iErrorCode, "increasing cavity error tolerance to:", TolError*Error_Rescale_Cavity
           If(Error_Rescale_Cavity.ge.Cavity_Error_Max) Then
              write(6,*) "Cavity error has reached its maximally allowed value:", Cavity_Error_Max, "Proceed to next step without guarantee in cavity error."
              go to 7777
            Endif
     Else
       go to 7777
     EndIf
       go to 6666
  ENDIF
7777    Continue
endif
ENDIF


End Subroutine Propagate_CavityField




!> @ingroup cavity
!> @brief This routine computes the expectation values with the atomic density
!> which are necessary to evaluate the right-hand side of the cavity equation of motion.
!! 
!! <b> Example: </b> In this [publication](https://arxiv.org/abs/1606.06058), the needed expecation values are
!! firstly, the expectation value of the cavity-atom coupling generated potential \f$U(\vec r)\f$,
!!
!! \f$  \begin{split}
!! \langle \Psi (t) | U(\mathbf r)|  \Psi (t) \rangle &= \sum_{k,q}^M \rho_{kq}(t) U_{kq}(t) \\
!! &= \sum_{k,q}^M \rho_{kq}(t) \langle \Phi_k | U(\mathbf r;t)|  \Phi_q \rangle  \\
!! &=\sum_{k,q}^M \rho_{kq}(t) \langle \Phi_k | U_0 \cos^2(k x)|  \Phi_q \rangle 
!! \end{split} \f$
!!
!! and secondly, the expectation value of a quantity proprtional to the atom pump
!! strength \f$ \eta \f$
!!
!! \f$  \begin{split}
!! \langle \Psi (t) | \eta(\mathbf r,t)|  \Psi (t) \rangle =& \sum_{k,q}^M \rho_{kq}(t) \eta_{kq}(t) \\
!! &= \sum_{k,q}^M \rho_{kq}(t) \langle \Phi_k | \eta(\mathbf r;t)|  \Phi_q \rangle  \\
!! &=\sum_{k,q}^M \rho_{kq}(t) \langle \Phi_k | V_0 \cos^2(k x)|  \Phi_q \rangle 
!! \end{split} \f$
!!
!! The first thing that this routine does is to call cavitybec::get_current_cavity_pumprate to
!! update the Current_Cavity_PumpRate. \n
!! Then the next step already is the control structure that leads to the system-adapted
!! calculation of the abovementioned expectation values. For our example, this looks as follows
!!
!! 
!!    DO M=1,NDVR_X*NDVR_Y*NDVR_Z
!!     
!!       CALL get_ijk_from_m(M,NDVR_X,NDVR_Y,i,j,k)
!! 
!!       DO N=1,Morb
!!          DO O=1,Morb
!!             DO L=1,NCavity_Modes
!!                Atom_CavityExpectation(L)=Atom_CavityExpectation(L)&
!!                            +U0(L)&
!!                            *Custom_Cavity_Profile(M,L)**2&
!!                            *Full_Rho1_Elements(N,O)*&
!!                            conjg(PSI_Cavity(M,N))*PSI_Cavity(M,O)
!! 
!!                Atom_PumpExpectation(L)=Atom_PumpExpectation(L)&
!!                            +V0(L)&
!!                            *Custom_Pump_Profile(M,L)&
!!                            *Full_Rho1_Elements(N,O)*&
!!                            conjg(PSI_Cavity(M,N))*PSI_Cavity(M,O)
!! 
!!             ENDDO
!!          ENDDO
!!       ENDDO  
!!    ENDDO
!!
!! Again, these expectation values are needed for the evaluation of the cavity EOMs.
!!
!! @param[inout] PSI_Cavity: cavity wavefunction
!! @param[in] Time: 
!!
!! @return Nothimg, instead results are saved to global variables Atom_CavityExpectation and
!! Atom_PumpExpectation
Subroutine Get_Atom_Pump_CavityExpectations(AtomCavityExpectation,&
                                            AtomPumpExpectation,&
                                            PSI_Cavity,&
                                            Full_Rho1_Elements,Time)

USE Global_Parameters,ONLY: NDVR_X,NDVR_Y,NDVR_Z,Morb,NCavity_Modes,&
                            CavityAtom_Coupling,Cavity_PumpRate,Cavity_K0,&
                            DIM_MCTDH,X_Cavity_Pump_Waist,Cavity_Mode_Waist,&
                            Pump_Switch,RampupTime,RampdownTime,PlateauTime,&
                            Cavity_Detuning,Custom_Cavity


USE DVR_Parameters
USE Auxiliary_Routines
Use Coefficients_Parameters,Only:NPar

IMPLICIT NONE

COMPLEX*16 :: Full_Rho1_Elements(Morb,Morb)
Real*8     :: AtomCavityExpectation(NCavity_Modes)
Complex*8     :: AtomPumpExpectation(NCavity_Modes)
Real*8     :: Time
Real*8, DIMENSION(NCavity_Modes) :: Current_Cavity_PumpRate,V0,U0,T0
COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb) :: PSI_Cavity
COMPLEX*16 :: Pump_Profile,Cavity_Profile

LOGICAL :: CC 

INTEGER*8 :: I,J,K,L,M,N,O

call Get_Current_Cavity_PumpRate(Current_Cavity_PumpRate,Time)

IF (Interaction_Type.ne.8) THEN
   V0=Current_Cavity_PumpRate(1:NCavity_Modes) ! Eta
   U0=CavityAtom_Coupling(1:NCavity_Modes)  ! U0  
ELSEIF (Interaction_Type.eq.8) THEN
   V0=Current_Cavity_PumpRate(1:NCavity_Modes)
ENDIF


AtomCavityExpectation=0.d0
AtomPumpExpectation=cmplx(0.d0,0.d0)

IF (Custom_Cavity.eqv..FALSE.) THEN

  IF ((DIM_MCTDH.eq.1).and.(Interaction_Type.ne.8)) THEN

   Do M=1,NDVR_X*NDVR_Y*NDVR_Z
 
     call get_ijk_from_m(M,NDVR_X,NDVR_Y,i,j,k)
 
     DO N=1,Morb
       DO O=1,Morb
         DO L=1,NCavity_Modes
 
            AtomCavityExpectation(L)=AtomCavityExpectation(L)&
                        +U0(L)&
                        *(COS(Cavity_K0(L)*Ort_X(I)))**2&
                        *Full_Rho1_Elements(N,O)*&
                        conjg(PSI_Cavity(M,N))*PSI_Cavity(M,O)
  
            AtomPumpExpectation(L)=AtomPumpExpectation(L)&
                        +V0(L)&
                        *COS(Cavity_K0(L)*Ort_X(I))&
                        *Full_Rho1_Elements(N,O)*&
                        conjg(PSI_Cavity(M,N))*PSI_Cavity(M,O)
  
  
           END DO
         END DO
       END DO
       
    END DO

  ELSE IF ((DIM_MCTDH.eq.2).and.(Interaction_Type.ne.8)) THEN

    Do M=1,NDVR_X*NDVR_Y*NDVR_Z
 
     call get_ijk_from_m(M,NDVR_X,NDVR_Y,i,j,k)
 
     DO N=1,Morb
       DO O=1,Morb
         DO L=1,NCavity_Modes
 
            Pump_Profile=cos((Cavity_K0(L))*Ort_Y(J))&
                        *exp(-(Ort_X(I)/X_Cavity_Pump_Waist(L))**2)
  
            Cavity_Profile=cos((Cavity_K0(L))*Ort_X(I))&
                        *exp(-(Ort_Y(J)/Cavity_Mode_Waist(L))**2)
  
            AtomCavityExpectation(L)=AtomCavityExpectation(L)&
                        +U0(L)&
                        *Cavity_Profile**2&
                        *Full_Rho1_Elements(N,O)*&
                        conjg(PSI_Cavity(M,N))*PSI_Cavity(M,O)
  
            AtomPumpExpectation(L)=AtomPumpExpectation(L)&
                        +V0(L)&
                        *Pump_Profile*Cavity_Profile&
                        *Full_Rho1_Elements(N,O)&
                        *conjg(PSI_Cavity(M,N))*PSI_Cavity(M,O)
  
  
           END DO
         END DO
       END DO
       
    END DO

  ENDIF

  IF ((DIM_MCTDH.eq.1).and.(Interaction_Type.eq.8)) THEN ! Cavity with cavity-mediated long-range interactions

     Do M=1,NDVR_X*NDVR_Y*NDVR_Z

        call get_ijk_from_m(M,NDVR_X,NDVR_Y,i,j,k)

        DO N=1,Morb
           DO O=1,Morb
              DO L=1,NCavity_Modes
 
                 AtomPumpExpectation(L)=AtomPumpExpectation(L)&
                        +V0(L)&
                        *(COS(Cavity_K0(L)*Ort_X(I)))&
                        *Full_Rho1_Elements(N,O)*&
                        conjg(PSI_Cavity(M,N))*PSI_Cavity(M,O)
         

              END DO
           END DO
        END DO
       
     END DO

  AtomCavityExpectation=0.d0

  ELSE IF ((DIM_MCTDH.eq.2).and.(Interaction_Type.eq.8)) THEN
    Do M=1,NDVR_X*NDVR_Y*NDVR_Z

     call get_ijk_from_m(M,NDVR_X,NDVR_Y,i,j,k)
 
     DO N=1,Morb
       DO O=1,Morb
         DO L=1,NCavity_Modes
 
            Pump_Profile=cos((Cavity_K0(L))*Ort_Y(J))&
                        *exp(-(Ort_X(I)/X_Cavity_Pump_Waist(L))**2)
  
            Cavity_Profile=cos((Cavity_K0(L))*Ort_X(I))&
                        *exp(-(Ort_Y(J)/Cavity_Mode_Waist(L))**2)
  
            AtomPumpExpectation(L)=AtomPumpExpectation(L)&
                        +V0(L)&
                        *Pump_Profile*Cavity_Profile&
                        *Full_Rho1_Elements(N,O)*&
                        conjg(PSI_Cavity(M,N))*PSI_Cavity(M,O)
  
           END DO
         END DO
       END DO
       
    END DO

  AtomCavityExpectation=0.d0

  ENDIF
ELSEIF (Custom_Cavity.eqv..TRUE.) THEN

!  IF ((ALLOCATED(Custom_Cavity_Profile).eqv..FALSE.).or.(ALLOCATED(Custom_Pump_Profile).eqv..FALSE.).or. &
!       (ALLOCATED(Custom_Interfere_Profile_Re).eqv..FALSE.).or.(ALLOCATED(Custom_Interfere_Profile_Im).eqv..FALSE.)) THEN

!      CC=Initialize_Custom_Cavity()
!      IF (ALLOCATED(Custom_Cavity_Profile).eqv..FALSE.) STOP "Allocation of cavity modes failed"
!      IF (ALLOCATED(Custom_Pump_Profile).eqv..FALSE.) STOP "Allocation of pump modes failed"
!      IF (ALLOCATED(Custom_Interfere_Profile_Re).eqv..FALSE.) STOP "Allocation of interference modes failed"
!      IF (ALLOCATED(Custom_Interfere_Profile_Im).eqv..FALSE.) STOP "Allocation of interference modes failed"

!  ENDIF 

!  CALL Get_Cavity_Functions(Time,Custom_Cavity_Profile,Custom_Pump_Profile,Custom_Interfere_Profile_Re,Custom_Interfere_Profile_Im,PSI_Cavity)
  CALL Get_Cavity_Functions(Time,PSI_Cavity)

  IF ((DIM_MCTDH.eq.1).and.(Interaction_Type.ne.8)) THEN
 

    Do M=1,NDVR_X*NDVR_Y*NDVR_Z
 
     call get_ijk_from_m(M,NDVR_X,NDVR_Y,i,j,k)
 
     DO N=1,Morb
       DO O=1,Morb
         DO L=1,NCavity_Modes
 
            AtomCavityExpectation(L)=AtomCavityExpectation(L)&
                        +U0(L)&
                        *Custom_Cavity_Profile(M,L)**2&
                        *Full_Rho1_Elements(N,O)*&
                        conjg(PSI_Cavity(M,N))*PSI_Cavity(M,O)
  
            AtomPumpExpectation(L)=AtomPumpExpectation(L)&
                        +V0(L)&
                        *Custom_Pump_Profile(M,L)&
                        *Custom_Cavity_Profile(M,L)&
                        *Full_Rho1_Elements(N,O)*&
                        conjg(PSI_Cavity(M,N))*PSI_Cavity(M,O)
  
  
           END DO
         END DO
       END DO
       
    END DO

  ELSEIF ((DIM_MCTDH.eq.2).and.(Interaction_Type.ne.8)) THEN

    Do M=1,NDVR_X*NDVR_Y*NDVR_Z
 
     DO N=1,Morb
       DO O=1,Morb
         DO L=1,NCavity_Modes
 
  
            AtomCavityExpectation(L)=AtomCavityExpectation(L)&
                        +U0(L)&
                        *Custom_Cavity_Profile(M,L)**2&
                        *Full_Rho1_Elements(N,O)*&
                        conjg(PSI_Cavity(M,N))*PSI_Cavity(M,O)
  
            AtomPumpExpectation(L)=AtomPumpExpectation(L)&
                        +V0(L)&
                        *(Custom_Interfere_Profile_Re(M,L)+Custom_Interfere_Profile_Im(M,L))&
                        *Full_Rho1_Elements(N,O)&
                        *conjg(PSI_Cavity(M,N))*PSI_Cavity(M,O)
  
  
           END DO
         END DO
       END DO
       
    END DO

  ELSEIF (Interaction_Type.eq.8) THEN

     !This case should've been covered before leading to a direct 
     !evaluation of the cavity. Just in case: 
     Write(6,*) "Interaction_Type=8 is not supported for the custom cavity treatment yet!!!"
     STOP


  ENDIF

ENDIF

End Subroutine Get_Atom_Pump_CavityExpectations 






!> @ingroup cavity
!> This routine computes the expectation values with the atomic density
!> which are necessary to evaluate the right-hand side of the cavity equation of motion.
Subroutine Get_Atom_Pump_CavityExpectations_Multilevel(AtomCavityExpectation,&
                                           AtomPumpExpectation,&
                                           PSI_Cavity,&
                                           Full_Rho1_Elements,Time)

USE Global_Parameters,ONLY: NDVR_X,NDVR_Y,NDVR_Z,Morb,NCavity_Modes,&
                           CavityAtom_Coupling,Cavity_PumpRate,Cavity_K0,&
                           DIM_MCTDH,X_Cavity_Pump_Waist,Cavity_Mode_Waist,&
                           Pump_Switch,RampupTime,RampdownTime,PlateauTime,&
                           Cavity_Multilevel_U,&
                           Cavity_AtomPotential_Detuning


USE DVR_Parameters
USE Auxiliary_Routines
Use Coefficients_Parameters,Only:NPar

COMPLEX*16 :: Full_Rho1_Elements(Morb,Morb)
Real*8     :: AtomCavityExpectation(NCavity_Modes)
Complex*8     :: AtomPumpExpectation(NCavity_Modes)
Real*8     :: Time
Real*8     :: Current_Cavity_PumpRate(NCavity_Modes),V0,T0
REAL*8     :: U0(2)
COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb,NLevel) :: PSI_Cavity
COMPLEX*16 :: Pump_Profile,Cavity_Profile
REAL*8 :: Phi

INTEGER*8 :: I,J,K,L,M,N,O

call Get_Current_Cavity_PumpRate(Current_Cavity_PumpRate,Time)

V0=Current_Cavity_PumpRate(1) !Current_Cavity_PumpRate*CavityAtom_Coupling/CavityAtom_Detuning!/sqrt(Npar*1.d0)
U0(1:2)=Cavity_Multilevel_U(1:2)   !CavityAtom_Coupling**2/CavityAtom_Detuning

AtomCavityExpectation=0.d0
AtomPumpExpectation=cmplx(0.d0,0.d0)

IF (DIM_MCTDH.eq.1) THEN

  Do M=1,NDVR_X*NDVR_Y*NDVR_Z

     call get_ijk_from_m(M,NDVR_X,NDVR_Y,i,j,k)

   DO N=1,Morb
     DO O=1,Morb
       DO L=1,NCavity_Modes

          If (trim(Cavity_BEC_Nlevel_model).eq.'Piazza')  Then
          AtomCavityExpectation(L)=AtomCavityExpectation(L)&
                      +U0(1)&
                      *(COS(Cavity_K0(L)*Ort_X(I)))**2&
                      *Full_Rho1_Elements(N,O)&
                      *conjg(PSI_Cavity(M,N,1))*PSI_Cavity(M,O,1)&
                      +U0(2)&
                      *(COS(Cavity_K0(L)*Ort_X(I)))**2&
                      *Full_Rho1_Elements(N,O)&
                      *conjg(PSI_Cavity(M,N,2))*PSI_Cavity(M,O,2)


          AtomPumpExpectation(L)=AtomPumpExpectation(L)&
                      +V0&
                      *COS(Cavity_K0(L)*Ort_X(I))&
                      *Full_Rho1_Elements(N,O)&
                      *(conjg(PSI_Cavity(M,N,1))*PSI_Cavity(M,O,2)&
                       +conjg(PSI_Cavity(M,N,2))*PSI_Cavity(M,O,1))

           Else
              write(6,*) 'Cavity_BEC_Nlevel_model = ',Cavity_BEC_Nlevel_model,' is not implemented for 1D.'
              STOP
           EndIf

         END DO
       END DO
     END DO

  END DO

ELSE IF ((DIM_MCTDH.eq.2).and.(NCavity_Modes.eq.1)) THEN
  
   Do M=1,NDVR_X*NDVR_Y*NDVR_Z
       call get_ijk_from_m(M,NDVR_X,NDVR_Y,i,j,k)
     DO N=1,Morb
      DO O=1,Morb

        If (trim(Cavity_BEC_Nlevel_model).eq.'RosaMedina')  Then
          Phi = Cavity_Extra_Parameters(1)
          AtomCavityExpectation(1)=0.d0

          AtomPumpExpectation(1)=AtomPumpExpectation(1)&
                      !-V0*cmplx(0.d0,1.d0)&
                      +V0&
                      *cos(Cavity_K0(1)*Ort_X(I))&
                      *cos(Cavity_K0(1)*Ort_Y(J)+Phi)&
                      *Full_Rho1_Elements(N,O)&
                      *((conjg(PSI_Cavity(M,N,2))*PSI_Cavity(M,O,1))&
                       +(conjg(PSI_Cavity(M,N,1))*PSI_Cavity(M,O,2)))&
                      +V0&
                      *cos(Cavity_K0(1)*Ort_X(I))&
                      *sin(Cavity_K0(1)*Ort_Y(J)+Phi)&
                      *Full_Rho1_Elements(N,O)&
                      *((conjg(PSI_Cavity(M,N,1))*PSI_Cavity(M,O,2))&
                       -(conjg(PSI_Cavity(M,N,2))*PSI_Cavity(M,O,1)))



       Else 
          write(6,*) 'Cavity_BEC_Nlevel_model = ',Cavity_BEC_Nlevel_model,' is not implemented for 2D.'
          STOP
       EndIf
 
     END DO
   END DO
  END DO

ELSE

  Write(6,*) "Multileveled atoms + cavity has not been implemented for D>2!!!"
  STOP

ENDIF

!Write(6,*) "rho1,u,eta", Full_Rho1_Elements,AtomCavityExpectation,AtomPumpExpectation

End Subroutine Get_Atom_Pump_CavityExpectations_Multilevel

!> @ingroup cavity
Subroutine Get_BunchingParameter(PSI_Cavity,&
                            Full_Rho1_Elements,Time)

USE Global_Parameters,ONLY: NDVR_X,NDVR_Y,NDVR_Z,Morb,NCavity_Modes,&
                            CavityAtom_Coupling,Cavity_PumpRate,Cavity_K0,&
                            DIM_MCTDH,X_Cavity_Pump_Waist,Cavity_Mode_Waist

USE DVR_Parameters
USE Auxiliary_Routines
Use Coefficients_Parameters,Only:NPar

COMPLEX*16 :: Full_Rho1_Elements(Morb,Morb)
Real*8     :: Time
Real*8,DIMENSION(NCavity_Modes)     :: U0
COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb) :: PSI_Cavity
COMPLEX*16 :: Pump_Profile,Cavity_Profile


INTEGER*8 :: I,J,K,L,M,N,O

LOGICAL :: CC

!U0=CavityAtom_Coupling**2/CavityAtom_Detuning
U0(1:NCavity_Modes)=CavityAtom_Coupling(1:NCavity_Modes)
BunchingParameter=0.d0

IF (DIM_MCTDH.eq.1) THEN

  IF (ALLOCATED(Pump_Profile_1D).eqv..FALSE.) THEN
      CC=Initialize_Pump_1D()
      IF (ALLOCATED(Pump_Profile_1D).eqv..FALSE.) STOP "Allocation of 1D pump failed"

      DO L=1,NCavity_Modes
        IF (Transverse_Pump(L).eqv..FALSE.) THEN 
           DO J=1,NDVR_X
             Pump_Profile_1D(J,L)=cos((Cavity_K0(L))*Ort_X(J))
           END DO
        ELSE
           DO J=1,NDVR_X
             Pump_Profile_1D(J,L)=1.d0
           END DO
        ENDIF 
      END DO

  ENDIF 

  Do M=1,NDVR_X*NDVR_Y*NDVR_Z

     call get_ijk_from_m(M,NDVR_X,NDVR_Y,i,j,k)

   DO N=1,Morb
     DO O=1,Morb
       DO L=1,NCavity_Modes

          BunchingParameter(L)=BunchingParameter(L)&
                      +U0(L)&
                      *(COS(Cavity_K0(L)*Ort_X(I)))**2&
                      *Full_Rho1_Elements(N,O)*&
                      conjg(PSI_Cavity(M,N))*PSI_Cavity(M,O)
 
         END DO
       END DO
     END DO
     
  END DO

ELSE IF (DIM_MCTDH.eq.2) THEN

  Do M=1,NDVR_X*NDVR_Y*NDVR_Z

   call get_ijk_from_m(M,NDVR_X,NDVR_Y,i,j,k)

   DO N=1,Morb
     DO O=1,Morb
       DO L=1,NCavity_Modes

          Cavity_Profile=cos((Cavity_K0(L))*Ort_X(I))&
                      *exp(-(Ort_Y(J)/Cavity_Mode_Waist(L))**2)
 
          BunchingParameter(L)=BunchingParameter(L)&
                      +U0(L)&
                      *Cavity_Profile**2&
                      *Full_Rho1_Elements(N,O)*&
                      conjg(PSI_Cavity(M,N))*PSI_Cavity(M,O)
 
         END DO
       END DO
     END DO
     
  END DO

ENDIF

end Subroutine Get_BunchingParameter

!> @ingroup cavity
!> @brief Called on by cavitybec::propagate_cavityfield to take one Runge-Kutta step for a vector ODE.
!!
!! @param[in] T0     :  the current time
!! @param[in] M      :  the dimension of the system
!! @param[in] U0     :  the solution estimate at the current time
!! @param[in] func   :  a subroutine of the form subroutine func ( t, u, uprime ) 
!!                      which evaluates the derivative UPRIME(1:M) given the time T and
!!                        solution vector U(1:M).
!! @param[out] U     : the fourth-order Runge-Kutta solution estimate at time T0+DT
subroutine Complex_rk4vec ( t0, m, u0, dt, func, u )

!*****************************************************************************80
!
!  Licensing:
!
!    This code is distributed under the GNU LGPL license. 
!
!  Modified:
!
!    08 October 2013, 14 January 2016: modified to complex version and 
!                                      func which has only 3 arguments
!
!  Author:
!
!    08 October 2013: John Burkardt, 14 January 2016: Axel U. J. Lode
!
 implicit none

 integer ( kind = 4 ) m

 real ( kind = 8 ) dt
 external func
 COMPLEX*16 f0(m)
 COMPLEX*16 f1(m)
 COMPLEX*16 f2(m)
 COMPLEX*16 f3(m)
 real ( kind = 8 ) t0
 real ( kind = 8 ) t1
 real ( kind = 8 ) t2
 real ( kind = 8 ) t3
 COMPLEX*16  u(m)
 COMPLEX*16 u0(m)
 COMPLEX*16 u1(m)
 COMPLEX*16 u2(m)
 COMPLEX*16 u3(m)
!
!  Get four sample values of the derivative.
!
  call func ( t0, u0, f0, .TRUE. )

  t1 = t0 + dt / 2.0D+00
  u1(1:m) = u0(1:m) + dt * f0(1:m) / 2.0D+00
  call func ( t1, u1, f1, .TRUE. )

  t2 = t0 + dt / 2.0D+00
  u2(1:m) = u0(1:m) + dt * f1(1:m) / 2.0D+00
  call func ( t2, u2, f2, .TRUE. )

  t3 = t0 + dt
  u3(1:m) = u0(1:m) + dt * f2(1:m)
  call func ( t1, u1, f3, .TRUE. )
!
!  Combine them to estimate the solution U at time T1.
!
  u(1:m) = u0(1:m) + dt * ( f0(1:m) + 2.0D+00 * f1(1:m) + 2.0D+00 * f2(1:m) &
    + f3(1:m) ) / 6.0D+00

  return
end subroutine Complex_rk4vec



Logical Function Initialize_Pump_1D()

USE Global_Parameters, ONLY: NDVR_X,NDVR_Y,NDVR_Z,NCavity_Modes

IMPLICIT NONE

ALLOCATE(Pump_Profile_1D(NDVR_X*NDVR_Y*NDVR_Z,NCavity_Modes))

Initialize_Pump_1D=.TRUE.

end Function Initialize_Pump_1D




!Logical Function Initialize_Custom_Cavity()

!USE Global_Parameters, ONLY: NDVR_X,NDVR_Y,NDVR_Z,NCavity_Modes

!IMPLICIT NONE

!ALLOCATE(Custom_Cavity_Profile(NDVR_X*NDVR_Y*NDVR_Z,NCavity_Modes))
!ALLOCATE(Custom_Pump_Profile(NDVR_X*NDVR_Y*NDVR_Z,NCavity_Modes))
!ALLOCATE(Custom_Interfere_Profile_Re(NDVR_X*NDVR_Y*NDVR_Z,NCavity_Modes))
!ALLOCATE(Custom_Interfere_Profile_Im(NDVR_X*NDVR_Y*NDVR_Z,NCavity_Modes))

!Write(6,*) "Custom cavity profile is NOW allocated"
!Initialize_Custom_Cavity=.TRUE.

!end Function Initialize_Custom_Cavity


end Module CavityBEC
