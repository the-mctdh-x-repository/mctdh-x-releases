!>GPLv3
!    MCTDH-X: the multiconfigurational time-dependent Hartree for 
!    indistinguishable particles software
!
!    Copyright (C) 2021  A. U. J. Lode, M. C. Tsatsos, 
!                        E. Fasshauer, S. E. Weiner, 
!                        R. Lin, L. Papariello, P. Molignini, 
!                        C. Lévêque, M. Büttner , J. Xiang, S. Dutta
!
!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <https://www.gnu.org/licenses/>.
!
!
!
!
!<GPLv3










MODULE Auxiliary_Routines
  USE   Global_Parameters
  USE   DVR_Parameters


CONTAINS


recursive subroutine QsortC_Pop(A,B,M)
  IMPLICIT NONE
  real*8, intent(in out), dimension(:) :: A
  real*8, intent(in out), dimension(M,size(A)) :: B
  integer, intent(IN) :: M
  integer :: iq

  if(size(A) > 1) then
     call Partition(A,B,M, iq)
     call QsortC_Pop(A(:iq-1),B(:,:iq-1),M)
     call QsortC_Pop(A(iq:),B(:,iq:),M)
  endif
end subroutine QsortC_Pop

subroutine Partition(A,B,M, marker)
  IMPLICIT NONE
  real*8, intent(in out), dimension(:) :: A
  real*8, intent(in out), dimension(M,size(A)) :: B
  integer, intent(out) :: marker
  integer, intent(IN) :: M

  integer :: i, j
  real*8 :: temp,temp1(M)
  real*8 :: x      ! pivot point
  x = A(1)
  i= 0
  j= size(A) + 1

  do
     j = j-1
     do
        if (A(j) <= x) exit
        j = j-1
     end do
     i = i+1
     do
        if (A(i) >= x) exit
        i = i+1
     end do
     if (i < j) then
        ! exchange A(i) and A(j)
        temp = A(i)
        temp1=(B(:,i))

        A(i) = A(j)
        B(:,i)=B(:,j)

        A(j) = temp
        B(:,j)= temp1
     elseif (i == j) then
        marker = i+1
        return
     else
        marker = i
        return
     endif
  end do

end subroutine Partition


!> Get the Natural Orbitals from a set of working orbitals, or 
SUBROUTINE GET_NOs(PSI,NOs,Natvec,reverse)
COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb), INTENT(IN)       :: PSI
COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb), INTENT(OUT)      :: NOs
COMPLEX*16, DIMENSION(MOrb,MOrb), INTENT(IN)              :: Natvec

LOGICAL, OPTIONAL :: REVERSE

COMPLEX*16, DIMENSION(MOrb,MOrb)              :: T_Natvec
INTEGER :: ind,K,J,I,k1,l1


IF ((present(reverse).eqv..FALSE.).or.((present(reverse).eqv..true.).and.(reverse.eqv..FALSE.))) THEN
   ind=1
   do K=1,NDVR_Z
     do J=1,NDVR_Y
       do I=1,NDVR_X
        Do k1=1,Morb
            NOs(ind,k1)=Zero
          Do l1=1,Morb
            NOs(ind,k1)=NOs(ind,k1)+Conjg(NatVec(l1,k1))*PSI(ind,l1) ! Natural orbitals 
          END DO
        END DO
        ind=ind+1
       enddo
     enddo
   enddo
ENDIF


IF ((present(reverse).eqv..TRUE.).and.(reverse.eqv..TRUE.)) THEN
   ind=1
   T_Natvec=TRANSPOSE(NatVec)
   do K=1,NDVR_Z
     do J=1,NDVR_Y
       do I=1,NDVR_X
        Do k1=1,Morb
            NOs(ind,k1)=Zero
          Do l1=1,Morb
            NOs(ind,k1)=NOs(ind,k1)+T_NatVec(l1,k1)*PSI(ind,l1) ! Natural orbitals 
          END DO
        END DO
        ind=ind+1
       enddo
     enddo
   enddo
ENDIF
END SUBROUTINE GET_NOs



!> According to the number of components, this routine applies spin matrices to 
!> the input spinor orbital Psi_in. The output is Psi_spin and direction
!> is an integer to select the spin matrix, 1->x;2->y;3->z.
      subroutine Apply_Spin(Psi_spin,Psi_in,direction)

      use Global_Parameters
      USE DVR_Parameters

      IMPLICIT NONE
 
      COMPLEX*16,DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Nlevel) :: psi_in
      COMPLEX*16,DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Nlevel) :: psi_spin
      INTEGER :: direction
      REAL*8  :: Spin

      IF (Nlevel.eq.3) THEN !! Spin-1 case

         If (direction.eq.1) THEN
            Psi_spin(:,1)=(1.d0/sqrt(2.d0))*Psi_in(:,2)
            Psi_spin(:,3)=(1.d0/sqrt(2.d0))*Psi_in(:,2)
            Psi_spin(:,2)=(1.d0/sqrt(2.d0))*(Psi_in(:,1)+Psi_in(:,3))
         ElseIf (direction.eq.2) THEN
            Psi_spin(:,1)=(dcmplx(0.d0,-1.d0)/sqrt(2.d0))*Psi_in(:,2)
            Psi_spin(:,3)=dcmplx(0.d0,(1.d0/sqrt(2.d0)))*Psi_in(:,2)
            Psi_spin(:,2)=(dcmplx(0.d0,1.d0)/sqrt(2.d0))*(Psi_in(:,1)-Psi_in(:,3))
         ElseIf (direction.eq.3) THEN
            Psi_spin(:,1)=Psi_in(:,1)
            Psi_spin(:,3)=-1.d0*Psi_in(:,3)
            Psi_spin(:,2)=dcmplx(0.d0,0.d0)
         Else 
            Stop "Spin 1 direction wrong !!!!?????"
         EndIf

      Elseif (Nlevel.eq.2) THEN !! Spin-1/2 case

          If (direction.eq.1) THEN
            Psi_spin(:,1)=0.5d0*Psi_in(:,2)
            Psi_spin(:,2)=0.5d0*Psi_in(:,1)
          ElseIf (direction.eq.2) THEN
            Psi_spin(:,1)=dcmplx(0.d0,-0.5d0)*Psi_in(:,2)
            Psi_spin(:,2)=dcmplx(0.d0,0.5d0)*Psi_in(:,1)
          ElseIf (direction.eq.3) THEN
            Psi_spin(:,1)=0.5d0*Psi_in(:,1)
            Psi_spin(:,2)=-0.5d0*Psi_in(:,2)
          Else
            Stop "Spin 1/2 direction wrong !!!?????"
          EndIf

      Elseif (Nlevel.eq.4) then !! 3/2 spin case
          If (direction.eq.1) THEN
               Psi_spin(:,1)=(sqrt(3.d0)/2.d0)*Psi_in(:,2)
               Psi_spin(:,2)=(sqrt(3.d0)/2.d0)*Psi_in(:,1) + 2.d0*Psi_in(:,3)
               Psi_spin(:,3)=2.d0*Psi_in(:,2)+(sqrt(3.d0)/2.d0)*Psi_in(:,4)
               Psi_spin(:,4)=(sqrt(3.d0)/2.d0)*Psi_in(:,3)
          ElseIf (direction.eq.2) THEN
               Psi_spin(:,1)=dcmplx(0.d0,sqrt(3.d0)/2.d0)*Psi_in(:,2)
               Psi_spin(:,2)=dcmplx(0.d0,-sqrt(3.d0)/2.d0)*Psi_in(:,1) + dcmplx(0.d0,2.d0)*Psi_in(:,3)
               Psi_spin(:,3)=dcmplx(0.d0,-2.d0)*Psi_in(:,2)+dcmplx(0.d0,sqrt(3.d0)/2.d0)*Psi_in(:,4)
               Psi_spin(:,4)=dcmplx(0,-sqrt(3.d0)/2.d0)*Psi_in(:,3)
          ElseIf (direction.eq.3) THEN
               Psi_spin(:,1)=1.5d0*Psi_in(:,1)
               Psi_spin(:,2)=0.5d0*Psi_in(:,2)
               Psi_spin(:,3)=-0.5d0*Psi_in(:,3)
               Psi_spin(:,4)=-1.5d0*Psi_in(:,4)
          EndIf
      Elseif (Nlevel.eq.5) THEN !! spin-2 case
          If (direction.eq.1) THEN
               Psi_spin(:,1)=0.5d0*(2.d0*Psi_in(:,2))
               Psi_spin(:,2)=0.5d0*(2.d0*Psi_in(:,1)+sqrt(6.d0)*Psi_in(:,3))
               Psi_spin(:,3)=0.5d0*(sqrt(6.d0)*(Psi_in(:,2)+Psi_in(:,4)))
               Psi_spin(:,4)=0.5d0*(sqrt(6.d0)*Psi_in(:,3) + 2.d0*Psi_in(:,5))
               Psi_spin(:,5)=0.5d0*(2.d0*Psi_in(:,4))
          ElseIf (direction.eq.2) THEN
               Psi_spin(:,1)=dcmplx(0.d0,1.d0)*Psi_in(:,2)
               Psi_spin(:,2)=dcmplx(0.d0,-1.d0)*Psi_in(:,1) + dcmplx(0.d0,sqrt(6.d0)/2)*Psi_in(:,3)
               Psi_spin(:,3)=dcmplx(0.d0,-sqrt(6.d0)/2.d0)*(Psi_in(:,2)-Psi_in(:,4))
               Psi_spin(:,4)=dcmplx(0,-sqrt(6.d0)/2.d0)*Psi_in(:,3)
               Psi_spin(:,5)=dcmplx(0,-2.d0)*Psi_in(:,4)
          ElseIf (direction.eq.3) THEN
               Psi_spin(:,1)=2.d0*Psi_in(:,1)
               Psi_spin(:,2)=1.d0*Psi_in(:,2)
               Psi_spin(:,3)=0.d0*Psi_in(:,3)
               Psi_spin(:,4)=-1.d0*Psi_in(:,4)
               Psi_spin(:,5)=-2.d0*Psi_in(:,5)
          EndIf
      ElseIf (Nlevel.eq.6) then
          If (direction.eq.1) THEN
               Psi_spin(:,1)=0.5d0*sqrt(5.d0)*Psi_in(:,2)
               Psi_spin(:,2)=0.5d0*sqrt(5.d0)*Psi_in(:,1)+0.5d0*sqrt(8.d0)*Psi_in(:,3)
               Psi_spin(:,3)=0.5d0*sqrt(8.d0)*Psi_in(:,2)+ 0.5d0*3.d0*Psi_in(:,4)
               Psi_spin(:,4)=0.5d0*3.d0*Psi_in(:,3) + 0.5d0*sqrt(8.d0)*Psi_in(:,5)
               Psi_spin(:,5)=0.5d0*sqrt(8.d0)*Psi_in(:,4) + 0.5d0*sqrt(5.d0)*Psi_in(:,6)
               Psi_spin(:,6)=0.5d0*sqrt(5.d0)*Psi_in(:,5)
           ElseIf (direction.eq.2) THEN
               Psi_spin(:,1)=dcmplx(0,sqrt(5.d0)/2.d0)*Psi_in(:,2)
               Psi_spin(:,2)=dcmplx(0,-sqrt(5.d0/2.d0))*Psi_in(:,1)+dcmplx(0,sqrt(8.d0)/2.d0)*Psi_in(:,3)
               Psi_spin(:,3)=dcmplx(0,-sqrt(8.d0)/2.d0)*Psi_in(:,2)+ dcmplx(0,3.d0/2.d0)*Psi_in(:,4)
               Psi_spin(:,4)=dcmplx(0,-1.5d0)*Psi_in(:,3) + dcmplx(0,sqrt(8.d0)/2.d0)*Psi_in(:,5)
               Psi_spin(:,5)=dcmplx(0,-sqrt(8.d0)/2.d0)*Psi_in(:,4) + dcmplx(0,sqrt(5.d0)/2.d0)*Psi_in(:,6)
               Psi_spin(:,6)=dcmplx(0,-sqrt(5.d0)/2.d0)*Psi_in(:,5)
           ElseIf (direction.eq.3) THEN
               Psi_spin(:,1)=2.5d0*Psi_in(:,1)
               Psi_spin(:,2)=1.5d0*Psi_in(:,2)
               Psi_spin(:,3)=0.5d0*Psi_in(:,3)
               Psi_spin(:,4)=-0.5d0*Psi_in(:,4)
               Psi_spin(:,5)=-1.5d0*Psi_in(:,5)
               Psi_spin(:,6)=-2.5d0*Psi_in(:,6)
           EndIf
      ELSEIF (Nlevel.gt.6) THEN
            Stop "Spin greater 5/2 not implemented yet !!!!!"
      EndIf

      end subroutine Apply_Spin



!> The routine returns the indexes i,j,k for a 3D array
!! given m, the index of a 1D array
!! of a vector that stores the ijk-th element in
!! the position m=i+gdimx(j-1)+gdimy*gdimx*(k-1)
!! [(ijk) start with (111)]
subroutine get_ijk_from_m(m,gdimx,gdimy,i,j,k)
implicit none
integer*8,intent(IN)      :: m,gdimx,gdimy
integer*8,intent(OUT)     :: i,j,k
integer*8               :: n

  n = m
  k = (n-1)/(gdimx*gdimy) + 1
  n =  n-(k-1)*gdimx*gdimy
  j = (n-1)/gdimx + 1
  n =  n-(j-1)*gdimx
  i =  n

end subroutine get_ijk_from_m

!> returns momentum grid in fft order
!! xi   - left border of box
!! xf   - right border of box ( =last grid point + dx)
!! gdim - number of grid points
!! mom  - array of momentum space grid points in FFT order
subroutine get_FFTmom(xi,xf,gdim,mom)
use Global_Parameters, ONLY: PI
implicit none

integer*8,intent(IN)              :: gdim
double precision,intent(OUT)    :: mom(gdim)

double precision                :: dp
integer                         :: i
  real*8             :: xi,xf

   xi=X_initial
   xf=X_final
mom(1)=0.d0

if (gdim.gt.1) then 
   dp             =     2.d0*PI/(xf-xi)
end if

do i=2,gdim/2
  mom(i) = (i-1)*dp
enddo

do i=gdim/2+1,gdim !put momenta in FFT order (0,dp,...,N/2*dp,(-N/2+1)*dp,...,-dp)
  mom(i)=1.d0*(i-gdim)*dp
end do

end subroutine get_FFTmom

!> returns momentum grid in ascending order
subroutine get_mom(xl,xu,gdim,mom,dilation)
use Global_Parameters, ONLY: PI
implicit none

integer*8,intent(IN)              :: gdim
integer,optional                :: dilation
double precision,intent(IN)     :: xl,xu
double precision,intent(OUT)    :: mom(gdim)

double precision                :: dp
integer                         :: i

if (gdim.gt.1) then 
   dp             =     2.d0*PI/(xu-xl)

   if (MOD(gdim,2).eq.0) then !gdim even

      do i=1,gdim
         mom(i)=(i-gdim/2-1)*dp  ! e.g.: (-dp,0,dp,2dp)
      end do

   else ! gdim uneven

      do i=1,gdim
         mom(i)=(i-gdim/2-1)*dp ! e.g.: (-2dp,-dp,0,dp,2dp)
      end do

   end if 

else if (gdim.eq.1) then

   mom(1) = 0.d0

else

   write(*,*)"ERROR in dimension"
   stop

end if

IF (Present(dilation).and.(Dilation.ne.1)) THEN
   mom=mom/(dilation*1.d0)
ENDIF

end subroutine get_mom

!>  Writes a real number in ten digit format to a character
!>  for naming files.
subroutine getDoubleAsString(x,doubleString)
IMPLICIT NONE
integer,parameter         :: DBL=8
real(kind=DBL)            :: x
CHARACTER(len=10)         :: doubleString

if (x .LE. 999999.999d0) WRITE(doubleString,'(F10.3)') x
if (x .LE. 99999.9999d0) WRITE(doubleString,'(F10.4)') x
if (x .LE. 9999.99999d0) WRITE(doubleString,'(F10.5)') x
if (x .LE. 999.999999d0) WRITE(doubleString,'(F10.6)') x
if (x .LE. 99.9999999d0) WRITE(doubleString,'(F10.7)') x
if (x .LE. 9.99999999d0) WRITE(doubleString,'(F10.8)') x

IF ((x .LT. 0.d0) .OR. (x .GE. 1000000.0d0)) THEN
  WRITE(6,*)"get double as string: x not implemented"
  STOP
END IF

end subroutine getDoubleAsString

function getArray(array, shape_) result(aptr)
  use iso_c_binding, only: C_LOC, C_F_POINTER
  ! Pass in the array as an array of fixed size so that there
  ! is no array descriptor associated with it. This means we
  ! can get a pointer to the location of the data using C_LOC
  complex*16, target :: array(1)
  integer :: shape_(:)
  complex*16, pointer :: aptr(:)

  ! Use C_LOC to get the start location of the array data, and
  ! use C_F_POINTER to turn this into a fortran pointer (aptr).
  ! Note that we need to specify the shape of the pointer using an
  ! integer array.
  call C_F_POINTER(C_LOC(array), aptr, shape_)
end function


function get3dFrom1dArray(array, shape_) result(aptr)
  use iso_c_binding, only: C_LOC, C_F_POINTER
  ! Pass in the array as an array of fixed size so that there
  ! is no array descriptor associated with it. This means we
  ! can get a pointer to the location of the data using C_LOC
  complex*16, target :: array(1)
  integer :: shape_(:)
  complex*16, pointer :: aptr(:,:,:)

  ! Use C_LOC to get the start location of the array data, and
  ! use C_F_POINTER to turn this into a fortran pointer (aptr).
  ! Note that we need to specify the shape of the pointer using an
  ! integer array.
  call C_F_POINTER(C_LOC(array), aptr, shape_)
end function



character(len=20) function inttostr(k)
!   "Convert an integer to string."
IMPLICIT NONE
integer, intent(in) :: k
write (inttostr, *) k
inttostr = adjustl(inttostr)
end function inttostr


END MODULE Auxiliary_Routines
