!>GPLv3
!    MCTDH-X: the multiconfigurational time-dependent Hartree for 
!    indistinguishable particles software
!
!    Copyright (C) 2021  A. U. J. Lode, M. C. Tsatsos, 
!                        E. Fasshauer, S. E. Weiner, 
!                        R. Lin, L. Papariello, P. Molignini, 
!                        C. Lévêque, M. Büttner , J. Xiang, S. Dutta
!
!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <https://www.gnu.org/licenses/>.
!
!
!
!
!<GPLv3










!> Routines handling the input and output of data of the main MCTDHB program are collected in this module.
      MODULE Input_Output
      CONTAINS 

      subroutine Read_OCT_Parameters(K)

      USE Global_Parameters
      USE CRAB_Parameters,ONLY:OCT_As,OCT_Bs,OCT_Omega,OCT_Gamma

      IMPLICIT NONE
      INTEGER K
      CHARACTER*18 fname2

      fname2='OCT_Restart'
       
      open(unit=112,file=trim(fname2),form='unformatted', &
                                      STATUS='unknown', &
                                      ACCESS='sequential')
  
      READ(112) K,&
                OCT_As,&
                OCT_Bs,&
                OCT_Omega,&
                OCT_Gamma 
    
      close(112)   


      end subroutine Read_OCT_Parameters


      subroutine Write_OCT_Parameters(Iteration,WriteControls, &
                                      Func_OCT_Value)

      USE Global_Parameters
      USE CRAB_Parameters
      USE Auxiliary_Routines

      IMPLICIT NONE

      Logical :: WriteControls
      REAL*8, Optional :: Func_OCT_Value

      Integer :: Iteration,I,J,K
      REAL*8 :: dt_OCT
      CHARACTER*18 fname
      CHARACTER*18 fname2
      CHARACTER*80 fname3

      dt_OCT=(Time_Final-Time_Begin)/OCT_TimeSteps


      IF ((WriteControls.eqv..TRUE.)) THEN

        IF (Iteration.lt.10) THEN
          write(fname,'(I1,a12)') Iteration,'controls.dat'
        ELSEIF ((Iteration.ge.10).and.(Iteration.lt.100)) THEN
          write(fname,'(I2,a12)') Iteration,'controls.dat'
        ELSEIF ((Iteration.ge.100).and.(Iteration.lt.1000)) THEN
          write(fname,'(I3,a12)') Iteration,'controls.dat'
        ELSEIF ((Iteration.ge.1000).and.(Iteration.lt.10000)) THEN
          write(fname,'(I4,a12)') Iteration,'controls.dat'
        ENDIF
  
 
        fname2='OCT_Restart'
        open(unit=111,file=trim(fname),form='formatted')

        DO I=1,OCT_TimeSteps
           write(111,'(99E25.16)')                                    & 
                    (I-1)*dt_OCT,(OCT_g(I,K),K=1,OCT_NGoals),         &
                                 (OCT_Gamma(I,K),K=1,OCT_NGoals)     
        END DO

        close(111)


        open(unit=112,file=trim(fname2),form='unformatted', &
                                        STATUS='unknown', &
                                        ACCESS='sequential')
  
        write(112) Iteration,&
                   OCT_As,&
                   OCT_Bs,&
                   OCT_Omega,&
                   OCT_Gamma 
    
        close(112)      

      ELSE  ! WriteControls present and false

        IF (Iteration.eq.1) THEN
           open(unit=134,file="OCT_Infidelities.dat",form='formatted')
        ELSE
           open(unit=134,file="OCT_Infidelities.dat",    &
                      form='formatted',ACCESS='APPEND')
        ENDIF
   
        fname3='OCT_CurrentGamma.'//trim(inttostr(Iteration))
        open(unit=136,file=trim(fname3),form='formatted')

        Write(134,'(99E25.16)')                                       &
              REAL(Iteration,8),(OCT_Infidelities(K),K=1,OCT_Ngoals), &
              func_OCT_Value
    
        DO K=1,OCT_TimeSteps
           Write(136,'(99E25.16)') (K-1)*dt_OCT, &
                              (OCT_CurrentGamma(K,J),J=1,OCT_NGoals)
        END DO
                                                                     
        close(134)                                                     
        close(136)

        fname3='OCT_GoalvsDensity.'//trim(inttostr(Iteration))
        open(unit=113,file=trim(fname3),form='formatted')
        
        do j=1,NDVR_X
        write(113,30)Ort_x(j),Abs(OCT_Density(j)),abs(OCT_Goal(j,1))
        enddo
        close(113)

30      FORMAT (99E25.16)   

      ENDIF

      END SUBROUTINE Write_OCT_Parameters

      Subroutine Write_Anyons(time,Syndrome,L)

      USE   Global_Parameters
      USE   Matrix_Elements
      USE   Coefficients_Parameters
      USE   Interaction_Parameters
      USE   DVR_Parameters
      USE   Auxiliary_Routines

      IMPLICIT NONE

      REAL*8 :: Time
      INTEGER :: L
      REAL*8 :: Syndrome(L*(L-1))

      integer*8 :: i,j,k,m,ind

      character*20 lname
     

      if(time.lt.99999.99999999D0) then
         Write( lname, '(F10.4,a10)') time,"anyons.dat"
      endif

      if(time.lt.9999.99999999D0) then
         Write( lname, '(F10.5,a10)') time,"anyons.dat"
      endif

      if(time.lt.999.99999999D0) then
         Write( lname, '(F10.6,a10)') time,"anyons.dat"
      endif

      if(time.lt. 99.99999999D0) then
         Write( lname, '(F10.7,a10)') time,"anyons.dat"
      endif

      if(time.lt.  9.99999999D0) then
         Write( lname, '(F10.8,a10)') time,"anyons.dat"
      endif

      open(unit=112,file=trim(lname),form='formatted')

      ind=1
      DO M=1,NDVR_X*NDVR_Y
         CALL get_ijk_from_m(m,NDVR_X,NDVR_Y,i,j,k)
         IF ((Ort_X(I).ge.(-L/2)).and.(Ort_X(I).le.L/2-1) &
             .and.(Ort_X(J).ge.(-L/2)).and.(Ort_Y(J).le.L/2)) THEN
             Write(112,6565) Ort_X(I),' ',Ort_Y(J),' ',Syndrome(ind),' '

             IF (mod(ind,L).eq.0) Then
                write(112,*) " "
             ENDIF

             ind=ind+1
         END IF
      END DO

6565  Format(3(F26.16,a3))

      close(112)

      End Subroutine Write_Anyons

!> Writes the coefficients to an ASCII file with the name 'time'coef.dat
      subroutine Write_Coefficients_BDAV(time,VIN)

      USE   Global_Parameters
      USE   Matrix_Elements
      USE   Coefficients_Parameters
      USE   Interaction_Parameters
      USE   DVR_Parameters
      USE   Addresses
      USE   Auxiliary_Routines

      IMPLICIT NONE

      COMPLEX*16, DIMENSION(Nconf*BlockSize) :: VIN
      real*8 time,t_total
      INTEGER :: jj,Ndim,ind,fock_vector(Morb),t(morb)
      character*80 lname,Formt,MorbasString,BlockasString

      t_total=time
!!! GP Exception
      IF (Morb.gt.1) THEN
!!! GP Exception

      if(t_total.lt.99999.99999999D0) then
         Write( lname, '(F10.4,a8)') t_total,"coef.dat"
      endif

      if(t_total.lt.9999.99999999D0) then
         Write( lname, '(F10.5,a8)') t_total,"coef.dat"
      endif

      if(t_total.lt.999.99999999D0) then
         Write( lname, '(F10.6,a8)') t_total,"coef.dat"
      endif
   
      if(t_total.lt. 99.99999999D0) then
         Write( lname, '(F10.7,a8)') t_total,"coef.dat"
      endif

      if(t_total.lt.  9.99999999D0) then
         Write( lname, '(F10.8,a8)') t_total,"coef.dat"
      endif

      open(unit=111,file=lname,form='formatted')
      write(111,*)"          "
      write(111,'(a1,a28,138(I26,a3))')"#","",(jj,"  ",jj=2,10)
      
      write(MorbasString,'(I8)') Morb
      write(BlockasString,'(I8)') 2*BlockSize

      FORMT='(i26,a3,'//trim(inttostr(2*Blocksize))//'(F26.16,a3),'//&
            trim(inttostr(Morb))//'(I26,a3))' 

      do ind=1,Nconf
          IF(ABS(VIN(ind)).ge.0.5d-14) THEN
            CALL Get_ConfigurationFromIndex(ind,Npar,Morb,t,fock_vector)
            write(111,FORMT) ind,"  ",                &
                 (DREAL(VIN((jj-1)*Nconf+ind)),"  ", &
                  DIMAG(VIN((jj-1)*Nconf+ind)),"  ", &
                   jj=1,BlockSize),                  &
                (fock_vector(jj),"  ",jj=1,Morb)
         ENDIF
      enddo


      close(111)

!!! GP Exception
      ENDIF
!!! GP Exception

      return
      end SUBROUTINE Write_Coefficients_BDAV



!> Writes the coefficients to an ASCII file with the name 'time'coef.dat
      subroutine Write_Coefficients(time,VIN)

      USE   Global_Parameters
      USE   Matrix_Elements
      USE   Coefficients_Parameters
      USE   Interaction_Parameters
      USE   DVR_Parameters
      USE   Addresses


      IMPLICIT NONE

      COMPLEX*16, DIMENSION(Nconf) :: VIN
      real*8 time,t_total
      INTEGER :: jj,Ndim,ind,fock_vector(Morb),t(morb)
      character*18 lname,Formt

      ndim=Nconf
      t_total=time
!!! GP Exception
      IF (Morb.gt.1) THEN
!!! GP Exception

      if(t_total.lt.99999.99999999D0) then
         Write( lname, '(F10.4,a8)') t_total,"coef.dat"
      endif

      if(t_total.lt.9999.99999999D0) then
         Write( lname, '(F10.5,a8)') t_total,"coef.dat"
      endif

      if(t_total.lt.999.99999999D0) then
         Write( lname, '(F10.6,a8)') t_total,"coef.dat"
      endif
   
      if(t_total.lt. 99.99999999D0) then
         Write( lname, '(F10.7,a8)') t_total,"coef.dat"
      endif

      if(t_total.lt.  9.99999999D0) then
         Write( lname, '(F10.8,a8)') t_total,"coef.dat"
      endif

      open(unit=111,file=lname,form='formatted')
      write(111,*)"          "
      write(111,'(a1,a28,138(I26,a3))')"#","",(jj,"  ",jj=2,10)
      
!      write(6,*) VIN
!      Write(FORMT,'(i26,a3,2(F26.16,a3),"Morb"(I26,a3))') 
      do ind=1,ndim
          IF(ABS(VIN(ind)).ge.0.5d-14) THEN
            CALL Get_ConfigurationFromIndex(ind,Npar,Morb,t,fock_vector)
            write(111,2222) ind,"  "&
                ,DREAL(VIN(ind)),"  ",DIMAG(VIN(ind)),"  ",&
                (fock_vector(jj),"  ",jj=1,Morb)
         ENDIF
      enddo


      close(111)

!!! GP Exception
      ENDIF
!!! GP Exception

      return
2222  format(i26,a3,2(F26.16,a3),99(I26,a3))
      end SUBROUTINE Write_Coefficients

!> This subroutine writes the output in the case of Bose-Hubbard computations
      subroutine Write_BH(time,Full_Rho1_Elements,BH_Offset)

      USE Global_Parameters, ONLY: Morb,Dim_MCTDH,NDVR_X,NDVR_Y
      USE Auxiliary_Routines

      IMPLICIT NONE  

      REAL*8, INTENT(IN) ::  Time
      COMPLEX*16, INTENT(IN) :: Full_Rho1_Elements(Morb,Morb)
      COMPLEX*16, INTENT(IN) :: BH_Offset(Morb)
      COMPLEX*16 :: Current_Op(Morb,Morb)
      COMPLEX*16 :: Current_EigenVectors(Morb,Morb)
      REAL*8     :: Current_EigenValues(Morb)

      COMPLEX*16 :: WORK(Morb*Morb)
      REAL*8     :: RWORK(3*Morb-2)
      INTEGER    :: INFO
      INTEGER*8  :: K,J,I,M

      character*10 TimeString
      character*80 FileName


       Call GetDoubleAsString(time,TimeString)
       FileName=TimeString//'-OneBody_Elements.dat'
       Open(unit=2004,file=trim(FileName))
       Do J=1,Morb
          Do K=1,Morb
            Write(2004,'(I4,1X,I4,1X,2G26.16)') &
                          J,K,Full_Rho1_Elements(J,K)
          End Do
          Write(2004,*) ' '
       End Do
       Close(2004)

       FileName=TimeString//'-BH_Density.dat'
       Open(unit=2002,file=trim(FileName))
       IF (DIM_MCTDH.eq.1) THEN
          DO J=1,Morb           
             Write(2002,'(I4,1X,4G26.16,1X)') &
                   J,Full_Rho1_Elements(J,J),BH_Offset(J)

          End Do
       ELSE IF (DIM_MCTDH.eq.2) THEN
          DO M=1,MORB

             Call Get_ijk_from_m(m,NDVR_x,NDVR_y,i,j,k)
             Write(2002,'(I4,1X,I4,1X,4G26.16,1X)') &
                       I,J,Full_Rho1_Elements(M,M),BH_Offset(M)
             IF (mod(M,NDVR_X).eq.0) THEN
                Write(2002,*)  "                    "
             ENDIF
          END DO
       ELSE IF (DIM_MCTDH.eq.3) THEN
          DO M=1,MORB

             Call Get_ijk_from_m(m,NDVR_x,NDVR_y,i,j,k)
             Write(2002,'(I4,1X,I4,1X,I4,1X,4G26.16,1X)') &
                       I,J,K,Full_Rho1_Elements(M,M),BH_Offset(M)
             IF ((mod(M,NDVR_X).eq.0).or.(mod(M,NDVR_Y).eq.0)) THEN
                Write(2002,*)  "                    "
             ENDIF
          END DO

       END IF      

       Close(2002)


       FileName=TimeString//'-Current_Operator.dat'
       Open(unit=2001,file=trim(FileName))
       Current_Op=cmplx(0.d0,0.d0)

       Do J=1,Morb
          DO K=1,Morb
            IF ((J.lt.Morb).and.(J.gt.1)) THEN
              IF ((K.eq.J+1).or.(K.eq.J-1)) THEN
                Current_OP(J,K)=Current_OP(J,K)+ &
                                (BH_J*Full_Rho1_Elements(J,K))
              ENDIF 
            ELSE IF (J.eq.Morb) THEN
              IF ((Periodic_BH.eqv..TRUE.).and.(K.eq.1)) THEN
                   Current_OP(J,K)=Current_OP(J,K)+cmplx(0.d0,-1.d0)* &
                                   (BH_J*Full_Rho1_Elements(J,K))
              ELSE IF (K.eq.J-1) THEN
                Current_OP(J,K)=Current_OP(J,K)+cmplx(0.d0,-1.d0)* &
                                 (BH_J*Full_Rho1_Elements(J,K))
              ENDIF
            ELSE IF (J.eq.1) THEN
              IF ((Periodic_BH.eqv..TRUE.).and.(K.eq.Morb)) THEN
                   Current_OP(J,K)=Current_OP(J,K)+cmplx(0.d0,-1.d0)* &
                                   (BH_J*Full_Rho1_Elements(J,K))
              ELSEIF (K.eq.J+1) THEN
                Current_OP(J,K)=Current_OP(J,K)+cmplx(0.d0,-1.d0)* &
                                (BH_J*Full_Rho1_Elements(J,K))
              ENDIF

            ENDIF 

          END DO
       END DO
     
       Current_Op=Current_Op-conjg(transpose(Current_Op))
       Do J=1,Morb
          DO K=1,Morb
            Write(2001,'(I4,1X,I4,1X,2G26.16)') J,K,Current_Op(J,K)
          END DO
          Write(2001,*) ' '
       END DO

       Close(2001)


       Current_Eigenvectors=Current_Op

       CALL ZHEEV('V','U',Morb,Current_EigenVectors,Morb, &
                    Current_EigenValues,WORK,Morb*Morb,   &
                                          RWORK,INFO)

       FileName=TimeString//'-Current_EigenVectors.dat'
       Open(unit=2001,file=trim(FileName))

       DO J=1,Morb
          Write(2001,'(100G26.16)')       &
            SUM(Current_Eigenvalues(J)*   &
               Current_Eigenvectors(J,:)  &
              *conjg(Current_Eigenvectors(J,:))), &
               (Current_Eigenvectors(J,K),K=1,Morb)
       END DO
       Close(2001)


       FileName=TimeString//'-Current_EigenValues.dat'
       Open(unit=2001,file=trim(FileName))

       DO J=1,Morb
          Write(2001,'(100G26.16)') Current_Eigenvalues(J)
       END DO

       Close(2001)
       
        end subroutine Write_BH

!MPI_MULTI_LEVEL VERSION NEEDED!
!> Write the NlevelOrbitals and density to the file 'time'orbs.dat
       subroutine Write_NlevelOrbitals(time,PSI,FT,Dilation)

       USE   Global_Parameters
       USE   Matrix_Elements
       USE   Coefficients_Parameters
       USE   Interaction_Parameters
       USE   DVR_Parameters
       USE   Auxiliary_Routines
       USE   Orbital_Allocatables, ONLY: VTRAP_EXT_Nlevel

       IMPLICIT NONE 

       COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb,Nlevel)  :: PSI
       LOGICAL,Optional:: FT
       Integer,Optional :: Dilation
       COMPLEX*16, DIMENSION(Morb,Nlevel)  :: v !,NO_Expectation_x(Morb)
       complex*16 rhoPR(Nlevel)
       complex*16 xx(Nlevel),xN,yy(Nlevel)
       real*8 time,t_total,ww,dk,coord_X(NDVR_X), &
             coord_y(NDVR_Y),coord_z(NDVR_Z)
       integer :: jj,Ndim,ind,k,j,i,k1,l1,L,M,Npotentials,Dil
       character*80 :: lname,suffix

       Logical :: Standard

      IF (MPI_ORBS.eqv..TRUE.) THEN
         Call Write_NlevelOrbs_MPI(time,PSI,Vtrap_Ext_Nlevel) ! Function to be written.. Done!
         return
      ENDIF

       ndim=NDVR_X*NDVR_Y*NDVR_Z
       t_total=time

       IF ((Present(FT).and.(FT.eqv..FALSE.)) &
           .or.(Present(FT).eqv..FALSE.)) THEN

           Write( suffix, '(a8)') "orbs.dat"

           Coord_X=Ort_X 
           Coord_Y=Ort_Y
           Coord_Z=Ort_Z 
           Standard=.TRUE.

       ELSEIF (Present(FT).and.(FT.eqv..TRUE.)) THEN

           Write( suffix, '(a10)') "ftorbs.dat"

           Coord_X=0.d0 
           Coord_Y=0.d0 
           Coord_Z=0.d0 
           Standard=.FALSE.
  
           IF (Present(Dilation).eqv..FALSE.) THEN 
              Dil=1
           ELSEIF (Present(Dilation).eqv..TRUE.) THEN 
              Dil=Dilation
           ENDIF

           call get_mom(x_initial*Dil,x_final*Dil, &
                        NDVR_X,Coord_X)
           dk     = Coord_X(2)-Coord_X(1) 
      
           IF (DIM_MCTDH.ge.2) THEN
              call get_mom(y_initial*Dil,y_final*Dil, &
                           NDVR_Y,Coord_Y)
              dk     = dk*(Coord_Y(2)-Coord_Y(1))
           ENDIF
           IF (DIM_MCTDH.ge.3) THEN
              call get_mom(z_initial*Dil,z_final*Dil, &
                           NDVR_Z,Coord_Z)
              dk     = dk*(Coord_Z(2)-Coord_Z(1))
           ENDIF

           ww      = DSQRT( dk )

       ENDIF



       IF (Conical_Intersection.eqv..TRUE.) THEN
          Npotentials=Nlevel+1
       ELSE
          Npotentials=Nlevel
       ENDIF   

       if(t_total.lt.99999.99999999D0) then
          Write( lname, '(F10.4,a10)') t_total,suffix
       endif

       if(t_total.lt.9999.99999999D0) then
          Write( lname, '(F10.5,a10)') t_total,suffix
       endif

       if(t_total.lt.999.99999999D0) then
          Write( lname, '(F10.6,a10)') t_total,suffix
       endif

       if(t_total.lt. 99.99999999D0) then
          Write( lname, '(F10.7,a10)') t_total,suffix
       endif

       if(t_total.lt.  9.99999999D0) then
          Write( lname, '(F10.8,a10)') t_total,suffix
       endif

       open(unit=112,file=trim(lname),form='formatted')
    
       write(112,*)"          "
       write(112,'(a1,a28,138(I26,a3))')"#","",(jj,"  ",jj=2,10+5*Morb)
       
       xN=1.0d0*Npar


       ind=1
       do K=1,NDVR_Z

         do J=1,NDVR_Y

           do I=1,NDVR_X

            xx=Zero
            yy=Zero

            IF(Standard.eqv..TRUE.) THEN
               ww=weight(ind)
            ENDIF

            DO M=1,Nlevel

             Do k1=1,Morb

               v(k1,M)=Zero

               Do l1=1,Morb

                 xx(M)=xx(M)+dconjg(PSI(ind,l1,M))&
                            *Full_Rho1_Elements(l1,k1)*&
                             PSI(ind,k1,M)
                 v(k1,M)=v(k1,M)+Conjg(NatVec(l1,k1))*PSI(ind,l1,M) 

               END DO

               yy(M)=yy(M)+Nocc(k1)*dconjg(v(k1,M))*v(k1,M)

             END DO

             rhoPR(M)=xx(M)/ww/ww

            end do

            write(112,2222) coord_X(I),"  ",& !1 X
             coord_Y(J),"  ",&                       !2 Y
             coord_Z(K),"  ",&                       !3 Z
             ww,"  ",&                             !4 Weight
             (DREAL(VTRAP_EXT_Nlevel(ind,jj)),"  ",jj=1,Npotentials),& ! 5-->5+NPotentials V_i(r)
             (DREAL(rhoPR(jj)/xN),&
             "   ",DIMAG(rhoPR(jj)/xN),"   ",jj=1,Nlevel),&! 5+Npotentials+1 --> 5+Npotentials+1+2*Nlevel rho_i(r) 
             (DREAL(yy(jj)/ww/ww/xN),"   ",& ! 6+Npotentials+2*Nlevel+1 --> 6+Npotentials+4*Nlevel+1 rho^(NO)_i(r)
             DIMAG(yy(jj)/ww/ww/xN),"   ",jj=1,Nlevel),&
             ((DREAL(PSI(ind,jj,L)),"  ",& ! 7+Npotentials+4*Nlevel+1 --> 7+Npotentials+4*Nlevel+Morb*Nlevel*2 Psi_i(r)
             DIMAG(PSI(ind,jj,L)),"  ",L=1,Nlevel),jj=1,Morb),&
             ((DREAL(v(jj,L)),"  ",DIMAG(v(jj,L)),& ! 7+Npotentials+(4+Morb*2)*Nlevel+1 --> 7+Npotentials+(4+Morb*4)*Nlevel+1 Psi^(NO)(r)
             "   ",L=1,Nlevel),jj=1,Morb)
                        ind=ind+1
            enddo

            write(112,*)"    " 

          enddo

        enddo
        close(112)
        return

2222  format((999(F26.16,a3)))

      end SUBROUTINE Write_NlevelOrbitals

!> Write the Orbitals and density to the file 'time'orbs.dat or 'time'ftorbs.datz
       subroutine Write_Orbitals(time,PSI,FT,Dilation)

       USE   Global_Parameters
       USE   Matrix_Elements
       USE   Coefficients_Parameters
       USE   Interaction_Parameters
       USE   DVR_Parameters
       USE   Auxiliary_Routines
       USE   Orbital_Allocatables, ONLY: VTRAP_EXT,LocalSize
       USE   Orbital_Parallelization_Parameters, ONLY: LocalPsiDim

       COMPLEX*16 :: PSI(LocalSize,Morb) 

       COMPLEX*16, DIMENSION(Morb)  :: v !,NO_Expectation_x(Morb)
       COMPLEX*16, DIMENSION(Morb,BlockSize)  :: v_Block !,NO_Expectation_x(Morb)
       real*8,DIMENSION(NDVR_X) :: Coord_X 
       real*8,DIMENSION(NDVR_Y) :: Coord_Y 
       real*8,DIMENSION(NDVR_Z) :: Coord_Z
       complex*16 rhoPR
       complex*16 xx,xN,yy,yy_block(BlockSize)
       Logical, Optional :: FT
       Integer, Optional :: Dilation
       Logical :: Standard
       real*8 time,t_total,ww,dk
       integer :: jj,Ndim,ind,k,j,i,k1,l1,Q
       character*80 lname,suffix

      IF (MPI_ORBS.eqv..TRUE.) THEN
         Call Write_Orbs_MPI(time,PSI,Vtrap_Ext)
         return
      ENDIF

       ndim=NDVR_X*NDVR_Y*NDVR_Z
       t_total=time

       IF ((Present(FT).and.(FT.eqv..FALSE.)) &
           .or.(Present(FT).eqv..FALSE.)) THEN

           Write( suffix, '(a8)') "orbs.dat"

           Coord_X=Ort_X 
           Coord_Y=Ort_Y
           Coord_Z=Ort_Z 
           Standard=.TRUE.

       ELSEIF (Present(FT).and.(FT.eqv..TRUE.)) THEN

           Write( suffix, '(a10)') "ftorbs.dat"

           Coord_X=0.d0 
           Coord_Y=0.d0 
           Coord_Z=0.d0 
           Standard=.FALSE.
         
           IF (Present(Dilation).eqv..FALSE.) THEN 
              Dilation=1
           ENDIF

           call get_mom(x_initial*Dilation,x_final*Dilation, &
                       NDVR_X,Coord_X)
           dk     = Coord_X(2)-Coord_X(1) 
      
           IF (DIM_MCTDH.ge.2) THEN
              call get_mom(y_initial*Dilation,y_final*Dilation, &
                          NDVR_Y,Coord_Y)
              dk     = dk*(Coord_Y(2)-Coord_Y(1))
           ENDIF
           IF (DIM_MCTDH.ge.3) THEN
              call get_mom(z_initial*Dilation,z_final*Dilation, &
                          NDVR_Z,Coord_Z)
              dk     = dk*(Coord_Z(2)-Coord_Z(1))
           ENDIF

           ww      = DSQRT( dk )

       ENDIF

       if(t_total.lt.99999.99999999D0) then
          Write( lname, '(F10.4,a10)') t_total,suffix
       endif

       if(t_total.lt.9999.99999999D0) then
          Write( lname, '(F10.5,a10)') t_total,suffix
       endif

       if(t_total.lt.999.99999999D0) then
          Write( lname, '(F10.6,a10)') t_total,suffix
       endif

       if(t_total.lt. 99.99999999D0) then
          Write( lname, '(F10.7,a10)') t_total,suffix
       endif

       if(t_total.lt.  9.99999999D0) then
          Write( lname, '(F10.8,a10)') t_total,suffix
       endif

       open(unit=112,file=trim(lname),form='formatted')
       write(112,*)"          "
       write(112,'(a1,a28,138(I26,a3))')"#","",(jj,"  ",jj=2,10+5*Morb)
       
       xN=1.0d0*Npar

       ind=1

       do K=1,NDVR_Z

         do J=1,NDVR_Y

           do I=1,NDVR_X

             xx=Zero
             yy=Zero
             yy_block=Zero

             IF(Standard.eqv..TRUE.) THEN
                ww=weight(ind)
             ENDIF
             ! Replace by a call to Get_NOs
             Do k1=1,Morb

               v(k1)=Zero
               v_block(k1,:)=zero

               Do l1=1,Morb

                 xx=xx+dconjg(PSI(ind,l1))*Full_Rho1_Elements(l1,k1)*&
                             PSI(ind,k1)
                 IF (trim(Coefficients_Integrator).ne.'BDV') THEN
                   v(k1)=v(k1)+Conjg(NatVec(l1,k1))*PSI(ind,l1) 
                 ELSE
                   DO Q=1,BlockSize
                     v_block(k1,Q)=v_block(k1,Q)                &
                                 +Conjg(NatVec_Block(l1,k1,Q))  &
                                        *PSI(ind,l1) 
                   END DO
                 ENDIF
               END DO

               IF (trim(Coefficients_Integrator).ne.'BDV') THEN
                 yy=yy+Nocc(k1)*dconjg(v(k1))*v(k1)
               ELSE
                 DO l1=1,BlockSize
                   yy_block(l1)=yy_block(l1)+Nocc_block(k1,l1)     &
                                *dconjg(v_block(k1,l1))*v_Block(k1,l1)

                 END DO
               ENDIF

             END DO

             rhoPR=xx/ww/ww

             IF (trim(Coefficients_Integrator).ne.'BDV') THEN
               write(112,2222) Coord_X(I),"  ",&
                             Coord_Y(J),"  ",&
                             Coord_Z(K),"  ",&
                             ww,"  ",&
                             DREAL(VTRAP_EXT(ind)),"  ",&
                             DREAL(rhoPR/xN),"   ",&
                             DIMAG(rhoPR/xN),"   ",&
                             DREAL(yy/ww/ww/xN),"   ",&
                             DIMAG(yy/ww/ww/xN),"   ",&
                             (DREAL(PSI(ind,jj)),"  ",&
                              DIMAG(PSI(ind,jj)),"  ",jj=1,Morb),&
                             (DREAL(v(jj)),"  ", &
                              DIMAG(v(jj)),"   ",jj=1,Morb)
             ELSE
                write(112,2222) Coord_X(I),"  ",&
                             Coord_Y(J),"  ",&
                             Coord_Z(K),"  ",&
                             ww,"  ",&
                             DREAL(VTRAP_EXT(ind)),"  ",&
                             (DREAL(yy_block(Q)/ww/ww/xN),"   ",&
                             DIMAG(yy_block(Q)/ww/ww/xN),"   ",&
                             (DREAL(v_block(jj,Q)),"  ", &
                              DIMAG(v_block(jj,Q)),"   ",jj=1,Morb)& 
                                              ,Q=1,BlockSize)
             ENDIF
             

             ind=ind+1

            enddo

            write(112,*)"    " 

          enddo

        enddo

        close(112)

        return

2222  format((999(F26.16,a3)))

      end SUBROUTINE Write_Orbitals

        
      !> This routine writes out the wavefunction and potential                                                                                    
      Subroutine Write_Orbs_MPI(time,Phi0,VTrap)
#if MPIFFT
      USE MPI_FFT_DATA, ONLY: Local_y_Offset,&
                              Local_Ly,Local_z_Offset,Local_Lz,&
                              Local_y_Offset_inter,Local_Ly_inter,&
                              Local_z_Offset_inter,Local_Lz_inter
#endif      
                              
      USE Orbital_Allocatables
      USE Orbital_Parallelization_Parameters, ONLY: LocalPsiDim, &
                                                    LocalPsiDim_inter
      
      USE DVR_Parameters, ONLY: Ort_X,Ort_Y,Ort_Z,weight

      USE Global_Parameters, ONLY: NDVR_X,NDVR_Y,NDVR_Z,Job_Prefactor, &
                                   MORB,DIM_MCTDH,NDVR_X_inter,&
                                   NDVR_Y_inter,NDVR_Z_inter
      USE Coefficients_Parameters, ONLY: NPar
      USE Matrix_Elements, ONLY : Full_Rho1_Elements,NatVec,Nocc

#if MPIFFT
      USE MPI
#endif

      IMPLICIT NONE
      
      
      REAL*8 :: Time,ww
      
      Complex*16 :: VTrap(LocalPsiDim(1)),rhoPR
      COMPLEX*16 :: Phi0(LocalPsiDim(1),MORB)
      COMPLEX*16, DIMENSION(Morb)  :: v !,NO_Expectation_x(Morb)
      complex*16 :: xx,xN,yy
      
      INTEGER ::  n_mpi_procs, my_mpi_rank
      
      INTEGER :: I,J,JJ,K,L,K1,L1,M,IND,I_Error,NDX,NDY,NDZ
      INTEGER :: loc_z,loc_y,loc_lz,loc_ly,localdim
      
      Character*80 lname, suffix
      
      write(suffix,'(a8)') 'orbs.dat'
      
      if(Time.lt.99999.99999999D0) then
         Write( lname, '(F10.4,a10)') Time,suffix
      endif
      
      if(Time.lt.9999.99999999D0) then
         Write( lname, '(F10.5,a10)') Time,suffix
      endif
      
      if(Time.lt.999.99999999D0) then
         Write( lname, '(F10.6,a10)') Time,suffix
      endif
      
      if(Time.lt. 99.99999999D0) then
         Write( lname, '(F10.7,a10)') Time,suffix
      endif
      
      if(Time.lt.  9.99999999D0) then
         Write( lname, '(F10.8,a10)') Time,suffix
      endif
      
#if defined (MPIFFT)
      
      IF (Allocated(LocalPsiDim_inter).eqv..TRUE.) THEN
         LocalDim=LocalPsiDim_Inter(1)
         Loc_y=Local_y_Offset_Inter
         Loc_ly=Local_Ly_inter

         NDX=NDVR_X_inter
         NDY=NDVR_Y_inter

         IF (DIM_MCTDH.eq.3) THEN
           Loc_z=Local_z_Offset_Inter
           Loc_lz=Local_Lz_inter
           NDZ=NDVR_Z_inter
         ENDIF

      ELSE
         LocalDim=LocalPsiDim(1)
         Loc_y=Local_y_Offset
         Loc_ly=Local_Ly
         NDX=NDVR_X
         NDY=NDVR_Y
         NDZ=NDVR_Z

         IF (DIM_MCTDH.eq.3) THEN
           Loc_z=Local_z_Offset
           Loc_y=0
           Loc_lz=Local_Lz
           Loc_ly=NDVR_Y
         ELSEIF (DIM_MCTDH.eq.2) THEN
           Loc_z=0
           Loc_lz=1
         ENDIF

      ENDIF

      xN=1.d0*NPar


      CALL MPI_COMM_SIZE(MPI_COMM_WORLD,n_mpi_procs,I_error)
      CALL MPI_COMM_RANK(MPI_COMM_WORLD,my_mpi_rank,I_error)
      CALL MPI_BCAST(Nocc,Morb, &
                     MPI_DOUBLE_COMPLEX,0,MPI_COMM_WORLD,I_error)
      CALL MPI_BCAST(NatVec,Morb*Morb, &
                     MPI_DOUBLE_COMPLEX,0,MPI_COMM_WORLD,I_error)

      DO L=0,n_mpi_procs-1 
      
        IF (my_mpi_rank.eq.L) THEN
          IF (L.eq.0) THEN
             OPEN(unit=1011,file=trim(lname))
          ELSE
             OPEN(unit=1011,file=trim(lname),ACCESS='APPEND')
          ENDIF    
      
          ind=1
          DO K=1,Loc_Lz
             Do J=1,Loc_LY
                Do I=1,NDX

                   xx=0.d0
                   yy=0.d0
  
                   ww=weight(ind)

                   Do k1=1,Morb
                     v(k1)=Zero
                   
                     Do l1=1,Morb
                   
                       xx=xx+dconjg(PSI(ind,l1))       &
                            *Full_Rho1_Elements(l1,k1) &
                            *PSI(ind,k1)
                       v(k1)=v(k1)+Conjg(NatVec(l1,k1))*Phi0(ind,l1) 
                     END DO

                     yy=yy+Nocc(k1)*dconjg(v(k1))*v(k1)
                   END DO
                   
                   yy=yy/ww/ww/xN
                   rhoPR=xx/ww/ww/xN

                   Write(1011,2222) &
                             Ort_X(I),"  ",&
                             Ort_Y(J+Loc_y),"  ",&
                             ORT_Z(K+Loc_z),"  ",&
                             ww,"  ",&
                             DREAL(VTRAP_EXT(ind)),"  ",&
                             DREAL(rhoPR),"   ",&
                             DIMAG(rhoPR),"   ",&
                             DREAL(yy),"   ",&
                             DIMAG(yy),"   ",&
                             (DREAL(Phi0(ind,jj)),"  ",&
                              DIMAG(Phi0(ind,jj)),"  ",jj=1,Morb),&
                             (DREAL(v(jj)),"  ", &
                              DIMAG(v(jj)),"   ",jj=1,Morb)

                   IF (mod(ind,NDVR_X).eq.0) Write(1011,*) "     "

                   ind=ind+1

                Enddo
             Enddo
          ENDDO
      
        
          CLOSE(1011)
        ENDIF
        CALL MPI_BARRIER(MPI_COMM_WORLD,I_Error)
      END DO
      
#else
        Write(6,*) "Should not be here..."
        STOP
#endif
2222  format((999(F26.16,a3)))
      
      end subroutine Write_Orbs_MPI
      


      !> This routine writes out the wavefunction and potential                                                                                    
      Subroutine Write_NlevelOrbs_MPI(time,Phi0,VTrap)
#if MPIFFT
      USE MPI_FFT_DATA, ONLY: Local_y_Offset,&
                              Local_Ly,Local_z_Offset,Local_Lz,&
                              Local_y_Offset_inter,Local_Ly_inter,&
                              Local_z_Offset_inter,Local_Lz_inter
#endif      
                              
      USE Orbital_Allocatables
      USE Orbital_Parallelization_Parameters, ONLY: LocalPsiDim, &
                                                    LocalPsiDim_inter
      
      USE DVR_Parameters, ONLY: Ort_X,Ort_Y,Ort_Z,weight

      USE Global_Parameters, ONLY: NDVR_X,NDVR_Y,NDVR_Z,Job_Prefactor, &
                                   MORB,DIM_MCTDH,NDVR_X_inter,&
                                   NDVR_Y_inter,NDVR_Z_inter
      USE Coefficients_Parameters, ONLY: NPar
      USE Matrix_Elements, ONLY : Full_Rho1_Elements,NatVec,Nocc

#if MPIFFT
      USE MPI
#endif

      IMPLICIT NONE
      
      
      REAL*8 :: Time,ww
      
      Complex*16 :: VTrap(LocalPsiDim(1),Nlevel),rhoPR(Nlevel)
      COMPLEX*16 :: Phi0(LocalPsiDim(1),MORB,Nlevel)
      COMPLEX*16 :: v(Morb,Nlevel) !,NO_Expectation_x(Morb)
      complex*16 :: xx(Nlevel),xN,yy(Nlevel)
      
      INTEGER ::  n_mpi_procs, my_mpi_rank
      
      INTEGER :: I,J,JJ,K,L,K1,L1,KK,M,IND,I_Error,NDX,NDY,NDZ
      INTEGER :: loc_z,loc_y,loc_lz,loc_ly,localdim
      
      Character*80 lname, suffix
      
      write(suffix,'(a8)') 'orbs.dat'
      
      if(Time.lt.99999.99999999D0) then
         Write( lname, '(F10.4,a10)') Time,suffix
      endif
      
      if(Time.lt.9999.99999999D0) then
         Write( lname, '(F10.5,a10)') Time,suffix
      endif
      
      if(Time.lt.999.99999999D0) then
         Write( lname, '(F10.6,a10)') Time,suffix
      endif
      
      if(Time.lt. 99.99999999D0) then
         Write( lname, '(F10.7,a10)') Time,suffix
      endif
      
      if(Time.lt.  9.99999999D0) then
         Write( lname, '(F10.8,a10)') Time,suffix
      endif
      
#if defined (MPIFFT)
      
      IF (Allocated(LocalPsiDim_inter).eqv..TRUE.) THEN
         LocalDim=LocalPsiDim_Inter(1)
         Loc_y=Local_y_Offset_Inter
         Loc_ly=Local_Ly_inter

         NDX=NDVR_X_inter
         NDY=NDVR_Y_inter

         IF (DIM_MCTDH.eq.3) THEN
           Loc_z=Local_z_Offset_Inter
           Loc_lz=Local_Lz_inter
           NDZ=NDVR_Z_inter
         ENDIF

      ELSE
         LocalDim=LocalPsiDim(1)
         Loc_y=Local_y_Offset
         Loc_ly=Local_Ly
         NDX=NDVR_X
         NDY=NDVR_Y
         NDZ=NDVR_Z

         IF (DIM_MCTDH.eq.3) THEN
           Loc_z=Local_z_Offset
           Loc_y=0
           Loc_lz=Local_Lz
           Loc_ly=NDVR_Y
         ELSEIF (DIM_MCTDH.eq.2) THEN
           Loc_z=0
           Loc_lz=1
         ENDIF

      ENDIF

      xN=1.d0*NPar


      CALL MPI_COMM_SIZE(MPI_COMM_WORLD,n_mpi_procs,I_error)
      CALL MPI_COMM_RANK(MPI_COMM_WORLD,my_mpi_rank,I_error)
      CALL MPI_BCAST(Nocc,Morb, &
                     MPI_DOUBLE_COMPLEX,0,MPI_COMM_WORLD,I_error)
      CALL MPI_BCAST(NatVec,Morb*Morb, &
                     MPI_DOUBLE_COMPLEX,0,MPI_COMM_WORLD,I_error)

      DO L=0,n_mpi_procs-1 
      
        IF (my_mpi_rank.eq.L) THEN
          IF (L.eq.0) THEN
             OPEN(unit=1011,file=trim(lname))
          ELSE
             OPEN(unit=1011,file=trim(lname),ACCESS='APPEND')
          ENDIF    
      
          ind=1
          DO K=1,Loc_Lz
             Do J=1,Loc_LY
                Do I=1,NDX

                   xx=0.d0
                   yy=0.d0
  
                   ww=weight(ind)

                   DO M=1,Nlevel
                   Do k1=1,Morb
                     v(k1,M)=Zero
                   
                     Do l1=1,Morb
                   
                       xx(M)=xx(M)+dconjg(Phi0(ind,l1,M)) &
                            *Full_Rho1_Elements(l1,k1) &
                            *Phi0(ind,k1,M)
                       v(k1,M)=v(k1,M)+Conjg(NatVec(l1,k1))*Phi0(ind,l1,M) 
                     END DO

                     yy(M)=yy(M)+Nocc(k1)*dconjg(v(k1,M))*v(k1,M)
                   END DO
                   yy(M)=yy(M)/ww/ww/xN
                   rhoPR(M)=xx(M)/ww/ww/xN
                   END DO

                   Write(1011,2222) &
                             Ort_X(I),"  ",&
                             Ort_Y(J+Loc_y),"  ",&
                             ORT_Z(K+Loc_z),"  ",&
                             ww,"  ",&
                             (DREAL(VTRAP_EXT_Nlevel(ind,jj)),"  ",jj=1,Nlevel),&
                             (DREAL(rhoPR(jj)),"   ",&
                             DIMAG(rhoPR(jj)),"   ",jj=1,Nlevel),&
                             (DREAL(yy(jj)),"   ",&
                             DIMAG(yy(jj)),"   ",jj=1,Nlevel),&
                             ((DREAL(Phi0(ind,jj,kk)),"  ",&
                              DIMAG(Phi0(ind,jj,kk)),"  ",kk=1,Nlevel),jj=1,Morb),&
                             ((DREAL(v(jj,kk)),"  ", &
                              DIMAG(v(jj,kk)),"   ",kk=1,Nlevel),jj=1,Morb)

                   IF (mod(ind,NDVR_X).eq.0) Write(1011,*) "     "

                   ind=ind+1

                Enddo
             Enddo
          ENDDO
      
        
          CLOSE(1011)
        ENDIF
        CALL MPI_BARRIER(MPI_COMM_WORLD,I_Error)
      END DO
      
#else
        Write(6,*) "Should not be here..."
        STOP
#endif
2222  format((999(F26.16,a3)))
      
      end subroutine Write_NlevelOrbs_MPI
      



!> This routine writes the output for the checks in the test suite.
      subroutine Write_Testing(morb,energy,Nocc,time,Npart,&
                     exec_tm,NDVR_X,NDVR_Y,NDVR_Z,dimmctdh,&
                     nprocs,nthreads,DVRX,DVRY,DVRZ)

      implicit NONE

      integer morb,Npart,i,dimmctdh,nprocs,nthreads
      integer*8 NDVR_X,NDVR_Y,NDVR_Z
      integer DVRX,DVRY,DVRZ
      Real*8 Nocc(morb)
      Real*8 energy,time,exec_tm
      
      write(6,'(A70)') '##############################################'
      write(6,'(A70)') '#########WRITING NUMBERS FOR COMPARISON!######'
      write(6,'(A70)') '##############################################'

      ! open(UNIT=102,FILE='current',STATUS='UNKNOWN')
      !  
      ! write(102,'(A15,I4,A4,I4,A4,F16.12,A5,F16.12)') &
      !  '#Comparison, N=',Npart,', M=',MORB,', t=',time,'t_cpu',exec_tm
      ! 
      ! write(102,'(A10,I4,A5,I4,A5,I4,A10,I4,A10,I4,A11,&
      !           I4,A7,I4,A7,I4,A7,I4)') &
      !       '# NDVR_X= ',NDVR_X,' NDVR_Y= ',NDVR_Y,' NDVR_Z=',NDVR_Z,&
      !       ' DIM-MCTDH=',dimmctdh,&
      !       ' threads=',nthreads,' processes=',nprocs,' DVR_X=',DVRX,&
      !       ' DVR_Y=',DVRY,' DVR_Z=',DVRZ
      !
      ! write(102,'(A6,E25.16)') 'Energy', energy
      !
      ! do i=1,morb
      !
      !   if (morb.lt.10) then
      !      write(102,'(A4,I1,E25.16)') 'rho_',i,Nocc(i)
      !   else
      !      write(102,'(A4,I2,E25.16)') 'rho_',i,Nocc(i)
      !   endif
      !
      ! end do
      !
      !
      ! close(102)
      !      
      end subroutine write_testing 

!> Open binary coefficients and orbitals files with the unit numbers 777,778, and 779. 
!> Per default, the file names are 'CIc_bin' and 'PSI_bin'.
!> If the flag "NamedBinaries" is passed, the file names are taken from the analysis input 
!> variables CI_Reference,PSI_Reference,Header_Reference. In this case the file unit numbers
!> are 801,802, and 803.
      subroutine Open_binary(MYID,NamedBinaries)

      USE Global_Parameters, ONLY: Orbital_Integrator,CI_V3,MPI_ORBS
       USE Orbital_Parallelization_Parameters, &
          ONLY: Orbital_BinaryUnitNumber
       USE Coefficients_Parallelization_Parameters, &
          ONLY: Coefficient_BinaryUnitNumber      
      USE Analysis_Input_Variables, ONLY: CI_Reference,PSI_Reference,Header_Reference

       USE MPI


       IMPLICIT NONE       

 
       INTEGER,OPTIONAL :: MYID
       LOGICAL, OPTIONAL :: NamedBinaries
       integer :: unitnum1,unitnum2,unitnum3,I_NUMBER,Ierr
       logical :: I_OPEN1,I_EXIST1,I_OPEN2,I_EXIST2,I_OPEN3,I_EXIST3
       character*80 :: CIc_FileName,PSI_FileName
       character*80 :: Header_FileName


      If ( Present(NamedBinaries) .eqv. .TRUE. ) THEN
         CIc_FileName=CI_Reference
         PSI_FileName=Psi_Reference
         Header_FileName=Header_Reference

         write(*,*) "Using this filename for the Psi reference:", PSI_FileName
         ! unit numbers in case of named binaries 
         unitnum1=801    ! coefficients
         unitnum2=802    ! orbitals
         unitnum3=803    ! header

      ELSE
         ! file names defaults
         CIc_FileName="CIc_bin"
         PSI_FileName="PSI_bin"
         Header_FileName="Header"

         ! unit number defaults
         unitnum1=777 ! coefficients 
         unitnum2=778 ! orbitals 
         unitnum3=779 ! header
      ENDIF



      IF ((Present(MYID).and.(MYID.eq.0)).or. &
          (Present(MYID).eqv..FALSE.)) THEN
         INQUIRE (FILE=CIc_FileName, OPENED=I_OPEN1, EXIST=I_EXIST1,&
                 NUMBER=I_NUMBER) 
         INQUIRE (FILE=PSI_FileName, OPENED=I_OPEN2, EXIST=I_EXIST2,&
                 NUMBER=I_NUMBER) 
         INQUIRE (FILE=Header_FileName, OPENED=I_OPEN3, EXIST=I_EXIST3,&
                 NUMBER=I_NUMBER) 
    
         IF(I_EXIST1.eqv..TRUE.) THEN
            WRITE(1003,*)" Binary CIc_bin exists",I_OPEN1,I_EXIST1
         ENDIF
    
         IF(I_EXIST2.eqv..TRUE.) THEN
            WRITE(1003,*)" Binary PSI_bin exists",I_OPEN2,I_EXIST2
         ENDIF
    
         IF(I_EXIST3.eqv..TRUE.) THEN
            WRITE(1003,*)"Header exists",I_OPEN3,I_EXIST3
         ENDIF

      ENDIF

        !open coefficients in unit 777
      IF (CI_V3.eqv..FALSE.) THEN

        IF (MYID.eq.0) THEN
          Open (unit=unitnum1, File=CIc_FileName, Status='unknown',& 
              Form='unformatted', Access='sequential')
        ENDIF

      ELSEIF (CI_V3.eqv..TRUE.) THEN
           call MPI_FILE_OPEN(MPI_COMM_WORLD, CIc_FileName, & 
                       MPI_MODE_RDWR + MPI_MODE_CREATE, & 
                       MPI_INFO_NULL,Coefficient_BinaryUnitNumber,ierr)
      ENDIF

      !open orbitals in unit 778
      IF (MPI_ORBS.eqv..FALSE.) THEN
        IF (MYID.eq.0) THEN
           Open (unit=unitnum2, File=PSI_FileName, Status='unknown',& 
                 Form='unformatted', Access='sequential')
           write(6,*)" Binary files properly opened"
        ENDIF
      ELSEIF (MPI_ORBS.eqv..TRUE.) THEN
           call MPI_FILE_OPEN(MPI_COMM_WORLD, PSI_FileName, & 
                       MPI_MODE_RDWR + MPI_MODE_CREATE, & 
                       MPI_INFO_NULL, Orbital_BinaryUnitNumber, ierr)
      ENDIF

        Open (unit=unitnum3, & 
              File=Header_FileName, &
              Status='unknown',& 
              Form='unformatted', & 
              Access='sequential')

      end SUBROUTINE Open_binary


!> Open binary coefficients and orbitals in 'CI0', 'PSI0' and "Head0" for second wavefunction.
       subroutine Open_bin_fidelity(MYID)

       USE Global_Parameters, ONLY: Orbital_Integrator,CI_V3,MPI_ORBS
       USE Orbital_Parallelization_Parameters, &
           ONLY: Orbital_BinaryUnitNumber
       USE Coefficients_Parallelization_Parameters, &
            ONLY: Coefficient_BinaryUnitNumber
       USE MPI

       implicit none
       integer, optional :: MYID
       integer :: unitnum1,unitnum2,unitnum3,I_NUMBER,ierr
       logical :: I_OPEN1,I_EXIST1,I_OPEN2,I_EXIST2,I_OPEN3,I_EXIST3
       character*7 :: filename1,filename2
       character*6 :: filename3


       filename1="CI0"
       unitnum1 = 801
       filename2="PSI0"
       unitnum2 = 802
       filename3="Head0"
       unitnum3 = 803
 
      IF ((Present(MYID).and.(MYID.eq.0)).or. &
          (Present(MYID).eqv..FALSE.)) THEN

        INQUIRE (FILE=filename1, OPENED=I_OPEN1, EXIST=I_EXIST1,&
                NUMBER=I_NUMBER) 
        INQUIRE (FILE=filename2, OPENED=I_OPEN2, EXIST=I_EXIST2,&
                NUMBER=I_NUMBER) 
        INQUIRE (FILE=filename3, OPENED=I_OPEN3, EXIST=I_EXIST3,&
                NUMBER=I_NUMBER) 

        IF(I_EXIST1.eqv..TRUE.) THEN
          WRITE(1003,*)" Binary CI0 exists",I_OPEN1,I_EXIST1
        ENDIF

        IF(I_EXIST2.eqv..TRUE.) THEN
          WRITE(1003,*)" Binary PSI0 exists",I_OPEN2,I_EXIST2
        ENDIF

        IF(I_EXIST3.eqv..TRUE.) THEN
          WRITE(1003,*)"Head0 exists",I_OPEN3,I_EXIST3
        ENDIF

      ENDIF

      !open coefficients in unit 801
      if (CI_V3 .eqv. .false.) then

        if (MYID .eq. 0) then
          Open (unit=unitnum1, File=filename1, Status='unknown',&
                Form='unformatted', Access='sequential')
        endif

      elseif (CI_V3 .eqv. .true.) then
        call MPI_FILE_OPEN(MPI_COMM_WORLD, filename1,       &
                           MPI_MODE_RDWR + MPI_MODE_CREATE, &
                           MPI_INFO_NULL,Coefficient_BinaryUnitNumber, &
                           ierr)
      endif


      !open orbitals for fidelity in unit 802
      if (MPI_ORBS .eqv. .false.) then
        if (MYID .eq. 0) then
          Open (unit=unitnum2, File=filename2, Status='unknown',&
                 Form='unformatted', Access='sequential')
        endif
      elseif (MPI_ORBS .eqv. .true.) then
        call MPI_FILE_OPEN(MPI_COMM_WORLD,filename2,      &
                           MPI_MODE_RDWR+MPI_MODE_CREATE, &
                           MPI_INFO_NULL,Orbital_BinaryUnitNumber,ierr)
      endif

      !open Header for fidelity in unit 803
      Open (unit=unitnum3, File=filename3, Status='unknown',&
            Form='unformatted', Access='sequential')

      write(6,*)" Binary files properly opened for fidelity"

      end subroutine Open_bin_fidelity

      !> Open binary coefficients and orbitals in 'CIc_bin_Reduced_WF' 
      !> and 'PSI_bin_Reduced_WF'.
      subroutine Open_Binary_Reduced_WF(mode)
      implicit none
      integer      :: mode, unitnum1, unitnum2, I_NUMBER
      logical      :: I_OPEN1, I_EXIST1, I_OPEN2, I_EXIST2
      character*20 :: filename1, filename2

      if (mode .eq. 1) then
        filename1 = "CIc_bin_Reduced_WF_X"
        filename2 = "PSI_bin_Reduced_WF_X"
      elseif (mode .eq. 2) then
        filename1 = "CIc_bin_Reduced_WF_K"
        filename2 = "PSI_bin_Reduced_WF_K"
      endif

      unitnum1  = 901
      unitnum2  = 902

      inquire (file=filename1,opened=I_OPEN1,exist=I_EXIST1,&
               number=I_NUMBER)
      inquire (file=filename2,opened=I_OPEN2,exist=I_EXIST2,&
               number=I_NUMBER)

      if(I_EXIST1 .eqv. .true.) then
        write(0,*)" Binary CI_bin_Reduced_WF exists", I_OPEN1, I_EXIST1
      endif

      if(I_EXIST2 .eqv. .true.) then
        write(0,*)" Binary PSI_bin_Reduced_WF exists", I_OPEN2, I_EXIST2
      endif

      open (unit=unitnum1,file=filename1,status='unknown',&
       form='unformatted',access='sequential')

      open (unit=unitnum2,file=filename2,status='unknown',&
            form='unformatted',access='sequential')

      write(0,*)" Binary files properly opened"

      end subroutine Open_Binary_Reduced_WF

      !> Routine for closing binary orbital (778), coefficient (777), and header (779) files.
      !> Assumes the units 777, 778, 779 to be opened and closes them. 
      !> Checks if 801 (named reference coefficient file), 802 (named reference orbital file), 
      !> and 803 (named reference header file) are opened and closes them if they are.
      subroutine Close_binary(MYID)
      USE Global_Parameters, ONLY: Orbital_Integrator,MPI_ORBS
      USE MPI
      USE Orbital_Parallelization_Parameters!   Orbital_BinaryUnitNumber

      IMPLICIT NONE
      INTEGER :: MYID,IERR
      logical itsopen 

!> Close binary coefficients and orbitals in 'CIc_bin' and 'PSI_bin'.
      IF (MYID.eq.0) THEN
         close(777) !Coefficients
      ENDIF

      close(779) !Header

! Orbitals
       IF (MPI_ORBS.eqv..TRUE.) THEN
          call MPI_FILE_CLOSE(Orbital_BinaryUnitNumber, ierr)
       ELSEIF (MPI_ORBS.eqv..FALSE.) THEN
         IF (MYID.eq.0) THEN
            close(778)
         ENDIF
       ENDIF

       write(6,*)" Binary files are properly closed, PROC ", MYID


       inquire(unit=803, opened=itsopen) 
       if ( itsopen ) then
           close(803)
       end if

       inquire(unit=802, opened=itsopen) 
       if ( itsopen ) then
           close(802)
       end if

       inquire(unit=801, opened=itsopen) 
       if ( itsopen ) then
           close(801)
       end if

       end SUBROUTINE Close_binary
 

!> Close binary coefficients and orbitals in 'CI0' and 'PSI0'.
       subroutine Close_bin_fidelity()
       close(801)
       close(802)
       write(6,*)" Binary files are properly closed for fidelity"
       end SUBROUTINE Close_bin_fidelity

!> Close binary coefficients and orbitals in 'CIc_bin_Reduced_WF' 
!> and 'PSI_bin_Reduced_WF'. 
       subroutine Close_Binary_Reduced_WF()
       close(901)
       close(902)
       write(6,*)" Binary files are properly closed for Reduced WF"
       end subroutine Close_Binary_Reduced_WF
       
!> Write the header data to binary files.
       subroutine Write_Header(unitnum,NPart)
       use Global_Parameters
       use Coefficients_Parameters
       use Orbital_Parallelization_Parameters
       use Coefficients_Parallelization_Parameters
       use Interaction_Parameters
       use DVR_Parameters
       use Matrix_Elements
       use CI_Production_Parameters

       implicit none

       integer      :: unitnum, NPart
       character*21 :: fname

       write(unitnum)  &
           Job_type,&
           Morb, &
           NPart, &
           Job_Prefactor,&
           xlambda_0,&
           mass,&
           GUESS,&
           Act_Operator_Init,&
           DO_OCT,&
           OCT_Restart,&
           OCT_NC,&
           OCT_Alpha,&
           OCT_NGoals,&
           OCT_Beta,&
           OCT_TimeSteps,&
           OCT_Randomize,&
           OCT_Bounded,&
           OCT_OptOmega,&
           OCT_Method,&
           OCT_LowerBounds,&
           OCT_UpperBounds,&
           OCT_NRandomize,&
           OCT_Func_Tolerance,&
           OCT_CRAB_Tolerance,&
           OCT_Bounded_Bandwidth,&
           OCT_Bandwidth_LowerBound,&
           OCT_Bandwidth_UpperBound,&
           DO_Fejer,&
           dCRAB,&
           SIMANN,&
           NProjections,&
           Multi_Level,&
           Conical_Intersection,&
           Nlevel,&
           InterLevel_InterParticle,&
           Spinor,&
           xlambda1, &
           xlambda2, &
           xlambda12,&
           xlambda3, &
           Lambda1,&
           Lambda2,&
           Bose_Hubbard,Periodic_BH,BH_J,BH_U,&
           Calculate_Rho2,&
           Cavity_BEC,& 
           NCavity_Modes,&
           Cavity_PumpRate,&
           Cavity_LossRate,& 
           Cavity_Detuning,& 
           Atom_Detuning,& 
           Cavity_K0,& 
           CavityAtom_Coupling,&
           X_Cavity_Pump_Waist,&
           Cavity_Mode_Waist,&
           Pump_Switch,&
           RampupTime,&
           RampdownTime,&
           PlateauTime,&
           Pump_Oscillate,& 
           Pump_Amplitude,& 
           Pump_Period,&    
           Custom_Cavity,&
           Which_Cavity,&
           Transverse_Pump,&
           Detuning_Switch,&
           Detuning_Switch_Mode,&
           Detuning_SwitchValue,&
           Detuning_SwitchTime,&
           Binary_Start_Time,&
           Restart_State,&
           Restart_Orbital_FileName,&
           Restart_Coefficients_FileName,&
           TNT_data,&
           Diagonalize_OneBodyh,&
           Fixed_Lz, &
           OrbLz,&
           Vortex_Imprint,&
           Profile
     
         write(unitnum) DIM_MCTDH, NDVR_X, NDVR_Y, NDVR_Z, &
           DVR_X,DVR_Y,DVR_Z,&
           X_initial,X_final,Y_initial,Y_final,Z_initial,Z_final
     
         write(unitnum)  &
           Time_Begin,&
           Time_Final,&
           Time_MAX,&
           Output_TimeStep,&
           Output_Coefficients,&
           Integration_StepSize,&
           Error_Tolerance,&
           Error_Rescale_TD,&
           Error_Rescale_Cavity,&
           Minimal_Krylov,&
           Maximal_Krylov,&
           Minimal_Occupation,&
           MPI_ORBS,&
           LZ,&
           Magnetic_FFT,&
           OmegaZ,&
           Coefficients_Integrator,&
           NProc_Max_CI,&
           BlockSize,&
           RLX_Emin,&
           RLX_Emax,&
           Olsen

       write(unitnum) whichpot,parameter1,      &
           parameter2, parameter3, parameter4,  &
           parameter5, parameter6, parameter7,  &
           parameter8, parameter9, parameter10, &
           parameter11,parameter12,parameter13, &
           parameter14,parameter15,parameter16, &
           parameter17,parameter18,parameter19, &
           parameter20,parameter21,parameter22, &
           parameter23,parameter24,parameter25, &
           parameter26,parameter27,parameter28, &
           parameter29,parameter30

       write(unitnum)&
           Which_Interaction,&
           Interaction_parameter1, &
           Interaction_parameter2, &
           Interaction_parameter3, &
           Interaction_parameter4, &
           Interaction_parameter5, &
           Interaction_parameter6, &
           Interaction_parameter7, &
           Interaction_parameter8, &
           Interaction_parameter9, &
           Interaction_parameter10,&
           Interaction_Width,&
           Interaction_Type

       if(unitnum.eq. 777) fname="called write header with wrong input"
       if(unitnum.eq. 778) fname="called write header with wrong input"
       if(unitnum.eq. 779) fname="Header"

       write(6,*)"Headers written to Binary file ",fname

       end SUBROUTINE Write_Header
      
!> Read the header of the binary files.
       subroutine Read_Header(unitnum,rtype)

       use Global_Parameters
       use Coefficients_Parameters
       use Orbital_Parallelization_Parameters
       use Coefficients_Parallelization_Parameters
       use Interaction_Parameters
       use DVR_Parameters
       use Matrix_Elements
       USE CI_Production_Parameters 
       USE Input_Namelists

       implicit none

       integer :: unitnum
       
       character*19 :: fname
       character*4 :: rtype !'scan','full'
       
       integer :: MYID,I_error
      

      select case (rtype)
          case('full')
     
       read(unitnum)  &
           Job_type,&
           Morb, &
           NPar, &
           Job_Prefactor,&
           xlambda_0,&
           mass,&
           GUESS,&
           Act_Operator_Init,&
           DO_OCT,&
           OCT_Restart,&
           OCT_NC,&
           OCT_Alpha,&
           OCT_NGoals,&
           OCT_Beta,&
           OCT_TimeSteps,&
           OCT_Randomize,&
           OCT_Bounded,&
           OCT_OptOmega,&
           OCT_Method,&
           OCT_LowerBounds,&
           OCT_UpperBounds,&
           OCT_Bounded_Bandwidth,&
           OCT_Bandwidth_LowerBound,&
           OCT_Bandwidth_UpperBound,&
           OCT_NRandomize,&
           OCT_Func_Tolerance,& 
           OCT_CRAB_Tolerance,&
           DO_Fejer,&
           dCRAB,&
           SIMANN,&
           NProjections,&
           Multi_Level,&
           Conical_Intersection,&
           Nlevel,&
           InterLevel_InterParticle,&
           Spinor,&
           xlambda1, &
           xlambda2, &
           xlambda12,&
           xlambda3, &
           Lambda1,&
           Lambda2,&
           Bose_Hubbard,Periodic_BH,BH_J,BH_U,&
           Calculate_Rho2,&
           Cavity_BEC,& 
           NCavity_Modes,&
           Cavity_PumpRate,&
           Cavity_LossRate,& 
           Cavity_Detuning,& 
           Cavity_K0,& 
           CavityAtom_Coupling,&
           X_Cavity_Pump_Waist,&
           Cavity_Mode_Waist,&
           Pump_Switch,&
           RampupTime,&
           RampdownTime,&
           PlateauTime,&
           Pump_Oscillate,& 
           Pump_Amplitude,& 
           Pump_Period,&    
           Binary_Start_Time,&
           Restart_State,&
           Restart_Orbital_FileName,&
           Restart_Coefficients_FileName,&
           TNT_data,&
           Diagonalize_OneBodyh,&
           Fixed_Lz, &
           OrbLz,&
           Vortex_Imprint,&
           Profile
     
         read(unitnum) DIM_MCTDH, NDVR_X, NDVR_Y, NDVR_Z, &
           DVR_X,DVR_Y,DVR_Z,&
           X_initial,X_final,Y_initial,Y_final,Z_initial,Z_final
     
         read(unitnum)  &
           Time_Begin,&
           Time_Final,&
           Time_MAX,&
           Output_TimeStep,&
           Output_Coefficients,&
           Integration_StepSize,&
           Error_Tolerance,&
           Error_Rescale_TD,&
           Error_Rescale_Cavity,&
           Minimal_Krylov,&
           Maximal_Krylov,&
           Minimal_Occupation,&
           MPI_ORBS,&
           Coefficients_Integrator,&
           NProc_Max_CI,&
           BlockSize,&
           RLX_Emin,&
           RLX_Emax,&
           Olsen

       read(unitnum) whichpot,parameter1,      &
           parameter2, parameter3, parameter4,  &
           parameter5, parameter6, parameter7,  &
           parameter8, parameter9, parameter10, &
           parameter11,parameter12,parameter13, &
           parameter14,parameter15,parameter16, &
           parameter17,parameter18,parameter19, &
           parameter20,parameter21,parameter22, &
           parameter23,parameter24,parameter25, &
           parameter26,parameter27,parameter28, &
           parameter29,parameter30

       read(unitnum)&
           Which_Interaction,&
           Interaction_parameter1, &
           Interaction_parameter2, &
           Interaction_parameter3, &
           Interaction_parameter4, &
           Interaction_parameter5, &
           Interaction_parameter6, &
           Interaction_parameter7, &
           Interaction_parameter8, &
           Interaction_parameter9, &
           Interaction_parameter10,&
           Interaction_Width,&
           Interaction_Type

      CALL MPI_COMM_RANK(MPI_COMM_WORLD,myid,I_error)

!      IF (MYID.eq.0) THEN
!        fname="OLD_MCTDHB_HEADER.inp"
!  
!        Write(6,*) "fname",fname,"unit",unitnum
!        open(776,file=fname, status='unknown', recl=8000,&
!                          delim='APOSTROPHE')
!  
!         write(776,NML=System_Parameters) 
!         write(776,NML=DVR) 
!         write(776,NML=Integration) 
!         write(776,NML=Potential) 
!         write(776,NML=Interaction) 
!         close(776)
!
!         write(1003,*)"Header from old binary file saved in ",fname
!       ENDIF

         write(1003,*)" MPI_ORBS READ FROM Header", MPI_ORBS

       return


            case ('scan')
       read(unitnum,end=10,err=10) 
       read(unitnum,end=10,err=10)
       read(unitnum,end=10,err=10)
       read(unitnum,end=10,err=10)
       read(unitnum,end=10,err=10)
       write(1003,*)"Scanning: Binary Headers are skipped.",unitnum
            return
            end select
10     write(1003,*)  "Binary Header System Parameters is corrupted"
       return
11     write(1003,*)  "Binary Header DVR Parameters is corrupted"
       return
12     write(1003,*)  "Binary Header Integration Pars is corrupted"
       return
13     write(1003,*)  "Binary Header Potential Parameters is corrupted"
       return
14     write(1003,*)  "Binary Header Interaction Pars is corrupted"
       return

       end SUBROUTINE Read_Header

!>     Write the Multilevel Orbitals to binary file.
       subroutine Write_NlevelOrbitals_Binary(time,iter,PSI)

       use Global_Parameters, ONLY: NDVR_X,NDVR_Y,NDVR_Z,Morb,Nlevel,&
                                    energy
       use Orbital_Allocatables, ONLY: VTRAP_EXT_Nlevel
       use Matrix_Elements, ONLY: Rho1_Elements, Rho2_Elements

       real*8 ::  time
       integer*8 :: iter
       COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb,Nlevel)  :: PSI

        write(778)  time, iter, energy &
                  ,Rho1_Elements,Rho2_Elements,PSI,VTRAP_EXT_Nlevel

       end SUBROUTINE Write_NlevelOrbitals_Binary



!>     Write the Orbitals to binary file.
       subroutine Write_Orbitals_Binary(time,iter,PSI,unitnum,MYID)
       use Global_Parameters
       use Coefficients_Parameters
       use Orbital_Parallelization_Parameters
       use Orbital_Allocatables,ONLY: VTRAP_EXT
       use Coefficients_Parallelization_Parameters
       use Interaction_Parameters
       use DVR_Parameters
       use Matrix_Elements
       USE CI_Production_Parameters 
    
       IMPLICIT NONE 

       real*8 ::  time,enrg
       integer*8 :: iter
       integer :: ierr, unitnum

       integer :: K

       COMPLEX*16 :: CDummy
       REAL*8 :: RDummy
       INTEGER*8 :: IDummy

       INTEGER ::  MYID

       INTEGER,SAVE :: orbstep=0

#if MPIFFT
       COMPLEX*16, DIMENSION(LocalPsiDim(MYID+1),Morb)  :: PSI
       integer(kind=MPI_OFFSET_KIND) ::  displacement       
#else
       COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb)  :: PSI
       integer*8 :: displacement
#endif
 
       IF (trim(Coefficients_Integrator).ne.'BDV') THEN
                
         IF (MPI_ORBS.eqv..TRUE.) THEN
           displacement=orbstep*Orbital_BlockSize               

!           write(6,*) "write step",orbstep,"block size", &
!                 Orbital_Blocksize,"init disp",displacement

!c============== Advance to position of this dataset
           CALL MPI_File_seek(Orbital_BinaryUnitNumber,            & 
                             displacement,               &
                             MPI_SEEK_SET, IERR )                   

           IF (MYID.eq.0) THEN
!c============= Write Time                                              
!            write(6,*) "writing time::", time, "at", displacement
             call MPI_FILE_WRITE(Orbital_BinaryUnitNumber,         &
                                 time, 1,                            &
                                 MPI_DOUBLE_PRECISION,               &
                                 MPI_STATUS_IGNORE, ierr)          

!c============= Advance                                                
             CALL MPI_File_seek(Orbital_BinaryUnitNumber,             &
                                displacement                          &
                                +SIZEOF(RDummy),                      &
                                MPI_SEEK_SET, IERR )                  
!c============= Write step number                                       

             call MPI_FILE_WRITE(Orbital_BinaryUnitNumber,         &
                                    REAL(iter,8), 1,                  &
                                    MPI_DOUBLE_PRECISION,             &
                                    MPI_STATUS_IGNORE, ierr)          
!c============= Advance                                                
             CALL MPI_File_seek(Orbital_BinaryUnitNumber,             &
                                displacement                          &
                                +SIZEOF(RDummy)+SIZEOF(RDummy),       &
                                MPI_SEEK_SET, IERR )                  
!c============= Write energy                                            

             call MPI_FILE_WRITE(Orbital_BinaryUnitNumber,         &
                                 energy, 1,                          &
                                 MPI_DOUBLE_PRECISION,               &
                                 MPI_STATUS_IGNORE, ierr)           
                                                                      
!c============= Advance                                                
             CALL MPI_File_seek(Orbital_BinaryUnitNumber,             &
                                displacement                          &
                                +2*SIZEOF(RDummy)+SIZEOF(RDummy),     &
                                MPI_SEEK_SET, IERR )                   
                                                                      

!c============= Write rho^1 elements                                    
             call MPI_FILE_WRITE(Orbital_BinaryUnitNumber,         &
                                    Rho1_Elements, RDim,              &
                                    MPI_Double_Complex,               &
                                    MPI_STATUS_IGNORE, ierr)          
!c============= Advance                                                
             CALL MPI_File_seek(Orbital_BinaryUnitNumber,             &
                                displacement                          &
                                +2*SIZEOF(RDummy)+SIZEOF(RDummy)      &
                                +Rdim*SIZEOF(CDummy),                 &
                                MPI_SEEK_SET, IERR )                  

!c============= Write rho^2 elements                                    

             call MPI_FILE_WRITE(Orbital_BinaryUnitNumber,         &
                                    Rho2_Elements, RDim1,             &
                                    MPI_Double_Complex,               &
                                    MPI_STATUS_IGNORE, ierr)
!c============== Advance processes to displaced positions of the respective
!c============== domain of PSI they are working on
           ENDIF     

              IF (MYID.ne.0) THEN



                 DISPLACEMENT=DISPLACEMENT                        &
                             +Sum(LocalPsiDim(1:MYID))            &
                             *SIZEOF(CDummy)                      &
                             +2*SIZEOF(RDummy)+SIZEOF(RDummy)     &
                             +(Rdim+Rdim1)*SIZEOF(CDummy)             


              ELSE

                 DISPLACEMENT=DISPLACEMENT                        &
                             +2*SIZEOF(RDummy)+SIZEOF(RDummy)     &
                             +(Rdim+Rdim1)*SIZEOF(CDummy)             

              ENDIF                                                 
                                                                   
 
             CALL MPI_File_seek(Orbital_BinaryUnitNumber,         &
                                DISPLACEMENT,                     &
                                MPI_SEEK_SET, IERR )

!c============== WRITE Orbitals

              DO K=1,Morb

!                 Write(6,*) "MYID", MYID,"ORB",K,"WRITES AT", &
!                           DISPLACEMENT
                 call MPI_FILE_WRITE(Orbital_BinaryUnitNumber,    &
                                     PSI(:,K),                    &
                                     INT(LocalPsiDim(MYID+1),4),  &
                                     MPI_Double_Complex,          &
                                     MPI_STATUS_IGNORE, ierr)       
                                                                        
                 DISPLACEMENT=DISPLACEMENT                      &
                               +NDVR_X*NDVR_Y*NDVR_Z            &   
                               *SIZEOF(CDummy)                        
                                                                       
!c============= Advance processes to displaced positions of the respect=ive
!c============= domain of PSI they are working on                       
                CALL MPI_File_seek(Orbital_BinaryUnitNumber,      &
                                   DISPLACEMENT,                  & 
                                   MPI_SEEK_SET,IERR)                 

            END DO                                                      

!           Write(6,*) "MYID", MYID,"HAS NORMS",& 
!                       SUM(PSI(:,1)*dconjg(PSI(:,1))),&
!                       SUM(PSI(:,2)*dconjg(PSI(:,2)))

           call MPI_FILE_WRITE(Orbital_BinaryUnitNumber,          &
                               VTrap_EXT,                         &
                               INT(LocalPsiDim(MYID+1),4),        &
                               MPI_Double_Complex,                &
                               MPI_STATUS_IGNORE, ierr)             
                                                                        
!           Write(6,*) "MYID", MYID,"POT DISPL WRITE", DISPLACEMENT, &
!                      "WROTE VTRAP_EXT", VTRAP_EXT(1), SIZE(VTRAP_EXT) 

        orbstep=orbstep+1
        ELSEIF (MPI_ORBS.eqv..FALSE.) THEN

          write(unitnum)  time, iter, energy &
              ,Rho1_Elements,Rho2_Elements,PSI,VTRAP_EXT
        ENDIF
       ELSE
          write(unitnum)  time, iter, energy &
              ,Rho1_Elements_Block,Rho2_Elements_Block,PSI,VTRAP_EXT
       ENDIF

!      CALL MPI_BARRIER(MPI_COMM_WORLD,ierr)
!      IF (MYID.eq.0) THEN
!        Write(6,*) "PSI 1 ::: ", PSI(:,1)
!        Write(6,*) "PSI 2 ::: ", PSI(:,2)
!      ENDIF
!      CALL MPI_BARRIER(MPI_COMM_WORLD,ierr)
!      IF (MYID.eq.1) THEN
!        Write(6,*) "PSI 1 ::: ", PSI(:,1)
!        Write(6,*) "PSI 2 ::: ", PSI(:,2)
!      ENDIF
!      CALL MPI_BARRIER(MPI_COMM_WORLD,ierr)
   


       end SUBROUTINE Write_Orbitals_Binary


       subroutine Write_Coefficients_Block_Binary(time,iter,VIN)
!>     Writes the CI coefficients for Block Davidson computations to binary file

       USE   Global_Parameters
       USE   Matrix_Elements
       USE   Coefficients_Parameters
       USE   Interaction_Parameters
       USE   DVR_Parameters

       real*8 ::  time
       integer*8 :: iter
       COMPLEX*16, DIMENSION(Nconf*BlockSize) :: VIN

       write(777)  time, iter, energy, VIN

       end SUBROUTINE Write_Coefficients_Block_Binary


!>     Writes the CI coefficients to binary file
       subroutine Write_Coefficients_Binary(time,iter,VIN,NConfig, &
                                            unitnum)

       USE   Global_Parameters
       USE   Matrix_Elements
       USE   Coefficients_Parameters
       USE   Interaction_Parameters
       USE   DVR_Parameters

       real*8 ::  time
       integer*8 :: iter
       integer ::  NConfig, unitnum
       COMPLEX*16, DIMENSION(NConfig) :: VIN

       IF (MORB.gt.1) THEN
          write(unitnum)  time, iter, energy, VIN
       ELSEIF (MORB.eq.1) THEN
          write(unitnum)  time, iter, energy, dcmplx(1.d0,0.d0)
       ENDIF

       end SUBROUTINE Write_Coefficients_Binary

      subroutine Read_BIN_string(unitnum,time,iter,enrg,ReadState)

      USE Global_Parameters,ONLY: MPI_ORBS

      IMPLICIT NONE

      real*8 ::  time, enrg
      integer*8 :: iter
      integer :: ReadState,unitnum
                            ! time; iteration; energy

      IF ((MPI_ORBS.eqv..TRUE.).and.(unitnum.eq.778)) THEN
        Write(6,*) "Now I analyze the binary::: should not be here!!!"
        stop
      ELSE
        read(unitnum,end=10,err=20)  time, iter, enrg
        return
      ENDIF

10    ReadState=1
      write(6,*)"Last record From binary ",unitnum, time,iter,enrg, &
                                           ReadState
      return

20    ReadState=-1
      write(6,*)"Wrong binary cannot be read",&
                       unitnum, time, iter, enrg, ReadState
      return

      end SUBROUTINE Read_BIN_string

      subroutine Read_BIN_string_MPI(unitnum,time,iter,enrg,state,MYID)

      USE Global_Parameters,ONLY: Orbital_Integrator,MPI_ORBS
      
      USE Orbital_Parallelization_Parameters, &
                    Only: LocalPsiDim,OrbitalWrite_Offsets,&
                          Orbital_Blocksize,Local_Offset,&
                          Orbital_BinaryUnitNumber, &
                          OrbitalBinarySize,&
                          LocalPsiDim_inter,Local_Offset_inter,&
                          Orbital_Blocksize_inter
                           
      USE MPI
      
      IMPLICIT NONE
      integer :: MYID

      real*8 ::  time, enrg,iter_r8
      integer*8 :: iter
      integer :: state,unitnum
                            ! time; iteration; energy
      logical,save :: First=.TRUE.
      integer,save :: Step,NSteps

      INTEGER*8  :: Idummy
      REAL*8     :: Rdummy
      COMPLEX*16 :: Cdummy 

      INTEGER*8,SAVE  :: Orbital_BlockSize_Local

      INTEGER :: IERR


      IF (First.eqv..TRUE.) THEN

         First=.FALSE.
         Write(6,*) "MYID", MYID,"set First",First
         Write(6,*) "Unit no", Orbital_BinaryUnitNumber
   
         CALL MPI_File_get_size(Orbital_BinaryUnitNumber,       &
                                OrbitalBinarySize, IERR )
         Write(6,*) "MYID", MYID,"found Filesize", OrbitalBinarySize   

         IF (Allocated(LocalPsiDim_inter).eqv..TRUE.) THEN
            Orbital_BlockSize_Local=Orbital_BlockSize_Inter
            Write(6,*) "MYID", MYID," Reads interp. block",&
                         Orbital_BlockSize_Inter, &
                        "and orbital Block", Orbital_BlockSize
         ELSE
            Orbital_BlockSize_Local=Orbital_BlockSize
         ENDIF

         NSteps=(OrbitalBinarySize/Orbital_BlockSize_Local)-1

         IF (NSteps.lt.0) THEN
            Write(6,*) "Binary seems corrupted"
            STOP
         ELSE 
            Write(6,*) "Binary contains", NSteps+1, "datasets"
         ENDIF

         Step=-1

      ENDIF

      Step=Step+1

      IF (MPI_ORBS.eqv..FALSE.) THEN
        Write(6,*) "Something Wrong in MPI I/O"
        stop
      ELSE
!c============== Advance to position of this dataset
              CALL MPI_File_seek(Orbital_BinaryUnitNumber,            & 
                                step*Orbital_BlockSize_Local,         &
                                MPI_SEEK_SET, IERR )                   
!c============= Read Time                                              
             call MPI_FILE_READ(Orbital_BinaryUnitNumber,         &
                                time, 1,                          &
                                MPI_DOUBLE_PRECISION,             &
                                MPI_STATUS_IGNORE, ierr)          

!c============= Advance                                                
             CALL MPI_File_seek(Orbital_BinaryUnitNumber,             &
                                step*Orbital_BlockSize_Local          &
                                +SIZEOF(RDummy),                      &
                                MPI_SEEK_SET, IERR )                  
!c============= Read step number                                       
 
             call MPI_FILE_READ(Orbital_BinaryUnitNumber,         &
                                iter_r8, 1,                  &
                                MPI_DOUBLE_PRECISION,             &
                                MPI_STATUS_IGNORE, ierr)          
            iter=nint(iter_r8) 
 
!c============= Advance                                                
             CALL MPI_File_seek(Orbital_BinaryUnitNumber,             &
                              step*Orbital_BlockSize_Local            &
                                +SIZEOF(RDummy)+SIZEOF(RDummy),       &
                                MPI_SEEK_SET, IERR )                  
!c============= Read energy                                            

             call MPI_FILE_READ_ALL(Orbital_BinaryUnitNumber,         &
                                    enrg, 1,                        &
                                    MPI_REAL8,            &
                                    MPI_STATUS_IGNORE, ierr)           
                                                                      

          IF (step.lt.NSteps) THEN
             state=1
          ELSEIF (step.eq.NSteps) THEN 
             state=-2
          ENDIF

          write(6,*) "MYID",MYID, "Last record From binary ", &
                               unitnum, time,iter,enrg,state
        return

      ENDIF

      end SUBROUTINE Read_BIN_string_MPI

!>       Routine assesses whether the reading operation 
!>       is possible.
!>       It's called after the header was read.
       subroutine Analyse_Binary(unitnum,required_time,MYID)

       use Global_Parameters
       use Coefficients_Parameters
       use Orbital_Parallelization_Parameters
       use Coefficients_Parallelization_Parameters
       use Interaction_Parameters
       use DVR_Parameters
       use Matrix_Elements
       USE CI_Production_Parameters 

       USE MPI

       integer :: MYID
       integer :: unitnum
       integer*8 :: iter
       real*8 :: tm,required_time,enrg

       INTEGER :: ReadState
       INTEGER :: ierr

       ReadState=1

       WRITE(1003,*) "################################"
       WRITE(1003,*) "#####Analyzing Binary Data######"
       WRITE(1003,*) "################################"

       DO WHILE (ReadState .GT.0)

          select case (unitnum)

          case (778)
             !> serial I/O
             IF (MPI_ORBS.eqv..FALSE.) THEN
                IF (MYID.eq.0) THEN
                   call Read_BIN_string(778,tm,iter,enrg,ReadState)
                   write(1003,*) "From PSI_bin file",tm,iter,enrg
                ELSE
                   tm=required_time
                ENDIF
             ELSE !> parallel I/O

                 call Read_BIN_string_MPI(778,tm,iter,enrg,ReadState, &
                                          MYID)
                 write(6,*) "MYID",MYID, "exit MPI_string:", ReadState
                 
                 IF (MYID.eq.0) THEN
                    write(1003,*) "From PSI_bin file",tm,iter,enrg
                 ENDIF
                
             ENDIF

          case (777)

              IF (MYID.eq.0) THEN
                call Read_BIN_string(777,tm,iter,enrg,ReadState)
                write(1003,*) "From CIc_bin file",tm,iter,enrg
              ELSE
                tm=required_time
              ENDIF
          end select 

          IF (ABS(tm-required_time).le.1.0d-8) THEN
             ReadState=-2
          ENDIF


       END DO 


       IF (ReadState.le.0) THEN
            write(1003,*) "Binary reading was stopped on",tm,iter,enrg
       ENDIF

       WRITE(1003,*) "################################"
       WRITE(1003,*) "#####Analyzed Binary Data#######"
       WRITE(1003,*) "################################"

       required_time=tm

       end SUBROUTINE Analyse_BINARY

!> *****************
!> ** Routine: READ_BINARY_Nlevel
!> *****************
!> Read Coefficients or multi-component orbitals from binary file
!> specified with unitnum at Required_Time.
!> Is called after the header was read.
!> *****************
!> ** Inputs:
!> *****************
!> unitnum (integer): file to be read (either 777 for coefficients or 778 for orbitals)
!> required_time (real*8): time at which binary data should be read 
!> VIN (complex*16): vector of coefficients (length Nconf)
!> PSI (complex*16): vector of orbitals (NDVR_X*NDVR_Y*NDVR_Z,Morb,NLevel)
!> *****************
!> ** Outputs:
!> *****************
!> required_time (real*8): time at which binary data has been read (maybe slightly different from Required_Time on input)
!> VIN (complex*16): if unitnum=777 VIN contains the coefficients on output  
!> PSI (complex*16): if unitnum=778 PSI contains the coefficients on output  
       subroutine READ_BINARY_Nlevel(unitnum,required_time,VIN,PSI) 

       USE Global_Parameters, ONLY: NDVR_X,NDVR_Y,NDVR_Z,Morb,NLevel, &
                                    energy !< global variables used
       USE Matrix_Elements, ONLY: Rho1_Elements, Rho2_Elements !< matrix elements used
       USE Coefficients_Parameters, ONLY: Nconf !< number of configurations used
       USE Orbital_Allocatables, ONLY: VTRAP_EXT_Nlevel !< multi-component potential is used

       implicit NONE  !< No default variable declarations
 
       COMPLEX*16, DIMENSION(Nconf), INTENT(INOUT) :: VIN !< array of coefficients
       COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb,NLevel), & 
                   INTENT(INOUT)  :: PSI !< array of multi-component orbitals
       integer,INTENT(IN) :: unitnum !< unit number to read from
       real*8, INTENT(INOUT) :: Required_Time !< time requested 

       real*8  :: ReadTime  !< time read from binary
       integer :: ReadState !< integer for the state of the read operation; ReadState<0 means we're done
       integer*8 :: iter    !< dummy integer for reading the integration step from the binary files

       !> Write status to Initialization.da (unit 1003)
       WRITE(1003,*) "################################"
       WRITE(1003,*) "#####READING Binary Data#########"
       WRITE(1003,*) "################################"

       !> initialize ReadState to something positive (i.e., "not done") 
       ReadState=1

       !> loop through entries in binary while ReadState > 0
       DO WHILE (ReadState .GT.0)

         !> perform read operations on selected binary file
         select case (unitnum)
         !> for unitnum = 778 the orbitals are read
         case (778)
         !> read (in this order): time, iteration number, energy, 
         !> Elements of the one-body density, Elements of the two-body density, 
         !> the multi-component orbitals, the multi-component potential
            read(778) ReadTime,iter,Energy,Rho1_Elements, &
                      Rho2_Elements,PSI,VTRAP_EXT_Nlevel
                      
         !> for unitnum = 777 the coefficients are read
         case (777)
         !> read (in this order): time, iteration number, energy, array of coefficients 
            read(777)  ReadTime, iter, Energy ,VIN
                     
         case default
         !> for unitnum != 777 or 778 throw an error
            STOP "wrong unit number specified READ_BINARY_NLevel"
         end select 
     
         !> if the required time has been reached, terminate the reading loop
         !> by setting ReadState < 0
         IF (ABS(ReadTime-Required_Time).le.0.0000001) THEN
             ReadState=-2 
             Required_Time=ReadTime !< output the actual time read from the binary as Required_Time
             !> write some logging information to "Initialization.dat" (unit 1003)
              write(1003,*) &
               "Guess from binary: reading was stopped at", &
               Required_Time,iter,Energy
         ENDIF

       END DO

       !> Write status to Initialization.dat (unit 1003)
       WRITE(1003,*) "################################"
       WRITE(1003,*) "#####READ Binary Data###########"
       WRITE(1003,*) "################################"
       end SUBROUTINE READ_BINARY_Nlevel

!> Read Coefficients and Orbitals from binary file 
!> specified with unitnum for restarting a block diagonalization.
!> Is called after the header was read.
       subroutine READ_BINARY_Block(unitnum,required_time,VIN,PSI)

       use Global_Parameters
       USE Matrix_Elements
       USE Coefficients_Parameters
       USE Orbital_Parallelization_Parameters
       USE DVR_Parameters
 
       COMPLEX*16, DIMENSION(Nconf*BlockSize) :: VIN

       COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb)  :: PSI
       integer :: unitnum,iter,nstate
       real*8 :: tm,required_time,enrg
       

       nstate=1

          WRITE(1003,*) "#############################################"
          WRITE(1003,*) "#####READING from Binary Block Data #########"
          WRITE(1003,*) "#############################################"
        
         
          IF (Allocated(Rho1_Elements_Block).eqv..FALSE.) THEN
             Write(6,*) "Rho1_Elements_Block not allocated & 
                        in READ_BINARY_BLOCK!!!"
             stop
          ENDIF       

          IF (Allocated(Rho2_Elements_Block).eqv..FALSE.) THEN
             Write(6,*) "Rho2_Elements_Block not allocated &
                        in READ_BINARY_BLOCK!!!"
             stop
          ENDIF       

         DO 21 WHILE (nstate .GT.0)
           select case (unitnum)
           case (778)
              read(778) tm,iter,enrg,Rho1_Elements_Block,&
                        Rho2_Elements_Block,PSI,VTRAP_EXT
              IF(ABS(tm-required_time).le.0.0000001) then
                  nstate=-2
                  Rho1_Elements=Rho1_Elements_Block(:,1)
                  Rho2_Elements=Rho2_Elements_Block(:,1)
              ENDIF

           case (777)
              read(777)  tm, iter, enrg ,VIN
              IF(ABS(tm-required_time).le.0.0000001) then
                  nstate=-2
              ENDIF
           end select 

           IF (nstate.le.0) write(1003,*) &
                "Guess from binary: reading was stopped at",tm,iter,enrg
   21    ENDDO
    
         required_time=tm
         energy=enrg

         WRITE(1003,*) "#############################################"
         WRITE(1003,*) "#####READING from Binary Block Data #########"
         WRITE(1003,*) "#############################################"

       end SUBROUTINE READ_BINARY_BLOCK


!> Read Coefficients and Orbitals from binary file
!> specified with unitnum. If the optional argument Block is true 
!> then a specific state from a previous block relaxation is read.
!> Is called after the header was read.
       subroutine READ_BINARY(unitnum,required_time,VIN,PSI,MYID,&
                              DVR_size,Block) 

       use Global_Parameters
       USE Matrix_Elements
       USE Orbital_Allocatables, ONLY: VTRAP_EXT
       USE Coefficients_Parameters
       USE DVR_Parameters
       USE Orbital_Parallelization_Parameters, &
           ONLY:Orbital_BinaryUnitNumber,Orbital_BlockSize,&
                LocalPsiDim,LocalPsiDim_Inter,Orbital_BlockSize_Inter
       USE Coefficients_Parallelization_Parameters, &
           ONLY:Coefficient_BinaryUnitNumber,Coefficient_BlockSize

       USE MPI       

       IMPLICIT NONE

       integer :: MYID, DVR_size
       INTEGER :: unitnum

       COMPLEX*16, DIMENSION(Nconf) :: VIN
       COMPLEX*16, DIMENSION(DVR_size,Morb)  :: PSI             
       COMPLEX*16, DIMENSION(DVR_size)       :: V_TRAP           

       real*8 :: required_time

       LOGICAL, OPTIONAL :: Block
       LOGICAL :: pr_Block

       LOGICAL :: itsopen


       COMPLEX*16, Allocatable :: VIN_Block(:)

       INTEGER :: iter_i4, nstate, ierr
       integer*8 :: step,K,iter_i8
       real*8 :: tm,enrg,iter_r8

       real*8 :: RDummy ! Dummy for MPI I/O data sizes
       COMPLEX*16 :: CDummy ! Dummy for MPI I/O data sizes
       INTEGER*8 :: IDummy ! Dummy for MPI I/O data sizes
       INTEGER :: Orbital_BlockSize_Local,LocalDim,LocOffset

       integer(kind=MPI_OFFSET_KIND) displacement

       nstate=1

       IF (PRESENT(Block)) THEN
         pr_Block = Block
       ELSE
         pr_Block = .FALSE.
       ENDIF

       IF ((Present(Block).and.(Block.eqv..FALSE.)).or. &
           (Present(Block).eqv..FALSE.)) THEN
       !IF (pr_Block.eqv..FALSE.) THEN

          WRITE(1003,*) "################################"
          WRITE(1003,*) "#### READING Binary Data #######"
          WRITE(1003,*) "################################"
        
         step = 0
        
         DO WHILE (nstate .GT.0)

         select case (unitnum)

         case (778,802) !Orbitals

            IF (MPI_ORBS.eqv..FALSE.) THEN
                
                read(unitnum) tm,iter_i8,enrg,Rho1_Elements,&
                          Rho2_Elements,PSI,V_TRAP !VTRAP_EXT
             
               !if (unitnum .eq. 802) then
               !   write(6,*) "Reading from PSI0"
               !   write(6,*) "Read tm:", tm
               !   write(6,*) "Read iter_i8:", iter_i8
               !   write(6,*) "Read enrg:", enrg
               !   write(6,*) "Read Rho1:", Rho1_Elements
               !   write(6,*) "Read Rho2:", Rho2_Elements
               !   write(6,*) "Read V_TRAP:", V_TRAP
               !   write(6,*) "Read PSI:", PSI
               !elseif (unitnum .eq. 778) then
               !   write(6,*) "Reading from PSI_bin"
               !   write(6,*) "Read tm:", tm
               !   write(6,*) "Read iter_i8:", iter_i8
               !   write(6,*) "Read enrg:", enrg
               !   write(6,*) "Read Rho1:", Rho1_Elements
               !   write(6,*) "Read Rho2:", Rho2_Elements
               !   write(6,*) "Read V_TRAP:", V_TRAP
               !   write(6,*) "Read PSI:", PSI               
               !endif

                IF ((INT(DVR_Size,8).eq. NDVR_X*NDVR_Y*NDVR_Z).AND.&
                    (ALLOCATED(VTRAP_EXT).eqv..TRUE.)) THEN
                    VTRAP_EXT=V_TRAP
                ENDIF
              
                step=step+1                                            

            ELSEIF (MPI_ORBS.eqv..TRUE.) THEN

                IF (Allocated(LocalPsiDim_inter).eqv..TRUE.) THEN
                   Orbital_BlockSize_Local=Orbital_BlockSize_Inter
                   LocalDim=SUM(LocalPsiDim_Inter(:))
                   LocOffset=SUM(LocalPsiDim_Inter(1:MYID))

!                   Write(6,*) "MYID", MYID," Reads interp. block",&
!                         Orbital_BlockSize_Inter, &
!                        "and orbital Block", Orbital_BlockSize,&
!                        "and local offsets:", LocOffset
                ELSE
                   Orbital_BlockSize_Local=Orbital_BlockSize
                   LocalDim=SUM(LocalPsiDim(:))
                   LocOffset=SUM(LocalPsiDim(1:MYID))
                ENDIF

            
              displacement=step*Orbital_BlockSize_Local               
!c============== Advance to position of this dataset
              CALL MPI_File_seek(Orbital_BinaryUnitNumber,            & 
                                displacement,               &
                                MPI_SEEK_SET, IERR )                   
!c============= Read Time                                              
             call MPI_FILE_READ_ALL(Orbital_BinaryUnitNumber,         &
                                    tm, 1,                            &
                                    MPI_DOUBLE_PRECISION,             &
                                    MPI_STATUS_IGNORE, ierr)          
!             Write(6,*) "MYID",MYID,"READ time",tm
!c============= Advance                                                
             CALL MPI_File_seek(Orbital_BinaryUnitNumber,             &
                               displacement                           &
                                +SIZEOF(RDummy),                      &
                                MPI_SEEK_SET, IERR )                  
!c============= Read step number                                       
             call MPI_FILE_READ_ALL(Orbital_BinaryUnitNumber,         &
                                    iter_r8, 1,                        &
                                    MPI_DOUBLE_PRECISION,             &
                                    MPI_STATUS_IGNORE, ierr)          
             iter_i4=NINT(Iter_r8)
!             Write(6,*) "MYID",MYID,"READ iter",Iter_r8
!c============= Advance                                                
             CALL MPI_File_seek(Orbital_BinaryUnitNumber,             &
                               displacement                           &
                                +SIZEOF(RDummy)+SIZEOF(RDummy),       &
                                MPI_SEEK_SET, IERR )                  
!c============= Read energy                                            
             call MPI_FILE_READ_ALL(Orbital_BinaryUnitNumber,         &
                                    enrg, 1,                          &
                                    MPI_DOUBLE_PRECISION,             &
                                    MPI_STATUS_IGNORE, ierr)           
!             Write(6,*) "MYID",MYID,"READ E",enrg
                                                                      
!c============= Advance                                                
             CALL MPI_File_seek(Orbital_BinaryUnitNumber,             &
                               displacement                           &
                                +2*SIZEOF(RDummy)+SIZEOF(RDummy),     &
                                MPI_SEEK_SET, IERR )                   
                                                                      
!c============= Read rho^1 elements                                    
             call MPI_FILE_READ_ALL(Orbital_BinaryUnitNumber,         &
                                    Rho1_Elements, RDim,              &
                                    MPI_Double_Complex,               &
                                    MPI_STATUS_IGNORE, ierr)          
!             Write(6,*) "MYID",MYID,"READ rho1",Rho1_Elements(1)
!c============= Advance                                                
             CALL MPI_File_seek(Orbital_BinaryUnitNumber,             &
                               displacement                           &
                                +2*SIZEOF(RDummy)+SIZEOF(RDummy)      &
                                +Rdim*SIZEOF(CDummy),                 &
                                MPI_SEEK_SET, IERR )                  
!c============= Read rho^2 elements                                    
             call MPI_FILE_READ_ALL(Orbital_BinaryUnitNumber,         &
                                    Rho2_Elements, RDim1,             &
                                    MPI_Double_Complex,               &
                                    MPI_STATUS_IGNORE, ierr)
!             Write(6,*) "MYID",MYID,"READ rho2",Rho2_Elements(1)
!c============== Advance processes to displaced positions of the respective
!c============== domain of PSI they are working on

              IF (MYID.ne.0) THEN

                 DISPLACEMENT=DISPLACEMENT                        &
                             +LocOffset                           & !!! BUG?????
                             *SIZEOF(CDummy)                      &
                             +2*SIZEOF(RDummy)+SIZEOF(RDummy)     &
                             +(Rdim+Rdim1)*SIZEOF(CDummy)             

              ELSE

                 DISPLACEMENT=DISPLACEMENT                        &
                             +2*SIZEOF(RDummy)+SIZEOF(RDummy)     &
                             +(Rdim+Rdim1)*SIZEOF(CDummy)             

              ENDIF                                  



!             Write(6,*) "MYID", MYID, "to read orbs", DISPLACEMENT

             CALL MPI_File_seek(Orbital_BinaryUnitNumber,         &
                                DISPLACEMENT,                    & 
                                MPI_SEEK_SET, IERR )

!c============== Read Orbitals
              DO K=1,Morb

                 call MPI_FILE_READ_ALL(Orbital_BinaryUnitNumber,    &
                                       PSI(:,K),                     &
                                       INT(DVR_SIZE,4),   &
                                       MPI_Double_Complex,           &
                                       MPI_STATUS_IGNORE, ierr)       

!                 CALL MPI_BARRIER(MPI_COMM_WORLD,ierr)
!                 Write(6,*) "MYID", MYID, "READ PSI",K, PSI(32,K), &
!                             "AT", DISPLACEMENT
!       CALL MPI_BARRIER(MPI_COMM_WORLD,ierr)
                                                                        
                 DISPLACEMENT=DISPLACEMENT                           &
                             +LocalDim*SIZEOF(CDUMMY)    
                                                                       
!                Write(6,*) "MYID", MYID, "sets displ 2", DISPLACEMENT,& 
!                           "DVR_SIZE", DVR_SIZE
!c============= Advance processes to displaced positions of the respect=ive
!c============= domain of PSI they are working on                       
                CALL MPI_File_seek(Orbital_BinaryUnitNumber,           &
                                   DISPLACEMENT, & 
                                  MPI_SEEK_SET, IERR )                 


            END DO                                                      
!   
!       CALL MPI_BARRIER(MPI_COMM_WORLD,ierr)
!           Write(6,*) "MYID", MYID,"HAS NORMS",& 
!                       SUM(PSI(:,1)*dconjg(PSI(:,1))),&
!                       SUM(PSI(:,2)*dconjg(PSI(:,2)))
!       CALL MPI_BARRIER(MPI_COMM_WORLD,ierr)

           call MPI_FILE_READ_ALL(Orbital_BinaryUnitNumber,           &
                                  V_Trap,& !_EXT,                          &
                                  INT(DVR_SIZE,4),         &
                                  MPI_Double_Complex,                 &
                                  MPI_STATUS_IGNORE, ierr)             
                                                                        

!       CALL MPI_BARRIER(MPI_COMM_WORLD,ierr)
!           Write(6,*) "MYID", MYID,"POT DISPL", DISPLACEMENT,&
!                      "found VTRAP_EXT", V_TRAP(1), SIZE(V_TRAP) 
!       CALL MPI_BARRIER(MPI_COMM_WORLD,ierr)
             step=step+1                                                
                                                                        
           ENDIF                                                        
                                                                        
        case (777,801) ! Coefficients                                       
           IF (CI_V3.eqv..TRUE.) THEN                             
                                                                       
!c============= Advance to position of this dataset                     
!             CALL MPI_File_seek(Coefficient_BinaryUnitNumber,          &
!                                step*Coefficient_BlockSize,            &
!                                MPI_SEEK_SET, IERR )                    
                                                                       
!c============= Read Time                                               
             call MPI_FILE_READ_ALL(Coefficient_BinaryUnitNumber,      &
                                    tm, 1,                             &
                                    MPI_REAL8,                         &
                                    MPI_STATUS_IGNORE, ierr)           
!c============= Advance                                                 
!             CALL MPI_File_seek(Coefficient_BinaryUnitNumber,          &
!                              step*Coefficient_BlockSize               &
!                                +SIZEOF(RDummy),                       &
!                                MPI_SEEK_SET, IERR )
!c=============   Read Statement for coefficients is still missing
!c=============   Read Statement for coefficients is still missing
!c=============   Read Statement for coefficients is still missing
!c=============   Read Statement for coefficients is still missing
               ELSE
                 IF (MYID.eq.0) THEN 
                    inquire(unit=unitnum, opened=itsopen) 
!                    write(6,*) "IN READ_BINARY",SIZE(VIN),itsopen
                    read(unitnum)  tm,       & 
                               iter_i8,  &
                               enrg,     &
                               VIN       

                    iter_i4=INT(iter_i8,4)
                            
                 ELSE
                    tm=required_time
                 ENDIF 
               END IF
       
            end select 
        
            IF(ABS(tm-required_time).le.0.00000001) nstate=-2
        
            IF (nstate.le.0) THEN
               iter_i4=INT(iter_i8,4)
               write(1003,*) & 
              "Guess from binary: reading was stopped at",tm,iter_i4,&
                         enrg
            ENDIF

          END DO 
          
          required_time=tm
          energy=enrg
          WRITE(1003,*) "################################"
          WRITE(1003,*) "#####READ Binary Data###########"
          WRITE(1003,*) "################################"

       ELSE ! Restart from a specific state of a Block relaxation
          WRITE(1003,*) "#############################################"
          WRITE(1003,*) "#####READING from Binary Block Data #########"
          WRITE(1003,*) "#############################################"
        
         
          IF (Allocated(VIN_Block).eqv..FALSE.) THEN
             Allocate(VIN_Block(NConf*BlockSize))
          ENDIF       

          IF (Allocated(Rho1_Elements_Block).eqv..FALSE.) THEN
             Allocate(Rho1_Elements_Block(Rdim,BlockSize))
          ENDIF       

          IF (Allocated(Rho2_Elements_Block).eqv..FALSE.) THEN
             Allocate(Rho2_Elements_Block(Rdim1,BlockSize))
          ENDIF       

         DO 21 WHILE (nstate .GT.0)
           select case (unitnum)
           case (778,802)
              read(unitnum) tm,iter_i4,enrg ,Rho1_Elements_Block,&
                        Rho2_Elements_Block,PSI,VTRAP_EXT
              IF(ABS(tm-required_time).le.0.0000001) then
                  nstate=-2
                  Rho1_Elements=Rho1_Elements_Block(:,Restart_State)
                  Rho2_Elements=Rho2_Elements_Block(:,Restart_State)
              ENDIF

           case (777,801)
              read(unitnum)  tm, iter_i8, enrg ,VIN_Block
              IF(ABS(tm-required_time).le.0.0000001) then
                  nstate=-2
                  VIN=VIN_Block((Restart_State-1)*NCONF+1: &
                                    Restart_State*NCONF)
              ENDIF
           end select 

           DeAllocate(VIN_Block)
           DeAllocate(Rho1_Elements_Block)
           DeAllocate(Rho2_Elements_Block)

           IF (nstate.le.0) write(1003,*) &
                "Guess from binary: reading was stopped at",tm,iter_i4,&
                enrg
  21     ENDDO
       
         required_time=tm
         energy=enrg

         WRITE(1003,*) "#############################################"
         WRITE(1003,*) "#####READING from Binary Block Data #########"
         WRITE(1003,*) "#############################################"

       ENDIF

!       CALL MPI_BARRIER(MPI_COMM_WORLD,ierr)
!       IF (MYID.eq.0) THEN
!         Write(6,*) "PSI 1 ::: ", PSI(:,1)
!         Write(6,*) "PSI 2 ::: ", PSI(:,2)
!       ENDIF
!       CALL MPI_BARRIER(MPI_COMM_WORLD,ierr)
!       IF (MYID.eq.1) THEN
!         Write(6,*) "PSI 1 ::: ", PSI(:,1)
!         Write(6,*) "PSI 2 ::: ", PSI(:,2)
!       ENDIF
!       CALL MPI_BARRIER(MPI_COMM_WORLD,ierr)
   

!       CALL Write_Orbs_MPI(tm,PSI,V_Trap)
!       STOP
       end SUBROUTINE READ_BINARY

       subroutine Read_bin_fidelity(unitnum,required_time,VIN,PSI,MYID,&
                                    DVR_size,Block)

       USE Global_Parameters
       USE Matrix_Elements
       USE Orbital_Allocatables, ONLY: VTRAP_EXT
       USE Coefficients_Parameters
       USE Orbital_Parallelization_Parameters
       USE DVR_Parameters
       USE Orbital_Parallelization_Parameters, &
           ONLY:Orbital_BinaryUnitNumber,Orbital_BlockSize,&
                LocalPsiDim,LocalPsiDim_Inter,Orbital_BlockSize_Inter
       USE Coefficients_Parallelization_Parameters, &
           ONLY:Coefficient_BinaryUnitNumber,Coefficient_BlockSize

       integer :: MYID, DVR_size
       COMPLEX*16, DIMENSION(NConf) :: VIN

       COMPLEX*16, DIMENSION(DVR_size,MOrb) :: PSI
       COMPLEX*16, DIMENSION(DVR_size)      :: V_TRAP

       integer :: unitnum, iter, a
       real*8 :: required_time

       LOGICAL,Optional :: Block
 
       COMPLEX*16, Allocatable :: VIN_Block(:)

       INTEGER :: iter_i4, nstate, ierr
       integer*8 :: step,K,iter_i8
       real*8 :: tm,enrg,iter_r8

       real*8 :: RDummy ! Dummy for MPI I/O data sizes
       COMPLEX*16 :: CDummy ! Dummy for MPI I/O data sizes
       INTEGER*8 :: IDummy ! Dummy for MPI I/O data sizes
       INTEGER :: Orbital_BlockSize_Local,LocalDim,LocOffset

       integer(kind=MPI_OFFSET_KIND) displacement

       nstate = 1

       IF ((Present(Block).and.(Block.eqv..FALSE.)).or. &
           (Present(Block).eqv..FALSE.)) THEN

         WRITE(1003,*) "###############################################"
         WRITE(1003,*) "##### READ Binary Data For Fidelity ###########"
         WRITE(1003,*) "###############################################"

         step = 1

         DO WHILE (nstate .gt. 0)

           select case (unitnum)
  
           case (802) ! orbitals 

             IF (MPI_ORBS.eqv..FALSE.) THEN  

               read(802) tm,iter_i8,enrg,Rho1_Elements,&
                         Rho2_Elements,PSI,V_TRAP

               !if (unitnum .eq. 802) then
               !            write(6,*) "Read tm:", tm
               !            write(6,*) "Read iter_i8:", iter_i8
               !            write(6,*) "Read enrg:", enrg
               !            write(6,*) "Read Rho1:", Rho1_Elements
               !            write(6,*) "Read Rho2:", Rho2_Elements
               !            write(6,*) "Read V_TRAP:", V_TRAP
               !            write(6,*) "Read PSI:", PSI
               !endif

             ELSEIF (MPI_ORBS.eqv..TRUE.) THEN
 
               IF (Allocated(LocalPsiDim_inter).eqv..TRUE.) THEN
                  Orbital_BlockSize_Local=Orbital_BlockSize_Inter
                  LocalDim=SUM(LocalPsiDim_Inter(:))
                  LocOffset=SUM(LocalPsiDim_Inter(1:MYID))
               ELSE
                 Orbital_BlockSize_Local=Orbital_BlockSize
                 LocalDim=SUM(LocalPsiDim(:))
                 LocOffset=SUM(LocalPsiDim(1:MYID))
               ENDIF

               displacement=step*Orbital_BlockSize_Local
!c============== Advance to position of this dataset
               CALL MPI_File_seek(Orbital_BinaryUnitNumber,            &
                                  displacement,                        &
                                  MPI_SEEK_SET, IERR )
!c============= Read Time                                              
               call MPI_FILE_READ_ALL(Orbital_BinaryUnitNumber,        &
                                      tm, 1,                           &
                                      MPI_DOUBLE_PRECISION,            &
                                      MPI_STATUS_IGNORE, ierr)
!c============= Advance                                                
               CALL MPI_File_seek(Orbital_BinaryUnitNumber,            &
                                  displacement                         &
                                  +SIZEOF(RDummy),                     &
                                  MPI_SEEK_SET, IERR )
!c============= Read step number                                       
               call MPI_FILE_READ_ALL(Orbital_BinaryUnitNumber,        &
                                      iter_r8, 1,                      &
                                      MPI_DOUBLE_PRECISION,            &
                                      MPI_STATUS_IGNORE, ierr)
               iter_i4=NINT(Iter_r8)
!c============= Advance                                                
               CALL MPI_File_seek(Orbital_BinaryUnitNumber,            &
                                  displacement                         &
                                  +SIZEOF(RDummy)+SIZEOF(RDummy),      &
                                  MPI_SEEK_SET, IERR )
!c============= Read energy                                            
               call MPI_FILE_READ_ALL(Orbital_BinaryUnitNumber,        &
                                      enrg, 1,                         &
                                      MPI_DOUBLE_PRECISION,            &
                                      MPI_STATUS_IGNORE, ierr)
 
!c============= Advance                                                
               CALL MPI_File_seek(Orbital_BinaryUnitNumber,            &
                                  displacement                         &
                                  +2*SIZEOF(RDummy)+SIZEOF(RDummy),    &
                                  MPI_SEEK_SET, IERR )
 
!c============= Read rho^1 elements                                    
               call MPI_FILE_READ_ALL(Orbital_BinaryUnitNumber,        &
                                      Rho1_Elements, RDim,             &
                                      MPI_Double_Complex,              &
                                      MPI_STATUS_IGNORE, ierr)
!c============= Advance                   
               CALL MPI_File_seek(Orbital_BinaryUnitNumber,            &
                                  displacement                         &
                                  +2*SIZEOF(RDummy)+SIZEOF(RDummy)     &
                                  +Rdim*SIZEOF(CDummy),                &
                                  MPI_SEEK_SET, IERR )
!c============= Read rho^2 elements                                    
               call MPI_FILE_READ_ALL(Orbital_BinaryUnitNumber,        &
                                      Rho2_Elements, RDim1,            &
                                      MPI_Double_Complex,              &
                                      MPI_STATUS_IGNORE, ierr)
!c============== Advance processes to displaced positions of the respective
!c============== domain of PSI they are working on
 
               IF (MYID.ne.0) THEN
 
                 DISPLACEMENT=DISPLACEMENT                        &
                             +LocOffset                           & !!! BUG?????
                             *SIZEOF(CDummy)                      &
                             +2*SIZEOF(RDummy)+SIZEOF(RDummy)     &
                             +(Rdim+Rdim1)*SIZEOF(CDummy)
 
               ELSE
 
                 DISPLACEMENT=DISPLACEMENT                        &
                             +2*SIZEOF(RDummy)+SIZEOF(RDummy)     &
                             +(Rdim+Rdim1)*SIZEOF(CDummy)
 
               ENDIF
 
 
               CALL MPI_File_seek(Orbital_BinaryUnitNumber,        &
                                  DISPLACEMENT,                    &
                                  MPI_SEEK_SET, IERR )
           

!c============== Read Orbitals
               DO K=1,Morb
 
                 call MPI_FILE_READ_ALL(Orbital_BinaryUnitNumber,    &
                                        PSI(:,K),                    &
                                        INT(DVR_SIZE,4),             &
                                        MPI_Double_Complex,          &
                                        MPI_STATUS_IGNORE, ierr)
 
                 DISPLACEMENT=DISPLACEMENT                           &
                             +LocalDim*SIZEOF(CDUMMY)
 
!c============= Advance processes to displaced positions of the respect=ive
!c============= domain of PSI they are working on                       
                 CALL MPI_File_seek(Orbital_BinaryUnitNumber,         &
                                    DISPLACEMENT,                     &
                                    MPI_SEEK_SET, IERR )
 
 
               END DO

               call MPI_FILE_READ_ALL(Orbital_BinaryUnitNumber,       &
                                      V_Trap,                         &
                                      INT(DVR_SIZE,4),                &
                                      MPI_Double_Complex,             &
                                      MPI_STATUS_IGNORE, ierr)
 
               step=step+1
 
             ENDIF


           case (801)  ! Coefficients

             IF (CI_V3.eqv..TRUE.) THEN
 
!c============= Read Time                                               
               call MPI_FILE_READ_ALL(Coefficient_BinaryUnitNumber,    &
                                      tm, 1,                           &
                                      MPI_REAL8,                       &
                                      MPI_STATUS_IGNORE, ierr)
             ELSE

               if(MYID .eq. 0) then 
                 read(801)  tm, iter_i8, enrg, VIN
!                 write(6,*) '--------- ', a, VIN(1) 
!                 a = a + 1
                 iter_i4=INT(iter_i8,4)
               else
                 tm=required_time
               endif
             ENDIF  

           end select

           IF(ABS(tm-required_time).le.1.d-8) nstate=-2

           IF (nstate.le.0) THEN
             iter_i4=INT(iter_i8,4)
             write(1003,*) &
             "Guess from binary: reading was stopped at",tm,iter_i4,enrg
           ENDIF

         enddo  

         required_time = tm
         energy        = enrg

         WRITE(1003,*) "###############################################"
         WRITE(1003,*) "##### READ Binary Data For Fidelity ###########"
         WRITE(1003,*) "###############################################"
       
       else ! Restart from a specific state of a Block relaxation

         WRITE(1003,*) "#############################################"
         WRITE(1003,*) "#####READING from Binary Block Data #########"
         WRITE(1003,*) "#############################################" 

         IF (Allocated(VIN_Block).eqv..FALSE.) THEN
           Allocate(VIN_Block(NConf*BlockSize))
         ENDIF
 
         IF (Allocated(Rho1_Elements_Block).eqv..FALSE.) THEN
           Allocate(Rho1_Elements_Block(Rdim,BlockSize))
         ENDIF
 
         IF (Allocated(Rho2_Elements_Block).eqv..FALSE.) THEN
           Allocate(Rho2_Elements_Block(Rdim1,BlockSize))
         ENDIF

         DO 21 WHILE (nstate .GT.0)
           select case (unitnum)
           case (802)
             read(802) tm,iter_i4,enrg ,Rho1_Elements_Block,&
                       Rho2_Elements_Block,PSI,VTRAP_EXT
             IF(ABS(tm-required_time).le.1d-8) then
               nstate=-2
               Rho1_Elements=Rho1_Elements_Block(:,Restart_State)
               Rho2_Elements=Rho2_Elements_Block(:,Restart_State)
             ENDIF
     
           case (801)
             read(801) tm, iter_i8, enrg ,VIN_Block
             IF(ABS(tm-required_time).le.1d-8) then
               nstate=-2
               VIN=VIN_Block((Restart_State-1)*NCONF+1: &
                             Restart_State*NCONF)
             ENDIF
           end select
     
           DeAllocate(VIN_Block)
           DeAllocate(Rho1_Elements_Block)
           DeAllocate(Rho2_Elements_Block)


         IF (nstate.le.0) write(1003,*) &
             "Guess from binary: reading was stopped at",tm,iter,enrg
  21     ENDDO


         required_time = tm
         energy        = enrg
 
         WRITE(1003,*) "#############################################"
         WRITE(1003,*) "#####READING from Binary Block Data #########"
         WRITE(1003,*) "#############################################"
 
       ENDIF

       end subroutine Read_bin_fidelity       

!> Read the wavefunction from the binary files if GUESS=BINR was set.
       subroutine  Binary_guess(MYID)

       USE   Global_Parameters,ONLY:Binary_Start_Time,              &
                                    Previous_Coefficients_Integrator,&
                                    Coefficients_Integrator,         &
                                    BlockSize,Previous_Blocksize, &
                                    GUESS,Orbital_Integrator,MPI_ORBS

       USE   Coefficients_Parameters
       USE   CI_Production_Parameters
       USE   Interaction_Parameters
       USE   DVR_Parameters
       USE   Matrix_Elements
       USE   MPI
       USE   MPI_FFT_DATA

       implicit NONE

       integer :: MYID
       real*8   time1,time2,Init_Start_Point_t
       INTEGER :: I_Error
       INTEGER*8 :: NDVR_X_input,NDVR_Y_input,NDVR_Z_input


       IF(GUESS=='BINR') THEN 

         NDVR_X_input = NDVR_X 
         NDVR_Y_input = NDVR_Y 
         NDVR_Z_input = NDVR_Z 

         Init_Start_Point_t=Binary_Start_Time

         CALL Open_binary(MYID) !Needed for restarts

         CALL Read_Header(779,'full')

! store DVR parameters from input in variables to potentially do interpolation 
         NDVR_X_inter = NDVR_X 
         NDVR_Y_inter = NDVR_Y 
         NDVR_Z_inter = NDVR_Z 
  
         if ( (NDVR_X_input .ne. NDVR_X_inter) .or. & 
              (NDVR_Y_input .ne. NDVR_Y_inter) .or. & 
              (NDVR_Z_input .ne. NDVR_Z_inter) ) then 
           IF (MPI_ORBS.eqv..TRUE.) THEN
             CALL MPI_FFT_Data_Interpol_Allocation(INT(NDVR_X_inter,8),&
                                                   INT(NDVR_Y_inter,8),&
                                                   INT(NDVR_Z_inter,8))
           ENDIF
         endif
 

         Xi_interpol  = X_initial
         Xf_interpol  = X_final
         Yi_interpol  = Y_initial
         Yf_interpol  = Y_final
         Zi_interpol  = Z_initial
         Zf_interpol  = Z_final

         Previous_Coefficients_Integrator = Coefficients_Integrator
         Previous_BlockSize               = BlockSize

          Write(6,*) "Starting time given, Binary_Guess",&
                       Init_Start_Point_t
!!!  If the starting time is still at the negative default, 
!!!  start from biggest time of former binary
         IF (abs(Binary_Start_Time+666.d0).gt.1.d-10) then
            time1=Init_Start_Point_t

            call Analyse_BINARY(778, time1, MYID)

            time2=Init_Start_Point_t

            call Analyse_BINARY(777, time2, MYID)

            CALL  Close_binary(MYID) 
         ELSEIF (abs(Binary_Start_Time+666.d0).le.1.d-10) then
            time1=1.d20
            
            call Analyse_BINARY(778, time1, MYID)

            time2=1.d20
            call Analyse_BINARY(777, time2, MYID)

            Binary_Start_Time=MIN(time1,time2)
            Init_Start_Point_t=Binary_Start_Time
            Write(6,*) "No starting time given; I adjusted it to",&
                        Binary_Start_Time
            CALL  Close_binary(MYID) 
         ENDIF
        WRITE(1003,*) "################################"
        WRITE(1003,*) "#####READING Binaries###########"
        WRITE(1003,*) "################################"
        write(1003,*)"MCTDHX.inp: Binary_Start_Time=",Init_Start_Point_t
        write(1003,*)"OLD_MCTDHB Binary_Start_Time=",Binary_Start_Time
        write(1003,*)"PSI_bin: Binary_Start_Time=",time1
        write(1003,*)"CIc_bin: Binary_Start_Time=",time2
 
        if (abs(time1-time2).gt.1.d-8) then
              Write(1003,*) "Time slices for coeffitients and orbitals &
                            unequal -- Change start time!! says", MYID
              STOP
        endif

        call Read_Input

        Binary_Start_Time=Init_Start_Point_t

        write(1003,*) "Binary_Start_Time=",Binary_Start_Time 

        IF (MYID.eq.0) THEN
         if(abs(Init_Start_Point_t-time2).ge.1.0d-8) then
           write(6,*)"Change the Start Time=",time2,"in MCTDHX.inp"
           stop
         endif
         if(abs(Init_Start_Point_t-time1).ge.1.0d-8) then
           write(6,*)"Change the Start Time=",time1,"in MCTDHX.inp"
           stop
         endif
        endif
        WRITE(1003,*) "################################"
        WRITE(1003,*) "########READ Binaries###########"
        WRITE(1003,*) "################################"

       ENDIF


       end SUBROUTINE Binary_guess


!> !!!!READING IN DATA FROM ASCII FILES!!!!
!> !!!!READING IN DATA FROM ASCII FILES!!!!
!> !!!!READING IN DATA FROM ASCII FILES!!!!
!> !!!!READING IN DATA FROM ASCII FILES!!!!




!> Read Orbitals from an ASCII file.
       SUBROUTINE Guess_Read_ORB(PSI)

       USE   Global_Parameters
       USE   DVR_Parameters
       USE   Function_Library

       IMPLICIT NONE

       COMPLEX*16, DIMENSION(NDVR_X*NDVR_Y*NDVR_Z,Morb)  :: PSI
       REAL*8, DIMENSION(1000)  :: V
       INTEGER :: ndim,I
       CHARACTER*18 lname
       INTEGER*8 :: ierr
       INTEGER :: jj,kk
       INTEGER*8 :: J,K,ind,Norb
       PSI = ZERO
       ndim = NDVR_X*NDVR_Y*NDVR_Z
       Norb = Morb
       lname = Restart_Orbital_FileName

      WRITE(*,*)"Orbitals Guess is taken from file: ",lname

      !> If the file is TNT-generated, read the data accordingly to
      !> get the orbitals:
      IF (TNT_data.eqv. .TRUE.) THEN
         WRITE(*,*) "#######################################################"
         WRITE(*,*) "##### READING ORBITAL GUESS FROM TNT CALCULATION ######"
         WRITE(*,*) "#######################################################"
      ELSEIF (TNT_data.eqv. .FALSE.) THEN
         WRITE(*,*) "##########################################################"
         WRITE(*,*) "##### READING ORBITAL GUESS FROM MCTDHX CALCULATION ######"
         WRITE(*,*) "##########################################################"
      ENDIF

      !> unit=11 is the unit associated with the file being opened and read.
      !> The error message associated with the error flag 101 will be
      !> raised if the file is missing/corrupted.
      open(unit=11, file=lname, Err=101, form='formatted')
      !> For MCTDH-X generated data: Read in the first two lines 
      !> (which are discarded; they are an empty line at the beginning
      !> of the file and the line corresponding to the column index).
      !> If the file ends already after this, there must be a problem and
      !> therefore we jump to the error message (end=101).
      IF (TNT_data.eqv. .FALSE.) THEN
       read(11,*,err=101,end=101)
       read(11,*,err=101,end=101)
      ENDIF
      !> ind is the index of the vectorized orbitals saved in PSI.
       ind=1

      !> Go through all the lines of the file. The file is indexed such that
      !> the x, y, and z-coordinates are saved in the first, second, and third column
      !> respectively, so we need three nested loops to go through all the datapoints. 
      DO K=1,NDVR_Z
         DO J=1,NDVR_Y
            DO I=1,NDVR_X
               !> Read from the file all the entries in the given line.
               !> There are in total 9+4*Norb entries, eg. for 4 orbitals -> 25 filled columns.
               !> The entries are saved in the array V, which has dimension 1000
               !> (chosen to be large enough, could accommodate 246 orbitals).
               IF (TNT_data.eqv. .FALSE.) THEN
                  READ(11,*,end=101,err=101) (V(jj),jj=1,9+4*Norb)


               ELSEIF (TNT_data.eqv. .TRUE.) THEN
                  !WRITE(*,*) "IM HERE"
                  READ(11,*,end=101,err=101) (V(jj),jj=1,3+Norb)
                  !> test:
                  !READ(11,*,end=101,err=101) (V(jj), jj=1,2)
                  !WRITE(*,*) "Z=",K,"Y=",J,"X=",I
               ENDIF
               !> Now extract only the entries corresponding to the working orbitals.
               !> For MCTDH-X generated data they are in the 10th column onward, 
               !> real parts in the even columns 10, 12, 14 etc. and
               !> imaginary part in the odd columns 11, 13, 15 etc). 
               !> For TNT-generated data, the "orbitals" (Wannier functions) are in the 
               !> 4th column onward (only real part).
               !> 
               !> Save the orbitals in the vectorized array PSI:
               !> PSI contains the vectorized coordinates in the first dimension 
               !> (x1,y1,z1), (x2,y1,z1), ..., (xN, y1, z1), (x1,y2,z1), (x2, y2, z1), ...)
               !> and the value of each working orbitals (Real and imaginary part) in the 
               !> second dimension.
               IF (TNT_data.eqv. .FALSE.) THEN
                  DO kk=1,Norb
                   PSI(ind,kk)=ZONER*V(10+2*(kk-1))+ &
                               ZONEI*V(10+2*(kk-1)+1) !Working orbitals
                  ENDDO
               ELSEIF (TNT_data.eqv. .TRUE.) THEN
                  DO kk=1, Norb
                     PSI(ind,kk)=ZONER*V(3+kk) + ZONEI*0.0000000000000000
                  ENDDO
               ENDIF


                ind=ind+1
            ENDDO
          ENDDO
       ENDDO

      !> The file has been read to completion, so close it.
      CLOSE(11)


      !> Call the routine to orthonormalize the orbitals:
      !> Can this be done for TNT-generated data too as is?
      CALL  schmidtortho(PSI,NDVR_X*NDVR_Y*NDVR_Z,Norb,ierr)

      WRITE(*,*)  &
           "PSI-file has been successfully Schmidt-Orthonormalized"

      WRITE(*,*) "################################"
      WRITE(*,*) "########READ ORBITAL GUESS######"
      WRITE(*,*) "################################"
      RETURN

      !> Error message in case the file is damaged or empty:
101   WRITE(6,*)"The input PSI DATA file is empty or corrupted"
      WRITE(6,*)"Change Guess"
      STOP 

       END SUBROUTINE Guess_Read_Orb




!> Read Coefficients from an ASCII file.
      SUBROUTINE Guess_Read_CI(VIN)

      USE   Global_Parameters
      USE   Matrix_Elements
      USE   Coefficients_Parameters
      USE   Interaction_Parameters
      USE   DVR_Parameters
      USE   Addresses
      USE   Function_Library

      IMPLICIT NONE

      COMPLEX*16, DIMENSION(Nconf) :: VIN
      REAL*8, DIMENSION(1000)  :: V
      integer :: i,jj,Ncnf,ind,n,m,ind_new
      integer :: CI_READ
      character*18 lname
      integer, DIMENSION(1000)   :: Ivec
      integer, DIMENSION(1000)   :: Nvec
      VIN=Zero
      lname = Restart_Coefficients_FileName
      
      !> If the file is TNT-generated, read the data accordingly to
      !> get the orbitals:
      IF (TNT_data.eqv. .TRUE.) THEN
         WRITE(*,*) "##################################################"
         WRITE(*,*) "##### READING CI GUESS FROM TNT CALCULATION ######"
         WRITE(*,*) "##################################################"
      ELSEIF (TNT_data.eqv. .FALSE.) THEN
         WRITE(*,*) "#####################################################"
         WRITE(*,*) "##### READING CI GUESS FROM MCTDHX CALCULATION ######"
         WRITE(*,*) "#####################################################"
      ENDIF

      WRITE(*,*)"CI coeff Guess is taken from file: ",lname
        CI_READ=0 ! Default  Reading N=Npar M=Morb

               
        CIRD  : SELECT CASE (CI_READ)
        CASE (0)

         !> The coefficients exist only for M>1:
          IF (Morb.gt.1) THEN
              N=Npar
              M=Morb
            !> Defining the number of configurations (N + M - 1 choose N):
            !> These are also the number of rows contained in the file. Eg
            !> for N=2, M=4 -> Ncnf = 10.
              Ncnf=NINT(BinomialCoefficient(N+M-1,N)) 

            !> Writing a message in the standard output:
            write(*,*)"Data for identical N=",N," Morb=",M

            !> Opening the <time>coef.dat file. For syntax see routine above.
              open(unit=11,file=lname,Err=101,form='formatted')

            !> For MCTDH-X generated data: Reading in the first two lines of the 
            !> file (empty line and column index), which are discarded.
            IF (TNT_data.eqv. .FALSE.) THEN
              read(11,*,err=101,end=101)
              read(11,*,err=101,end=101)
            ENDIF
            !> For TNT generated data: read the whole file directly.

            !> Going through all the rows corresponding to the number of configurations:
            DO I=1,Ncnf
               !> MCTDH-X generated data: Reading the first three elements in each row in the array V.
               !> These are the index of the configuration, the real part of the coefficient,
               !> and its imaginary part.
               IF (TNT_data .eqv. .FALSE.) THEN
                 read(11,*,end=10,err=101) (V(jj),jj=1,3)
                  !> Saving the coefficient in the array VIN:
                 VIN(NINT(V(1)))=ZONER*V(2)+ZONEI*V(3)
               ELSEIF (TNT_data .eqv. .TRUE.) THEN
               !> TNT-generated data: Reading the second and third elements 
               !> (real/imaginary part of the coefficient), but also the configuration
               !> (element 4 to 4+Morb) from which we will construct the index.

                  read(11,*,end=10,err=101) (V(jj),jj=1,3+Morb)
                  !> Get the configuration and saving it into an array Nvec:
                  DO jj=4,3+Morb
                     Nvec(jj-3) = V(jj)
                     Write(*,*) 'Nvec of jj-3=', jj-3
                     Write(*,*) Nvec(jj-3)
                  ENDDO

                  !WRITE(*,*) Nvec

                  !> Converting the configuration array Nvec into the correct index:
                  call Get_IndexFromConfiguration(Npar,Morb,Nvec,Ind_new)
                  !> Saving the coefficient in the correct indexed position in VIN:
                  WRITE(*,*) "Ind_new", Ind_new
                  !> Make sure that all indices are accounted for!

                  VIN(Ind_new)=ZONER*V(2)+ZONEI*V(3)
                  !VIN(NINT(V(1)))=ZONER*V(2)+ZONEI*V(3)


               ENDIF



            ENDDO
         !> If M=1, there is only one configuration and VIN only contains a trivial element equal to one.
          ELSEIF (Morb.eq.1) THEN
              VIN(1)=dcmplx(1.d0,0.d0)
          ENDIF
         !> Close the file:
10       close(11)

      !> Not sure what this second case does... 
      !> Apparently taking the coefficients from a different system?
                  CASE (2)

         !> The number of orbitals is taken to be one less than what given(?)
              N=Npar
              M=Morb-1
         IF(N.GT.Npar) STOP "READ CI: Npar wrong"
         IF(M.GT.Morb) STOP "READ CI: Morb wrong"

      write(1003,*)" Data taken from DIFFERENT system N=",N," M=",M
      open(unit=11,file=lname,Err=101,form='formatted')

         !> Reading in the first two lines of the file (empty line and column index), 
         !> which are discarded.
      read(11,*,end=101,err=101)
      read(11,*,end=101,err=101)
         !> Defining the number of configurations (N + M - 1 choose N):
         Ncnf=NINT(BinomialCoefficient(Npar+Morb-1,Npar)) 

         DO I=1,Ncnf
            !> Reading the first three elements in each row in the array V
            !> These are the index of the configuration, the real part of the coefficient,
            !> and its imaginary part.
         read(11,*,end=11,err=101) (V(jj),jj=1,3)
            !> Define the index of the configuration as the same configuration index from the file:
         ind=NINT(V(1))
         Ivec=0
         Nvec=0

         !> Constructing the configuration from the index via subroutine.
         !> E.g a configuration for N=2, M=4 could be:
         !> (1,0,0,1), meaning one particle in the first orbital, one in the fourth.
         call Get_ConfigurationFromIndex_OLD(Ind,N,M,Nvec)
         !> Get the index from the constructed configuration.
         call Get_IndexFromConfiguration(Npar,Morb,Nvec,Ind_new)
         VIN(Ind_new)=ZONER*V(2)+ZONEI*V(3)
         ENDDO
         !> Close the file:
11      close(11)

      END SELECT CIRD
        WRITE(*,*) "################################"
        WRITE(*,*) "##########READ CI GUESS#########"
        WRITE(*,*) "################################"
          return
101    write(6,*)"The input CI DATA file is empty or corrupted"
       write(6,*)"Change Guess"
        stop
      END SUBROUTINE Guess_Read_CI

      !> @ingroup inout
      !> @brief Read the input file MCTDHX.inp .
      !> This subroutine uses the use Module_Global_Parameters.f90 module to globally store all of the
      !> variables that are to be read. Since the variables already exist with certain values 
      !> in Module_Global_Parameters.f90, the first step is setting defaults.
      !> The MCTDHX.inp file has the form of Fortran namelists. As such they are read in, e.g.
      !> via
      !>
      !>      READ(8,NML=System_Parameters)
      !>
      !> The routine then checks for a number of incompatibilities between input parameters and prints
      !> warning signals.
      !> A Python wrapper for Input read-in is in development.
        subroutine Read_Input

        use Global_Parameters
        use Coefficients_Parameters
        use Orbital_Parallelization_Parameters
        use Coefficients_Parallelization_Parameters
        use Interaction_Parameters
        use DVR_Parameters
        use Matrix_Elements
        USE CI_Production_Parameters 
        USE Input_Namelists
        USE   MPI
        
        IMPLICIT NONE 

        INTEGER :: IERR,MYID


        call MPI_COMM_RANK(MPI_COMM_WORLD,MYID,ierr)

!    Setting defaults on input variables.
!    Setting defaults on input variables.
!    Setting defaults on input variables.
        JOB_Type='BOS'
        mass=1.d0
        Job_Prefactor=(-1.0d0,0.0d0)
        GUESS='HAND' 
        Act_Operator_Init=.FALSE. 
 
        DO_OCT=.FALSE.
        OCT_Restart=.FALSE.
        OCT_NC=5
        OCT_Alpha=0.8d0
        OCT_NGoals=1
        OCT_Beta=0.2d0
        OCT_TimeSteps=10000
        OCT_Func_Tolerance=0.0001 
        OCT_CRAB_Tolerance=0.0001
        OCT_Randomize=.FALSE.
        OCT_NRandomize=100
        OCT_Bounded=.FALSE.
        OCT_Bounded_Bandwidth=.FALSE.
        OCT_OptOmega=.FALSE.
        OCT_Method='SIMP'
        OCT_UpperBounds=10.d0
        OCT_LowerBounds=-10.d0
        SIMANN=.FALSE.
        TruncatedNewton=.FALSE.

        Multi_Level=.FALSE.           
        NLevel=1
        Conical_Intersection=.FALSE.

        Bose_Hubbard=.FALSE.
        Periodic_BH=.TRUE.
        BH_J=(0.d0,0.d0)
        BH_U=0.d0
        Calculate_Rho2=.TRUE.
        Cavity_BEC = .FALSE. !< toggle to couple the MCTDHB equations to a cavity field
        NCavity_Modes = 1 !< Number of cavity modes
        Cavity_PumpRate = 0.d0 !< Rate at which the cavity is pumped
        Cavity_LossRate = 0.d0 !< Rate at which it loses photons                            
        Cavity_Detuning = 0.d0 !< Cavity detuning                                           
        Atom_Detuning = 0.d0 !< Atom detuning                                           
        Cavity_K0  = 0.d0 !< Cavity resonance                                               
        CavityAtom_Coupling = 0.d0 !< Coupling strength of the atoms to the cavity          
        X_Cavity_Pump_Waist = 0.d0 !< For 2D systems: X- Waist of pump beam.
        Cavity_Mode_Waist = 0.d0 !< For 2D systems: Waist of cavity beam.
        Pump_Switch = .FALSE.  !< Smoothly switch on cavity pump laser.                     
        RampupTime = 0.d0 !< Rampup time for exponential ramp.                              
        RampdownTime = 0.d0 !< Rampdown time for exponential ramp.                          
        PlateauTime = 0.d0 !< Plateau time in pump power.                                   
        Pump_Oscillate = .FALSE. !< Pump power is oscillating when the plateau is reached?
        Pump_Amplitude = 0.d0 !< Amplitude of the oscillation of the pump's power as a fraction of Cavity_PumpRate
        Pump_Period = 0.d0 !< At what period the power is oscillating.
        Custom_Cavity = .FALSE.
        Detuning_Switch=.FALSE.
        Detuning_Switch_Mode = 'linear' 
        Detuning_SwitchValue = 0.d0
        Detuning_SwitchTime= 0.d0
        Which_Cavity = 'SinCos' 
        Transverse_Pump=.FALSE.

        Diagonalize_OneBodyh=.FALSE.
        Binary_Start_Time=-666.d0
        Restart_State=1 

        Fixed_Lz=.FALSE.
        OrbLz=0.d0
        Vortex_Imprint=.FALSE.
        Profile='tanh'

        DIM_MCTDH=1
        NDVR_X=256
        NDVR_Y=1
        NDVR_Z=1           
        DVR_X=4
        DVR_Y=4
        DVR_Z=4
        x_initial=-12.0d0
        x_final=+12.0d0  
        y_initial=-8.0d0 
        y_final=+8.0d0   
        z_initial=-8.0d0
        z_final=+8.0d0  

        

        Time_Begin=0.0d0              
        Time_Final=20.0d0             
        Time_Max=1.d99                
        Output_TimeStep=0.1d0         
        Output_Coefficients=1                
        Integration_Stepsize=0.01d0           
        Error_Tolerance=1.0d-9               
        Minimal_Occupation=1.0d-12           
        Minimal_Krylov=5                     
        Maximal_Krylov=20                    
        Integration_StepSize=0.01
        Orbital_Integrator='OMPABM'
        Orbital_Integrator_order=7
        Orbital_Integrator_MaximalStep=0.01d0
        MPI_ORBS=.FALSE.              
        Write_ASCII=.TRUE.              
        Error_Rescale_TD=1.0d0             
        Error_Rescale_Cavity=1.d0
        LZ=.FALSE.                             
        Magnetic_FFT=.FALSE.
        SyntheticMagnetic=.FALSE.
        Magnetic_field_type=1
        Charge=0.d0
        Magnetic_Strength=0.d0            
        a_vec=0.d0
        b_vec=0.d0
        OMEGAZ=0.0d0                 
        STATE=1                      
        Coefficients_Integrator='MCS'
        NProc_Max_CI=0
        BlockSize=4
        RLX_Emin=-1.d90
        RLX_Emax=1.d90
        Olsen=.FALSE.

        whichpot="HO1D" 
        parameter1=1.d0 
        parameter2=0.d0 
        parameter3=0.d0 
        parameter4=0.d0 
        parameter5=0.d0 
        parameter6=0.d0 
        parameter7=0.d0 
        parameter8=0.d0 
        parameter9=0.d0 
        parameter10=0.d0
        parameter11=0.d0
        parameter12=0.d0
        parameter13=0.d0
        parameter14=0.d0
        parameter15=0.d0
        parameter16=0.d0
        parameter17=0.d0
        parameter18=0.d0
        parameter19=0.d0
        parameter20=0.d0
        parameter21=0.d0
        parameter22=0.d0
        parameter23=0.d0
        parameter24=0.d0
        parameter25=0.d0
        parameter26=0.d0
        parameter27=0.d0
        parameter28=0.d0
        parameter29=0.d0
        parameter30=0.d0


        Which_Interaction='gauss'
        Interaction_parameter1 =0.d0
        Interaction_parameter2 =0.d0
        Interaction_parameter3 =0.d0
        Interaction_parameter4 =0.d0
        Interaction_parameter5 =0.d0
        Interaction_parameter6 =0.d0
        Interaction_parameter7 =0.d0
        Interaction_parameter8 =0.d0
        Interaction_parameter9 =0.d0
        Interaction_parameter10=0.d0

        Interaction_Width=0.15d0 
        Interaction_Type=0       

!  Experimental -- Domain decomposition of the coefficients
        CI_V3=.FALSE.
        NPROC_CI=1
        NPROC_OP1B=1
        NPROC_OP2B=1


 
        WRITE(1003,*) "################################"
        WRITE(1003,*) "#######READING INPUT############"
        WRITE(1003,*) "################################"
       
        write(1003,*) "Trying to Read MCTDHX.inp"

        open(8,file="MCTDHX.inp",status='OLD',recl=8000,&
                                         delim='APOSTROPHE')
        Write(1003,*) "Opened MCTDHX.inp", MYID

        read(8,NML=System_Parameters) 
        read(8,NML=DVR)
        read(8,NML=Integration) 
        read(8,NML=Potential) 
        read(8,NML=Interaction)

        IF ((GUESS.eq.'HAND').and.&
            (abs(Binary_Start_Time-666.d0).le.1.d-10)) THEN
           Binary_Start_Time=0.d0 ! If Binary Start time was not given, reset it for Guess=HAND
        ENDIF
! Catches for erroneous input.
        IF ((DIM_MCTDH.eq.1).and.((NDVR_Y.gt.1).or.(NDVR_Y.gt.1))) THEN
           Write(6,*) "For one-dimensional computations, the DVR in Z &
                       and Y directions must have 1 point."
           STOP
        ENDIF
        IF ((DIM_MCTDH.eq.2).and.(NDVR_Z.gt.1)) THEN
           Write(6,*) "For two-dimensional computations, the DVR in Z &
                       direction must have 1 point."
           STOP
        ENDIF
   

        IF ((REAL(Job_Prefactor).lt.1.d-10).and.&
           (ABS((DIMAG(Job_Prefactor))-1.d0).lt.1.d-10).and.&
           (STATE.ne.1)) then
           write(*,*) "I will not do a propagation with the 2nd &
           Krylov Vector, Change STATE or Job_Prefactor in MCTDHX.inp!!"
           stop
        endif
 
        IF ((REAL(Job_Prefactor).lt.1.d-10).and.&
           (ABS((DIMAG(Job_Prefactor))-1.d0).lt.1.d-10).and.&
           (Fixed_Lz.eqv..TRUE.)) THEN
           write(6,*) "Cannot do propagations with fixed orbital &
                       angular momenta!!!"
           stop
        ENDIF
 
        IF (((DVR_X.ne.4).or.&
            (DVR_Y.ne.4)).and.(LZ.eqv..TRUE.)) then
           write(6,*) "ANGULAR MOMENTUM WORKS WITH FFTDVR ONLY!!!"
           stop
        endif

        IF ((Vortex_Seeding.eqv..TRUE.)&
           .and.(Vortex_Imprint.eqv..TRUE.)) THEN
           Write(6,*) "Only either Vortex_Imprint   &
                     or Vortex_Seeding can be true and not both!!!!"
           STOP
        ENDIF

        IF ((Multi_Level.eqv..FALSE.).and.(NLevel.ne.1)) THEN
            NLevel=1
            write(6,*) "Multi_Level is not activated, NLevel is set to 1."
        ENDIF

        IF ((Diagonalize_OneBodyh.eqv..TRUE.).and. &
           (((DIM_MCTDH.eq.1).and.(DVR_X.eq.4)).or.&
           ((DIM_MCTDH.eq.2).and.((DVR_X.eq.4).or.(DVR_Y.eq.4))).or.&
           ((DIM_MCTDH.eq.3).and.((DVR_X.eq.4).or.(DVR_Y.eq.4) &
                                                .or.(DVR_Z.eq.4))))) THEN
           Write(6,*) "Diagonalize_OneBodyh not possible for FFT DVR!!!"
           STOP
        ENDIF

        IF ((trim(which_Interaction).eq.'').or.(trim(whichpot).eq.'')) THEN
           Write(6,*) "which_Interaction of whichpot is empty!!!"
           STOP
        ENDIF 
   
        IF (((Cavity_BEC.eqv..TRUE.) &
             .and.(Pump_Oscillate(1).eqv..TRUE.)) &
            .and.(Job_Prefactor.eq.dcmplx(-1.0d0,0.0d0))) THEN
           write(6,*) "Oscillating laser pump in relaxation selected!!!"
           STOP
        ENDIF
        close(8)
        Write(1003,*) "Closed MCTDHX.inp", MYID


        IF ((TRIM(Coefficients_Integrator).eq.'DAV').or.&
           (TRIM(Coefficients_Integrator).eq.'DAV')) THEN
           IF (ABS(REAL(Job_Prefactor)).lt.0.001d0) then
              WRITE(6,*) "DAV and DSL are only for RELAXATION!!!!"
              STOP
          ENDIF
        ENDIF



        IF (MYID.eq.0) THEN  
          IF (save_input.eqv..TRUE.) THEN
		call SYSTEM("cp MCTDHX.inp OLD_MCTDHX.inp_`date +%s`")
          ENDIF
	ENDIF

        WRITE(1003,*) "################################"
        WRITE(1003,*) "#######INPUT READ ##############"
        WRITE(1003,*) "################################"


        end subroutine Read_Input

      END MODULE Input_Output
