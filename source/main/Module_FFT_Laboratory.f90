!>GPLv3
!    MCTDH-X: the multiconfigurational time-dependent Hartree for 
!    indistinguishable particles software
!
!    Copyright (C) 2021  A. U. J. Lode, M. C. Tsatsos, 
!                        E. Fasshauer, S. E. Weiner, 
!                        R. Lin, L. Papariello, P. Molignini, 
!                        C. Lévêque, M. Büttner , J. Xiang, S. Dutta
!
!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <https://www.gnu.org/licenses/>.
!
!
!
!
!<GPLv3










!> This module collects all FFT routines necessary to compute momentum space representations of orbitals or to apply kinetic energy operators.
!> \todo include MPI FFT routines or maybe GPU FFTs as well as the corresponding preprocessor flags.
Module FFT_Laboratory
IMPLICIT NONE
Contains 

!> This routine destroys all FFTW/MKL FFT plans
Subroutine Destroy_Global_FFT_Plans

USE DVR_Parameters

     !!! Destroy FFT Plans/Descriptors
        IF (((DVR_X.eq.4).and.(DIM_MCTDH.eq.1)).or.&
          ((DVR_X.eq.4).and.(DVR_Y.ne.4)).or.&
          ((DVR_X.ne.4).and.(DIM_MCTDH.eq.1).and.&
           ((Interaction_Type.eq.4).or.(Interaction_Type.eq.5).or.(Interaction_Type.eq.7)))) &
           THEN 

           CALL Destroy_FFT(1,0,1)

#if FFTW
           CALL Destroy_FFT(1,1,1)
#endif        

        
        ELSEIF ((DIM_MCTDH.ge.2).and.(DVR_Y.eq.4).and.(DVR_X.ne.4)) THEN
           CALL Destroy_FFT(1,0,2)
#if FFTW
           CALL Destroy_FFT(1,1,2)
#endif        
        ELSEIF ((DIM_MCTDH.ge.2).and.(DVR_Z.eq.4).and.&
               ((DVR_X.ne.4).or.(DVR_Y.ne.4))) THEN
           CALL Destroy_FFT(1,0,3)
#if FFTW
           CALL Destroy_FFT(1,1,3)
#endif        
        ELSEIF (DIM_MCTDH.eq.2.and.((DVR_X.eq.4).and.(DVR_Y.eq.4))) THEN 
           CALL Destroy_FFT(2,0,666)
#if FFTW
           CALL Destroy_FFT(2,1,666)
#endif        
        ELSEIF (DIM_MCTDH.eq.3.and.&
               ((DVR_X.eq.4).and.(DVR_Y.eq.4).and.(DVR_Z.eq.4))) THEN 
           CALL Destroy_FFT(3,0,666)
#if FFTW
           CALL Destroy_FFT(3,1,666)
#endif        
        ENDIF


        IF (((LZ.eqv..TRUE.).and.(DIM_MCTDH.ne.1)).or. &
            ((Magnetic_FFT.eqv..TRUE.).and.(DIM_MCTDH.ne.1)).or. &
            ((SpinOrbit.eqv..TRUE.).and.(DIM_MCTDH.ne.1))) THEN

           IF ((DIM_MCTDH.ge.2).and.(DVR_X.eq.4).and.(DVR_Y.eq.4)) THEN
            CALL Destroy_FFT(1,0,1)

#if FFTW
            CALL Destroy_FFT(1,1,1)
#endif        

            CALL Destroy_FFT(1,0,2)
#if FFTW
            CALL Destroy_FFT(1,1,2)
#endif        
           ENDIF

           IF ((DIM_MCTDH.eq.3).and.(DVR_X.eq.4).and.(DVR_Y.eq.4).and.(DVR_Z.eq.4)) THEN
            CALL Destroy_FFT(1,0,3)
#if FFTW
            CALL Destroy_FFT(1,1,3)
#endif        
           ENDIF

        ENDIF
End Subroutine Destroy_Global_FFT_Plans

Subroutine Initialize_Global_FFT_Plans(Dilation)

USE MPI_FFT_DATA
USE Global_Parameters,ONLY:Orbital_Integrator,MPI_ORBS
USE DVR_Parameters
USE Analysis_Input_Variables, ONLY: Variance_observables 

USE MPI

IMPLICIT NONE
INTEGER, OPTIONAL,VALUE :: Dilation
INTEGER :: IERR,MYID

IF(.NOT. PRESENT(Dilation)) Dilation = 1 ! default value for dilation
WRITE(*,'("DILATION IN MPI_FFT ALLOC is ", I0)') Dilation

        IF (MPI_ORBS.eqv..FALSE.) THEN
           CALL MPI_FFT_Data_Allocation(5,MYID)  
        ENDIF

        call MPI_COMM_RANK(MPI_COMM_WORLD,MYID,ierr)

        IF (((DVR_X.eq.4).and.(DIM_MCTDH.eq.1)).or.&
          ((DVR_X.eq.4).and.(DVR_Y.ne.4)).or.&
          ((DVR_X.ne.4).and.(DIM_MCTDH.eq.1).and.&
           ((Interaction_Type.eq.4).or.(Interaction_Type.eq.5).or.(Interaction_Type.eq.7)))) &
        THEN 

           IF (MPI_ORBS.eqv..TRUE.) THEN
              Write(6,*) "MPI FFTs for 1D systems not yet implemented"
              STOP
           ENDIF

           CALL Init_FFT(1,0,1)

#if FFTW
           CALL Init_FFT(1,1,1)
#endif        

        ELSEIF ((DIM_MCTDH.ge.2).and.(DVR_Y.eq.4).and.(DVR_X.ne.4)) THEN

           IF (MPI_ORBS.eqv..TRUE.) THEN
              Write(6,*) "MPI FFTs for 1D systems not yet implemented"
              STOP
           ENDIF

           CALL Init_FFT(1,0,2)
#if FFTW
           CALL Init_FFT(1,1,2)
#endif        
        ELSEIF ((DIM_MCTDH.ge.2).and.(DVR_Z.eq.4).and.&
               ((DVR_X.ne.4).or.(DVR_Y.ne.4))) THEN

           IF (MPI_ORBS.eqv..TRUE.) THEN
              Write(6,*) "MPI FFTs for 1D systems not yet implemented"
              STOP
           ENDIF

           CALL Init_FFT(1,0,3)
#if FFTW
           CALL Init_FFT(1,1,3)
#endif        
        ELSEIF (DIM_MCTDH.eq.2.and.((DVR_X.eq.4).and.(DVR_Y.eq.4))) THEN 

           IF (MPI_ORBS.eqv..TRUE.) THEN
#if MPIFFT
               CALL MPI_FFT_Data_Allocation(2,MYID,Dilation)  
#endif        
           ENDIF

           CALL Init_FFT(2,0,666)
#if FFTW
           CALL Init_FFT(2,1,666)
#endif        
        ELSEIF (DIM_MCTDH.eq.3.and. &
               ((DVR_X.eq.4).and.(DVR_Y.eq.4).and.(DVR_Z.eq.4))) THEN 
           IF (MPI_ORBS.eqv..TRUE.) THEN
#if MPIFFT
               CALL MPI_FFT_Data_Allocation(3,MYID,Dilation)  
#endif        
           ENDIF

           CALL Init_FFT(3,0,666)
#if FFTW
           CALL Init_FFT(3,1,666)
#endif        
        ENDIF

! If angular momentum or spin-orbit coupling operators are needed, one needs the 1D plans also.
        IF (((LZ.eqv..TRUE.).and.(DIM_MCTDH.ne.1)).or. &
            ((Variance_observables.eqv..TRUE.).and.(DIM_MCTDH.ne.1)).or. &
            ((Magnetic_FFT.eqv..TRUE.).and.(DIM_MCTDH.ne.1)).or. &
            ((SyntheticMagnetic.eqv..TRUE.).and.(DIM_MCTDH.ne.1)).or. &
            ((SpinOrbit.eqv..TRUE.).and.(DIM_MCTDH.ne.1))) THEN
           IF ((DIM_MCTDH.ge.2).and.(DVR_X.eq.4).and.(DVR_Y.eq.4)) THEN
            CALL Init_FFT(1,0,1)
#if FFTW
            CALL Init_FFT(1,1,1)
#endif        


            CALL Init_FFT(1,0,2)
#if FFTW
            CALL Init_FFT(1,1,2)
#endif        
           ENDIF

           IF ((DIM_MCTDH.eq.3).and.(DVR_X.eq.4).and.(DVR_Y.eq.4).and.(DVR_Z.eq.4)) THEN
            CALL Init_FFT(1,0,3)
#if FFTW
            CALL Init_FFT(1,1,3)
#endif        
           ENDIF

        ENDIF
 
End Subroutine Initialize_Global_FFT_Plans



!> This routine frees specific descriptors/destroys FFT plans.
subroutine Destroy_FFT(Dimensionality,Direction,Dimension)

USE Global_Parameters
USE DVR_Parameters

#if MKLFFT
USE MKL_DFTI
#endif

#if FFTW
INCLUDE "fftw3.f"
#endif

INTEGER :: Dimensionality, Direction,Dimension,Status

Dim: SELECT CASE (Dimensionality)
CASE(1)
 IF (Direction.eq.0) THEN

    IF (Dimension.eq.1) THEN

#if FFTW
       call dfftw_destroy_plan(FFTW_Forward_Master_Plan_1D_X)
#endif
#if MKLFFT
       Status = DftiFreeDescriptor(MKL_Master_Plan_1D_X)
#endif
    ENDIF

    IF (Dimension.eq.2) THEN

#if FFTW
       call dfftw_destroy_plan(FFTW_Forward_Master_Plan_1D_Y)
#endif
#if MKLFFT
       Status = DftiFreeDescriptor(MKL_Master_Plan_1D_Y)
#endif

    ENDIF

    IF (Dimension.eq.3) THEN

#if FFTW
       call dfftw_destroy_plan(FFTW_Forward_Master_Plan_1D_Z)
#endif
#if MKLFFT
       Status = DftiFreeDescriptor(MKL_Master_Plan_1D_Z)
#endif

    ENDIF

 ENDIF

 IF (Direction.eq.1) THEN

    IF (Dimension.eq.1) THEN
#if FFTW
       call dfftw_destroy_plan(FFTW_Backward_Master_Plan_1D_X)
#endif
#if MKLFFT
       Status = DftiFreeDescriptor(MKL_Master_Plan_1D_X)
#endif
    ENDIF

    IF (Dimension.eq.2) THEN
#if FFTW
       call dfftw_destroy_plan(FFTW_Backward_Master_Plan_1D_Y)
#endif
#if MKLFFT
       Status = DftiFreeDescriptor(MKL_Master_Plan_1D_Y)
#endif
    ENDIF

    IF (Dimension.eq.3) THEN
#if FFTW
       call dfftw_destroy_plan(FFTW_Backward_Master_Plan_1D_Z)
#endif
#if MKLFFT
       Status = DftiFreeDescriptor(MKL_Master_Plan_1D_Z)
#endif
    ENDIF

 ENDIF

CASE(2)

  IF (Direction.eq.0) THEN

#if FFTW
       call dfftw_destroy_plan(FFTW_Forward_Master_Plan_2D)
#endif
#if MKLFFT
       Status = DftiFreeDescriptor(MKL_Master_Plan_2D)
#endif

  ENDIF

  IF (Direction.eq.1) THEN

#if FFTW
       call dfftw_destroy_plan(FFTW_Backward_Master_Plan_2D)
#endif
#if MKLFFT
       Status = DftiFreeDescriptor(MKL_Master_Plan_2D)
#endif

  ENDIF

CASE(3)

  IF (Direction.eq.0) THEN

#if FFTW
       call dfftw_destroy_plan(FFTW_Forward_Master_Plan_3D)
#endif
#if MKLFFT
       Status = DftiFreeDescriptor(MKL_Master_Plan_3D)
#endif

  ENDIF

  IF (Direction.eq.1) THEN

#if FFTW
       call dfftw_destroy_plan(FFTW_Backward_Master_Plan_3D)
#endif
#if MKLFFT
       Status = DftiFreeDescriptor(MKL_Master_Plan_3D)
#endif

  ENDIF
End Select Dim
end subroutine Destroy_FFT




!> This routine initializes all necessary plans.
subroutine Init_FFT(Dimensionality,Direction,Dimension)

USE Global_Parameters
USE DVR_Parameters

COMPLEX*16 :: Psi(NDVR_X*NDVR_Y*NDVR_Z)

INTEGER :: Dimensionality, Direction,Dimension

Dim: SELECT CASE (Dimensionality)
CASE(1)
 IF (Direction.eq.0) THEN

    IF (Dimension.eq.1) THEN

#if FFTW
       CALL Initialize_FFTW_1D(FFTW_Forward_Master_Plan_1D_X,Dimension,Direction)
#endif
#if MKLFFT
       CALL Initialize_MKL_FFT_1D(MKL_Master_Plan_1D_X,Dimension)
#endif
    ENDIF

    IF (Dimension.eq.2) THEN

#if FFTW
       CALL Initialize_FFTW_1D(FFTW_Forward_Master_Plan_1D_Y,Dimension,Direction)
#endif
#if MKLFFT
       CALL Initialize_MKL_FFT_1D(MKL_Master_Plan_1D_Y,Dimension)
#endif

    ENDIF

    IF (Dimension.eq.3) THEN

#if FFTW
       CALL Initialize_FFTW_1D(FFTW_Forward_Master_Plan_1D_Z,Dimension,Direction)
#endif
#if MKLFFT
       CALL Initialize_MKL_FFT_1D(MKL_Master_Plan_1D_Z,Dimension)
#endif

    ENDIF

 ENDIF

 IF (Direction.eq.1) THEN

    IF (Dimension.eq.1) THEN
#if FFTW
       CALL Initialize_FFTW_1D(FFTW_Backward_Master_Plan_1D_X,Dimension,Direction)
#endif
#if MKLFFT
       CALL Initialize_MKL_FFT_1D(MKL_Master_Plan_1D_X,Dimension)
#endif
    ENDIF

    IF (Dimension.eq.2) THEN
#if FFTW
       CALL Initialize_FFTW_1D(FFTW_Backward_Master_Plan_1D_Y,Dimension,Direction)
#endif
#if MKLFFT
       CALL Initialize_MKL_FFT_1D(MKL_Master_Plan_1D_Y,Dimension)
#endif
    ENDIF

    IF (Dimension.eq.3) THEN
#if FFTW
       CALL Initialize_FFTW_1D(FFTW_Backward_Master_Plan_1D_Z,Dimension,Direction)
#endif
#if MKLFFT
       CALL Initialize_MKL_FFT_1D(MKL_Master_Plan_1D_Z,Dimension)
#endif
    ENDIF

 ENDIF

CASE(2)

  IF (Direction.eq.0) THEN

#if FFTW
     CALL Initialize_FFTW_2D(FFTW_Forward_Master_Plan_2D,Direction)
#endif
#if MKLFFT
       CALL Initialize_MKL_FFT_2D(MKL_Master_Plan_2D)
#endif

  ENDIF

  IF (Direction.eq.1) THEN

#if FFTW
     CALL Initialize_FFTW_2D(FFTW_Backward_Master_Plan_2D,Direction)
#endif
#if MKLFFT
       CALL Initialize_MKL_FFT_2D(MKL_Master_Plan_2D)
#endif

  ENDIF

CASE(3)

  IF (Direction.eq.0) THEN

#if FFTW
     CALL Initialize_FFTW_3D(FFTW_Forward_Master_Plan_3D,Direction)
#endif
#if MKLFFT
       CALL Initialize_MKL_FFT_3D(MKL_Master_Plan_3D)
#endif

  ENDIF

  IF (Direction.eq.1) THEN

#if FFTW
     CALL Initialize_FFTW_3D(FFTW_Backward_Master_Plan_3D,Direction)
#endif
#if MKLFFT
       CALL Initialize_MKL_FFT_3D(MKL_Master_Plan_3D)
#endif

  ENDIF
  
  END SELECT Dim

end subroutine Init_FFT

!> Same as Get_FFT for dilated FFTs,i.e., increased resolution in the case of analysis.
subroutine Get_Dilated_FFT(Dimensionality,Psi,Direction,Dimension,Dilation)

USE DVR_Parameters

INTEGER :: Dimensionality, Direction,Dimension,dilation
COMPLEX*16 :: Psi(NDVR_X*NDVR_Y*NDVR_Z*Dilation**Dimensionality)


IF ((Dimensionality.eq.3).or.(Direction.ne.0)) THEN

#if FFTW
        CALL Get_FFT_Forward_FFTW_3D_Dilated(PSI,Dilation)
#endif

#if MKLFFT
     CALL Get_FFT_Forward_MKL_3D_Dilated(PSI,Dilation)
#endif
ENDIF

IF ((Dimensionality.eq.2).and.(Direction.eq.0)) THEN

#if FFTW
     CALL Get_FFT_Forward_FFTW_2D_Dilated(PSI,Dilation)
#endif
 
#if MKLFFT
     CALL Get_FFT_Forward_MKL_2D_Dilated(PSI,Dilation)
#endif

ENDIF

IF ((Dimensionality.eq.1).and.(Direction.eq.0)) THEN
#if FFTW
     CALL Get_FFT_Forward_FFTW_1D_Dilated(PSI,Dilation)
#endif
#if MKLFFT
     CALL Get_FFT_Forward_MKL_1D_Dilated(PSI,Dilation)
#endif
ENDIF

end subroutine Get_Dilated_FFT

subroutine Get_FFT_Forward_FFTW_1D_Dilated(Psi,Dilation)
USE DVR_Parameters
INTEGER :: Dimension,Dilation
COMPLEX*16,   DIMENSION(NDVR_X*NDVR_Y*NDVR_Z*Dilation) :: psi

#if FFTW
include 'fftw3.f'
integer*8 Plan_FFTW
INTEGER ::  ierr


call Initialize_FFTW_1D_Dilated(Plan_FFTW,0,Dilation)
call dfftw_execute_dft(Plan_FFTW,psi,psi)
call dfftw_destroy_plan(Plan_FFTW)

#endif

end subroutine Get_FFT_Forward_FFTW_1D_Dilated

subroutine Get_FFT_Forward_MKL_1D_Dilated(Psi,Dilation)
#if MKLFFT
Use MKL_DFTI
USE omp_lib
#endif
USE DVR_Parameters

Integer :: Status,Dilation
COMPLEX*16,   DIMENSION(NDVR_X*NDVR_Y*NDVR_Z*Dilation) :: psi
#if MKLFFT
type(DFTI_DESCRIPTOR), POINTER :: Desc_Handle_Dim1

CALL Initialize_MKL_FFT_1D_Dilated(Desc_Handle_Dim1,Dilation)

Status = DftiComputeForward(Desc_Handle_Dim1,psi)
Status = DftiFreeDescriptor(Desc_Handle_Dim1)

#endif

end subroutine Get_FFT_Forward_MKL_1D_Dilated

subroutine Get_FFT_Forward_FFTW_2D_Dilated(Psi,Dilation)
USE DVR_Parameters
#if MPIFFT
USE MPI_FFT_DATA, ONLY : Plan_MPI_forward_dilated,local_Ly_dilated
use, intrinsic :: iso_c_binding 
#endif
INTEGER :: Dimension,Dilation
integer*8 Plan_FFTW
#if !MPIFFT
COMPLEX*16,   DIMENSION(NDVR_X*NDVR_Y*NDVR_Z*Dilation*Dilation) :: psi
include 'fftw3.f'
#endif

#if MPIFFT
COMPLEX*16,   DIMENSION(NDVR_X*Local_LY_dilated*Dilation*Dilation),Target :: psi
COMPLEX(C_DOUBLE_COMPLEX), pointer :: Phi_P(:,:)
include 'fftw3-mpi.f03'
#endif

#if MPIFFT
IF (MPI_ORBS.eqv..TRUE.) THEN

  call Initialize_FFTW_2D_Dilated(Plan_FFTW,0,Dilation)
  WRite(6,*) "DIL-2D:", NDVR_X,local_LY_dilated,Dilation 
  Phi_P(0:(Dilation*NDVR_X)-1, 0:(Dilation*local_LY_dilated)-1) => PSI
  call fftw_mpi_execute_dft(Plan_MPI_forward_dilated,Phi_P,Phi_P)
  
  call fftw_destroy_plan(Plan_MPI_forward_dilated);
  
ELSE

  call Initialize_FFTW_2D_Dilated(Plan_FFTW,0,Dilation)
  call dfftw_execute_dft(Plan_FFTW,psi,psi)
  call dfftw_destroy_plan(Plan_FFTW)

ENDIF

#endif

#if FFTW
#if !MPIFFT

call Initialize_FFTW_2D_Dilated(Plan_FFTW,0,Dilation)
call dfftw_execute_dft(Plan_FFTW,psi,psi)
call dfftw_destroy_plan(Plan_FFTW)

#endif
#endif

end subroutine Get_FFT_Forward_FFTW_2D_Dilated

subroutine Get_FFT_Forward_FFTW_3D_Dilated(Psi,Dilation)
USE DVR_Parameters
#if MPIFFT
USE MPI_FFT_DATA, ONLY: Plan_MPI_forward_dilated,local_Lz_dilated
use, intrinsic :: iso_c_binding 
#endif

#if !MPIFFT
COMPLEX*16,   DIMENSION(NDVR_X*NDVR_Y*NDVR_Z*Dilation*Dilation*Dilation) :: psi
#if FFTW
include 'fftw3.f'
#endif
#endif

INTEGER :: Dimension,Dilation
integer*8 Plan_FFTW

#if MPIFFT
COMPLEX*16, TARGET :: PSI(NDVR_X*NDVR_Y*NDVR_Z*Dilation*Dilation*Dilation)

COMPLEX(C_DOUBLE_COMPLEX), pointer :: Phi_P(:,:,:)
include 'fftw3-mpi.f03'

IF (MPI_ORBS.eqv..TRUE.) THEN

  call Initialize_FFTW_3D_Dilated(Plan_FFTW,0,Dilation)
  write(6,*) "IN 3D-DIL-FFT:", Local_LZ_dilated,Dilation,SIZE(PSI),NDVR_X,NDVR_Y,NDVR_X*NDVR_Y*Local_LZ_dilated*Dilation*Dilation*Dilation
  
  Phi_P(0:(Dilation*NDVR_X)-1, 0:(Dilation*NDVR_Y)-1, 0:(local_Lz_dilated)-1) => PSI
  
  call fftw_mpi_execute_dft(Plan_MPI_forward_dilated,Phi_P,Phi_P)
  
  call fftw_destroy_plan(Plan_MPI_forward_dilated);
ELSE

  call Initialize_FFTW_3D_Dilated(Plan_FFTW,0,Dilation)
  call dfftw_execute_dft(Plan_FFTW,psi,psi)
  call dfftw_destroy_plan(Plan_FFTW)

ENDIF
#endif


#if !MPIFFT
#if FFTW
call Initialize_FFTW_3D_Dilated(Plan_FFTW,0,Dilation)
call dfftw_execute_dft(Plan_FFTW,psi,psi)
call dfftw_destroy_plan(Plan_FFTW)
#endif
#endif

end subroutine Get_FFT_Forward_FFTW_3D_Dilated



subroutine Get_FFT_Forward_MKL_2D_Dilated(Psi,Dilation)
#if MKLFFT
Use MKL_DFTI
USE omp_lib
#endif
USE DVR_Parameters

Integer :: Status,Dilation
COMPLEX*16,   DIMENSION(NDVR_X*NDVR_Y*NDVR_Z*Dilation*Dilation) :: psi
#if MKLFFT
type(DFTI_DESCRIPTOR), POINTER :: Desc_Handle_Dim1

CALL Initialize_MKL_FFT_2D_Dilated(Desc_Handle_Dim1,Dilation)

Status = DftiComputeForward(Desc_Handle_Dim1,psi)
Status = DftiFreeDescriptor(Desc_Handle_Dim1)

#endif

end subroutine Get_FFT_Forward_MKL_2D_Dilated

subroutine Get_FFT_Forward_MKL_3D_Dilated(Psi,Dilation)
#if MKLFFT
Use MKL_DFTI
USE omp_lib
#endif
USE DVR_Parameters

Integer :: Status,Dilation
COMPLEX*16,   DIMENSION(NDVR_X*NDVR_Y*NDVR_Z*Dilation*Dilation*Dilation) :: psi
#if MKLFFT
type(DFTI_DESCRIPTOR), POINTER :: Desc_Handle_Dim1

CALL Initialize_MKL_FFT_3D_Dilated(Desc_Handle_Dim1,Dilation)

Status = DftiComputeForward(Desc_Handle_Dim1,psi)
Status = DftiFreeDescriptor(Desc_Handle_Dim1)

#endif

end subroutine Get_FFT_Forward_MKL_3D_Dilated



subroutine Initialize_MKL_FFT_1D_Dilated(Descriptor_Handle,Dilation)
Use DVR_Parameters
#if MKLFFT
Use MKL_DFTI
#endif

INTEGER :: L,Dimension,Status,Dilation
#if MKLFFT
type(DFTI_DESCRIPTOR), POINTER :: Descriptor_Handle
#endif

#if FFTW
INTEGER :: Descriptor_Handle
#endif

#if MKLFFT
L=NDVR_X*Dilation
    
Status = DftiCreateDescriptor(Descriptor_Handle, DFTI_DOUBLE,DFTI_COMPLEX, 1, L ) 
Status = DftiCommitDescriptor(Descriptor_Handle) 

#endif
end subroutine Initialize_MKL_FFT_1D_Dilated

subroutine Initialize_MKL_FFT_2D_Dilated(Descriptor_Handle,Dilation)
Use DVR_Parameters
#if MKLFFT
Use MKL_DFTI
#endif

INTEGER :: L(2),Dimension,Status,Dilation
#if MKLFFT
type(DFTI_DESCRIPTOR), POINTER :: Descriptor_Handle
#endif

#if FFTW
INTEGER :: Descriptor_Handle
#endif

#if MKLFFT
L(1)=NDVR_X*Dilation
L(2)=NDVR_Y*Dilation
    
Status = DftiCreateDescriptor(Descriptor_Handle, DFTI_DOUBLE,DFTI_COMPLEX, 2, L ) 
Status = DftiCommitDescriptor(Descriptor_Handle) 

#endif
end subroutine Initialize_MKL_FFT_2D_Dilated

subroutine Initialize_MKL_FFT_3D_Dilated(Descriptor_Handle,Dilation)
Use DVR_Parameters
#if MKLFFT
Use MKL_DFTI
#endif

INTEGER :: L(3),Dimension,Status,Dilation
#if MKLFFT
type(DFTI_DESCRIPTOR), POINTER :: Descriptor_Handle
#endif

#if FFTW
INTEGER :: Descriptor_Handle
#endif

#if MKLFFT
L(1)=NDVR_X*Dilation
L(2)=NDVR_Y*Dilation
L(3)=NDVR_Z*Dilation
    
Status = DftiCreateDescriptor(Descriptor_Handle, DFTI_DOUBLE,DFTI_COMPLEX, 3, L ) 
Status = DftiCommitDescriptor(Descriptor_Handle) 

#endif
end subroutine Initialize_MKL_FFT_3D_Dilated




subroutine Initialize_FFTW_1D_Dilated(Plan_FFTW,Direction,Dilation)

USE DVR_Parameters
USE omp_lib

Integer :: Direction,Dilation
integer*8 Plan_FFTW
INTEGER :: NTHR,IRET

COMPLEX*16 :: DummyPSI(NDVR_X*NDVR_Y*NDVR_Z*Dilation)
#if FFTW
include 'fftw3.f'
nthr = OMP_GET_MAX_THREADS()
call dfftw_init_threads(iret)
call dfftw_plan_with_nthreads(nthr)
!!!!!!!!!!!!!!! Forward FFT
IF (DIRECTION.eq.0) THEN
!!!!!!!!!!!!!!! Forward FFT

  call dfftw_plan_dft_1d(plan_FFTW,NDVR_X*dilation,&
                         DummyPSI,DummyPSI,FFTW_FORWARD,FFTW_PATIENT)

!!!!!!!!!!!!!!! Backward FFT
ELSEIF (DIRECTION.eq.1) THEN
!!!!!!!!!!!!!!! Backward FFT
   Write(6,*) "Dilated 2D backward FFTW-FFT not currently implemented."
ENDIF

#endif
end subroutine Initialize_FFTW_1D_Dilated 


subroutine Initialize_FFTW_2D_Dilated(Plan_FFTW,Direction,Dilation)

USE DVR_Parameters
USE omp_lib
#if MPIFFT
USE MPI_FFT_DATA, ONLY : Local_Ly_Dilated,Local_y_Offset_Dilated,plan_mpi_forward_dilated,cu_tmp_dilated,alloc_local_dilated,cdata_untransposed_dilated,Psi_pointer_2D_dilated
use, intrinsic :: iso_c_binding 
USE MPI
#endif

Integer :: Direction,Dilation
integer*8 Plan_FFTW
INTEGER :: NTHR,IRET
COMPLEX*16,ALLOCATABLE :: DummyPSI(:)
#if MPIFFT
include "fftw3-mpi.f03"

IF (MPI_ORBS.eqv..TRUE.) THEN
  plan_mpi_forward_dilated = fftw_mpi_plan_dft_2d( INT8(NDVR_X*Dilation), INT8(NDVR_Y*Dilation), Psi_Pointer_2D_dilated, Psi_Pointer_2D_dilated, MPI_COMM_WORLD, FFTW_FORWARD, FFTW_MEASURE)
  write(6,*) "dilated 2d ffts:", local_LY_dilated, local_y_offset_dilated, MPI_ORBS
ELSE IF (MPI_ORBS.eqv..FALSE.) THEN

ALLOCATE(DummyPSI(NDVR_X*NDVR_Y*NDVR_Z*Dilation*dilation))
nthr = OMP_GET_MAX_THREADS()
call dfftw_init_threads(iret)
call dfftw_plan_with_nthreads(nthr)
call dfftw_plan_dft_2d(plan_FFTW,NDVR_X*dilation,NDVR_Y*dilation,&
                       DummyPSI,DummyPSI,FFTW_FORWARD,FFTW_PATIENT,fftw_unaligned)

ENDIF
#endif

#if FFTW
#if !MPIFFT

include 'fftw3.f'
ALLOCATE(DummyPSI(NDVR_X*NDVR_Y*NDVR_Z*Dilation*dilation))
nthr = OMP_GET_MAX_THREADS()
call dfftw_init_threads(iret)
call dfftw_plan_with_nthreads(nthr)
!!!!!!!!!!!!!!! Forward FFT
IF (DIRECTION.eq.0) THEN
!!!!!!!!!!!!!!! Forward FFT

  call dfftw_plan_dft_2d(plan_FFTW,NDVR_X*dilation,NDVR_Y*dilation,&
                DummyPSI,DummyPSI,FFTW_FORWARD,FFTW_PATIENT,fftw_unaligned)

!!!!!!!!!!!!!!! Backward FFT
ELSEIF (DIRECTION.eq.1) THEN
!!!!!!!!!!!!!!! Backward FFT
   Write(6,*) "Dilated 2D backward FFTW-FFT not currently implemented."
ENDIF
#endif
#endif
end subroutine Initialize_FFTW_2D_Dilated 

subroutine Initialize_FFTW_3D_Dilated(Plan_FFTW,Direction,Dilation)

USE DVR_Parameters
USE omp_lib

#if MPIFFT
USE MPI_FFT_DATA, ONLY : Local_Lz_Dilated,Local_z_Offset_Dilated,plan_mpi_forward_dilated,cu_tmp_dilated,alloc_local_dilated,cdata_untransposed_dilated,Psi_pointer_3D_dilated
USE, intrinsic :: iso_c_binding
USE MPI
#endif

Integer :: Direction,Dilation
integer*8 Plan_FFTW
INTEGER :: NTHR,IRET

#if FFTW
COMPLEX*16, ALLOCATABLE :: DummyPSI(:)
#if !MPIFFT
include 'fftw3.f'
#endif
#endif

#if MPIFFT
include "fftw3-mpi.f03"


IF (MPI_ORBS.eqv..TRUE.) THEN
  plan_mpi_forward_dilated = fftw_mpi_plan_dft_3d(INT8(NDVR_X*Dilation), INT8(NDVR_Y*Dilation), INT8(NDVR_Z*Dilation), Psi_Pointer_3D_dilated, Psi_Pointer_3D_dilated, MPI_COMM_WORLD, FFTW_FORWARD, FFTW_MEASURE)
  
ELSE

  nthr = OMP_GET_MAX_THREADS()
  call dfftw_init_threads(iret)
  call dfftw_plan_with_nthreads(nthr)
  !!!!!!!!!!!!!!! Forward FFT
  IF (DIRECTION.eq.0) THEN
  !!!!!!!!!!!!!!! Forward FFT
  
    ALLOCATE(DummyPSI(NDVR_X*NDVR_Y*NDVR_Z*Dilation*dilation*dilation))
    call dfftw_plan_dft_3d(plan_FFTW,NDVR_X*dilation,NDVR_Y*dilation,NDVR_Z*dilation,&
                  DummyPSI,DummyPSI,FFTW_FORWARD,FFTW_PATIENT,fftw_unaligned)
  
  !!!!!!!!!!!!!!! Backward FFT
  ELSEIF (DIRECTION.eq.1) THEN
  !!!!!!!!!!!!!!! Backward FFT
     Write(6,*) "Dilated 3D backward FFTW-FFT not currently implemented."
  ENDIF

ENDIF
#endif

#if FFTW
#if !MPIFFT
nthr = OMP_GET_MAX_THREADS()
call dfftw_init_threads(iret)
call dfftw_plan_with_nthreads(nthr)
!!!!!!!!!!!!!!! Forward FFT
IF (DIRECTION.eq.0) THEN
!!!!!!!!!!!!!!! Forward FFT
  ALLOCATE(DummyPSI(NDVR_X*NDVR_Y*NDVR_Z*Dilation*dilation*dilation))

  call dfftw_plan_dft_3d(plan_FFTW,NDVR_X*dilation,NDVR_Y*dilation,NDVR_Z*dilation,&
                DummyPSI,DummyPSI,FFTW_FORWARD,FFTW_PATIENT,fftw_unaligned)

!!!!!!!!!!!!!!! Backward FFT
ELSEIF (DIRECTION.eq.1) THEN
!!!!!!!!!!!!!!! Backward FFT
   Write(6,*) "Dilated 3D backward FFTW-FFT not currently implemented."
ENDIF

#endif
#endif
end subroutine Initialize_FFTW_3D_Dilated 



!> This routine computes the FFT or FFT^-1. 
!> Direction=0/1 decides if forward/backward transform is computed.
!> Dimension=1/2/3, in the case of 1D-transforms, if X/Y/Z transform shall be computed.
subroutine Get_FFT(Dimensionality,Psi,Direction,Dimension,Psi_Transposed,MYID,Transposed)
#if MPIFFT
USE MPI_FFT_DATA
#endif
USE DVR_Parameters
USE, intrinsic :: iso_c_binding

INTEGER :: Dimensionality, Direction, Dimension
INTEGER :: MYID
Logical, Optional :: Transposed
#if MPIFFT
COMPLEX*16,TARGET :: Psi(LocalPsiDim(MYID+1))
COMPLEX*16, OPTIONAL,TARGET :: Psi_Transposed(LocalPsiDim_Trans(MYID+1))
#else
COMPLEX*16 :: Psi(NDVR_X*NDVR_Y*NDVR_Z)
COMPLEX*16, OPTIONAL :: Psi_Transposed(1)
#endif

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
IF (Direction.eq.0) THEN ! Forward FFT
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  IF (Dimensionality.eq.1) THEN
#if FFTW
    IF (MPI_ORBS.eqv..FALSE.) THEN
       CALL Get_FFT_Forward_FFTW_1D(PSI,Dimension)
    ELSEIF (MPI_ORBS.eqv..TRUE.) THEN
       CALL Get_FFT_Forward_FFTW_MPI_1D(PSI,Dimension,MYID)
    ENDIF
#endif
#if MKLFFT
    CALL Get_FFT_Forward_MKL_1D(PSI,Dimension)
#endif
  ENDIF

  IF (Dimensionality.eq.2) THEN
#if FFTW
    IF (MPI_ORBS.eqv..FALSE.) THEN
       CALL Get_FFT_Forward_FFTW_2D(PSI)
    ELSEIF (MPI_ORBS.eqv..TRUE.) THEN
#if MPIFFT
       PSI_Pointer_2D_aux(0:NDVR_X-1, 0:local_Ly-1) => PSI
       IF (Present(Transposed).and.(Transposed.eqv..FALSE.)) THEN
          CALL Get_FFT_Forward_FFTW_MPI_2D_UNTRANSPOSED(PSI_pointer_2D_aux)
       ELSE 
          PSI_Transposed_Pointer_2D_aux(0:NDVR_Y-1, 0:local_Lx-1) => PSI_Transposed
          CALL Get_FFT_Forward_FFTW_MPI_2D(PSI_pointer_2D_aux,Psi_Transposed_Pointer_2D_aux)
       ENDIF
#endif
    ENDIF
#endif
#if MKLFFT
     CALL Get_FFT_Forward_MKL_2D(PSI)
#endif
  ENDIF

  IF (Dimensionality.eq.3) THEN
#if FFTW
    IF (MPI_ORBS.eqv..FALSE.) THEN
       CALL Get_FFT_Forward_FFTW_3D(PSI)
    ELSEIF (MPI_ORBS.eqv..TRUE.) THEN
#if MPIFFT
       !> Assign pointers for 3D MPI FFTs
       PSI_Pointer_aux(0:NDVR_X-1, 0:NDVR_Y-1, 0:local_Lz-1) => PSI
       IF (Present(Transposed).and.(Transposed.eqv..FALSE.)) THEN
          CALL Get_FFT_Forward_FFTW_MPI_3D_UNTRANSPOSED(PSI_Pointer_aux)
       ELSE
          PSI_Transposed_Pointer_aux(0:NDVR_X-1,0:NDVR_Z-1, 0:local_Ly-1) => PSI_Transposed
          CALL Get_FFT_Forward_FFTW_MPI_3D(PSI_Pointer_aux,PSI_Transposed_Pointer_aux)
       ENDIF
#endif
    ENDIF
#endif
#if MKLFFT
     CALL Get_FFT_Forward_MKL_3D(PSI)
#endif
  ENDIF
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
ELSEIF (Direction.eq.1) THEN ! Backward FFT
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  IF (Dimensionality.eq.1) THEN
#if FFTW
    IF (MPI_ORBS.eqv..FALSE.) THEN
      CALL Get_FFT_Backward_FFTW_1D(PSI,Dimension)
    ELSEIF (MPI_ORBS.eqv..TRUE.) THEN
      CALL Get_FFT_Backward_FFTW_MPI_1D(PSI,Dimension,MYID)
    ENDIF
#endif
#if MKLFFT
    CALL Get_FFT_Backward_MKL_1D(PSI,Dimension)
#endif
  ENDIF

  IF (Dimensionality.eq.2) THEN
#if FFTW
    IF (MPI_ORBS.eqv..FALSE.) THEN
      CALL Get_FFT_Backward_FFTW_2D(PSI)
    ELSEIF (MPI_ORBS.eqv..TRUE.) THEN
#if MPIFFT
       PSI_Pointer_2D_aux(0:NDVR_X-1, 0:local_Ly-1) => PSI
       PSI_Transposed_Pointer_2D_aux(0:NDVR_Y-1, 0:local_Lx-1) => PSI_Transposed
       CALL Get_FFT_Backward_FFTW_MPI_2D(PSI_transposed_Pointer_2D_aux,Psi_Pointer_2D_aux)
#endif
    ENDIF
#endif
#if MKLFFT
     CALL Get_FFT_Backward_MKL_2D(PSI)
#endif
  ENDIF

  IF (Dimensionality.eq.3) THEN
#if FFTW
    IF (MPI_ORBS.eqv..FALSE.) THEN
      CALL Get_FFT_Backward_FFTW_3D(PSI)
    ELSEIF (MPI_ORBS.eqv..TRUE.) THEN
#if MPIFFT
        PSI_Pointer_aux(0:NDVR_X-1, 0:NDVR_Y-1, 0:local_Lz-1) => PSI
        PSI_Transposed_Pointer_aux(0:NDVR_X-1,         0:NDVR_Z-1, 0:local_Ly-1) => PSI_Transposed
        CALL Get_FFT_Backward_FFTW_MPI_3D(Psi_Transposed_Pointer_aux,Psi_Pointer_aux)
#endif
    ENDIF
#endif
#if MKLFFT
     CALL Get_FFT_Backward_MKL_3D(PSI)
#endif
  ENDIF
ENDIF

end subroutine Get_FFT

subroutine Initialize_FFTW_1D(Plan_FFTW,Dimension,Direction)

USE DVR_Parameters
use Orbital_Parallelization_Parameters, ONLY: LocalPsiDim_Trans,LocalPsiDim
USE omp_lib
USE MPI

use, intrinsic :: iso_c_binding

IMPLICIT NONE

INTEGER :: Dimension,Direction
INTEGER :: NTHR,IRET,Ierr,MYID
integer*8 Plan_FFTW
#if FFTW

#if MPIFFT
include 'fftw3-mpi.f03'
#else
include 'fftw3.f'
#endif


COMPLEX*16 :: DummyX(NDVR_X)
COMPLEX*16 :: DummyY(NDVR_Y)
COMPLEX*16 :: DummyZ(NDVR_Z)
COMPLEX*16, ALLOCATABLE :: Local_DummyPSI(:)
COMPLEX*16,ALLOCATABLE :: Local_DummyX(:)

IF (MPI_ORBS.eqv..TRUE.) THEN

   IF(Dimension.ne.1) THEN
      Write(6,*) "1d domain decomposition of orbitals only in X-direction!"
      STOP
   ENDIF

   call MPI_COMM_RANK(MPI_COMM_WORLD,MYID,ierr)
   ALLOCATE(Local_DummyX(LocalPsiDim(MYID+1))) 
#if MPIFFT
   CALL fftw_mpi_init()
#endif
ENDIF

nthr = OMP_GET_MAX_THREADS()
call dfftw_init_threads(iret)
call dfftw_plan_with_nthreads(nthr)

!!!!!!!!!!!!!!! Forward FFT
IF (DIRECTION.eq.0) THEN
!!!!!!!!!!!!!!! Forward FFT

  IF (Dimension.eq.1) THEN

    IF (MPI_ORBS.eqv..FALSE.) THEN
       call dfftw_plan_dft_1d(Plan_FFTW,NDVR_X,dummyX,dummyX,&
                            FFTW_FORWARD,FFTW_PATIENT)
    ELSEIF (MPI_ORBS.eqv..TRUE.) THEN
!       Plan_FFTW = fftw_mpi_plan_dft_1d(INT8(NDVR_X), Local_DummyX, Local_DummyX, MPI_COMM_WORLD,FFTW_FORWARD, FFTW_MEASURE)
    ENDIF

  ENDIF
  IF (Dimension.eq.2) THEN
    call dfftw_plan_dft_1d(Plan_FFTW,NDVR_Y,dummyY,dummyY,&
                            FFTW_FORWARD,FFTW_PATIENT)
  ENDIF
  IF (Dimension.eq.3) THEN
    call dfftw_plan_dft_1d(Plan_FFTW,NDVR_Z,dummyZ,dummyZ,&
                            FFTW_FORWARD,FFTW_PATIENT)
  ENDIF

ENDIF

!!!!!!!!!!!!!!! Backward FFT
IF (DIRECTION.eq.1) THEN
!!!!!!!!!!!!!!! Backward FFT
  IF (Dimension.eq.1) THEN

    IF (MPI_ORBS.eqv..FALSE.) THEN
      call dfftw_plan_dft_1d(Plan_FFTW,NDVR_X,dummyX,dummyX,&
                            FFTW_BACKWARD,FFTW_PATIENT)
    ELSEIF (MPI_ORBS.eqv..TRUE.) THEN
!       Plan_FFTW = 1_plan_dft_1d(NDVR_X, Local_DummyX, Local_DummyX, MPI_COMM_WORLD, FFTW_BACKWARD,FFTW_MEASURE)
    ENDIF

  ENDIF
  IF (Dimension.eq.2) THEN
    call dfftw_plan_dft_1d(Plan_FFTW,NDVR_Y,dummyY,dummyY,&
                            FFTW_BACKWARD,FFTW_PATIENT)
  ENDIF
  IF (Dimension.eq.3) THEN
    call dfftw_plan_dft_1d(Plan_FFTW,NDVR_Z,dummyZ,dummyZ,&
                            FFTW_BACKWARD,FFTW_PATIENT)
  ENDIF
ENDIF
#endif
end subroutine Initialize_FFTW_1D 

subroutine Initialize_FFTW_2D(Plan_FFTW,Direction)

USE MPI
USE DVR_Parameters
USE omp_lib
use, intrinsic :: iso_c_binding 

#if MPIFFT
USE MPI_FFT_DATA,ONLY: Psi_pointer_2D,PSI_transposed_pointer_2D,plan_mpi_forward, plan_mpi_backward, plan_mpi_forward_analysis
#endif

IMPLICIT NONE

#if FFTW
#if MPIFFT
include "fftw3-mpi.f03"
#else
include 'fftw3.f'
#endif
#endif

INTEGER*8 :: Plan_FFTW

Integer :: Direction
INTEGER :: NTHR,IRET

COMPLEX*16 :: DummyPSI(NDVR_X*NDVR_Y*NDVR_Z)
COMPLEX*16, ALLOCATABLE :: Local_DummyPSI(:)

#if FFTW
#if !MPIFFT
nthr = OMP_GET_MAX_THREADS()
call dfftw_init_threads(iret)
call dfftw_plan_with_nthreads(nthr)
#endif
!!!!!!!!!!!!!!! Forward FFT
IF (DIRECTION.eq.0) THEN
!!!!!!!!!!!!!!! Forward FFT
    IF (MPI_ORBS.eqv..FALSE.) THEN
      call dfftw_plan_dft_2d(plan_FFTW,NDVR_X,NDVR_Y,DummyPSI,DummyPSI,FFTW_FORWARD,FFTW_PATIENT,fftw_unaligned)
    ELSEIF (MPI_ORBS.eqv..TRUE.) THEN
#if MPIFFT
      plan_mpi_forward = fftw_mpi_plan_dft_2d(INT8(NDVR_X), INT8(NDVR_Y), Psi_pointer_2D, Psi_Transposed_Pointer_2D, MPI_COMM_WORLD, FFTW_FORWARD, ior(FFTW_MEASURE,FFTW_MPI_TRANSPOSED_OUT))
      plan_mpi_forward_analysis = fftw_mpi_plan_dft_2d(INT8(NDVR_X), INT8(NDVR_Y), Psi_pointer_2D, Psi_pointer_2D, MPI_COMM_WORLD, FFTW_FORWARD, FFTW_MEASURE)
#endif
    ENDIF
!!!!!!!!!!!!!!! Backward FFT
ELSEIF (DIRECTION.eq.1) THEN
!!!!!!!!!!!!!!! Backward FFT
    IF (MPI_ORBS.eqv..FALSE.) THEN
       call dfftw_plan_dft_2d(plan_FFTW,NDVR_X,NDVR_Y,DummyPSI,DummyPSI,FFTW_BACKWARD,FFTW_PATIENT,fftw_unaligned)
    ELSEIF (MPI_ORBS.eqv..TRUE.) THEN
#if MPIFFT
       plan_mpi_backward = fftw_mpi_plan_dft_2d(INT8(NDVR_X), INT8(NDVR_Y), Psi_Transposed_Pointer_2D, Psi_Pointer_2D, MPI_COMM_WORLD, FFTW_BACKWARD, ior(FFTW_MEASURE,FFTW_MPI_TRANSPOSED_IN))
#endif
    ENDIF

ENDIF

#endif
end subroutine Initialize_FFTW_2D 


subroutine Initialize_FFTW_3D(Plan_FFTW,Direction)

USE MPI
USE DVR_Parameters
USE omp_lib

use, intrinsic :: iso_c_binding 

#if MPIFFT
USE MPI_FFT_DATA,ONLY: Psi_pointer,PSI_transposed_pointer,plan_mpi_forward,plan_mpi_forward_analysis, plan_mpi_backward
USE MPI
#endif

IMPLICIT NONE

Integer :: Direction

INTEGER*8 :: Plan_FFTW
#if FFTW

#if MPIFFT
include 'fftw3-mpi.f03'
#else
include 'fftw3.f'
#endif


Integer :: IRET,NTHR 
COMPLEX*16 :: DummyPSI(NDVR_X*NDVR_Y*NDVR_Z)
COMPLEX*16, ALLOCATABLE :: Local_DummyPSI(:)


nthr = OMP_GET_MAX_THREADS()
call dfftw_init_threads(iret)
call dfftw_plan_with_nthreads(nthr)
!!!!!!!!!!!!!!! Forward FFT
IF (DIRECTION.eq.0) THEN
!!!!!!!!!!!!!!! Forward FFT
  IF (MPI_ORBS.eqv..FALSE.) THEN
     call dfftw_plan_dft_3d(plan_FFTW,NDVR_X,NDVR_Y,NDVR_Z,DummyPSI,DummyPSI,FFTW_FORWARD,FFTW_PATIENT,fftw_unaligned)
  ELSEIF (MPI_ORBS.eqv..TRUE.) THEN
#if MPIFFT
     Plan_mpi_forward = fftw_mpi_plan_dft_3d(INT8(NDVR_x), INT8(NDVR_Y), INT8(NDVR_z), Psi_pointer, Psi_Transposed_Pointer, MPI_COMM_WORLD, FFTW_FORWARD, ior(FFTW_MEASURE,FFTW_MPI_TRANSPOSED_OUT))
     Plan_mpi_forward_analysis = fftw_mpi_plan_dft_3d(INT8(NDVR_x), INT8(NDVR_Y), INT8(NDVR_z), Psi_pointer, Psi_pointer, MPI_COMM_WORLD, FFTW_FORWARD, FFTW_MEASURE)
#endif
  ENDIF


!!!!!!!!!!!!!!! Backward FFT
ELSEIF (DIRECTION.eq.1) THEN
!!!!!!!!!!!!!!! Backward FFT

  IF (MPI_ORBS.eqv..FALSE.) THEN
     call dfftw_plan_dft_3d(plan_FFTW,NDVR_x,NDVR_Y,NDVR_z,DummyPSI,DummyPSI,FFTW_BACKWARD,FFTW_PATIENT,fftw_unaligned)
  ELSEIF (MPI_ORBS.eqv..TRUE.) THEN
#if MPIFFT
     Plan_mpi_backward = fftw_mpi_plan_dft_3d(INT8(NDVR_x), INT8(NDVR_Y), INT8(NDVR_z), Psi_Transposed_Pointer, Psi_Pointer, MPI_COMM_WORLD, FFTW_BACKWARD, ior(FFTW_MEASURE,FFTW_MPI_TRANSPOSED_IN))
#endif
  ENDIF

ENDIF

#endif
end subroutine Initialize_FFTW_3D 


subroutine Initialize_MKL_FFT_2D(Descriptor_Handle)
#if MKLFFT
Use DVR_Parameters
Use MKL_DFTI

INTEGER :: L(2),Dimension,Status
type(DFTI_DESCRIPTOR), POINTER :: Descriptor_Handle
#endif

#if FFTW
INTEGER :: Descriptor_Handle
#endif

#if MKLFFT
L(1)=NDVR_X
L(2)=NDVR_Y
    
Status = DftiCreateDescriptor(Descriptor_Handle, DFTI_DOUBLE,DFTI_COMPLEX, 2, L ) 
Status = DftiCommitDescriptor(Descriptor_Handle) 

#endif
end subroutine Initialize_MKL_FFT_2D

subroutine Initialize_MKL_FFT_3D(Descriptor_Handle)
#if MKLFFT
Use DVR_Parameters
Use MKL_DFTI

INTEGER :: L(3),Status

type(DFTI_DESCRIPTOR), POINTER :: Descriptor_Handle
#endif

#if FFTW
INTEGER :: Descriptor_Handle
#endif

#if MKLFFT
L(1)=NDVR_X
L(2)=NDVR_Y
L(3)=NDVR_Z
    
Status = DftiCreateDescriptor(Descriptor_Handle, DFTI_DOUBLE,DFTI_COMPLEX, 3, L ) 
Status = DftiCommitDescriptor(Descriptor_Handle) 

#endif
end subroutine Initialize_MKL_FFT_3D

subroutine Initialize_MKL_FFT_1D(Descriptor_Handle,Dimension)

Use DVR_Parameters

#if MKLFFT
Use MKL_DFTI
type(DFTI_DESCRIPTOR), POINTER :: Descriptor_Handle
#endif

#if FFTW
INTEGER :: Descriptor_Handle
#endif

INTEGER :: Stride(2),Dimension,status

#if MKLFFT

IF (Dimension.eq.1) THEN
  Status = DftiCreateDescriptor(Descriptor_Handle,DFTI_DOUBLE,DFTI_COMPLEX, 1, NDVR_X)
  Status = DftiSetValue( Descriptor_Handle,DFTI_NUMBER_OF_TRANSFORMS, NDVR_Y*NDVR_Z)
  Status = DftiSetValue( Descriptor_Handle,DFTI_INPUT_DISTANCE, NDVR_X)
  Status = DftiSetValue( Descriptor_Handle,DFTI_OUTPUT_DISTANCE,NDVR_X)
  Status = DftiCommitDescriptor(Descriptor_Handle)
ENDIF 

IF (Dimension.eq.2) THEN
  Status = DftiCreateDescriptor(Descriptor_Handle, DFTI_DOUBLE,DFTI_COMPLEX,1,NDVR_Y)
  Stride(2) = NDVR_X
  Stride(1) = 0
  Status = DftiSetValue(Descriptor_Handle,DFTI_NUMBER_OF_TRANSFORMS, NDVR_X)
  Status = DftiSetValue(Descriptor_Handle,DFTI_INPUT_DISTANCE, 1)
  Status = DftiSetValue(Descriptor_Handle,DFTI_OUTPUT_DISTANCE,1)
  Status=  DftiSetValue(Descriptor_Handle,DFTI_INPUT_STRIDES,Stride) 
  Status=  DftiSetValue(Descriptor_Handle,DFTI_OUTPUT_STRIDES,Stride)
  Status = DftiCommitDescriptor(Descriptor_Handle)
ENDIF 

IF (Dimension.eq.3) THEN
  Status = DftiCreateDescriptor(Descriptor_Handle, DFTI_DOUBLE,DFTI_COMPLEX, 1, NDVR_Z )
  Stride(1) = 0
  Stride(2) = NDVR_X*NDVR_Y
  Status = DftiSetValue(Descriptor_Handle,DFTI_NUMBER_OF_TRANSFORMS, NDVR_X*NDVR_Y )
  Status = DftiSetValue(Descriptor_Handle,DFTI_INPUT_DISTANCE, 1)
  Status = DftiSetValue(Descriptor_Handle,DFTI_OUTPUT_DISTANCE,1)
  Status = DftiSetValue(Descriptor_Handle,DFTI_INPUT_STRIDES,Stride)
  Status = DftiSetValue(Descriptor_Handle,DFTI_OUTPUT_STRIDES,Stride)
  Status = DftiCommitDescriptor(Descriptor_Handle)
ENDIF 
#endif
end subroutine Initialize_MKL_FFT_1D


subroutine Get_FFT_Backward_MKL_1D(PSI,Dimension)
use Global_Parameters
USE DVR_Parameters
USE Interaction_Parameters
#if MKLFFT
Use MKL_DFTI
USE omp_lib

IMPLICIT NONE

#endif

INTEGER ::  ierr,Dimension,i
COMPLEX*16,   DIMENSION(NDVR_X*NDVR_Y*NDVR_Z) :: psi

#if MKLFFT
INTEGER :: STRIDE(2)

type(DFTI_DESCRIPTOR), POINTER :: Descriptor_Handle
Integer :: Status

 
IF (Dimension.eq.1) THEN
!  CALL Initialize_MKL_FFT_1D(Descriptor_Handle,1)
  Status = DftiComputeBackward(MKL_Master_Plan_1D_X,psi)
!  Status = DftiFreeDescriptor(Descriptor_Handle)
ENDIF
IF (Dimension.eq.2) THEN
!  CALL Initialize_MKL_FFT_1D(Descriptor_Handle,2)

  Stride(2) = NDVR_X
  Stride(1) = 0
  DO I=1,NDVR_X*NDVR_Y*NDVR_Z,NDVR_X*NDVR_Y
     Stride(1) = I-1
     Status=  DftiSetValue(MKL_Master_Plan_1D_Y,DFTI_INPUT_STRIDES,Stride)
     Status=  DftiSetValue(MKL_Master_Plan_1D_Y,DFTI_OUTPUT_STRIDES,Stride)
     Status = DftiCommitDescriptor(MKL_Master_Plan_1D_Y)
     Status = DftiComputeBackward(MKL_Master_Plan_1D_Y,psi)
  ENDDO
!  Status = DftiFreeDescriptor(Descriptor_Handle)
ENDIF
IF (Dimension.eq.3) THEN
!  CALL Initialize_MKL_FFT_1D(Descriptor_Handle,3)
  Status = DftiComputeBackward(MKL_Master_Plan_1D_Z,psi)
!  Status = DftiFreeDescriptor(Descriptor_Handle)
ENDIF


#endif 
end subroutine Get_FFT_Backward_MKL_1D

subroutine Get_FFT_Forward_MKL_1D(PSI,Dimension)

use Global_Parameters
USE DVR_Parameters
USE Interaction_Parameters
#if MKLFFT
Use MKL_DFTI
USE omp_lib

IMPLICIT NONE

#endif
INTEGER ::  ierr,Dimension,i
COMPLEX*16,   DIMENSION(NDVR_X*NDVR_Y*NDVR_Z) :: psi

#if MKLFFT
INTEGER :: STRIDE(2)

type(DFTI_DESCRIPTOR), POINTER :: Descriptor_Handle
Integer :: Status

 
IF (Dimension.eq.1) THEN
!  CALL Initialize_MKL_FFT_1D(Descriptor_Handle,1)
  Status = DftiComputeForward(MKL_Master_Plan_1D_X,psi)
!  Status = DftiFreeDescriptor(Descriptor_Handle)
ENDIF

IF (Dimension.eq.2) THEN
!  CALL Initialize_MKL_FFT_1D(Descriptor_Handle,2)

  Stride(2) = NDVR_X
  Stride(1) = 0

  DO I=1,NDVR_X*NDVR_Y*NDVR_Z,NDVR_X*NDVR_Y
     Stride(1) = I-1
     Status=  DftiSetValue(MKL_Master_Plan_1D_Y,DFTI_INPUT_STRIDES,Stride) 
     Status=  DftiSetValue(MKL_Master_Plan_1D_Y,DFTI_OUTPUT_STRIDES,Stride)
     Status = DftiCommitDescriptor(MKL_Master_Plan_1D_Y)
     Status = DftiComputeForward(MKL_Master_Plan_1D_Y,psi)
  ENDDO
!  Status = DftiFreeDescriptor(Descriptor_Handle)
ENDIF

IF (Dimension.eq.3) THEN
!  CALL Initialize_MKL_FFT_1D(Descriptor_Handle,3)

  Status = DftiComputeForward(MKL_Master_Plan_1D_Z,psi)
!  Status = DftiFreeDescriptor(Descriptor_Handle)
ENDIF
#endif
end subroutine Get_FFT_Forward_MKL_1D

subroutine Get_FFT_Forward_MKL_2D(Psi)
#if MKLFFT
Use MKL_DFTI
USE omp_lib
#endif
USE DVR_Parameters
COMPLEX*16,   DIMENSION(NDVR_X*NDVR_Y*NDVR_Z) :: psi
#if MKLFFT
Integer :: Status
type(DFTI_DESCRIPTOR), POINTER :: Desc_Handle_Dim1

!CALL Initialize_MKL_FFT_2D(Desc_Handle_Dim1)

Status = DftiComputeForward(MKL_Master_Plan_2D,psi)
!Status = DftiFreeDescriptor(Desc_Handle_Dim1)

#endif

end subroutine Get_FFT_Forward_MKL_2D

subroutine Get_FFT_Forward_MKL_3D(Psi)
#if MKLFFT
Use MKL_DFTI
USE omp_lib
#endif
USE DVR_Parameters
COMPLEX*16,   DIMENSION(NDVR_X*NDVR_Y*NDVR_Z) :: psi
#if MKLFFT
type(DFTI_DESCRIPTOR), POINTER :: Desc_Handle
INTEGER :: Status

!CALL Initialize_MKL_FFT_3D(Desc_Handle)

Status = DftiComputeForward(MKL_Master_Plan_3D,psi)
!Status = DftiFreeDescriptor(Desc_Handle)

#endif
end subroutine Get_FFT_Forward_MKL_3D

subroutine Get_FFT_Forward_FFTW_1D(Psi,Dimension)
USE DVR_Parameters
INTEGER :: Dimension,Status
COMPLEX*16,   DIMENSION(NDVR_X*NDVR_Y*NDVR_Z) :: psi

#if FFTW
include 'fftw3.f'
COMPLEX*16 :: dummyZ(NDVR_Z),dummyY(NDVR_Y),dummyX(NDVR_X)
INTEGER ::  ierr
INTEGER :: I,IND,J,K

!call Initialize_FFTW_1D(Plan_FFTW,Dimension,0)

IF (Dimension.eq.1) THEN
   Do K=1,NDVR_Z
      do i=1,NDVR_Y
         do j=1,NDVR_X
!            ind=(i-1)*NDVR_X+J
            ind=J+(I-1)*NDVR_X+(K-1)*NDVR_X*NDVR_Y
            dummyX(j)=psi(ind)
         end do
         call dfftw_execute_dft(FFTW_Forward_Master_Plan_1D_X,dummyx,dummyx)
         do j=1,NDVR_X
!            ind=(i-1)*NDVR_X+J
            ind=J+(I-1)*NDVR_X+(K-1)*NDVR_X*NDVR_Y
            PSI(ind)=dummyX(j)
         end do
      end do
    end do
!      call dfftw_destroy_plan(Plan_FFTW)
ENDIF
IF (Dimension.eq.2) THEN
   Do K=1,NDVR_Z
      do j=1,NDVR_X
         do i=1,NDVR_Y
            ind=J+(I-1)*NDVR_X+(K-1)*NDVR_X*NDVR_Y
            dummyY(i)=psi(ind)
         end do
         call dfftw_execute_dft(FFTW_Forward_Master_Plan_1D_Y,dummyY,dummyY)
         do i=1,NDVR_Y
            ind=J+(I-1)*NDVR_X+(K-1)*NDVR_X*NDVR_Y
            PSI(ind)=dummyY(i)
         end do
      end do
   end do
!      call dfftw_destroy_plan(Plan_FFTW)
ENDIF
IF (Dimension.eq.3) THEN
   do j=1,NDVR_X
      do i=1,NDVR_Y
         Do K=1,NDVR_Z
            ind=J+(I-1)*NDVR_X+(K-1)*NDVR_X*NDVR_Y
            dummyZ(k)=psi(ind)
         end do
         call dfftw_execute_dft(FFTW_Forward_Master_Plan_1D_Z,dummyZ,dummyZ)
         do K=1,NDVR_Z
            ind=J+(I-1)*NDVR_X+(K-1)*NDVR_X*NDVR_Y
            PSI(ind)=dummyZ(k)
         end do
      end do
   end do
ENDIF
#endif
end subroutine Get_FFT_Forward_FFTW_1D

subroutine Get_FFT_Forward_FFTW_MPI_1D(Psi,Dimension,MYID)
USE DVR_Parameters
use Orbital_Parallelization_Parameters, ONLY: LocalPsiDim
use, intrinsic:: iso_c_binding
#if MPIFFT
USE MPI_FFT_DATA, ONLY: Plan_mpi_forward
#endif

INTEGER :: Dimension,Status,MYID
COMPLEX*16,   DIMENSION(LocalPsiDim(MyID+1)) :: psi

#if MPIFFT
COMPLEX*16 :: dummy(NDVR_Y),dummyX(NDVR_X)
INTEGER ::  ierr
INTEGER*8 :: I,IND,J
include "fftw3-mpi.f03"

!call Initialize_FFTW_1D(Plan_FFTW,Dimension,0)

IF (Dimension.eq.1) THEN
         do j=1,LocalPsiDim(Myid+1)
            dummyX(j)=psi(j)
         end do
         call fftw_mpi_execute_dft(Plan_mpi_forward, dummyx, dummyx)
         do j=1,LocalPsiDim(Myid+1)
            PSI(j)=dummyX(j)
         end do
!      call dfftw_destroy_plan(Plan_FFTW)
ENDIF
IF (Dimension.eq.2) THEN
   Write(6,*) "Sorry, but 1D MPI FFTs with FFTW in Y direction are currently not implemented..."
   STOP
ENDIF
IF (Dimension.eq.3) THEN
   Write(6,*) "Sorry, but 1D MPI FFTs with FFTW in Z direction are currently not implemented..."
   STOP
ENDIF
#endif
end subroutine Get_FFT_Forward_FFTW_MPI_1D



subroutine Get_FFT_Forward_FFTW_2D(Psi)
USE DVR_Parameters
COMPLEX*16,   DIMENSION(NDVR_X*NDVR_Y*NDVR_Z) :: psi

#if FFTW
include 'fftw3.f'
INTEGER ::  ierr


!call Initialize_FFTW_2D(Plan_FFTW,0)
call dfftw_execute_dft(FFTW_Forward_Master_Plan_2D,psi,psi)
!call dfftw_destroy_plan(Plan_FFTW)

#endif

end subroutine Get_FFT_Forward_FFTW_2D


subroutine Get_FFT_Forward_FFTW_MPI_2D(Phi_P,Phi_Transposed_P)
#if MPIFFT
USE MPI_FFT_DATA, ONLY : Plan_mpi_forward
#endif
USE DVR_Parameters
use Orbital_Parallelization_Parameters, ONLY: LocalPsiDim
use, intrinsic :: iso_c_binding
COMPLEX(C_DOUBLE_COMPLEX), pointer :: Phi_Transposed_P(:,:)
COMPLEX(C_DOUBLE_COMPLEX), pointer :: Phi_P(:,:)

#if MPIFFT
include "fftw3-mpi.f03"

call fftw_mpi_execute_dft(Plan_MPI_forward,Phi_P,Phi_Transposed_P)
#endif

end subroutine Get_FFT_Forward_FFTW_MPI_2D

subroutine Get_FFT_Forward_FFTW_MPI_2D_UNTRANSPOSED(Phi_P)
#if MPIFFT
USE MPI_FFT_DATA, ONLY : Plan_mpi_forward_analysis
#endif
USE DVR_Parameters
use Orbital_Parallelization_Parameters, ONLY: LocalPsiDim
use, intrinsic :: iso_c_binding
COMPLEX(C_DOUBLE_COMPLEX), pointer :: Phi_P(:,:)

#if MPIFFT
include "fftw3-mpi.f03"

call fftw_mpi_execute_dft(Plan_MPI_forward_analysis,Phi_P,Phi_P)
#endif

end subroutine Get_FFT_Forward_FFTW_MPI_2D_UNTRANSPOSED

subroutine Get_FFT_Forward_FFTW_3D(Psi)
USE DVR_Parameters
COMPLEX*16,   DIMENSION(NDVR_X*NDVR_Y*NDVR_Z) :: psi
#if FFTW
include 'fftw3.f'
COMPLEX*16 :: dummy(NDVR_Y),dummyX(NDVR_X)
INTEGER ::  ierr

call dfftw_execute_dft(FFTW_Forward_Master_Plan_3D,psi,psi)

#endif

end subroutine Get_FFT_Forward_FFTW_3D

subroutine Get_FFT_Forward_FFTW_MPI_3D(Phi_P,Phi_Transposed_P)
#if MPIFFT
USE MPI_FFT_DATA, ONLY : Plan_mpi_forward
#endif
USE DVR_Parameters
use Orbital_Parallelization_Parameters, ONLY: LocalPsiDim
use, intrinsic :: iso_c_binding
COMPLEX(C_DOUBLE_COMPLEX), pointer :: Phi_Transposed_P(:,:,:)
COMPLEX(C_DOUBLE_COMPLEX), pointer :: Phi_P(:,:,:)

#if MPIFFT
include "fftw3-mpi.f03"

call fftw_mpi_execute_dft(Plan_MPI_Forward,Phi_P,Phi_Transposed_P)
#endif

end subroutine Get_FFT_Forward_FFTW_MPI_3D

subroutine Get_FFT_Forward_FFTW_MPI_3D_UNTRANSPOSED(Phi_P)
#if MPIFFT
USE MPI_FFT_DATA, ONLY : Plan_mpi_forward_analysis
#endif
USE DVR_Parameters
use Orbital_Parallelization_Parameters, ONLY: LocalPsiDim
use, intrinsic :: iso_c_binding
COMPLEX(C_DOUBLE_COMPLEX), pointer :: Phi_P(:,:,:)

#if MPIFFT
include "fftw3-mpi.f03"

call fftw_mpi_execute_dft(Plan_MPI_Forward_analysis,Phi_P,Phi_P)
#endif

end subroutine Get_FFT_Forward_FFTW_MPI_3D_UNTRANSPOSED



subroutine Get_FFT_Backward_MKL_2D(Psi)
#if MKLFFT
Use MKL_DFTI
#endif
USE omp_lib
USE DVR_Parameters

COMPLEX*16,   DIMENSION(NDVR_X*NDVR_Y*NDVR_Z) :: psi
#if MKLFFT
type(DFTI_DESCRIPTOR), POINTER :: Desc_Handle_Dim1
INTEGER :: Status

!CALL Initialize_MKL_FFT_2D(Desc_Handle_Dim1)

Status = DftiComputeBackward(MKL_Master_Plan_2D,psi)
!Status = DftiFreeDescriptor(Desc_Handle_Dim1)

#endif
end subroutine Get_FFT_Backward_MKL_2D

subroutine Get_FFT_Backward_MKL_3D(Psi)
#if MKLFFT
Use MKL_DFTI
#endif
USE omp_lib
USE DVR_Parameters
COMPLEX*16,   DIMENSION(NDVR_X*NDVR_Y*NDVR_Z) :: psi
#if MKLFFT
INTEGER :: Status

!CALL Initialize_MKL_FFT_3D(Desc_Handle)

Status = DftiComputeBackward(MKL_Master_Plan_3D,psi)
!Status = DftiFreeDescriptor(Desc_Handle)

#endif
end subroutine Get_FFT_Backward_MKL_3D

subroutine Get_FFT_Backward_FFTW_1D(Psi,Dimension)
USE DVR_Parameters
INTEGER :: Dimension
COMPLEX*16,   DIMENSION(NDVR_X*NDVR_Y*NDVR_Z) :: psi
#if FFTW
include 'fftw3.f'
COMPLEX*16 :: dummy(NDVR_Y),dummyX(NDVR_X)
INTEGER ::  ierr
INTEGER :: I,J,K,IND

!call Initialize_FFTW_1D(Plan_FFTW,Dimension,1)

IF (Dimension.eq.1) THEN
   Do K=1,NDVR_Z
      do i=1,NDVR_Y
         do j=1,NDVR_X
!            ind=(i-1)*NDVR_X+J
            ind=J+(I-1)*NDVR_X+(K-1)*NDVR_X*NDVR_Y
            dummyX(j)=psi(ind)
         end do
         call dfftw_execute_dft(FFTW_Backward_Master_Plan_1D_X,dummyx,dummyx)
         do j=1,NDVR_X
!            ind=(i-1)*NDVR_X+J
            ind=J+(I-1)*NDVR_X+(K-1)*NDVR_X*NDVR_Y
            PSI(ind)=dummyX(j)
         end do
      end do
    end do


!     do i=1,NDVR_Y
!         do j=1,NDVR_X
!            ind=(i-1)*NDVR_X+J
!            dummyX(j)=psi(ind)
!         end do
!         call dfftw_execute_dft(FFTW_Backward_Master_Plan_1D_X,dummyx,dummyx)
!         do j=1,NDVR_X
!            ind=(i-1)*NDVR_X+J
!            PSI(ind)=dummyX(j)
!         end do
!      end do
!      call dfftw_destroy_plan(plan_fftw)
ENDIF


IF (Dimension.eq.2) THEN
   Do K=1,NDVR_Z
      do j=1,NDVR_X
         do i=1,NDVR_Y
            ind=J+(I-1)*NDVR_X+(K-1)*NDVR_X*NDVR_Y
            dummy(i)=psi(ind)
         end do
         call dfftw_execute_dft(FFTW_Backward_Master_Plan_1D_Y,dummy,dummy)
         do i=1,NDVR_Y
            ind=J+(I-1)*NDVR_X+(K-1)*NDVR_X*NDVR_Y
            PSI(ind)=dummy(i)
         end do
      end do
   end do

!       do i=1,NDVR_X
!         do j=1,NDVR_Y
!            ind=i+NDVR_X*(J-1)
!            dummy(j)=psi(ind)
!         end do
!         call dfftw_execute_dft(FFTW_Backward_Master_Plan_1D_Y,dummy,dummy)
!         do j=1,NDVR_Y
!            ind=i+NDVR_X*(J-1)
!            PSI(ind)=dummy(j)
!         end do
!      end do
!      call dfftw_destroy_plan(plan_fftw)
ENDIF

IF (Dimension.eq.3) THEN
   do j=1,NDVR_X
      do i=1,NDVR_Y
         Do K=1,NDVR_Z
            ind=J+(I-1)*NDVR_X+(K-1)*NDVR_X*NDVR_Y
            dummy(k)=psi(ind)
         end do
         call dfftw_execute_dft(FFTW_Backward_Master_Plan_1D_Z,dummy,dummy)
         do K=1,NDVR_Z
            ind=J+(I-1)*NDVR_X+(K-1)*NDVR_X*NDVR_Y
            PSI(ind)=dummy(k)
         end do
      end do
   end do
ENDIF
#endif
end subroutine Get_FFT_Backward_FFTW_1D

subroutine Get_FFT_Backward_FFTW_MPI_1D(Psi,Dimension,MYID)
USE DVR_Parameters
use Orbital_Parallelization_Parameters, ONLY: LocalPsiDim_Trans
use, intrinsic:: iso_c_binding
#if MPIFFT
USE MPI_FFT_DATA, ONLY: Plan_mpi_backward
#endif

INTEGER :: Dimension
INTEGER :: MYID
COMPLEX*16,   DIMENSION(LocalPsiDim_Trans(MYID+1)) :: psi,dummyX
#if MPIFFT
INTEGER ::  ierr
INTEGER*8 :: I,J,IND

include "fftw3-mpi.f03"
!call Initialize_FFTW_1D(Plan_FFTW,Dimension,1)

IF (Dimension.eq.1) THEN
         do j=1,LocalPsiDim_Trans(MYID+1)
            dummyX(j)=psi(j)
         end do
         call fftw_mpi_execute_dft(Plan_mpi_backward,dummyx,dummyx)
         do j=1,LocalPsiDim_Trans(MYID+1)
            PSI(j)=dummyX(j)
         end do
!      call dfftw_destroy_plan(plan_fftw)
ENDIF

IF (Dimension.eq.2) THEN
   Write(6,*) "Sorry, but 1D MPI FFTs with FFTW in Y direction are currently not implemented..."
   STOP
ENDIF

IF (Dimension.eq.3) THEN
   Write(6,*) "Sorry, but 1D MPI FFTs with FFTW in Z direction are currently not implemented..."
   STOP
ENDIF
#endif
end subroutine Get_FFT_Backward_FFTW_MPI_1D


subroutine Get_FFT_Backward_FFTW_2D(Psi)
USE DVR_Parameters
#if FFTW
include 'fftw3.f'
#endif
COMPLEX*16,   DIMENSION(NDVR_X*NDVR_Y*NDVR_Z) :: psi
#if FFTW

!call Initialize_FFTW_2D(Plan_FFTW,1)
call dfftw_execute_dft(FFTW_Backward_Master_Plan_2D,psi,psi)
!call dfftw_destroy_plan(Plan_FFTW)
#endif
end subroutine Get_FFT_Backward_FFTW_2D

subroutine Get_FFT_Backward_FFTW_MPI_2D(Phi_Transposed_P,Phi_P)
#if MPIFFT
USE MPI_FFT_DATA, ONLY : Plan_mpi_backward
#endif
USE DVR_Parameters
use Orbital_Parallelization_Parameters, ONLY: LocalPsiDim_Trans
use, intrinsic :: iso_c_binding

COMPLEX(C_DOUBLE_COMPLEX), pointer :: Phi_Transposed_P(:,:)
COMPLEX(C_DOUBLE_COMPLEX), pointer :: Phi_P(:,:)

#if MPIFFT
include "fftw3-mpi.f03"

call fftw_mpi_execute_dft(Plan_MPI_backward,Phi_Transposed_P,Phi_P)
#endif

end subroutine Get_FFT_Backward_FFTW_MPI_2D

subroutine Get_FFT_Backward_FFTW_3D(Psi)
USE DVR_Parameters
COMPLEX*16,   DIMENSION(NDVR_X*NDVR_Y*NDVR_Z) :: psi
#if FFTW
include 'fftw3.f'
INTEGER ::  ierr


!call Initialize_FFTW_3D(Plan_FFTW,1)
call dfftw_execute_dft(FFTW_Backward_Master_Plan_3D,psi,psi)
!call dfftw_destroy_plan(Plan_FFTW)

#endif

end subroutine Get_FFT_Backward_FFTW_3D

subroutine Get_FFT_Backward_FFTW_MPI_3D(Phi_Transposed_P,Phi_P)
#if MPIFFT
USE MPI_FFT_DATA, ONLY : Plan_mpi_backward
#endif
USE DVR_Parameters
use Orbital_Parallelization_Parameters, ONLY: LocalPsiDim_Trans
use, intrinsic :: iso_c_binding

COMPLEX(C_DOUBLE_COMPLEX), pointer :: Phi_Transposed_P(:,:,:)
COMPLEX(C_DOUBLE_COMPLEX), pointer :: Phi_P(:,:,:)

#if MPIFFT
include "fftw3-mpi.f03"

call fftw_mpi_execute_dft(Plan_mpi_backward,Phi_Transposed_P,Phi_P)
#endif

end subroutine Get_FFT_Backward_FFTW_MPI_3D

subroutine FFT_of_AC(NOP, func_TD, func_FD)
integer*8                :: FFT_AC_plan,NOP
complex(kind=8), allocatable   :: func_TD(:), func_FD(:)

include 'fftw3.f'

call dfftw_plan_dft_1d(FFT_AC_plan,NOP,func_TD,func_FD,FFTW_FORWARD,FFTW_ESTIMATE)
call dfftw_execute_dft(FFT_AC_plan, func_TD, func_FD)
call dfftw_destroy_plan(FFT_AC_plan)

end subroutine FFT_of_AC

End Module FFT_Laboratory
