!>GPLv3
!    MCTDH-X: the multiconfigurational time-dependent Hartree for 
!    indistinguishable particles software
!
!    Copyright (C) 2021  A. U. J. Lode, M. C. Tsatsos, 
!                        E. Fasshauer, S. E. Weiner, 
!                        R. Lin, L. Papariello, P. Molignini, 
!                        C. Lévêque, M. Büttner , J. Xiang, S. Dutta
!
!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <https://www.gnu.org/licenses/>.
!
!
!
!
!<GPLv3










!> This Module collects the routines to perform the optimal control run.
MODULE OptimalControl

IMPLICIT NONE
contains
!> This subroutine assigns the goal(s) of the optimization to the global array OCT_Goal
subroutine Get_OCT_Goal(PSI,VIN,OCT_Goal)
USE Global_Parameters,ONLY: Morb,NDVR_X,NDVR_Y,NDVR_Z,whichpot
USE CRAB_PARAMETERS, ONLY : OCT_Ngoals
USE Coefficients_Parameters,ONLY: Npar,Nconf
USE DVR_PARAMETERS

COMPLEX*16 :: PSI(NDVR_X*NDVR_Y*NDVR_Z,Morb)
COMPLEX*16 ::  VIN(Nconf)
COMPLEX*16 :: OCT_Goal(NDVR_X*NDVR_Y*NDVR_Z,OCT_NGoals)

INTEGER :: J

IF (trim(whichpot).eq.'OCT_1D') THEN
  IF(Morb.eq.1) THEN
    DO J=1,NDVR_X
       OCT_Goal(j,1)=(2.d0*Ort_X(J)/sqrt(2.d0)*(1.d0/PI)**0.25 &
            *exp(-Ort_x(j)**2/2.d0))**2
    END DO

  ELSEIF (Morb.gt.1) THEN
    DO J=1,NDVR_X
       OCT_Goal(J,1)=dconjg(PSI(J,2))*PSI(J,2)/weight(J)**2
    END DO
  ENDIF
ENDIF


end subroutine Get_OCT_Goal

!> This subroutine computes the infidelities
    SUBROUTINE Get_Infidelities(VIN,PSI,Natvec,Nocc)

    USE Global_Parameters,ONLY: Morb,NDVR_X,NDVR_Y,NDVR_Z,whichpot
    USE DVR_Parameters
    USE Crab_Parameters
    USE Coefficients_Parameters,ONLY: Npar,Nconf
    USE Auxiliary_Analysis_Routines !for Get_NOs

    REAL*8     :: Nocc(Morb)
    COMPLEX*16 :: NatVec(Morb,Morb)
    COMPLEX*16 :: PSI(NDVR_X*NDVR_Y*NDVR_Z,Morb)

   COMPLEX*16 ::  VIN(Nconf),TMP(NConf)
    COMPLEX*16 ::  PSI_NO(NDVR_X*NDVR_Y*NDVR_Z,Morb)
    COMPLEX*16 ::  Density(NDVR_X*NDVR_Y*NDVR_Z)
    INTEGER    :: I,J
!    
    real*8     ::rnorm 

    CALL Get_NOs(PSI,PSI_NO,NatVec)   ! obtain natural Orbitals

    OCT_Density=0.d0
    DO I=1,Morb
        OCT_Density(:)=OCT_Density(:)+(Nocc(I)/NPar)*dconjg(PSI_NO(:,I))*PSI_NO(:,I)/weight(1)**2
    END DO
       
       
    IF (trim(whichpot).eq.'OCT_1D') THEN
      OCT_Infidelities(1)=Npar*SUM(abs(OCT_Density(:)-OCT_Goal(:,1)))*weight(1)**2!+SUM(sqrt((VIN(:)-TMP)**2))
    ENDIF

    IF (trim(whichpot).eq.'OCT_1DDW') THEN
      DO I=1,OCT_NGoals 
         OCT_Infidelities(I)=Npar*sqrt((Nocc(1)/Npar-0.5d0)**2)+Npar*sqrt((Nocc(2)/Npar-0.5d0)**2) !+SUM(sqrt((VIN(:)-TMP)**2))
      END DO
    ENDIF

    IF (trim(whichpot).eq.'OCT_CavityC') THEN
      DO I=1,OCT_NGoals 
         OCT_Infidelities(I)=Npar*SUM(Nocc(2:Morb))
      END DO
    ENDIF

    IF (trim(whichpot).eq.'OCT_CavityF') THEN
      DO I=1,OCT_NGoals 
         OCT_Infidelities(I)=Npar*sqrt((Nocc(1)/Npar-0.5d0)**2)+Npar*sqrt((Nocc(2)/Npar-0.5d0)**2) !+SUM(sqrt((VIN(:)-TMP)**2))
      END DO
    ENDIF


    IF (Crab_Break.eqv..TRUE.) THEN
       OCT_Infidelities=1.d3
    ENDIF
!>--------------Writing OCT_Goal & Density------------------------------   
!>----------------------------------------------------------------------
    END Subroutine Get_Infidelities

    SUBROUTINE Get_dGammaCRAB(dGamma,T)

    USE CRAB_Parameters
    USE Global_Parameters, ONLY: Time_Begin,Time_Final,OCT_CRAB_Tolerance,PI,OCT_TimeSteps,OCT_NGoals,OCT_NC, &
                                 dCRAB,do_fejer, OCT_Randomize,OCT_NRandomize,OCT_Bounded,OCT_UpperBounds,OCT_LowerBounds, &
                                 OCT_OptOmega
    USE Random

    IMPLICIT NONE


    REAL*8 :: dt_OCT
    REAL*8 :: dGamma(OCT_TimeSteps,OCT_NGoals)

    REAL*8 :: Mask,Mask_norm,Mask_type

    INTEGER :: I,K,SEED,T
    INTEGER, SAVE :: Randomize_Count
    LOGICAL :: DO_Mask=.FALSE.


    IF (CRAB_First.eqv..TRUE.) THEN
       Randomize_Count=0
    ELSE
       Randomize_Count=Randomize_Count+1
    ENDIF

    DO_Mask=.TRUE.
    dt_OCT=(Time_Final-Time_Begin)/OCT_TimeSteps

    Write(6,*) "Global Pars", Time_Final,Time_Begin,OCT_TimeSteps,OCT_NGoals,OCT_NC
    Write(6,*) "As,Bs,ws",OCT_As,OCT_Bs,OCT_Omega,dt_OCT
    dGamma=0.d0
     IF (DO_Fejer.eqv..TRUE.) THEN
!>------------Ferje Series(Masked or not)-------------------------------
      do i=1,OCT_TimeSteps
         DO K=1,OCT_NGoals
           Mask=1.d0
           If(Do_Mask.eqv..true.)then
              Mask_type=1.d0
              Mask=tanh(Mask_Type*((i-1)*dt_OCT))&
              *tanh(Mask_Type*(Time_Final-((i-1)*dt_OCT)+Time_Begin))
           end if 

           if (T.eq.OCT_NC+1) THEN
              dGamma(I,K)=1.d0
           endif
          
           if ((T.ne.OCT_NC+1).and.(T.le.OCT_NC)) THEN
               dGamma(I,K)=dGamma(I,K) &
                          +cos(OCT_Omega(T,K)*((i-1)*dt_OCT+Time_Begin))&
                          *Mask*(OCT_NC+1.d0-T)
           elseif ((T.ne.OCT_NC+1).and.(T.gt.OCT_NC+1)) THEN
               dGamma(I,K)=dGamma(I,K) &
                          +Sin(OCT_Omega(T-OCT_NC-1,K)*((i-1)*dt_OCT+Time_Begin)) &
                          *Mask*(OCT_NC+1.d0-T)
           endif
          
         enddo
      end do
     ELSE
!>---------------Fourier Series(Extended/Masked or not)-----------------
      do i=1,OCT_TimeSteps
         DO K=1,OCT_NGoals
           Mask=1.d0
           If(Do_Mask.eqv..true.)then
              Mask_type=1.d0
              Mask=tanh(Mask_Type*((i-1)*dt_OCT))&
              *tanh(Mask_Type*(Time_Final-((i-1)*dt_OCT)+Time_Begin))
           end if 

           if (T.eq.OCT_NC+1) THEN
              dGamma(I,K)=1.d0
           endif

           if ((T.ne.OCT_NC+1).and.(T.le.OCT_NC)) THEN
               dGamma(I,K)=dGamma(I,K) &
                          +Cos(OCT_Omega(T,K)*((i-1)*dt_OCT+Time_Begin))&
                          *Mask
           elseif ((T.ne.OCT_NC+1).and.(T.gt.OCT_NC+1)) THEN
               dGamma(I,K)=dGamma(I,K) &
                          +Sin(OCT_Omega(T-OCT_NC-1,K)*((i-1)*dt_OCT+Time_Begin)) &
                          *Mask
           endif

         enddo

      end do

     ENDIF
    end subroutine get_dgammaCRAB



!>This routine computes the set of updated control parameters
!>If the optional argument Update=.T. is given, then Gamma and g are updated. 
!>If the optional argument Update=.T. is not given, then only g is updated. 
    SUBROUTINE Get_NewGammaCRAB(Update)

    USE CRAB_Parameters
    USE Global_Parameters, ONLY: Time_Begin,Time_Final,OCT_CRAB_Tolerance,PI,OCT_TimeSteps,OCT_NGoals,OCT_NC, &
                                 dCRAB,do_fejer, OCT_Randomize,OCT_NRandomize,OCT_Bounded,OCT_UpperBounds,OCT_LowerBounds, &
                                 OCT_OptOmega
    USE Random

    IMPLICIT NONE

    LOGICAL, Optional :: Update

    REAL*8 :: dt_OCT
    REAL*8 :: OCT_NewGamma(OCT_TimeSteps,OCT_Ngoals)

    REAL*8 :: Mask,Mask_norm,Mask_type

    INTEGER :: I,J,K,SEED
    INTEGER, SAVE :: Randomize_Count
    LOGICAL :: DO_Mask=.FALSE.


    IF (CRAB_First.eqv..TRUE.) THEN
       Randomize_Count=0
    ELSE
       Randomize_Count=Randomize_Count+1
    ENDIF

    DO_Mask=.TRUE.
    dt_OCT=(Time_Final-Time_Begin)/OCT_TimeSteps

    Write(6,*) "Global Pars", Time_Final,Time_Begin,OCT_TimeSteps,OCT_NGoals,OCT_NC
    Write(6,*) "As,Bs,ws",OCT_As,OCT_Bs,OCT_Omega,dt_OCT

    IF (CRAB_CONTINUE.eqv..TRUE.) THEN
     IF (DO_Fejer.eqv..TRUE.) THEN
!>------------Ferje Series(Masked or not)-------------------------------
      do i=1,OCT_TimeSteps
         DO K=1,OCT_NGoals
           Mask=1.d0
           If(Do_Mask.eqv..true.)then
              Mask_type=1.d0
              Mask=tanh(Mask_Type*((i-1)*dt_OCT))&
              *tanh(Mask_Type*(Time_Final-((i-1)*dt_OCT)+Time_Begin))
           end if 

           OCT_g(I,K)=OCT_As(OCT_NC+1,K)

           do j=1,OCT_NC
             OCT_g(I,K)=OCT_g(I,K)+(OCT_As(j,K)*Cos(OCT_Omega(j,K)*((i-1)*dt_OCT+Time_Begin))&
                        +OCT_Bs(j,K)*Sin(OCT_Omega(j,K)*((i-1)*dt_OCT+Time_Begin)))& 
                        *Mask*(OCT_NC+1.d0-j)
           enddo
         enddo
      end do
     ELSE
!>---------------Fourier Series(Extended/Masked or not)-----------------
      do i=1,OCT_TimeSteps
         DO K=1,OCT_NGoals
           Mask=1.d0
           If(Do_Mask.eqv..true.)then
              Mask_type=1.d0
              Mask=tanh(Mask_Type*((i-1)*dt_OCT))&
              *tanh(Mask_Type*(Time_Final-((i-1)*dt_OCT)+Time_Begin))
           end if 

           OCT_g(I,K)=OCT_As(OCT_NC+1,K)

           do j=1,OCT_NC
             OCT_g(I,K)=OCT_g(I,K)+(OCT_As(j,K)*Cos(OCT_Omega(j,K)*((i-1)*dt_OCT+Time_Begin))&
                        +OCT_Bs(j,K)*Sin(OCT_Omega(j,K)*((i-1)*dt_OCT+Time_Begin)))*Mask
           enddo

         enddo

      end do

     ENDIF

      if (dCRAB.eqv..FALSE.) THEN
        DO I=1,OCT_TimeSteps
           DO K=1,OCT_NGoals
             OCT_CurrentGamma(I,K)=OCT_Gamma(I,K)*Oct_g(I,K)
           enddo
        end do
      Elseif (dCRAB.eqv..TRUE.) THEN
        DO I=1,OCT_TimeSteps

           DO K=1,OCT_NGoals
             OCT_CurrentGamma(I,K)=OCT_Gamma(I,K)+Oct_g(I,K)
           END DO

        end do
      ENDIF
    ENDIF

    IF (OCT_Bounded.eqv..TRUE.) THEN
         DO I=1,OCT_TimeSteps

           DO K=1,OCT_NGoals
             IF (OCT_CurrentGamma(I,K).gt.OCT_UpperBounds(K)) THEN
                OCT_CurrentGamma(I,K)=OCT_UpperBounds(K)
             ELSEIF (OCT_CurrentGamma(I,K).lt.OCT_LowerBounds(K)) THEN
                OCT_CurrentGamma(I,K)=OCT_LowerBounds(K)
             ENDIF
           END DO

        end do
    ENDIF

    IF ((Randomize_Count.ge.OCT_NRandomize).and.(OCT_Randomize.eqv..TRUE.)) THEN
      DO K=1,OCT_NC
       DO J=1,OCT_NGoals    
 
          CALL GNU_Rand(OCT_Rand8)

          OCT_Omega(K,J)=2.d0*K*PI*(1+(OCT_Rand8-0.5d0))  &
                         /(time_final-time_begin)    

        END DO
      END DO 

      Randomize_Count=0

    ENDIF  

    IF (Present(Update).and.Update.eqv..TRUE.) THEN
      IF (OCT_Randomize.eqv..TRUE.) THEN
         Randomize_Count=OCT_NRandomize
      ENDIF
            
      if (dCRAB.eqv..FALSE.) THEN
        do i=1,OCT_TimeSteps
           DO K=1,OCT_NGoals
              OCT_NewGamma(I,K)=OCT_Gamma(I,K)*OCT_g(I,K)
           enddo
        end do
      Elseif (dCRAB.eqv..TRUE.) THEN
        do i=1,OCT_TimeSteps
           DO K=1,OCT_NGoals

             OCT_NewGamma(I,K)=OCT_Gamma(I,K)+OCT_g(I,K)

           enddo
        end do
      ENDIF
  
      IF (SUM(ABS(OCT_Gamma(:,:)-OCT_NewGamma(:,:))).lt.OCT_CRAB_Tolerance) THEN
         BREAK_CG=.TRUE.
         OCT_Gamma=OCT_CurrentGamma
         RETURN
      ENDIF

      IF (CRAB_CONTINUE.eqv..TRUE.) THEN

        OCT_Gamma=OCT_NewGamma
        OCT_CurrentGamma=OCT_NewGamma
        OCT_As=0.d0
        OCT_Bs=0.d0

        IF (OCT_OptOmega.eqv..FALSE.) THEN
          IF (OCT_Bounded_Bandwidth.eqv..FALSE.) THEN
            DO K=1,OCT_NC
              DO J=1,OCT_NGoals    
          
                CALL GNU_Rand(OCT_Rand8)
               
                OCT_Omega(K,J)=2.d0*K*PI*(1+(OCT_Rand8-0.5d0))  &
                               /(time_final-time_begin)    
          
              END DO
            END DO 
          ELSEIF (OCT_Bounded_Bandwidth.eqv..TRUE.) THEN
            
            DO K=1,OCT_NC
              DO J=1,OCT_NGoals    
                CALL GNU_Rand(OCT_Rand8)
                OCT_Omega(K,J)=(1.d0*K+OCT_Rand8-0.5d0)*(OCT_Bandwidth_UpperBound-OCT_Bandwidth_LowerBound)/(OCT_NC+1)
              END DO
            END DO 
          ENDIF
        ENDIF

      ELSEIF(CRAB_CONTINUE.eqv..FALSE.) THEN 
         OCT_Gamma=OCT_CurrentGamma
      ENDIF

    ENDIF

    IF (OCT_Bounded.eqv..TRUE.) THEN
         DO I=1,OCT_TimeSteps

           DO K=1,OCT_NGoals
             IF (OCT_Gamma(I,K).gt.OCT_UpperBounds(K)) THEN
                OCT_Gamma(I,K)=OCT_UpperBounds(K)
             ELSEIF (OCT_Gamma(I,K).lt.OCT_LowerBounds(K)) THEN
                OCT_Gamma(I,K)=OCT_LowerBounds(K)
             ENDIF
           END DO

        end do
    ENDIF

    End Subroutine Get_NewGammaCRAB

END MODULE OptimalControl



